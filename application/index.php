<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
$page_title = '';
$now = time();
$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
//$user = 'id_member_20181019_085817749200_19065';
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>
    <?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>

<!-- On Page CSS -->


<!-- Dashboard CSS -->
<link href="<? echo $fullurl ?>assets/css/gridstack.css" rel="stylesheet">
<style>
.grid-stack-item-content {
	background-color: white;
	border: 3px black solid;
	border-radius: 3px;
	padding: 3px;
}
table {
font-family: arial, sans-serif;
border-collapse: collapse;
width: 100%;
table-layout: fixed;
}

td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}
.section_header {
    margin-top: 10px;
    border-bottom: 1px solid #009688;
    padding-bottom: 3px;
    color: #009688;
}
</style>
</head>

<body>

    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>

           				<!--- PAGE CONTENT HERE ---->
<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span>Dashboard</div>

<div class="panel panel-default">
<div class="panel-body">
  <div class="col-lg-12">


<?
if(in_array('DashboardLiveLeads',$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){

$Month=0;
$Live=0;
$Booking=0;
$Completed=0;
$Outstanding=0;
$Above7Days=0;
    echo '<h3 class="section_header">Live Leads '.date('01 F Y').' - '.date('t F Y').'</h3>';?>
    <div class="row">
          <div class="col-lg-6 col-sm-6">
              <table>
                <tr>

                    <th>Live</th>
                    <th>Booking</th>
                    <th>Completed</th>
                    <th>Outstanding</th>
                    <th>Above 7 Days</th>
                </tr>

                <tr>
                    <td><?echo $Live;?></td>
                    <td><?echo $Booking;?></td>
                    <td><?echo $Completed;?></td>
                    <td><?echo $Outstanding;?></td>
                    <td><?echo $Above7Days;?></td>
                </tr>
            </table>
          </div>
          </div>
<?}

if(in_array('DashboardAccountManager',$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){
$day_start = strtotime(date('01-m-Y 00:00:00'));
$day_end  = strtotime(date('t-m-Y 23:59:59'));

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'1');
$db->bind(4,'rst_install');
$db->execute();
$Liveall = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'1');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Live = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'2');
$db->bind(4,'rst_install');
$db->execute();
$Pauseall = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'2');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Pause = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'3');
$db->bind(4,'rst_install');
$db->execute();
$Completeall = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'3');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Complete = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'0');
$db->bind(4,'rst_install');
$db->execute();
$Killedall = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'0');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Killed = $db->rowcount();

$Knob1=$Live;
$Knob2=$Live+$Pause;
$Knob3=$Live+$Pause+$Complete;
$Knob4=$Live+$Pause+$Complete+$Killed;
$total=$Live+$Pause+$Complete+$Killed;
$totalall=$Liveall+$Pauseall+$Completeall+$Killedall;

echo '<h3 class="section_header">Account Manager '.date('01 F Y').' - '.date('t F Y').'</h3>';


$db->query("select hx_jobs.id FROM hx_jobs JOIN hx_customer ON hx_jobs.customer_id = hx_customer.id  where hx_customer.am = ? and hx_jobs.status = ? AND (hx_jobs.job_status = ? OR hx_jobs.job_status = ? OR hx_jobs.job_status = ?)");
	$db->bind(1,$user);
  $db->bind(2,'1');
	$db->bind(3,'S1');
	$db->bind(4,'S2');
	$db->bind(5,'S3');
$db->execute();
$Survey = $db->rowcount();

$db->query("select hx_jobs.id FROM hx_jobs JOIN hx_customer ON hx_jobs.customer_id = hx_customer.id  where hx_customer.am = ? and hx_jobs.status = ? AND (hx_jobs.job_status = ? OR hx_jobs.job_status = ?)");
	$db->bind(1,$user);
  $db->bind(2,'1');
	$db->bind(3,'C1');
	$db->bind(4,'C2');
$db->execute();
$Compliance = $db->rowcount();

$db->query("select hx_jobs.id FROM hx_jobs JOIN hx_customer ON hx_jobs.customer_id = hx_customer.id  where hx_customer.am = ? and hx_jobs.status = ? AND hx_jobs.job_status = ?");
$db->bind(1,$user);
$db->bind(2,'1');
$db->bind(3,'C3');
$db->execute();
$SendCompliance = $db->rowcount();

$db->query("select hx_jobs.id FROM hx_jobs JOIN hx_customer ON hx_jobs.customer_id = hx_customer.id  where hx_customer.am = ? and hx_jobs.status = ? AND hx_jobs.job_status = ?");
	$db->bind(1,$user);
  $db->bind(2,'1');
	$db->bind(3,'R1');
$db->execute();
$SurveyReview = $db->rowcount();

$db->query("select hx_jobs.id FROM hx_jobs JOIN hx_customer ON hx_jobs.customer_id = hx_customer.id  where hx_customer.am = ? and hx_jobs.status = ? AND (hx_jobs.job_status = ? OR hx_jobs.job_status = ? OR hx_jobs.job_status = ? OR hx_jobs.job_status = ?)");
	$db->bind(1,$user);
  $db->bind(2,'1');
	$db->bind(3,'I1');
	$db->bind(4,'I2');
	$db->bind(5,'I3');
    $db->bind(6,'O2');
$db->execute();
$Install = $db->rowcount();

$db->query("select hx_jobs.id FROM hx_jobs JOIN hx_customer ON hx_jobs.customer_id = hx_customer.id  where hx_customer.am = ? and hx_jobs.status = ? AND (hx_jobs.job_status = ? OR hx_jobs.job_status = ? OR hx_jobs.job_status = ? OR hx_jobs.job_status = ? OR hx_jobs.job_status = ?) OR (hx_jobs.job_status = ? AND hx_jobs.job_type = ?)");
	$db->bind(1,$user);
  $db->bind(2,'1');
	$db->bind(3,'T1');
	$db->bind(4,'T2');
	$db->bind(5,'T3');
    $db->bind(6,'T6');
    $db->bind(7,'T8');
    $db->bind(8,'E1');
    $db->bind(9,'tm_elect');
$db->execute();
$TechMonitoring = $db->rowcount();
?>
<div class="row">
      <div class="col-lg-3 col-sm-3">
        <table>
        <tr>
        <th>Jobs</th>
        <th>value</th>
        </tr>

        <tr>
        <th>Survey</th>
        <td><?echo $Survey?></td>
        </tr>
        <tr>
        <th>Compliance</th>
        <td><?echo $Compliance?></td>
        </tr>
        <tr>
        <th>Survey Review</th>
        <td><?echo $SurveyReview?></td>
        </tr>
        <tr>
        <th>Install</th>
        <td><?echo $Install?></td>
        </tr>
        <tr>
        <th>Tech Monitoring</th>
        <td><?echo $TechMonitoring?></td>
        </tr>
        <tr>
        <th>Send Compliance</th>
        <td><?echo $SendCompliance?></td>
        </tr>


        </table>
      </div>
      <div class="col-lg-2 col-sm-2">
      <div class="pager" style="min-height:200px;">
      <input type="text" value="<?echo $total?>" class="dial5" readonly>
      <input type="text" value="<?echo $Knob4?>" class="dial4" readonly data-displayInput=false>
      <input type="text" value="<?echo $Knob3?>" class="dial3" readonly data-displayInput=false>
      <input type="text" value="<?echo $Knob2?>" class="dial2" readonly data-displayInput=false>
      <input type="text" value="<?echo $Knob1?>" class="dial1" readonly data-displayInput=false>
      </div>
      </div>
      <div class="col-lg-2 col-sm-2"></div>
      <div class="col-lg-5 col-sm-5">
      <table>
      <tr>
      <th style="width:50px;">Key</th>
      <th>Jobs Status</th>
      <th>Yours</th>
      <th>Company Wide</th>
      </tr>

      <tr>
      <th style="background-color:rgb(76, 175, 80)"></th>
      <th>Live</th>
      <td><?echo $Live?></td>
      <td><?echo $Liveall?></td>
      </tr>
      <tr>
      <th style="background-color:rgb(248, 189, 27)"></th>
      <th>Pause</th>
      <td><?echo $Pause?></td>
      <td><?echo $Pauseall?></td>
      </tr>
      <tr>
      <th style="background-color:rgb(15, 178, 252"></th>
      <th>Complete</th>
      <td><?echo $Complete?></td>
      <td><?echo $Completeall?></td>
      </tr>
      <tr>
      <th style="background-color:rgb(244, 67, 54)"></th>
      <th>Killed</th>
      <td><?echo $Killed?></td>
      <td><?echo $Killedall?></td>
      </tr>
      <tr>
      <th></th>
      <th>Total</th>
      <td><?echo $total;?></td>
      <td><?echo $totalall;?></td>
      </tr>
      </table>
      </div>
      </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-6">
          <div id="AccountManagerChart" style="margin: 0 auto; display:table;">Loading</div>
        </div>
        <div class="col-lg-6">
          <div id="AccountManagerAllChart" style="margin: 0 auto; display:table;">Loading</div>
        </div>
    </div>

<?
}


if(in_array('DashboardInstallers',$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){
//$user = 'id_member_20170906_165249907800_89096';

$day_start = strtotime(date('01-m-Y 00:00:00'));
$day_end  = strtotime(date('t-m-Y 23:59:59'));

$db->query("SELECT
hx_appointments.id as appid,
hx_appointments.contact_id,
hx_appointments.app_to,
hx_appointments.status as appstatus ,
hx_jobs.callback,
hx_jobs.id AS jobid,
hx_jobs.job_status,
hx_appointments.app_from,
hx_appointments.booktype,
hx_customer.*
FROM hx_appointments
JOIN hx_jobs ON hx_appointments.jobid = hx_jobs.id
JOIN hx_customer ON hx_appointments.contact_id = hx_customer.id
WHERE
hx_appointments.created_for = ? AND
hx_jobs.status = ? AND
(hx_jobs.job_status= ? OR
hx_jobs.job_status= ? OR
hx_jobs.job_status= ?) AND
hx_appointments.status = ? AND
hx_appointments.booktype = ?
order by hx_appointments.app_from ASC ");
$db->bind(1,$user);
$db->bind(2,'1');
$db->bind(3,'T5');
$db->bind(4,'T6');
$db->bind(5,'T7');
$db->bind(6,'4');
$db->bind(7,'inbook');
$myfail = $db->resultset();
$myfailcount = $db->rowcount();


$db->query("SELECT
hx_appointments.id as appid
FROM hx_appointments
JOIN hx_jobs ON hx_appointments.jobid = hx_jobs.id
WHERE
hx_appointments.created_for = ? AND
(hx_jobs.status = ? OR hx_jobs.status = ?) AND
(hx_jobs.job_status= ? OR
hx_jobs.job_status= ?) AND
hx_appointments.status = ? AND
hx_appointments.booktype = ?
order by hx_appointments.app_from ASC ");
$db->bind(1,$user);
$db->bind(2,'1');
$db->bind(3,'3');
$db->bind(4,'C3');
$db->bind(5,'Complete');
$db->bind(6,'4');
$db->bind(7,'inbook');
$mypass= $db->resultset();
$mypasscount = $db->rowcount();

$mytotal=$myfailcount+$mypasscount;

$Boiler=0;
$Flue=0;
$Condense=0;
$Insulation=0;
$Pipework=0;
$BlowOff=0;
$Radiators=0;
$Stat=0;
$MakingGood=0;
$Benchmark=0;
$Certificate=0;
$MissingPictures=0;
$over7=0;


echo '<h3 class="section_header">Installer</h3>';
if($myfailcount!=0){
?>
<h3>Failed Technical Monitoring Jobs</h3>
<table class="table">

<tr>
<th>Type</th>
<th>Customer Name</th>
<th>Appointment</th>
<th>Address 1</th>
<th>Town</th>
<th>Post Code</th>
</tr>

<?


$days7= strtotime("-7 day");

foreach ($myfail as $fail) {

    $db->Query("select fail_list, created_date from hx_technical_monitoring_visit where jobid = ? order by created_date desc limit 1");
    $db->bind(1, $fail['jobid']);
    $myfaillist = $db->single();

    if (strpos($myfaillist['fail_list'], 'Boiler & Flue') == true) {$Boiler++;}
    if (strpos($myfaillist['fail_list'], 'Condense') == true) {$Condense++;}
    if (strpos($myfaillist['fail_list'], 'Insulation') == true) {$Insulation++;}
    if (strpos($myfaillist['fail_list'], 'Gas/Oil Pipework') == true) {$Pipework++;}
    if (strpos($myfaillist['fail_list'], 'Blow off') == true) {$BlowOff++;}
    if (strpos($myfaillist['fail_list'], 'Radiators') == true) {$Radiators++;}
    if (strpos($myfaillist['fail_list'], 'Stat') == true) {$Stat++;}
    if (strpos($myfaillist['fail_list'], 'Making good') == true) {$MakingGood++;}
    if (strpos($myfaillist['fail_list'], 'Benchmark') == true) {$Benchmark++;}
    if (strpos($myfaillist['fail_list'], 'Certificate') == true) {$Certificate++;}
    if (strpos($myfaillist['fail_list'], 'Missing Pictures') == true) {$MissingPictures++;}
    if($myfaillist['created_date'] < $days7){$over7++;}

$timestamp=$fail['app_from'];
$thisapp = gmdate("dS M Y H:i:s", $timestamp);


$thisButton = '<span class="label label-success">Due</span>';
$link=$fail["nametitle"].' '.$fail["surname"];


$row = '<td><a href="../../application/customers/customer_view.php?id='.$fail["id"].'">Installation</a></td><td class="text-capitalize">'.$link.'</td><td>'. $thisapp .'</td><td>'. $fail["ad_title"].' '.$fail["address1"].'</td><td>'. $fail["city"] .'</td><td>'. $fail["postcode"] .'</td>';


?>
<tr>
<? echo $row; ?>
</tr>
<? } ?>

</table>
<?}?>
      <!-- install table-->
     <div class="row">
      <div class="col-lg-6">
      <table class="table">
          <tr>
              <th>Outstanding Remedial</th>
              <th>Remedials Over 7 days</th>
              <th>Total Passed</th>
              <th>Total</th>

          </tr>
          <tr>
              <td><?echo $myfailcount;?></td>
              <td><?echo $over7;?></td>
              <td><?echo $mypasscount;?></td>
              <td><?echo $mytotal;?></td>
          </tr>
      </table>
  </div>

  <div class="col-lg-6">
      <table class="table">
              <tr>
                  <th></th>
                  <th>Total</th>
                  <th></th>
                  <th>Total</th>
              </tr>
                <tr>
                    <th>Boiler & Flue</th>
                    <td><? echo $Boiler; ?></td>
                    <th>Radiators</th>
                    <td><? echo $Radiators; ?></td>
                </tr>
                <tr>
                    <th>Pipework</th>
                    <td><? echo $Pipework; ?></td>
                    <th>Stat</th>
                    <td><? echo $Stat; ?></td>
                </tr>
                <tr>
                    <th>Condense</th>
                    <td><? echo $Condense; ?></td>
                    <th>Making Good</th>
                    <td><? echo $MakingGood; ?></td>
                </tr>
                <tr>
                    <th>Insulation</th>
                    <td><? echo $Insulation; ?></td>
                    <th>Benchmark</th>
                    <td><? echo $Benchmark; ?></td>
                </tr>
                <tr>
                    <th>Blow Off</th>
                    <td><? echo $BlowOff; ?></td>
                    <th>Certificate</th>
                    <td><? echo $Certificate; ?></td>
                </tr>
                <tr>
                    <th>Missing Pictures</th>
                    <td><? echo $MissingPictures; ?></td>
                    <th></th>
                    <td></td>
                </tr>
     </table>
 </div>
</div>

  </div>
  </div>
<?}?>


  <!--  <div class="col-lg-12">
     <div class="grid-stack"> <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="5" data-gs-height="4"> <div class="grid-stack-item-content"><div id="topleft" style="margin: 0 auto; display:table;">Loading</div> </div> <div class="grid-stack-item-content"><div style="margin: 0 auto; display:table; margin-top:20px;"> <p style="text-align:center; font-weight:bold; margin-bottom:5px;">Time Worked This Week</p> <input type="text" value="0" class="dial" readonly> </div></div> </div> <div class="grid-stack-item" data-gs-x="5" data-gs-y="0" data-gs-width="5" data-gs-height="4"> <div class="grid-stack-item-content"><div id="topright" style="margin: 0 auto; display:table;">Loading</div> </div> </div> <div class="grid-stack-item" data-gs-x="0" data-gs-y="4" data-gs-width="6" data-gs-height="4"> <div class="grid-stack-item-content"><div id="4topleft" style="margin: 0 auto; display:table;">Loading</div> </div> </div> <div class="grid-stack-item" data-gs-x="6" data-gs-y="4" data-gs-width="4" data-gs-height="4"> <div class="grid-stack-item-content"><div style="margin: 0 auto; display:table; margin-top:20px;"> <p style="text-align:center; font-weight:bold; margin-bottom:5px;">Active Proposals</p> <p style="text-align:center; margin-bottom:20px;">Active Total</p> <input type="text" value="23" class="dial" readonly> </div></div> </div> <div class="grid-stack-item" data-gs-x="0" data-gs-y="8" data-gs-width="6" data-gs-height="4"> <div class="grid-stack-item-content"><div id="4topright" style="margin: 0 auto; display:table;">Loading</div> </div> </div> <div class="grid-stack-item" data-gs-x="6" data-gs-y="8" data-gs-width="4" data-gs-height="4"> <div class="grid-stack-item-content"><div style="margin: 0 auto; display:table; margin-top:20px;"> <p style="text-align:center; font-weight:bold; margin-bottom:5px;">Active Commisions</p> <p style="text-align:center; margin-bottom:20px;">Active Total</p> <input type="text" value="42" class="dial" readonly> </div></div> </div> </div>
  </div> -->
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->







</body>

</html>

<!-- PAGE JS -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<?
    //JS Include
 ?>
    <script src="<? echo $fullurl ?>assets/app_scripts/index.js"></script>



    <!-- Dashboard JS -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.2.0/knockout-min.js"></script>


<?
        //JS Include
        include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

<script src="<? echo $fullurl ?>assets/js/gridstack.js"></script>
<script src="<? echo $fullurl ?>assets/js/gridstack.jQueryUI.min.js"></script>
<script src="<? echo $fullurl ?>assets/js/jquery.knob.js"></script>

<!-- Charting -->
<script src="<? echo $fullurl ?>assets/charts/fusioncharts.js"></script>
<script src="<? echo $fullurl ?>assets/charts/themes/fusioncharts.theme.fint.js"></script>
<script src="<? echo $fullurl ?>assets/charts/themes/fusioncharts.theme.carbon.js"></script>
<script src="<? echo $fullurl ?>assets/charts/themes/fusioncharts.theme.ocean.js"></script>
<script src="<? echo $fullurl ?>assets/charts/themes/fusioncharts.theme.zune.js"></script>

<script>
var fullurl = '<?echo $fullurl;?>';
$(function () {
$('.knob').knob({});
$(".knob").parent("div").css("position", "absolute");
});

$(function() {

$(".dial1").knob({
'min':0,
'max':'<?echo $total;?>',
'data-displayInput':false,
"fgColor":"rgb(76, 175, 80)",
"bgColor":"transparent",

});
$(".dial2").knob({
'min':0,
'max':'<?echo $total;?>',
"fgColor":"rgb(248, 189, 27)",
"bgColor":"transparent",
});
$(".dial3").knob({
'min':0,
'max':'<?echo $total;?>',
"fgColor":"rgb(15, 178, 252)",
"bgColor":"transparent",
});
$(".dial4").knob({
'min':0,
'max':'<?echo $total;?>',
"fgColor":"rgb(244, 67, 54)",
"bgColor":"transparent",
});
$(".dial5").knob({
'min':0,
'max':'<?echo $total;?>',
"fgColor":"rgb(76, 175, 80)",
"bgColor":"transparent",
});
$(".dial1").parent("div").css("position", "absolute");
$(".dial2").parent("div").css("position", "absolute");
$(".dial3").parent("div").css("position", "absolute");
$(".dial4").parent("div").css("position", "absolute");
$(".dial5").parent("div").css("position", "absolute");
});
</script>

<script>
$(function() {
 $(".dial").knob({
     'min':0,
     'max':100,
     "fgColor":"rgb(108, 170, 3)",
 });

 $(".dialLate").knob({
     'min':0,
     'max':100,
     "fgColor":"rgb(228, 75, 2)",
 });
 $(".dialOnHold").knob({
     'min':0,
     'max':100,
     "fgColor":"rgb(248, 189, 27)",
 });
});

</script>

<script type="text/javascript">
$(function () {
    var options = {
        cell_height: 80,
        vertical_margin: 10
    };
    $('.grid-stack').gridstack(options);
});

FusionCharts.ready(function(){
//    var APMontly = new FusionCharts({
//     type: 'angulargauge',
//     renderAt: 'topleft',
//     width: '450',
//     height: '300',
//     dataFormat: 'json',
//     dataSource: {
//     "chart": {
//         "caption": "Complete Proposals",
//         "subcaption": "This Month",
//         "theme": "fint",
//         "chartBottomMargin": "50",
//         "showValue": "1"
//     },
//     "colorRange": {
//         "color": [
//             {
//                 "minValue": "0",
//                 "maxValue": "20",
//                 "code": "#e44a00"
//             },
//             {
//                 "minValue": "21",
//                 "maxValue": "45",
//                 "code": "#f8bd19"
//             },
//             {
//                 "minValue": "46",
//                 "maxValue": "60",
//                 "code": "#6baa01"
//             }
//         ]
//     },
//     "dials": {
//         "dial": [
//             {
//                 "value": "24"
//             }
//         ]
//     }
// }
// }
// );

<?if(in_array('DashboardAccountManager',$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){?>
   var SOPchart = new FusionCharts({
      type: 'stackedcolumn2d',
      renderAt: 'AccountManagerChart',
      width: '100%',
      height: '300',
      dataFormat: 'json',
      dataSource: {
          "chart": {
              "caption": "Account Manager Chart",
              "subCaption": "",
              "xAxisname": "Month",
              "yAxisMaxValue": "10",
              "yAxisName": "Leads",
              "showSum": "0",
              "numberPrefix": "",
              "paletteColors": "#4cae50,#F8BD1C,#2AB1FB,#f44336",
              "theme": "fint",
              "lineThickness":"1"
          },

          "categories": [{
              "category": [{
                  "label": "Jan"
              }, {
                  "label": "Feb"
              }, {
                  "label": "Mar"
              }, {
                  "label": "Apr"
              }, {
                  "label": "May"
              }, {
                  "label": "Jun"
              }, {
                  "label": "Jul"
              }, {
                  "label": "Aug"
              }, {
                  "label": "Sep"
              }, {
                  "label": "Oct"
              }, {
                  "label": "Nov"
              }, {
                  "label": "Dec"
              }]
          }],
<? $Livearray= array();
$Pausearray= array();
$Completearray= array();
$Killedarray= array();
$day_startarray= array();
$day_endarray= array();
$i=1;
while ($i <= 12) {

$day_start = strtotime(date('01-'.$i.'-Y 00:00:00'));
$day_end  = strtotime(date('t-'.$i.'-Y 23:59:59'));

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'1');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Live = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'2');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Pause = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'3');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Complete = $db->rowcount();

$db->query("select hx_customer.id from hx_customer
join hx_jobs on hx_jobs.customer_id = hx_customer.id
where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ? and hx_customer.am = ?");
$db->bind(1,$day_start);
$db->bind(2,$day_end);
$db->bind(3,'0');
$db->bind(4,'rst_install');
$db->bind(5,$user);
$db->execute();
$Killed = $db->rowcount();

array_push($Livearray,$Live);
array_push($Pausearray,$Pause);
array_push($Completearray,$Complete);
array_push($Killedarray,$Killed);

$i++;
}?>

          "dataset": [{
              "seriesname": "Live",
              "data": [
                <?
                foreach ($Livearray as $key => $v) {
                  if($key=='11'){
                    echo '{"value": "'.$v.'"}';
                  }else{
                    echo '{"value": "'.$v.'"},';
                  }
                }
                ?>]
          }, {
              "seriesname": "Pause",
              "data": [<? foreach ($Pausearray as $key => $v) {
                if($key=='11'){
                  echo '{"value": "'.$v.'"}';
                }else{
                  echo '{"value": "'.$v.'"},';
                }
              }
              ?>]
          }, {
              "seriesname": "Complete",
              "data": [<? foreach ($Completearray as $key => $v) {
                if($key=='11'){
                  echo '{"value": "'.$v.'"}';
                }else{
                  echo '{"value": "'.$v.'"},';
                }
              }
              ?>]
          }, {
              "seriesname": "Killed",
              "data": [<? foreach ($Killedarray as $key => $v) {
                if($key=='11'){
                  echo '{"value": "'.$v.'"}';
                }else{
                  echo '{"value": "'.$v.'"},';
                }
              }
              ?>]
          }]
      }
  }
  );
   SOPchart.render();

   var SOPchart2 = new FusionCharts({
      type: 'stackedcolumn2d',
      renderAt: 'AccountManagerAllChart',
      width: '100%',
      height: '300',
      dataFormat: 'json',
      dataSource: {
          "chart": {
              "caption": "All Account Manager Chart",
              "subCaption": "",
              "xAxisname": "Month",
              "yAxisMaxValue": "10",
              "yAxisName": "Leads",
              "showSum": "0",
              "numberPrefix": "",
              "paletteColors": "#4cae50,#F8BD1C,#2AB1FB,#f44336",
              "theme": "fint",
              "lineThickness":"1"
          },

          "categories": [{
              "category": [{
                  "label": "Jan"
              }, {
                  "label": "Feb"
              }, {
                  "label": "Mar"
              }, {
                  "label": "Apr"
              }, {
                  "label": "May"
              }, {
                  "label": "Jun"
              }, {
                  "label": "Jul"
              }, {
                  "label": "Aug"
              }, {
                  "label": "Sep"
              }, {
                  "label": "Oct"
              }, {
                  "label": "Nov"
              }, {
                  "label": "Dec"
              }]
          }],
   <? $Livearray= array();
   $Pausearray= array();
   $Completearray= array();
   $Killedarray= array();
   $day_startarray= array();
   $day_endarray= array();
   $i=1;
   while ($i <= 12) {

   $day_start = strtotime(date('01-'.$i.'-Y 00:00:00'));
   $day_end  = strtotime(date('t-'.$i.'-Y 23:59:59'));

   $db->query("select hx_customer.id from hx_customer
   join hx_jobs on hx_jobs.customer_id = hx_customer.id
   where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
   $db->bind(1,$day_start);
   $db->bind(2,$day_end);
   $db->bind(3,'1');
   $db->bind(4,'rst_install');
   $db->execute();
   $Live = $db->rowcount();

   $db->query("select hx_customer.id from hx_customer
   join hx_jobs on hx_jobs.customer_id = hx_customer.id
   where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
   $db->bind(1,$day_start);
   $db->bind(2,$day_end);
   $db->bind(3,'2');
   $db->bind(4,'rst_install');
   $db->execute();
   $Pause = $db->rowcount();

   $db->query("select hx_customer.id from hx_customer
   join hx_jobs on hx_jobs.customer_id = hx_customer.id
   where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
   $db->bind(1,$day_start);
   $db->bind(2,$day_end);
   $db->bind(3,'3');
   $db->bind(4,'rst_install');
   $db->execute();
   $Complete = $db->rowcount();

   $db->query("select hx_customer.id from hx_customer
   join hx_jobs on hx_jobs.customer_id = hx_customer.id
   where hx_customer.created_date > ? and hx_customer.created_date < ? and hx_jobs.status= ? and hx_jobs.job_type= ?");
   $db->bind(1,$day_start);
   $db->bind(2,$day_end);
   $db->bind(3,'0');
   $db->bind(4,'rst_install');
   $db->execute();
   $Killed = $db->rowcount();

   array_push($Livearray,$Live);
   array_push($Pausearray,$Pause);
   array_push($Completearray,$Complete);
   array_push($Killedarray,$Killed);

   $i++;
   }?>

          "dataset": [{
              "seriesname": "Live",
              "data": [
                <?
                foreach ($Livearray as $key => $v) {
                  if($key=='11'){
                    echo '{"value": "'.$v.'"}';
                  }else{
                    echo '{"value": "'.$v.'"},';
                  }
                }
                ?>]
          }, {
              "seriesname": "Pause",
              "data": [<? foreach ($Pausearray as $key => $v) {
                if($key=='11'){
                  echo '{"value": "'.$v.'"}';
                }else{
                  echo '{"value": "'.$v.'"},';
                }
              }
              ?>]
          }, {
              "seriesname": "Complete",
              "data": [<? foreach ($Completearray as $key => $v) {
                if($key=='11'){
                  echo '{"value": "'.$v.'"}';
                }else{
                  echo '{"value": "'.$v.'"},';
                }
              }
              ?>]
          }, {
              "seriesname": "Killed",
              "data": [<? foreach ($Killedarray as $key => $v) {
                if($key=='11'){
                  echo '{"value": "'.$v.'"}';
                }else{
                  echo '{"value": "'.$v.'"},';
                }
              }
              ?>]
          }]
      }
   }
   );
   SOPchart2.render();
   <?}?>
});

</script>

            <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
