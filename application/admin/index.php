<?php
/**
*  ______   ____    ____
* |   _  \  \   \  /   /
* |  |_)  |  \   \/   /
* |   _  <    \      /
* |  |_)  |    \    /
* |______/      \__/
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = '1' order by name asc");
 $accounts = $db->resultSet();
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>

<!-- On Page CSS -->
<style>

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>
            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span><span class="menuText">Customers</span></div>
								<div class="btn-group"></div>
								<div class="clearfix"></div>
								<div class="panel panel-default">
								<div class="panel-body">
                  <div class="btn-group-holder">
                  <div class="btn-group" style="padding:5px;">
                      <a href="javascript:void(0)" data-target="#" class="btn dropdown-toggle" style="padding: 0; padding-top: 5px;padding-bottom: 5px;" data-toggle="dropdown" aria-expanded="false" id="tableView" value="all_view"> Fuel Filter <span class="caret"></a>
                      <ul class="dropdown-menu">
                          <li><a href="javascript:void(0)" class="changeCustView" id="all_fuel">Fuel Filter</a></li>
                          <li><a href="javascript:void(0)" class="changeCustView" id="gas">Gas</a></li>
                          <li><a href="javascript:void(0)" class="changeCustView" id="oil">Oil</a></li>
                          <li><a href="javascript:void(0)" class="changeCustView" id="electric">Electric</a></li>
                          <li><a href="javascript:void(0)" class="changeCustView" id="lpg">LPG</a></li>
                          <li><a href="javascript:void(0)" class="changeCustView" id="nafuel">N/A</a></li>
                      </ul>
                  </div>
                  <div class="btn-group" style="padding:5px;">
                      <a href="javascript:void(0)" data-target="#" class="btn dropdown-toggle" style="padding: 0; padding-top: 5px;padding-bottom: 5px;" data-toggle="dropdown" aria-expanded="false" id="typeview" value="lead_type"> Lead Type <span class="caret"></a>
                      <ul class="dropdown-menu">
                          <li><a href="javascript:void(0)" class="changeLeadType" id="all_lead">Lead Type</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="b">Boiler</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="bl">Boiler &amp; Loft</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="bc">Boiler &amp; Cav</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="blc">Boiler &amp; Loft &amp; Cav</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="s">Storage Heater</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="sl">Storage Heater &amp; Loft</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="sc">Storage Heater &amp; Cav</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="slc">Storage Heater &amp; Loft &amp; Cav</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="c">Cav</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="l">Loft</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="lc">Loft &amp; Cav</a></li>
                          <li><a href="javascript:void(0)" class="changeLeadType" id="nalead">N/A</a></li>
                      </ul>
                  </div>
                  <?if(in_array('see_all_customers',$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){?>
                  <div class="btn-group" style="padding:5px;">
                    <a href="javascript:void(0)" data-target="#" class="btn dropdown-toggle" style="padding: 0; padding-top: 5px;padding-bottom: 5px;" data-toggle="dropdown" aria-expanded="false" id="ManagerView" value="all">Account Manager<span class="caret"><span class="ManagerID" data-id="all"></span></a>

                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)" class="changeManagerStatus" id="all">Account All</a></li>
                        <?foreach ($accounts as $a){
                                 echo '<li><a href="javascript:void(0)" class="changeManagerStatus" id="'.$a['id'].'">'.$a['name'].'</a></li>';
                        }?>
                    </ul>
                  </div>
                  <?}?>
                  </div>
                    <table id="table" class="striped"
                    data-toggle="table"
                    data-show-export="true"
                    data-export-types="['excel']"
                    data-click-to-select="true"
                    data-filter-control="true"
                    data-show-columns="true"
                    data-show-refresh="true"

                    data-url="../../../assets/app_ajax/customer/index/listcustomer.php"
                    data-height="400"
                    data-side-pagination="server"
                    data-pagination="true"
                    data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                    data-search="true"
                    >
                    <thead>
    	            <tr>
                        <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                        <th data-field="lead" data-sortable="true" data-visible="true">Lead</th>
                        <th data-field="firstname" data-sortable="true" data-visible="true" data-formatter="LinkFormatter">First Name</th>
                        <th data-field="surname" data-sortable="true" data-visible="true" data-formatter="LinkFormatter">Surname</th>
                        <th data-field="city" data-sortable="true" data-visible="false">Town</th>
                        <th data-field="postcode" data-sortable="true" data-visible="true">Post Code</th>
                        <th data-field="telephone" data-sortable="true" data-visible="true">Telephone</th>
                        <th data-field="email" data-sortable="true" data-visible="true">Email</th>
                        <th data-field="name" data-sortable="true" data-visible="true">Account Manager</th>
                        <th data-field="lead_type_1" data-sortable="true" data-visible="true" data-formatter="" >Fuel</th>
                        <th data-field="lead_type_2" data-sortable="true" data-visible="true" data-formatter="">Lead Type</th>
                        <th data-field="created_date" data-sortable="true" data-visible="false" data-formatter="DateFormatter">Added On</th>
                        <!-- <th data-field="supplier" data-sortable="true" data-visible="false">Supplier</th> -->

    	            </tr>
    	            	</thead>
    	        	</table>

								</div>
							</div>

            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');
?>


</body>
</html>

<!-- PAGE JS -->
<script>
$( document ).ready(function() {


    var $table = $('#table');
    	 var $sixty = $( document ).height();
        $sixty = ($sixty/100)*70;
         $table.bootstrapTable( 'resetView' , {height: $sixty} );


         $(function () {
             $('.changeCustView').click(function () {
               var id = $(this).attr("id");
               var name = $(this).text();
               $("#tableView").html(name + '<span class="caret"><span class="fuelID" data-id="'+id+'">');
               $('#tableView').val(id);
               var fuel = $('.fuelID').attr("data-id");
               var type = $('.typeID').attr("data-id");
               var manager = $('.ManagerID').attr("data-id");
               $table.bootstrapTable('refresh', {
                  url: '../../../assets/app_ajax/customer/index/listcustomer.php?fid='+fuel+'&tid='+type+'&manager='+manager+''
              });
             });
         });

         $(function () {
             $('.changeLeadType').click(function () {
               var id = $(this).attr("id");
               var name = $(this).text();
               $("#typeview").html(name + '<span class="caret"><span class="typeID" data-id="'+id+'">');
               $('#typeview').val(id);
               var fuel = $('.fuelID').attr("data-id");
               var type = $('.typeID').attr("data-id");
               var manager = $('.ManagerID').attr("data-id");
               $table.bootstrapTable('refresh', {
                  url: '../../../assets/app_ajax/customer/index/listcustomer.php?fid='+fuel+'&tid='+type+'&manager='+manager+''
              });
             });
         });
         $(function () {
             $('.changeManagerStatus').click(function () {
               var id = $(this).attr("id");
               var name = $(this).text();
               $("#ManagerView").html(name + '<span class="caret"><span class="ManagerID" data-id="'+id+'">');
               $('#ManagerView').val(id);
               var fuel = $('.fuelID').attr("data-id");
               var type = $('.typeID').attr("data-id");
               var manager = $('.ManagerID').attr("data-id");
               $table.bootstrapTable('refresh', {
                  url: '../../../assets/app_ajax/customer/index/listcustomer.php?fid='+fuel+'&tid='+type+'&manager='+manager+''
              });
             });
         });
     });
    $fileName="Customers_<?echo date('d-m-Y');?>";
</script>
<script src="<? echo $fullurl ?>assets/app_scripts/customer/index.js"></script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/inuse_modals.php'); ?>
<?  //include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/customer/index.php'); ?>

<? ob_end_flush(); ?>
