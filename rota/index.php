<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

if(empty($_POST)){


    }
    else{


}

$PW = generateRandomString(7);

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>


<!-- On Page CSS -->
<style>
label.this-error{
    color:#f5594d !important;
}
.qualifi_holder{
    border:#d2d2d2 solid 1px;
    width: 100%;
    padding: 1% 2%;
}
.delete {
    margin: 0px auto;
    padding: 2px 7px 0px 7px !important;
    color: #fff !important;
    background-color: ##f44336 !important;
    border-color: #d43f3a !important;
}

.delete:hover {
    margin: 0px auto;
    padding: 2px 7px 0px 7px !important;
    color: #fff !important;
    background-color: #f55549 !important;
    border-color: ##ac2925 !important;
}
</style>
<style type="text/css">

  li.list-group-item.upImhLi {
      width: 105%;
      float: right;
  }

  .gallery_images {
      height: 200px;
      width: 200px;
      background-size: cover;
      background-position: center;
  }

  .install-gallery {
       padding-bottom: 10px;
  }
  .install-gallery > ul {
    margin-bottom:0;
  }
  .install-gallery > ul > li {
       float: left;
       margin-top: 15px;
       margin-left: 20px;
       width: 200px;
  }

  .install-gallery > ul > li a {
    border: 3px solid #000;
    border-radius: 3px;
    display: block;
    overflow: hidden;
    position: relative;
    float: left;
  }
  .install-gallery > ul > li > a >.gallery_images:hover {
    -webkit-transform: scale(1.2);
    transform: scale(1.2);
    cursor: pointer;
    transition: 0.3s;
  }


  .page_bold_list {
    font-weight: bold;
    font-size: 1.2em;
  }

  .panel.panel-default {
       margin-top: 20px;
  }

  .documentText{
    background-color: rgba(0,0,0,0.7);
    color: white;
    padding: 4px;
    bottom: 0;
    position: absolute;
    width: 100%;
    min-height: 25px;
 }

 .info_holder{
     min-height: 20px;
     padding: 15px;
     margin-bottom: 20px;
     background-color: #f5f5f5;
     border: 1px solid #e3e3e3;
     border-radius: 4px;
     -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
     box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
 }
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span><span class="menuText">Staff Database</span></div>



								<div class="clearfix"></div>

                                <div class="panel panel-default userlist">
                                        <div class="panel-body">



                                        </div>
                                </div>




            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>


</body>
</html>

<!-- PAGE JS -->



<script>
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
