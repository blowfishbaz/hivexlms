<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

$id = decrypt($_SESSION['SESS_ACCOUNT_ID']);

function get_my_account_type($id){
    $db = new database;
    $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
    $db->bind(1,$id);
    $me = $db->single();

    return $me['account_type'];
}

function get_my_company($id){
  $db = new database;
  $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
  $db->bind(1,$id);
  $me = $db->single();

  return $me['company_id'];
}

function get_my_provision($id){
  $db = new database;
  $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
  $db->bind(1,$id);
  $me = $db->single();

  return $me['main_provision'];
}


$my_provision = get_my_provision($id);
$my_company = get_my_company($id);

$db->Query("select title from ws_roles where status = 1 order by title asc");
$db->bind(1,$id);
$roles = $db->ResultSet();




?>

<h3>Create Shift</h3>

<form id="bid_shift_form">
  <input type="hidden" name="prov_id" value="<?echo $my_provision;?>" />
  <input type="hidden" name="company_id" value="<?echo $my_company;?>" />

  <div class="form-group label-floating is-empty">
    <label for="title" class="control-label">Title</label>
    <input type="text" class="form-control" id="title" name="title">
    <span class="material-input"></span>
  </div><br>

  <div class="form-group label-floating is-empty">
    <label for="start_date" class="control-label">Start Date</label>
    <input type="date" class="form-control" id="start_date" name="start_date">
    <span class="material-input"></span>
  </div><br>

  <div class="form-group label-floating is-empty">
    <label for="start_time" class="control-label">Start Time</label>
    <input type="time" class="form-control" id="start_time" name="start_time">
    <span class="material-input"></span>
  </div><br>

  <div class="form-group label-floating is-empty">
    <label for="end_date" class="control-label">End Date</label>
    <input type="date" class="form-control" id="end_date" name="end_date">
    <span class="material-input"></span>
  </div><br>

  <div class="form-group label-floating is-empty">
    <label for="end_time" class="control-label">End Time</label>
    <input type="time" class="form-control" id="end_time" name="end_time">
    <span class="material-input"></span>
  </div><br>


    <div class="form-group is-empty" style="margin-top:1px;">
      <label for="role">Role</label>
      <select class="form-control" id="role" name="role">
            <option value="" disabled selected>Select</option>
            <?foreach ($roles as $r) {?>
              <option value="<?echo $r['id'];?>"><?echo $r['title'];?></option>
            <?}?>


      </select>
    <span class="material-input"></span>
  </div><br />

  <div class="form-group label-floating is-empty">
    <label for="salery_per_hour" class="control-label">Salary (Per Hour)</label>
    <input type="text" class="form-control" id="salery_per_hour" name="salery_per_hour">
    <span class="material-input"></span>
  </div><br>








</form>

<button type="button" class="btn btn-sm btn-raised btn-danger" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raied btn-success pull-right" id="save_create_shift_bid">Save</button>
