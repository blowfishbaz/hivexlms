<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

$db->query("SELECT * from hx_rota where id = ?");
$db->bind(1,$_GET['id']);
$shift = $db->single();


$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,$shift['member_id']);
$staff = $db->single();

$db->query("SELECT * FROM hx_homes WHERE id = ?");
$db->bind(1,$shift['home_id']);
$home = $db->single();

$db->query("SELECT accounts.id,accounts.name,accounts.work_mobile FROM accounts
            LEFT JOIN hx_home_users
            ON hx_home_users.user_id = accounts.id
            WHERE hx_home_users.home_id = ? order by accounts.name ASC");
$db->bind(1,$shift['home_id']);
$peeps = $db->resultSet();

$db->query("SELECT * FROM hx_rota_cover WHERE pid = ?");
$db->bind(1,$shift['id']);
$cover = $db->resultSet();

$users_id= array();
$decision= array();
foreach($cover as $c){
    array_push($users_id,$c['member_id']);
    array_push($decision,$c['decision']);
}

$date1 = '2020-06-01';
$date2 = date('Y-m-d');

$ts1 = strtotime($date1);
$ts2 = strtotime($date2);

$year1 = date('Y', $ts1);
$year2 = date('Y', $ts2);

$month1 = date('m', $ts1);
$month2 = date('m', $ts2);

$diff = (($year2 - $year1) * 12) + ($month2 - $month1);


$max_loop = 5;
$month_diff = $diff;

function move($array,$max = 10,$months = 0, $current = 0){
     if($current > $max){
        exit();
     }
    if(empty($array)){
      exit();
    }else{
      if($months != 0 && $months > $current && $max > $current){
          $first = $array[0];
          $new_array = array();
          unset($array[0]);

          foreach($array as $a){
              array_push($new_array, $a);
          }
          array_push($new_array, $first);
          $current = $current + 1;
          return move($new_array, $max, $months, $current);
      }else{
          return $array;
      }
      exit();
    }
}

$new_peeps = move($peeps, $max_loop, $month_diff);

?>

<h3><?echo $home['name'].'<br />'.$staff['name'].'<br />'.$shift['start_date'].'<br />'.$shift['start_time'].'-'.$shift['end_time'];?></h3>
<div>
  <table>
       <tbody>
         <tr><th>Name</th><th>Number</th><th>Decision</th></tr>
         <?foreach ($new_peeps as $key => $p) {

           if($staff['id']!=$p['id']){
           	if (in_array($p['id'], $users_id)) {

               $num=array_search($p['id'],$users_id);
               $buttons='<p>Decision set as '.$decision[$num].'</p>';
            }else{

           $buttons='<button type="button" data-id="'.$p['id'].'" class="btn btn-xs btn-danger btn-raised cover_no">No</button>
           <button type="button" style="margin-left: 10px;" data-id="'.$p['id'].'" class="btn btn-xs btn-info btn-raised cover_yes">Yes</button>';
         }
           echo '<tr><td>'.$p['name'].'</td><td>'.$p['work_mobile'].'</td><td>'.$buttons.'</td></tr>';
         }
       }?>
     </tbody>
   </table>

</div>


<button type="button" class="btn btn-raised btn-warning" data-dismiss="modal">Close</button>
<div class="clearfix"></div>
