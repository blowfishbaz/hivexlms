<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

$dateNow=time();

$db->query("SELECT * from hx_rota where id = ?");
$db->bind(1,$_GET['id']);
$shift = $db->single();

$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,$_GET['user']);
$staff = $db->single();

?>
<form id="cover_staff_rota_form" name="cover_staff_rota_form" method="post" enctype="application/x-www-form-urlencoded">
<div class="col-lg-6">
  <div class="form-group" style="margin:0;">
  <label class="form-control">From</label>
  <input type="time" name="time_from" value="<?echo $shift['start_time']?>" style="padding: 10px;width:95%; margin:0px;">
</div>
</div>
<div class="col-lg-6">
<div class="form-group" style="margin:0;">
  <label class="form-control">To</label>
  <input type="time" name="time_to" value="<?echo $shift['end_time']?>" style="padding: 10px;width:95%; margin:0px;">
</div>
</div>
<div class="col-lg-12 pager" style="margin:0;">
  <input type="hidden" name="shift" value="<?echo $_GET['id'];?>"/>
  <input type="hidden" name="user" value="<?echo $_GET['user'];?>"/>
  <button type="button" class="btn btn-raised btn-success" id="save_cover_staff_rota_form">Save</button>

</div>
</form>
