<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

$dateNow=time();

$db->query("SELECT * from hx_rota where id = ?");
$db->bind(1,$_POST['shift']);
$shift = $db->single();


$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,$_POST['user']);
$staff = $db->single();

$new_id = createid('rcov');

$db->query("insert into hx_rota_cover (id,pid,member_id,decision,covered,created_date,created_by) values (?,?,?,?,?,?,?)");
$db->bind(1,$new_id);
$db->bind(2,$shift['id']);
$db->bind(3,$staff['id']);
$db->bind(4,'yes');
$db->bind(5,$staff['covered']);
$db->bind(6,$dateNow);
$db->bind(7,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->execute();

$db->query("update accounts set covered = ? where id = ?");
$db->bind(1,'0');
$db->bind(2,$staff['id']);
$db->execute();


$db->query("update hx_rota set status = ? where id = ?");
$db->bind(1,'0');
$db->bind(2,$_POST['shift']);
$db->execute();

$thisId = createid('shift');

$StartDate = $shift['start_date'];
$StartTime = $_POST['time_from'];

$thisEndDate = $shift['end_date'];
$EndTime = $_POST['time_to'];

$Start_ts = date('U', strtotime($StartDate.' '.$StartTime));
$End_ts = date('U', strtotime($EndDate.' '.$EndTime));

$db->query("insert into hx_rota (save_id,id,member_id,home_id,start_date,start_time,end_date,end_time,start_ts,end_ts,week_num,year,type) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1, $shift['save_id']);
$db->bind(2, $thisId);
$db->bind(3, $_POST['user']);
$db->bind(4, $shift['home_id']);
$db->bind(5, $shift['start_date']);
$db->bind(6, $StartTime);
$db->bind(7, $shift['end_date']);
$db->bind(8, $EndTime);
$db->bind(9, $Start_ts);
$db->bind(10, $End_ts);
$db->bind(11, $shift['week_num']);
$db->bind(12, $shift['year']);
$db->bind(13, $shift['type']);
$db->execute();

echo 'ok';
?>
