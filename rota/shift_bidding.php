<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 //Page Title
 $page_title = 'Home';


	$id = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Sky Recruitment</title>


<style>

/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>



	<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#eee;">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 background" >
				<div class="page_spacer"></div>


				<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<div>

							<!-- How are we storing this information / where is it going to go? -->

							<button type="button" class="btn btn-sm btn-raised btn-info pull-right" id="add_bid_shift">Create</button>

							<div class="clearfix"></div>

							<table id="table" class="striped"
							data-toggle="table"
							data-show-export="true"
							data-export-types="['excel']"
							data-click-to-select="true"
							data-filter-control="true"
							data-show-columns="true"
							data-show-refresh="true"
							data-url="<? echo $fullurl; ?>rota/ajax/table/bidding.php"
							data-height="400"
							data-side-pagination="server"
							data-pagination="true"
							data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
							data-search="true">
											<thead>
			<tr>
							<th data-field="id" data-sortable="true" data-visible="false">ID</th>
							<th data-field="company_name" data-sortable="true" data-visible="true" data-formatter="LinkCompanyFormatter">Company Name</th>
							<th data-field="main_telephone" data-sortable="true" data-visible="true">Main Telephone</th>
							<th data-field="main_email" data-sortable="true" data-visible="true">Main Email</th>
							<th data-field="created_date" data-sortable="true" data-visible="true" data-formatter="DateTimeFormatter">Created Date</th>
			</tr>
											</thead>
							</table>



					</div>


			</div>

		</div>
		<div class="page_spacer"></div>
	</div>
	<div class="clearfix"></div>


	</div>
	<div class="clearfix"></div>
<div class="clearfix"></div>
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/footer.php'); ?>
</div>
</div>
</div><!--wrapper-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>

<script>

$( "body" ).on( "click", "#add_bid_shift", function(e) {

  $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
  $("#BaseModalL").modal('show');

	$.ajax({
		 type: "POST",
		 url: "ajax/forms/add_shift_bid.php",
		 success: function(msg){
				//alert(msg);
				$("#BaseModalLContent").delay(1000)
					 .queue(function(n) {
							$(this).html(msg);
							n();
					 }).fadeIn("slow").queue(function(n) {
							$.material.init();
							n();
				});
		 }
	});


});


$( "body" ).on( "click", "#save_create_shift_bid", function(e) {
	var form = $('#bid_shift_form').serialize();

	var HasError = 0;

	$('#bid_shift_form').find('input').each(function(){
		$(this).parent().removeClass('has-error');
		Messenger().hideAll();
		if(!$.trim(this.value).length) { // zero-length string AFTER a trim
						if(!$(this).prop('required')){
						} else {
										HasError = 1;
										$(this).parent().addClass('has-error');
						}
					}
			});
			if (HasError == 1) {
			Messenger().post({
							message: 'Please make sure all required elements of the form are filled out.',
							type: 'error',
							showCloseButton: false
			});
			} else {
				$.ajax({
									type: "POST",
									url: "ajax/save_bid_shift.php",
									data: form,
									success: function(msg){
											if(msg == 'ok' || msg == ' ok'){
													Messenger().post({
														message: 'Bid Created',
														showCloseButton: false
											});

											setTimeout(function(){
													window.location.reload();
													}, 2000);


											}else{
													Messenger().post({
														message: 'Please try again later',
														type: 'error',
														showCloseButton: false
												});
											}
									}
							});
			}
});

</script>
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_web_modals.php'); ?>
</body>

</html>
