<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where account_status = 1 order by name asc");
 $accounts = $db->resultSet();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

    <?	//Base CSS Include
    	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>

<link rel="stylesheet" href="<? echo $fullurl; ?>assets/css/jquery.fileupload.css">
<link rel="stylesheet" href="<? echo $fullurl; ?>assets/css/jquery.fileupload-ui.css">
<link href="../../../assets/css/lightgallery.css" rel="stylesheet">

<style>

.rota_holder{
  /* background-color: red; */
  margin-bottom: 85px;
}




.shift_day{
  background-color: #9dd8d0;
  width: 103px;
  font-size: 12px;
}

.shift_over{
  background-color: #eef58d;
  width: 103px;
  font-size: 12px;
}

.c_dblue{
  background-color: #4C6472;
}

.c_lblue{
  background-color: #57A4B1;
}

.c_green{
  background-color: #B0D894;
}

.c_yellow{
  background-color: #FADE89;
}

.c_red{
  background-color: #F95355;
}


.day_head{
    width: 12.5%;
    background-color: #bcdbe0;
    border: 1px solid #c7c7c7;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    text-align: center;
    padding: 7px 0px;
    border-right: none;
    font-weight:bolder;

}

.wrow {
  width: 12.5%;
  border: 1px solid #c7c7c7;
  border-top: none;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  text-align: center;
  padding: 5px 0px;
  border-right: none;
  font-weight:bolder;
  min-height: 65px;
  background-color: white;
  }


.name_tag{
  background-color: #e8fdff;
  float: left;
}

.name_tag {
  font-size: 12px;
}

.name_tag p {
  margin-top: 10px;
  margin-bottom: 10px;
}

p.info_p{
  margin-top: 2px;
  font-size: 10px;
  border-bottom: 1px solid #c7c7c7;
  width: 90%;
  margin-left: 5%;
  padding-bottom: 3px;
  margin-bottom: 4px;
}

.info_val{
  font-weight: bold;
}



.info_bar{
  background-color: #f5f6f7;
  float: left;
  border: 1px solid #c7c7c7;
  border-top: none;
  border-bottom: none;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  color:#737373;
  padding: 5px 0px;
  font-size: 12px;
  text-align: center;
}


.day_head:first-child {
  background-color: #b2c2cc;
  padding: 17px 0px;
  /* color: #fff; */
  -webkit-border-top-left-radius: 30px;
  -moz-border-radius-topleft: 30px;
  border-top-left-radius: 30px;
}

.day_head:nth-child(3) ,.day_head:nth-child(5) ,.day_head:nth-child(7) {
  background: #ddedef;
}

.day_head:last-child, .wrow:last-child {
  border-right: 1px solid #c7c7c7;
}


.s_date{
  font-size: 11px;
  /* font-weight: bold; */
  padding: 3px 0px 0px 0px;
  border-top: 1px solid #B1B8C3;
}



.c_panel,.c_panel_top{
  width:87.5%;
  background-color: white;
  margin-left: 12.5%;
  -webkit-border-top-left-radius: 30px;
  -moz-border-radius-topleft: 30px;
  border-top-left-radius: 30px;
  height: 50px;
  min-height: 50px;
  line-height: 50px;
  border: 1px solid #c7c7c7;
  border-bottom: none;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

.c_panel_top{
  margin-left: 74%;
  width: 26%;
  height: 30px;
  min-height: 30px;
  line-height: 30px;
  -webkit-border-top-left-radius: 20px;
  -moz-border-radius-topleft: 20px;
  border-top-left-radius: 20px;
  background-color: #e8fdff;
  text-align: center !important;
}

@media (min-width: 768px){
          .day_head,.wrow {
              width: 12.5%;
          }
          .c_panel_top{
            margin-left: 60%;
            width: 40%;
          }

}

@media (min-width: 992px) {
      .day_head ,.wrow {
        width: 12.5%;
      }
      .c_panel_top{
        margin-left: 60%;
        width: 40%;
      }

}

@media (min-width: 1282px) {
      .day_head ,.wrow {
            width: 12.5%;
      }
      .c_panel_top{
        margin-left: 70%;
        width: 30%;
      }

}

/* day in */
.in_day,.span_day,.other_day,.other_span_day{
  background-color: #00a4b2;
  width: 92%;
  margin-left: 4%;
  font-size: 12px;
  font-weight: bold;
  margin-bottom: 4px;
  color: #fbfbfb;
  padding: 2px 0px;
  -webkit-border-radius: 12px;
  -moz-border-radius: 12px;
  border-radius: 12px;
  float:left;
}

/* other day in */
.other_day{
  background-color: #797979;
  color: #fbfbfb;
}

/* span day */
.span_day,.other_span_day{
  background-color: #4c6fb8;
  width: 92%;
  margin-left: 55%;
  position: absolute;
  z-index: 9;
  bottom: 0;
  color:#fff;
}

/* other span days */
.other_span_day{
  background-color: #797979;
  color: #fbfbfb;
}

/* add button */
.day_add{
    background: #23b9a761;
    width: 17%;
    margin: 0px;
    margin-top: -5px;
    font-size: 10px;
    text-align: left;
    margin-bottom: 7px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-border-radius:0px;
    -webkit-border-bottom-right-radius: 12px;
    -moz-border-radius: 0px;
    -moz-border-radius-bottomright: 12px;
    border-radius: 0px;
    border-bottom-right-radius: 12px;
    color: #fff;
}


/* span day next */
.wrow:nth-child(8) > .span_day, .wrow:nth-child(8) > .other_span_day{
  background-color:#079cf5;
  width: auto;
  color: #fff;
  right: 0;
  padding: 2px 4px;
  -webkit-border-radius: 0px;
  -webkit-border-top-left-radius: 12px;
  -webkit-border-bottom-left-radius: 12px;
  -moz-border-radius: 0px;
  -moz-border-radius-topright: 12px;
  -moz-border-radius-bottomright: 12px;
  border-radius: 0px;
  border-top-left-radius: 12px;
  border-bottom-left-radius: 12px;

}

/* next other span day */
.wrow:nth-child(8) > .other_span_day{
  background-color:#797979;
  color: #fbfbfb;
}

/* span day before */
.wrow:nth-child(2) > .span_day.before_day, .wrow:nth-child(2) > .other_span_day.before_day {
  background-color: #079cf5;
  width:auto;
  margin-left:0%;
  color: #fff;
  padding: 2px 4px;
  -webkit-border-radius: 0px;
  -webkit-border-top-right-radius: 12px;
  -webkit-border-bottom-right-radius: 12px;
  -moz-border-radius: 0px;
  -moz-border-radius-topright: 12px;
  -moz-border-radius-bottomright: 12px;
  border-radius: 0px;
  border-top-right-radius: 12px;
  border-bottom-right-radius: 12px;

}

.wrow:nth-child(2) > .other_span_day.before_day{
  background-color:#797979;
  color: #fbfbfb;
}

.my_hol > .day_add{
  display: none;
}

.my_hol{
      background: repeating-linear-gradient(
        -45deg,
        #e4e4e4,
        #e4e4e4 5px,
        #ececec 5px,
        #ececec 10px
      )!important;
}

.half_hol{
      color: #797979;
      background: repeating-linear-gradient(
        -45deg,
        #e4e4e4,
        #e4e4e4 5px,
        #ececec 5px,
        #ececec 10px
      )!important;
}


.hol{
    background-color: #f0eeee !important;
}

.info_val > .glyphicons:before {
  padding: 2px 2px;
  margin-left: 2px;
}


.cur_info{
  margin-left: 15px;
  float: left;
}

.w_dat{
  margin-left:4px;

}

.w_num{
  margin-left:10px;
}


.cur_info > .form-group{
  margin: 0px;
  padding: 0px;
  margin-top: 3px;
  margin-left: 10px;
}

.cur_info_opts{
  float: right;
  margin-right: 10px;
}

.cur_info_opts > .btn{
  -moz-box-shadow: none !important;
  -webkit-box-shadow: none !important;
  box-shadow: none !important;
  background-color: transparent !important;
  border: 1px solid #797979;
  color: #797979 !important;
  padding: 3px 12px;
  margin-left: 15px;
}

.cur_info_opts > .btn:hover{
  background-color: #daffda !important;
  /* color: #daffda !important; */
  /* border: 1px solid #daffda; */

}

.info_p:last-child {
  border: none;
}

.main_add{
  background-color: #1eb9a7;
  color: #fff;
}

.main_add_past{
  background-color: #cccccc;
  color: #fff;
}



.rota_num{
  font-weight: bold;
    text-align: center;
    padding: 7px;
    border-bottom: 1px solid #c7c7c7;
}

.p_hol{
    width: 94%;
    font-size: 8px;
    margin-left:3%;
    float: left;
    text-align: right;
    /* margin-bottom: 7px; */

}

.today_box{
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  background-color: #f3fafd !important ;
  }


  .my_hol.today_box {
      background: repeating-linear-gradient( -45deg, #f3fafd, #f3fafd 5px, #ececec 5px, #ececec 10px )!important;
  }


.nowait{
    padding: 9px;
    font-size: 14px;
    margin: 0px;
    margin-left: 15px;
    margin-bottom: 5px;
    color: #fff !important;
}

.btn-success.nowait{
    background-color: #4caf50;
    color: rgba(255,255,255, 0.84);
}

.small_date{
  font-size: 12px;
  width: 100%;
  float: left;
  margin-bottom: -16px;
  font-weight: bold;
  display: none;
}

.small_date_td{
  width: 30%;
    float: left;
    font-size: 12px;
    margin-left: 2px;
}

.small_date_nd{
  float: left;
  width: 33%;
  font-size: 12px;
}

.input_error{
  border-bottom: 1px solid red;
}

.clash_error{
    width:100%;
    font-size:10px;
    color:red;
    text-align:center;
    margin-bottom: -14px;
    margin-top: 2px;
  }

.hidden_form_line{
  margin-bottom: 10px;
  padding: 5px;
  background-color: gold;
}

.cover_this {
    cursor: pointer;
}

table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    table-layout: fixed;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
</style>

</head>
<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?>

                      <!--- PAGE CONTENT HERE ---->
                      <div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-calendar" aria-hidden="true"></span><span class="menuText">Shift Cover</span>

                        <?
                        $db->query("SELECT hx_homes.*,hx_home_users.type FROM hx_homes
                                    LEFT JOIN hx_home_users
                                    ON hx_homes.id = hx_home_users.home_id
                                    WHERE hx_home_users.user_id = ? order by hx_homes.name ASC");
                                    $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
                                    $homes = $db->resultSet();

                        ?>
                        <select class="form-control chosen-select" id="rota_home" name="rota_home" required="yes">
                          <option value="" disabled="" selected="">Select Home...</option>
                            <? foreach ($homes as $key => $value) { ?>
                            <option value="<? echo $value['id']; ?>"><? echo $value['name']; ?></option>
                            <? } ?>
                        </select>
                      </div>

                      <div class="clearfix"></div>
                      <br />

<!-- main rota div -->
<div class="main_rota_div" style="display:none;">
<!-- main rota div -->


<!-- main rota div -->
</div>
<!-- main rota div -->

<div class="rota_week_form_holder">
</div>

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>


</body>
</html>

<!-- PAGE JS -->

<script src="<? echo $fullurl ?>assets/app_scripts/crm/customer/index.js"></script>
<script src="../../../assets/js/picturefill.min.js"></script>
<script src="../../../assets/js/lightgallery.js"></script>
<script src="../../../assets/js/lg-fullscreen.js"></script>


<script>

var $cover='';

function isTimeOk($oldStart,$oldEnd,$newStart,$newEnd){
					if(($newStart<=$oldStart&&$newEnd<=$oldStart) || ($newStart>=$oldEnd&&$newEnd>=$oldEnd)){
 											return true;
          }
          else{
                return false;
          }
}

$clash = '<p class="clash_error">Time clashing</p>';


// $( "body" ).on( "click", ".test_but", function(e) {

function checkAllInputs(){

            $noTimeClashError = true;

            $('.clash_error').remove();
            $('.input_error').removeClass('input_error');
            //----------------------------------------------------grab each day
            $("#add_rota_week_form .day_tr").each(function(){
            //----------------------------------------------------grab each day

                              //----grab each input
                              $newS = 0;
                              $newE = 0;
                              $thisLoopCount = 1;
                              $slotCount = 1;
                              $slotArray = [];
                              $(this).find(':input[type=time]').each(function(){
                              //----grab each input
                                              $v = $(this);
                                              if($thisLoopCount==3){ $thisLoopCount = 1; }
                                              if($thisLoopCount==1){
                                                $newS = $v.val();
                                              }
                                              else{
                                                $newE = $v.val();
                                                // ----------- both inputs here-----------
                                                // ----------- both inputs here-----------
                                                // ----------- both inputs here-----------

                                                if($slotCount>1){
                                                        $.each($slotArray, function( index, value ){

                                                              $res = isTimeOk(value['stime'],value['etime'],$newS,$newE);

                                                              $overSapn = $v.parent().children('.span_v').val();

                                                                    if($res ==false && $overSapn ==0){





                                                                        $v.parent().prevAll('td').first().children('.start_input').addClass('input_error');
                                                                        $v.addClass('input_error');
                                                                        $v.parent().prevAll('td').first().append($clash+'XXX');
                                                                        $v.parent().append($clash);
                                                                        $noTimeClashError = false;





                                                                    }else{

                                                                    }
                                                        });
                                                      ///////////
                                                      console.dir($slotArray);
                                                      ///////////
                                                }
                                                //--store all slots
                                                $slotArray.push({stime:$newS, etime:$newE})
                                                //--store all slots
                                                // ----------- both inputs here-----------
                                                // ----------- both inputs here-----------
                                                // ----------- both inputs here-----------
                                                $slotCount++;
                                              }
                              //----grab each input
                              $thisLoopCount++;
                              });
                              //----grab each input
            //----------------------------------------------------grab each day
            });
            //----------------------------------------------------grab each day

            return $noTimeClashError;

};





function DateFmt() {
      this.dateMarkers = {
	     d:['getDate',function(v) { return ("0"+v).substr(-2,2)}],
             m:['getMonth',function(v) { return ("0"+v).substr(-2,2)}],
             n:['getMonth',function(v) {
                 var mthNames = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                 return mthNames[v];
	             }],
             w:['getDay',function(v) {
                 var dayNames = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
                 return dayNames[v];
	             }],
             y:['getFullYear'],
             H:['getHours',function(v) { return ("0"+v).substr(-2,2)}],
             M:['getMinutes',function(v) { return ("0"+v).substr(-2,2)}],
             S:['getSeconds',function(v) { return ("0"+v).substr(-2,2)}],
             i:['toISOString',null]
      };

      this.format = function(date, fmt) {
        var dateMarkers = this.dateMarkers
        var dateTxt = fmt.replace(/%(.)/g, function(m, p){
        var rv = date[(dateMarkers[p])[0]]()

        if ( dateMarkers[p][1] != null ) rv = dateMarkers[p][1](rv)

        return rv
      });

      return dateTxt
      }
    }


// table //
// table //

function in_rota_table(){

            delay = 0;
            delay2 = 0;
            $('.day_worker').each(function() {
                var self = this;
                /// 1st delay
                setTimeout( function(){
                    var $biggest_height = 0;
                    var $cur_height = 0;
                    $(self).children('.wrow').each(function () {
                          var cself = this;
                          // 2nd delay
                          setTimeout( function(){
                          $cur_height = $(cself).height();
                          if($cur_height>$biggest_height){
                          $biggest_height = $cur_height;
                          }
                          // $(cself).css('background','green');
                          // 2nd delay
                          }, delay2+=0);
                    });
                    $(self).children('.wrow').each(function () {
                        var cself = this;
                        var $s = $(cself).parent().children().children('.span_day').length;
                        if($s==0){
                        var $s = $(cself).parent().children().children('.other_span_day').length;
                        }
                        if($s > 0){
                        var $h = 22;
                        }else{
                        var $h = 0;
                        }
                        // 3nd delay
                        setTimeout( function(){
                        $(cself).height($biggest_height+$h);
                        //$(cself).css('background','white');
                        // 3nd delay
                        }, delay2+=0);
                    });
                    var $s = 0;
                    /// 1st delay
                }, delay+=0);
            });

}
// table //
// table //


function getDateOfWeek(w, y) {
      var simple = new Date(y, 0, 1 + (w - 1) * 7);
      var dow = simple.getDay();
      var ISOweekStart = simple;
      if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
      else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
      return ISOweekStart;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}






//main drop
$( "body" ).on( "change", "#rota_home", function(e) {

        $('.rota_week_form_holder').html('');

        $('.main_rota_div').fadeIn();

        var f_date = "<? echo date('Y-m-d'); ?>";


        $('.main_rota_div').html($Loading);

        $home_id = $(this).val();


        var url = "ajax/get_full_cover_table.php?id="+$home_id+'&date='+f_date; // PHP save page

        $.ajax({
               type: "POST",
               url: url,
               success: function(data){
                    setTimeout(function(){
                          $('.main_rota_div').html(data);
                          in_rota_table();
                    }, 600);
                }
             });



});

$( "body" ).on( "change", "#week", function(e) {


          $('.main_rota_div').html($Loading);

          var $drop_date = $(this).val();
          var $drop_week = $drop_date.substring(6, 8);
          var $drop_year = $drop_date.substring(0, 4);

          $nd = getDateOfWeek($drop_week,$drop_year);

          $nd = new Date($nd);
          $nd = formatDate($nd, "d-m-y");

          $drop_home_id = $('#rota_home').val();

          var url = "ajax/get_full_cover_table.php?id="+$drop_home_id+'&date='+$nd; // PHP save page

          $.ajax({
                 type: "POST",
                 url: url,
                 success: function(data){
                      setTimeout(function(){
                            $('.main_rota_div').html(data);
                            in_rota_table();
                      }, 600);
                  }
               });

});




$( "body" ).on( "click", ".add_home", function(e) {


          $('.main_rota_div').html($Loading);

          $nd = $(this).data('date');

          $drop_home_id = $('#rota_home').val();

          var url = "ajax/get_full_cover_table.php?id="+$home_id+'&date='+$nd; // PHP save page

          $.ajax({
                 type: "POST",
                 url: url,
                 success: function(data){
                      setTimeout(function(){
                            $('.main_rota_div').html(data);
                            in_rota_table();
                            $('.rota_week_form_holder').html('');
                      }, 600);
                  }
               });




});



    $( "body" ).on( "click", ".main_add", function(e) {


                var $staffId = $(this).data('staff');
                var $staffName = $(this).data('staff_name');
                var $bdate = $(this).data('bdate');
                var $edate = $(this).data('edate');
                var $ehome = $(this).data('home');
                var $eweek= $(this).data('week');
                var $b_type = $(this).data('but');


                if ($("."+$staffId+"").length == 0){
                              $('#BaseModalM').modal({backdrop: 'static', keyboard: false});

                              if($b_type=='new'){
                                    $r_url = "ajax/add_hours_staff.php?id="+$staffId+"&week_s="+$bdate+"&week_e="+$edate+"&home="+$ehome+"&week="+$eweek;
                              }
                              else{
                                    $r_url = "ajax/edit_hours_staff.php?id="+$staffId+"&week_s="+$bdate+"&week_e="+$edate+"&home="+$ehome+"&week="+$eweek;
                              }


                              $.ajax({
                                    type: "POST",
                                    url: $r_url,
                                    data: "id:1",
                                    success: function(msg){
                                          //alert(msg);
                                          $("#BaseModalMContent").delay(1000)
                                          .queue(function(n) {
                                                $(this).html(msg);
                                                n();
                                          }).fadeIn("slow").queue(function(n) {
                                                $.material.init();
                                                n();
                                          });
                                    }
                                });
                }
                else{

                          var $time_array_values = $('.'+$staffId+'_values').text();
                          var $part_add_rota_week_form = $("."+$staffId+"").html();
                          var $time_array = $time_array_values.split(",");

                          $('#BaseModalM').modal({backdrop: 'static', keyboard: false});

                          $(".day_rota_add_form").delay(1000)
                          .queue(function(n) {
                                $(this).html($part_add_rota_week_form);
                                n();
                          }).fadeIn("slow").queue(function(n) {
                                $.material.init();
                                n();
                                $("#add_rota_week_form :input[type=time]").each(function(i, obj) {
                                          var input = $(obj).val();
                                          $(this).val($time_array[i]);
                                });
                          });




                }

    });

$( "body" ).on( "click", ".cover_this", function(e) {

  $('#BaseModalLContent').html($Loading);
  $('#BaseModalL').modal({backdrop: 'static', keyboard: false});

  $cover=$(this).attr('data-id');
  $.ajax({
        type: "POST",
        url: "ajax/cover_staff_rota_form.php?id="+$cover,
        data: "id:1",
        success: function(msg){
              //alert(msg);
              $("#BaseModalLContent").delay(1000)
              .queue(function(n) {
                    $(this).html(msg);
                    n();
              }).fadeIn("slow").queue(function(n) {
                    $.material.init();
                    n();
              });
        }
    });
});

$( "body" ).on( "click", ".cover_no", function(e) {
  $user=$(this).attr('data-id');
  $this=$(this);
  $(this).attr("disabled", true);

  $.ajax({
        type: "POST",
        url: "ajax/cover_staff_rota_decision_no.php?id="+$cover+"&user="+$user,
        data: "id:1",
        success: function(msg){
              $($this).parent().html('<p>Decision set as no</p>');
        }
    });
});

$( "body" ).on( "click", ".cover_yes", function(e) {
  $user=$(this).attr('data-id');
  $this=$(this);
  $(this).attr("disabled", true);

  //$(this).parent().html('<p>Decision Set as Yes</p>');
  $.ajax({
        type: "POST",
      url: "ajax/cover_staff_rota_decision_yes_form.php?id="+$cover+"&user="+$user,
        data: "id:1",
        success: function(msg){
              //alert(msg);
              $($this).parent().delay(600)
              .queue(function(n) {
                    $(this).html(msg);
                    n();
              }).fadeIn("slow").queue(function(n) {
                    $.material.init();
                    n();
              });
        }
    });
});

$( "body" ).on( "click", "#save_cover_staff_rota_form", function(e) {
  $user=$(this).attr('data-id');
  $this=$(this);
  $(this).attr("disabled", true);

  //$(this).parent().html('<p>Decision Set as Yes</p>');
  $.ajax({
        type: "POST",
      url: "ajax/cover_staff_rota_decision_yes_form.php?id="+$cover+"&user="+$user,
        data: "id:1",
        success: function(msg){
              //alert(msg);
              $($this).parent().delay(600)
              .queue(function(n) {
                    $(this).html(msg);
                    n();
              }).fadeIn("slow").queue(function(n) {
                    $.material.init();
                    n();
              });
        }
    });
});

$( "body" ).on( "click", "#save_cover_staff_rota_form", function() {
   var form = $('#cover_staff_rota_form').serialize();
   var HasError = 0;

$('#cover_staff_rota_form').find('input').each(function(){
  $(this).parent().removeClass('has-error');
  Messenger().hideAll();
  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
          if(!$(this).prop('required')){
          } else {
                  HasError = 1;
                  $(this).parent().addClass('has-error');
          }
        }
   });

   if (HasError == 1) {
   Messenger().post({
           message: 'Please make sure all required elements of the form are filled out.',
           type: 'error',
           showCloseButton: false
   });
   } else {
    $.ajax({
              type: "POST",
              url: "ajax/cover_staff_rota_decision_yes.php",
              data: form,
              success: function(msg){
                   if(msg == 'ok' || msg == ' ok'){
                       Messenger().post({
                        message: 'Approval Yes Decision',
                        showCloseButton: false
                   });

                   setTimeout(function(){
                       location.reload();
                     }, 1000);

                   }else{
                       Messenger().post({
                        message: 'Please try again later',
                        type: 'error',
                        showCloseButton: false
                    });
                   }
              }
           });
   }
});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
