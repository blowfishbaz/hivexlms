
INSERT INTO `accounts` (`id`, `username`, `password`, `name`, `email`, `type`, `status`, `attempts`, `lastattempt`, `created_date`, `profilepic`, `account_type`, `user_status`, `service_area`)
VALUES
	('id_member_20210322_094920342600_72454', '68941617063f0f05fd5f374af7c415ac165c967a98377e02', '8617f5391c19ee9f2bac15f3e366ec93', 'Paul Brown', 'paulbrown2@nhs.net', '', 1, 0, 0, 1616406560, '', 'user', 1, NULL),
	('id_member_20210322_094920385200_61025', '607c44c9915aa7fe7755aa2749dbe9b5046e4e7de5e70c1cadb8d4967e40c559', '8617f5391c19ee9f2bac15f3e366ec93', 'Peter McLaren', 'petemclaren@doctors.org.uk', '', 1, 0, 0, 1616406560, '', 'user', 1, NULL);


  INSERT INTO `ws_accounts_extra_info` (`id`, `account_id`, `company_id`, `main_provision`, `created_date`, `created_by`, `modified_date`, `modified_by`, `mobile_number`, `telephone`, `job_title`, `status`, `account_type`, `credit`, `old_id`, `address1`, `address2`, `address3`, `city`, `postcode`, `smart_card`, `smart_card_name`, `dob`, `nino`, `nhs_pension_ref`, `gmc`, `sex`, `nhs_pension_contributor`, `nhs_pension_rate`, `nhs_pension_added_years`, `nhsavc`, `nhsap`, `nsherrbo`, `car1`, `car2`, `car3`, `nok1`, `nok1_phone`, `nok2`, `nok2_phone`)
  VALUES
  	('id_ainfo_20210322_094920342600_60954', 'id_member_20210322_094920342600_72454', NULL, NULL, 1616406560, 'id_member_20210322_094920342600_72454', NULL, NULL, 2147483647, 1513428240, NULL, 1, 'Learner', 0, '20593', '29 Oaklands Drive', 'Heswall', 'NULL', 'WIRRAL', 'CH61 6UU', NULL, NULL, '28875', 'JL768674C', NULL, '6054700', 'M', 'Y', '0', '0', '0', '0', '0', NULL, NULL, NULL, 'Kate Brown', '7779644715', NULL, NULL),
  	('id_ainfo_20210322_094920385300_43025', 'id_member_20210322_094920385200_61025', NULL, NULL, 1616406560, 'id_member_20210322_094920385200_61025', NULL, NULL, 2147483647, 1513395733, NULL, 1, 'Learner', 0, '20605', '18 Glade Drive', 'Little Sutton', NULL, 'ELLESEMERE PORT.', 'CH66 4JE', '610872041039', 'Peter McLaren', '28545', 'JL878450C', NULL, '6028715', 'M', 'N', '0', '0', '0', '0', '0', 'Volvo XC 90 NA17 RLU', NULL, NULL, 'Zoe McLaren', '7989500461', NULL, NULL);

    INSERT INTO `ws_accounts_locations` (`id`, `location_id`, `account_id`, `status`)
    VALUES
    	('id_a_loc_20210326_093446819600_91583', 'Wirral', 'id_member_20210322_094920342600_72454', 1),
    	('id_a_loc_20210326_093446851900_24158', 'Wirral', 'id_member_20210322_094920385200_61025', 1);


          INSERT INTO `ws_accounts_roles` (`id`, `role_id`, `account_id`, `status`)
          VALUES
          	('id_a_role_20210426_115192928700_56663', '10', 'id_member_20210322_094920342600_72454', 1),
            ('id_a_role_20210426_11511022928700_56663', '10', 'id_member_20210322_094920385200_61025', 1),


            INSERT INTO `ws_accounts_upload` (`id`, `account_id`, `type`, `name`, `path`, `status`, `created_date`, `created_by`)
            VALUES
            	('id_img_20210322_100547016500_21759', 'id_member_20210322_094920385200_61025', 'Manual', '100005.jpg', 'uploads/users/20605/100005.jpg', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547619800_90563', 'id_member_20210322_094920385200_61025', 'Manual', '100273.pdf', 'uploads/users/20605/100273.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547626000_37506', 'id_member_20210322_094920385200_61025', 'Manual', '100274.pdf', 'uploads/users/20605/100274.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547631700_53565', 'id_member_20210322_094920385200_61025', 'Manual', '100275.pdf', 'uploads/users/20605/100275.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547636300_85046', 'id_member_20210322_094920385200_61025', 'Manual', '100276.jpg', 'uploads/users/20605/100276.jpg', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547642500_88726', 'id_member_20210322_094920385200_61025', 'Manual', '100277.jpg', 'uploads/users/20605/100277.jpg', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547646700_48092', 'id_member_20210322_094920385200_61025', 'Manual', '100278.jpg', 'uploads/users/20605/100278.jpg', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547651200_26430', 'id_member_20210322_094920385200_61025', 'Manual', '100279.pdf', 'uploads/users/20605/100279.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547655700_94881', 'id_member_20210322_094920385200_61025', 'Manual', '100280.pdf', 'uploads/users/20605/100280.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547660000_95826', 'id_member_20210322_094920385200_61025', 'Manual', '100281.pdf', 'uploads/users/20605/100281.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547664300_84103', 'id_member_20210322_094920385200_61025', 'Manual', '100283.doc', 'uploads/users/20605/100283.doc', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547668700_18896', 'id_member_20210322_094920385200_61025', 'Manual', '100284.jpg', 'uploads/users/20605/100284.jpg', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547673300_92761', 'id_member_20210322_094920342600_72454', 'Manual', '100285.pdf', 'uploads/users/20593/100285.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547678400_84210', 'id_member_20210322_094920342600_72454', 'Manual', '100286.pdf', 'uploads/users/20593/100286.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547684000_31871', 'id_member_20210322_094920342600_72454', 'Manual', '100287.pdf', 'uploads/users/20593/100287.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547690400_77863', 'id_member_20210322_094920342600_72454', 'Manual', '100288.pdf', 'uploads/users/20593/100288.pdf', 1, 1616407547, 'id_member_20161103_110736904700_82639'),
            	('id_img_20210322_100547695900_32474', 'id_member_20210322_094920342600_72454', 'Manual', '100289.doc', 'uploads/users/20593/100289.doc', 1, 1616407547, 'id_member_20161103_110736904700_82639');

              INSERT INTO `ws_accounts` (`id`, `created_date`, `first_name`, `surname`, `email`, `username`, `password`, `attempts`, `lastattempt`, `profilepic`, `status`, `type`, `pid`, `lxp`, `rota`, `hr`)
VALUES
	('id_member_20210322_094920342600_72454', 1616406560, 'Paul', 'Brown', 'paulbrown2@nhs.net', '68941617063f0f05fd5f374af7c415ac165c967a98377e02', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_094920342600_72454', 1, 1, 1),
	('id_member_20210322_094920385200_61025', 1616406560, 'Peter', 'McLaren', 'petemclaren@doctors.org.uk', '607c44c9915aa7fe7755aa2749dbe9b5046e4e7de5e70c1cadb8d4967e40c559', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_094920385200_61025', 1, 1, 1);



'id_member_20210322_094920342600_72454','id_member_20210322_094920385200_61025'
