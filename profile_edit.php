<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/

ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 //Page Title
 $page_title = 'Register';

 $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 $db = new database;
  $db->query("select * from ws_accounts where id = ? ");
  $db->bind(1,$uid);
  $user = $db->single();

	$db->query("select * from ws_addresses where pid = ? and status = ? ");
  $db->bind(1,$uid);
	$db->bind(2,'1');
  $user_address = $db->single();

	$db->query("select * from ws_uploads where pid = ?  and status = ? and type = ?");
  $db->bind(1,$uid);
	$db->bind(2,'1');
	$db->bind(3,'cv');
  $user_cv = $db->single();

	$db->query("select * from ws_uploads where pid = ?  and status = ? and type = ?");
  $db->bind(1,$uid);
	$db->bind(2,'1');
	$db->bind(3,'bds');
  $user_bds = $db->single();

	$db->query("select * from ws_accounts_info where pid = ? and status = ? ");
	$db->bind(1,$uid);
	$db->bind(2,'1');
	$user_acc_info = $db->single();


	$db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
	$db->bind(1,$uid);
	$db->bind(2,'bds');
	$db->bind(3,'1');
	$bds = $db->single();

	$db->query("select * from ws_accounts_qualifications where pid = ? and status = ? ");
	$db->bind(1,$uid);
	$db->bind(2,'1');
	$qualification = $db->resultset();

	$db->query("select * from ws_accounts_employment where pid = ? and status = ? ");
	$db->bind(1,$uid);
	$db->bind(2,'1');
	$employment = $db->resultset();

	$db->query("select * from ws_accounts_references where pid = ? and status = ?");
	$db->bind(1,$uid);
	$db->bind(2,'1');
	$reference = $db->resultset();

 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Sky Recruitment</title>


<style>



/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 parallax" style=' min-height: 50vh; background-image: url("<? echo $fullurl; ?>assets/images/Sky-about1.jpg");'>
		<div class="col-lg-2 col-md-2"></div>
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="page_spacer"></div>

				<h2 class="text-center media-heading color">Edit Profile</h2>
				<h3 class="text-center color">Edit and update your profile</h3>
				<h4 class="text-center color"><br />Edit and update your profile so that recruiters see your most up to date details.</h4>


			</div>
	</div>

	<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="page_spacer"></div>


						<h2 style=" margin-top: 0;"><span class="color1">Profile Details</span></h2>

						<div class="clearfix"></div>

						<h3 style=" margin-top: 0;"><span class="color1">Profile Address/Number:</span></h3>

						<div class="clearfix"></div>

							<form id="edit_profile_form" class="edit_profile_form" enctype="application/x-www-form-urlencoded" method="post">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step1">
			          <div class="form-group label-floating is-empty">
			                      <label for="address1" class="control-label">First Address Line*</label>
														<p class="input_error_message">first line address is required</p>
			                      <input id="address1" type="text" name="address1" class="form-control" value="<? if ( strlen($user_address['address1']) != 0 ) {  echo $user_address['address1'];  } ?>" maxlength="200" required>
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="address2" class="control-label">Second Address Line</label>
														<!-- <p class="input_error_message"></p> -->
			                      <input id="address2" type="text" name="address2" class="form-control" value="<? if ( strlen($user_address['address2']) != 0 ) {  echo $user_address['address2'];  } ?>" maxlength="200">
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="address3" class="control-label">Third Address Line</label>
														<!-- <p class="input_error_message"></p> -->
			                      <input id="address3" type="text" name="address3" class="form-control" value="<? if ( strlen($user_address['address3']) != 0 ) {  echo $user_address['address3']; } ?>" maxlength="200">
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="city" class="control-label">City*</label>
														<p class="input_error_message">a city is required</p>
			                      <input id="city" type="text" name="city" class="form-control" value="<? if ( strlen($user_address['city']) != 0 ) {  echo $user_address['city'];  } ?>" maxlength="100" required>
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="county" class="control-label">County</label>
														<!-- <p class="input_error_message"></p> -->
			                      <input id="county" type="text" name="county" class="form-control" value="<? if ( strlen($user_address['county']) != 0 ) {  echo $user_address['county'];  } ?>" maxlength="100">
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="postcode" class="control-label">Postcode*</label>
														<p class="input_error_message">a postcode is required</p>
			                      <input id="postcode" type="text" name="postcode" class="form-control" value="<? if ( strlen($user_address['postcode']) != 0 ) {  echo $user_address['postcode'];  } ?>" maxlength="20" required>
			                    <span class="material-input"></span>
			          </div>

			          <div class="form-group label-floating is-empty">
			                  <label for="telephone" class="control-label">Telephone*</label>
													<p class="input_error_message">a telephone number is required</p>
			                  <input id="telephone" type="number" name="telephone" class="form-control" value="<? if ( strlen($user_address['telephone']) != 0 ) {  echo $user_address['telephone'];  } ?>" maxlength="30" required>
			                <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                  <label for="mobile" class="control-label">Mobile</label>
													<p class="input_error_message">not a vailed mobile number</p>
			                  <input id="mobile" type="number" name="mobile" class="form-control" value="<? if ( strlen($user_address['mobile']) != 0 ) {  echo $user_address['mobile'];  } ?>" maxlength="30">
			                <span class="material-input"></span>
			          </div>


									<div class="page_spacer2"></div>
										<div class="page_spacer2"></div>

										</div>
										</div>

										<h3 style=" margin-top: 0;"><span class="color1">Profile information:</span></h3>

										<div class="clearfix"></div>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step2" style="xxxdisplay:none;">



                    <div class="form-group">
											<label class="control-label">Position Looking To Apply*</label>
											<select name="positions" class="big_in form-control selectpicker" data-live-search="true" id="positions" required>
                            <option style="display:none">&nbsp;</option>

                            <option <? if ($user_acc_info['positions'] == 'Health Care Assistant' ) { ?> selected=""<? } ?> value="Health Care Assistant">Health Care Assistant</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Adults' ) { ?> selected=""<? } ?> value="Support Worker - Adults">Support Worker - Adults</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Children' ) { ?> selected=""<? } ?> value="Support Worker - Children">Support Worker - Children</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Adult Mental Health' ) { ?> selected=""<? } ?> value="Support Worker - Adult Mental Health">Support Worker - Adult Mental Health</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Children Mental Health' ) { ?> selected=""<? } ?> value="Support Worker - Children Mental Health">Support Worker - Children Mental Health</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Adult Learning Disabilities' ) { ?> selected=""<? } ?> value="Support Worker - Adult Learning Disabilities">Support Worker - Adult Learning Disabilities</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Chilren Learning Disabilities' ) { ?> selected=""<? } ?> value="Support Worker - Chilren Learning Disabilities">Support Worker - Chilren Learning Disabilities</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Adults - Autism' ) { ?> selected=""<? } ?> value="Support Worker - Adults - Autism">Support Worker - Adults - Autism</option>
                            <option <? if ($user_acc_info['positions'] == 'Support Worker - Chilren - Autism' ) { ?> selected=""<? } ?> value="Support Worker - Chilren - Autism">Support Worker - Chilren - Autism</option>
                            <option <? if ($user_acc_info['positions'] == 'Registered Nurse' ) { ?> selected=""<? } ?> value="Registered Nurse">Registered Nurse</option>
                            <option <? if ($user_acc_info['positions'] == 'Registered Nurse - RMN' ) { ?> selected=""<? } ?> value="Registered Nurse - RMN">Registered Nurse - RMN</option>
                            <option <? if ($user_acc_info['positions'] == 'Learning Disability Nurse (RNLD)' ) { ?> selected=""<? } ?> value="Learning Disability Nurse (RNLD)">Learning Disability Nurse (RNLD)</option>
                            <option <? if ($user_acc_info['positions'] == 'Peadiatric Nurse' ) { ?> selected=""<? } ?> value="Peadiatric Nurse">Peadiatric Nurse</option>
                            <option <? if ($user_acc_info['positions'] == 'Registered Manager - Adult' ) { ?> selected=""<? } ?> value="Registered Manager - Adult">Registered Manager - Adult</option>
                            <option <? if ($user_acc_info['positions'] == 'Registered Manager - Children' ) { ?> selected=""<? } ?> value="Registered Manager - Children">Registered Manager - Children</option>
                            <option <? if ($user_acc_info['positions'] == 'Deputy Manager - Adult' ) { ?> selected=""<? } ?> value="Deputy Manager - Adult">Deputy Manager - Adult</option>
                            <option <? if ($user_acc_info['positions'] == 'Deputy Manager - Children' ) { ?> selected=""<? } ?> value="Deputy Manager - Children">Deputy Manager - Children</option>
                            <option <? if ($user_acc_info['positions'] == 'Team Leader - Adult' ) { ?> selected=""<? } ?> value="Team Leader - Adult">Team Leader - Adult</option>
                            <option <? if ($user_acc_info['positions'] == 'Team Leader - Children' ) { ?> selected=""<? } ?> value="Team Leader - Children">Team Leader - Children</option>
                            <option <? if ($user_acc_info['positions'] == 'Senior Support Worker - Adult' ) { ?> selected=""<? } ?> value="Senior Support Worker - Adult">Senior Support Worker - Adult</option>
                            <option <? if ($user_acc_info['positions'] == 'Senior Support Worker - Children' ) { ?> selected=""<? } ?> value="Senior Support Worker - Children">Senior Support Worker - Children</option>
                    </select></div>


												<div class="form-group">
													<label class="control-label">Looking For Job Type*</label>
													<div class="checkbox" style="margin-top: 0;">
									          <label><input <? if ($user_acc_info['jobtype'] == 'temporary' ) { ?> checked=""<? } ?> type="checkbox" value="temporary" name="temporary" class="jobtype">Temporary</label>
									        </div>
													<div class="checkbox">
									          <label><input <? if ($user_acc_info['jobtype'] == 'permanent' ) { ?> checked=""<? } ?>type="checkbox" value="permanent" name="permanent" class="jobtype">Permanent</label>
									        </div>
												</div>
												<input type="hidden" name="info_id" value="<? echo $user_acc_info['id'];?>">
												<div class="form-group">
													<label class="control-label">Willing To Travel*</label>
													<select name="travle" class="big_in form-control selectpicker" data-live-search="false" id="travle" required>
		                            <option  style="display:none">&nbsp;</option>
																<option <? if ($user_acc_info['travle'] == '1' ) { ?> selected=""<? } ?> value="1">1 mile</option>
																<option <? if ($user_acc_info['travle'] == '3' ) { ?> selected=""<? } ?>value="3">3 miles</option>
																<option <? if ($user_acc_info['travle'] == '5' ) { ?> selected=""<? } ?>value="5">5 miles</option>
																<option <? if ($user_acc_info['travle'] == '10' ) { ?> selected=""<? } ?>value="10">10 miles</option>
																<option <? if ($user_acc_info['travle'] == '15' ) { ?> selected=""<? } ?>value="15">15 miles</option>
																<option <? if ($user_acc_info['travle'] == '20' ) { ?> selected=""<? } ?>value="20">20 miles</option>
																<option <? if ($user_acc_info['travle'] == '30' ) { ?> selected=""<? } ?>value="30">30 miles</option>
																<option <? if ($user_acc_info['travle'] == '50' ) { ?> selected=""<? } ?>value="50">50 miles</option>
																<option <? if ($user_acc_info['travle'] == 'any' ) { ?> selected=""<? } ?>value="any">Any Distance</option>
		                    </select></div>
<!--  -->
												<div class="form-group">
													<label class="control-label">Do you have a valid driving license*</label>
													<select name="license" class="big_in form-control selectpicker" data-live-search="false" id="license" required>
		                            <option selected="" style="display:none">&nbsp;</option>
																<option <? if ($user_acc_info['license'] == 'yes' ) { ?> selected=""<? } ?>value="yes">Yes</option>
																<option <? if ($user_acc_info['license'] == 'no' ) { ?> selected=""<? } ?>value="no">No</option>
		                    </select></div>

												<div class="form-group">
													<label class="control-label">Do you want to upload CV or talk to the team*</label>
													<select name="infotype" class="big_in form-control selectpicker" data-live-search="false" id="infotype" required>
		                            <option style="display:none">&nbsp;</option>
																<option <? if ($user_acc_info['infotype'] == 'cv' ) { ?> selected=""<? } ?>value="cv">Upload CV</option>
																<option <? if ($user_acc_info['infotype'] == 'talk' ) { ?> selected=""<? } ?>value="talk">Talk to the Team (A member of our team will call you on the number provided)</option>
		                    </select></div>



                        <div class="input-group cv_upload form-group" <? if ($info['$user_acc_info'] != 'cv' ) { echo 'style="display:none;"';} ?>>
														<label class="control-label">Upload CV</label>
                            <input type="text" class="input_button" id="cv_upload" placeholder="<? if ($user_acc_info['infotype'] == 'cv' ) {echo $user_cv['name']; } ?>" name="cv_upload" readonly>
                            <input type="hidden" name="cv_upload_id" id="cv_upload_id" value = "<?echo $user_cv['id'];?>"/>
                            <span class="input-group-btn">
                                    <button style="margin-top: 25px;" class="btn btn-default input_button btn-primary" id="cv_upload_but" type="button">Upload</button>
                            </span>
                        </div><!-- /input-group -->
<!--  -->
											  <div class="row">
												 <div class="row visible-md visible-lg">
													 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															 	<label class="control-label">Qualifications</label>
														 	</div>
														 	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
															 	<label class="control-label">Grade</label>
														 	</div>
														 	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
															 	<label class="control-label">Date</label>
														 </div>
												 </div>
											 </div>


											 <? foreach($qualification as $key => $qual) { ?>


												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
																<label class="control-label visible-sm visible-xs">Qualifications</label>
																<input type="text" class="form-control" name="qualifications[]" value="<?  echo $qual['qualifications'];  ?>" ><span class="material-input"></span>
																<input type="hidden" name="qual_id[]" value="<? echo $qual['id'];?>">
															</div>
															<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
																<label class="control-label visible-sm visible-xs">Grade</label>
																<input type="text" class="form-control" name="grade[]" value="<?  echo $qual['grade'];  ?>" ><span class="material-input"></span>
															</div>
															<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
																<label class="control-label visible-sm visible-xs">Date</label>


																<input type="date" class="form-control" name="date[]" value="<? echo date ('Y-m-d',$qual['qdate']); ?>" ><span class="material-input"></span>
															</div>
															<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
																<button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible-sm visible-xs">
																<div class="clearfix"></div>
																<hr class="btn-default" />
															</div>
													</div>
												</div>

												<?  } ?>




												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<button type="button" class="btn btn-raised btn-success pull-right add_qualification">Add Row<div class="ripple-container"></div></button>
												</div>
											</div>

											<label class="control-label">Upload DBS check</label>
											<div class="input-group dbs_upload form-group">
													<input type="text" class="input_button" id="dbs_upload" placeholder="<?echo $user_bds['name'];?>" name="dbs_upload" readonly>
													<input type="hidden" name="dbs_upload_id" id="dbs_upload_id" value="<? echo $user_bds['id'];?>"/>
													<span class="input-group-btn">
																	<button class="btn btn-default input_button btn-primary" id="dbs_upload_but" type="button">Upload</button>
													</span>
											</div>



											<div class="form-group">
												<label class="control-label">Do you have any convictions, cautions, reprimands or final warnings which are not protected as defined by the Rehabilitation of Offenders Act 1974 (Exceptions) Order 1975 (as amended in 2013)?</label>
												<select name="conviction" class="big_in form-control selectpicker" data-live-search="false" id="conviction" required>
															<option selected="" style="display:none">&nbsp;</option>
															<option <? if ($user_acc_info['conviction'] == 'yes' ) { ?> selected=""<? } ?>value="yes">Yes</option>
															<option <? if ($user_acc_info['conviction'] == 'no' ) { ?> selected=""<? } ?>value="no">No</option>
											</select></div>




											<div class="form-group">
														<div id="conviction" class="form-group" <? if ($user_acc_info['conviction'] != 'yes' ) { ?> style="display:none" <? } ?> >
																	<label class="control-label">Please provide details of your criminal record in the space below</label>
																	<textarea name="conviction_info" class="form-control" rows="5" id="conviction_info"><? echo $user_acc_info['conviction_info']; ?> </textarea>
														</div>
											</div>





												<div class="page_spacer2"></div>
													<div class="page_spacer2"></div>


												</div>


												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step3" style="xxxdisplay:none;">




													<div class="row">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																 <h3 style=" margin-top: 0;"><span class="color1">Employment History</span></h3>
															 </div>

															 <? foreach($employment as $key => $employ) { ?>
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Employer Name</label>
																	<input type="text" class="form-control" name="emp_name[]" value="<?  echo $employ['name'];  ?>" ><span class="material-input"></span>
																	<input type="hidden" name="emp_id[]" value="<? echo $employ['id'];?>">
																</div>
																<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Job Title</label>
																	<input type="text" class="form-control" name="emt_title[]" value="<?  echo $employ['title'];  ?>" ><span class="material-input"></span>
																</div>
																<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Date</label>
																	<input type="date" class="form-control" name="emp_date_start[]" value="<? echo date ('Y-m-d',$employ['date_start']); ?>" ><span class="material-input"></span>
																</div>
																<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Date</label>
																	<input type="date" class="form-control" name="emp_date_end[]" value="<? echo date ('Y-m-d',$employ['date_end']); ?>" ><span class="material-input"></span>
																</div>

																<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Employer Address</label>
																	<textarea name="emp_address[]" class="form-control" rows="5" id="emp_address"><?  echo $employ['address'];  ?></textarea></div>
																	<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
																		<div class="page_spacer visible-md visible-lg"></div>
																		<div class="page_spacer visible-md visible-lg"></div>
																		<div class="page_spacer2 visible-md visible-lg"></div>
																		<button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button>
																</div>
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<div class="clearfix"></div>
																	<hr class="btn-default" />
																</div>
														</div>
													</div>
													<?}?>

													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<button type="button" class="btn btn-raised btn-success pull-right add_employer">Add Row<div class="ripple-container"></div></button>
													</div>
												</div>


												<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															 <label class="control-label">References</label>
														 </div>


														 <? foreach($reference as $key => $refer) { ?>
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																<label class="control-label">References Name</label>
																<input type="text" class="form-control" name="ref_name[]" value="<?  echo $refer['name'];  ?>" ><span class="material-input"></span>
																<input type="hidden" name="ref_id[]" value="<? echo $refer['id'];?>">
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																<label class="control-label">Contact Number</label>
																<input type="text" class="form-control" name="ref_contact[]" value="<?  echo $refer['contact'];  ?>" ><span class="material-input"></span>
															</div>
															<div class="col-lg-4 col-md-2 col-sm-12 col-xs-12 form-group">
																<label class="control-label">Position Held</label>
																<input type="text" class="form-control" name="ref_position[]" value="<?  echo $refer['position'];  ?>" ><span class="material-input"></span>
															</div>


															<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group">
																<label class="control-label">Contact Address</label>
																<textarea name="ref_address[]" class="form-control" rows="5" id="ref_address"> <?  echo $refer['address'];  ?></textarea></div>

																<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
																	<div class="page_spacer visible-md visible-lg"></div>
																	<div class="page_spacer visible-md visible-lg"></div>
																	<div class="page_spacer2 visible-md visible-lg"></div>
																	<button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button>
																</div>

															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="clearfix"></div>
																<hr class="btn-default" />
															</div>
													</div>
												</div>
												<?}?>
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<button type="button" class="btn btn-raised btn-success pull-right add_references">Add Row<div class="ripple-container"></div></button>
												</div>
											</div>


													<div class="page_spacer2"></div>
													<button class="btn btn-lg btn-primary btn-block" id="edit_profile_form_but" type="button">Save Changes</button>

												</div>

												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step4" style="display:none;"></div>

												</div>
									</form>
									<div class="page_spacer"></div>
			</div>

		</div>
		<div class="page_spacer"></div>
	</div>
	</div>
	<div class="clearfix"></div>
<div class="clearfix"></div>
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/footer.php'); ?>
</div>
</div>
</div><!--wrapper-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_web_modals.php'); ?>
</body>
</html>
