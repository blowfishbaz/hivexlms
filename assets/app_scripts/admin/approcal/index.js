$( document ).ready(function() {


var $table = $('#table');
    var $sixty = $( document ).height();
     $sixty = ($sixty/100)*70;
      $table.bootstrapTable( 'resetView' , {height: $sixty} );


$(function () {
     $table.on('click-row.bs.table', function (e, row, $element) {
         $('.success').removeClass('success');
         $($element).addClass('success');
     });
});
});

function jsUcfirst(value, row, index)
{
    return value.charAt(0).toUpperCase() + value.slice(1);
}

function account_code(value, row, index){
    //return "<a href='proposal.php?id="+row.id+"'>"+value+"</a>"
      if(row.acc_type == 'escort'){
         return 'ES'+value;
      }else if(row.acc_type == 'agency'){
         return 'AG'+value;
      }else if(row.acc_type == 'user'){
         return 'US'+value;
      }else if(row.acc_type == 'admin'){
         return 'AD'+value;
      }
}

function link(value, row, index){
    //return "<a href='proposal.php?id="+row.id+"'>"+value+"</a>"
      return "<a href='javascript:void(0)' data-id='"+row.app_id+"' id='view_approval'>"+value+"</a>"
}

function date_time(value, row, index) {
   var timestamp = value;
   var date = new Date(timestamp*1000);

   var year = date.getFullYear();
   var month = date.getMonth() + 1;
   var day = date.getDate();
   var hours = date.getHours();
   var minutes = date.getMinutes();
   var seconds = date.getSeconds();

   if(day < 10){
       day = "0"+day;
   }
   if(month < 10){
       month = "0"+month;
   }

   if(hours < 10){
      hours = "0"+hours;
   }

   if(minutes < 10){
      minutes = "0"+minutes;
   }

   var date = day + "-" + month + "-" + year+ " "+hours+":"+minutes;
   if(date == "01-01-1970" ){
     date = '';
   }

   return date;
}

$( "body" ).on( "click", "#view_approval", function() {
   var id = $(this).data('id');
   $('#BaseModalL').modal('show');
   $.ajax({
   type: "POST",
   url: "../../../assets/app_ajax/admin/forms/approval_view.php?id="+id,
   data: { id1: id },
   success: function(msg){
      //alert(msg);
      $("#BaseModalLContent").delay(1000)
         .queue(function(n) {
            $(this).html(msg);
            n();
         }).fadeIn("slow").queue(function(n) {
            $.material.init();
            n();
      });
   }
});
});

$( "body" ).on( "click", "#decline_approval", function() {
   var id = $(this).data('id');
   $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
   $.ajax({
      type: "POST",
      url: "../../../assets/app_ajax/admin/forms/decine_approval.php?id="+id,
      data: { id1: id },
      success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
            .queue(function(n) {
               $(this).html(msg);
               n();
            }).fadeIn("slow").queue(function(n) {
               $.material.init();
               n();
               $('#decline_reason').summernote({
                   height: 300,                 // set editor height
                   minHeight: null,             // set minimum height of editor
                   maxHeight: null,             // set maximum height of editor
                   focus: true,                  // set focus to editable area after initializing summernote
                   placeholder: 'Please specify a reason...',
                   toolbar: [
                     ['font', ['bold', 'italic', 'underline']],
                     ['color', ['color']],
                     ['para', ['ul', 'ol', 'paragraph']],
                     ['table', ['table']],
                     ['insert', ['link', 'hr']],
                     ['help', ['help']]
                    ]
                });
         });
      }
   });
});



$( "body" ).on( "click", "#confirm_decline_approval", function() {
   var form = $('#decline_approval_form').serialize();

var HasError = 0;

$('#decline_approval_form').find('textarea').each(function(){
  $(this).parent().removeClass('has-error');
  Messenger().hideAll();
  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
          if(!$(this).prop('required')){
          } else {
                  HasError = 1;
                  $(this).parent().addClass('has-error');
          }
        }
   });

   if (HasError == 1) {
   Messenger().post({
           message: 'Please make sure all required elements of the form are filled out.',
           type: 'error',
           showCloseButton: false
   });
   } else {
    $.ajax({
              type: "POST",
              url: "../../../assets/app_ajax/admin/decline_approval.php",
              data: form,
              success: function(msg){
                   if(msg == 'ok' || msg == ' ok'){
                       Messenger().post({
                        message: 'Approval Declined',
                        showCloseButton: false
                   });

                   setTimeout(function(){
                       //window.location.replace("index.php");
                       location.reload();
                       }, 2000);

                   }else{
                       Messenger().post({
                        message: 'Please try again later',
                        type: 'error',
                        showCloseButton: false
                    });
                   }
              }
           });
   }
});

$( "body" ).on( "click", "#approval_accept", function() {
   var id = $(this).data('id');
   $.ajax({
             type: "POST",
             url: "../../../assets/app_ajax/admin/approve_approval.php",
             data: {approval_id:id},
             success: function(msg){
                  if(msg == 'ok' || msg == ' ok'){
                     Messenger().post({
                       message: 'Approval Accepted',
                       showCloseButton: false
                  });

                  setTimeout(function(){
                     //window.location.replace("index.php");
                     location.reload();
                     }, 2000);

                  }else{
                     Messenger().post({
                       message: 'Please try again later',
                       type: 'error',
                       showCloseButton: false
                   });
                  }
             }
          });
});
