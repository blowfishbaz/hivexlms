<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 //Page Title

 ?>
      <form class="form-signin" action="<?echo $fullurl;?>assets/app_ajax/website/auth.php?action=logon" enctype="application/x-www-form-urlencoded" method="post">
      <!-- <form class="form-signin" action="<?echo $fullurl;?>auth.php?action=logon" enctype="application/x-www-form-urlencoded" method="post">-->

	       <!--<p>Sign In</p>

	    <? if (isset($_GET['error'])) { ?>
       <div class="alert alert-warning alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  Username or Password Incorrect.
		</div>

		<? } ?>
		<? if (isset($_GET['account'])) { ?>
		<div class="alert alert-warning alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  Account has been locked.
		</div>
		<? } ?> -->



		<div class="form-group label-floating is-empty">
                <label for="login" class="control-label">Email</label>
                <input type="email" name="login" class="form-control"  required>

              <span class="material-input"></span>
        </div>

        <div class="form-group label-floating is-empty">
                <label for="password" class="control-label">Password</label>
                <input type="password" class="form-control" name="password" required>

              <span class="material-input"></span>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="1" name="remember"> Remember me
          </label>
        </div>
        <input type="hidden" name="key" value="<? echo getCurrentKey(); ?>">
        <input type="hidden" name="username">
        <button class="btn btn-lg btn-info btn-block" type="submit">Sign in</button>
        <a href="<?echo $fullurl;?>register.php" class="btn btn-lg btn-primary btn-block" type="submit">Register</a>



      </form>
