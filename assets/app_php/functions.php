<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    functions.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */




// create an id, requires id type
function createId($idType){
	//generate time code, with millisecondes added
	$now = DateTime::createFromFormat('U.u', microtime(true));
	$idTime = $now->format("Ymd_Hisu");
	//create random number to stop duplicate ids being created
	$idRandom = rand(10000,99999);
	//join time and random
	return 'id_'.$idType.'_'.$idTime.'_'.$idRandom;
}



function getCurrentKey() {


		$key1 = 77;
		$date = new DateTime();
		$key2 = $date->format('YmdH');

		$keytotal = $key1 * $key2;

		$keyoutput = encrypt($keytotal);
	return $keyoutput;
}



function clean($str) {
		$str = strtolower($str);
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
}

function loadProfilePic($imagename) {


	$file = $_SERVER['DOCUMENT_ROOT'].'/'.'uploads/profiles/'.$imagename.'.png';

	if (file_exists($file)) {

	   return 'http://'.$_SERVER['HTTP_HOST']."/uploads/profiles/".$imagename.'.png';
	} else {

	    return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png";
	}

}

function loadProfilePic2($imagename) {


	$file = $_SERVER['DOCUMENT_ROOT'].'/'.'uploads/profiles/'.$imagename.'.png';

	if (file_exists($file)) {

	   return 'http://'.$_SERVER['HTTP_HOST']."/uploads/profiles/".$imagename.'.png';
	} else {

	    return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/IconMyAccount.png";
	}

}
function checklogin($member){
	$session=session_id();

if($member){}
	$db = new database;
	$db->Query("select * from hx_logged_in where member_id = ? and session_id = ?");
	$db->bind(1,$member);
	$db->bind(2,$session);
	$logged_in = $db->single();
	$logged_in_count = $db->rowcount();

	if($logged_in_count==0){

		$db->query("delete from accounts_persistence where id = ?");
		$db->bind(1,$member);
		$db->execute();

		$thisId = createId('klog');
		$dateNow = time();

if($member!=''){

	return 'not login';
	}
	else{ return 'login';}
}
}

function getCookiePermissions($id) {

			//$id = decrypt($id);
			$pa = array();

			// $db = new database;
			// $db->query("select * from hx_permissions where staffId = ?");
			// $db->bind(1,$id);
			// $permissionsdb = $db->resultset();
			//
			// foreach ($permissionsdb as $p) {
			// 	array_push($pa, $p['area']);
			// }

			$db = new database;
			$db->query("select * from hx_perm where user_id = ? and status = 1");
			$db->bind(1,$id);
			$permissionsdb = $db->resultset();

			foreach ($permissionsdb as $p) {
				array_push($pa, $p['perm_id']);
			}

			return $pa;
}


function checkCookie() {

	if (isset($_COOKIE['sesskey1']) && isset($_COOKIE['sesskey2']) && isset($_COOKIE['sesskey3'])) {

		$salt1 = "jskdhfskjdhfsdklf_hdsjklahgdjkl-sjhfgdkfjhgslkdghdfkj_ghjsdklfhdfjkghjd-lkfghjkdjfhgouwhkjdf093845dhjkvsd";
		$salt2 = "dskfjh38434785348564idfghdkfjhg938457347856_sjdkhfgdjkghdfjkgh--dlkfgjdkfgjdf_dfglkhdfkgjhdfkj";

		$key1 = decrypt($_COOKIE['sesskey1']);
		$key1 = $salt1.$key1.$salt2;
		$key1 = sha1($key1);

		$key2 = decrypt($_COOKIE['sesskey2']);
		$key2 = $salt1.$key2.$salt2;
		$key2 = sha1($key2);

		$key3 = decrypt($_COOKIE['sesskey3']);
		$key3 = $salt1.$key3.$salt2;
		$key3 = sha1($key3);


		$db = new database;
		$db->query("select * from accounts_persistence where key1 = ? and key2 = ? and key3 = ?");
		$db->bind(1,$key1);
		$db->bind(2,$key2);
		$db->bind(3,$key3);
		$member = $db->single();
		$count = $db->rowcount();

		if ($count >= 1) {
			$howLongCookie = CookieLength;
			$lastlogon = $member['lastlogon'];

			$date = date('Y-m-d H:i:s');
			$expire_stamp = strtotime($lastlogon) + (60*$howLongCookie);
			$date = strtotime($date);
			if ($date <= $expire_stamp) {
				//Auto Logon
				setcookie("sesskey1", "", time() - 3600);
				setcookie("sesskey2", "", time() - 3600);
				setcookie("sesskey3", "", time() - 3600);
				$db->query("delete from accounts_persistence where id = ?");
				$db->bind(1,$member['id']);
				$db->execute();
				//$complete = SetSessions($member['userId'],1);

				return $complete;


			} else {

//add relogin here//

//create session variables and cookies.
//$db = new database;
//Get Account
$db->query("select id,screenname,email,profilepic,type from accounts where id = ?");
$db->bind(1,$member['userid']);
$account = $db->single();

//Get Permissions

//$_SESSION['SESS_ACCOUNT_PERMISSIONS'] = getCookiePermissions($account['id']);
$_SESSION['SESS_ACCOUNT_ID'] = encrypt($account['id']);
$_SESSION['SESS_ACCOUNT_Type'] = $account['type'];
$_SESSION['SESS_ACCOUNT_NAME'] = $account['screenname'];
$_SESSION['SESS_EMAIL'] = $account['email'];
$_SESSION['SESS_PROFILEPIC'] = $account['profilepic'];


$db->query("delete from hx_logged_in where member_id = ?");
$db->bind(1,$account['id']);
$db->execute();


$thisId = createId('log');
$dateNow = time();
$session=session_id();

$db->query("insert into hx_logged_in (id,member_id,session_id,create_ts) values (?,?,?,?)");
$db->bind(1,$thisId);
$db->bind(2,$account['id']);
$db->bind(3,$session);
$db->bind(4,$dateNow);
$db->execute();




/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////
// if($account['menuid']!=''){
// 		$db = new database;
// 		$db->query("select * from hx_menu where id = ?");
// 		$db->bind(1,$account['menuid']);
// 		$menu = $db->single();
// 		$_SESSION["MENU"] = $menu['typeof'];
// }
// else{
// 		$_SESSION["MENU"] = 'default';
// }
$_SESSION["MENU"] = 'default';
/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////

				return 0;
			}


		} else {
			$thisId = createId('klog');
			$dateNow = time();

			// $db->query("insert into hx_stat_kicked (id,userid,create_ts) values (?,?,?)");
			// $db->bind(1,$thisId);
			// $db->bind(2,$account['id']);
			// $db->bind(3,$dateNow);
			// $db->execute();

			session_destroy();
			return 0;
		}

	} else {

		session_destroy();
		return 0;
	}

}


function checkPerm($loc){



				switch ($loc) {
	          case "calendar":
	                    $l = 'crm_1';
	                    break;
	          case "communications":
	                    $l = 'crm_2';
	                    break;
	          case "contacts":
	                    $l = 'crm_3';
	                    break;
	          case "customers":
	                    $l = 'crm_4';
	                    break;
	          case "dashboard":
	                    $l = 'crm_5';
	                    break;
	          case "holidays":
	                    $l = 'crm_6';
	                    break;
						case "jobs":
											$l = 'crm_7';
											break;
						case "newsletter":
											$l = 'crm_8';
											break;
						case "quotes":
											$l = 'crm_9';
											break;
						case "reports":
											$l = 'crm_10';
											break;
						case "targets":
											$l = 'crm_11';
											break;
						case "todo":
											$l = 'crm_12';
											break;
						case "users":
											$l = 'crm_13';
											break;
							case "stats":
                    	$l = 'bft_1';
                      break;
	          default:
	                    $l = 'error';
	}

	$url = $_SERVER[REQUEST_URI];

	if (in_array($l, $_SESSION["SESS_ACCOUNT_PERMISSIONS"])) {}
	else{
		error_log('http://'.$_SERVER['HTTP_HOST'].'/application/index.php');
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/application/index.php ');
		die();
	};

}




function loadProfilePicwithID($id) {

	$db = new database;
	$db->query("select ws_uploads.path,ws_uploads.name from accounts join ws_uploads on accounts.profilepic=ws_uploads.id where accounts.id = ?");
	$db->bind(1,$id);
	$img = $db->single();


	$lowres = 'http://'.$_SERVER['HTTP_HOST'].'/img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';

	if ($img['name']!='') {
	   return $lowres;
		 //return '<img class="my_profile_icon" src=" echo loadProfilePicwithID(decrypt($_SESSION['SESS_ACCOUNT_ID'])); " data-holder-rendered="true">';
	} else {
	    //return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png?id=".$id;
			return '<span class="my_profile_icon glyphicons glyphicons-user" data-holder-rendered="true"></span>';
	}

}

// function loadProfilePicwithID($id) {
//
// 	$db = new database;
// 	$db->query("select * from accounts where id = ?");
// 	$db->bind(1,$id);
// 	$data = $db->single();
// 	$imagename = $data['profilepic'];
//
// 	$file = $_SERVER['DOCUMENT_ROOT'].'/'.'uploads/profiles/'.$imagename.'.png';
//
// 	if (file_exists($file)) {
// 	   return 'http://'.$_SERVER['HTTP_HOST']."/uploads/profiles/".$imagename.'.png';
// 	} else {
// 	    return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png";
// 	}
//
// }

function getAccountName($id) {

	$db = new database;
	$db->query("select * from accounts where id = ?");
	$db->bind(1,$id);
	$data = $db->single();

	return $data['name'];

}



function datetimeformat( $ptime ) {

	 $output = date('d F Y - H:i', $ptime);
	    $output = str_replace('-', 'at', $output);
	    return $output;
}


	function timeago( $ptime )
{
    $estimate_time = time() - $ptime;

    if( $estimate_time < 1 )
    {
        return 'less than 1 second ago';
    }

    $condition = array(
                12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    //echo $estimate_time;
    if ($estimate_time >= 86400) {

	    $output = date('d F Y - H:i', $ptime);
	    $output = str_replace('-', 'at', $output);
	    return $output;
    } else {

	   foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;

        if( $d >= 1 )
        {
            $r = round( $d );
            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
        }
    }


    }

    }

////////////////att2db///////////////////////
////////////////att2db///////////////////////
		/*  $tble = 'hx_notes_attachments';
		$idPre = 'att_notx';
		$pid = $_POST["pid"];
		$type = $_POST["type"];
		$name = $_POST["name"];
		$compId = decryptID($_GET["id"]);
		$db = new database;

		att2db($db,$tble,$idPre,$pid, $compId,$type,$name);
		*/
////////////////att2db///////////////////////
////////////////att2db///////////////////////


		function att2db($thisId,$db,$tble,$idPre,$pid,$compId,$type,$name) {


		            $dateNow = time();

		            $db->Query("insert into ".$tble." (id,pid,company_id,type,name,created_date,created_by) values (?,?,?,?,?,?,?)");
		            $db->bind(1,$thisId);
		            $db->bind(2,$pid);
		            $db->bind(3,$compId);
		            $db->bind(4,$type);
		            $db->bind(5,$name);
		            $db->bind(6,$dateNow);
		            $db->bind(7,$_SESSION['SESS_ACCOUNT_NAME']);
		            $db->execute();


								echo 'complete';


		}

/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////
	function okExts() {
		$docExts = array("pdf","doc","dot","docx","dotx","docm","dotm","xls","xlt","xla","xlsx","xltx","xlsm","xltm","xlam","xlsb","ppt","pot","pps","ppa","pptx","potx","ppsx","ppam","pptm","potm","ppsm","odt","ott","odp","otp","ods","ots","gif","png","jpe?g");
		$exList = '';
			foreach($docExts as $ext){
				$exList .= $ext . '|'. strtoupper($ext).'|';
			}
		return rtrim($exList, "|");
	}
/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////

/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////
	function okImageExts() {
		$docExts = array("gif","png","jpe?g");
		$exList = '';
			foreach($docExts as $ext){
				$exList .= $ext . '|'. strtoupper($ext).'|';
			}
		return rtrim($exList, "|");
	}
/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////

	function getFileIcon($myfile) {

	$docMimes = array("application/pdf","application/msword","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.template","application/vnd.ms-word.document.macroEnabled.12", "application/vnd.ms-word.template.macroEnabled.12","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.text-template");
	$spreadMimes = array("application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.spreadsheetml.template","application/vnd.ms-excel.sheet.macroEnabled.12","application/vnd.ms-excel.template.macroEnabled.12","application/vnd.ms-excel.addin.macroEnabled.12","application/vnd.ms-excel.sheet.binary.macroEnabled.12","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.spreadsheet-template");
	$presMimes = array("application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.presentationml.template","application/vnd.openxmlformats-officedocument.presentationml.slideshow","application/vnd.ms-powerpoint.addin.macroEnabled.12","application/vnd.ms-powerpoint.presentation.macroEnabled.12","application/vnd.ms-powerpoint.template.macroEnabled.12","application/vnd.ms-powerpoint.slideshow.macroEnabled.12","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.presentation-template");

								if (in_array($myfile, $docMimes)) {
								    return "filetypes filetypes-doc";
								}
								elseif (in_array($myfile, $spreadMimes)) {
								    return "filetypes filetypes-xls";
								}
								elseif(in_array($myfile, $presMimes)) {
								    return "filetypes filetypes-ppt";
								}
 							else{
								     return "filetypes filetypes-unknown";
								}
	}





		// example code

		//$typesOfFiles = array("images", "docs");


	//	$docMimes = array("application/msword","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.template","application/vnd.ms-word.document.macroEnabled.12", "application/vnd.ms-word.template.macroEnabled.12","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.spreadsheetml.template","application/vnd.ms-excel.sheet.macroEnabled.12","application/vnd.ms-excel.template.macroEnabled.12","application/vnd.ms-excel.addin.macroEnabled.12","application/vnd.ms-excel.sheet.binary.macroEnabled.12","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.presentationml.template","application/vnd.openxmlformats-officedocument.presentationml.slideshow","application/vnd.ms-powerpoint.addin.macroEnabled.12","application/vnd.ms-powerpoint.presentation.macroEnabled.12","application/vnd.ms-powerpoint.template.macroEnabled.12","application/vnd.ms-powerpoint.slideshow.macroEnabled.12","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.text-template","application/vnd.oasis.opendocument.text-web","application/vnd.oasis.opendocument.text-master","application/vnd.oasis.opendocument.graphics","application/vnd.oasis.opendocument.graphics-template","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.presentation-template","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.spreadsheet-template","application/vnd.oasis.opendocument.chart","application/vnd.oasis.opendocument.formula","application/vnd.oasis.opendocument.database","application/vnd.oasis.opendocument.image","application/vnd.openofficeorg.extension");


	function recursive_array_search($needle,$haystack) {
	    foreach($haystack as $key=>$value) {
	        $current_key=$key;
	        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
	            return $current_key;
	        }
	    }
	    return false;
	}


//Get table and return a single record.
	function getSingleRecord($id) {

		$data = explode('_',$id);
		$codeid = $data[1];

		$tables = array(
		    0 => array(
		        'code' => 'err',
		        'table' => 'null'
		    ),
		    1 => array(
		        'code' => 'comx',
		        'table' => 'hx_communications'
		    ),
		    2 => array(
		        'code' => 'acx',
		        'table' => 'hx_activity'
		    ),
		    3 => array(
		        'code' => 'chx',
		        'table' => 'hx_customers'
		    ),
		    4 => array(
		        'code' => 'phx',
		        'table' => 'hx_contacts'
		    ),
		    5 => array(
		        'code' => 'rem',
		        'table' => 'hx_reminders'
		    ),
		    6 => array(
		        'code' => 'adx',
		        'table' => 'hx_addresses'
		    ),
		    7 => array(
		        'code' => 'member',
		        'table' => 'accounts'
		    ),
		    8 => array(
		        'code' => 'notx',
		        'table' => 'hx_notes'
		    ),



		);

		$codeid = recursive_array_search($codeid,$tables);
		$tableSearch = $tables[$codeid]['table'];

		if ( $tableSearch == 'null') {
			return null;
		} else {
			$db = new database;
			$db->Query("select * from ".$tableSearch." where id = ?");
			$db->bind(1,$id);
			$data = $db->single();
			}


		return $data;

	}


	/////////////////////////////////////////////////
	///////////////////Status selecter///////////////
	/////////////////////////////////////////////////

	function job_area($status) {
		switch ($status) {
							case '1':$section='Add Lead';break;
							case '2':$section='EPC Booking Team';break;
							case '3':$section='EPC Confirmation';break;
							case '4':$section='EPC Accessor Que';break;
							case '5':$section='Compliance';break;
							case '6':$section='EPC Reference';break;
							case '7':$section='Price';break;
							case '8':$section='Contribution';break;
							case '9':$section='Book Technical Survey';break;
							case '10':$section='Technical Survey';break;
							case '11':$section='Technical Survey Review';break;
							case '12':$section='Install Booking Team';break;
							case '13':$section='Install Confirmation';break;
							case '14':$section='Installation';break;
							case '15':$section='Technical Monitoring';break;
							case '16':$section='Technical Monitoring Visit';break;
							case '19':$section='Send Compliance';break;
							case '20':$section='Completed';break;
							case '21':$section='Boiler Builds';break;
							case 'end':$section='Untill End';break;
							default:$section='Error';break;
					}
				return $section;
	}

		function job_area_fix($status) {
			switch ($status) {
								case '1':$section='Add Fix';break;
								case '2':$section='Book Fix';break;
								case '3':$section='Fix';break;
								case '4':$section='Complete Fix';break;
								default:$section='Error';break;
						}
					return $section;
		}


function job_status($v){
     switch ($v) {
          case '0':return 'Killed';break;
          case '1':return 'Live';break;
          case '2':return 'Pause';break;
          case '3':return 'Completed';break;
          default:return 'Error_'.$v;break;
     }
}

function job_type($v){
     switch ($v) {
          case 'rst_install':return'Install';break;
          case 'rst_fix':return 'Fix';break;
					case 'tm_elect':return'TM Electrician';break;
					case 'electrician':return'Electrician Fix';break;
					case 'heating':return'Heating Fix';break;
					case 'loft':return'Loft';break;
					case 'boiler service':return'Boiler Service';break;


          default:return'Error_404_'.$v;break;
    }
}

function appointment_status($v){
			switch ($v) {
case '0':return 'Deleted';break;
case '1':return 'Due';break;
case '2':return 'Waiting Confirmation';break;
case '3':return 'On Hold';break;
case '4':return 'Completed';break;
default:return 'Error_'.$v;break;
			}
 }

function appointment_type($v){
switch ($v) {
case 'epcbook':return'EPC Booking';break;
case 'tsbook':return 'Technical Survey Booking';break;
case 'inbook':return'Install Booking';break;
case 'elbook':return'Electrical Booking';break;
case 'tmbook':return'Technical Monitoring Booking';break;
case 'fixbook':return'Fix Booking';break;
case 'loftbook':return'Loft Booking';break;
default:return'Error_'.$v;break;
		 }
}

function installer_type($v){
     switch ($v) {
case 'tsbook':return 'JobSurvey';break;
case 'inbook':return'JobInstall';break;
case 'elbook':return'JobElectrician';break;
case 'tmbook':return'JobTechMonitoring';break;
case 'fixbook':return'JobInstall';break;
case 'loftbook':return'JobInstall';break;
default:return'Error';break;
          }
}

/////////////////////////////////////////////////
///////////////////Status selecter///////////////
/////////////////////////////////////////////////



/////////////////////////////////////////////////
///////////////////new permossions///////////////
/////////////////////////////////////////////////

/////default menu
//    $menu = 'menu';
/////default menu


/*
2 – write
4 – read
*/
/*$sections = array("crm", "sales", "marketing", "products","hr","reports","usermanage","datamanage","admin");#
$area = array("quote", "bar", "hello", "world");
$pages = array("index", "bar", "hello", "world");

$z = array(
        array(
        'used' => 0,
        'section' => 'sales',
        'area'=>'quotes',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'sales',
        'area'=>'orders',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'marketing',
        'area'=>'emailist',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'marketing',
        'area'=>'campaign',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'sales',
        'area'=>'quotes',
        'page' => 'index'
        ),
        array(
        'used' => 0,
        'section' => 'sales',
        'area'=>'quotes',
        'page' => 'index'
        )
  );


	foreach($allPages as $pages){


	    if($pages['used'] == 1 && in_array($pages['section'], $sections)){

	        echo '+'.$pages['section'];
	        echo '</br>';
	        echo '-'.$pages['area'];
	        echo '</br>';


	    }



	}

	echo '<hr>';

	foreach($allPages as $pages){


	    if($pages['used'] == 1 ){

	        echo '+'.$pages['section'];
	        echo '</br>';
	        echo '-'.$pages['area'];
	        echo '</br>';


	    }



	}





*/


/////////////////////////////////////////////////
///////////////////new permossions///////////////
/////////////////////////////////////////////////


function stage_staus($v) {
	switch ($v) {
		case '1':
			return 'Proposal';
			break;
		case '2':
			return 'Proposal / Commission';
			break;
		case '3':
			return 'Error 3';
			break;
		case '4':
			return 'Commissioned';
			break;
		case '5':
			return 'Cold';
			break;

		default:
			return 'Error';
			break;
	}
}

function rate_type($v){
 switch ($v) {
  case 'hourly_rate':
   return "Hourly Rate";
   break;
   case 'hourly_rate_capped':
    return "Hourly Rate Capped (£)";
    break;
    case 'hourly_rate_capped_hours':
     return "Hourly Rate Capped (Hrs)";
     break;
  case 'fixed_rate':
   return "Fixed Fee";
   break;
  case 'budget_range':
   return "Budget Fee";
   break;
		case 'flexi_fee':
			return "Flexi Fee";
			break;
   case 'N/A':
    return "N/A";
    break;
   case '':
 			return "No Rate Selected";
 			break;
  default:
   return 'Error';
   break;
 }
}

function department_status($v){
		switch ($v) {
			case '0':
				return '<span class="label label-default" style="padding:5px;">N/A</span>';
				break;
			case '1':
				return '<span class="label label-info" style="padding:5px;">Proposal</span>';
				break;
			case '2':
				return '<span class="label label-success" style="padding:5px;">Commissioned</span>';
				break;
			case '3':
				return '<span class="label label-warning" style="padding:5px;">Cold</span>';
				break;
   case '4':
    return '<span class="label" style="padding:5px;">Hold</span>';
    break;
			default:
				return '<span class="label label-danger" style="padding:5px;">Error</span>';
				break;
		}
}


 function first_letter($v){
     if(!empty($v)){
         $arr = explode(' ',trim($v));
         if($arr[0] == 'Mileage'){
             return 'Mileage';
         }else{
             return '';
         }
     }else{
         return '';
     }
 }

 function mileage($v){
     $arr = array('id_exp_20180604_140653199700_64813','id_exp_20180604_140702389300_98092','id_exp_20180604_140803577200_83171','id_exp_20180604_140817698600_91445');
     if(in_array($v,$arr)){
         return 'Mileage';
     }else{
         return '';
     }
 }

 function proposal_status($v){
   switch ($v) {
    case '1':
          return 'Awaiting Allocation';
    break;
		case '1.5':
          return 'Qualification';
    break;
    case '2':
    						return 'Awaiting Proposal';
    break;
    case '3':
    						return 'Awaiting Peer Review';
    break;
    case '4':
    						return 'Awaiting Proof Read';
    break;
    case '5':
    						return 'Ready to Send';
    break;
    case '6':
    						return 'Proposal Sent';
    break;
		case '7':
								return 'Proposal Follow Up';
		break;
    default:
     return 'Error - :(';
     break;
   }
 }

 function commission_status($v){
  switch ($v) {
   case '1':
    return 'Awaiting Allocation';
    break;
			case '2':
				return 'Work in Progress';
				break;
				case '3':
					return 'Completed';
					break;
   default:
   	return 'Error - :(';
    break;
  }
 }

 function month($v){
  switch ($v) {
   case '1':
         return 'January';
   break;
   case '2':
         return 'February';
   break;
   case '3':
         return 'March';
   break;
   case '4':
         return 'April';
   break;
   case '5':
         return 'May';
   break;
   case '6':
         return 'June';
   break;
   case '7':
         return 'July';
   break;
   case '8':
         return 'August';
   break;
   case '9':
         return 'September';
   break;
   case '10':
         return 'October';
   break;
   case '11':
         return 'November';
   break;
   case '12':
         return 'December';
   break;
   default:
    return 'Error - :(';
    break;
  }
 }

 function perm_checker($perm1){
 if(in_array($perm1, $_SESSION['SESS_ACCOUNT_PERMISSIONS'])){
  return 'yes';
 }else{
   return 'nope';
 }
}

function ExpensesStatus($s){
 switch ($s) {
	 case '0':
 				return 'Request Rejected';
 	break;
	case '1':
				return 'Waiting Approval';
	break;
	case '2':
				return 'Approved';
	break;
	case '3':
				return 'Expense Sent';
	break;
	case '4':
				return 'Expense Paid';
	break;
	default:
	 return 'Error - :(';
	 break;
 }
}
	//////////////
	//add activity
	//////////////
	function add_activity($activity_type,$activity_area,$company_id,$item_id){
			$created_date = time();
			$activity_by =decrypt($_SESSION['SESS_ACCOUNT_ID']);
			$act_id=createid('acx');
			$page=$_SERVER['HTTP_REFERER'];

			 	$db = new database;
			  $db->query("insert into hx_activity (id, activity_type, activity_area, activity_by, created_date, company_id, item_id,page) values (?,?,?,?,?,?,?,?)");
			  $db->bind(1,createid('acx'));
			  $db->bind(2,$activity_type);
			  $db->bind(3,$activity_area);
			  $db->bind(4,$activity_by);
			  $db->bind(5,$created_date);
			  $db->bind(6,$company_id);
			  $db->bind(7,$item_id);
				$db->bind(8,$page);
			  $db->execute();


			return 'complete';
	}

	function add_activity_pid($activity_type,$activity_area,$company_id,$item_id,$pid){
			$created_date = time();
			$activity_by =decrypt($_SESSION['SESS_ACCOUNT_ID']);
			$act_id=createid('acx');
			$page=$_SERVER['HTTP_REFERER'];
			$picked_date=strtotime ($date);

				$db = new database;
				$db->query("insert into hx_activity (id, activity_type, activity_area, activity_by, created_date, company_id, item_id,page, pid) values (?,?,?,?,?,?,?,?,?)");
				$db->bind(1,createid('acx'));
				$db->bind(2,$activity_type);
				$db->bind(3,$activity_area);
				$db->bind(4,$activity_by);
				$db->bind(5,$created_date);
				$db->bind(6,$company_id);
				$db->bind(7,$item_id);
				$db->bind(8,$page);
				$db->bind(9,$pid);
				$db->execute();


			return 'complete';
	}

	function add_activity_date($activity_type,$activity_area,$company_id,$item_id,$date,$pid){
			$created_date = time();
			$activity_by =decrypt($_SESSION['SESS_ACCOUNT_ID']);
			$act_id=createid('acx');
			$page=$_SERVER['HTTP_REFERER'];
			$picked_date=strtotime ($date);

				$db = new database;
				$db->query("insert into hx_activity (id, activity_type, activity_area, activity_by, created_date, company_id, item_id,page, pick_date,pid) values (?,?,?,?,?,?,?,?,?,?)");
				$db->bind(1,createid('acx'));
				$db->bind(2,$activity_type);
				$db->bind(3,$activity_area);
				$db->bind(4,$activity_by);
				$db->bind(5,$created_date);
				$db->bind(6,$company_id);
				$db->bind(7,$item_id);
				$db->bind(8,$page);
				$db->bind(9,$picked_date);
				$db->bind(10,$pid);
				$db->execute();


			return 'complete';
	}
	//////////////
	//add activity
	//////////////

 function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

 function add_notification($user, $pid, $title, $message, $type) {

	 $thisId = createId('not');
	 $now = time();

	 $db = new database;
	 $db->query("insert into bv_notifications (id,created_for,pid,title,message,type,created_date) values (?,?,?,?,?,?,?)");
	 $db->bind(1,$thisId);
	 $db->bind(2,$user);
	 $db->bind(3,$pid);
	 $db->bind(4,$title);
	 $db->bind(5,$message);
	 $db->bind(6,$type);
	 $db->bind(7,$now);
	 $db->execute();
 }


 function find_min_sal($salary_type,$per_annum,$per_hourly){
	 if($salary_type == 'per annum'){
		 if('0' <= $per_annum && $per_annum <= '9999'){return '0';}
		 else if('10000' <= $per_annum && $per_annum <= '11999'){return '10000';}
		 else if('12000' <= $per_annum && $per_annum <= '13999'){return '12000';}
		 else if('14000' <= $per_annum && $per_annum <= '15999'){return '14000';}
		 else if('16000' <= $per_annum && $per_annum <= '17999'){return '16000';}
		 else if('18000' <= $per_annum && $per_annum <= '19999'){return '18000';}
		 else if('20000' <= $per_annum && $per_annum <= '21999'){return '20000';}
		 else if('22000' <= $per_annum && $per_annum <= '23999'){return '22000';}
		 else if('24000' <= $per_annum && $per_annum <= '25999'){return '24000';}
		 else if('26000' <= $per_annum && $per_annum <= '27999'){return '26000';}
		 else if('28000' <= $per_annum && $per_annum <= '29999'){return '28000';}
		 else if('30000' <= $per_annum && $per_annum <= '34999'){return '30000';}
		 else if('35000' <= $per_annum && $per_annum <= '39999'){return '35000';}
		 else if('40000' <= $per_annum && $per_annum <= '44999'){return '40000';}
		 else if('45000' <= $per_annum && $per_annum <= '49999'){return '45000';}
		 else if('50000' <= $per_annum && $per_annum <= '54999'){return '50000';}
		 else if('55000' <= $per_annum && $per_annum <= '59999'){return '55000';}
		 else if('60000' <= $per_annum && $per_annum <= '64999'){return '60000';}
		 else if('65000' <= $per_annum && $per_annum <= '69999'){return '65000';}
		 else if('70000' <= $per_annum && $per_annum <= '74999'){return '70000';}
		 else if('75000' <= $per_annum && $per_annum <= '79999'){return '75000';}
		 else if('80000' <= $per_annum && $per_annum <= '84999'){return '80000';}
		 else if('85000' <= $per_annum && $per_annum <= '89999'){return '85000';}
		 else if('90000' <= $per_annum && $per_annum <= '94999'){return '90000';}
		 else if('95000' <= $per_annum && $per_annum <= '99999'){return '95000';}
		 else if('100000' <= $per_annum && $per_annum <= '999999'){return '100000';}
		 else{return 0;}
	 }else if($salary_type == 'per hour'){
		 if('0' <= $per_hourly && $per_hourly <= '5.00'){return '0';}
		 else if('5.00' <= $per_hourly && $per_hourly <= '5.99'){return '5';}
		 else if('6.00' <= $per_hourly && $per_hourly <= '6.99'){return '6';}
		 else if('7.00' <= $per_hourly && $per_hourly <= '7.99'){return '7';}
		 else if('8.00' <= $per_hourly && $per_hourly <= '8.99'){return '8';}
		 else if('9.00' <= $per_hourly && $per_hourly <= '9.99'){return '9';}
		 else if('10.00' <= $per_hourly && $per_hourly <= '10.99'){return '10';}
		 else if('11.00' <= $per_hourly && $per_hourly <= '11.99'){return '11';}
		 else if('12.00' <= $per_hourly && $per_hourly <= '12.99'){return '12';}
		 else if('13.00' <= $per_hourly && $per_hourly <= '13.99'){return '13';}
		 else if('14.00' <= $per_hourly && $per_hourly <= '14.99'){return '14';}
		 else if('15.00' <= $per_hourly && $per_hourly <= '17.99'){return '15';}
		 else if('17.00' <= $per_hourly && $per_hourly <= '19.99'){return '18';}
		 else if('20.00' <= $per_hourly && $per_hourly <= '22.99'){return '20';}
		 else if('22.00' <= $per_hourly && $per_hourly <= '24.99'){return '23';}
		 else if('25.00' <= $per_hourly && $per_hourly <= '26.99'){return '25';}
		 else if('27.00' <= $per_hourly && $per_hourly <= '29.99'){return '28';}
		 else if('30.00' <= $per_hourly && $per_hourly <= '32.99'){return '30';}
		 else if('32.00' <= $per_hourly && $per_hourly <= '34.99'){return '33';}
		 else if('35.00' <= $per_hourly && $per_hourly <= '36.99'){return '35';}
		 else if('37.00' <= $per_hourly && $per_hourly <= '39.99'){return '38';}
		 else if('40.00' <= $per_hourly && $per_hourly <= '42.99'){return '40';}
		 else if('42.00' <= $per_hourly && $per_hourly <= '44.99'){return '43';}
		 else if('45.00' <= $per_hourly && $per_hourly <= '46.99'){return '45';}
		 else if('47.00' <= $per_hourly && $per_hourly <= '49.99'){return '48';}
		 else if('50.00' <= $per_hourly && $per_hourly <= '52.99'){return '50';}
		 else if('52.00' <= $per_hourly && $per_hourly <= '54.99'){return '53';}
		 else if('55.00' <= $per_hourly && $per_hourly <= '56.99'){return '55';}
		 else if('57.00' <= $per_hourly && $per_hourly <= '59.99'){return '58';}
		 else if('60.00' <= $per_hourly && $per_hourly <= '62.99'){return '60';}
		 else if('62.00' <= $per_hourly && $per_hourly <= '64.99'){return '63';}
		 else if('65.00' <= $per_hourly && $per_hourly <= '999999'){return '65';}
		 else{return 0;}
	 }else{
		 return 0;
	 }
 }
 function find_max_sal($salary_type,$per_annum,$per_hourly){
	if($salary_type == 'per annum'){
		if('0' <= $per_annum && $per_annum <= '9999'){return '9999';}
		else if('10000' <= $per_annum && $per_annum <= '11999'){return '11999';}
		else if('12000' <= $per_annum && $per_annum <= '13999'){return '13999';}
		else if('14000' <= $per_annum && $per_annum <= '15999'){return '15999';}
		else if('16000' <= $per_annum && $per_annum <= '17999'){return '17999';}
		else if('18000' <= $per_annum && $per_annum <= '19999'){return '19999';}
		else if('20000' <= $per_annum && $per_annum <= '21999'){return '21999';}
		else if('22000' <= $per_annum && $per_annum <= '23999'){return '23999';}
		else if('24000' <= $per_annum && $per_annum <= '25999'){return '25999';}
		else if('26000' <= $per_annum && $per_annum <= '27999'){return '27999';}
		else if('28000' <= $per_annum && $per_annum <= '29999'){return '29999';}
		else if('30000' <= $per_annum && $per_annum <= '34999'){return '34999';}
		else if('35000' <= $per_annum && $per_annum <= '39999'){return '39999';}
		else if('40000' <= $per_annum && $per_annum <= '44999'){return '44999';}
		else if('45000' <= $per_annum && $per_annum <= '49999'){return '49999';}
		else if('50000' <= $per_annum && $per_annum <= '54999'){return '54999';}
		else if('55000' <= $per_annum && $per_annum <= '59999'){return '59999';}
		else if('60000' <= $per_annum && $per_annum <= '64999'){return '64999';}
		else if('65000' <= $per_annum && $per_annum <= '69999'){return '69999';}
		else if('70000' <= $per_annum && $per_annum <= '74999'){return '74999';}
		else if('75000' <= $per_annum && $per_annum <= '79999'){return '79999';}
		else if('80000' <= $per_annum && $per_annum <= '84999'){return '84999';}
		else if('85000' <= $per_annum && $per_annum <= '89999'){return '89999';}
		else if('90000' <= $per_annum && $per_annum <= '94999'){return '94999';}
		else if('95000' <= $per_annum && $per_annum <= '99999'){return '99999';}
		else if('100000' <= $per_annum && $per_annum <= '999999'){return '999999';}
		else{return 0;}
	}else if($salary_type == 'per hour'){
		if('0' <= $per_hourly && $per_hourly <= '5.00'){return '5';}
		else if('5.00' <= $per_hourly && $per_hourly <= '5.99'){return '6';}
		else if('6.00' <= $per_hourly && $per_hourly <= '6.99'){return '7';}
		else if('7.00' <= $per_hourly && $per_hourly <= '7.99'){return '8';}
		else if('8.00' <= $per_hourly && $per_hourly <= '8.99'){return '9';}
		else if('9.00' <= $per_hourly && $per_hourly <= '9.99'){return '10';}
		else if('10.00' <= $per_hourly && $per_hourly <= '10.99'){return '11';}
		else if('11.00' <= $per_hourly && $per_hourly <= '11.99'){return '12';}
		else if('12.00' <= $per_hourly && $per_hourly <= '12.99'){return '13';}
		else if('13.00' <= $per_hourly && $per_hourly <= '13.99'){return '14';}
		else if('14.00' <= $per_hourly && $per_hourly <= '14.99'){return '15';}
		else if('15.00' <= $per_hourly && $per_hourly <= '17.99'){return '18';}
		else if('17.00' <= $per_hourly && $per_hourly <= '19.99'){return '20';}
		else if('20.00' <= $per_hourly && $per_hourly <= '22.99'){return '23';}
		else if('22.00' <= $per_hourly && $per_hourly <= '24.99'){return '25';}
		else if('25.00' <= $per_hourly && $per_hourly <= '26.99'){return '28';}
		else if('27.00' <= $per_hourly && $per_hourly <= '29.99'){return '30';}
		else if('30.00' <= $per_hourly && $per_hourly <= '32.99'){return '33';}
		else if('32.00' <= $per_hourly && $per_hourly <= '34.99'){return '35';}
		else if('35.00' <= $per_hourly && $per_hourly <= '36.99'){return '38';}
		else if('37.00' <= $per_hourly && $per_hourly <= '39.99'){return '40';}
		else if('40.00' <= $per_hourly && $per_hourly <= '42.99'){return '43';}
		else if('42.00' <= $per_hourly && $per_hourly <= '44.99'){return '45';}
		else if('45.00' <= $per_hourly && $per_hourly <= '46.99'){return '48';}
		else if('47.00' <= $per_hourly && $per_hourly <= '49.99'){return '50';}
		else if('50.00' <= $per_hourly && $per_hourly <= '52.99'){return '53';}
		else if('52.00' <= $per_hourly && $per_hourly <= '54.99'){return '55';}
		else if('55.00' <= $per_hourly && $per_hourly <= '56.99'){return '58';}
		else if('57.00' <= $per_hourly && $per_hourly <= '59.99'){return '60';}
		else if('60.00' <= $per_hourly && $per_hourly <= '62.99'){return '63';}
		else if('62.00' <= $per_hourly && $per_hourly <= '64.99'){return '65';}
		else if('65.00' <= $per_hourly && $per_hourly <= '999999'){return '999999';}
		else{return 0;}
	}else{
		return 0;
	}
 }




 function get_document_list($account_id){
	 $return_array = array();

	 $db = new database;
	 $db->query("select * from ws_accounts_roles where account_id = ? and status = 1");
	 $db->bind(1,$account_id);
	 $user = $db->resultset();


	 foreach ($user as $u) {
	   switch($u['role_id']){
	     case '1':
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'photo_id');
	         break;
	     case '2':
	         array_push($return_array,'bls_certificate');
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'university_qualifications');
	         array_push($return_array,'safeguarding_adults');
	         array_push($return_array,'safeguarding_children');
	         array_push($return_array,'CV');
	         array_push($return_array,'hep_b_certificate');
	         array_push($return_array,'photo_id');
	         break;
	     case '3':
			 case '11':
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'photo_id');
	         array_push($return_array,'reference_from_university');
	         break;
	     case '4':
	         array_push($return_array,'bls_certificate');
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'university_qualifications');
	         array_push($return_array,'safeguarding_adults');
	         array_push($return_array,'safeguarding_children');
	         array_push($return_array,'CV');
	         array_push($return_array,'hep_b_certificate');
	         array_push($return_array,'photo_id');
	         break;
	     case '5':
	         array_push($return_array,'bls_certificate');
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'safeguarding_adults');
	         array_push($return_array,'safeguarding_children');
	         array_push($return_array,'hep_b_certificate');
	         array_push($return_array,'photo_id');
	         array_push($return_array,'phlebotomy_training');
	         break;
	     case '6':
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'photo_id');
	         break;
	     case '7':
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'photo_id');
	         break;
	     case '8':
	         array_push($return_array,'bls_certificate');
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'university_qualifications');
	         array_push($return_array,'safeguarding_adults');
	         array_push($return_array,'safeguarding_children');
	         array_push($return_array,'photo_id');
	         break;
	     case '9':
	         array_push($return_array,'photo_id');
	         break;
	     case '10':
	         array_push($return_array,'bls_certificate');
	         array_push($return_array,'enhanced_dbs');
	         array_push($return_array,'university_qualifications');
	         array_push($return_array,'safeguarding_adults');
	         array_push($return_array,'safeguarding_children');
	         array_push($return_array,'cv');
	         array_push($return_array,'hep_b_certificate');
	         array_push($return_array,'photo_id');
	         array_push($return_array,'gp_cct_mrcgp_jcptgp');
	         break;
					 case '12':
						 array_push($return_array,'enhanced_dbs');
						 array_push($return_array,'photo_id');
						 array_push($return_array,'bsc_training_prog_coll_para');
						 array_push($return_array,'health_and_care_profession_council');
						 break;
					 case '13':
						 array_push($return_array,'enhanced_dbs');
						 array_push($return_array,'photo_id');
						 break;
					 case '15':
						 array_push($return_array,'enhanced_dbs');
						 array_push($return_array,'photo_id');
						 array_push($return_array,'registered_general_nurse_q');
						 array_push($return_array,'nmc_registration');
						 array_push($return_array,'bsc_advanced_clinical_practice');
						 array_push($return_array,'ind_prescriber_qual');
						 break;
					 case '16':
						 array_push($return_array,'enhanced_dbs');
						 array_push($return_array,'photo_id');
						 array_push($return_array,'general_phara_council');
						 array_push($return_array,'masters_in_pharmacy');
						 array_push($return_array,'specialist_through_post_grad');
						 array_push($return_array,'independent_presriber');
						 array_push($return_array,'cli_min_post_experience');
						 break;
					 case '17':
						 array_push($return_array,'enhanced_dbs');
						 array_push($return_array,'photo_id');
						 array_push($return_array,'proof_of_experience_phara_tech');
						 break;
					 case '18':
						 array_push($return_array,'enhanced_dbs');
						 array_push($return_array,'photo_id');
						 array_push($return_array,'phlebotomy_training');
						 break;
	       }
	 }


	 $return_array = array_unique ($return_array);

	 return $return_array;


 }


 function document_type_full_name($name){
	 switch ($name) {
	 	case 'bls_certificate':
	 		return 'BLS Certificate';
	 		break;
		case 'enhanced_dbs':
			return 'Enhanced DBS';
			break;
		case 'university_qualifications':
			return 'University Qualifications';
			break;
		case 'safeguarding_adults':
			return 'Safeguarding Adults';
			break;
		case 'safeguarding_children':
			return 'Safeguarding Children';
			break;
		case 'cv':
		case 'CV':
			return 'CV';
			break;
		case 'hep_b_certificate':
			return 'Hep B Certificate';
			break;
		case 'photo_id':
			return 'Photo ID';
			break;
		case 'university_qualification':
			return 'University Qualification';
			break;
		case 'phlebotomy_training':
			return 'Phlebotomy Training';
			break;
		case 'hep_b_cert':
			return 'Hep B Cert';
			break;
		case 'reference_from_university':
			return 'Reference from University';
			break;
		case 'gp_cct_mrcgp_jcptgp':
			return 'GP (CCT/MRCGP/JCPTGP)';
			break;
		case 'bsc_training_prog_coll_para':
			return 'BSc in training programme approved by College of Paramedics';
			break;
		case 'health_and_care_profession_council':
			return 'Health and Care Professions Council (HCPC) registration';
			break;
		case 'registered_general_nurse_q':
			return 'Registered General Nurse qualification';
			break;
		case 'nmc_registration':
			return 'NMC registration';
			break;
		case 'bsc_advanced_clinical_practice':
			return 'BSC in advanced clinical practice (Level 7)or RCN Credentialling qualification (this should include clinical skills and diagnostics)';
			break;
		case 'ind_prescriber_qual':
			return 'Independent Prescriber qualification (V300)';
			break;
		case 'general_phara_council':
			return 'Mandatory registration with General Pharmaceutical Council (GPC)';
			break;
		case 'masters_in_pharmacy':
			return 'Masters degree in pharmacy (MPharm) (or equivalent)';
			break;
		case 'specialist_through_post_grad':
			return 'Specialist knowledge acquired through post graduate diploma level or equivalent training/experience';
			break;
		case 'independent_presriber':
			return 'Independent prescriber (preferred)';
			break;
		case 'cli_min_post_experience':
			return 'Minimum of 2 years post[1]qualification experience';
			break;
		case 'proof_of_experience_phara_tech':
			return 'Proof of experience in pharmacy technician';
			break;

	 	default:
	 		return 'Error'.$name;
	 		break;
	 }
 }

 function subadmin_areas($id){
	 $db = new database;
	 $db->Query("select * from accounts_service_area where pid = ? and status = 1");
	 $db->bind(1,$id);
	 $data = $db->resultSet();

	 $return_array = array();

	 foreach ($data as $d) {
	 	array_push($return_array, $d['area']);
	 }

	 return $return_array;
 }

 function my_account_type($id){
	 $db = new database;
	 $db->Query("select * from accounts where id = ?");
	 $db->bind(1,$id);
	 $data = $db->single();
	 return $data['account_type'];
 }

 function create_api_token($id){
	 $now = DateTime::createFromFormat('U.u', microtime(true));
	 $idTime = $now->format("Ymd_Hisu");
	 //create random number to stop duplicate ids being created
	 $idRandom = rand(10000,99999);
	 $token = 'id_api_token_'.$id.'_'.$idTime.'_'.$idRandom;

	 return $token;
 }

 function id_checker($id, $var){
	 $array = explode("_", $id);
	 if($array['1'] == $var){
		 return true;
		 exit();
	 }else{
		 return false;
		 exit();
	 }
 }

 function subPCNList(){
	 $id = decrypt($_SESSION['SESS_ACCOUNT_ID']);

	 $db = new database;
	 $db->Query("select * from accounts_sub_pcn where account_id = ? and status = 1");
	 $db->bind(1,$id);
	 $data = $db->resultSet();

	 //My PCN

	 //My Practices Under the PCN

	 //return $return_sting;
 }

 function subPracList(){

 }

 function sub_admin_query($q_var, $op = ''){
	 $id = decrypt($_SESSION['SESS_ACCOUNT_ID']);

	 $db = new database;
	 $db->Query("select * from accounts_sub_pcn where account_id = ? and status = 1");
	 $db->bind(1,$id);
	 $pcn = $db->resultSet();

	 $db = new database;
	 $db->Query("select * from accounts_sub_practice where account_id = ? and status = 1");
	 $db->bind(1,$id);
	 $practice = $db->resultSet();

	 if(my_account_type($id) == 'subadmin'){
		 $return = ' '.$op.' '.$q_var.' in ("0",';

		 foreach ($pcn as $p) {
			 $db->query("select * from ws_practice where pcn_location = ? and status = 1");
			 $db->bind(1,$p['pcn_id']);
			 $practice_inner = $db->ResultSet();

		 	$return .= '"'.$p['pcn_id'].'",';

			foreach ($practice_inner as $pi) {
			 $return .= '"'.$pi['id'].'",';
			}
		 }

		 foreach ($practice as $pr) {
			$return .= '"'.$pr['practice_id'].'",';
		 }
		 $return .= '"0")';

		 return $return;

	 }else{
		 return '';
	 }
 }

 function myPractPCNArray($id){

	 $return_array = array();

	 $db = new database;
	 $db->Query("select * from accounts_sub_pcn where account_id = ? and status = 1");
	 $db->bind(1,$id);
	 $pcn = $db->resultSet();
	 $db = new database;
	 $db->Query("select * from accounts_sub_practice where account_id = ? and status = 1");
	 $db->bind(1,$id);
	 $practice = $db->resultSet();

	 foreach ($pcn as $p) {
	 	array_push($return_array,$p['pcn_id']);
	 }

	 foreach ($practice as $prac) {
		array_push($return_array,$prac['practice_id']);
	 }

	 return $return_array;

 }

?>
