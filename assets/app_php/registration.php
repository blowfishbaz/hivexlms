<?
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    auth.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


	include ($_SERVER["DOCUMENT_ROOT"] .'/application.php');

					$newid = createId('member');
					$now = time();

					$username=$_POST["username"];
					$first_name=$_POST["first_name"];
					$surname=$_POST["surname"];
					$email=$_POST["email"];
					$age=$_POST["age"];
					$type=$_POST["type"];
					$password=$_POST["new_password"];

					$gender=$_POST["gender"];
					$orientation=$_POST["orientation"];
					$dob=$_POST["dob"];
					$postcode=$_POST["postcode"];
					$region=$_POST["region"];
					$county=$_POST["county"];
					$about=$_POST["about"];

					// $db->query("update accounts set attempts = ? where id = ?");
					// $db->bind(1,'0');
					// $db->bind(2,$member['id']);
					// $db->execute();

$db->query("insert into accounts (id, username, password, screenname, email, type ,status, attempts, lastattempt,created_date, profilepic) values (?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1, $newid);
$db->bind(2, encrypt($email));
$db->bind(3, encrypt($password));
$db->bind(4, $username);
$db->bind(5, $email);
$db->bind(6, $type);
$db->bind(7, '1');
$db->bind(8, '0');
$db->bind(9, $now);
$db->bind(10, $now);
$db->bind(11, '');
$db->execute();

$newinfoid = createId('info');
$db->query("insert into bv_accounts_info (id, pid, first_name, surname, type, status, gender ,orientation, dob, postcode,region, county,about,created_date,profilepic1,profilepic2) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1, $newinfoid);
$db->bind(2, $newid);
$db->bind(3, $first_name);
$db->bind(4, $surname);
$db->bind(5, $type);
$db->bind(6, '1');
$db->bind(7, $gender);
$db->bind(8, $orientation);
$db->bind(9, $dob);
$db->bind(10, $postcode);
$db->bind(11, $region);
$db->bind(12, $county);
$db->bind(13, '');
$db->bind(14, $now);
$db->bind(15, '');
$db->bind(16, '');
$db->execute();


//header('Location: '.$fullurl.'/index.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');
?>

<form style="display:none;" class="form-signin" action="<?echo $fullurl;?>assets/app_php/auth.php?action=logon&registration" enctype="application/x-www-form-urlencoded" method="post">
<div class="form-group label-floating is-empty">
					<label for="login" class="control-label">Email</label>
					<input type="email" name="login" class="form-control" value="<? echo $email;?>" required>
				<span class="material-input"></span>
	</div>
	<div class="form-group label-floating is-empty">
					<label for="password" class="control-label">Password</label>
					<input type="password" class="form-control" name="password" value="<? echo $password;?>" required>

				<span class="material-input"></span>
	</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" value="1" name="remember"> Remember me
		</label>
	</div>
	<input type="hidden" name="key" value="<? echo getCurrentKey(); ?>">
	<input type="hidden" name="username">
	<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>

<script>
$(document).ready(function() {
			$(".form-signin").submit();

});
</script>
