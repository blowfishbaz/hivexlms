<?
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    auth.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


	include ($_SERVER["DOCUMENT_ROOT"] .'/application.php');
	include ($_SERVER["DOCUMENT_ROOT"] .'/assets/app_php/authfunctions.php');
	 	// Logon Section

	if (isset($_GET['action']) && $_GET['action'] == 'logon') {

		//echo 'a';

		$logonError = 0;

		//Check that the form isnt fully Filled

		if ($_POST['username'] == '') {


			//Check for Key Match to confirm form coming from main site.
			$db->console("Posted".$_POST['key']." current is ".getCurrentKey());
			if (isset($_POST['key']) && $_POST['key'] == getCurrentKey()) {

				//All Checks are ok now check auth.

					//Sanitize the POST values
					$login = $_POST['login'];
					$password = $_POST['password'];
					$login = $login;
					//Validate no Blank Details
					if($login == '') {
						$logonError = 1;
					}
					if($password == '') {
						$logonError = 1;
					}
					//if no errors so far then carry on

					if($logonError == 0) {

						$login = encrypt($login);
						$password = encrypt($password);


						$db->query("select * from accounts where username= ? AND password= ?");
						$db->bind(1,$login);
						$db->bind(2,$password);
						$member = $db->single();
						$count = $db->rowcount();

						if ($count >= 1) {

							//Check to see if user is locked?
							$date = date('Y-m-d H:i:s');

							$expire_stamp = strtotime($member['lastattempt']) + (30*60);
							$date = strtotime($date);
							$attempts = $member['attempts'];

							if($attempts == 3 && $date >= $expire_stamp) {
								$locked = 0;
							} else if ($attempts == 3 && $date < $expire_stamp) {
								$locked = 1	;
							} else {
								$locked = 0;
							}

							if ($locked == 0) {

								//Username and pass correct and not locked so can logon
								$id = $member['id'];

								$remember = $_POST['remember'];


								$db->query("update accounts set attempts = ? where id = ?");
								$db->bind(1,'0');
								$db->bind(2,$member['id']);
								$db->execute();

								//remove old persistent records in db
								$db->query("delete from accounts_persistence where lastlogon  <= CURRENT_DATE() - INTERVAL 1 MONTH");
								$db->execute();

								//echo $logonError;
								$logonError = SetSessions($id,$remember);

							}
							//Account Locked
							else {

								$logonError = 2;
							}



						}
						//Cannot Find User with username and pass so see if the email exists
						else {


							$db->query("select * from accounts where username = ?");
							$db->bind(1,$login);
							$member2 = $db->single();
							$count2 = $db->rowcount();
							$db->console($count2);
							//If found add attempts
							if ($count2 >= 1) {

								//Check to see if user is locked?
								$date = date('Y-m-d H:i:s');
								$expire_stamp = strtotime($member2['lastattempt']) + (30*60);
								$date = strtotime($date);
								$attempts = $member2['attempts'];
								$db->console("attempts:".$attempts);
								if($attempts == 3 && $date >= $expire_stamp) {
									$locked = 0;
								} else if ($attempts == 3 && $date < $expire_stamp) {
									$locked = 1	;
								} else {
									$locked = 0;
								}

								$db->console("locked:".$locked);

								if ($locked == 0) {


								$current = $attempts + 1;
								$date = date('Y-m-d H:i:s');

								$db->query("update accounts set attempts = ?, lastattempt = ? where id = ?");
								$db->bind(1,$current);
								$db->bind(2,time());
								$db->bind(3,$member2['id']);
								$db->execute();
								}
								else {
									$logonError = 2;
								}
							}
							if ($logonError == 0 ) { $logonError = 1; }
						}
					}
			} else {
				if ($logonError == 0 ) { $logonError = 1; }
			}
		} else {
			if ($logonError == 0 ) { $logonError = 1; }
		}


	// Decide What to do.

	if ($logonError == 1) {
			header('Location:'.$fullurl.'admin/index.php?error');
	} elseif ($logonError == 2) {
			header('Location:'.$fullurl.'admin/index.php?account');
	} elseif ($logonError == 77) {

			///////////
$thisId = createId('log');
$dateNow = time();
$session=session_id();
$member=decrypt($_SESSION['SESS_ACCOUNT_ID']);
//error_log('======================'.$session.'================================');
//error_log('============================1====================================');
$db->query("select * from hx_logged_in where member_id = ?");
$db->bind(1,$member);
$logged_in = $db->single();
$logged_in_count = $db->rowcount();
//error_log('============================2====================================');
if($logged_in_count>0){
//error_log('==========================3======================================');
	$db->query("delete from hx_logged_in where member_id = ?");
	$db->bind(1,$member);
	$db->execute();
}
//error_log('============================5====================================');
$db->query("insert into hx_logged_in (id,member_id,session_id,create_ts) values (?,?,?,?)");
$db->bind(1,$thisId);
$db->bind(2,$member);
$db->bind(3,$session);
$db->bind(4,$dateNow);
$db->execute();
//error_log('============================6====================================');

			/////////
	header("location: ".$fullurl."admin/dashboard.php");

		// if(isset($_GET['registration'])){
		// 	header("location: ".$fullurl."profile/age.php");
		// }
		// else {
		// header("location: ".$_SERVER['HTTP_REFERER']."");
		// }

	} else {

	header("location: ".$fullurl."logon.php");
	}





	}



	//logout Section

	if (isset($_GET['action']) && $_GET['action'] == 'logout') {


	$db->query("delete from accounts_persistence where id = ?");
	$db->bind(1,$_SESSION['SESS_PERSIST_ID']);
	$db->execute();


	setcookie("sessKey1", "", time() - 3600);
	setcookie("sessKey2", "", time() - 3600);
	setcookie("sessKey3", "", time() - 3600);

	session_unset();

	session_destroy();



	header("location:/index.php");





	}
