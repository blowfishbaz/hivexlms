<?php
	/**
	 *
	 *
	 *
	 * @Filename    crypt.php
	 * @author     Graham Palfreyman
	 * @copyright  1997-2015 Blowfish Technology Ltd
	 * @version    1
	 * @Date        08/03/2016
	 */


function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function decrypt($encrypted) {


$cipher = mcrypt_module_open(MCRYPT_BLOWFISH,'','cbc','');
$key = '23534534645764535643756778789789567';
$iv = '15468546';
$en = hex2bin($encrypted);

mcrypt_generic_init($cipher, $key, $iv);
$decrypted = mdecrypt_generic($cipher,$en);
mcrypt_generic_deinit($cipher);
$result = $decrypted;
$result = preg_replace('/[^(\x20-\x7F)]*/','', $result);
return $result;
}


function encrypt($encrypt) {
$cipher = mcrypt_module_open(MCRYPT_BLOWFISH,'','cbc','');
$key = '23534534645764535643756778789789567';
$iv = '15468546';

mcrypt_generic_init($cipher, $key, $iv);
$encrypted = mcrypt_generic($cipher,$encrypt);
mcrypt_generic_deinit($cipher);

$result = bin2hex($encrypted);
return $result;
}




function decryptID($encrypted) {


$cipher = mcrypt_module_open(MCRYPT_BLOWFISH,'','cbc','');
$key = '384753986349890238734908jdfhg3985kjdghdfkgj9759345675';
$iv = '34753834';
$en = hex2bin($encrypted);

mcrypt_generic_init($cipher, $key, $iv);
$decrypted = mdecrypt_generic($cipher,$en);
mcrypt_generic_deinit($cipher);
$result = $decrypted;
$result = preg_replace('/[^(\x20-\x7F)]*/','', $result);

$result = substr($result, 10);
$result = substr($result, 0, -10);

return $result;
}


function encryptID($encrypt) {
$cipher = mcrypt_module_open(MCRYPT_BLOWFISH,'','cbc','');
$key = '384753986349890238734908jdfhg3985kjdghdfkgj9759345675';
$iv = '34753834';
$string1 = generateRandomString();
$string2 = generateRandomString();
$encrypt = $string1.$encrypt.$string2;
mcrypt_generic_init($cipher, $key, $iv);
$encrypted = mcrypt_generic($cipher,$encrypt);
mcrypt_generic_deinit($cipher);

$result = bin2hex($encrypted);
return $result;
}



?>
