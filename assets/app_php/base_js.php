	<!-- jQuery -->
  <script src="<? echo $fullurl ?>assets/js/jquery.min.js"></script>
  <!-- Base Plugins -->
  <script src="<? echo $fullurl ?>assets/js/bootstrap.min.js"></script>
  <script src="<? echo $fullurl ?>assets/js/material.js"></script>
  <script src="<? echo $fullurl ?>assets/js/ripples.js"></script>

  <script src="<? echo $fullurl ?>assets/js/jquery.dropdown.js"></script>
  <!-- Datatables -->
  <script src="<? echo $fullurl ?>assets/js/bootstrap-table.js"></script>
  <script src="<? echo $fullurl ?>assets/js/tableExport.js"></script>
	<script src="<? echo $fullurl ?>assets/js/table-extensions/export/bootstrap-table-export.js"></script>
	<!-- Messenger -->
	<script src="<? echo $fullurl ?>assets/js/messenger/messenger.js"></script>
	<!-- Summernote -->
	<script src="<? echo $fullurl ?>assets/js/summernote.js"></script>
	<!-- DatetimePicker -->
	<script src="<? echo $fullurl ?>assets/js/moment.js"></script>
	<script src="<? echo $fullurl ?>assets/js/bootstrap-material-datetimepicker.js"></script>
  <script src="<? echo $fullurl ?>assets/js/bootstrap-datetimepicker.min.js"></script>

	<!-- Calendar -->
	<script src="<? echo $fullurl ?>assets/js/fullcalendar.js"></script>

	<!-- Bootbox -->
	<script src="<? echo $fullurl ?>assets/js/bootbox.min.js"></script>

<!-- Base INITS -->
	<script src="<? echo $fullurl ?>assets/js/base.js"></script>

	<!-------------------------------- uploader --------------------------------->
	<!-------------------------------- uploader --------------------------------->
	<!-------------------------------- uploader --------------------------------->


	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="<? echo $fullurl; ?>assets/js/load-image.all.min.js"></script>


	<script src="<? echo $fullurl; ?>assets/js/jquery.ui.widget.js"></script>

	<script src="<? echo $fullurl; ?>assets/js/tmpl.min.js"></script>


	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="<? echo $fullurl; ?>assets/js/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.blueimp-gallery.min.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-process.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-image.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-audio.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-video.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-validate.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-ui.js"></script>

  <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}

    console.dir(files);

        <tr class="template-upload fade{%=o.options.loadImageFileTypes.test(file.type)?' image':''%}">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">{%=file.name%}</p>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
                  <button class="btn btn-success edit" data-index="{%=i%}" disabled>
                      <i class="glyphicon glyphicon-edit"></i>
                      <span>Edit</span>
                  </button>
                {% } %}
                {% if (!i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start" disabled>
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
  </script>
  <!-- The template to display files available for download -->
  <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade{%=file.thumbnailUrl?' image':''%}">
            <td>
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
            console.dir(file.deleteUrl);
                {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Delete</span>
                    </button>
                    <input type="checkbox" name="delete" value="1" class="toggle">
                {% } else { %}
                    <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
  </script>


  <!-- Base INITS -->
  	<script src="<? echo $fullurl ?>assets/js/backend.js"></script>

    <!-- Base admin -->
  <? if($_SESSION['SESS_ACCOUNT_Type'] == 'admin'){?>
	   <script src="<? echo $fullurl ?>assets/js/admin.js"></script>
  <?}?>

	<!-------------------------------- uploader --------------------------------->
	<!-------------------------------- uploader --------------------------------->
	<!-------------------------------- uploader --------------------------------->
<script>
var $Loading='<div id="FRMloader"><img src="<? echo $fullurl ?>assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading</div>';
var $Loader='<div id="FRMloader"><img src="<? echo $fullurl ?>assets/images/loaderweb.gif" alt="loader" width="64" height="51" /><br>Loading</div>';


var $fullurl="<? echo $fullurl ?>";

$( "body" ).on( "click", "#dismiss_notification", function() {
  var id = $(this).data('id');
  //$(this).parent().fadeOut();
  $.ajax({
        type: "POST",
        url: $fullurl+"assets/app_ajax/notification_dismiss.php",
        data: {id:id},
        success: function(msg){
          $message=$.trim(msg);




       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occur#b90000 please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
  $(this).parent().fadeOut();
});
</script>
