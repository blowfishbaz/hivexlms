<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    functions.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */




// create an id, requires id type
function createId($idType){
	//generate time code, with millisecondes added
	$now = DateTime::createFromFormat('U.u', microtime(true));
	$idTime = $now->format("Ymd_Hisu");
	//create random number to stop duplicate ids being created
	$idRandom = rand(10000,99999);
	//join time and random
	return 'id_'.$idType.'_'.$idTime.'_'.$idRandom;
}



function getCurrentKey() {


		$key1 = 77;
		$date = new DateTime();
		$key2 = $date->format('YmdH');

		$keytotal = $key1 * $key2;

		$keyoutput = encrypt($keytotal);
	return $keyoutput;
}



function clean($str) {
		$str = strtolower($str);
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
}

function loadProfilePic($imagename) {


	$file = $_SERVER['DOCUMENT_ROOT'].'/'.'uploads/profiles/'.$imagename.'.png';

	if (file_exists($file)) {

	   return 'http://'.$_SERVER['HTTP_HOST']."/uploads/profiles/".$imagename.'.png';
	} else {

	    return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png";
	}

}

function loadProfilePic2($imagename) {


	$file = $_SERVER['DOCUMENT_ROOT'].'/'.'uploads/profiles/'.$imagename.'.png';

	if (file_exists($file)) {

	   return 'http://'.$_SERVER['HTTP_HOST']."/uploads/profiles/".$imagename.'.png';
	} else {

	    return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png";
	}

}
function checklogin($member){
	$session=session_id();

	$db = new database;
	$db->Query("select * from hx_logged_in where member_id = ? and session_id = ?");
	$db->bind(1,$member);
	$db->bind(2,$session);
	$logged_in = $db->single();
	$logged_in_count = $db->rowcount();

	if($logged_in_count==0){

		$db->query("delete from ws_accounts_persistence where id = ?");
		$db->bind(1,$member);
		$db->execute();

		$thisId = createId('klog');
		$dateNow = time();

		if($member!=''){
				return 'not login';
			}
			else{
				return 'login';
			}
	}
}

function getCookiePermissions($id) {

			//$id = decrypt($id);
			$pa = array();

			// $db = new database;
			// $db->query("select * from hx_permissions where staffId = ?");
			// $db->bind(1,$id);
			// $permissionsdb = $db->resultset();
			//
			// foreach ($permissionsdb as $p) {
			// 	array_push($pa, $p['area']);
			// }

			$db = new database;
			$db->query("select * from hx_perm where user_id = ? and status = 1");
			$db->bind(1,$id);
			$permissionsdb = $db->resultset();

			foreach ($permissionsdb as $p) {
				array_push($pa, $p['perm_id']);
			}

			return $pa;
}


function checkCookie() {

	if (isset($_COOKIE['sesskey1']) && isset($_COOKIE['sesskey2']) && isset($_COOKIE['sesskey3'])) {

		$salt1 = "jskdhfskjdhfsdklf_hdsjklahgdjkl-sjhfgdkfjhgslkdghdfkj_ghjsdklfhdfjkghjd-lkfghjkdjfhgouwhkjdf093845dhjkvsd";
		$salt2 = "dskfjh38434785348564idfghdkfjhg938457347856_sjdkhfgdjkghdfjkgh--dlkfgjdkfgjdf_dfglkhdfkgjhdfkj";

		$key1 = decrypt($_COOKIE['sesskey1']);
		$key1 = $salt1.$key1.$salt2;
		$key1 = sha1($key1);

		$key2 = decrypt($_COOKIE['sesskey2']);
		$key2 = $salt1.$key2.$salt2;
		$key2 = sha1($key2);

		$key3 = decrypt($_COOKIE['sesskey3']);
		$key3 = $salt1.$key3.$salt2;
		$key3 = sha1($key3);


		$db = new database;
		$db->query("select * from accounts_persistence where key1 = ? and key2 = ? and key3 = ?");
		$db->bind(1,$key1);
		$db->bind(2,$key2);
		$db->bind(3,$key3);
		$member = $db->single();
		$count = $db->rowcount();

		if ($count >= 1) {
			$howLongCookie = CookieLength;
			$lastlogon = $member['lastlogon'];

			$date = date('Y-m-d H:i:s');
			$expire_stamp = strtotime($lastlogon) + (60*$howLongCookie);
			$date = strtotime($date);
			if ($date <= $expire_stamp) {
				//Auto Logon
				setcookie("sesskey1", "", time() - 3600);
				setcookie("sesskey2", "", time() - 3600);
				setcookie("sesskey3", "", time() - 3600);
				$db->query("delete from accounts_persistence where id = ?");
				$db->bind(1,$member['id']);
				$db->execute();
				//$complete = SetSessions($member['userId'],1);

				return $complete;


			} else {

//add relogin here//

//create session variables and cookies.
//$db = new database;
//Get Account
$db->query("select id,first_name, surname, email, type from ws_accounts where id = ?");
$db->bind(1,$member['userid']);
$account = $db->single();

//Get Permissions

$_SESSION['SESS_ACCOUNT_PERMISSIONS'] = getCookiePermissions($account['id']);
$_SESSION['SESS_ACCOUNT_ID'] = encrypt($account['id']);
$_SESSION['SESS_ACCOUNT_Type'] = $account['type'];
$_SESSION['SESS_ACCOUNT_NAME'] = $account['screenname'];
$_SESSION['SESS_EMAIL'] = $account['email'];
$_SESSION['SESS_PROFILEPIC'] = $account['profilepic'];


$db->query("delete from hx_logged_in where member_id = ?");
$db->bind(1,$account['id']);
$db->execute();


$thisId = createId('log');
$dateNow = time();
$session=session_id();

$db->query("insert into hx_logged_in (id,member_id,session_id,create_ts) values (?,?,?,?)");
$db->bind(1,$thisId);
$db->bind(2,$account['id']);
$db->bind(3,$session);
$db->bind(4,$dateNow);
$db->execute();




/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////
// if($account['menuid']!=''){
// 		$db = new database;
// 		$db->query("select * from hx_menu where id = ?");
// 		$db->bind(1,$account['menuid']);
// 		$menu = $db->single();
// 		$_SESSION["MENU"] = $menu['typeof'];
// }
// else{
// 		$_SESSION["MENU"] = 'default';
// }
$_SESSION["MENU"] = 'default';
/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////

				return 0;
			}


		} else {
			$thisId = createId('klog');
			$dateNow = time();

			// $db->query("insert into hx_stat_kicked (id,userid,create_ts) values (?,?,?)");
			// $db->bind(1,$thisId);
			// $db->bind(2,$account['id']);
			// $db->bind(3,$dateNow);
			// $db->execute();

			session_destroy();
			return 0;
		}

	} else {

		session_destroy();
		return 0;
	}

}


function checkPerm($loc){



				switch ($loc) {
	          case "calendar":
	                    $l = 'crm_1';
	                    break;
	          case "communications":
	                    $l = 'crm_2';
	                    break;
	          case "contacts":
	                    $l = 'crm_3';
	                    break;
	          case "customers":
	                    $l = 'crm_4';
	                    break;
	          case "dashboard":
	                    $l = 'crm_5';
	                    break;
	          case "holidays":
	                    $l = 'crm_6';
	                    break;
						case "jobs":
											$l = 'crm_7';
											break;
						case "newsletter":
											$l = 'crm_8';
											break;
						case "quotes":
											$l = 'crm_9';
											break;
						case "reports":
											$l = 'crm_10';
											break;
						case "targets":
											$l = 'crm_11';
											break;
						case "todo":
											$l = 'crm_12';
											break;
						case "users":
											$l = 'crm_13';
											break;
							case "stats":
                    	$l = 'bft_1';
                      break;
	          default:
	                    $l = 'error';
	}

	$url = $_SERVER[REQUEST_URI];

	if (in_array($l, $_SESSION["SESS_ACCOUNT_PERMISSIONS"])) {}
	else{
		error_log('http://'.$_SERVER['HTTP_HOST'].'/application/index.php');
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/application/index.php ');
		die();
	};

}




function loadProfilePicwithID($id) {

	$db = new database;
	$db->query("select ws_uploads.path,ws_uploads.name from accounts join ws_uploads on accounts.profilepic=ws_uploads.id where accounts.id = ?");
	$db->bind(1,$id);
	$img = $db->single();


	$lowres = 'http://'.$_SERVER['HTTP_HOST'].'/img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';

	if ($img['name']!='') {
	   return $lowres;
	} else {
	    //return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png?id=".$id;
				return '<span class="my_profile_icon glyphicons glyphicons-user" data-holder-rendered="true"></span>';
	}

}

// function loadProfilePicwithID($id) {
//
// 	$db = new database;
// 	$db->query("select * from accounts where id = ?");
// 	$db->bind(1,$id);
// 	$data = $db->single();
// 	$imagename = $data['profilepic'];
//
// 	$file = $_SERVER['DOCUMENT_ROOT'].'/'.'uploads/profiles/'.$imagename.'.png';
//
// 	if (file_exists($file)) {
// 	   return 'http://'.$_SERVER['HTTP_HOST']."/uploads/profiles/".$imagename.'.png';
// 	} else {
// 	    return 'http://'.$_SERVER['HTTP_HOST']."/assets/images/profileplaceholder.png";
// 	}
//
// }

function getAccountName($id) {

	$db = new database;
	$db->query("select * from accounts where id = ?");
	$db->bind(1,$id);
	$data = $db->single();

	return $data['name'];

}



function datetimeformat( $ptime ) {

	 $output = date('d F Y - H:i', $ptime);
	    $output = str_replace('-', 'at', $output);
	    return $output;
}


	function timeago( $ptime )
{
    $estimate_time = time() - $ptime;

    if( $estimate_time < 1 )
    {
        return 'less than 1 second ago';
    }

    $condition = array(
                12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    //echo $estimate_time;
    if ($estimate_time >= 86400) {

	    $output = date('d F Y - H:i', $ptime);
	    $output = str_replace('-', 'at', $output);
	    return $output;
    } else {

	   foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;

        if( $d >= 1 )
        {
            $r = round( $d );
            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
        }
    }


    }

    }

////////////////att2db///////////////////////
////////////////att2db///////////////////////
		/*  $tble = 'hx_notes_attachments';
		$idPre = 'att_notx';
		$pid = $_POST["pid"];
		$type = $_POST["type"];
		$name = $_POST["name"];
		$compId = decryptID($_GET["id"]);
		$db = new database;

		att2db($db,$tble,$idPre,$pid, $compId,$type,$name);
		*/
////////////////att2db///////////////////////
////////////////att2db///////////////////////


		function att2db($thisId,$db,$tble,$idPre,$pid,$compId,$type,$name) {


		            $dateNow = time();

		            $db->Query("insert into ".$tble." (id,pid,company_id,type,name,created_date,created_by) values (?,?,?,?,?,?,?)");
		            $db->bind(1,$thisId);
		            $db->bind(2,$pid);
		            $db->bind(3,$compId);
		            $db->bind(4,$type);
		            $db->bind(5,$name);
		            $db->bind(6,$dateNow);
		            $db->bind(7,$_SESSION['SESS_ACCOUNT_NAME']);
		            $db->execute();


								echo 'complete';


		}

/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////
	function okExts() {
		$docExts = array("pdf","doc","dot","docx","dotx","docm","dotm","xls","xlt","xla","xlsx","xltx","xlsm","xltm","xlam","xlsb","ppt","pot","pps","ppa","pptx","potx","ppsx","ppam","pptm","potm","ppsm","odt","ott","odp","otp","ods","ots","gif","png","jpe?g");
		$exList = '';
			foreach($docExts as $ext){
				$exList .= $ext . '|'. strtoupper($ext).'|';
			}
		return rtrim($exList, "|");
	}
/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////

/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////
	function okImageExts() {
		$docExts = array("gif","png","jpe?g");
		$exList = '';
			foreach($docExts as $ext){
				$exList .= $ext . '|'. strtoupper($ext).'|';
			}
		return rtrim($exList, "|");
	}
/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////

/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////
	function okFileExts() {
		$docExts = array("pdf","doc","dot","docx","dotx","docm","dotm","odt","ott","odp","otp","ods","ots");
		$exList = '';
			foreach($docExts as $ext){
				$exList .= $ext . '|'. strtoupper($ext).'|';
			}
		return rtrim($exList, "|");
	}
/////////////////////////////////////////////////
////////exceptable file extensoins///////////////
/////////////////////////////////////////////////
	function getFileIcon($myfile) {

	$docMimes = array("application/pdf","application/msword","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.template","application/vnd.ms-word.document.macroEnabled.12", "application/vnd.ms-word.template.macroEnabled.12","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.text-template");
	$spreadMimes = array("application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.spreadsheetml.template","application/vnd.ms-excel.sheet.macroEnabled.12","application/vnd.ms-excel.template.macroEnabled.12","application/vnd.ms-excel.addin.macroEnabled.12","application/vnd.ms-excel.sheet.binary.macroEnabled.12","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.spreadsheet-template");
	$presMimes = array("application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.presentationml.template","application/vnd.openxmlformats-officedocument.presentationml.slideshow","application/vnd.ms-powerpoint.addin.macroEnabled.12","application/vnd.ms-powerpoint.presentation.macroEnabled.12","application/vnd.ms-powerpoint.template.macroEnabled.12","application/vnd.ms-powerpoint.slideshow.macroEnabled.12","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.presentation-template");

								if (in_array($myfile, $docMimes)) {
								    return "filetypes filetypes-doc";
								}
								elseif (in_array($myfile, $spreadMimes)) {
								    return "filetypes filetypes-xls";
								}
								elseif(in_array($myfile, $presMimes)) {
								    return "filetypes filetypes-ppt";
								}
 							else{
								     return "falsexxxx";
								}
	}





		// example code

		//$typesOfFiles = array("images", "docs");


	//	$docMimes = array("application/msword","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.template","application/vnd.ms-word.document.macroEnabled.12", "application/vnd.ms-word.template.macroEnabled.12","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.spreadsheetml.template","application/vnd.ms-excel.sheet.macroEnabled.12","application/vnd.ms-excel.template.macroEnabled.12","application/vnd.ms-excel.addin.macroEnabled.12","application/vnd.ms-excel.sheet.binary.macroEnabled.12","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.presentationml.template","application/vnd.openxmlformats-officedocument.presentationml.slideshow","application/vnd.ms-powerpoint.addin.macroEnabled.12","application/vnd.ms-powerpoint.presentation.macroEnabled.12","application/vnd.ms-powerpoint.template.macroEnabled.12","application/vnd.ms-powerpoint.slideshow.macroEnabled.12","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.text-template","application/vnd.oasis.opendocument.text-web","application/vnd.oasis.opendocument.text-master","application/vnd.oasis.opendocument.graphics","application/vnd.oasis.opendocument.graphics-template","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.presentation-template","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.spreadsheet-template","application/vnd.oasis.opendocument.chart","application/vnd.oasis.opendocument.formula","application/vnd.oasis.opendocument.database","application/vnd.oasis.opendocument.image","application/vnd.openofficeorg.extension");


	function recursive_array_search($needle,$haystack) {
	    foreach($haystack as $key=>$value) {
	        $current_key=$key;
	        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
	            return $current_key;
	        }
	    }
	    return false;
	}


//Get table and return a single record.
	function getSingleRecord($id) {

		$data = explode('_',$id);
		$codeid = $data[1];

		$tables = array(
		    0 => array(
		        'code' => 'err',
		        'table' => 'null'
		    ),
		    1 => array(
		        'code' => 'comx',
		        'table' => 'hx_communications'
		    ),
		    2 => array(
		        'code' => 'acx',
		        'table' => 'hx_activity'
		    ),
		    3 => array(
		        'code' => 'chx',
		        'table' => 'hx_customers'
		    ),
		    4 => array(
		        'code' => 'phx',
		        'table' => 'hx_contacts'
		    ),
		    5 => array(
		        'code' => 'rem',
		        'table' => 'hx_reminders'
		    ),
		    6 => array(
		        'code' => 'adx',
		        'table' => 'hx_addresses'
		    ),
		    7 => array(
		        'code' => 'member',
		        'table' => 'accounts'
		    ),
		    8 => array(
		        'code' => 'notx',
		        'table' => 'hx_notes'
		    ),



		);

		$codeid = recursive_array_search($codeid,$tables);
		$tableSearch = $tables[$codeid]['table'];

		if ( $tableSearch == 'null') {
			return null;
		} else {
			$db = new database;
			$db->Query("select * from ".$tableSearch." where id = ?");
			$db->bind(1,$id);
			$data = $db->single();
			}


		return $data;

	}


	/////////////////////////////////////////////////
	///////////////////Status selecter///////////////
	/////////////////////////////////////////////////

	function job_area($status) {
		switch ($status) {
							case '1':$section='Add Lead';break;
							case '2':$section='EPC Booking Team';break;
							case '3':$section='EPC Confirmation';break;
							case '4':$section='EPC Accessor Que';break;
							case '5':$section='Compliance';break;
							case '6':$section='EPC Reference';break;
							case '7':$section='Price';break;
							case '8':$section='Contribution';break;
							case '9':$section='Book Technical Survey';break;
							case '10':$section='Technical Survey';break;
							case '11':$section='Technical Survey Review';break;
							case '12':$section='Install Booking Team';break;
							case '13':$section='Install Confirmation';break;
							case '14':$section='Installation';break;
							case '15':$section='Technical Monitoring';break;
							case '16':$section='Technical Monitoring Visit';break;
							case '19':$section='Send Compliance';break;
							case '20':$section='Completed';break;
							case '21':$section='Boiler Builds';break;
							case 'end':$section='Untill End';break;
							default:$section='Error';break;
					}
				return $section;
	}

		function job_area_fix($status) {
			switch ($status) {
								case '1':$section='Add Fix';break;
								case '2':$section='Book Fix';break;
								case '3':$section='Fix';break;
								case '4':$section='Complete Fix';break;
								default:$section='Error';break;
						}
					return $section;
		}


function job_status($v){
     switch ($v) {
          case '0':return 'Killed';break;
          case '1':return 'Live';break;
          case '2':return 'Pause';break;
          case '3':return 'Completed';break;
          default:return 'Error_'.$v;break;
     }
}

function job_type($v){
     switch ($v) {
          case 'rst_install':return'Install';break;
          case 'rst_fix':return 'Fix';break;
					case 'tm_elect':return'TM Electrician';break;
					case 'electrician':return'Electrician Fix';break;
					case 'heating':return'Heating Fix';break;
					case 'loft':return'Loft';break;
					case 'boiler service':return'Boiler Service';break;


          default:return'Error_404_'.$v;break;
    }
}

function appointment_status($v){
			switch ($v) {
case '0':return 'Deleted';break;
case '1':return 'Due';break;
case '2':return 'Waiting Confirmation';break;
case '3':return 'On Hold';break;
case '4':return 'Completed';break;
default:return 'Error_'.$v;break;
			}
 }

function appointment_type($v){
switch ($v) {
case 'epcbook':return'EPC Booking';break;
case 'tsbook':return 'Technical Survey Booking';break;
case 'inbook':return'Install Booking';break;
case 'elbook':return'Electrical Booking';break;
case 'tmbook':return'Technical Monitoring Booking';break;
case 'fixbook':return'Fix Booking';break;
case 'loftbook':return'Loft Booking';break;
default:return'Error_'.$v;break;
		 }
}

function installer_type($v){
     switch ($v) {
case 'tsbook':return 'JobSurvey';break;
case 'inbook':return'JobInstall';break;
case 'elbook':return'JobElectrician';break;
case 'tmbook':return'JobTechMonitoring';break;
case 'fixbook':return'JobInstall';break;
case 'loftbook':return'JobInstall';break;
default:return'Error';break;
          }
}

/////////////////////////////////////////////////
///////////////////Status selecter///////////////
/////////////////////////////////////////////////



/////////////////////////////////////////////////
///////////////////new permossions///////////////
/////////////////////////////////////////////////

/////default menu
//    $menu = 'menu';
/////default menu


/*
2 – write
4 – read
*/
/*$sections = array("crm", "sales", "marketing", "products","hr","reports","usermanage","datamanage","admin");#
$area = array("quote", "bar", "hello", "world");
$pages = array("index", "bar", "hello", "world");

$z = array(
        array(
        'used' => 0,
        'section' => 'sales',
        'area'=>'quotes',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'sales',
        'area'=>'orders',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'marketing',
        'area'=>'emailist',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'marketing',
        'area'=>'campaign',
        'page' => 'index'
        ),
        array(
        'used' => 1,
        'section' => 'sales',
        'area'=>'quotes',
        'page' => 'index'
        ),
        array(
        'used' => 0,
        'section' => 'sales',
        'area'=>'quotes',
        'page' => 'index'
        )
  );


	foreach($allPages as $pages){


	    if($pages['used'] == 1 && in_array($pages['section'], $sections)){

	        echo '+'.$pages['section'];
	        echo '</br>';
	        echo '-'.$pages['area'];
	        echo '</br>';


	    }



	}

	echo '<hr>';

	foreach($allPages as $pages){


	    if($pages['used'] == 1 ){

	        echo '+'.$pages['section'];
	        echo '</br>';
	        echo '-'.$pages['area'];
	        echo '</br>';


	    }



	}





*/


/////////////////////////////////////////////////
///////////////////new permossions///////////////
/////////////////////////////////////////////////


function stage_staus($v) {
	switch ($v) {
		case '1':
			return 'Proposal';
			break;
		case '2':
			return 'Proposal / Commission';
			break;
		case '3':
			return 'Error 3';
			break;
		case '4':
			return 'Commissioned';
			break;
		case '5':
			return 'Cold';
			break;

		default:
			return 'Error';
			break;
	}
}

function rate_type($v){
 switch ($v) {
  case 'hourly_rate':
   return "Hourly Rate";
   break;
   case 'hourly_rate_capped':
    return "Hourly Rate Capped (£)";
    break;
    case 'hourly_rate_capped_hours':
     return "Hourly Rate Capped (Hrs)";
     break;
  case 'fixed_rate':
   return "Fixed Fee";
   break;
  case 'budget_range':
   return "Budget Fee";
   break;
		case 'flexi_fee':
			return "Flexi Fee";
			break;
   case 'N/A':
    return "N/A";
    break;
   case '':
 			return "No Rate Selected";
 			break;
  default:
   return 'Error';
   break;
 }
}

function department_status($v){
		switch ($v) {
			case '0':
				return '<span class="label label-default" style="padding:5px;">N/A</span>';
				break;
			case '1':
				return '<span class="label label-info" style="padding:5px;">Proposal</span>';
				break;
			case '2':
				return '<span class="label label-success" style="padding:5px;">Commissioned</span>';
				break;
			case '3':
				return '<span class="label label-warning" style="padding:5px;">Cold</span>';
				break;
   case '4':
    return '<span class="label" style="padding:5px;">Hold</span>';
    break;
			default:
				return '<span class="label label-danger" style="padding:5px;">Error</span>';
				break;
		}
}


 function first_letter($v){
     if(!empty($v)){
         $arr = explode(' ',trim($v));
         if($arr[0] == 'Mileage'){
             return 'Mileage';
         }else{
             return '';
         }
     }else{
         return '';
     }
 }

 function mileage($v){
     $arr = array('id_exp_20180604_140653199700_64813','id_exp_20180604_140702389300_98092','id_exp_20180604_140803577200_83171','id_exp_20180604_140817698600_91445');
     if(in_array($v,$arr)){
         return 'Mileage';
     }else{
         return '';
     }
 }

 function proposal_status($v){
   switch ($v) {
    case '1':
          return 'Awaiting Allocation';
    break;
		case '1.5':
          return 'Qualification';
    break;
    case '2':
    						return 'Awaiting Proposal';
    break;
    case '3':
    						return 'Awaiting Peer Review';
    break;
    case '4':
    						return 'Awaiting Proof Read';
    break;
    case '5':
    						return 'Ready to Send';
    break;
    case '6':
    						return 'Proposal Sent';
    break;
		case '7':
								return 'Proposal Follow Up';
		break;
    default:
     return 'Error - :(';
     break;
   }
 }

 function commission_status($v){
  switch ($v) {
   case '1':
    return 'Awaiting Allocation';
    break;
			case '2':
				return 'Work in Progress';
				break;
				case '3':
					return 'Completed';
					break;
   default:
   	return 'Error - :(';
    break;
  }
 }

 function month($v){
  switch ($v) {
   case '1':
         return 'January';
   break;
   case '2':
         return 'February';
   break;
   case '3':
         return 'March';
   break;
   case '4':
         return 'April';
   break;
   case '5':
         return 'May';
   break;
   case '6':
         return 'June';
   break;
   case '7':
         return 'July';
   break;
   case '8':
         return 'August';
   break;
   case '9':
         return 'September';
   break;
   case '10':
         return 'October';
   break;
   case '11':
         return 'November';
   break;
   case '12':
         return 'December';
   break;
   default:
    return 'Error - :(';
    break;
  }
 }

 function perm_checker($perm1){
 if(in_array($perm1, $_SESSION['SESS_ACCOUNT_PERMISSIONS'])){
  return 'yes';
 }else{
   return 'nope';
 }
}

function ExpensesStatus($s){
 switch ($s) {
	 case '0':
 				return 'Request Rejected';
 	break;
	case '1':
				return 'Waiting Approval';
	break;
	case '2':
				return 'Approved';
	break;
	case '3':
				return 'Expense Sent';
	break;
	case '4':
				return 'Expense Paid';
	break;
	default:
	 return 'Error - :(';
	 break;
 }
}
	//////////////
	//add activity
	//////////////
	function add_activity($activity_type,$activity_area,$company_id,$item_id){
			$created_date = time();
			$activity_by =decrypt($_SESSION['SESS_ACCOUNT_ID']);
			$act_id=createid('acx');
			$page=$_SERVER['HTTP_REFERER'];

			 	$db = new database;
			  $db->query("insert into hx_activity (id, activity_type, activity_area, activity_by, created_date, company_id, item_id,page) values (?,?,?,?,?,?,?,?)");
			  $db->bind(1,createid('acx'));
			  $db->bind(2,$activity_type);
			  $db->bind(3,$activity_area);
			  $db->bind(4,$activity_by);
			  $db->bind(5,$created_date);
			  $db->bind(6,$company_id);
			  $db->bind(7,$item_id);
				$db->bind(8,$page);
			  $db->execute();


			return 'complete';
	}

	function add_activity_pid($activity_type,$activity_area,$company_id,$item_id,$pid){
			$created_date = time();
			$activity_by =decrypt($_SESSION['SESS_ACCOUNT_ID']);
			$act_id=createid('acx');
			$page=$_SERVER['HTTP_REFERER'];
			$picked_date=strtotime ($date);

				$db = new database;
				$db->query("insert into hx_activity (id, activity_type, activity_area, activity_by, created_date, company_id, item_id,page, pid) values (?,?,?,?,?,?,?,?,?)");
				$db->bind(1,createid('acx'));
				$db->bind(2,$activity_type);
				$db->bind(3,$activity_area);
				$db->bind(4,$activity_by);
				$db->bind(5,$created_date);
				$db->bind(6,$company_id);
				$db->bind(7,$item_id);
				$db->bind(8,$page);
				$db->bind(9,$pid);
				$db->execute();


			return 'complete';
	}

	function add_activity_date($activity_type,$activity_area,$company_id,$item_id,$date,$pid){
			$created_date = time();
			$activity_by =decrypt($_SESSION['SESS_ACCOUNT_ID']);
			$act_id=createid('acx');
			$page=$_SERVER['HTTP_REFERER'];
			$picked_date=strtotime ($date);

				$db = new database;
				$db->query("insert into hx_activity (id, activity_type, activity_area, activity_by, created_date, company_id, item_id,page, pick_date,pid) values (?,?,?,?,?,?,?,?,?,?)");
				$db->bind(1,createid('acx'));
				$db->bind(2,$activity_type);
				$db->bind(3,$activity_area);
				$db->bind(4,$activity_by);
				$db->bind(5,$created_date);
				$db->bind(6,$company_id);
				$db->bind(7,$item_id);
				$db->bind(8,$page);
				$db->bind(9,$picked_date);
				$db->bind(10,$pid);
				$db->execute();


			return 'complete';
	}
	//////////////
	//add activity
	//////////////

 function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

 function add_notification($user, $pid, $title, $message, $type) {

	 $thisId = createId('not');
	 $now = time();

	 $db = new database;
	 $db->query("insert into bv_notifications (id,created_for,pid,title,message,type,created_date) values (?,?,?,?,?,?,?)");
	 $db->bind(1,$thisId);
	 $db->bind(2,$user);
	 $db->bind(3,$pid);
	 $db->bind(4,$title);
	 $db->bind(5,$message);
	 $db->bind(6,$type);
	 $db->bind(7,$now);
	 $db->execute();
 }
?>
