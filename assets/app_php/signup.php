<?
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    auth.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


	include ($_SERVER["DOCUMENT_ROOT"] .'/application.php');
	 include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');
	function random_string()
{
	// 8 characters: 7 lower-case alphabets and 1 digit
	$character_set_array = array();
	$character_set_array[] = array('count' => 22, 'characters' => 'abcdefghijklmnopqrstuvwxyz');
	$character_set_array[] = array('count' => 22, 'characters' => '0123456789');
	$temp_array = array();
	foreach ($character_set_array as $character_set) {
			for ($i = 0; $i < $character_set['count']; $i++) {
					$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
			}
	}
	shuffle($temp_array);
	return implode('', $temp_array);
}



if($_POST['username']!='' && $_POST['first_name']!='' && $_POST['surname']!='' && $_POST['email']!='' && $_POST['age']!='' && $_GET["type"]!=''){




					// $db = new Database;
					// $db->Query('insert into hh_new_tbl (id, fn, sn, createdate, act_ref, email) values (?,?,?,?,?,?)');
					// $db->bind(1, $poiid);
					// $db->bind(2, strtolower ($_POST['appfn']));
					// $db->bind(3, strtolower ($_POST['appsn']));
					// $db->bind(4, $createddate);
					// $db->bind(5, $rand);
					// $db->bind(6, strtolower ($_POST['appe']));
					// $db->execute();


					$newid = createId('signup');
					$now = time();
					$random = random_string();
					$username=$_POST["username"];
					$first_name=$_POST["first_name"];
					$surname=$_POST["surname"];
					$email=$_POST["email"];
					$age=$_POST["age"];
					$type=$_GET["type"];

					$db = new Database;
					$db->Query('insert into bv_signup (id,created_date, username, first_name, surname, email, type, age,status,random_string) values (?,?,?,?,?,?,?,?,?,?)');
					$db->bind(1, $newid);
					$db->bind(2, $now);
					$db->bind(3, $username);
					$db->bind(4, $first_name);
					$db->bind(5, $surname);
					$db->bind(6, $email);
					$db->bind(7, $type);
					$db->bind(8, $age);
					$db->bind(9, '1');
					$db->bind(10, $random);
					$db->execute();


					           $subject='Confirm BlueVoyeurs Account';

					           $head='';
                $body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #dedede;margin-top: -18px;"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--<![endif]--><div class="wrapper" ><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#dedede;" align="center"> <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="display: block;border: 0;max-width: 900px;" src="https://www.bluevoyeurs.com/imgs/email4.png" alt="" width="600" height="300"> </div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;text-align:center;padding: 5px;background-color: #194d75;color: white;"><h1>Welcome!</h1></div></div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;">';

					           $body2='Hi '.ucfirst ($first_name).', Welcome To Blue Voyeurs, to confirm your account either by <a href="'.$fullurl.'index.php?ref='.$random.'&signup"> clicking here </a> or copy and pasting the following link into the address bar of your browser:'.$fullurl.'index.php?ref='.$random.'&signup</p>';
					           $body3='';

                $footer='<!-- Callout Panel --></div><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:5px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"> </div><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:5px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;"><div class="divider" style="display: block;font-size: 2px;line-height: 2px;width: 40px;background-color: #c9c2a8;Margin-left: 260px;Margin-right: 260px;">&nbsp;</div></div></td></tr></tbody></table><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;"><a href="https://twitter.com/@bluevoyeurs"><span class="fa fa-twitter" style="padding: 5px; font-size: 15px; width: 15px; text-align: center; text-decoration: none; margin: 5px 2px; border-radius: 50%; background: #55ACEE; color: white;"></span></a><a href="https://www.bluevoyeurs.com/"><span class="fa fa-link" style="padding: 5px; font-size: 15px; width: 15px; text-align: center; text-decoration: none; margin: 5px 2px; border-radius: 50%; background: #4caf50; color: white;"></span></a></p><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="width:560px; height:2px; background-color:#dedede;"></div><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><p class="size-16" style="Margin-bottom: 20px;font-size: 10px;line-height: 24px;text-align: center;">Copyright © *|2019|* *|DDAF Services Ltd|*, All rights reserved.</p><!-- Callout Panel --></div></td></tr></tbody></table><table class="footer" style="border-collapse: collapse;table-layout: fixed;Margin-right: auto;Margin-left: auto;border-spacing: 0;width: 560px;" align="center"><tbody><tr><td style="padding: 0 0 40px 0;"><table class="footer__right" style="border-collapse: collapse;table-layout: auto;border-spacing: 0;" align="right"><tbody><tr><td class="footer__inner" style="padding: 0;"> </td></tr></tbody></table><table class="footer__left" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;color: #b3b3b3;font-family: &quot;PT Sans&quot;,&quot;Trebuchet MS&quot;,sans-serif;width: 380px;"><tbody><tr><td class="footer__inner" style="padding: 0;font-size: 12px;line-height: 19px;"><div> </div><div class="footer__permission" style="Margin-top: 18px;"> </div></td></tr></tbody></table></td></tr></tbody></table></div></body>';

					           $htmlContent = $head.$body1.$body2.$body3.$footer;
					           /////////////send html mail


					                   $mail = new PHPMailer;
					                   $mail->Host = $emailhost;
					                   $mail->Port = 577;
					                   $mail->setFrom('enquiries@bluevoyeurs.com', 'Blue Voyeurs');
					                   //$mail->addAddress('karen.m@renewablesolutionsteam.co.uk', 'Karen');
					                   //$mail->addAddress('stephen@blowfishtechnology.com', 'HiveX');
					                   $mail->addAddress($email, $name);
					                   $mail->MsgHTML($htmlContent);
					                   $mail->IsHTML(true);
					                   $mail->CharSet="utf-8";
					                   $mail->Subject = $subject;
					                   $mail->Body = $htmlContent;




					                   if(!$mail->send()) {
					                    error_log('email failed');
															header('Location: '.$fullurl.'index.php?error=email&username='.$username.'&fname='.$first_name.'&sname='.$surname.'&email='.$email.'&type='.$type.'');
					                   //msg failed to send.
					                   } else {
															 header('Location: '.$fullurl.'index.php?signup='.$newid.'');
					                   //msg sent
					               }

}

else{
	$username=$_POST["username"];
	$first_name=$_POST["first_name"];
	$surname=$_POST["surname"];
	$email=$_POST["email"];
	$age=$_POST["age"];
	$type=$_GET["type"];
header('Location: '.$fullurl.'/index.php?error=required&username='.$username.'&fname='.$first_name.'&sname='.$surname.'&email='.$email.'&type='.$type.'');
}


?>
