<?
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    auth.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


	include ($_SERVER["DOCUMENT_ROOT"] .'/application.php');

	$db->query("SELECT accounts.*, bv_accounts_info.* FROM accounts LEFT JOIN bv_accounts_info ON accounts.id = bv_accounts_info.pid and bv_accounts_info.status = '1'  where accounts.id = ?");
	$db->bind(1, decrypt($_SESSION['SESS_ACCOUNT_ID']));
	$db->execute();
	$user = $db->single();

	$now = time();
					$id = $user["pid"];
					$type=$user["type"];
$email=$user["email"];

					$first_name=$_POST["first_name"];
					$surname=$_POST["surname"];
					$gender=$_POST["gender"];
					$orientation=$_POST["orientation"];
					$dob=$_POST["dob"];
					$postcode=$_POST["postcode"];
					$region=$_POST["region"];
					$county=$_POST["county"];
					$about=$_POST["about"];

					$likes=$_POST["likeslist"];


// 					 $db->query("update accounts set attempts = ? where id = ?");
// 					 $db->bind(1,'0');
// 					 $db->bind(2,$member['id']);
// 					 $db->execute();
//
// $db->query("insert into accounts (id, username, password, screenname, email, type ,status, attempts, lastattempt,created_date, profilepic) values (?,?,?,?,?,?,?,?,?,?,?)");
// $db->bind(1, $newid);
// $db->bind(2, encrypt($email));
// $db->bind(3, encrypt($password));
// $db->bind(4, $username);
// $db->bind(5, $email);
// $db->bind(6, $type);
// $db->bind(7, '1');
// $db->bind(8, '0');
// $db->bind(9, $now);
// $db->bind(10, $now);
// $db->bind(11, '');
// $db->execute();


	$db->query("update bv_accounts_info set status = ? where pid = ? ");
				$db->bind(1,'0');
				$db->bind(2,$id);
				$db->execute();


$newinfoid = createId('info');
$db->query("insert into bv_accounts_info (id, pid, first_name, surname, type, status, gender ,orientation, dob, postcode,region, county,about,created_date,profilepic1,profilepic2) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1, $newinfoid);
$db->bind(2, $id);
$db->bind(3, $first_name);
$db->bind(4, $surname);
$db->bind(5, $type);
$db->bind(6, '1');
$db->bind(7, $gender);
$db->bind(8, $orientation);
$db->bind(9, $dob);
$db->bind(10, $postcode);
$db->bind(11, $region);
$db->bind(12, $county);
$db->bind(13, $about);
$db->bind(14, $now);
$db->bind(15, '');
$db->bind(16, '');
$db->execute();


$db->query("delete from bv_accounts_likes where pid = ?");
$db->bind(1,$id);
$db->execute();



foreach ($likes as $like) {

	$newinfoid = createId('like');
	$db->query("insert into bv_accounts_likes (id, pid, likes) values (?,?,?)");
	$db->bind(1, $newinfoid);
	$db->bind(2, $id);
	$db->bind(3, $like);
	$db->execute();

}



header('Location: '.$fullurl.'index.php');
?>
