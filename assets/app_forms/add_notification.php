<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 ?>

 <h3>Add Notification</h3>

 <form id="add_notification_form">

    <div class="form-group label-floating is-empty">
      <label for="not_title" class="control-label">Title*</label>
      <input id="not_title" type="text" name="not_title" class="form-control" value="" required>
      <span class="material-input"></span>
    </div>

    <div class="form-group">
        <label class=“”>Notification*</label>
        <textarea name="notification" class="form-control" rows="5" id="notification_information"></textarea>
    </div>

 </form>

 <button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
 <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_notification">Save</button>
