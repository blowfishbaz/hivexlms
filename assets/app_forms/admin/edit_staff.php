<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$page_title = 'Staff';

$id = $_GET['id'];

$db->query("select * from hx_staff where id = ? ");
$db->bind(1,$_GET['id']);
$staff = $db->single();

$db->query("select * from ws_uploads where id = ? ");
$db->bind(1,$staff['image']);
$staffimage = $db->single();
?>
<h3>Staff Profile</h3>
<form id="EditStaffForm" name="EdiStaffForm" method="post" enctype="application/x-www-form-urlencoded">

               <div class="form-group">
                         <label class=“”>Staff Name</label>
                         <input type="text" name="staff_name" class="form-control" id="staff_name" value="<? echo $staff['name'];?>">
                         <input type="hidden" name="staff_id" class="form-control" id="staff_id" value="<? echo $staff['id'];?>">
               </div>

                  <div class="form-group">
                            <label class=“”>Staff Description</label>
                            <textarea name="info" class="form-control" rows="5" id="info"><? echo $staff['info'];?></textarea>

                  </div>
                  <input type="hidden" name="photo_id"  id="photo_id" value="<? echo $staff['image'];?>"/>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <img src="<? echo $fullurl.$staffimage['path'].$staffimage['name'];?>" class="img-responsive">
                  </div>
          </form>

          <form id="fileupload" action="<? echo $fullurl; ?>/assets/app_form/uploads/upload_path.php" method="POST" enctype="multipart/form-data">
          <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
          <div class="row fileupload-buttonbar">
           <div class="col-lg-12">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success btn-raised fileinput-button btn-sm">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Edit files...</span>
                    <input type="file" name="files">
                </span>

           </div>
           <!-- The global progress state -->
           <div class="col-lg-12 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
           </div>
          </div>
          <!-- The table listing the files available for upload/download -->
          <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
          </form>
          <div class="clearfix"></div>
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-warning btn-raised" data-id="<? echo $staff['id'];?>"id="DeleteStaffFormBut">Delete</button>

<button type="button" class="btn btn-primary btn-raised pull-right" id="NewStaffFormBut">Save</button>
<div class="clearfix"></div>

<script>
$('#info').summernote({
height: 300,                 // set editor height
minHeight: null,             // set minimum height of editor
maxHeight: null,             // set maximum height of editor
placeholder:'Staff Description...',
toolbar: [
['font', ['bold', 'italic', 'underline']],
['color', ['color']],
['para', ['ul', 'ol', 'paragraph']],
['table', ['table']],
['insert', ['link', 'hr']],
['help', ['help']]
]
});









//////////okExts();

$(function () {
$('#fileupload').fileupload({
dataType: 'json',
acceptFileTypes: /(\.|\/)(<? echo okImageExts(); ?>)$/i,
maxFileSize: 10000000,
done: function (e, data) {
 att_name =data._response.result['files'][0].name;
                          if(data.textStatus=='success'){

      att_name_url="<? echo $fullurl;?>uploads/site/"+att_name+"";
      $path = 'uploads/site/';
      $name = att_name;
      $type = 'meet the team';

          $.ajax({
          type: "POST",
          url: "../../../assets/app_ajax/uploads/db_upload.php",
          data: {path:$path,name:$name,type:$type},
          success: function(msg){

    $img_id=msg.trim();

    //$('#cv_upload').val($name);
    $('#photo_id').val($img_id);


                                        }
                    });
          }
}


});


$('#fileupload').fileupload()
.bind('fileuploadstart', function(){
// disable submit
})
.bind('fileuploadprogressall', function (e, data) {
if (data.loaded == data.total){
 // all files have finished uploading, re-enable submit

}
})

});
</script>
