<?php

/**
 *
 *
 *
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from accounts where id = ? ");
$db->bind(1,$_GET['id']);
$ac = $db->single();
?>
<h3>New Job</h3>
<form id="NewJobForm" name="NewJobForm" method="post" enctype="application/x-www-form-urlencoded">

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group label-floating is-empty">
                  <label for="company" class="control-label">Company*</label>
                  <input type="text" class="form-control" id="company" name="company" maxlength="100" required="yes"><span class="material-input"></span></div>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group label-floating is-empty">
                  <label for="title" class="control-label">Job Title*</label>
                  <input type="text" class="form-control" id="title" name="title" required="yes"  maxlength="200"><span class="material-input"></span></div>
              </div>

                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                     <div class="form-group" style=" margin: -3px 0 0 0;">
                     <label for="job_category" class="control-label">Job Category*</label>
                     <select name="job_category" class="form-control selectpicker" data-live-search="true" id="job_category" required>
                               <option selected="" style="display:none">&nbsp;</option>
                               <option value="Children"<?if($_POST['job_category']=='Children'){echo ' selected';}?>>Children</option>
                               <option value="Deputy Managers"<?if($_POST['job_category']=='Deputy Managers'){echo ' selected';}?>>Deputy Managers</option>
                               <option value="Outdoor Support Workers"<?if($_POST['job_category']=='Outdoor Support Workers'){echo ' selected';}?>>Outdoor Support Workers</option>
                               <option value="Registered Managers"<?if($_POST['job_category']=='Registered Managers'){echo ' selected';}?>>Registered Managers</option>
                               <option value="Residential Support Workers"<?if($_POST['job_category']=='Residential Support Workers'){echo ' selected';}?>>Residential Support Workers</option>
                               <option value="Team Leaders"<?if($_POST['job_category']=='Team Leaders'){echo ' selected';}?>>Team Leaders</option>
                     </select></div>
                   </div>

                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group" style=" margin: -3px 0 0 0;">
                      <label for="email" class="control-label">Region*</label>
                      <select name="region" class="form-control selectpicker" data-live-search="true" id="region" required>
                                <option selected="" style="display:none">&nbsp;</option>
                                <option value="East Anglia">East Anglia</option>
                                <option value="East Midlands">East Midlands</option>
                                <option value="London">London</option>
                                <option value="North East">North East</option>
                                <option value="North West">North West</option>
                                <option value="Northern Ireland">Northern Ireland</option>
                                <option value="Scotland">Scotland</option>
                                <option value="South West">South West</option>
                                <option value="Wales">Wales</option>
                                <option value="West Midlands">West Midlands</option>
                                <option value="Yorkshire">Yorkshire</option>
                                <option value="UK">UK</option>
                                <option value="Eire">Eire</option>
                                <option value="Europe">Europe</option>
                                <option value="South East">South East</option>

                      </select></div>
                    </div>

                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group" style=" margin: -3px 0 0 0;">
                    <label for="email" class="control-label">County*</label>
                    <select name="county" class="form-control selectpicker" data-live-search="true" id="county" required>
                              <option selected="" style="display:none">&nbsp;</option>
                              <?
                              $db->query("select * from hx_counties where status = '1'");
                              $counties = $db->resultset();
                              foreach ($counties as $county) {
                                echo '<option value="'.$county['name'].'">'.$county['name'].'</option>';
                              }
                              ?>
                    </select></div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group label-floating is-empty">
                      <label for="postcode" class="control-label">Postcode*</label>
                      <input type="text" class="form-control" id="postcode" name="postcode" maxlength="200"><span class="material-input"></span></div>
                  </div>






                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label">Job Type*</label>
                    <div class="radio" style="margin-top: 0;">
                              <label><input type="radio" value="full time" name="jobtype" class="jobtype"><span class="checkbox-material"><span class="check"></span></span>Full Time</label>
                    </div>
                    <div class="radio">
                              <label><input type="radio" value="part time" name="jobtype" class="jobtype"><span class="checkbox-material"><span class="check"></span></span>Part Time</label>
                    </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">Contract Type*</label>
                    <div class="radio" style="margin-top: 0;">
                              <label><input type="radio" value="temporary" name="contracttype" class="contracttype"><span class="checkbox-material"><span class="check"></span></span>Temporary</label>
                    </div>
                    <div class="radio">
                              <label><input type="radio" value="permanent" name="contracttype" class="contracttype"><span class="checkbox-material"><span class="check"></span></span>Permanent</label>s
                    </div>
          </div>


          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <label for="name" class="control-label">Salary*</label>
          <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="radio" style="margin-top: 0;">
                              <label><input type="radio" value="per annum" name="salarytype" class="salarytype"><span class="checkbox-material"><span class="check"></span></span>Per Annum</label>
                    </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="radio" style="margin-top: 0;">
                              <label><input type="radio" value="pro rata" name="salarytype" class="salarytype"><span class="checkbox-material"><span class="check"></span></span>Pro Rata</label>
                    </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="radio" style="margin-top: 0;">
                              <label><input type="radio" value="per hour" name="salarytype" class="salarytype"><span class="checkbox-material"><span class="check"></span></span>Per Hour</label>
                    </div>
          </div>
          </div>

                <div class="form-group label-floating is-empty" style="display:none;">
                  <label for="name" class="control-label">per annum (&pound;)*</label>
                  <input  type="number" min="0" step="1" class="form-control" id="per_annum" name="per_annum"  maxlength="20"><span class="material-input"></span></div>

                  <div class="form-group label-floating is-empty" style="display:none;">
                   <label for="name" class="control-label">Pro Rata (&pound;)*</label>
                   <input  type="number" min="0" step="1" class="form-control" id="pro_rata" name="pro_rata"  maxlength="20"><span class="material-input"></span></div>

                   <div class="form-group label-floating is-empty" style="display:none;">
                    <label for="name" class="control-label">Per Hour (&pound;)*</label>
                    <input  type="number" min="0" step="0.01" class="form-control" id="per_hour" name="per_hour"  maxlength="20"><span class="material-input"></span></div>

             </div>
           </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                    <label for="overview" class="">Job Overview*</label>
                    <textarea class="form-control" id="overview" name="overview" required></textarea></div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="form-group">
                    <label for="duties" class="">Duties*</label>
                    <textarea class="form-control" id="duties" name="duties" required></textarea></div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group" style=" margin: -3px 0 0 0;">
            <label for="email" class="control-label">Post Job*</label>
            <select name="post_job" class="form-control selectpicker" data-live-search="true" id="post_job" required>
                       <option selected="" disabled>Select</option>
                       <option value="Website Only">Website Only</option>
                       <option value="Total Jobs & Website">Total Jobs & Website</option>
            </select></div>
          </div>




          </form>

          <div class="clearfix"></div>
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-primary btn-raised pull-right" id="NewJobFormBut">Save</button>
<div class="clearfix"></div>
