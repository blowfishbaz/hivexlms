<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from accounts where id = ? ");
$db->bind(1,$_GET['id']);
$ac = $db->single();
?>
<h3>Edit User</h3>
<form id="editAccForm" name="editAccForm" method="post" enctype="application/x-www-form-urlencoded">

            <div class="col-md-6 col-sm-6">
              <div class="form-group label-floating">
                <label for="name" class="control-label">Name*</label>
                <input type="text" class="form-control" id="name" name="name" required="yes" value="<? echo $ac['name']; ?>"><span class="material-input"></span></div>
               </div>
               <div class="col-md-6 col-sm-6">
                  <div class="form-group label-floating">
                    <label for="email" class="control-label">Email*</label>
                    <input type="email" class="form-control" id="email" name="email" required="yes" value="<? echo $ac['email']; ?>"><span class="material-input"></span></div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="form-group label-floating">
                          <label for="email" class="control-label">Account Type*</label>
                          <select class="form-control chosen-select" id="type" name="type" required="yes">
                               <option value=""></option>
                               <option value="admin"<? if($ac['type']=='admin'){echo ' selected';}?>>Admin</option>
                               <option value="user"<? if($ac['type']=='user'){echo ' selected';}?>>User</option>
                          </select>
                    </div>
               </div>

               <div class="col-md-6 col-sm-6">
                  <div class="form-group label-floating">
                       <label for="email" class="control-label">Account Status*</label>
                       <select class="form-control chosen-select" id="status" name="status" required="yes">
                            <option value="1"<? if($ac['status']=='1'){echo ' selected';}?>>Live</option>
                            <option value="0"<? if($ac['status']=='0'){echo ' selected';}?>>Disable</option>
                            <option value="42"<? if($ac['status']=='42'){echo ' selected';}?>>Delete</option>
                       </select>
                 </div>
                  </div>

              <input type="hidden" class="form-control" id="id" name="id" value="<? echo $_GET['id']; ?>">

          </form>

          <div class="clearfix"></div>
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>
         <?
          if($_SESSION['SESS_ACCOUNT_Type']=='admin'){?>
          <button type="button" class="btn btn-primary btn-raised" data-id="<? echo $_GET['id'];?>" id="PasswordBut">Password</button>
            <?}?>
<button type="button" class="btn btn-primary btn-raised pull-right" id="editAccFormBut">Save</button>
<div class="clearfix"></div>
