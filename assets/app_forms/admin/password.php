<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
$db->query("select * from accounts where id = ? ");
$db->bind(1,$_GET['id']);
$ac = $db->single();
?>

<form id="editpasswordForm" name="editpasswordForm" method="post" enctype="application/x-www-form-urlencoded">
          <h3>Set New Password For User</h3>
              <div class="col-md-6 col-sm-6">
                <div class="form-group label-floating is-empty">
                  <label for="newpassword" class="control-label">New Password</label>
                  <input type="password" class="form-control" id="newpassword" name="newpassword" value="" required="yes"><span class="material-input"></span></div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group label-floating is-empty">
                <label for="confirmnewpassword" class="control-label">Confirm Password</label>
                <input type="hidden" class="form-control" id="id" name="id" value="<? echo $_GET['id']; ?>">
                <input type="password" class="form-control" id="confirmnewpassword" name="confirmnewpassword" value="" required="yes"><span class="material-input"></span></div>
          </div>
          </form>
          <div class="clearfix"></div>

<button type="button" class="btn btn-warning btn-raised" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-primary btn-raised pull-right" id="saveNewPassword">Save</button>
<div class="clearfix"></div>
