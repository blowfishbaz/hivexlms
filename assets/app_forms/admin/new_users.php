<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from accounts where id = ? ");
$db->bind(1,$_GET['id']);
$ac = $db->single();
?>
<h3>New User</h3>
<form id="NewUserForm" name="NewUserForm" method="post" enctype="application/x-www-form-urlencoded">

            <div class="col-md-6 col-sm-6">
              <div class="form-group label-floating is-empty">
                <label for="name" class="control-label">Name*</label>
                <input type="text" class="form-control" id="name" name="name" required="yes" value="<? echo $ac['name']; ?>"><span class="material-input"></span></div>
               </div>
               <div class="col-md-6 col-sm-6">
                  <div class="form-group label-floating is-empty">
                    <label for="email" class="control-label">Email*</label>
                    <input type="email" class="form-control" id="email" name="email" required="yes" value="<? echo $ac['email']; ?>"><span class="material-input"></span></div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="form-group label-floating">
                          <label for="email" class="control-label">Account Type*</label>
                          <select class="form-control chosen-select" id="type" name="type" required="yes">
                               <option value="admin">Admin</option>
                               <option value="user" selected>User</option>
                          </select>
                    </div>
               </div>

               <div class="col-md-6 col-sm-6">
                  <div class="form-group label-floating">
                       <label for="email" class="control-label">Account Status*</label>
                       <select class="form-control chosen-select" id="status" name="status" required="yes">
                            <option value="1" selected>Live</option>
                            <option value="0">Disable</option>
                       </select>
                 </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                           <div class="form-group label-floating is-empty">
                                    <label for="newpassword" class="control-label">New Password*</label>
                                    <input type="password" class="form-control" id="newpassword" name="newpassword" value="" required="yes"><span class="material-input"></span>
                           </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                           <div class="form-group label-floating is-empty">
                                    <label for="confirmnewpassword" class="control-label">Confirm Password*</label>
                                    <input type="password" class="form-control" id="confirmnewpassword" name="confirmnewpassword" value="" required="yes"><span class="material-input"></span>
                           </div>
                  </div>

          </form>

          <div class="clearfix"></div>
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-primary btn-raised pull-right" id="NewUserFormBut">Save</button>
<div class="clearfix"></div>
