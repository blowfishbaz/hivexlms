<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $customer = $_GET['customer'];
 $job = $_GET['jobid'];
 $pid = $_GET['pid'];


 $path = 'rural_documents/'.$customer.'/'.$job.'/'.$pid.'/';

 ?>

 <h3> New Upload</h3>


<div class="container col-lg-12">

    <!-- The file upload form used as target for the file upload widget -->


    <form id="fileupload" action="<? echo $fullurl; ?>/assets/app_ajax/upload.php?customer=<?echo $customer;?>&jobid=<?echo $job;?>&pid=<?echo $pid;?>" method="POST" enctype="multipart/form-data">


        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-12">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success btn-raised fileinput-button btn-sm">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary btn-raised btn-sm start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning btn-raised btn-sm cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-12 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
    </form>
    <br>

</div>

<div class="clearfix"></div>

<!-- The template to display files available for upload -->



<script>

//////////okExts();

$(function () {

    $('#fileupload').fileupload({
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(<? echo okExts(); ?>)$/i,
         maxFileSize: 10000000,
         done: function (e, data) {


			console.dir(data);
            console.log(data.textStatus);
            console.dir(data._response.result['files'][0].name);
            console.dir(data._response.result['files'][0].type);

            att_com_id = "<?php echo $_GET['id'] ?>";
            att_type = data._response.result['files'][0].type;
            att_name =data._response.result['files'][0].name;

			if(data.textStatus=='success'){

                    //ftype = getFileIcon(att_type);

                    var $docMimes = ["application/pdf","application/msword","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.wordprocessingml.template","application/vnd.ms-word.document.macroEnabled.12", "application/vnd.ms-word.template.macroEnabled.12","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.text-template"];
                    var $spreadMimes = ["application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.spreadsheetml.template","application/vnd.ms-excel.sheet.macroEnabled.12","application/vnd.ms-excel.template.macroEnabled.12","application/vnd.ms-excel.addin.macroEnabled.12","application/vnd.ms-excel.sheet.binary.macroEnabled.12","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.spreadsheet-template"];
                    var $presMimes = ["application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.presentationml.template","application/vnd.openxmlformats-officedocument.presentationml.slideshow","application/vnd.ms-powerpoint.addin.macroEnabled.12","application/vnd.ms-powerpoint.presentation.macroEnabled.12","application/vnd.ms-powerpoint.template.macroEnabled.12","application/vnd.ms-powerpoint.slideshow.macroEnabled.12","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.presentation-template"];

                    if ($docMimes.indexOf(att_type)!='-1') {
                    ftype = "filetypes filetypes-doc";
                    }
                    else if ($spreadMimes.indexOf(att_type)!='-1') {
                    ftype = "filetypes filetypes-xls";
                    }
                    else if($presMimes.indexOf(att_type)!='-1') {
                    ftype = "filetypes filetypes-ppt";
                    }
                    else{
                    ftype = "false";
                    }

                    var customer = '<?echo $customer;?>';
                    var jobid = '<?echo $job;?>';
                    var pid = '<?echo $pid;?>';
                    var path = '<?echo $path;?>';

                    console.log(path);

                    /*image*/
                    if(ftype == 'false'){
                        att_name_url="<? echo $fullurl; ?>"+path+att_name+"";
                        $('.'+pid+'_images').append('<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="'+att_name_url+'" data-src="'+att_name_url+'" data-sub-html="<h4>'+att_name+'</h4>"><a href="javascript:void(0)"><div class="gallery_images" style="background-image:url(\''+att_name_url+'\');"><img class="img-responsive" src="'+att_name_url+'" style="display:none;"></div></a></li>');
                        //$('.lightgallery').lightGallery();
                        $('.'+pid+'_images').data('lightGallery').destroy(true);
                        $('.'+pid+'_images').lightGallery();

                    }
                    /* document*/
                    else{
                        $('.'+pid+'_no_document').remove();
                        $('.'+pid+'_document').append('<li class="list-group-item upImhLi"> <span class="filetypes filetypes-doc" aria-hidden="true" style="float:left; font-size:44px;"></span><span class="pull-left ipImtitle"><a href="<? echo $fullurl; ?>'+path+att_name+'" download="'+att_name+'">'+att_name+'</a></span><span class="pull-right"><button type="button" class="btn btn-danger delAtt" style="margin:0px; padding:0px;" data-attid='+path+'/'+att_name+'><span class="glyphicons glyphicons-bin"></span></button></span><br /><small><?echo date("d-m-Y");?> - 1 seconds ago - <?echo $_SESSION['SESS_ACCOUNT_NAME'];?></small></li> ');
                    }
                            $.ajax({
                                type: "POST",
                                url: "../../../assets/app_ajax/save_upload.php",
                                data: {customer:customer, jobid:jobid, pid:pid, path:path, name:att_name, type:ftype},
                                success: function(msg){

                                }
                            });
			}
        }


    });


    $('#fileupload').fileupload()
    .bind('fileuploadstart', function(){
        // disable submit
    })
    .bind('fileuploadprogressall', function (e, data) {
        if (data.loaded == data.total){
            // all files have finished uploading, re-enable submit
            	$('#noteUpload').modal('hide');
        }
    })

});
</script>
</body>
</html>
