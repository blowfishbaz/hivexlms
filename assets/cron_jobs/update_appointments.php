<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $date = strtotime(date('Y-m-d'));

  $db = new database;
  $db->Query("UPDATE ws_appointments SET status= ? WHERE app_date <= ? and status= ?");
  $db->bind(1,'2');
  $db->bind(2,$date);
  $db->bind(3,'1');
  $db->execute();

  echo 'ok';
?>
