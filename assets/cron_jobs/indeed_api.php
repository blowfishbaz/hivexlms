<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

//Get this to run ever hour?



$publish_id = '123';
$v = '2';
$userip = $_SERVER['REMOTE_ADDR'];
$useragent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)';

 $url = 'https://api.indeed.com/ads/apisearch?publisher='.$publish_id.'&q=&l=&sort=&radius=&st=&jt=&start=&limit=&fromage=&filter=&latlong=&co=&chnl=&userip='.$userip.'&useragent='.$useragent.'&v='.$v.'&format=json';
 $url = 'https://api.indeed.com/ads/apisearch?publisher=123412341234123&q=java+developer&l=austin%2C+tx&sort=&radius=&st=&jt=&start=&limit=&fromage=&filter=&latlong=1&co=us&chnl=&userip=1.2.3.4&useragent=Mozilla/%2F4.0%28Firefox%29&v=2';


$data_string = json_encode($data);

// $ch=curl_init($url);
// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $result = curl_exec($ch);
// curl_close($ch);
// $json = json_decode($result, True);

$json = '{
    "version":2,
    "query":"java",
    "location":"austin, tx",
    "dupefilter":true,
    "highlight":false,
    "radius":25,
    "start":1,
    "end":10,
    "totalResults":547,
    "pageNumber":0,
    "results":[
        {
            "jobtitle":"Java Developer",
            "company":"XYZ Corp.,",
            "city":"Austin",
            "state":"TX",
            "country":"US",
            "formattedLocation":"Austin, TX",
            "source":"Dice",
            "date":"Mon, 02 Aug 2017 16:21:00 GMT",
            "snippet":"<p>This is a snupper</p>",
            "url":"https://www.indeed.com/viewjob?jk=12345&indpubnum=8343699265155203",
            "onmousedown":"indeed_clk(this, );",
            "latitude":30.27127,
            "longitude":-97.74103,
            "jobkey":"12345",
            "sponsored":false,
            "expired":false,
            "indeedApply":true,
            "formattedLocationFull":"Austin, TX",
            "formattedRelativeTime":"11 hours ago"
        },{
            "jobtitle":"Java Developer",
            "company":"XYZ Corp.,",
            "city":"Austin",
            "state":"TX",
            "country":"US",
            "formattedLocation":"Austin, TX",
            "source":"Dice",
            "date":"Mon, 02 Aug 2017 16:21:00 GMT",
            "snippet":"<p>This is a snupper</p>",
            "url":"https://www.indeed.com/viewjob?jk=12345&indpubnum=8343699265155203",
            "onmousedown":"indeed_clk(this, );",
            "latitude":30.27127,
            "longitude":-97.74103,
            "jobkey":"12345",
            "sponsored":false,
            "expired":false,
            "indeedApply":true,
            "formattedLocationFull":"Austin, TX",
            "formattedRelativeTime":"11 hours ago"
        }
    ]
}';

$json_result = json_decode($json,true);

$results_array = $json_result['results'];

foreach($results_array as $r){
    echo 'foreach<br />';
    //CHECK IF THERE IS ALREADY A JOB ID
    //INSERT NEW

    $newid = createId('job');
    $now = time();

    $company = $r['company'];
    $title = $r['jobtitle'];
    $category = $r[''];
    $county = $r['state'];
    $job_type = $r[''];
    $contract_type = $r[''];
    $salary_type = $r[''];
    $per_annum = $r[''];
    $per_hourly = $r[''];
    $overview = $r['snippet'];
    $indeed_id = $r['jobkey'];
    $indeed_url = $r['url'];
    $expired = $r['expired'];

    echo $overview.'<br />';

    $db = new database;
    $db->query("select * from hx_jobs where indeed_id = ? and indeed_job = 1");
    $db->bind(1,$indeed_id);
    $job = $db->single();

    if(empty($job)){
        echo 'New<br />';
      //New
      if($expired != 1 ||$expired != true ){
          echo 'False<br />';
        $db = new database;
        $db->Query("INSERT INTO hx_jobs (id ,created_date ,created_by, status, title, company, category,county, job_type, contract_type, salary_type, per_annum, per_hourly, overview, duties, indeed_id, indeed_job, indeed_url) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $db->bind(1,$newid);
        $db->bind(2,$now);
        $db->bind(3,'indeed');
        $db->bind(4,'1');
        $db->bind(5,$title);
        $db->bind(6,$company);
        $db->bind(7,'');
        $db->bind(8,$county);
        $db->bind(9,'');
        $db->bind(10,'');
        $db->bind(11,'');
        $db->bind(12,'');
        $db->bind(13,'');
        $db->bind(14,$overview);
        $db->bind(15,'');
        $db->bind(16,$indeed_id);
        $db->bind(17,'1');
        $db->bind(18,$indeed_url);
        $db->execute();
      }
    }else{
      //Update
      if($expired != 1 ||$expired != true ){
        $expired = 1;
      }else{
        $expired = 0;
      }

      echo 'Update<br />';

      $db = new database;
      $db->Query("UPDATE hx_jobs SET title= ?, company= ?, category= ? ,county= ?, job_type= ?, contract_type= ?, salary_type= ?, per_annum= ?, per_hourly= ?, overview= ?, duties= ?, indeed_url = ?, status = ? WHERE indeed_id = ?");
      $db->bind(1,$title);
      $db->bind(2,$company);
      $db->bind(3,'');
      $db->bind(4,$county);
      $db->bind(5,'');
      $db->bind(6,'');
      $db->bind(7,'');
      $db->bind(8,'');
      $db->bind(9,'');
      $db->bind(10,$overview);
      $db->bind(11,'');

      $db->bind(12,$indeed_url);
      $db->bind(13,$expired);

      $db->bind(14,$indeed_id);
      $db->execute();
    }
}
