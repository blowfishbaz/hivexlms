<?php



class Form {


	public $FormStart = "1";
	public $FormOutput;
	public $FormEnd;
	public $bindValue = 0;
	public $queryText;
	public $queryBind;
	public $queryBindID;
	public $queryTextID;
	public $formType = "POST";


	/**
	*
	* Constructor
	*
	*/
	public function Form() {


	}

	//Create Toggle
	//$id
	//name
	//lable1
	//label2
	//josh

	public function createToggle($id = "toggle1", $name = null, $label1 = null, $label2 = null){
		$o = '<div class="togglebutton '.$id.'" style="margin-bottom: 10px;">';
		$o .= '<label>';
		$o .= '<span class="togText">'.$label1.'</span>';
		$o .= '</label>';
		$o .= '<label style="padding-left:20px;">';
		$o .= '<input type="checkbox" name="'.$name.'" id="'.$id.'" value="1">';
		$o .= '</label>';
		$o .= '<label>';
		$o .= '<span class="togText">'.$label2.'</span>';
		$o .= '</label>';
		$o .='</div>';

		$this->FormOutput .= $o;
	}


	//Create Form
	//$id = Dom Id
	//$action = "script.torunonsubmit"
	//$method = POST or GET
	//$enctype : application/x-www-form-urlencoded or multipart/form-data or text/plain
	//$class = Any class or Bootstrap classes - form-inline, form-horizontal

	public function createForm($id = "form1", $action = null, $method = null, $enctype = null, $class = null) {

		$this->formType = strtoupper($method);

		$o = '<form ';
		if ($id != null) {
			$o .= 'id="'.$id.'" ';
			$o .= 'name="'.$id.'" ';
		}
		if ($action != null) {
			$o .= 'action="'.$action.'" ';
		}
		if ($method != null) {
			$o .= 'method="'.$method.'" ';
		}
		if ($enctype != null) {
			$o .= 'enctype="'.$enctype.'" ';
		}
		if ($class != null) {
			$o .= 'class="'.$class.'" ';
		}
		$o .= '>';

		$this->FormStart = $o;

	}


	//Create Text Field
	//$type = text,email,number etc
	//$label = Field Label (Will use name if null)
	//$placeholder = Placeholder (Will user name if null)
	//$value = Value for a update
	//$required = yes no
	public function createLabel($label) {

		//$o = '<div class="form-group">';
		$o .= '<label>';
		$o .= $label;
		$o .= '</label>';
	//	$o .= '</div>';
		$this->FormOutput .= $o;
	}



	//Create Text Field
	//$type = text,email,number etc
	//$label = Field Label (Will use name if null)
	//$placeholder = Placeholder (Will user name if null)
	//$value = Value for a update
	//$required = yes no
	public function createTextField($type = 'text', $label = null, $name, $placeholder = null, $value=null, $required='no') {

		$o = '<div class="form-group">';
		$o .= '<label for="'.$name.'">';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}

		$o .= '</label>';

		$o .= '<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'"';

		if ($value != null ) {
			$o .= 'value="'.$value.'"';
		}

		$o .= 'placeholder="';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}

		if ($required == 'yes') {
			$o .= 'required="'.$required.'">';
		} else {
			$o .= '>';
		}

		$o .= '<span class="material-input"></span>';
		$o .= '</div>';


		$this->FormOutput .= $o;

		$s = $name.' = ?,';

		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';

		$this->queryText .= $s;

		$this->queryBind .= $q.'<br>';

	}

	//Create Text Field
	//$label = Field Label (Will use name if null)
	//$placeholder = Placeholder (Will user name if null)
	//$value = Value for a update
	//$required = yes no
	public function createDateField($label = null, $name, $value=null, $required='no') {

		$o = '<div class="form-group"><div id="datetimepicker1" class="input-append">';
		$o .= '<label for="'.$name.'">';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}

		$o .= '</label>';

		$o .= '<input type="text" class="form-control  date form-date"" id="'.$name.'" name="'.$name.'"';

		if ($value != null ) {
			$o .= 'value="'.$value.'"';
		}

		if ($required == 'yes') {
			$o .= 'required="'.$required.'">';
		} else {
			$o .= '>';
		}

		$o .= '<span class="material-input"></span>';
		$o .= '</div></div>';


		$this->FormOutput .= $o;

		$s = $name.' = ?,';

		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';

		$this->queryText .= $s;

		$this->queryBind .= $q.'<br>';

	}

		//Create Text Field
	//$type = text,email,number etc
	//$label = Field Label (Will use name if null)
	//$placeholder = Placeholder (Will user name if null)
	//$value = Value for a update
	//$required = yes no
	public function createFloatingTextField($type = 'text', $label = null, $name, $value=null, $required='no') {

		$o = '<div class="form-group label-floating';

		if($value == null || $value == '') {
			$o .= ' is-empty">';
		} else {
			$o .= '">';
		}

		$o .= '<label for="'.$name.'"class="control-label">';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}

		$o .= '</label>';

		$o .= '<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'"';

		if ($value != null) {
			$o .= 'value="'.$value.'"';
		}

		if ($required == 'yes') {
			$o .= 'required="'.$required.'">';
		} else {
			$o .= '>';
		}


		$o .= '<span class="material-input"></span>';
		$o .= '</div><br>';


		$this->FormOutput .= $o;

		$s = $name.' = ?,';

		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';

		$this->queryText .= $s;

		$this->queryBind .= $q.'<br>';

	}
	
	


	//Create Text Field
	//$type = text,email,number etc
	//$label = Field Label (Will use name if null)
	//$placeholder = Placeholder (Will user name if null)
	//$value = Value for a update
	//$required = yes no
	public function createFloatingTextFieldWithButtonAddon($type = 'text', $label = null, $name, $value=null, $required='no',$buttontext = 'button', $buttonid, $buttonclass) {

		$o = '<div class="input-group">';
		
		$o .= '<div class="form-group label-floating';

		if($value == null || $value == '') {
			$o .= ' is-empty">';
		} else {
			$o .= '">';
		}

		$o .= '<label for="'.$name.'"class="control-label">';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}

		$o .= '</label>';

		$o .= '<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'"';

		if ($value != null) {
			$o .= 'value="'.$value.'"';
		}

		if ($required == 'yes') {
			$o .= 'required="'.$required.'">';
		} else {
			$o .= '>';
		}


		$o .= '<span class="material-input"></span>';
		
		
		
		
		$o .= '</div>';
		$o .= '<span class="input-group-btn"><button class="btn btn-default btn-fab '.$buttonclass.'" id="'.$buttonid.'" type="button">'.$buttontext.'</button></span>';
		
		$o .= ' </div><br>';


		$this->FormOutput .= $o;

		$s = $name.' = ?,';

		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';

		$this->queryText .= $s;

		$this->queryBind .= $q.'<br>';

	}

//Create Text Field
	//$type = text,email,number etc
	//$label = Field Label (Will use name if null)
	//$placeholder = Placeholder (Will user name if null)
	//$value = Value for a update
	//$required = yes no
	public function createTextFieldWithButtonAddon($type = 'text', $label = null, $name, $value=null, $required='no',$buttontext = 'button', $buttonid, $buttonclass) {

		$o = '<div class="input-group">';
		
		$o .= '<div class="form-group';

		if($value == null || $value == '') {
			$o .= ' is-empty">';
		} else {
			$o .= '">';
		}

		$o .= '<label for="'.$name.'"class="control-label">';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}

		$o .= '</label>';

		$o .= '<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'"';

		if ($value != null) {
			$o .= 'value="'.$value.'"';
		}

		if ($required == 'yes') {
			$o .= 'required="'.$required.'">';
		} else {
			$o .= '>';
		}


		$o .= '<span class="material-input"></span>';
		
		
		
		
		$o .= '</div>';
		$o .= '<span class="input-group-btn"><button class="btn btn-default btn-fab '.$buttonclass.'" id="'.$buttonid.'" type="button">'.$buttontext.'</button></span>';
		
		$o .= ' </div><br>';


		$this->FormOutput .= $o;

		$s = $name.' = ?,';

		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';

		$this->queryText .= $s;

		$this->queryBind .= $q.'<br>';

	}



	//Create Id Field
	//$name = name of id field
	//$value = value of id field
	public function createIdField($name, $value) {
		$o = '<input type="hidden" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'">';
		$this->FormOutput .= $o;
		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;
		$this->queryBindID = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';
		$this->queryTextID = 'where '.$name.' = ?';
	}



	public function createTextArea($label = null, $name='textarea1',$rows = '3', $value=null, $required='no', $placeholder=null ) {




		$o = '<div class="form-group label-floating';
		if($value == null || $value == '') {
			$o .= ' is-empty">';
		} else {
			$o .= '">';
		}

		$o .= '<label for="'.$name.'" class="control-label">';

		if ($label != null) {
			$o .= $label;
		} else {
			$o .= $name;
		}
		if ($required == 'yes') {
				$r = 'required="'.$required.'">';
			} else {
				$r = '>';
			}

		$o .= '</label>';

		$o .= '<textarea name="'.$name.'" id="'.$name.'" class="form-control" rows="'.$rows.'" '.$r.''.$value.'</textarea>';
		$o .= '<span class="material-input"></span>';
		$o .= '</div>';

		$this->FormOutput .= $o;

		$s = $name.' = ?,';

		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';

		$this->queryText .= $s;

		$this->queryBind .= $q.'<br>';

	}

	//Create Hidden Field
	//$name = name of hidden field
	//$value = value of hidden field
	public function createHiddenField($name, $value) {
		$o = '<input type="hidden" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'">';
		$this->FormOutput .= $o;
		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;

		$this->queryBind .= '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);<br>';
		$this->queryText .= $name.' = ?,';
	}


	//CreateCheckBox
	//$name = name
	//$type = class or bootstrap checkbox or checkbox-inline
	//$checked = null or checked
	public function createCheckbox($name, $value, $text, $disabled = null, $type = "checkbox-inline", $check ) {


		if ($disabled != null) {
			$dis ='disabled';
		} else {
			$dis ='';
		}

  if($check == 1){
   $check = 'checked';
  }else{
   $check = '';
  }
		$o = '<div class="form-group">';
		$o = '<div class="checkbox">';
		$o .= '<label>';
		$o .= '<input type="checkbox" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$dis.' '.$check.'/>'.$text.'';
		$o .= '</label>';
		$o .= '</div></div><br>';

		$this->FormOutput .= $o;
		$s = $name.' = ?,';
		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;
		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';
		$this->queryText .= $s;
		$this->queryBind .= $q.'<br>';
	}


	//createRadio
	//$name = name
	//$type = class or bootstrap checkbox or checkbox-inline
	//$myopts = array of options
	//$isChcked = set for default option, can '' or number (starts @ 0 for option 1!)


	public function createCheckbox2($name, $value, $text, $disabled = null, $type = "checkbox-inline" ) {


		if ($disabled != null) {
			$dis ='disabled';
		} else {
			$dis ='';
		}


		$o .= '<label class="'.$type.'">';
		$o .= '<input type="checkbox" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$dis.'><span class="checkbox-material"><span class="check"></span></span> '.$text.'';
		$o .= '</label>';




		$this->FormOutput .= $o;

		$s = $name.' = ?,';
		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;
		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';
		$this->queryText .= $s;
		$this->queryBind .= $q.'<br>';
	}




	public function createRadio($name, $type = "radio-inline", $myopts, $isChcked, $mainLabel = 'null', $required = 'null') {

		$howmanyoptions = count($myopts);

		if($required!='null'){
			$rq = 'required';
		}
		else{
			$rq = '';
		}


		if($mainLabel!='null'){
			$o = '<label>'.$mainLabel.'</label>';
		}
		else{
			$o = '';
		}

		$o .= '<div class="checkbox">';

		for ($l = 0 ; $l < $howmanyoptions; $l++){


			if($required!='null'){
				if($l==0){$rq = 'required';}
				else{$rq = '';}
			}
			else{
				$rq = '';
			}




			if ($isChcked != '' && $l == $isChcked) {
				$chkThis = 'checked="checked"';
			} else {
				$chkThis ='';
			}
			$o .= '<label class="'.$type.'"><input type="radio" value="'.$myopts[$l].'" name="'.$name.'" '.$chkThis.' '.$rq.'>'.$myopts[$l].'</label>';
		}
		//$o .= '<span class="material-input"></span>';
		$o .= '</div><br>';
		$this->FormOutput .= $o;


		$s = $name.' = ?,';
		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;
		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';
		$this->queryText .= $s;
		$this->queryBind .= $q.'<br>';

	}


	//createSelectBox
	//$name = name
	//$type = class or bootstrap checkbox or checkbox-inline
	//$myopts = array of options
	//$myvals = vals if diffrent from options, same order required
	//$isCSel = set for default select, can '' or number (starts @ 0 for option 1!)
	public function createSelectBox($name, $text, $type = "form-control", $myopts, $myvals, $isSel='') {

		$howmanyoptions = count($myopts);


		if ($myvals == '') {
			$myvals = $myopts;
		}



		$o = '<div class="form-group">';
		$o .='<label for="'.$name.'">'.$text.'</label>';
		$o .='<select class="'.$type.'" id="'.$name.'" name="'.$name.'">';

		for ($l = 0 ; $l < $howmanyoptions; $l++){

					if ($isSel != '' && $l == $isSel) {
						$chkThis = 'selected';
					} else {
						$chkThis ='';
					}


					$o .= '<option value="'.$myvals[$l].'" '.$chkThis.'>'.$myopts[$l].'</option>';
		}
		$o .= '</select>';
		$o .= '<span class="material-input"></span>';
		$o .= '</div>';

		$this->FormOutput .= $o;

		$s = $name.' = ?,';
		$num = $this->bindValue;
		$num = $num + 1;
		$this->bindValue = $num;
		$q = '$db->bind('.$num.',$_'.$this->formType.'["'.$name.'"]);';
		$this->queryText .= $s;
		$this->queryBind .= $q.'<br>';


	}


	public function createButton($id,$name = "submit", $type="button",$btnclass='btn-default') {

		$o = '<button type="'.$type.'" class="btn '.$btnclass.'" id="'.$id.'">'.$name.'</button>';
		$this->FormOutput .= $o;
	}


	//Finally Displays the Form
	public function DisplayForm() {


		$output = $this->FormStart;

		$output .= $this->FormOutput;

		$output .= '</form>';

		echo $output;


	}

	public function createDiv($class='col-lg-12') {

		$o = '<div class="'.$class.'">';
		$this->FormOutput .= $o;
	}

	public function createDivStyle($class='col-lg-12', $style = NULL) {

		$o = '<div class="'.$class.'" style="'.$style.'">';
		$this->FormOutput .= $o;
	}

	public function endDiv() {

		$o = '</div>';
		$this->FormOutput .= $o;
	}

	//Finally Displays the Form
	public function DisplaySQL($type, $table) {


		echo '<pre>';

		if ($type == 'update') {
			echo '$db->Query("update '.$table.' set '.rtrim($this->queryText, ",").' '.$this->queryTextID.'");<br>';
			echo $this->queryBind;
			echo $this->queryBindID;
		}
		if ($type == 'insert') {
			echo '$db->Query("insert into '.$table.' set '.rtrim($this->queryText, ",").'");<br>';
			echo $this->queryBind;
		}




		echo "</pre>";




	}


}
