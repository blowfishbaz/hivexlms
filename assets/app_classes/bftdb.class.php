<?php

/*

Class Name : bftdb class by Blowfish Technology
Version : 1
Owned and Copyright : Blowfish Technology
Author : Graham Palfreyman

Date : 22/09/2014

Filename : bftdb.class.php

##########################################################


Main Class

 gvcnjfgcgnvn

##########################################################
*/


//Defines the database connection details
define("DB_HOST", "localhost"); //Define Address
define("DB_USER", "root"); // Define Username
define("DB_PASS", "bft77tfb"); // Define Password
define("DB_NAME", "hx_crm"); // Define DB_NAME



class Database {


private $host = DB_HOST;
private $user = DB_USER;
private $pass = DB_PASS;
private $dbname = DB_NAME;
private $dbh;
private $error;
private $stmt;
private $is_log_on = true; // Turn On and Off Console Logging
private $items_per_page;
private $items_total;
private $current_page;
private $num_pages;
private $mid_range = 7;
private $low;
private $high;
private $limit;
private $return;
private $default_ipp = 10;
private $start_ipp;
private $querystring;



/**
*
* Constructor
*
*/
public function Database() {

          //Set the DSN
    $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
    error_log("Connecting to database with ".$dsn);
    // Set DSN Options
    $options = array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    //Make the connection to the database
    try {
        $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
		error_log("Successfully Connected to DB");
    }
    //Catch any errors
    catch (PDOException $e) {
        $this->error = $e->getMessage();
		error_log("Error Occurred = ".$this->error);
    }

	$this->current_page = 1;
	$this->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp']:$this->default_ipp;


}


/**
*
* Query Function
*
* @param string $query - Give SQL Query with bind values placeholders e.g :id
*/
public function Query($query){
    $this->stmt = $this->dbh->prepare($query);
	error_log("Running Query : ".$query);
}

/**
*
* Bind Function
*
* @param string $param - BindValue placeholder e.g :id
* @param string $value - Variable Value
* @param string $type - Optional function detemines what type is required from the $value variable.
*/
public function bind($param, $value, $type = null) {

    if (is_null($type)) {
     switch (true) {
         case is_int($value):
            $type = PDO::PARAM_INT;
            break;
         case is_bool($value):
            $type = PDO::PARAM_BOOL;
            break;
         case is_null($value):
            $type = PDO::PARAM_NULL;
            break;
         default:
            $type = PDO::PARAM_STR;
    	}

    $this->stmt->bindValue($param,$value, $type);
	error_log("Binding Variable : ".$param.",value : ".$value.", Type : ".$type);
	}

}

/**
*
* searchbind Function
*
* @param string $param - BindValue placeholder e.g :id
* @param string $value - Variable Value
* @param string $type - Optional -  function detemines what type is required from the $value variable.
* @param string $type - Optional - Places % in variable for like... left , right or both. Default : Both.
*/
public function searchbind($param, $value, $direction, $type = null) {

  switch ($direction) {
         case "left":
            $value = "%".$value;
            break;
         case "right":
            $value = $value."%";
            break;
         default:
            $value = "%".$value."%";
    	}

    if (is_null($type)) {
     switch (true) {
         case is_int($value):
            $type = PDO::PARAM_INT;
            break;
         case is_bool($value):
            $type = PDO::PARAM_BOOL;
            break;
         case is_null($value):
            $type = PDO::PARAM_NULL;
            break;
         default:
            $type = PDO::PARAM_STR;
    	}



    $this->stmt->bindValue($param,$value, $type);
	error_log("Binding Variable : ".$param.",value : ".$value.", Type : ".$type);
	}

}


/**
*
* Executes query Function
*
*/
public function execute() {
    return $this->stmt->execute();
	error_log("Executing Query");
}

/**
*
* resultset Function
*
* runs execute function then -
* Pulls results into array
*
*/
public function resultset(){
    $this->execute();
    return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
*
* single Function
*
* runs execute function then -
* Pulls single result into array
*
*/
public function single() {
    $this->execute();
    return $this->stmt->fetch(PDO::FETCH_ASSOC);
}

/**
*
* rowcount Function
*
* Get rowCount
*
*/
public function rowcount() {
    return $this->stmt->rowCount();
}

/**
*
* Last Insert Function
*
* Get the last inserts primary key id
*
*/
public function lastInsertId() {
 return $this->dbh->lastInsertId();
}

/**
*
* Query Page Function
*
* @param string $query - Give same Query but automatically tags the limit statements with bind values placeholders e.g :id
*/
public function paginate_query($query){
	$this->paginate();
	$query = $query." ".$this->limit;
    $this->stmt = $this->dbh->prepare($query);
	error_log("Running Paging Query : ".$query);
}


/**
*
* createtotalcount Function
*
* @param int $count - Sets total record count
*/

public function paginate_createtotalcount($count) {

  $this->items_total = $count;
  error_log("Total Items in first query is and setting create total count to : ".$count);
}


/**
*
* setItemsPerPage Function
*
* @param int $ipp - Sets total record count
*/

public function paginate_setItemsPerPage($ipp) {
  if(isset($_GET['ipp'])){
	  $this->default_ipp = $_GET['ipp'];
	  $this->items_per_page = $_GET['ipp'];

	  error_log("Setting Items per Page to : ".$ipp);
  } else {
	  $this->default_ipp = $ipp;
	  $this->items_per_page = $ipp;
	  error_log("Setting Items per Page to : ".$ipp);
  }
  $this->start_ipp = $ipp;
}

/**
*
* Paginate Function
*
*/


function paginate()
	{
		error_log("Paginating");

		if(isset($_GET['ipp']) && $_GET['ipp'] == 'All')
		{
			$this->num_pages = ceil($this->items_total/$this->items_total);
			$this->items_per_page = $this->default_ipp;
			error_log("num_pages is :".$this->num_pages);
		}
		else
		{

			error_log("items per page is :".$this->items_per_page);
			$this->items_per_page = $this->default_ipp;
			$this->num_pages = ceil($this->items_total/$this->items_per_page);


		}


		$this->current_page = isset($_GET['page']) ? (int) $_GET['page'] : 1; // must be numeric > 0
		if($this->current_page < 1 Or !is_numeric($this->current_page)) $this->current_page = 1;
		if($this->current_page > $this->num_pages) $this->current_page = $this->num_pages;
		$prev_page = $this->current_page-1;
		$next_page = $this->current_page+1;

		if($_GET)
		{
			$args = explode("&",$_SERVER['QUERY_STRING']);
			foreach($args as $arg)
			{
				$keyval = explode("=",$arg);
				if($keyval[0] != "page" And $keyval[0] != "ipp") $this->querystring .= "&" . $arg;
			}
		}

		if($_POST)
		{
			foreach($_POST as $key=>$val)
			{
				if($key != "page" And $key != "ipp") $this->querystring .= "&$key=$val";
			}
		}

		if($this->num_pages > 10)
		{
			$this->return = ($this->current_page != 1 And $this->items_total >= 10) ? "<a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=$prev_page&ipp=$this->items_per_page$this->querystring\">&laquo; Previous</a> ":"<span class=\"inactive\" href=\"#\">&laquo; Previous</span> ";

			$this->start_range = $this->current_page - floor($this->mid_range/2);
			$this->end_range = $this->current_page + floor($this->mid_range/2);

			if($this->start_range <= 0)
			{
				$this->end_range += abs($this->start_range)+1;
				$this->start_range = 1;
			}
			if($this->end_range > $this->num_pages)
			{
				$this->start_range -= $this->end_range-$this->num_pages;
				$this->end_range = $this->num_pages;
			}
			$this->range = range($this->start_range,$this->end_range);

			for($i=1;$i<=$this->num_pages;$i++)
			{
				if($this->range[0] > 2 And $i == $this->range[0]) $this->return .= " ... ";
				// loop through all pages. if first, last, or in range, display
				if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range))
				{
					$this->return .= ($i == $this->current_page And $_GET['page'] != 'All') ? "<a title=\"Go to page $i of $this->num_pages\" class=\"current\" href=\"#\">$i</a> ":"<a class=\"paginate\" title=\"Go to page $i of $this->num_pages\" href=\"$_SERVER[PHP_SELF]?page=$i&ipp=$this->items_per_page$this->querystring\">$i</a> ";
				}
				if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) $this->return .= " ... ";
			}
			$this->return .= (($this->current_page != $this->num_pages And $this->items_total >= 10) And ($_GET['page'] != 'All')) ? "<a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=$next_page&ipp=$this->items_per_page$this->querystring\">Next &raquo;</a>\n":"<span class=\"inactive\" href=\"#\">&raquo; Next</span>\n";
			$this->return .= ($_GET['page'] == 'All') ? "<a class=\"current\" style=\"margin-left:10px\" href=\"#\">All</a> \n":"<a class=\"paginate\" style=\"margin-left:10px\" href=\"$_SERVER[PHP_SELF]?page=1&ipp=All$this->querystring\">All</a> \n";
		}
		else
		{
			for($i=1;$i<=$this->num_pages;$i++)
			{
				$this->return .= ($i == $this->current_page) ? "<a class=\"current\" href=\"#\">$i</a> ":"<a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=$i&ipp=$this->items_per_page$this->querystring\">$i</a> ";
			}
			$this->return .= "<a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=1&ipp=All$this->querystring\">All</a> \n";
		}

		error_log("Current page is :".$this->current_page." and items per page is ".$this->items_per_page);
		$this->low = ($this->current_page-1) * $this->items_per_page;
		$this->high = (isset($_GET['ipp']) && $_GET['ipp'] == 'All') ? $this->items_total:($this->current_page * $this->items_per_page)-1;
		$this->limit = (isset($_GET['ipp']) && $_GET['ipp'] == 'All') ? "":" LIMIT $this->low,$this->items_per_page";
}

/**
*
* display_items_per_page Function
*
* Diplays the how many items per page for current pagination.
*/
function paginate_display_items_per_page()
{
	$items = '';
	$ipp_array = array(10,25,50,100,'All',$this->start_ipp);
	foreach($ipp_array as $ipp_opt)	$items .= ($ipp_opt == $this->items_per_page) ? "<option selected value=\"$ipp_opt\">$ipp_opt</option>\n":"<option value=\"$ipp_opt\">$ipp_opt</option>\n";
	return "<span class=\"paginate\">Items per page:</span><select class=\"paginate\" onchange=\"window.location='$_SERVER[PHP_SELF]?page=1&ipp='+this[this.selectedIndex].value+'$this->querystring';return false\">$items</select>\n";
}

/**
*
* display_pages Function
*
* Diplays the links to travel per page for current pagination.
*/
function paginate_display_pages()
{
	return $this->return;
}


/**
*
* Console Log
*
*
*/

public function console($data) {

   if ($this->is_log_on) {

    if(is_array($data) || is_object($data))
	{
		echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
	} else {
		echo("<script>console.log('PHP: ".$data."');</script>");
	}
   }
}





}


?>
