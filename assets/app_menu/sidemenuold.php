<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    sidemenu.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////
 $url = $_SERVER['SCRIPT_NAME'];
 $path = parse_url($url, PHP_URL_PATH);
 $arr = explode("/",$path);

     //echo ltrim($path,"/");


     switch ($arr[2]) {
          case "crm":
                    $menu = 'CRM';
                    break;
          case "rst":
                    $menu = 'RST';
                    break;
          case "hr":
                    $menu = 'HR';
                    break;
          default:
                    $menu = 'default';
     }

/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////



///////////check Permissions///////////////////////

if($arr[2]!='profile' && isset($arr[3])){
          //checkPerm($arr[3]);
}
///////////check Permissions///////////////////////

function checkRow($a){
    if($a <= 5){
        $color = "#777";
    }else if($a <= 15){
        $color = "#f0ad4e";
    }else if($a > 15){
        $color = "#f44336";
    }
    return $color;
}
?>


<div class="menutype" data-id='<? echo $menu; ?>'></div>



        <div id="sidebar-wrapper">

	        <div id="borderleft"></div>

            <ul class="sidebar-nav">




                 <!-- <div style="position:fixed; width:100%; min-height:20px; background-color:red;"></div> -->
                <div class="sidebarTop menu-toggle" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger 1" aria-hidden="true" ></span></div>

                <div class="sidebarTop selectTOPmenu" id="menu-selecttop"  ><span class="icon-hive"></span></div>
                <?
                if(!isset($_GET["hide"])){
                ?>


          <?
                     $uri = $path;
                     $uri = substr($uri, 1);

                     $db->query("select * from hx_menu_config where url = ? and (status = 1 OR status = 2)");
                     $db->bind(1, $uri);
                     $current = $db->single();

                     $db->query("select * from hx_menu_config where pid = ? and (status = 1 OR status = 2) AND hidden = 0 order by menu_order asc");
                     $db->bind(1, $current['id']);
                     $sub = $db->resultset();

                     if(empty($sub)){

                          $db->query("select * from hx_menu_config where pid = ? and (status = 1 OR status = 2) AND hidden = 0 order by menu_order asc");
                          $db->bind(1, $current['pid']);
                          $sub = $db->resultset();
                     }


                     if(!empty($current['pid']) && $current['hidden'] == '1'){
                         $db->query("select * from hx_menu_config where id = ? and hidden = 0");
                         $db->bind(1, $current['pid']);
                         $pid = $db->single();
                     }else{
                       $pid = null;
                     }



                     ?>
                     <div id="topmenu"  class="leftmenus" <?if(!empty($sub)){echo 'style="display:none;"';}?>>
                     <? $db->query("select * from hx_menu_config where pid IS NULL and (status = 1 OR status = 2) AND hidden = 0 order by menu_order asc");
                        $section = $db->resultset();

                        foreach ($section as $s) {

                             if(in_array($s['perm_id'],$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){?>
                                  <li class='<?if(ltrim($path,"/") == $s['url'] || $s['url'] == $pid['url']){echo "selected_menu_li";}?>'>
                                  <a class='<?if(ltrim($path,"/") == $s['url'] || $s['url'] == $pid['url']){echo "selected_menu_a";}?>' href="<?if($s['url'] == '#'){echo '#';}else{
                                    // if($s['perm_id'] == 'clients'){
                                    //     $db->query("select * from hx_customers order by company_name asc limit 1");
                                    //     $cust_menu = $db->single();
                                    //     //echo $fullurl.$s['url'].'?id='.$cust_menu['id'];
                                    //     echo $fullurl.$s['url'].'?id=map';
                                    // }elseif($s['perm_id'] == 'third'){
                                    //     $db->query("select * from hx_third_party order by company_name asc limit 1");
                                    //     $t_menu = $db->single();
                                    //     echo $fullurl.$s['url'].'?id='.$t_menu['id'];
                                    // }else if ($s['perm_id'] == 'arch_clients'){
                                    //     $db->query("select * from hx_clients where  archived = 0  order by case when name2 = '' or name2 is null then name1 else name2 end limit 1");
                                    //     $archived_client = $db->single();
                                    //     echo $fullurl.$s['url'].'?id='.$archived_client['id'];
                                    // }else{
                                        echo $fullurl.$s['url'];
                                    //}


                                }?>"><span class="menuglyph glyphicons <?if(empty($s['glyphicon'])){echo 'glyphicons-parents';}else{echo $s['glyphicon'];}?>" aria-hidden="true"></span><span class="menuText"><?echo $s['name'];?></span >
                                    <? if(!empty($rowcount)){ ?> <span id="<? echo $idName;?>" class="badge <? echo $isClass; ?>"><? echo $rowcount; ?></span><? }?></a>
                                  </li>
                             <?}?>
                        <?}?>
                     </div>
                     <div id="" class="leftmenus" <?if(empty($sub)){echo 'style="display:none;"';}?>>
                          <?
                               foreach ($sub as $s) {
                                   $perm_count = '0';
                                   if($s['perm_id'] == 'p_p_allocation'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? AND hx_prospect.status = 1");
                                       $db->bind(1, '1');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_upload'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? AND hx_prospect.status = 1");
                                       $db->bind(1, '2');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_peer'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? AND hx_prospect.status = 1");
                                       $db->bind(1, '3');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_proof'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? AND hx_prospect.status = 1");
                                       $db->bind(1, '4');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_send'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? AND hx_prospect.status = 1");
                                       $db->bind(1, '5');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_sent'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? and job_status <= 3 AND hx_prospect.status = 1");
                                       $db->bind(1, '6');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'exp_app'){
                                       $db->query("select id from hx_expenses where status = ?");
                                       $db->bind(1, '1');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'dis_app'){
                                       $db->query("select id from hx_disbursement where status = ?");
                                       $db->bind(1, '1');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'exchange_sent'){
                                       $rowcount = OutlookService::getCounts(oAuthService::getAccessToken($redirectUri), $_SESSION['user_email'],'SentItems');
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'exchange_delete'){
                                       $rowcount = OutlookService::getCounts(oAuthService::getAccessToken($redirectUri), $_SESSION['user_email'],'DeletedItems');
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'exchange_inbox'){
                                       $rowcount = OutlookService::getCountsUnread(oAuthService::getAccessToken($redirectUri), $_SESSION['user_email'],'Inbox');
                                       $perm_count = 1;
                                   }else if ($s['perm_id'] == 'comm_comm'){
                                       $db->query("select hx_prospect.* from hx_prospect where (hx_prospect.status = ? or hx_prospect.status = ? or hx_prospect.status = ?) and (hx_prospect.job_status = ? or hx_prospect.job_status = ?)");
                                       $db->bind(1, '1');
                                       $db->bind(2, '2');
                                       $db->bind(3, '3');
                                       $db->bind(4, '3');
                                       $db->bind(5, '4');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if ($s['perm_id'] == 'arch_clients'){
                                       $db->query("select * from hx_clients where  archived = 0  order by case when name2 = '' or name2 is null then name1 else name2 end limit 1");
                                       $archived_client = $db->single();
                                   }else if($s['perm_id'] == 'qual'){
                                       $db->query("select hx_prospect.* from hx_prospect where hx_prospect.proposal_status = ? AND hx_prospect.status = 1");
                                       $db->bind(1, '1.5');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_first'){
                                       $db->query("select distinct hx_prospect.*
                                       from hx_prospect
                                       left join hx_todo
                                       on hx_todo.pid = hx_prospect.id
                                       where hx_prospect.proposal_status = ? and hx_prospect.job_status != 4 and hx_todo.task_itm = 'First Proposal follow up' and hx_todo.complete = 0 and hx_prospect.status = 1");
                                       $db->bind(1, '6');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }else if($s['perm_id'] == 'p_p_second'){
                                       $db->query("select distinct hx_prospect.*
                                       from hx_prospect
                                       left join hx_todo
                                       on hx_todo.pid = hx_prospect.id
                                       where hx_prospect.proposal_status = ? and hx_todo.task_itm = 'Second Proposal follow up' and hx_prospect.job_status != 4 and hx_todo.complete = 0 and hx_prospect.status = 1");
                                       $db->bind(1, '6');
                                       $db->execute();
                                       $rowcount = $db->rowcount();
                                       $perm_count = 1;
                                   }



                                    if(in_array($s['perm_id'],$_SESSION['SESS_ACCOUNT_PERMISSIONS'])){?>
                                         <li class='<?if(ltrim($path,"/") == $s['url'] || $s['url'] == trim($pid['url'])){echo "selected_menu_li";}?>'>
                                         <a class='<?if(ltrim($path,"/") == $s['url'] || $s['url'] == trim($pid['url'])){echo "selected_menu_a";}?>' href="<?if($s['url'] == '#'){echo '#';}else{

                                             if($s['perm_id'] == 'clients'){
                                                 $db->query("select * from hx_customers order by company_name asc limit 1");
                                                 $cust_menu = $db->single();
                                                 //echo $fullurl.$s['url'].'?id=xxxxxx'.$cust_menu['id'];
                                                 echo $fullurl.$s['url'].'?map';
                                             }else if ($s['perm_id'] == 'arch_clients'){
                                                 echo $fullurl.$s['url'].'?id='.$archived_client['id'];
                                             }else{
                                                 echo $fullurl.$s['url'];
                                             }

                                         }?>"><span class="menuglyph glyphicons <?if(empty($s['glyphicon'])){echo 'glyphicons-parents';}else{echo $s['glyphicon'];}?>" aria-hidden="true"></span><span class="menuText"><?echo $s['name'];?> </span>
                                         <?if(!empty($perm_count)){?><span class="badge menu_badge" style="margin-left: 10px; display: inline; background-color: <?checkRow($rowcount);?>;"><?echo $rowcount;?></span><?}?></a>

                                         </li>
                                    <?}?>
                               <?}?>
                     </div>



                <div class="sidebarTop selectTOPmenu" id="menu-selectbottom"><span class="icon-hive"></span></div>


                <?}?>




            </ul>


        </div>
        <!-- /#sidebar-wrapper -->

<?
//include ($fullpath."assets/app_php/inuse.php");
if(isset($_GET['id'])){$tedid=$_GET['id'];}
else{$tedid='';}
?>
<script>
$tedid="<? echo $tedid; ?>";
//document.getElementById('sidebar-wrapper').innerHTML += "<div class='war' style='width:100%;background-color:red;padding:5px 0;text-align:center; position:absolute;z-index:9999;'>WARNING BAR</div>";

</script>
