<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    sidemenu.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

$_SESSION["MENU"] = 'default';

$url = $_SERVER['SCRIPT_NAME'];
$path = parse_url($url, PHP_URL_PATH);
$arr = explode("/",$path);

$db = new database;
$db->query("SELECT * from ws_notifications where created_for = ? and viewed = 0");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$notifications_count = $db->single();
$notifications_counter = $db->rowcount();

?>


<div class="menutype"></div>

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
	        <div id="borderleft" ></div>
            <ul class="sidebar-nav">



                <div class="sidebarTop menu-toggle-new" id="menu-toggle">
                        <!-- <span class="glyphicons glyphicons-remove" style="font-size:2em; -webkit-text-stroke: 8px white;" aria-hidden="true" ></span> -->
                        <img class="" src="<? echo $fullurl ?>/assets/images/menu_close.png" style="height:50px; width:auto; padding:6px 8px 6px 8px;" />

                </div>
                <div class="sidebarTop selectTOPmenu" id="menu-selecttop"  >
                        <div id="leftmenu-other">
                             <a href="javascript:void(0)" id="top_menu_reminders" class="btn" style="color: #999999;font-size: 42px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Notifications"><img lass="img-rounded" alt="140x140" src="<?echo $fullurl;?>assets/images/IconNotification.png" style="width: 40px; height: 40px;" /><div class="ripple-container"></div><? if($notifications_counter>0){ ?><span class="badge badge-danger toBadgeNumber" title="Reminders"><?echo $notifications_counter;?></span><? } ?></a>

                        </div>
                        <div id="leftmenu-other">
        	                <a href="javascript:void(0)" class="btn profileleft" data-toggle="modal" data-target="#ProfileModal"><img class="img-rounded" alt="140x140" src="<? echo loadProfilePic2($_SESSION['SESS_PROFILEPIC']); ?>" data-holder-rendered="true" style="width: 40px; height: 40px;"><div class="ripple-container"></div></a>
                        </div>
                        <img class="menu_image_animate" src="<? echo $fullurl ?>/assets/images/mini_logo.png" style="height:62px; width:auto;" />

                </div>








                <!-- TOP MENU --->
                <div class="clearfix"></div>
                <div style="width:100%; height:15px;"></div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                  <a href="<? echo $fullurl ?>admin/rota/index.php"><img src="<? echo $fullurl ?>/staff-box/images/pink.png" style="width: 100%; height: auto; max-width: 100px; margin: 0 auto;display: table;" /></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <img src="<? echo $fullurl ?>/staff-box/images/blue.png" style="width: 100%; height: auto; max-width: 100px; margin: 0 auto;display: table;" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <img src="<? echo $fullurl ?>/staff-box/images/green.png" style="width: 100%; height: auto; max-width: 100px; margin: 0 auto;display: table;" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <img src="<? echo $fullurl ?>/staff-box/images/yellow.png" style="width: 100%; height: auto; max-width: 100px; margin: 0 auto;display: table;" />
                </div>

                <div class="clearfix"></div>

                <div id="topmenu" class="leftmenus">

                        <?if($arr[2] == 'rota'){?>
                                <?

                                $db->query("select * from accounts where id = ?");
                                $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
                                $menu_account = $db->single();

                                ?>
                                <li> <a href="<? echo $fullurl?>admin/rota/index.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Dashboard</span> </a> </li>
                                <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
                                  <li> <a href="<? echo $fullurl?>admin/rota/rota.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Calendar</span> </a> </li>
                                <?}else{?>
                                  <li> <a href="<? echo $fullurl?>admin/rota/my_calendar.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">My Calendar</span> </a> </li>
                                <?}?>

                                <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
                                <li> <a href="<? echo $fullurl?>admin/rota/admin/services.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Services</span> </a> </li>
                                <?}?>
                                <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
                                  <li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/profile_view.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">Users</span> </a> </li>
                                <?}else{?>
                                  <li> <a href="<? echo $fullurl?>admin/rota/account/view_profile.php?id=<?echo decrypt($_SESSION['SESS_ACCOUNT_ID']);?>"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">My Account</span> </a> </li>
                                <?}?>
                                <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
                                        <li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/approval.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">Account Approval</span> </a> </li>
                                        <li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/rota_bid.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">New Rota</span> </a> </li>
                                        <li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/invoices.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">Invoices</span> </a> </li>
                                <?}?>
                                <?if($menu_account['account_type'] == 'user'){?>
                                        <li> <a href="<? echo $fullurl?>admin/rota/account/pending_shifts.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">Available Shifts</span> </a> </li>
                                <?}?>
                                <?if($menu_account['account_type'] != 'user'){?>
                                  <li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/accounts/index.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">System users</span> </a> </li>
                                <?}?>
                                <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
                                <li class="rota_menu_mobile">
                                        <a href="javascript:void(0);">
                                                <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">Admin</span>
                                                <span class="glyphicons glyphicons-circle-arrow-down admin-menu-dropdown" style="color:#edaac6;"></span>
                                        </a>
                                        <div class="clearfix"></div>

                                </li>
                                <?}?>
                                <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
                                        <table id="rota_menu_mobile_table">
                                                <tr><th><a href="<? echo $fullurl?>admin/rota/admin/rota_bid.php">Rota Bid</a></th></tr>
                                                <tr class="spacer_rota_mobile"></tr>
                                                <tr style="background-color: #fafafa;"><th><a href="<? echo $fullurl?>admin/rota/admin/profile_view.php">Users</a></th></tr>
                                                <tr class="spacer_rota_mobile"></tr>
                                                <tr><th><a href="<? echo $fullurl?>admin/rota/admin/approval.php">Accounts Aprovals</a></th></tr>
                                                <tr class="spacer_rota_mobile"></tr>
                                                <tr style="background-color: #fafafa;"><th><a href="<? echo $fullurl?>admin/rota/admin/accounts/index.php">System users</a></th></tr>
                                                <tr class="spacer_rota_mobile"></tr>
                                                <tr><th><a href="<? echo $fullurl?>admin/rota/admin/invoices.php">Invoices</a></th></tr>
                                                <tr class="spacer_rota_mobile"></tr>
                                        </table>
                                        <?}?>


                                        <?if($menu_account['account_type'] == 'admin'){?><li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/pcn.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">PCN</span> </a> </li><?}?>
                                        <?if($menu_account['account_type'] != 'user'){?><li class="rota_menu_desktop"> <a href="<? echo $fullurl?>admin/rota/admin/practice.php"> <span class="menuglyph glyphicons " aria-hidden="true"></span> <span class="menuText menuTextLeft">Practice</span> </a> </li> <?}?>

                                <li>
                                    <a href="<? echo $fullurl ?>assets/app_php/auth.php?action=logout" class="btn btn-raised menu_log_out_button" class="menu_log_out_button">Log Out</a>
                                </li>



                        <?}else if($arr[2] == 'courses' || $arr[2] == 'credits' || $arr[2] == 'accounts' || $arr[2] == 'dashboard.php' ){?>
                                <li> <a href="<? echo $fullurl?>admin/dashboard.php"> <span class="menuglyph" aria-hidden="true"></span> <span class="menuText menuTextLeft">Dashboard</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/courses/index.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Manage Courses</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/accounts/learners.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Users</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/accounts/company.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Companies</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/accounts/company.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Reports</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/courses/orders/index.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Orders</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/accounts/company.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Email Templates</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/accounts/company.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Config</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/users/index.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">HR</span> </a> </li>
                                <li> <a href="<? echo $fullurl?>admin/credits/manage_credits.php"> <span class="menuglyph " aria-hidden="true"></span> <span class="menuText menuTextLeft">Credits</span> </a> </li>
                                <li>
                                    <a href="<? echo $fullurl ?>assets/app_php/auth.php?action=logout" class="btn btn-raised menu_log_out_button" class="menu_log_out_button">Log Out</a>


                                </li>
                        <?}?>






                </div>
                <div class="clearfix"></div>
                <!-- END OF TOP MENU --->

                <!-- END OF PRODUCTS MENU --->
                <div class="sidebarTop selectTOPmenu" id="menu-selectbottom"><span class="icon-hive"></span></div>
            </ul>
            <div class="clearfix"></div>



            <!-- <div class="menu_log_out">
                <button type="button" class="btn btn-raised">Log Out</button>
            </div> -->



            <div class="clearfix"></div>

        </div>


        <!-- /#sidebar-wrapper -->
