<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    sidemenu.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

$_SESSION["MENU"] = 'default';

$url = $_SERVER['SCRIPT_NAME'];
$path = parse_url($url, PHP_URL_PATH);
$arr = explode("/",$path);

?>


<div class="menutype"></div>

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
	        <div id="borderleft" ></div>
            <ul class="sidebar-nav">
                <div class="sidebarTop menu-toggle" id="menu-toggle"><span class="glyphicons glyphicons-menu-hamburger" aria-hidden="true" ></span></div>
                <div class="sidebarTop selectTOPmenu" id="menu-selecttop"  ><span class="icon-hive"></span></div>

                <div id="leftmenu-other">
	                <a href="javascript:void(0)" class="btn profileleft" data-toggle="modal" data-target="#ProfileModal"><img class="img-rounded" alt="140x140" src="<? echo loadProfilePic($_SESSION['SESS_PROFILEPIC']); ?>" data-holder-rendered="true" style="width: 60px; height: 60px;"><div class="ripple-container"></div></a>
                </div>
                <div id="leftmenu-other">
                     <a href="javascript:void(0)" id="top_menu_reminders" class="btn" style="color: #999999;font-size: 42px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Notifications"><span class="glyphicons glyphicons-bell"></span><div class="ripple-container"></div></a>
                </div>

                <!-- TOP MENU --->

<div id="topmenu" class="leftmenus">

  <?if(decrypt($_SESSION['SESS_ACCOUNT_ID']) == 'id_member_20160913_120305129000_41074'){?>
    <?

    $db->query("select * from accounts where id = ?");
    $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $menu_account = $db->single();

    ?>
    <li> <a href="<? echo $fullurl?>admin/dashboard.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Dashboard</span> </a> </li>
    <li> <a href="<? echo $fullurl?>admin/rota/rota.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Rota</span> </a> </li>
    <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
    <li> <a href="<? echo $fullurl?>admin/rota/admin/services.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Services</span> </a> </li>
    <?}?>
    <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
      <li> <a href="<? echo $fullurl?>admin/rota/admin/profile_view.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Accounts</span> </a> </li>
    <?}else{?>
      <li> <a href="<? echo $fullurl?>admin/rota/account/view_profile.php?id=<?echo decrypt($_SESSION['SESS_ACCOUNT_ID']);?>"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">My Account</span> </a> </li>
    <?}?>
    <?if($menu_account['account_type'] == 'admin' || $menu_account['account_type'] == 'subadmin'){?>
    <li> <a href="<? echo $fullurl?>admin/rota/admin/approval.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Account Approval</span> </a> </li>
    <li> <a href="<? echo $fullurl?>admin/rota/admin/rota_bid.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Rota Bid</span> </a> </li>
    <li> <a href="<? echo $fullurl?>admin/rota/admin/invoices.php"> <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span> <span class="menuText">Invoices</span> </a> </li>
    <?}?>
  <?}else{?>



    <li>
    <a href="<? echo $fullurl?>admin/dashboard.php">
    <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span>
    <span class="menuText">Dashboard</span>
    </a>
    </li>

    <li>
    <a href="<? echo $fullurl?>admin/courses/index.php">
    <span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span>
    <span class="menuText">Manage Courses</span>
    </a>
    </li>


    <!-- <li><a href="<? echo $fullurl?>admin/dashboard.php"><span class="menuglyph glyphicons glyphicons-dashboard" aria-hidden="true"></span><span class="menuText">Dashboard</span></a></li>
    <li><a href="<? echo $fullurl?>admin/jobs/index.php"><span class="menuglyph glyphicons glyphicons-building" aria-hidden="true"></span><span class="menuText">Jobs</span></a></li>
    <li><a href="<? echo $fullurl?>admin/appointments/index.php"><span class="menuglyph glyphicons glyphicons-calendar" aria-hidden="true"></span><span class="menuText">Appointments</span></a></li>
    <li><a href="<? echo $fullurl?>admin/news/index.php"><span class="menuglyph glyphicons glyphicons-newspaper" aria-hidden="true"></span><span class="menuText">News</span></a></li>
    <li><a href="<? echo $fullurl?>admin/members/index.php"><span class="menuglyph glyphicons glyphicons-user" aria-hidden="true"></span><span class="menuText">Members</span></a></li>
   -->



     <li>
       <a href="<? echo $fullurl?>admin/accounts/learners.php">
         <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
         <span class="menuText">Users</span>
       </a>
     </li>

     <li>
       <a href="<? echo $fullurl?>admin/accounts/company.php">
         <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
         <span class="menuText">Companies</span>
       </a>
     </li>

     <li>
      <a href="<? echo $fullurl?>admin/accounts/company.php">
         <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
         <span class="menuText">Reports</span>
      </a>
     </li>

     <li>
     <a href="<? echo $fullurl?>admin/accounts/company.php">
         <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
         <span class="menuText">Orders</span>
     </a>
     </li>

     <li>
     <a href="<? echo $fullurl?>admin/accounts/company.php">
         <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
         <span class="menuText">Email Templates</span>
     </a>
     </li>

     <li>
     <a href="<? echo $fullurl?>admin/accounts/company.php">
         <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
         <span class="menuText">Config</span>
     </a>
     </li>


    <li>
      <a href="<? echo $fullurl?>admin/users/index.php">
        <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
        <span class="menuText">HR</span>
      </a>
    </li>

    <li>
      <a href="<? echo $fullurl?>admin/credits/manage_credits.php">
        <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
        <span class="menuText">Credits</span>
      </a>
    </li>

    <!-- <li>
      <a href="<? echo $fullurl?>admin/pages/index.php">
        <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
        <span class="menuText">Pages</span>
      </a>
    </li>
    <li>
      <a href="<? echo $fullurl?>admin/pages/staff/index.php">
        <span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span>
        <span class="menuText">Meet The Team</span>
      </a>
    </li> -->

<?}?>
</div>

                <!-- END OF TOP MENU --->

                <!-- END OF PRODUCTS MENU --->
                <div class="sidebarTop selectTOPmenu" id="menu-selectbottom"><span class="icon-hive"></span></div>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
