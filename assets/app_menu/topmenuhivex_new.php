<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    topmenu.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 ?>

  <div class="row">
     <div id="topbar">
         <div class="outer_holder">
             <!-- <a href="<? echo $fullurl ?>"><img src="<? echo $fullurl ?>assets/images/skylogo.png" id="logo"></a> -->

						 <div class="sidebarTop menu-toggle-new" id="menu-toggle"><img class="" src="<? echo $fullurl ?>/assets/images/menu_hamburger.png" style="height:35px; width:auto; padding:6px 8px 6px 8px;" /></div>


				 <div class="btn-group pull-right navrightbtns">
			 	      <!-- <a href="javascript:void(0)" class="btn menu-toggle" id="rightmenutoggle"><span class="glyphicons glyphicons-menu-hamburger"></span><span class="badge"><?echo $rowcount;?></span><div class="ripple-container"></div></a> -->

			 	      <a href="javascript:void(0)" id="top_menu_reminders" class="btn menu-other" data-toggle="tooltip" data-placement="bottom" title="Notifications" data-original-title="Notifications"><img lass="img-rounded" alt="140x140" src="<?echo $fullurl;?>assets/images/IconNotification.png" style="width: 40px; height: 40px;" /><span class="badge" style="position: relative;top: 5px;left: -17px;"><?echo $notecount;?></span><div class="ripple-container"></div></a>
			 	      <!-- <a href="javascript:void(0)" class="btn menu-other" data-toggle="tooltip" data-placement="bottom" title="Settings" data-original-title="Settings"><span class="glyphicons glyphicons-cogwheels"></span><div class="ripple-container"></div></a> -->


							<a href="javascript:void(0)" class="btn menu-other" data-toggle="modal" data-target="#ProfileModal" data-placement="bottom" title="Profile" data-original-title="Profile" id="profilemenu"><img class="img-rounded" alt="140x140" src="<? echo loadProfilePic2($_SESSION['SESS_PROFILEPIC']); ?>" data-holder-rendered="true" style="width: 40px; height: 40px;"><div class="ripple-container"></div></a>


							<a href="javascript:void(0)" class="btn menu-area-type" style="width: auto; font-weight: unset; font-size: 20px!important; text-transform: none;">
								<img src="<? echo $fullurl ?>/assets/images/sb-logo-03-rota.png" style="" />
								<br /><p style="font-size:9px; line-height:0px;">Brought to you by Hive<span style="color:#009688;">X</span></p>
							</a>

			 	</div>
			 	<div id="Notifications_box" class="headline">
			 		<div id="FRMloader" style="color: #000; margin-top: 50px;"><img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Notifications</div>
			  </div>
				<div class="pull-right btn-group navrightbtns">
					<div class="form-group" style="margin:0;">
				</div>
			</div>

     </div>
  </div>
</div>
