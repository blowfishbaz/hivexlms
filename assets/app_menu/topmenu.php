<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    topmenu.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 ?>

  <div class="row">

     <div id="main_header" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">
       <div class="clearfix"></div>
       <div class="page_spacer2"></div>
         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
         <div class="col-lg-10 col-md-11 col-sm-12 col-xs-12">
             <a href="<? echo $fullurl ?>"><img src="<? echo $fullurl ?>assets/images/skylogox.png" id="logo"></a>


        <div id="main_menu_holder">
        <ul id="main_menu">

            <!-- <li><a href="<? echo $fullurl ?>about.php">About</a></li>
            <li><a href="<? echo $fullurl ?>news.php">News</a></li>
            <li><a href="http://www.skyrandd-blog.co.uk/" target="_blank">Blog</a></li>
            <li><a href="<? echo $fullurl ?>meettheteam.php">Meet the Team</a></li>
            <li><a href="<? echo $fullurl ?>training.php">Training</a></li>
            <li><a href="<? echo $fullurl ?>services.php">Services</a></li>
            <li><a href="<? echo $fullurl ?>jobs.php">Jobs</a></li>
            <li><a href="<? echo $fullurl ?>contact.php">Contact Us</a></li>
            <li><a href="/landing" class="btn btn-info" style="color:white !important">Register Here</a></li> -->

        <?


        if(isset($_SESSION['SESS_ACCOUNT_ID'])){?>
        				<div class="btn-group pull-right">
                <?if($_SESSION['SESS_ACCOUNT_Type'] == 'admin'){?>
                <a href="/admin/dashboard.php" class="btn menu_button" type="button" data-toggle="tooltip" data-placement="bottom" title="Admin" data-original-title="Admin"><span class="glyphicons glyphicons-parents"></span></a>
                <?}?>
                    <?if($_SESSION['SESS_ACCOUNT_Type'] != 'admin'){?>

                  <button id="top_menu_reminders" class="btn menu_button" type="button" data-toggle="tooltip" data-placement="bottom" title="Notifications" data-original-title="Notifications"><span class="glyphicons glyphicons-bell"></span></button>
                  <?}?>
                  <button class="btn menu_button" data-toggle="modal" data-target="#ProfileModal" data-placement="bottom" title="Profile" data-original-title="Profile" id="profilemenu">
                  <? echo loadProfilePicwithID(decrypt($_SESSION['SESS_ACCOUNT_ID']));?>
                  <div class="ripple-container"></div></button>
                  <?}else{?>
                  <button class="btn logon_button menu_button" type="button"><span class="glyphicons glyphicons-user"></span></button>

                  <div class="pull-right btn-group navrightbtns">
                  <div class="form-group" style="margin:0;">
                  </div>
                  </div>
                  <?}?>
          </ul>
        </div>
				<div id="Notifications_box" class="headline border2 roundoff background">
			 		<div id="FRMloader" style="color: #000; margin-top: 40px;"><img src="<? echo $fullurl ?>assets/images/loaderweb.gif" alt="loader" width="64" height="51" /><br>
     </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="page_spacer2"></div>
</div>

<div id="mobile_header" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">
  <div class="clearfix"></div>
  <div class="page_spacer2"></div>
    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
        <a href="<? echo $fullurl ?>"><img src="<? echo $fullurl ?>assets/images/skylogo.png" id="logo"></a>
   <button class="btn menu_button menu-mobile_link pull-right" id="menu-mobile_link" type="button" style="margin: 20px 0 0 0;"><span class="glyphicons glyphicons-menu-hamburger"></span></button>
</div>
<div class="clearfix"></div>
<div class="page_spacer2"></div>

<nav id="mobile_menu" class="panel background2" role="navigation" style="position: fixed; top: 0px; bottom: 0px; width: 19.625em; height: 100%; transition: right 300ms ease 0s; right: -19.625em; z-index: 9999; border: none;">
  <div id="list-group" class="background2">
      <a class="list-group-item background2 pull-left color pager logon_button" style="width:50%;"  href="<? echo $fullurl ?>logon.php"><span class="glyphicons glyphicons-user"></span></a>
      <a class="list-group-item background2 pull-left color pager" id="close-menu-mobile_link" style="width:50%;"><span class="glyphicons glyphicons-menu-hamburger"></span></a><div class="clearfix"></div>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>index.php">Home</a>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>about.php">About</a>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>news.php">News</a>
      <a class="list-group-item background2 color" href="http://www.skyrandd-blog.co.uk/" target="_blank">Blog</a>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>meettheteam.php">Meet the Team</a></li>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>training.php">Training</a></li>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>services.php">Services</a></li>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>jobs.php">Jobs</a></li>
      <a class="list-group-item background2 color" href="<? echo $fullurl ?>contact.php">Contact Us</a></li>
  </div>
</nav>

</div>
<div class="clearfix"></div>




<?if($workingon==1){?>
<div class="row"><div class="row"><h4 class="unfortunately" style="color: #fff;background-color: #d9534f;padding: 10px;margin: 0;text-align: center;"><span class="menuglyph glyphicons glyphicons-settings" aria-hidden="true"></span>Under Maintenance<span class="menuglyph glyphicons glyphicons-settings" aria-hidden="true"></span><br /> If page is needed call Blowfish Technology on 01695 550 980</h4></div></div><?}?>
