<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    topmenu.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 ?>
<script src="https://kit.fontawesome.com/932233db91.js" crossorigin="anonymous"></script>
<style>
  .footer_icon:before{
    margin:0 auto;
    display: table;
  }
  .footer_icon{
    width:37px;
  }
</style>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background3">
         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>


        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
            <h3 class="color1">Sky Recruitment</h3>
            <p class="color">Having worked in a range of sectors for many years, our team knows what you’re looking for, and most importantly we know a good candidate when we see one.<br />
              <br />Store Hours: 8am - 6pm, Monday - Friday
              <br />Out of Hours: 7pm - 11pm, Monday - Friday. 7am - 11pm, Saturday - Sunday<br />
            Out of Hours Tel: 07519 121385</p>
            <a href="https://twitter.com/recruitment_sky " target="_blank"><i class="footer_icon fab fa-twitter" style="color: #ffffff;
    font-size: 25px;
    padding: 6px;
    background-color: #54c4ff;
    border-radius: 10px;"></i></a>
            <a href="https://www.linkedin.com/company/42315626" target="_blank"><i class="footer_icon fab fa-linkedin" style="color: #ffffff;
    font-size: 25px;
    padding: 6px;
    background-color: #54c4ff;
    border-radius: 10px;"></i></a>
            <a href="https://www.facebook.com/skyrecruitmentfb" target="_blank"><i class="footer_icon fab fa-facebook-square" style="color: #ffffff;
    font-size: 25px;
    padding: 6px;
    background-color: #54c4ff;
    border-radius: 10px;"></i></a>
            <a href="https://www.instagram.com/sky_recruitment/" target="_blank"><i class="footer_icon fab fa-instagram" style="color: #ffffff;
    font-size: 25px;
    padding: 6px;
    background-color: #54c4ff;
    border-radius: 10px;"></i></a>
        </div>
        <!-- <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12"> -->

        <div class="col-lg-3 col-md-3  col-sm-6 col-xs-12">
            <address class="color pull-left pull-none-xs pull-right-sm">
            <h3 class="color1">Contact</h3>
            88-91 Walton Vale<br />
            Liverpool<br />
            L9 4RQ<br /><br />

            <a href="tel:01519092616" style="color:white;">Tel: 0151 909 2616</a><br />
            <a href="mailto:admin@skyrandd.co.uk" style="color:white;">E: admin@skyrandd.co.uk</a>
            </address>
        </div>
         <!-- <div class="col-lg-0 col-md-0 col-sm-4 col-xs-3"></div> -->
        <!-- <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-4 col-xs-offset-3"> <div class="page_spacer2"></div><div class="page_spacer2"></div><img class="img-responsive" src="<? echo $fullurl ?>assets/images/Comb-logo.png"><div class="page_spacer2"></div><div class="page_spacer2"></div></div> -->

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p style="text-align: center;">
          <a style="text-decoration: underline; color:white;" href="/feedback.php">Feedback Form</a>
          <a style="text-decoration: underline; color:white;" href="/privacy_policy.php" target="_blank">| Privacy Policy</a>
          <a style="text-decoration: underline; color:white;" href="http://www.skyrandd.co.uk/assets/images/Timesheet New.docx" target="_blank">| Time Sheet</a>
          </p>
        </div>

      </div>
<div class="clearfix"></div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background4">
    <div class="page_spacer2"></div>
       <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
       <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
  <!-- <p class="color pull-left"><img class="footer_img" src="<? echo $fullurl ?>assets/images/skylogo.png" alt=""> &copy; <?echo date('Y');?> Sky Recruitment Ltd. </p> -->
  <div class="clearfix-xs"></div>
  <p class="text-right color pull-right pull-left-xs">Created By <a href="http://www.blowfishtechnology.com/">Blowfish Technology </a><img class="footer_img"src="<? echo $fullurl ?>assets/images/blowfish.png" alt=""></p>
    </div>
</div>
