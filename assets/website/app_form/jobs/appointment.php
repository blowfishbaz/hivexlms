<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 $db = new database;
  $db->query("select * from ws_accounts where id = ? ");
  $db->bind(1,$uid);
  $user = $db->single();
 ?>

 <h3>Make An Appointment</h3>

<div id="calendar"></div>
                 <div class="row">
                     <div class="timebox" style="display:none;">
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a1">9:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a2">10:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a3">11:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a4">12:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a5">13:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a6">14:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a7">15:00</button></div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"><div class="page_spacer2"></div><div class="clearfix"></div><button type="button" class="btn btn-block btn-info time_picker" data-id="a8">16:00</button></div>

                     <input type="hidden" name="timepicked" class="timepicked" value=""/>
                     <input type="hidden" name="datepicked" class="datepicked" value=""/>
                 </div>

                 <h3 class="noapps" style="display:none;">Sorry, there are no appointments available on <span>dd-mm-yyyy</span></h3>
                 <div class="clearfix"></div>
                 </div>
<div class="page_spacer2"></div>
<div class="clearfix"></div>
    <button type="button" class="btn btn-lg btn-danger btn-raised" data-dismiss="modal">Close</button>
    <button href="javascript:void(0)" class="btn btn-primary btn-raised pull-right" id="BookApp" style="display:none;">Book Apointment</button>

<div class="clearfix"></div>
