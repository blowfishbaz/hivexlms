<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 $db = new database;
  $db->query("select * from ws_accounts where id = ? ");
  $db->bind(1,$uid);
  $user = $db->single();
 ?>

 <h3>Apply For Job</h3>
<?if($user['status']=='1'){


  $db->query("select * from ws_accounts_info where pid = ? and status = ?");
  $db->bind(1,$uid);
  $db->bind(2,'1');
  $info = $db->single();

  $db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
  $db->bind(1,$uid);
  $db->bind(2,'cv');
  $db->bind(3,'1');
  $cv = $db->single();

  $db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
  $db->bind(1,$uid);
  $db->bind(2,'bds');
  $db->bind(3,'1');
  $bds = $db->single();

?>


<table class="table table-bordered table-striped">
            <tbody>
              <?if($info['infotype']=='talk'){?>
                <tr>
                  <th>CV</th>
                  <td>Talk to our team</td>
                </tr>
                <?}else {?>
              <tr>
                <th>CV</th>
                <td><a href="<?echo $fullurl.$cv['path'].$cv['name'];?>" target="_blank"><?echo $cv['name']?></a></td>
              </tr>
              <?}?>
              <?if($bds['name']!=''){?>
              <tr>
                <th>DBS</th>
                <td><a href="<?echo $fullurl.$bds['path'].$bds['name'];?>" target="_blank"><?echo $bds['name']?></a></td>
              </tr>
              <?}?>
              <tr>
                <th>Driving License</th>
                <td><?echo $info['license'];?></td>
              </tr>

              <tr>
                <th>Conviction</th>
                <td><?echo $info['conviction'];?></td>
              </tr>
            </tbody>
          </table>


    <button type="button" class="btn btn-lg btn-danger btn-raised" data-dismiss="modal">Close</button>
    <button class="btn btn-lg btn-primary btn-raised pull-right" id="job_apply" data-id="<? echo $_POST['id'];?>" type="button">Apply</button>
    <?}else if ($user['status']=='2') {?>

      <h3>Please complete registration to apply</h3>

      <button type="button" class="btn btn-lg btn-danger btn-raised" data-dismiss="modal">Close</button>
      <a href="<?echo $fullurl;?>register.php" target="_blank" class="btn btn-lg btn-primary btn-raised pull-right" type="button">Register</a>

    <?}else{echo 'error 418';}?>
<div class="clearfix"></div>
