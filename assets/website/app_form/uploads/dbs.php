<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
 ?>
 <!-- On Page CSS -->

 <style>
 </style>
 <h3>Upload DBS Check</h3>

    <!-- The file upload form used as target for the file upload widget -->

    <form id="fileupload" action="<? echo $fullurl; ?>/assets/website/app_form/uploads/upload_path.php" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-12">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success btn-raised fileinput-button btn-sm">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files">
                </span>
                <button type="submit" class="btn btn-primary btn-raised btn-sm start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="button" class="btn btn-danger btn-raised btn-sm closes" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i>
                    <span>Close</span>
                </button>

            </div>
            <!-- The global progress state -->
            <div class="col-lg-12 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
    </form>
<div class="clearfix"></div>

<!-- The template to display files available for upload -->

<script>

//////////okExts();

$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(<? echo okFileExts(); ?>)$/i,
         maxFileSize: 10000000,
         done: function (e, data) {
            att_name =data._response.result['files'][0].name;
			      if(data.textStatus=='success'){

                att_name_url="<? echo $fullurl.'uploads/users/'.$user;?>/"+att_name+"";
                $path = '<? echo 'uploads/users/'.$user;?>/';
                $name = att_name
                $type = 'bds';

                				$.ajax({
            						type: "POST",
            						url: "../../../assets/website/app_ajax/uploads/db_upload.php",
            						data: {path:$path,name:$name,type:$type},
            						success: function(msg){

                                            $img_id=msg.trim();
                                            $('#BaseModalL').modal('hide');
                                            $('#dbs_upload').val($name);
                                            $('#dbs_upload_id').val($img_id);


                                            // $('.gallery_holder').append('<div class="holder"><div class="gallery_images" data-src="'+att_name_url+'"> <div class="gallery_images_view" style="background-image:url(\''+att_name_url+'\');"></div> <img src="'+att_name_url+'" style="display: none;"> </div>'+$dropdown1+$img_id+$dropdown2+'</div>');
                                            //$(".gallery_holder").destroy(true);
                                            // $(".gallery_holder").data("lightGallery").destroy(true);
                                            // $('.gallery_holder').lightGallery({selector: '.gallery_images'});
                            						}

            					});
			           }
        }


    });


    $('#fileupload').fileupload()
    .bind('fileuploadstart', function(){
        // disable submit
    })
    .bind('fileuploadprogressall', function (e, data) {
        if (data.loaded == data.total){
            // all files have finished uploading, re-enable submit

        }
    })

});
</script>
</body>
</html>
