<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

  $dateNow = time();
  $date = $_POST["date"];
	$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);

  $date = strtotime ($date.' 2:00:00');


  $db = new database;
	$db->query("SELECT * FROM ws_appointments where app_date = ? AND status = ? ORDER BY appointment ASC");
	$db->bind(1,$date);
  $db->bind(2,'1');
	$appointments = $db->resultset();


    $apps= array();
    foreach ($appointments as $key => $app) {
      array_push($apps,$app['slot']);
      }
      echo json_encode($apps);


?>
