<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

  $thisId = createId('japp');
  $dateNow = time();
  $pid = $_POST["id"];
	$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
  $type = $_POST["type"];

  $db = new database;
	$db->query("SELECT * FROM ws_appointments where pid = ? AND status = ? AND appointment > ?");
	$db->bind(1,$user);
  $db->bind(2,'1');
  $db->bind(3,$dateNow);
	$bookings = $db->resultset();
	$rowcount = $db->rowcount();

if($rowcount== '0'){
  echo 'appointment';
}
else{

  $db->Query("INSERT INTO ws_job_applications (id ,created_date ,pid, user_id, status) values (?,?,?,?,?)");
  $db->bind(1,$thisId);
  $db->bind(2,$dateNow);
  $db->bind(3,$pid);
  $db->bind(4,$user);
  $db->bind(5,'1');
  $db->execute();

  echo 'application';
}

?>
