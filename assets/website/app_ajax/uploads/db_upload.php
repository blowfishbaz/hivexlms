<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $thisId = createId('att');
  $dateNow = time();
  $path = $_POST["path"];
  $name = $_POST["name"];
	$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
  $type = $_POST["type"];

  $db->Query("UPDATE ws_uploads SET status = ? WHERE pid = ? AND type = ? AND status = ?");
  $db->bind(1, '0');
  $db->bind(2, $user);
  $db->bind(3, $type);
  $db->bind(4, '1');
  $db->execute();

  $db = new database;
  $db->Query("INSERT INTO ws_uploads (id ,created_by ,created_date ,path, name, pid, type, status) values (?,?,?,?,?,?,?,?)");
  $db->bind(1,$thisId);
  $db->bind(2,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $db->bind(3,$dateNow);
  $db->bind(4,$path);
  $db->bind(5,$name);
  $db->bind(6,$user);
  $db->bind(7,$type);
  $db->bind(8,'1');
  $db->execute();

sleep(1);
echo $thisId;
?>
