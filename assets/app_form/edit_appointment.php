<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db->query("select * from ws_appointments where id = ?");
 $db->bind(1,$_POST['id']);
 $app = $db->single();

 switch ($app['slot']) {
   case 'a1':$time='09:00:00';break;
   case 'a2':$time='10:00:00';break;
   case 'a3':$time='11:00:00';break;
   case 'a4':$time='12:00:00';break;
   case 'a5':$time='13:00:00';break;
   case 'a6':$time='14:00:00';break;
   case 'a7':$time='15:00:00';break;
   case 'a8':$time='16:00:00';break;
   default:$time='error';break;
 }
 ?>
 <!-- On Page CSS -->

 <style>
 </style>
 <h3>Appointment</h3>
 <table class="table table-bordered table-striped">
     <tbody>

         <tr>
           <th>Date</th>
           <td><?echo date('d-m-Y',$app['app_date'])?></td>
         </tr>
      <tr>
        <th>Slot</th>
        <td><? echo $time;?></td>
      </tr>

     </tbody>
   </table>

 <button type="button" class="btn btn-lg btn-danger btn-raised" data-dismiss="modal">Close</button>
 <button type="button" id="Remove_Appointment" class="btn btn-lg btn-danger btn-raised" data-id="<?echo $_POST['id'];?>">Remove Appointment</button>
 <button type="button" id="Edit_Appointment"class="btn btn-lg btn-primary btn-raised pull-right" data-id="<?echo $app['pid'];?>">Edit Appointment</button>


<div class="clearfix"></div>
