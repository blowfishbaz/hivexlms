<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db->query("select * from ws_appointments where id = ?");
 $db->bind(1,$_POST['id']);
 $app = $db->single();

 $db->query("SELECT hx_jobs.*, ws_job_applications.id as app_id, ws_job_applications.status as app_status FROM ws_job_applications JOIN hx_jobs ON ws_job_applications.pid = hx_jobs.id where ws_job_applications.user_id = ? and (ws_job_applications.status = ? OR ws_job_applications.status = ? OR ws_job_applications.status = ?)");
 $db->bind(1,$app['pid']);
 $db->bind(2,'1');
 $db->bind(3,'2');
 $db->bind(4,'3');
 $jobs = $db->ResultSet();
 $count = $db->rowcount();
 ?>
 <!-- On Page CSS -->

 <style>
 </style>
 <h3>Are you sure you want to remove this appointment?</h3>


<?if($count>'0'){?>
  <h3>Removing this appointment will update all jobs applications to unsuccessful?</h3>
<table class="table table-bordered table-striped">
<tbody>
  <tr>
  <th>Title</th>
  <th>Job Number</th>
  <th>Job Application Status</th>
  </tr>
<?foreach ($jobs as $key => $job) {?>


<tr>
  <td><?echo $job['title'];?></td>
  <td>SR<?echo $job['job_number'];?></td>


<td><? if($job['app_status']=='1'){echo '<span class="label label-info">Waiting</span>';}//
else if($job['app_status']=='2'){echo '<span class="label label-info">Maybe</span>';}//
else if($job['app_status']=='3'){echo '<span class="label label-success">Successful</span>';}//
else if($job['app_status']=='0'){echo '<span class="label label-danger">Unsuccessful</span>';}//
else{}
?></td>
</tr>

<?}?>
</tbody>
</table>
<?}?>
 <button type="button" class="btn btn-lg btn-danger btn-raised" data-dismiss="modal">Close</button>
 <button type="button" id="Remove_Appointment_Ok" class="btn btn-lg btn-danger btn-raised pull-right" data-id="<?echo $_POST['id'];?>">Remove Appointment</button>


<div class="clearfix"></div>
