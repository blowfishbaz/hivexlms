<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    uploadNote.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');



$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);

$files_path = $fullpath.'uploads/site/';
$options=array(
	'upload_dir' => $files_path
);

include( $fullpath."assets/app_classes/upload.class.php");
$upload_handler = new UploadHandler($options);
?>
