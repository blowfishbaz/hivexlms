<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    newAddressForm.php
 * @author     Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
 $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
 ?>
 <!-- On Page CSS -->

 <style>
 </style>
 <h3>Upload CV</h3>

    <!-- The file upload form used as target for the file upload widget -->

    <form id="fileupload" action="<? echo $fullurl; ?>/assets/app_form/uploads/upload_path.php" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-12">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success btn-raised fileinput-button btn-sm">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files">
                </span>
                <button type="submit" class="btn btn-primary btn-raised btn-sm start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="button" class="btn btn-danger btn-raised btn-sm closes" data-dismiss="modal">
                    <i class="glyphicon glyphicon-remove"></i>
                    <span>Close</span>
                </button>

            </div>
            <!-- The global progress state -->
            <div class="col-lg-12 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
    </form>
<div class="clearfix"></div>

<!-- The template to display files available for upload -->

<script>

//////////okExts();

$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(<? echo getFileIcon(); ?>)$/i,
         maxFileSize: 10000000,
         done: function (e, data) {
            att_name =data._response.result['files'][0].name;
			      if(data.textStatus=='success'){

                att_name_url="<? echo $fullurl;?>uploads/site/"+att_name+"";
                $path = 'uploads/site/';
                $name = att_name;
                $type = 'cv';

                				$.ajax({
            						type: "POST",
            						url: "../../../assets/app_ajax/uploads/db_upload.php",
            						data: {path:$path,name:$name,type:$type},
            						success: function(msg){

                                            $img_id=msg.trim();
                                            $('#BaseModalL').modal('hide');
                                            $('#cv_upload').val($name);
                                            $('#cv_upload_id').val($img_id);



                            						}

            					});
			           }
        }


    });


    $('#fileupload').fileupload()
    .bind('fileuploadstart', function(){
        // disable submit
    })
    .bind('fileuploadprogressall', function (e, data) {
        if (data.loaded == data.total){
            // all files have finished uploading, re-enable submit

        }
    })

});
</script>
</body>
</html>
