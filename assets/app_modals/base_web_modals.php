<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    base_modals.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 ?>
<!--- Search Modal --->

<div class="modal" id="SearchModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-body">
            <div class="form-group label-floating is-empty searchGroup">
            <div class="input-group">
              <span class="input-group-addon"><i class="material-icons pull-left">search</i></span>
              <label class="control-label" for="addon3a">HiveX Search</label>
              <input type="text" id="addon3a" class="form-control">

            </div>
            <div class="searchResults col-md-12">

            </div>
            <div class="clearfix"></div>
          <span class="material-input"></span></div>
<div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>


<!--- Profile Modal --->

<div class="modal" id="ProfileModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body user_info">

<?  $userid = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 if($_SESSION['SESS_ACCOUNT_Type']=='web'){

     $db->query("select * from ws_accounts where id = ?");
     $db->bind(1,$userid);
     $account = $db->single();

     $db->query("select * from ws_accounts_info where pid = ? and status = ?");
     $db->bind(1,$userid);
     $db->bind(2, '1');
     $account_info = $db->single();

     $db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
     $db->bind(1,$userid);
     $db->bind(2,'cv');
     $db->bind(3,'1');
     $cv = $db->single();

     $db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
     $db->bind(1,$userid);
     $db->bind(2,'bds');
     $db->bind(3,'1');
     $bds = $db->single();
     ?>
     <!-- THIS ONE -->

<h3 style="margin-top: 0;"><? echo $account['first_name']." ".$account['surname']; ?></h3>




 <ul class="list-unstyled">
 <li><? echo $account['email']; ?></li>
 <li>
   <?if($account_info['infotype']=='talk'){?>
     <tr>
       CV: Talk to our team
     <?}else {?>
   <tr>
     CV: <a href="<?echo $fullurl.$cv['path'].$cv['name'];?>" target="_blank"><?echo $cv['name']?></a>
   <?}?>
 </li>
 <li>
   <?if($bds['name']!=''){?>
   DBS: <a href="<?echo $fullurl.$bds['path'].$bds['name'];?>" target="_blank"><?echo $bds['name']?></a>
   <?}?>
 </li>
 <tr>

 </ul>

 <a href="<? echo $fullurl ?>profile.php"><button class="btn btn-raised btn-primary pull-left" id="edit_profile">View Profile<div class="ripple-container"></div></button>
   <a href="<? echo $fullurl ?>assets/app_ajax/website/auth.php?action=logout" class="btn btn-raised btn-default pull-right"><span class="glyphicons glyphicons-lock" style="margin: -10px 0 -10px 0;"></span> Sign Out<div class="ripple-container"></div></a>
   <div class="clearfix"></div>
 </div>



   <?}else {

       $db->query("select email from accounts where id = ?");
       $db->bind(1,$userid);
       $account = $db->single();
       ?>

   <h3 style="margin-top: 0;"><? echo $account['name']; ?></h3>
   <? echo $account['email']; ?>
   <div class="page_spacer2"></div>
   <div class="clearfix"></div>



   <a href="<? echo $fullurl ?>admin/dashboard.php"><button class="btn btn-raised btn-primary pull-left" id="edit_profile">Admin Page<div class="ripple-container"></div></button>
     <a href="<? echo $fullurl ?>assets/app_ajax/website/auth.php?action=logout" class="btn btn-raised btn-default pull-right"><span class="glyphicons glyphicons-lock" style="margin: -10px 0 -10px 0;"></span> Sign Out<div class="ripple-container"></div></a>
 <?}?>



			<div class="clearfix"></div>
     	</div>



<div class="modal-body edit_own_password" style="display:none;">
  <div class="row">
    <form id="edit_own_passwordForm" name="edit_own_passwordForm" method="post"  enctype="application/x-www-form-urlencoded">
              <div class="col-md-12 col-sm-12">
                <h3>Set New Password</h3>
                <div class="form-group label-floating is-empty">
                  <label for="current_own_password" class="">Current Password</label>
                  <input type="password" class="form-control" id="current_own_password" name="current_own_password" value="" required="yes"><span class="material-input"></span></div>
            </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group label-floating is-empty">
                      <label for="new_own_password" class="">New Password</label>
                      <input type="password" class="form-control" id="new_own_password" name="new_own_password" value="" required="yes"><span class="material-input"></span></div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group label-floating is-empty">
                    <label for="confirm_new_own_password" class="">Confirm New Password</label>
                    <input type="hidden" class="form-control" id="id" name="id" value="<? echo $userid; ?>">
                    <input type="password" class="form-control" id="confirm_new_own_password" name="confirm_new_own_password" value="" required="yes"><span class="material-input"></span></div>
              </div>


<div class="col-md-12 col-sm-12">
    <button type="button" class="btn btn-warning btn-raised" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-raised pull-right" id="saveNewOwnPassword">Save</button>
</div>
<div class="clearfix"></div>
</form>
</div>
</div>

     </div>
  </div>
</div>


<!-- View Activity Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="viewFullActivity">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
	  <div class="modal-body" id="viewFullActivityContent">
		 <div id="FRMloader">
	  <img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>
     Loading
		 </div>

      </div>

    </div>
  </div>
</div>


<!-- Data Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="BaseModalXL">
 <div class="modal-dialog modal-big">
   <div class="modal-content" >
   <div class="modal-body" id="BaseModalXLContent">
    <div id="FRMloader">
     <img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>
      Loading
      </div>
     </div>
   </div>
 </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="BaseModalL">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
	  <div class="modal-body" id="BaseModalLContent">
		 <div id="FRMloader">
	  <img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>
     Loading
		 </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="BaseModalM">
  <div class="modal-dialog">
    <div class="modal-content" >
	  <div class="modal-body" id="BaseModalMContent">
		 <div id="FRMloader">
	  <img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>
     Loading
		 </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="BaseModalS">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
	  <div class="modal-body" id="BaseModalSContent">
		 <div id="FRMloader">
	  <img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>
     Loading Activity
		 </div>
      </div>
    </div>
  </div>
</div>
