<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');



$newid = createId('img');

$account_id = $_POST['pid'];
$path = $_POST['path'];
$file_name = $_POST['filename'];
$doc_type = $_POST['doc_type'];

$db->Query("insert into ws_accounts_upload (id,account_id, type, name, path, created_date, created_by) values (?,?,?,?,?,?,?)");
$db->bind(1,$newid);
$db->bind(2,$account_id);
$db->bind(3,$doc_type);
$db->bind(4,$file_name);
$db->bind(5,$path);
$db->bind(6,time());
$db->bind(7,$account_id);
$db->execute();


?>
