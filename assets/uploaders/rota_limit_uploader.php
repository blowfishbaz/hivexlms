<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$account_id = $_GET['id'];

error_log('*****************************************');

$db = new database;
$db->query("select * from ws_accounts_roles where account_id = ? and status = 1");
$db->bind(1,$account_id);
$user = $db->resultset();

$return_array = array();

foreach ($user as $u) {
  switch($u['role_id']){
    case '1':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        break;
    case '2':
        array_push($return_array,'bls_certificate');
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'university_qualifications');
        array_push($return_array,'safeguarding_adults');
        array_push($return_array,'safeguarding_children');
        array_push($return_array,'CV');
        array_push($return_array,'hep_b_certificate');
        array_push($return_array,'photo_id');
        break;
    case '3':
    case '11':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        array_push($return_array,'reference_from_university');
        break;
    case '4':
        array_push($return_array,'bls_certificate');
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'university_qualifications');
        array_push($return_array,'safeguarding_adults');
        array_push($return_array,'safeguarding_children');
        array_push($return_array,'CV');
        array_push($return_array,'hep_b_certificate');
        array_push($return_array,'photo_id');
        break;
    case '5':
        array_push($return_array,'bls_certificate');
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'safeguarding_adults');
        array_push($return_array,'safeguarding_children');
        array_push($return_array,'hep_b_certificate');
        array_push($return_array,'photo_id');
        array_push($return_array,'phlebotomy_training');
        break;
    case '6':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        break;
    case '7':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        break;
    case '8':
        array_push($return_array,'bls_certificate');
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'university_qualifications');
        array_push($return_array,'safeguarding_adults');
        array_push($return_array,'safeguarding_children');
        array_push($return_array,'photo_id');
        break;
    case '9':
        array_push($return_array,'photo_id');
        break;
    case '10':
        array_push($return_array,'bls_certificate');
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'university_qualifications');
        array_push($return_array,'safeguarding_adults');
        array_push($return_array,'safeguarding_children');
        array_push($return_array,'cv');
        array_push($return_array,'hep_b_certificate');
        array_push($return_array,'photo_id');
        array_push($return_array,'gp_cct_mrcgp_jcptgp');
        break;
      case '12':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        array_push($return_array,'bsc_training_prog_coll_para');
        array_push($return_array,'health_and_care_profession_council');
        break;
      case '13':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        break;
      case '15':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        array_push($return_array,'registered_general_nurse_q');
        array_push($return_array,'nmc_registration');
        array_push($return_array,'bsc_advanced_clinical_practice');
        array_push($return_array,'ind_prescriber_qual');
        break;
      case '16':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        array_push($return_array,'general_phara_council');
        array_push($return_array,'masters_in_pharmacy');
        array_push($return_array,'specialist_through_post_grad');
        array_push($return_array,'independent_presriber');
        array_push($return_array,'cli_min_post_experience');
        break;
      case '17':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        array_push($return_array,'proof_of_experience_phara_tech');
        break;
      case '18':
        array_push($return_array,'enhanced_dbs');
        array_push($return_array,'photo_id');
        array_push($return_array,'phlebotomy_training');
        break;


      }
}


$return_array = array_unique ($return_array);



echo json_encode($return_array);
