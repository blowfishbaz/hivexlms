<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    archiveAddress.php
* @author     Baz Kika
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


ini_set('display_errors', 'On');
$fullpath = $_SERVER['DOCUMENT_ROOT'].'/';
require $fullpath.'vendor/autoload.php';
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

require_once('storage.php');
require_once('hivex_xero.php');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, 'sample.env');
        $dotenv->load();
        $clientId = getenv('CLIENT_ID');
        $clientSecret = getenv('CLIENT_SECRET');
        $redirectUri = getenv('REDIRECT_URI');
        $storage = init_hivex_xero($clientId,$clientSecret,$redirectUri);
        $ex = new ExampleClass();

        $xeroTenantId = (string)$storage->getSession()['tenant_id'];
        $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
        $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        $assetApi = new XeroAPI\XeroPHP\Api\AssetApi(
            new GuzzleHttp\Client(),
            $config
        );

        $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
            new GuzzleHttp\Client(),
            $config
        );

        $projectApi = new XeroAPI\XeroPHP\Api\ProjectApi(
            new GuzzleHttp\Client(),
            $config
        );

        $payrollAuApi = new XeroAPI\XeroPHP\Api\PayrollAuApi(
            new GuzzleHttp\Client(),
            $config
        );


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////// functions /////////////////////////////////////////////////////////
//////////////////////////////// functions /////////////////////////////////////////////////////////
//////////////////////////////// functions /////////////////////////////////////////////////////////


//////////////////////////////week//////////////////////////////////////////////////////
function getNextTimeWeek($startDate, $day, $h, $m, $returnType){
            $newDate  = date("Y-m-d", strtotime($startDate));
            $date = new DateTime($newDate);
            $z = $date->format('Y-m-d');
            //is this date the same day?
            $sd_day = $date->format('l');
            if($sd_day==$day){
                  $date->setTime($h,$m,'00');
                  if($returnType=='TS'){
                            return $date->format('U');
                  }else{
                            return $date->format('Y-m-d H:i:s a');
                  }
            }
            else{
                $date->modify("next $day");
                $date->setTime($h,$m,'00');
                if($returnType=='TS'){
                          return $date->format('U');
                }else{
                          return $date->format('Y-m-d H:i:s a');
                }
            }
}
//////////////////////////////week//////////////////////////////////////////////////////
////////////////////////////////month///////////////////////////////////////////////////
function getNextTimeMonth($startDate, $dt, $h, $m, $ty,$returnType){

          //if last day of month
          if($dt=='lastDay'){
              $newDate  = date("Y-m", strtotime($startDate));
              $date = new DateTime($newDate);
                 if($ty=='quarterly'){
                      $date->modify('first day of +3 months');
                  }
              $date->modify('last day of this month');
              $date->setTime($h,$m,'00');
          }
          else{
              $dt = $dt - 1;
              $newDate  = date("Y-m", strtotime($startDate));
              $date = new DateTime($newDate);
              if($ty=='quarterly'){
                      $date->modify('first day of +3 months');
              }
              $date->modify("+$dt day");
              $date->setTime($h,$m,'00');
          }
          if($returnType=='TS'){
                    return $date->format('U');
          }else{
                    return $date->format('Y-m-d H:i:s a');
          }
}
////////////////////////////////month///////////////////////////////////////////////////
////////////////////////////////year////////////////////////////////////////////////////

function getNextTimeYear($startDate, $dt, $mth, $h, $m, $returnType){

        if($dt=='lastDay'){
                $newDate  = date("Y-m", strtotime($startDate));
                $date = new DateTime($newDate);
                $date->modify("last day of $mth");
                $date->setTime($h,$m,'00');
                $chosenDate =  $date->format('Y-m-d H:i:s a');
                $now = new DateTime();
                $now = $now->format('Y-m-d H:i:s a');

                if($chosenDate < $now){
                    $date->modify("+12 months");

                    if($returnType=='TS'){
                              return $date->format('U');
                    }else{
                              return $date->format('Y-m-d H:i:s a');
                    }
                }
                else{
                    if($returnType=='TS'){
                              return $date->format('U');
                    }else{
                              return $date->format('Y-m-d H:i:s a');
                    }
                }

        }
        else{
                $dt = $dt - 1;
                $newDate  = date("Y-m", strtotime($startDate));
                $date = new DateTime($newDate);
                $date->modify("first day of $mth");
                $date->modify("+$dt day");
                $date->setTime($h,$m,'00');
                $chosenDate =  $date->format('Y-m-d H:i:s a');
                $now = new DateTime();
                $now = $now->format('Y-m-d H:i:s a');

                if($chosenDate < $now){
                    $date->modify("+12 months");

                    if($returnType=='TS'){
                              return $date->format('U');
                    }else{
                              return $date->format('Y-m-d H:i:s a');
                    }
                }
                else{

                    if($returnType=='TS'){
                              return $date->format('U');
                    }else{
                              return $date->format('Y-m-d H:i:s a');
                    }
                }

        }

}
////////////////////////////////year////////////////////////////////////////////////////


function get_account_number($name){

        switch ($name) {
            case 'Sales':
                $r =  "200";
                break;
            case 'Other Revenue':
                $r =  "260";
                break;
            case 2:
                $r =  "";
                break;
        }

        return $r;

}


//////////////////////////////// functions /////////////////////////////////////////////////////////
//////////////////////////////// functions /////////////////////////////////////////////////////////
//////////////////////////////// functions /////////////////////////////////////////////////////////




$db->query("select * from cron_running where id = ?");
$db->bind(1,'1');
$is_running = $db->single();

//fasle means nothing running, true means its already running
if($is_running['status']=='false'){



/////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////cron//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // uncomment when live
      $db->query("update cron_running set status = ? where id = ?");
      $db->bind(1,'true');
      $db->bind(2,'1');
      $db->execute();

      // FAKE FAKE FAKE FAKE FAKE
      // $db->query("select * from hx_reoccuring
      // WHERE (status = ? or status = ?) group by cid, runnextinfo limit 1");
      // $db->bind(1,'1');
      // $db->bind(2,'9');
      // $data = $db->resultset();
      // // get one at a time


      $db->query("select * from hx_reoccuring
      WHERE runnext < ? and (status = ? or status = ?) group by cid, runnextinfo limit 1");
      $db->bind(1,time());
      $db->bind(2,'1');
      $db->bind(3,'9');
      $data = $db->resultset();

      // echo '<pre>';
      // print_r($data);
      // echo '</pre>';



      foreach ($data as $key => $value) {




        //////////////
        //cron activity
        //////////////
        				$db->query("insert into cron_runner (id,time_run) values (?,?)");
        				$db->bind(1,createid('cron'));
        				$db->bind(2,time());
        				$db->execute();
        //////////////
        //cron activity
        //////////////


                      $db->query("select * from hx_reoccuring
                      WHERE cid = ? and runnextinfo = ? and  (status = ? or status = ?)");
                      $db->bind(1,$value['cid']);
                      $db->bind(2,$value['runnextinfo']);
                      $db->bind(3,'1');
                      $db->bind(4,'9');
                      $lines = $db->resultset();


                      $db->query("select * from hx_customers where id = ?");
                      $db->bind(1,$value['cid']);
                      $cus = $db->single();




                      //echo '-----------start-------------<br />';
                      $allLinesArray = array();

                      foreach ($lines as $key => $v) {

                              $invoiceType = $v['invoicetype'];
                              $desc = $v['description'];
                              $accountType = get_account_number($v['account']);
                              $quan = $v['quant'];
                              $price = $v['price'];


                              $lArray = array($invoiceType.' - '.$desc ,$accountType,$quan,$price);
                              array_push($allLinesArray, $lArray);


                              // echo '<pre>';
                              // print_r($v);
                              // echo '</pre>';


                              ///////////////////update hx_reoccuring here//////////////////////
                              if($v['type']=='weekly'){

                                  $nTime = getNextTimeWeek($v['runnextinfo'],$v['day'],$v['hour'],$v['mins'],'all');
                                  $uTime = getNextTimeWeek($v['runnextinfo'],$v['day'],$v['hour'],$v['mins'],'TS');

                              }
                              elseif($v['type']=='monthly' || $v['type']=='quarterly'){

                                  $nTime = getNextTimeMonth($v['runnextinfo'],$v['day'],$v['hour'],$v['mins'],$v['type'],'all');
                                  $uTime = getNextTimeMonth($v['runnextinfo'],$v['day'],$v['hour'],$v['mins'],$v['type'],'TS');

                              }
                              elseif($v['type']=='yearly'){

                                  $nTime = getNextTimeYear($v['runnextinfo'],$v['day'],$v['month'],$v['hour'],$v['mins'],'all');
                                  $uTime = getNextTimeYear($v['runnextinfo'],$v['day'],$v['month'],$v['hour'],$v['mins'],'TS');

                              }
                              else{

                              }


                              $db->query("update hx_reoccuring set runnext = ?, runnextinfo = ?, freq = ? where id = ?");
                              $db->bind(1, $uTime);
                              $db->bind(2, $nTime);
                              $db->bind(3, ($v['freq']+1));
                              $db->bind(4, $v['id']);
                              $db->execute();


                              ///////////////////update hx_reoccuring here//////////////////////


                              // echo $v['type'].' - '.$nTime.'<br />';
                              // echo $v['type'].' - '.$uTime.'<br />';
                              // echo '<br />';

                      }

                      //$ex->hivex_createInvoices($xeroTenantId,$accountingApi,false,$cus['xero_id'],$allLinesArray);


                      //echo '-------------end-------------<br />';





    }



    // uncomment when live
    $db->query("update cron_running set status = ? where id = ?");
    $db->bind(1,'false');
    $db->bind(2,'1');
    $db->execute();


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////cron//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//else its already running
}else{



}





//





?>
