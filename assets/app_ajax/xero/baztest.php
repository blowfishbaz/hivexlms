<?php
    ini_set('display_errors', 'On');
    $fullpath = $_SERVER['DOCUMENT_ROOT'].'/';
    require $fullpath.'vendor/autoload.php';

	require_once('storage.php');
	require_once('hivex_xero.php');

  $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, 'sample.env');
	$dotenv->load();
	$clientId = getenv('CLIENT_ID');
	$clientSecret = getenv('CLIENT_SECRET');
	$redirectUri = getenv('REDIRECT_URI');

	// Storage Classe uses sessions for storing token > extend to your DB of choice
	$storage = new StorageClass();

	// ALL methods are demonstrated using this class
	$ex = new ExampleClass();

	$xeroTenantId = (string)$storage->getSession()['tenant_id'];



	// Check if Access Token is expired
	// if so - refresh token
	if ($storage->getHasExpired()) {
		$provider = new \League\OAuth2\Client\Provider\GenericProvider([
			'clientId'                => $clientId,
			'clientSecret'            => $clientSecret,
			'redirectUri'             => $redirectUri,
        	'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
        	'urlAccessToken'          => 'https://identity.xero.com/connect/token',
        	'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
		]);

	    $newAccessToken = $provider->getAccessToken('refresh_token', [
	        'refresh_token' => $storage->getRefreshToken()
	    ]);
	    // Save my token, expiration and refresh token
         // Save my token, expiration and refresh token
		 $storage->setToken(
            $newAccessToken->getToken(),
            $newAccessToken->getExpires(),
            $xeroTenantId,
            $newAccessToken->getRefreshToken(),
            $newAccessToken->getValues()["id_token"] );
	}

	$config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
	$accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
	    new GuzzleHttp\Client(),
	    $config
	);

	$assetApi = new XeroAPI\XeroPHP\Api\AssetApi(
	    new GuzzleHttp\Client(),
	    $config
	);

	$identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
	    new GuzzleHttp\Client(),
	    $config
	);

	$projectApi = new XeroAPI\XeroPHP\Api\ProjectApi(
	    new GuzzleHttp\Client(),
	    $config
	);

	$payrollAuApi = new XeroAPI\XeroPHP\Api\PayrollAuApi(
	    new GuzzleHttp\Client(),
	    $config
	);

?>
<html>
<head>
	<title>xero-php-oauth2-app</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.min.js"  crossorigin="anonymous"></script>
	<script src="xero-sdk-ui/xero.js"  crossorigin="anonymous"></script>

</head>
<body>

	<div class="container">


		<strong>Result</strong><br>

		<?php

        try {

              $allLinesArray = array();
              $lArray = array('baztest','200','3','10');
              array_push($allLinesArray, $lArray);

              // $lArray = array('75286','baz8 test desc','200','99','12');
              // array_push($allLinesArray, $lArray);

              echo $ex->hivex_createInvoices($xeroTenantId,$accountingApi,false,'c523e12f-8b74-4d3a-bbd8-32d7a2f598b4',$allLinesArray);


        } catch (Exception $e) {
              echo 'Exception when calling Xero API: ', $e->getMessage(), PHP_EOL;
        }




        $itms = $ex->getItem($xeroTenantId,$accountingApi,true);


        echo '<pre>';
        print_r($itms);
        echo '</pre>';

        echo $itms['item_id'];
		?>
	</div>
	</body>
</html>
