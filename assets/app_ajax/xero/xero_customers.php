<?php
  ini_set('display_errors', 'On');
  $fullpath = $_SERVER['DOCUMENT_ROOT'].'/';
  require $fullpath.'vendor/autoload.php';
  include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  require_once('storage.php');
  require_once('hivex_xero.php');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, 'sample.env');
        $dotenv->load();
        $clientId = getenv('CLIENT_ID');
        $clientSecret = getenv('CLIENT_SECRET');
        $redirectUri = getenv('REDIRECT_URI');
        $storage = init_hivex_xero($clientId,$clientSecret,$redirectUri);
        $ex = new ExampleClass();

        $xeroTenantId = (string)$storage->getSession()['tenant_id'];
        $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
        $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $config
        );
        $assetApi = new XeroAPI\XeroPHP\Api\AssetApi(
            new GuzzleHttp\Client(),
            $config
        );

        $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
            new GuzzleHttp\Client(),
            $config
        );

        $projectApi = new XeroAPI\XeroPHP\Api\ProjectApi(
            new GuzzleHttp\Client(),
            $config
        );

        $payrollAuApi = new XeroAPI\XeroPHP\Api\PayrollAuApi(
            new GuzzleHttp\Client(),
            $config
        );


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


?>

<html>
    <head>
      <style>
      table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }

      tr:nth-child(even) {
        background-color: #dddddd;
      }
      </style>
    </head>
    <body>

    <?

    function getContact($xeroTenantId,$apiInstance,$returnObj=false)
    	{
    		$str = '';

    		$new = $this->createContacts($xeroTenantId,$apiInstance, true);
    		$contactId = $new->getContacts()[0]->getContactId();
        //[Contact:Read]

        $result = $apiInstance->getContacts($xeroTenantId, $contactId)->orderBy("updated_date_utc", "DESC")->execute();
        //[/Contact:Read]

    		$str = $str . "Get specific Contact name: " . $result->getContacts()[0]->getName() . "<br>";

    		if($returnObj) {
    			return $result;
    		} else {
    			return $str;
    		}
    	}



      $o =  $ex->getContact($xeroTenantId,$accountingApi,true);


      echo '<div style="width:70%">';
      echo '<table style="width:100%">';
      echo '<tr>';
      echo '<th>Number</th>';
      echo '<th>Name</th>';
      echo '<th>ID</th>';
      echo '<th>Status</th>';
      echo '</tr>';


      foreach ($o as $key => $value) {


				$db->query("select * from hx_customers where xero_id = ?");
				$db->bind(1,$value['contact_id']);
				$xero_id = $db->single();

        if($xero_id){
          $d = 'Added';
        }else{
          $d ='-';
        }

        echo '<tr>';
        echo '<td>'.($key+1).'</td>';
        echo '<td>'.$value['name'].'</td>';
        echo '<td>'.$value['contact_id'].'</td>';
        echo '<td>'.$d.'</td>';
        echo '</tr>';

      }


      echo '</table>';
      echo '</div>';


      //
      // echo '<pre>';
      // print_r($o);
      // echo '</pre>';


    ?>


    </body>
</html>
