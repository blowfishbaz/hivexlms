<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');

  $now = time();
  $new_id = $_POST["new_id"];
  $first_name=strtolower($_POST["first_name"]);
  $surname=strtolower($_POST["surname"]);
  $email=strtolower($_POST["email"]);
  $password=$_POST["new_password"];

  $db = new database;
  $db->query("SELECT email FROM ws_accounts WHERE id = ? OR email = ?");
  $db->bind(1,$new_id);
  $db->bind(2,$email);
  $accounts = $db->single();
  $acount_num = $db->rowcount();



  if($acount_num==0){
      $db = new database;
      $db->Query("INSERT INTO ws_accounts (id ,created_date ,first_name ,surname, email, username, password, attempts,lastattempt,profilepic,status) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
      $db->bind(1,$new_id);
      $db->bind(2,$now);
      $db->bind(3,$first_name);
      $db->bind(4,$surname);
      $db->bind(5,$email);
      $db->bind(6,encrypt($email));
      $db->bind(7,encrypt($password));
      $db->bind(8,'0');
      $db->bind(9,$now);
      $db->bind(10,'');
      $db->bind(11,'2');
      $db->execute();

      $head='';
      $body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #dedede;margin-top: -18px;"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--<![endif]--><div class="wrapper" ><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#ffffff;" align="center"> <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="background-color:#ffffff; padding-top:35px; display: block;border: 0;max-width: 450px;" src="'.$fullurl.'assets/images/skylogo.png" alt="" width="550" height="85"> </div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;text-align:center;padding: 5px;background-color: #5bc3ff;color: white;"><h1>Candidate Registration</h1></div></div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: left;">';

      $body2= 'Hi '.$first_name.' '.$surname.', has filled out a new candidate registration form.';
      $body4= '<br />'.$email.'';
      $body5= '<br /></p>';
      $footer='<!-- Callout Panel --></div><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-bottom: 20px;font-size: 10px;line-height: 24px;text-align: right;"><img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="vertical-align: middle;border: 0;max-width: 900px;" src="'.$fullurl.'assets/images/logo_group_final.png" alt="" width="70" height="70"></p><!-- Callout Panel --></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-bottom: 20px;font-size: 10px;line-height: 24px;text-align: center;">Copyright © *|2019|* *|Sky Recruitment Ltd|*, All rights reserved.</p><!-- Callout Panel --></div></td></tr></tbody></table></body>';

        $htmlContent = $head.$body1.$body2.$body3.$body4.$body5.$footer;
        /////////////send html mail

        $mail = new PHPMailer;
        $mail->Host = $emailhost;
        $mail->Port = 577;
        //$mail->setFrom($staff['email'], $staff['name']);
        $mail->setFrom($email, $first_name.' '.$surname);
        //$mail->addAddress('stephen@blowfishtechnology.com', 'Supplier');
        $mail->addAddress('admin@skyrandd.co.uk', 'Admin');
        //$mail->addAddress($customer['email'],ucwords($customer['nametitle'].' '.$customer['firstname'].' '.$customer['surname']));
        //$mail->addBCC('cps@renewablesolutionsteam.co.uk', 'CPS');

        //$mail->AddCustomHeader( "X-Confirm-Reading-To: cps@renewablesolutionsteam.co.uk" );
        //$mail->AddCustomHeader( "Return-Receipt-To: cps@renewablesolutionsteam.co.uk" );
        //$mail->AddCustomHeader( "Disposition-Notification-To: cps@renewablesolutionsteam.co.uk" );
        $mail->MsgHTML($htmlContent);
        $mail->IsHTML(true);
        $mail->CharSet="utf-8";
        $mail->Subject = 'Candidate Registration';
        $mail->Body = $htmlContent;




        // if(!$mail->send()) {
        //  error_log('email failed');
        // //msg failed to send.
        // } else {
        // //msg sent
        // error_log('email SEND');
        // }

      echo $new_id;
  }else{
    if($accounts['email']==$email){
      echo 'email already exists';
    }
    else{
    echo 'id already exists';
    }
  }
?>
