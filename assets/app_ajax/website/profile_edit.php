<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

  $now = time();
  $new_id = $_POST["new_id"];
  $first_name=strtolower($_POST["first_name"]);
  $surname=strtolower($_POST["surname"]);
  $email=strtolower($_POST["email"]);
  $password=$_POST["new_password"];

  $db = new database;
  $db->query("SELECT email FROM ws_accounts WHERE id = ? OR email = ?");
  $db->bind(1,$new_id);
  $db->bind(2,$email);
  $accounts = $db->single();
  $acount_num = $db->rowcount();



  if($acount_num==0){
      $db = new database;
      $db->Query("INSERT INTO ws_accounts (id ,created_date ,first_name ,surname, email, username, password, attempts,lastattempt,profilepic,status) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
      $db->bind(1,$new_id);
      $db->bind(2,$now);
      $db->bind(3,$first_name);
      $db->bind(4,$surname);
      $db->bind(5,$email);
      $db->bind(6,encrypt($email));
      $db->bind(7,encrypt($password));
      $db->bind(8,'0');
      $db->bind(9,$now);
      $db->bind(10,'');
      $db->bind(11,'2');
      $db->execute();

      echo $new_id;
  }else{
    if($accounts['email']==$email){
      echo 'email already exists';
    }
    else{
    echo 'id already exists';
    }
  }
?>
