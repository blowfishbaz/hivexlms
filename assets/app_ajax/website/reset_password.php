<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');



  $now = time();
  $password=$_POST["new_password"];
  $time=$now-3600;


if (isset($_POST['key']) && $_POST['key'] == getCurrentKey()) {

  $db->query("SELECT * FROM ws_accounts_reset where id= ? and status = ? and created_date > ?");
  $db->bind(1,$_POST['id']);
  $db->bind(2, '1');
  $db->bind(3, $time);
  $rest = $db->single();
  $count = $db->rowcount();

  if ($count >= 1) {



    $db->Query("UPDATE ws_accounts SET password = ?, lastattempt = ? WHERE id = ?");
    $db->bind(1,encrypt($password));
    $db->bind(2, '0');
    $db->bind(3, $rest['pid']);
    $db->execute();

    $db->Query("UPDATE accounts SET password = ?, lastattempt = ? WHERE id = ?");
    $db->bind(1,encrypt($password));
    $db->bind(2, '0');
    $db->bind(3, $rest['pid']);
    $db->execute();


    $db->Query("UPDATE ws_accounts_reset SET status = ? WHERE pid = ?");
    $db->bind(1, '0');
    $db->bind(2, $rest['pid']);
    $db->execute();
     echo 'ok';
  }else{
    echo 'error1';
  }
}else{
  echo 'error2';
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



?>
