<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    authfunctions.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */





	function SetSessions($id,$remember) {
			//create session variables and cookies.
			$db = new database;
			//Get Account
			$db->query("select id,first_name, surname, email, type from ws_accounts where id = ?");
			$db->bind(1,$id);
			$account = $db->single();

			//Get Permissions

			//$_SESSION['SESS_ACCOUNT_PERMISSIONS'] = getPermissions($id);
			$_SESSION['SESS_ACCOUNT_Type'] = $account['type'];
			$_SESSION['SESS_ACCOUNT_ID'] = encrypt($account['id']);
			$_SESSION['SESS_ACCOUNT_NAME'] = $account['first_name'].' '.$account['surname'];
			$_SESSION['SESS_EMAIL'] = $account['email'];
			$_SESSION['SESS_PROFILEPIC'] = $account['profilepic'];

/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////
			// if($account['menuid']!=''){
			// 		$db = new database;
			// 		$db->query("select * from hx_menu where id = ?");
			// 		$db->bind(1,$account['menuid']);
			// 		$menu = $db->single();
			// 		$_SESSION["MENU"] = $menu['typeof'];
			// }
			// else{
					$_SESSION["MENU"] = 'default';
		//	}
/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////

			//Set Cookie for Remember Logon

			if ($remember == 1 ) {

				$db->query("delete from ws_accounts_persistence where id = ?");
				$db->bind(1,$account['id']);
				$db->execute();

				$salt1 = "jskdhfskjdhfsdklf_hdsjklahgdjkl-sjhfgdkfjhgslkdghdfkj_ghjsdklfhdfjkghjd-lkfghjkdjfhgouwhkjdf093845dhjkvsd";
				$salt2 = "dskfjh38434785348564idfghdkfjhg938457347856_sjdkhfgdjkghdfjkgh--dlkfgjdkfgjdf_dfglkhdfkgjhdfkj";
				$date = new DateTime();
				$dateseed = $date->format('YmdH');

				//Key1
				$u_key1 = $dateseed.uniqid(rand(), true);
				$cookieKey1 = encrypt($u_key1);
				$c_key1 = $salt1.$u_key1.$salt2;

				$key1 = sha1($c_key1);

				error_log($salt1);

				setcookie("sesskey1", $cookieKey1, time() + strtotime( '+30 days' ));
				//Key2
				$u_key2 = $dateseed.uniqid(rand(), true);
				$cookieKey2 = encrypt($u_key2);
				$c_key2 = $salt1.$u_key2.$salt2;
				$key2 = sha1($c_key2);
				setcookie("sesskey2", $cookieKey2, time() + strtotime( '+30 days' ));
				//Key3
				$u_key3 = $dateseed.uniqid(rand(), true);
				$cookieKey3 = encrypt($u_key3);
				$c_key3 = $salt1.$u_key3.$salt2;
				$key3 = sha1($c_key3);
				setcookie("sesskey3", $cookieKey3, time() + strtotime( '+30 days' ));
				$persistID = createId('memberp');


				$db->query("insert into ws_accounts_persistence (id,userId,Key1,Key2,Key3,LastLogon) values (?,?,?,?,?,?)");
				$db->bind(1,$persistID);
				$db->bind(2,$account['id']);
				$db->bind(3,$key1);
				$db->bind(4,$key2);
				$db->bind(5,$key3);
				$db->bind(6,time());
				$db->execute();

				$_SESSION['SESS_PERSIST_ID'] = $persistID;
			}
	return 77;
	}

	function checkCookieLogon() {
		$howLongCookie = CookieLength;

		if (isset($_COOKIE['sessKey1']) && isset($_COOKIE['sessKey2']) && isset($_COOKIE['sessKey3'])) {

			$salt1 = "jskdhfskjdhfsdklf_hdsjklahgdjkl-sjhfgdkfjhgslkdghdfkj_ghjsdklfhdfjkghjd-lkfghjkdjfhgouwhkjdf093845dhjkvsd";
			$salt2 = "dskfjh38434785348564idfghdkfjhg938457347856_sjdkhfgdjkghdfjkgh--dlkfgjdkfgjdf_dfglkhdfkgjhdfkj";

			$key1 = decrypt($_COOKIE['sessKey1']);
			$key1 = $salt1.$key1.$salt2;
			$key1 = sha1($key1);

			$key2 = decrypt($_COOKIE['sessKey2']);
			$key2 = $salt1.$key2.$salt2;
			$key2 = sha1($key2);

			$key3 = decrypt($_COOKIE['sessKey3']);
			$key3 = $salt1.$key3.$salt2;
			$key3 = sha1($key3);
			$db = new database;
			$db->query("select id,userId,lastlogon from accounts_persistence where key1 = ? and key2 = ? and key3 = ?");
			$db->bind(1,$key1);
			$db->bind(2,$key2);
			$db->bind(3,$key3);
			$member = $db->single();
			$count = $db->rowcount();

			if ($count >= 1) {

				$lastlogon = $member['lastlogon'];

				$date = date('Y-m-d H:i:s');
				$expire_stamp = strtotime($lastlogon) + (60*$howLongCookie);
				$date = strtotime($date);
				if ($date <= $expire_stamp) {
					//Auto Logon
					setcookie("sesskey1", "", time() - 3600);
					setcookie("sesskey2", "", time() - 3600);
					setcookie("sesskey3", "", time() - 3600);
					$db->query("delete from accounts_persistence where id = ?");
					$db->bind(1,$member['id']);
					$db->execute();
					$complete = SetSessions($member['userId'],1);

					return $complete;


				} else {
					setcookie("sesskey1", "", time() - 3600);
					setcookie("sesskey2", "", time() - 3600);
					setcookie("sesskey3", "", time() - 3600);
					$db->query("delete from accounts_persistence where id = ?");
					$db->bind(1,$member['id']);
					$db->execute();

					return 0;
				}
			} else {
				return 0;
			}

		} else {
			return 0;
		}



	}


 function getPermissions($id) {

		// //$id = decrypt($id);
		// $pa = array();
		//
		// // $db = new database;
		// // $db->query("select * from hx_permissions where staffId = ?");
		// // $db->bind(1,$id);
		// // $permissionsdb = $db->resultset();
		// //
		// // foreach ($permissionsdb as $p) {
		// // 	array_push($pa, $p['area']);
		// // }
		//
		// $db = new database;
		// $db->query("select * from hx_perm where user_id = ? and status = 1");
		// $db->bind(1,$id);
		// $permissionsdb = $db->resultset();
		//
		// foreach ($permissionsdb as $p) {
		// 	array_push($pa, $p['perm_id']);
		// }
		//
		// return $pa;
	}



/*
	function getPermissions($id) {
		$db = new database;
		$db->query("select sectionid,accessType from accounts_permissions where userId = ?");
		$db->bind(1,$id);
		$permissionsdb = $db->resultset();

		$permissions = array();
		$salt = rand();
		$temparray = array(0,encrypt($salt));

		array_push($permissions, $temparray);
		$temparray = "";
		foreach ($permissionsdb as $pdb) {

		$temparray = array($pdb['sectionid'],encrypt($salt.'-'.$pdb['accessType']));

		array_push($permissions, $temparray);

		$temparray = "";
		}
		return $permissions;
	}
*/


	function checkAuth() {
error_log('xxxxxxxxxxxxxxcheckAuthxxxxxxxxxxxxxxxxxxxxxxxxcheckAuthxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcheckAuthxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
		//check for Sessions
		if(isset($_SESSION['SESS_ACCOUNT_ID']) || (trim($_SESSION['SESS_ACCOUNT_ID']) != '')) {

			return 77;

		} else {
			error_log('xxxxxxxxxxxxxxruncheckCookieLogonxxxxxxxxxxxxxxxxxxxxxxxxcheckCookieLogonxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcheckCookieLogonxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');

			$check = checkCookieLogon();
			error_log($check);
			error_log('xxxxxxxxxxxxxxruncheckCookieLogonxxxxxxxxxxxxxxxxxxxxxxxxcheckCookieLogonxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcheckCookieLogonxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');

			return $check;


		}

	}


?>
