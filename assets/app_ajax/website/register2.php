<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

  $now = time();
  $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////Updates///////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  $db->Query("UPDATE ws_addresses SET status = ?, modified_date = ? WHERE pid = ? AND status = ?");
  $db->bind(1, '0');
  $db->bind(2, $now);
  $db->bind(3, $user);
  $db->bind(4, '1');
  $db->execute();

  $db->Query("UPDATE ws_accounts_info SET status = ?, modified_date = ? WHERE pid = ? AND status = ?");
  $db->bind(1, '0');
  $db->bind(2, $now);
  $db->bind(3, $user);
  $db->bind(4, '1');
  $db->execute();

  $db->Query("UPDATE ws_accounts_qualifications SET status = ?, modified_date = ? WHERE pid = ? AND status = ?");
  $db->bind(1, '0');
  $db->bind(2, $now);
  $db->bind(3, $user);
  $db->bind(4, '1');
  $db->execute();

  $db->Query("UPDATE ws_accounts_employment SET status = ?, modified_date = ? WHERE pid = ? AND status = ?");
  $db->bind(1, '0');
  $db->bind(2, $now);
  $db->bind(3, $user);
  $db->bind(4, '1');
  $db->execute();

  $db->Query("UPDATE ws_accounts_references SET status = ?, modified_date = ? WHERE pid = ? AND status = ?");
  $db->bind(1, '0');
  $db->bind(2, $now);
  $db->bind(3, $user);
  $db->bind(4, '1');
  $db->execute();

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$addres_id = createId('add');
$address1=$_POST["address1"];
$address2=$_POST["address2"];
$address3=$_POST["address3"];
$city=$_POST["city"];
$county=$_POST["county"];
$postcode=$_POST["postcode"];
$telephone=$_POST["telephone"];
$mobile=$_POST["mobile"];

$db->Query("INSERT INTO ws_addresses (id, pid, status, created_date ,address1 ,address2, address3, city, county, postcode, telephone, mobile) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,$addres_id);
$db->bind(2,$user);
$db->bind(3,'1');
$db->bind(4,$now);
$db->bind(5,$address1);
$db->bind(6,$address2);
$db->bind(7,$address3);
$db->bind(8,$city);
$db->bind(9,$county);
$db->bind(10,$postcode);
$db->bind(11,$telephone);
$db->bind(12,$mobile);
$db->execute();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$positions=$_POST["positions"];

if(isset($_POST["temporary"]) && isset($_POST["permanent"])){
  $jobtype=$_POST["temporary"].'/'.$_POST["permanent"];
}else{$jobtype=$_POST["temporary"].$_POST["permanent"];}

$acc_info_id=createId('ainfo');
$travle=$_POST["travle"];
$license=$_POST["license"];
$infotype=$_POST["infotype"];

$conviction=$_POST["conviction"];
$conviction_info=$_POST["conviction_info"];

$dbs_upload=$_POST["dbs_upload"];
$dbs_upload_id=$_POST["dbs_upload_id"];
$cv_upload=$_POST["cv_upload"];
$cv_upload_id=$_POST["cv_upload_id"];

$confirm=$_POST["confirm"];


$db->Query("INSERT INTO ws_accounts_info (id, pid, created_date, status, positions, jobtype, travle, license, infotype, conviction, conviction_info) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,$acc_info_id);
$db->bind(2,$user);
$db->bind(3,$now);
$db->bind(4,'1');
$db->bind(5,$positions);
$db->bind(6,$jobtype);
$db->bind(7,$travle);
$db->bind(8,$license);
$db->bind(9,$infotype);
$db->bind(10,$conviction);
$db->bind(11,$conviction_info);
$db->execute();


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



$qualifications=$_POST["qualifications"];
$grade=$_POST["grade"];
$date=$_POST["date"];
$db = new database;

foreach ($qualifications as $key => $val) {
  $acc_qua_id=createId('aqua');
  $db->Query("INSERT INTO ws_accounts_qualifications (id, pid, created_date, status, qualifications, grade, qdate) VALUES (?,?,?,?,?,?,?)");
  $db->bind(1,$acc_qua_id);
  $db->bind(2,$user);
  $db->bind(3,$now);
  $db->bind(4,'1');
  $db->bind(5,$qualifications[$key]);
  $db->bind(6,$grade[$key]);
  $db->bind(7,strtotime ($date[$key].' 12:00:00'));
  $db->execute();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$emp_name=$_POST["emp_name"];
$title=$_POST["emt_title"];
$date_start=$_POST["emp_date_start"];
$date_end=$_POST["emp_date_end"];
$address=$_POST["emp_address"];
$db = new database;

foreach ($emp_name as $key => $val) {
  $acc_qua_id=createId('aemp');
  $db->Query("INSERT INTO ws_accounts_employment (id, pid, created_date, status, name, title, date_start, date_end, address ) VALUES (?,?,?,?,?,?,?,?,?)");
  $db->bind(1,$acc_qua_id);
  $db->bind(2,$user);
  $db->bind(3,$now);
  $db->bind(4,'1');
  $db->bind(5,$emp_name[$key]);
  $db->bind(6,$title[$key]);
  $db->bind(7,strtotime ($date_start[$key].' 12:00:00'));
  $db->bind(8,strtotime ($date_end[$key].' 12:00:00'));
  $db->bind(9,$address[$key]);
  $db->execute();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$ref_name=$_POST["ref_name"];
$contact=$_POST["ref_contact"];
$position=$_POST["ref_position"];
$address=$_POST["ref_address"];
$db = new database;

foreach ($ref_name as $key => $val) {
  $acc_qua_id=createId('aref');
  $db->Query("INSERT INTO ws_accounts_references (id, pid, created_date, status, name, contact, position, address ) VALUES (?,?,?,?,?,?,?,?)");
  $db->bind(1,$acc_qua_id);
  $db->bind(2,$user);
  $db->bind(3,$now);
  $db->bind(4,'1');
  $db->bind(5,$ref_name[$key]);
  $db->bind(6,$contact[$key]);
  $db->bind(7,$position[$key]);
  $db->bind(8,$address[$key]);
  $db->execute();
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$db->Query("UPDATE ws_accounts SET status = ? WHERE id = ?");
$db->bind(1, '1');
$db->bind(2, $user);
$db->execute();


     echo 'ok';

?>
