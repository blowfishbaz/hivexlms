<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $message_title = $_POST['not_title'];
 $notification = $_POST['notification'];

if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){
  $db->query("select * from accounts where user_status = 1");
  $accoutns = $db->resultset();
}else if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
  $db->Query("select * accounts_service_area where pid = ? and status = 1");
  $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $locations = $db->resultset();

  $subadmin_search = ' and location_id in ("0",';

  foreach ($locations as $l) {
    $subadmin_search .='"'.$l.'",';
  }

  $subadmin_search .= '"0")';


  $db->query("select distinct ws_accounts.id
              from ws_accounts
              join ws_accounts_locations
              on ws_accounts_locations.account_id = ws_accounts.id $subadmin_search");
  $accoutns = $db->resultset();
}

foreach ($accoutns as $acc) {
    $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status) values (?,?,?,?,?,?,?,?)");
    $db->bind(1,createid('not'));
    $db->bind(2,$acc['id']);
    $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $db->bind(4,time());
    $db->bind(5,$message_title);
    $db->bind(6,$notification);
    $db->bind(7,'notification');
    $db->bind(8,'1');
    $db->execute();
}

 echo 'ok';
