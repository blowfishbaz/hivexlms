<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $thisId = createId('app');
	$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);

  $type = $_POST["type"].'_temp';
  $pid=$_POST["pid"];

  $db->Query("update accounts set $type = ? where id = ?");
  $db->bind(1, $pid);
  $db->bind(2, $user);
  $db->execute();



?>
