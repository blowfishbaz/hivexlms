<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');

  $id = $_POST["id"];
  $first_name = $acc['first_name'];
  $surname = $acc['surname'];
  $email = $acc['email'];

  $db->query("select * from ws_appointments where id = ?");
  $db->bind(1,$_POST['id']);
  $app = $db->single();

  $db->query("select * from ws_accounts where id = ?");
  $db->bind(1,$app['pid']);
  $acc = $db->single();

  $db = new database;
  $db->Query("UPDATE ws_job_applications SET status= ? WHERE user_id = ? AND (status= ? OR status= ? OR status= ?)");
  $db->bind(1,'0');
  $db->bind(2,$app['pid']);
  $db->bind(3,'1');
  $db->bind(4,'2');
  $db->bind(5,'3');
  $db->execute();

  $db = new database;
  $db->Query("UPDATE ws_appointments SET status= ? WHERE id = ?");
  $db->bind(1,'0');
  $db->bind(2,$id);
  $db->execute();

  $db->query("SELECT hx_jobs.*, ws_job_applications.id as app_id, ws_job_applications.status as app_status FROM ws_job_applications JOIN hx_jobs ON ws_job_applications.pid = hx_jobs.id where ws_job_applications.user_id = ? and (ws_job_applications.status = ? OR ws_job_applications.status = ? OR ws_job_applications.status = ?)");
  $db->bind(1,$app['pid']);
  $db->bind(2,'1');
  $db->bind(3,'2');
  $db->bind(4,'3');
  $jobs = $db->ResultSet();
  $count = $db->rowcount();



  $subject='Sky Recruitment Contact Form';


  //////////////
  /////email////
  //////////////



$head='';
$body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #dedede;margin-top: -18px;"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--<![endif]--><div class="wrapper" ><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#ffffff;" align="center"> <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="background-color:#ffffff; padding-top:35px; display: block;border: 0;max-width: 450px;" src="'.$fullurl.'assets/images/skylogo.png" alt="" width="550" height="85"> </div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;text-align:center;padding: 5px;background-color: #5bc3ff;color: white;"><h1>Appointment Cancelled!</h1></div></div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: left;">';

$body2= 'Hi '.$first_name.', Your appointment and applications have been cancelled for the following...';
$body3= '<br /> To rebook or cancel your appointment contact us on 0151 909 2616';

$body4='';
if($count>'0'){
  $body4= '<h3>Removing this appointment will update all jobs applications to unsuccessful?</h3><table class="table table-bordered table-striped"><tbody> <tr> <th>Title</th> <th>Job Number</th> </tr>';
foreach ($jobs as $key => $job) {

  $body4.='<tr> <td>'.$job['title'].'</td> <td>SR'.$job['job_number'].'</td></tr>';
}
  $body4.='</tbody></table>';
}
$footer='<!-- Callout Panel --></div><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-bottom: 20px;font-size: 10px;line-height: 24px;text-align: right;"><img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="vertical-align: middle;border: 0;max-width: 900px;" src="'.$fullurl.'assets/images/logo_group_final.png" alt="" width="70" height="70"></p><!-- Callout Panel --></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-bottom: 20px;font-size: 10px;line-height: 24px;text-align: center;">Copyright © *|2019|* *|Sky Recruitment Ltd|*, All rights reserved.</p><!-- Callout Panel --></div></td></tr></tbody></table></body>';

  $htmlContent = $head.$body1.$body2.$body3.$body4.$footer;
  /////////////send html mail
$fullurl.'/admin/members/member.php?id='.$user;
  $mail = new PHPMailer;
  $mail->Host = $emailhost;
  $mail->Port = 577;
  //$mail->setFrom($staff['email'], $staff['name']);
  $mail->setFrom('admin@skyrandd.co.uk', 'Admin');
  $mail->addAddress($email, $first_name.' '.$surname);
  $mail->addAddress('admin@skyrandd.co.uk', 'Admin');
  //$mail->addAddress($customer['email'],ucwords($customer['nametitle'].' '.$customer['firstname'].' '.$customer['surname']));
  //$mail->addBCC('cps@renewablesolutionsteam.co.uk', 'CPS');

  //$mail->AddCustomHeader( "X-Confirm-Reading-To: cps@renewablesolutionsteam.co.uk" );
  //$mail->AddCustomHeader( "Return-Receipt-To: cps@renewablesolutionsteam.co.uk" );
  //$mail->AddCustomHeader( "Disposition-Notification-To: cps@renewablesolutionsteam.co.uk" );
  $mail->MsgHTML($htmlContent);
  $mail->IsHTML(true);
  $mail->CharSet="utf-8";
  $mail->Subject = $subject;
  $mail->Body = $htmlContent;




  if(!$mail->send()) {
   error_log('email failed');
   echo 'error';
  //msg failed to send.
  } else {
  //msg sent
  error_log('email SEND');
  echo 'ok';
}
?>
