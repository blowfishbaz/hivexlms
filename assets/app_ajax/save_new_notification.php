<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename   notifications.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $message_title = $_POST['not_title'];
 $notification = $_POST['notification'];
 $who = $_POST['notify_who'];

 $users = $_POST['specific_users'];
 $location = $_POST['location'];
 $roles = $_POST['roles'];

 if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){
   $db->query("select * from accounts where user_status = 1");
   $accoutns = $db->resultset();
 }else if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
   $db->Query("select * accounts_service_area where pid = ? and status = 1");
   $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
   $locations = $db->resultset();

   $subadmin_search = ' and location_id in ("0",';

   foreach ($locations as $l) {
     $subadmin_search .='"'.$l.'",';
   }

   $subadmin_search .= '"0")';


   $db->query("select distinct ws_accounts.id
               from ws_accounts
               join ws_accounts_locations
               on ws_accounts_locations.account_id = ws_accounts.id $subadmin_search");
   $accoutns = $db->resultset();
 }


if($who == 'all'){
  foreach ($accoutns as $acc) {
      $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status) values (?,?,?,?,?,?,?,?)");
      $db->bind(1,createid('not'));
      $db->bind(2,$acc['id']);
      $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $db->bind(4,time());
      $db->bind(5,$message_title);
      $db->bind(6,$notification);
      $db->bind(7,'notification');
      $db->bind(8,'1');
      $db->execute();
  }
}else if($who == 'specific'){
  foreach ($users as $u) {
    $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status) values (?,?,?,?,?,?,?,?)");
    $db->bind(1,createid('not'));
    $db->bind(2,$u);
    $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $db->bind(4,time());
    $db->bind(5,$message_title);
    $db->bind(6,$notification);
    $db->bind(7,'notification');
    $db->bind(8,'1');
    $db->execute();
  }
}else if($who == 'location'){
  $location_search = ' location_id in ("0",';

  foreach ($location as $l) {
    $location_search .='"'.$l.'",';
  }

  $location_search .= '"0")';

  $db->query("SELECT distinct account_id from ws_accounts_locations where $location_search AND STATUS = 1 ");
  $accoutns = $db->resultset();

  foreach ($accoutns as $acc) {
      $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status) values (?,?,?,?,?,?,?,?)");
      $db->bind(1,createid('not'));
      $db->bind(2,$acc['id']);
      $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $db->bind(4,time());
      $db->bind(5,$message_title);
      $db->bind(6,$notification);
      $db->bind(7,'notification');
      $db->bind(8,'1');
      $db->execute();
  }
}else if($who == 'role'){
  $role_search = ' role_id in ("0",';

  foreach ($roles as $r) {
    $role_search .='"'.$r.'",';
  }

  $role_search .= '"0")';

  $db->query("SELECT distinct account_id from ws_accounts_roles where $location_search AND STATUS = 1 ");
  $accoutns = $db->resultset();

  foreach ($accoutns as $acc) {
      $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status) values (?,?,?,?,?,?,?,?)");
      $db->bind(1,createid('not'));
      $db->bind(2,$acc['id']);
      $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $db->bind(4,time());
      $db->bind(5,$message_title);
      $db->bind(6,$notification);
      $db->bind(7,'notification');
      $db->bind(8,'1');
      $db->execute();
  }
}



 echo 'ok';
