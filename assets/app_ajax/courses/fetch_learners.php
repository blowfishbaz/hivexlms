<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("SELECT ws_accounts.*, ws_accounts.id as l_id, ws_accounts_extra_info.*, ws_companies.* FROM ws_accounts
            LEFT JOIN ws_accounts_extra_info
            ON ws_accounts.id = ws_accounts_extra_info.account_id
            LEFT JOIN ws_companies
            ON ws_accounts_extra_info.company_id = ws_companies.id
            WHERE ws_accounts.status = ?
            ORDER BY first_name ASC");
$db->bind(1,1);
$learners = $db->ResultSet();


// echo '<pre>';
// print_r($learners);
// echo '</pre>';

$db->query("SELECT account_id FROM ws_booked_on
            WHERE course_id = ?");
$db->bind(1,$_GET['id']);
$on_this_course = $db->ResultSet();


function flatten(array $array) {
    $return = array();
    array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
    return $return;
}

$on_this_course = flatten($on_this_course);



$db->query("SELECT * FROM lms_course_inst WHERE id = ?");
$db->bind(1,$_GET['id']);
$instance_of_course = $db->single();



$db->query("SELECT * FROM lms_course WHERE id = ?");
$db->bind(1,$instance_of_course['c_id']);
$course = $db->single();


if($instance_of_course['c_type']=="online"){
  $credCost = $course['online_course_credit_price'];
}

if($instance_of_course['c_type']=="vc"){
  $credCost = $course['vc_course_credit_price'];
}

if($instance_of_course['c_type']=="f2f"){
  $credCost = $course['f2f_course_credit_price'];
}


?>

<form id="save_add_learning_form">



<h4 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Add Learner</h4>

<div class="col-lg-12" style="margin-top:15px;">
  <p style=" background-color: #dfe4fd; padding: 10px; font-size: 20px;">Course Cost - <? echo $credCost; ?> Credits</p>
</div>

<div class="col-lg-12">
      <div class="form-group is-empty">
            <label for="add_learner_sel" class="control-label" style="margin-top:0px;">Choose Learner</label>
            <select class="form-control" data-show-subtext="true" data-live-search="true" style="" name="add_learner_sel" id="add_learner_sel">
            <option class="" value="" selected=""></option>
            <? foreach ($learners as $key => $value) {
              if(!in_array($value['l_id'],$on_this_course)){

                if($value['company_name']==''){
                  $c_name = "";
                }else{
                  $c_name = " - ".$value['company_name'];
                }

                if($credCost>=$value['credit'] && $credCost != 0){
                  $_d = 'disabled';
                }
                else{
                  $_d = '';
                }

            ?>
            <option class="" value="<? echo $value['l_id']; ?>" <? echo $_d; ?>><? echo $value['first_name']; ?> <? echo $value['surname']; ?> <? echo $c_name; ?></option>
          <? } } ?>
            </select>
            <span class="material-input"></span>
      </div>
      <br>
</div>
<input type="hidden" name="coursecost" value="<? echo $credCost; ?>"/>
<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="course_status" class="control-label" style="margin-top:0px;">Course Status</label>
        <select class="form-control" name="course_status" style="" id="course_status">
            <option></option>
                        <option class="" value="1">Assigned Course</option>
                        <option class="" value="2">In Progress</option>
                        <option class="" value="3">Finished Course</option>
                        <option class="" value="4">Completed Course</option>
        </select>
        <span class="material-input"></span>
    </div>
</div>

<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="learner_pass_score" class="control-label">Pass Score (%): </label>
        <input type="text" class="form-control" name="learner_pass_score" id="learner_pass_score" value="">
        <span class="material-input"></span>
      </div>
</div>

<button type="button" data-id="<? echo $_GET['id']; ?>" class="btn btn-sm btn-success btn-raised pull-right save_add_learning">Save<div class="ripple-container"></div></button>

</form>

<div class="clearfix"></div>
