<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from lms_course where id = ?");
$db->bind(1,$_GET["cid"]);
$c = $db->single();

$db->query("select * from lms_resource where id = ?");
$db->bind(1,$_GET["id"]);
$r = $db->single();


$online=false;
$vc=false;
$f2f=false;

if($c['online']==1){
      $online=true;
}
if($c['vc']==1){
      $vc=true;
}
if($c['f2f']==1){
      $f2f=true;
}

$ronline="";
$rvc="";
$rf2f="";

if($r['online']==1){
      $ronline='checked';
}
if($r['vc']==1){
      $rvc='checked';
}
if($r['f2f']==1){
      $rf2f='checked';
}


?>

<h3>Edit Resource - <? echo $r['rtitle']; ?></h3>



<div class="col-md-12 col-sm-12 text-center res_options" style="margin-top:15px;">
      <div class="col-md-4 col-sm-4 text-center">
              <button type="button" class="btn btn-sm btn-info btn-raised res_edit_content" >
              Edit Content
              </button>
      </div>
      <div class="col-md-4 col-sm-4 text-center">
              <button type="button" class="btn btn-sm btn-info btn-raised res_avail_content" >
              Change Availability
              </button>
      </div>
      <div class="col-md-4 col-sm-4 text-center">
              <button type="button" class="btn btn-sm btn-info btn-raised res_delete_content" >
              Delete
              </button>
      </div>
</div>


<div class="col-md-12 col-sm-12 res_content_div" style="margin-top:15px; display:none;">
    <form id="res_content_form">
          <input type="hidden" name="rid" value="<? echo $_GET["id"]  ?>">
          <div class="form-group label-floating">
                <label for="resource_name" class="control-label">Resource Name</label>
                      <input type="text" class="form-control" name="resource_name" required="yes" value="<? echo $r['rtitle'] ?>" required >
                <span class="material-input"></span>
          </div>
          <div class="form-group">
                <label class=“”>Resource Description</label>
                <textarea name="resource_desc" class="form-control" rows="3" id="page_info"><? echo $r['rdesc'] ?></textarea>
          </div>
          <button type="button" class="btn btn-sm btn-success btn-raised pull-right save_res_details" style="margin-top:25px;">
          Save
          </button>
      </form>
      <button type="button" class="btn btn-sm btn-defualt btn-raised res_options_cancel" style="margin-top:25px;">
      Cancel
      </button>
</div>



<div class="col-md-12 col-sm-12 res_avail_div" style="margin-top:15px; display:none;">
    <form id="res_avail_form">
          <input type="hidden" name="rid" value="<? echo $_GET["id"]  ?>">

          <div class="row">


          <? if($online){ ?>
          <div class="col-md-4 col-sm-4">
                <label class="checkbox-inline">
                      <input type="checkbox" name="online" id="ch_on" <? echo $ronline; ?>> Online
                </label>
          </div>
          <? } ?>

          <? if($vc){ ?>
          <div class="col-md-4 col-sm-4">
                <label class="checkbox-inline">
                        <input type="checkbox" name="vc" id="ch_vc" <? echo $rvc; ?>> Virtual Classroom
                </label>
          </div>
          <? } ?>

          <? if($f2f){ ?>
          <div class="col-md-4 col-sm-4">
                <label class="checkbox-inline">
                  <input type="checkbox" name="f2f" id="ch_f2" <? echo $rf2f; ?>> Face To Face
                </label>
          </div>
          <? } ?>
          </div>

          <div class="clearfix"></div>
          <br />
          <br />

          <button type="button" class="btn btn-sm btn-success btn-raised pull-right save_avail_details" style="margin-top:25px;">
          Save
          </button>
      </form>
      <button type="button" class="btn btn-sm btn-defualt btn-raised res_options_cancel" style="margin-top:25px;">
      Cancel
      </button>
</div>



<div class="col-md-12 col-sm-12 res_delete_div" style="margin-top:15px; display:none;">
    <form id="res_delete_form">
          <input type="hidden" name="rid" value="<? echo $_GET["id"]  ?>">

          <div class="row">
            <p>
              Are you sure you want to delete this resource?
            </p>
          </div>

          <div class="clearfix"></div>
          <br />
          <br />

          <button type="button" class="btn btn-sm btn-success btn-raised pull-right save_delete_details" style="margin-top:25px;">
          Confirm
          </button>
      </form>
      <button type="button" class="btn btn-sm btn-defualt btn-raised res_options_cancel" style="margin-top:25px;">
      Cancel
      </button>
</div>



<div class="clearfix"></div>
<br />
<br />

<div class="col-md-12 col-sm-12">
    <div class="modal-footer" style="margin-bottom:15px;border-top: 1px solid #d0c8c8;padding-bottom: 9px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</div>
<div class="clearfix"></div>
