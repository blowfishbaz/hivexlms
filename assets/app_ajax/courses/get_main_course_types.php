<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from lms_course where id = ?");
$db->bind(1,$_POST["id"]);
$c = $db->single();

$nums = 0;

if($c['online']==1){
  $nums++;
}
if($c['vc']==1){
  $nums++;
}
if($c['f2f']==1){
  $nums++;
}

$db->query("select * from lms_upload where cid = ? and type = ?");
$db->bind(1,$_POST["id"]);
$db->bind(2,"resource");
$ups = $db->resultset();


$onlineIdArray = array();
$onlineNameArray = array();

$vcIdArray = array();
$vcNameArray = array();

$f2fIdArray = array();
$f2fNameArray = array();

foreach ($ups as $key => $v) {


          $db->query("select * from lms_resource where rid = ?");
          $db->bind(1,$v["id"]);
          $r = $db->single();

          $rnums = 0;
          $rtype = 0;

          if($r['online']==1){
                $rnums++;
                $rtype = 'online';
          }
          if($r['vc']==1){
                $rnums++;
                $rtype = 'vc';
          }
          if($r['f2f']==1){
                $rnums++;
                $rtype = 'f2f';
          }


          if($rnums==1){


                if($rtype=='online'){
                  array_push($onlineIdArray, $r['id']);
                  array_push($onlineNameArray, $r['rtitle']);
                }

                if($rtype=='vc'){
                  array_push($vcIdArray, $r['id']);
                  array_push($vcNameArray, $r['rtitle']);
                }

                if($rtype=='f2f'){
                  array_push($f2fIdArray, $r['id']);
                  array_push($f2fNameArray, $r['rtitle']);
                }



          }


}


if(count($onlineNameArray)>0){
    $online_resources = 'true';
}else{
    $online_resources = 'false';
}

if(count($vcNameArray)>0){
    $vc_resources = 'true';
}else{
    $vc_resources = 'false';
}

if(count($f2fNameArray)>0){
    $f2f_resources = 'true';
}else{
    $f2f_resources = 'false';
}


?>



<div class="col-md-12 col-sm-12 course_opts">

      <p><strong style="margin-right:10px; font-size:18px;">Course Availability: </strong></p>

      <br />
      <br />

      <div class="col-md-4 col-sm-4 text-center">

                          <? if($c['online']==1){ ?>
                                <? if($nums==1){ ?>
                                      <button type="button" class="btn btn-sm btn-danger btn-raised disabled" disabled>
                                      Remove Online Course
                                      </button>
                                <? }else { ?>
                                      <button type="button" class="btn btn-sm btn-danger btn-raised " id="remove_online" data-type="<? echo $online_resources; ?>">
                                      Remove Online Course
                                      </button>
                                <?} ?>
                          <? }else{ ?>
                                      <button type="button" class="btn btn-sm btn-primary btn-raised" id="make_online">
                                      Make Online Course
                                      </button>
                          <? } ?>

      </div>

      <div class="col-md-4 col-sm-4 text-center">

                    <? if($c['vc']==1){ ?>
                            <? if($nums==1){ ?>
                                          <button type="button" class="btn btn-sm btn-danger btn-raised disabled" disabled>
                                          Remove Virtual Classroom Course
                                          </button>
                                    <? }else { ?>
                                          <button type="button" class="btn btn-sm btn-danger btn-raised " id="remove_vc" data-type="<? echo $vc_resources; ?>">
                                          Remove Virtual Classroom Course
                                          </button>
                                    <?} ?>
                            <? }else{ ?>
                                          <button type="button" class="btn btn-sm btn-primary btn-raised " id="make_vc">
                                          Make Virtual Classroom Course
                                          </button>
                    <? } ?>

      </div>


      <div class="col-md-4 col-sm-4 text-center">

                    <? if($c['f2f']==1){ ?>
                                  <? if($nums==1){ ?>
                                        <button type="button" class="btn btn-sm btn-danger btn-raised disabled" disabled >
                                        Remove Face To Face Course
                                        </button>
                                  <? }else { ?>
                                        <button type="button" class="btn btn-sm btn-danger btn-raised " id="remove_f2f" data-type="<? echo $f2f_resources; ?>">
                                        Remove Face To Face Course
                                        </button>
                                  <?} ?>
                          <? }else{ ?>
                                        <button type="button" class="btn btn-sm btn-primary btn-raised " id="make_f2f">
                                        Make Face To Face Course
                                        </button>
                    <? } ?>

      </div>
<div class="clearfix"></div>
<br />
<br />
</div>


<div class="col-md-12 col-sm-12">

    <div class="col-md-12 col-sm-12">


      <div class="add_course">

      </div>

      <div class="delete_online_r r_div" style="display:none;">

              <div class="col-md-12 col-sm-12">
                <p>Online - </p>
                <? if($online_resources=='true'){ ?>
                <h3 style="margin-bottom:35px;"> These resources will be removed!</h3>
                <ul>
                    <? foreach ($onlineNameArray as $key => $value) {
                          echo '<li><strong>'.strtoupper($value).'</strong></li>';
                        }

                        echo '</ul>';

                        echo "<form id='delete_online_form'>";
                        echo "<input type='hidden' name='courseId' value='".$_GET['id']."' />";
                        foreach ($onlineIdArray as $key => $value) {
                              echo "<input type='hidden' name='deleteids[]' value='".$value."' />";
                            }
                        echo "</form>";
                    ?>
                        <button type="button" class="btn btn-lg btn-danger btn-raised pull-right" id="remove_online_confirm">
                        Confirm Delete & Remove All Resources
                        </button>
                    <? }else {
                        echo "<form id='delete_online_form'>";
                        echo "<input type='hidden' name='courseId' value='".$_GET['id']."' />";
                        echo "</form>";
                        ?>
                        <button type="button" class="btn btn-lg btn-danger btn-raised pull-right" id="remove_online_confirm">
                        Confirm Delete
                        </button>
                    <? } ?>
                </div>

                <button type="button" class="btn btn-xs cancel_div">Cancel</button>

      </div>

      <div class="delete_vc_r r_div" style="display:none;">

        <div class="col-md-12 col-sm-12" >
          <p>Vitual Classroom - </p>
          <? if($vc_resources=='true'){ ?>
          <h3 style="margin-bottom:35px;"> These resources will be removed!</h3>
          <ul>
              <? foreach ($vcNameArray as $key => $value) {
                    echo '<li><strong>'.strtoupper($value).'</strong></li>';
                  }

                  echo '</ul>';

                  echo "<form id='delete_vc_form'>";
                  echo "<input type='input' name='courseId' value='".$_GET['id']."' />";
                  foreach ($vcIdArray as $key => $value) {
                        echo "<input type='hidden' name='deleteids[]' value='".$value."' />";
                      }
                  echo "</form>";

              ?>
                  <button type="button" class="btn btn-lg btn-danger btn-raised pull-right" id="remove_vc_confirm">
                  Confirm Delete & Remove All Resources
                  </button>
              <? }else {

                echo "<form id='delete_vc_form'>";
                echo "<input type='hidden' name='courseId' value='".$_GET['id']."' />";
                echo "</form>";

                ?>
                  <button type="button" class="btn btn-lg btn-danger btn-raised pull-right" id="remove_vc_confirm">
                  Confirm Delete
                  </button>
              <? } ?>
          </div>

          <button type="button" class="btn btn-xs cancel_div">Cancel</button>
      </div>


      <div class="delete_f2f_r r_div" style="display:none;">

        <div class="col-md-12 col-sm-12">
          <p>Face To Face - </p>
          <? if($f2f_resources=='true'){ ?>
          <h3 style="margin-bottom:35px;"> These resources will be removed!</h3>
          <ul>
              <? foreach ($f2fNameArray as $key => $value) {
                    echo '<li><strong>'.strtoupper($value).'</strong></li>';
                  }

                  echo '</ul>';
                  echo "<form id='delete_f2f_form'>";
                  echo "<input type='hidden' name='courseId' value='".$_GET['id']."' />";
                  foreach ($f2fIdArray as $key => $value) {
                        echo "<input type='hidden' name='deleteids[]' value='".$value."' />";
                      }
                  echo "</form>";

              ?>
                  <button type="button" class="btn btn-lg btn-danger btn-raised pull-right" id="remove_f2f_confirm">
                  Confirm Delete & Remove All Resources
                  </button>
              <? }else {

                echo "<form id='delete_f2f_form'>";
                echo "<input type='hidden' name='courseId' value='".$_GET['id']."' />";
                echo "</form>";

                ?>
                  <button type="button" class="btn btn-lg btn-danger btn-raised pull-right" id="remove_f2f_confirm">
                  Confirm Delete
                  </button>
              <? } ?>
          </div>
          <button type="button" class="btn btn-xs cancel_div">Cancel</button>
      </div>

    </div>
    <div class="clearfix"></div>
    <br />
</div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12">
                    <div class="modal-footer" style="margin-bottom:15px;border-top: 1px solid #d0c8c8;padding-bottom: 9px;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>

                <div class="clearfix"></div>
