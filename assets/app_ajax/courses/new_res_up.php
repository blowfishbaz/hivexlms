<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

?>


<div class="col-md-12 col-sm-12">
    <p><strong>Add Featured Image</strong></p>
    <input type="hidden" name="featured_image_up" id="in_featured_image_up" value="empty" />
    <form id="featured_image_up" action="../../assets/app_ajax/courses/upload_file.php" method="POST" enctype="multipart/form-data">
        <div class="row fileupload-buttonbar">
            <div class="col-lg-12">
                <span class="btn btn-success btn-raised fileinput-button btn-sm">
                    <span>Add Featured Image</span>
                    <input type="file" name="files">
                </span>
                <span class="fileupload-process"></span>
            </div>
            <div class="col-lg-12 fileupload-progress fade">
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
    </form>
</div>

<div class="clearfix">

</div>

<script>


$(function () {
$('#featured_image_up').fileupload({
dataType: 'json',
acceptFileTypes: /(\.|\/)(<? echo okExts(); ?>)$/i,
maxFileSize: 10000000,
autoUpload: true,
done: function (e, data) {
if(data.textStatus=='success'){

        att_name =data._response.result['files'][0].name;
        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////
        $.ajax({
              type: "POST",
              url: $fullurl+"assets/app_ajax/admin/new_res_db.php",
              data: {uptype: 'resource', cid : '<? echo $_GET['id'];?>', filename: att_name}, // serializes the form's elements.
              success: function(msg){
              },error: function (xhr, status, errorThrown) {
              setTimeout(function(){alert('Error');},300);
              }
          });
          //////////////////////////////////////////////////////////
          //////////////////////////////////////////////////////////
          //////////////////////////////////////////////////////////


          if ( $( ".template-upload" ).length>1 ) {

              $( ".template-upload" ).first().remove();

          }

}
}
});

$('#featured_image_up').fileupload()
.bind('fileuploadstart', function(){
      Messenger().post({
            message: 'Resource Added',
            showCloseButton: false
      });
      setTimeout(function(){
      location.reload();
      },600);
})
});

</script>
