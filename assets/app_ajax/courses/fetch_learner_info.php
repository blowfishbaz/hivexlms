<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("SELECT ws_accounts.*, ws_accounts.id as l_id, ws_accounts_extra_info.*, ws_booked_on.*, ws_booked_on.id as bid, ws_booked_on.status as c_status FROM ws_accounts
            JOIN ws_accounts_extra_info
            ON ws_accounts.id = ws_accounts_extra_info.account_id
            JOIN ws_booked_on
            ON ws_accounts.id = ws_booked_on.account_id
            WHERE ws_accounts.status = ? and ws_booked_on.id = ?
            ORDER BY first_name ASC");
$db->bind(1,1);
$db->bind(2,$_GET['id']);
$learners = $db->single();

?>

<form id="save_add_learning_edit_form">


<h4 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Learner Information
  <button type="button" style="margin-top:0px;" data-book="<? echo $_GET['id'] ?>" data-id="<? echo $_GET['id'] ?>" class="btn btn-xs btn-danger btn-raised pull-right remove_learning_edit">Remove & Refund<div class="ripple-container"></div></button>
  <div class="clearfix"></div>
</h4>

<div class="confirm_remove_div" style=" background: #ffe2e2; display:none;">

    <div class="col-lg-6 pager">
          <button type="button" class="btn btn-xs btn-default btn-raised cancel_remove_learning_edit">
            Cancel
            <div class="ripple-container"></div>
          </button>
    </div>


    <div class="col-lg-6 pager">
          <button type="button" data-learner="<? echo $learners['l_id'] ?>" data-book="<? echo $_GET['id'] ?>" class="btn btn-xs btn-danger btn-raised confirm_remove_learning_edit">
            Confirm Remove & Refund
            <div class="ripple-container"></div>
          </button>
    </div>

    <div class="clearfix"></div>



</div>


<div class="col-lg-12">
      <div class="form-group is-empty">
            <label for="add_learner_sel" class="control-label" style="margin-top:0px;"> Learner</label>
            <select class="form-control" data-show-subtext="true" data-live-search="true" style="" name="add_learner_sel" id="add_learner_sel">
            <option class="" value="<? echo $learners['l_id']; ?>"><? echo $learners['first_name']; ?> <? echo $learners['surname']; ?></option>
            </select>
            <span class="material-input"></span>
      </div>
      <br>
</div>

<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="course_status" class="control-label" style="margin-top:0px;">Course Status</label>
        <select class="form-control" name="course_status" style="" id="course_status">
            <option></option>
                        <option class="" value="1" <? if($learners['c_status']=='1'){ echo "selected";} ?>>Assigned Course</option>
                        <option class="" value="2" <? if($learners['c_status']=='2'){ echo "selected";} ?>>In Progress</option>
                        <option class="" value="3" <? if($learners['c_status']=='3'){ echo "selected";} ?>>Finished Course</option>
                        <option class="" value="4" <? if($learners['c_status']=='4'){ echo "selected";} ?>>Completed Course</option>
        </select>
        <span class="material-input"></span>
    </div>
</div>

<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="learner_pass_score" class="control-label">Pass Score (%): </label>
        <input type="text" class="form-control" name="learner_pass_score" id="learner_pass_score" value="<? echo $learners['pass_score']; ?>">
        <span class="material-input"></span>
      </div>
</div>

<button type="button" data-book="<? echo $_GET['id'] ?>" data-id="<? echo $_GET['id'] ?>" class="btn btn-sm btn-success btn-raised pull-right save_add_learning_edit">Save<div class="ripple-container"></div></button>


<div class="clearfix"></div>
<br />
<h4 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Registration</h4>
<br />
<?


$db->query("select * from lms_course_inst_date where c_id = ? and c_date < ?");
$db->bind(1,$learners["course_id"]);
$db->bind(2, time());
$d = $db->ResultSet();

foreach ($d as $key => $value){

  $db->query("select * from lms_course_inta_date_reg where book_id = ? and account_id = ?");
  $db->bind(1,$value["id"]);
  $db->bind(2,$learners["l_id"]);
  $lesson = $db->single();


  if($lesson['id']!=''){
    $pres = '<span class="label label-success pull-right">Present</span>';
  }else{
    $pres = '<span class="label label-danger pull-right">Missed</span>';
  }

?>
  <div class="col-lg-12" style="border-bottom:1px #b8b8b8 solid; padding-bottom:10px; margin-bottom:10px;">
    <div class="col-lg-4">
    <? echo date("d-m-Y", $value['c_date']); ?> <? echo date("H:i", $value['c_date']); ?>
    </div>
    <div class="col-lg-8">
      <strong><? echo ucwords($value['title']);?></strong>

      <? echo $pres; ?>

    </div>
  </div>
  <?
  }
  ?>





</form>

<div class="clearfix"></div>
