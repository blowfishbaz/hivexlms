<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$cid = $_POST['cid'];
$filename = $_POST['filename'];
$path = $_POST['path'];
$pid = $_POST['pid'];

$time = time();
$created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);



$db->Query("insert into ws_courses_content (id, coures_id, created_date, created_by, status, zip_name, zip_path) value (?,?,?,?,?,?,?)");
$db->bind(1,$pid);
$db->bind(2,$cid);
$db->bind(3,$time);
$db->bind(4,$created_by);
$db->bind(5,'2');
$db->bind(6,$filename);
$db->bind(7,$path);
$db->execute();




?>
