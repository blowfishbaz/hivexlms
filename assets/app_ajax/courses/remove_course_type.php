<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

if(isset($_POST['deleteids'])){
      foreach ($_POST['deleteids'] as $key => $value) {

            $db->query("select * from lms_resource where id = ?");
            $db->bind(1,$value);
            $res = $db->single();

            $db->query("select * from lms_upload where id = ?");
            $db->bind(1,$res['rid']);
            $ups = $db->single();

            $path = '../../../uploads/courses/';
            unlink($path.$ups['link']);

            $db->query("delete from lms_resource where id = ?");
            $db->bind(1,$value);
            $db->execute();

            $db->query("delete from lms_upload where id = ?");
            $db->bind(1,$res['rid']);
            $db->execute();
      }
}

if($_GET['type']=='online'){
      $db->Query("UPDATE lms_course SET online = ? WHERE id = ?");
      $db->bind(1,'0');
      $db->bind(2,$_POST['courseId']);
      $db->execute();
}

if($_GET['type']=='vc'){
      $db->Query("UPDATE lms_course SET vc = ? WHERE id = ?");
      $db->bind(1,'0');
      $db->bind(2,$_POST['courseId']);
      $db->execute();
}

if($_GET['type']=='f2f'){
      $db->Query("UPDATE lms_course SET f2f = ? WHERE id = ?");
      $db->bind(1,'0');
      $db->bind(2,$_POST['courseId']);
      $db->execute();
}

echo 'ok';


?>
