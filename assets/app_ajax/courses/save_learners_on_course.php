<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

//debited
//credited
//refunded

$course = $_GET['id'];
$learner = $_POST['add_learner_sel'];
$status = $_POST['course_status'];
$score = $_POST['learner_pass_score'];



$db->query("SELECT * FROM lms_course_inst WHERE id = ?");
$db->bind(1,$_GET['id']);
$instance_of_course = $db->single();

$db->query("SELECT * FROM lms_course WHERE id = ?");
$db->bind(1,$instance_of_course['c_id']);
$course = $db->single();


$db->query("SELECT * FROM ws_accounts WHERE id = ?");
$db->bind(1,$learner);
$learner_account = $db->single();

$db->query("SELECT * FROM ws_accounts_extra_info WHERE account_id = ?");
$db->bind(1,$learner);
$learner_accoun_extra = $db->single();

$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$user = $db->single();

if($user['name']!=''){

$user_ = ucfirst ($user['name']);

}else{

    $db->query("SELECT * FROM ws_accounts WHERE id = ?");
    $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $user = $db->single();

    $user_ = ucfirst ($user['first_name']).' '.ucfirst ($user['surname']);

}


$date_ = date("h:i:s A d-m-Y");


//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
if($instance_of_course['c_type']=="online"){
  $c_type = "Online Course";
}

if($instance_of_course['c_type']=="vc"){
  $c_type = "Virtual Classroom Course";
}

if($instance_of_course['c_type']=="f2f"){
  $c_type = "Face to Face Course";
}

$courseTitle = $course['title'];

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

if($learner_accoun_extra['company_id']!="N/A"){

      $db->query("SELECT * FROM ws_companies WHERE id = ?");
      $db->bind(1,$learner_accoun_extra['company_id']);
      $comp_ = $db->single();
      $company_id = $learner_accoun_extra['company_id'];
      $log_description = ucfirst ($learner_account['first_name']).' '.ucfirst ($learner_account['surname']).' was addded on the '.ucfirst ($course['title']) .' ('.$c_type.'). '.ucfirst ($comp_['company_name']). 'was debited '.$_POST['coursecost'].' credits. This was done by '.$user_.' on '.$date_;

      $up_date_credit = $comp_['credit']-$_POST['coursecost'];

      $db->Query("update ws_companies set credit = ? where id = ?");
      $db->bind(1,$up_date_credit);
      $db->bind(2,$learner_accoun_extra['company_id']);
      $db->execute();

}else{
      $company_id = 'N/A';
      $log_description = ucfirst ($learner_account['first_name']).' '.ucfirst ($learner_account['surname']).' was addded on the '.ucfirst ($course['title']) .' ('.$c_type.'). They were debited '.$_POST['coursecost'].' credits. This was done by '.$user_.' on '.$date_;

      // add learner update credit
      // $db->Query("update ws_accounts_extra_info set credit = ? where id = ?");
      // $db->bind(1,$up_date_credit);
      // $db->bind(2,$learner_accoun_extra['company_id']);
      // $db->execute();

}

$db = new database;
$db->Query("insert into ws_credit_log (id, amount, company_id, learner_id, course_inst, course_tem, type, completed_by, completed_date, description ) values (?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,createid('c_log'));
$db->bind(2,$_POST['coursecost']);
$db->bind(3,$company_id);
$db->bind(4,$learner);
$db->bind(5,$_GET['id']);
$db->bind(6,$instance_of_course['c_id']);
$db->bind(7,"debited");
$db->bind(8,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(9,time());
$db->bind(10,$log_description);
$db->execute();



$db = new database;
$db->Query("insert into ws_booked_on (id, course_id, created_date, created_by, booked_on, account_id, book_type, status, pass_score ) values (?,?,?,?,?,?,?,?,?)");
$db->bind(1,createid('book'));
$db->bind(2,$instance_of_course['id']);
$db->bind(3,time());
$db->bind(4,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(5,time());
$db->bind(6,$learner);
$db->bind(7,"manual");
$db->bind(8,$status);
$db->bind(9,$score);
$db->execute();

echo "ok";






?>
