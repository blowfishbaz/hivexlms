<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'completed_date';
	$order = 'desc';
}


$db->query("SELECT ws_credit_log.*, ws_credit_log.type as stype, ws_accounts.*, ws_companies.company_name as c_name FROM ws_credit_log
LEFT JOIN ws_accounts
ON ws_credit_log.learner_id = ws_accounts.id
LEFT JOIN ws_companies
ON ws_credit_log.company_id = ws_companies.id
order by $sort $order LIMIT ? offset ?");
$db->bind(1,(int) $limit);
$db->bind(2,(int) $offset);
$db->execute();
$rowcount = $db->rowcount();

$db->query("SELECT ws_credit_log.*, ws_credit_log.type as stype, ws_accounts.*, ws_companies.company_name as c_name FROM ws_credit_log
LEFT JOIN ws_accounts
ON ws_credit_log.learner_id = ws_accounts.id
LEFT JOIN ws_companies
ON ws_credit_log.company_id = ws_companies.id
order by $sort $order LIMIT ? offset ?");
$db->bind(1,(int) $limit);
$db->bind(2,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
