<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');



$db->query("select * from lms_course where id = ?");
$db->bind(1,$_GET["id"]);
$c = $db->single();


// echo '<pre>';
// print_r($c);
// echo '</pre>';
?>


<form id="new_course_form">




<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="" class="control-label" style="margin-top:0px;">Course Type</label>
        <select class="form-control" name="course_type" style="" id="course_type">
            <option class="" value="" selected=""></option>

              <? if($c['online']=='1'){ ?>
              <option class="" value="online">Online</option>
              <? }?>
              <? if($c['vc']=='1'){ ?>
              <option class="" value="vc">Virtual Classroom</option>
              <? }?>
              <? if($c['f2f']=='1'){ ?>
              <option class="" value="f2f">Face To Face</option>
              <? }?>

        </select>
        <span class="material-input"></span>
    </div>
</div>

<div class="col-lg-12 web_link_in"  style="display:none;">
  <div class="form-group is-empty">
    <label for="web_link" class="control-label">Online Webinar Link: </label>
    <input type="text" class="form-control" name="web_link">
    <span class="material-input"></span>
  </div>
</div>

<div class="col-md-12 col-sm-12">
  <div class="form-group">
          <label class=“”>Course Description</label>
          <textarea name="course_info" class="form-control" rows="5" id="page_info"></textarea>
      </div>

</div>


<div class="clearfix"></div>

<div class="always_opt">
    <div class="col-lg-6" >
        <div class="form-group is-empty">
              <label for="always_start" class="control-label">Start Date: </label>
              <input type="date" class="form-control" id="always_start" name="always_start" value="<? echo date("Y-m-d", time()); ?>" required="yes">
              <span class="material-input"></span>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group is-empty">
              <label for="always_end" class="control-label">End Date: </label>
              <input type="date" class="form-control" id="always_end" name="always_end" value="<? echo date("Y-m-d", strtotime(date("Y-m-d", mktime()) . " + 365 day")); ?>" required="yes">
              <span class="material-input"></span>
        </div>
    </div>
</div>



<div class="clearfix"></div>

<div class="dates_opt" style="margin-top:20px;">
      <!-- <span class="date_line">
        <div class="col-lg-6">
          <div class="form-group is-empty">
            <label for="date_title" class="control-label">Date Title: </label>
            <input type="text" class="form-control" name="date_title[]" required="yes">
            <span class="material-input"></span>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="form-group is-empty">
            <label for="date_" class="control-label">Date: </label>
            <input type="date" class="form-control" name="date_[]" value="<? echo date("Y-m-d", time()); ?>" required="yes">
            <span class="material-input"></span>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="form-group is-empty">
            <label for="date_" class="control-label">Time: </label>
            <input type="time" class="time_in" name="appt[]" min="09:00" max="22:00" required>
            <span class="material-input"></span>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="col-lg-2 pull-right align-right">
              <button type="button" class="btn btn-raised btn-danger btn-xs pull-right del_meter"> X </button></div>
          </div>
        </div>
      </span> -->
      <div class="row add_row">
          <div class="col-lg-12" >
            <br />
            <br />
                <div class="col-lg-2 pull-right align-right" >
                      <button type="button" class="btn btn-raised btn-success btn-xs pull-right add_line"> Add Date </button>
                </div>
          </div>
      </div>
</div>



<div class="row">
    <div class="col-lg-12" >
      <br />
      <br />
          <div class="col-lg-2 pull-right align-right" >
                <button type="button" class="btn btn-raised btn-success btn-lg  pull-right save_course"> Save </button>
          </div>
    </div>
</div>

</form>
