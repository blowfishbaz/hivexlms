<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("SELECT ws_accounts.*, ws_accounts.id as l_id, ws_accounts_extra_info.*, ws_accounts_extra_info.credit as l_credit, ws_booked_on.*, ws_booked_on.id as bid, ws_booked_on.status as c_status FROM ws_accounts
            JOIN ws_accounts_extra_info
            ON ws_accounts.id = ws_accounts_extra_info.account_id
            JOIN ws_booked_on
            ON ws_accounts.id = ws_booked_on.account_id
            WHERE ws_accounts.status = ? and ws_booked_on.id = ?
            ORDER BY first_name ASC");
$db->bind(1,1);
$db->bind(2,$_POST['book']);
$learners = $db->single();


//get course cost
$db->query("select * from lms_course_inst where id = ?");
$db->bind(1,$learners["course_id"]);
$c_inst = $db->single();

$db->query("select * from lms_course where id = ?");
$db->bind(1,$c_inst["c_id"]);
$c_ = $db->single();

if($c_inst['c_type']=="online"){
  $c_type = "Online Course";
  $credCost = $c_['online_course_credit_price'];
}

if($c_inst['c_type']=="vc"){
  $c_type = "Virtual Classroom Course";
  $credCost = $c_['vc_course_credit_price'];
}

if($c_inst['c_type']=="f2f"){
  $c_type = "Face to Face Course";
  $credCost = $c_['f2f_course_credit_price'];
}

$date_ = date("h:i:s A d-m-Y");

$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$user = $db->single();

if($user['name']!=''){

    $user_ = ucfirst ($user['name']);

}else{

    $db->query("SELECT * FROM ws_accounts WHERE id = ?");
    $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $user = $db->single();

    $user_ = ucfirst ($user['first_name']).' '.ucfirst ($user['surname']);

}



//update ws_accounts_extra credits or update company credits

// "id":"id_book_20210310_111611718500_85595",
// "created_date":"1615374971",
// "first_name":"Company Level",
// "surname":"Learner",
// "email":"cp@emailcom",
// "username":"7d69eb39d14ca74fb9959ae1b3b004fc",
// "password":"3c383aa4311ed8c3",
// "attempts":"0",
// "lastattempt":"0",
// "profilepic":"",
// "status":"1",
// "type":"web",
// "pid":null,
// "lxp":"1",
// "rota":"0","hr":"1",
// "l_id":"id_user_20210218_143756915200_34426",
// "account_id":"id_user_20210218_143756915200_34426",
// "company_id":"id_comp_20210217_101123718100_59489","main_provision":"N\\/A","created_by":"id_member_20160913_120305129000_41074","modified_date":null,"modified_by":null,"mobile_number":"0","telephone":"0","job_title":"","account_type":"Learner","credit":"0","l_credit":"0","course_id":"id_c_in_20210303_145124708700_14217","booked_on":"1615374971","start_date":null,"completed_date":null,"pass_score":"1","course_status":"1","course_date":null,"book_type":"manual","bid":"id_book_20210310_111611718500_85595","c_status":"1"
//
//

if($learners['company_id']!='N/A'){

      $db->query("select * from ws_companies where id = ?");
      $db->bind(1,$learners['company_id']);
      $comp = $db->single();

      $up_date_cred = $comp['credit']+$credCost;

      $db->Query("update ws_companies set credit = ? where id = ?");
      $db->bind(1,$up_date_cred);
      $db->bind(2,$learners['company_id']);
      $db->execute();


      //add to log
      $log_description = ucfirst ($learners['first_name']).' '.ucfirst ($learners['surname']).' was remove from the '.ucfirst ($c_['title']) .' ('.$c_type.'). '.ucfirst ($comp['company_name']). 'was refunded '.$credCost.' credits. This was done by '.$user_.' on '.$date_;

      $db = new database;
      $db->Query("insert into ws_credit_log (id, amount, company_id, learner_id, course_inst, course_tem, type, completed_by, completed_date, description ) values (?,?,?,?,?,?,?,?,?,?)");
      $db->bind(1,createid('c_log'));
      $db->bind(2,$credCost);
      $db->bind(3,$learners['company_id']);
      $db->bind(4,$learners['l_id']);
      $db->bind(5,$learners["course_id"]);
      $db->bind(6,$c_inst["c_id"]);
      $db->bind(7,"refunded");
      $db->bind(8,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $db->bind(9,time());
      $db->bind(10,$log_description);
      $db->execute();


}else{

      $up_date_cred = $comp['credit']+$credCost;

      $db->Query("update ws_accounts_extra_info set credit = ? where id = ?");
      $db->bind(1,$up_date_cred);
      $db->bind(2,$learners['company_id']);
      $db->execute();

      $log_description = ucfirst ($learners['first_name']).' '.ucfirst ($learners['surname']).' was removed from the '.ucfirst ($c_['title']) .' ('.$c_type.'). They were refunded '.$credCost.' credits. This was done by '.$user_.' on '.$date_;

      $db = new database;
      $db->Query("insert into ws_credit_log (id, amount, company_id, learner_id, course_inst, course_tem, type, completed_by, completed_date, description ) values (?,?,?,?,?,?,?,?,?,?)");
      $db->bind(1,createid('c_log'));
      $db->bind(2,$credCost);
      $db->bind(3,"N/A");
      $db->bind(4,$learners['l_id']);
      $db->bind(5,$learners["course_id"]);
      $db->bind(6,$c_inst["c_id"]);
      $db->bind(7,"refunded");
      $db->bind(8,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $db->bind(9,time());
      $db->bind(10,$log_description);
      $db->execute();

}


//update book status to refunded - 5

$db->query("delete from ws_booked_on where id = ?");
$db->bind(1,$learners['bid']);
$db->execute();



echo "ok";



?>
