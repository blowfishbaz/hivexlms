<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'ws_accounts.first_name';
	$order = 'desc';
}


$db->query("SELECT ws_accounts.*, ws_accounts.id as l_id, ws_booked.* FROM ws_accounts
            JOIN ws_booked
            ON ws_booked.account_id = ws_accounts.id
            WHERE ws_accounts.status = ? and ws_booked.course_id = ?");
            $db->bind(1,1);
            $db->bind(2,$_GET['course']);
            $rowcount = $db->rowcount();


$db->query("SELECT ws_accounts.*, ws_accounts.id as l_id, ws_booked_on.* FROM ws_accounts
            JOIN ws_booked_on
            ON ws_booked_on.account_id = ws_accounts.id
            WHERE ws_accounts.status = ? and ws_booked_on.course_id = ?
            order by $sort $order LIMIT ? offset ?");
            $db->bind(1,1);
            $db->bind(2,$_GET['course']);
            $db->bind(3,(int) $limit);
            $db->bind(4,(int) $offset);
            $data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
