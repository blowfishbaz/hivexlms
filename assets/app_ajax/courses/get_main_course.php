<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from lms_course where id = ?");
$db->bind(1,$_POST["id"]);
$c = $db->single();


?>



<form id="NewCourseForm" name="NewCourseForm" method="post" enctype="application/x-www-form-urlencoded">


                   <div class="col-md-12 col-sm-12">

                       <div class="form-group" style="margin-top:0px;">
                           <p style="margin-bottom:15px;border-bottom: 1px solid #d0c8c8;padding-bottom: 9px;">
                             <strong style="margin-right:10px; font-size:18px;">Course Availability: </strong>
                             <? if($c['online']==1){ $_on = 'checked'; $_on_val = 'true';}else{ $_on = ''; $_on_val = 'false'; } ?>
                               <label class="checkbox-inline">
                                   <input class="c_in" type="hidden" name="c_online" value="<? echo $_on_val; ?>" />
                                   <input class="c_avail c_online_in" type="checkbox" value="<? echo $_on_val; ?>" name="online" <? echo $_on; ?>> Online
                               </label>
                               <? if($c['vc']==1){ $_vc = 'checked'; $_vc_val = 'true';}else{ $_vc = ''; $_vc_val = 'false';} ?>
                               <label class="checkbox-inline">
                                   <input class="c_in" type="hidden" name="c_vc" value="<? echo $_vc_val; ?>" />
                                   <input class="c_avail c_vc_in" type="checkbox" value="<? echo $_vc_val; ?>" name="vc" <? echo $_vc; ?>> Virtual Classroom
                               </label>
                               <? if($c['f2f']==1){ $_f2f = 'checked'; $_f2f_val = 'true';}else{ $_f2f = ''; $_f2f_val = 'false'; } ?>
                               <label class="checkbox-inline">
                                   <input class="c_in" type="hidden" name="c_f2f" value="<? echo $_f2f_val; ?>" />
                                   <input class="c_avail c_f2f_in" type="checkbox" value="<? echo $_f2f_val; ?>" name="f2f" <? echo $_f2f; ?>> Face to Face
                               </label>
                           </p>

                           <br />

                           <div class="warning_online" style="display:none;">
                                <p>If you delete this all "Online" resources &amp; "Online" content will also be deleted?</p>
                                <br />
                                <br />
                                <p>
                                  <button type="button" class="btn btn-default btn-sm btn-raised" data-id="<? echo $_POST["id"]; ?>" id="add_new_course_cancel">Cancel</button>
                                  <button type="button" class="btn btn-primary btn-sm btn-raised pull-right" data-id="<? echo $_POST["id"]; ?>" id="add_new_course_continue">Continue</button>
                                </p>
                           </div>

                           <!-- <div class="warning_online" style="display:none;">
                                <p>If you remove this all "Online" resources &amp; "Online" content will also be deleted?</p>
                                <br />
                                <br />
                                <p>
                                  <button type="button" class="btn btn-default btn-sm btn-raised " id="add_new_course">Cancel</button>
                                  <button type="button" class="btn btn-primary btn-sm btn-raised pull-right" id="add_new_course">Continue</button>
                                </p>
                           </div> -->


                       </div>





                   </div>


                   <div class="col-md-12 col-sm-12" style="display: none;">
                         <div class="form-group label-floating">
                             <label for="title" class="control-label">Course Title*</label>
                             <input type="text" class="form-control" id="title" name="title" required="yes" value="<? echo $c['title']; ?>">
                             <span class="material-input"></span>
                         </div>
                   </div>


                   <div class="col-md-12 col-sm-12" style="display: none;">
                     <div class="form-group">
                             <label class=“”>Course Description</label>
                             <textarea name="course_info" class="form-control" rows="5" id="page_info_edit"><? echo $c['description']; ?></textarea>
                         </div>
                   </div>


</form>

<div class="clearfix">

</div>

<script>

$('#page_info_edit').summernote({
    height: 180,                 // set editor height
    minHeight: null,             // set minimum height of editor
    maxHeight: null,             // set maximum height of editor
    placeholder:'Description........',
    toolbar: [
        ['font', ['bold', 'italic', 'underline']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'hr']],
        ['fontsize', ['fontsize']],
        ['help', ['help']]
    ]
});


$( "body" ).on( "change", ".c_online_in", function(e) {
    if(this.checked){
          $('.warning_online').hide();
          $(this).parent().children(".c_in").val('true');
    }else{
          $('.warning_online').show();
          $(this).parent().children(".c_in").val('false');
    }
});



$( "body" ).on( "click", "#add_new_course_cancel", function(e) {
      $('.warning_online').hide();
      $('.c_online_in').parent().children(".c_in").val('true');
      $( ".c_online_in" ).prop( "checked", true );
});



$( "body" ).on( "click", "#add_new_course_continue", function(e) {
      $c_id = $(this).data('id');
      alert($c_id);
});



</script>
