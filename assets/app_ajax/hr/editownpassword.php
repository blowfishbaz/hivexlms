<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    contacts/contact_view/calendar/load_calendar_appointments.php
 * @author    Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $password = $_POST['new_own_password'];

 $db->Query("update accounts set password = ? where id = ?");
 $db->bind(1,encrypt($password));
 $db->bind(2,decrypt($_SESSION['SESS_ACCOUNT_ID']));
 $db->execute();

 $db->Query("update ws_accounts set password = ? where id = ?");
 $db->bind(1,encrypt($password));
 $db->bind(2,decrypt($_SESSION['SESS_ACCOUNT_ID']));
 $db->execute();

 echo 'ok';
