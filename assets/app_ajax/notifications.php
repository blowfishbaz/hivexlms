<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename   notifications.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $todayMidnight = strtotime('today midnight');
 $tomorrowMidnight = strtotime('tomorrow midnight');

  $db->query("select * from ws_notifications where created_for = ? and status = ? and viewed = ? ORDER BY created_date desc");
  $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $db->bind(2,'1');
  $db->bind(3,'0');
  $notifications = $db->resultSet();
  $rowcount = $db->rowcount();

  $db->Query("update ws_notifications set viewed = ? where created_for = ?");
$db->bind(1, '1');
$db->bind(2, decrypt($_SESSION['SESS_ACCOUNT_ID']));
//$db->execute();




 ?>
<style>

  .notifications:hover{
    cursor: pointer;
  }
  .notification_row {
      float: left;
      width: 100%;
      margin: 5px 0 10px 0;
      color: #000;
  }

  .notification_img {
    float: left;
    margin-right: 5px;
    border-radius: 50%;
}
  .notification_text {
    max-height: 20px;
    min-height: 17px;
    overflow: hidden;
    float: left;
    width: 100%;
    font-size: 12px;
}
.notification_titel {
    float: left;
    width: -webkit-calc(100% - 65px);
    width: -moz-calc(100% - 65px);
    width: calc(100% - 65px);
    max-height: 60px;
    overflow: hidden;
}

.notification_header_hidden{
  display:none;
}
</style>
<!-- <div class=""><span class="menuglyph glyphicons glyphicons-bell" aria-hidden="true"></span><h3 style="display: inline;">Notifications</h3></span></div> -->


<div style="margin-top: 5px; border-bottom:3px solid black;">
  <h3 style="padding-bottom:5px; margin:0px;" class="pull-left">My Notifications</h3>
  <img class="img-rounded pull-right" alt="140x140" src="/assets/images/IconNotification.png" style="width: 40px; height: 40px;">
  <div class="clearfix"></div>
</div>
<!-- <a href="javascript:void(0)" class="change_notifications" data-id="headline">Headlines</a> / <a href="javascript:void(0)" class="change_notifications" data-id="details">Details</a> -->

  <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) != 'user'){?><a href="<?echo $fullurl;?>admin/rota/admin/add_notification.php" class="btn btn-sm btn-raised btn-success" id="">Add</a><?}?>

<div class="clearfix"></div>
<?
if( $rowcount == 0){
  ?>
  <strong style="text-align: center;width: 100%;float: left;margin-top: 30px; margin-bottom: 30px;">You Have No Notifications</strong>
  <?
}else{

  foreach ($notifications as $key => $n){

    $db->query("select * from accounts where id= ?");
    $db->bind(1,$n['created_by']);
    $ac = $db->single();

$url = loadProfilePicwithID($n['created_by']);?>

<div style="border-bottom: 1px solid black;">


<a href="
<?if($n['type'] == 'new_shift'){echo $fullurl.'admin/rota/account/shift_response.php?id='.$n['pid'].'&account='.$n['created_for'].'';}
else if($n['type'] == 'user_shift_response'){echo $fullurl.'admin/rota/admin/view_responses.php?id='.$n['pid'].'';}
  else{echo '/admin/rota/notification_view.php?id='.$n['id'];}?>" class="notification_row">
<!-- <img src='<?echo $url;?>' class='img-rounded notification_img' width='60' height='60' data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="<?echo $n['created_by'];?>"> -->
<div style="padding-left:10px;">
  <strong class="notification_titel"> <?echo $n['title'];?></strong><br />
     <div class="truncate notification_text">
           <?echo strip_tags(nl2br($n['message']));?>
     </div>
  <small><?echo 'by '.$ac['screenname'].' '.timeago($n['created_date']);?> - <?echo $ac['name'];?></small>
</div>

</a>
<button type="button" class="btn btn-sm btn-raised btn-danger pull-right" style="margin-top: 0px; margin-bottom: 5px; padding-left: 10px; padding-right: 10px;" id="dismiss_notification" data-id="<?echo $n['id'];?>">Dismiss</button>
<div class="clearfix"></div>

</div>

<?   }
}
?>
