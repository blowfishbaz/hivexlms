<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename   notifications.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
 ?>
<!-- <style>
h2 {
    text-align: center;
    border-bottom: 1px solid #ebeff8;
    line-height: 0.1em;
    margin: 20px 0 20px;
    font-size: 24px;
    color: #2c3764;
}
</style> -->
<style>




.main_sign_img{
    background: url(<? echo $fullurl ?>assets/images/imgblue.jpg) no-repeat center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    min-height: 30vh;
}

.main_sign{
    min-height: 30vh;
    /* background-color: red; */
}


.big_in {
    width: 100%;
    border-radius: 11px;
    border: solid 1px #dadbdc;
    font-size: 18px;
    box-sizing: border-box;
}

#pcs_in_but {
    background: #0471A6;
    color: #ffffff;
    border: solid 1px #0471A6;
    padding: 7px;
}

#pcs_in{
    width: 100%;
    padding: 7px;
    margin-bottom: 9px;
}

.btn-group.bootstrap-select.big_in.form-control {
    width: 100%;
    border-radius: 11px;
    font-size: 18px;
    box-sizing: border-box;
    padding: 0px;
}
.btn-group.bootstrap-select.big_in.form-control > button {
    padding: 7px;
    background: none;
    width: 100%;
    border-radius: 11px;
    border: solid 1px #dadbdc;
    font-size: 18px;
    box-sizing: border-box;
}
h1{
    margin-top: 55px;
    margin-left: 20%;
    width:60%;
    margin-bottom: 55px;
    color: #2c3764;
    font-size: 75px;
    text-align: center;
}

.slabtexted .slabtext
    {
    display: -moz-inline-box;
    display: inline-block;
    white-space: nowrap;
    }
.slabtextinactive .slabtext
    {
    display: inline;
    white-space: normal;
    font-size: 1em !important;
    letter-spacing: inherit !important;
    word-spacing: inherit !important;
    *letter-spacing: normal !important;
    *word-spacing: normal !important;
    }

.slabtextdone .slabtext
    {
    display: block;
    }


.main_but_choice{
    margin-bottom: 40px;
}


h2 {
   text-align: center;
   border-bottom: 1px solid #ebeff8;
   line-height: 0.1em;
   margin: 10px 0 0 0;
   font-size: 24px;
   color: #2c3764;
}

h2 span {
    background:#fff;
    padding:0 10px;
}







.button_class{
    padding: 7px;
font-size: 18px;
border-radius: 10px;
background: #0471A6;
color: #ffffff;
border: solid 1px #0471A6;
}
.button_class:hover{
    background: #1a71bd;
    border: solid 1px #1a71bd;
    color: #ffffff;
}


.preferenceslist {
    float: left;
    width: auto;
    border: solid 2px #cccccc;
    border-radius: 10px;
    margin: 3px;
    font-size: 18px;
    padding: 0 0 0 10px;
}

.removepreferenceslist {
    background: transparent;
    border: none;
    padding: 0;
}
.glyphicons-remove.glyphicons:before {
    padding: 3px 5px;
}
.removepreferenceslist:hover {
    color:#f00;
}
.removepreferenceslist:focus {
    outline: none;
}
#Preferences_box {
    min-height: 20px;
}
.tab_page{
    float: left;
    width: 100%;
    margin: 50px 0;
}
</style>


<div class="tab_page">
    <div class="row">
    <div class="col-lg-1"></div>
        <div class="col-lg-10">
  <form class="form-signin" action="<?echo $fullurl;?>assets/app_php/auth.php?action=logon" enctype="application/x-www-form-urlencoded" method="post">
      <!-- <form class="form-signin" action="<?echo $fullurl;?>auth.php?action=logon" enctype="application/x-www-form-urlencoded" method="post">-->

         <!--<p>Sign In</p>

      <? if (isset($_GET['error'])) { ?>
       <div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Username or Password Incorrect.
    </div>

    <? } ?>
    <? if (isset($_GET['account'])) { ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Account has been locked.
    </div>
    <? } ?> -->



    <div class="form-group label-floating is-empty">
                <label for="login" class="control-label">Email</label>
                <input type="email" name="login" class="form-control"  required>

              <span class="material-input"></span>
        </div>

        <div class="form-group label-floating is-empty">
                <label for="password" class="control-label">Password</label>
                <input type="password" class="form-control" name="password" required>

              <span class="material-input"></span>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="1" name="remember"> Remember me
          </label>
        </div>
        <input type="hidden" name="key" value="<? echo getCurrentKey(); ?>">
        <input type="hidden" name="username">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      </div>
      </div>




                      <script>

                      $(document).ready(function() {
                          $('.selectpicker').selectpicker('refresh');
                          setTimeout(function(){
                              $h=$('.main_sign').height();
                              $h=$h+40;
                              $('.main_sign_img').height($h);
                          }, 200);

                     });
                     $( window ).resize(function() {
                         $h=$('.main_sign').height();
                         $h=$h+40;
                         $('.main_sign_img').height($h);
                     });
                      </script>
