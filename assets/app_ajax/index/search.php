<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename   notifications.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
 ?>
<!-- <style>
h2 {
    text-align: center;
    border-bottom: 1px solid #ebeff8;
    line-height: 0.1em;
    margin: 20px 0 20px;
    font-size: 24px;
    color: #2c3764;
}
</style> -->
<style>




.main_sign_img{
    background: url(<? echo $fullurl ?>assets/images/imgblue.jpg) no-repeat center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    min-height: 30vh;
}

.main_sign{
    min-height: 30vh;
    /* background-color: red; */
}


.big_in {
    width: 100%;
    border-radius: 11px;
    border: solid 1px #dadbdc;
    font-size: 18px;
    box-sizing: border-box;
}

#pcs_in_but {
    background: #0471A6;
    color: #ffffff;
    border: solid 1px #0471A6;
    padding: 7px;
}

#pcs_in{
    width: 100%;
    padding: 7px;
    margin-bottom: 9px;
}

.btn-group.bootstrap-select.big_in.form-control {
    width: 100%;
    border-radius: 11px;
    font-size: 18px;
    box-sizing: border-box;
    padding: 0px;
}
.btn-group.bootstrap-select.big_in.form-control > button {
    padding: 7px;
    background: none;
    width: 100%;
    border-radius: 11px;
    border: solid 1px #dadbdc;
    font-size: 18px;
    box-sizing: border-box;
}
h1{
    margin-top: 55px;
    margin-left: 20%;
    width:60%;
    margin-bottom: 55px;
    color: #2c3764;
    font-size: 75px;
    text-align: center;
}

.slabtexted .slabtext
    {
    display: -moz-inline-box;
    display: inline-block;
    white-space: nowrap;
    }
.slabtextinactive .slabtext
    {
    display: inline;
    white-space: normal;
    font-size: 1em !important;
    letter-spacing: inherit !important;
    word-spacing: inherit !important;
    *letter-spacing: normal !important;
    *word-spacing: normal !important;
    }

.slabtextdone .slabtext
    {
    display: block;
    }


.main_but_choice{
    margin-bottom: 40px;
}


h2 {
   text-align: center;
   border-bottom: 1px solid #ebeff8;
   line-height: 0.1em;
   margin: 10px 0 0 0;
   font-size: 24px;
   color: #2c3764;
}

h2 span {
    background:#fff;
    padding:0 10px;
}







.button_class{
    padding: 7px;
font-size: 18px;
border-radius: 10px;
background: #0471A6;
color: #ffffff;
border: solid 1px #0471A6;
}
.button_class:hover{
    background: #1a71bd;
    border: solid 1px #1a71bd;
    color: #ffffff;
}


.preferenceslist {
    float: left;
    width: auto;
    border: solid 2px #cccccc;
    border-radius: 10px;
    margin: 3px;
    font-size: 18px;
    padding: 0 0 0 10px;
}

.removepreferenceslist {
    background: transparent;
    border: none;
    padding: 0;
}
.glyphicons-remove.glyphicons:before {
    padding: 3px 5px;
}
.removepreferenceslist:hover {
    color:#f00;
}
.removepreferenceslist:focus {
    outline: none;
}
#Preferences_box {
    min-height: 20px;
}
.tab_page{
    float: left;
    width: 100%;
    margin: 50px 0;
}
</style>


<div class="tab_page">
                          <div class="col-lg-12">
                          <div class="row">
                          <div class="col-lg-1"></div>
                              <div class="col-lg-5">
                                  <select name="lookingfor" class="big_in form-control selectpicker" data-live-search="true" id="region">
                                          <option selected style="display:none">Looking for a</option>
                                          <option value="Male">Male</option>
                                          <option value="Female">Female</option>
                                          <option value="Couple MF">Couple MF</option>
                                          <option value="Couple MM">Couple MM</option>
                                          <option value="Couple FF">Couple FF</option>
                                          <option value="TV/TS">TV/TS</option>
                                  </select>
                              </div>
                              <div class="col-lg-5">
                              <select name="lookingfor" class="big_in form-control selectpicker" data-live-search="true" id="region">
                                      <option selected style="display:none">Orientation</option>
                                      <option value="Bi-curious">Bi-curious</option>
                                      <option value="Bi-sexual">Bi-sexual</option>
                                      <option value="Gay">Gay</option>
                                      <option value="Straight">Straight</option>
                              </select>
                          </div>
                          </div>


                      <div class="row">
                          <div class="col-lg-1"></div>
                          <div class="col-lg-5">
                              <div class="form-group"></div>
                                  <input type="text" class=" big_in" id="pcs_in" name="search" placeholder="Search">
                          </div>

                              <div class="col-lg-5">

                              <div class="row">
                              <div class="col-lg-6">
                              <select name="age_from" class="big_in form-control selectpicker" data-live-search="true" id="region">
                                      <option selected style="display:none">Age From</option>
                                      <?$a=18;
                                      while ($a <= 99) {
                                          echo '<option value="'.$a.'">'.$a.'</option>';
                                          $a++;
                                      }?>
                              </select>
                              </div>

                              <div class="col-lg-6">
                              <select name="age_to" class="big_in form-control selectpicker" data-live-search="true" id="region">
                                      <option selected style="display:none">Age To</option>
                                      <?$a=18;
                                      while ($a <= 99) {
                                          echo '<option value="'.$a.'">'.$a.'</option>';
                                          $a++;
                                      }?>
                              </select>
                              </div>
                              </div>
                              </div>

                              </div>

                              <div class="row">
                              <div class="col-lg-4"></div>
                               <div class="col-lg-4">
                                   <div class="form-group"></div>
                                           <input type="text" class=" big_in" id="pcs_in" placeholder="Postcode">
                                </div>
                            </div>

                               <div class="row">
                                   <div class="col-lg-1"></div>
                                   <div class="col-lg-10">
                                       <h2><span>Or</span></h2>
                                   </div>
                               </div>


                               <div class="row">
                                   <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    <select name="region" class="big_in form-control selectpicker" data-live-search="true" id="region">
                                            <option selected style="display:none">Region</option>
                                            <option value="Greater London">Greater London</option>
                                            <option value="South East">South East</option>
                                            <option value="South West">South West</option>
                                            <option value="West Midlands">West Midlands</option>
                                            <option value="North West">North West</option>
                                            <option value="North East">North East</option>
                                            <option value="Yorkshire and the Humber">Yorkshire and the Humber</option>
                                            <option value="East Midlands">East Midlands</option>
                                            <option value="East of England">East of England</option>
                                    </select>
                                </div>

                                <div class="col-lg-5">

                                    <select name="countys" class="big_in form-control selectpicker" data-live-search="true" id="countys">
                                            <option selected style="display:none">County</option>
                                            <option value="Bedfordshire">Bedfordshire</option>
                                            <option value="Berkshire">Berkshire</option>
                                            <option value="Bristol">Bristol</option>
                                            <option value="Buckinghamshire">Buckinghamshire</option>
                                            <option value="Cambridgeshire">Cambridgeshire</option>
                                            <option value="Cheshire">Cheshire</option>
                                            <option value="Cornwall">Cornwall</option>
                                            <option value="County Durham">County Durham</option>
                                            <option value="Cumberland">Cumberland</option>
                                            <option value="Derbyshire">Derbyshire</option>
                                            <option value="Devon">Devon</option>
                                            <option value="Dorset">Dorset</option>
                                            <option value="Gloucestershire">Gloucestershire</option>
                                            <option value="Essex">Essex</option>
                                            <option value="Hampshire">Hampshire</option>
                                            <option value="Herefordshire">Herefordshire</option>
                                            <option value="Hertfordshire">Hertfordshire</option>
                                            <option value="Huntingdonshire">Huntingdonshire</option>
                                            <option value="Kent">Kent</option>
                                            <option value="Lancashire">Lancashire</option>
                                            <option value="Leicestershire">Leicestershire</option>
                                            <option value="Lincolnshire">Lincolnshire</option>
                                            <option value="Middlesex">Middlesex</option>
                                            <option value="Norfolk">Norfolk</option>
                                            <option value="Northamptonshire">Northamptonshire</option>
                                            <option value="Northumberland">Northumberland</option>
                                            <option value="Nottinghamshire">Nottinghamshire</option>
                                            <option value="Oxfordshire">Oxfordshire</option>
                                            <option value="Rutland">Rutland</option>
                                            <option value="Shropshire">Shropshire</option>
                                            <option value="Somerset">Somerset</option>
                                            <option value="Staffordshire">Staffordshire</option>
                                            <option value="Suffolk">Suffolk</option>
                                            <option value="Surrey">Surrey</option>
                                            <option value="Sussex">Sussex</option>
                                            <option value="Warwickshire">Warwickshire</option>
                                            <option value="Westmorland">Westmorland</option>
                                            <option value="Wiltshire">Wiltshire</option>
                                            <option value="Worcestershire">Worcestershire</option>
                                            <option value="Yorkshire">Yorkshire</option>
                                    </select>
                                </div>
                          </div>

                              <div class="row">
                                   <div class="col-lg-1"></div>
                              <div class="col-lg-10">
                                  <select name="preferences" class="big_in form-control selectpicker" data-live-search="true" id="preferences">
                                          <option selected style="display:none">Preferences:</option>
                                          <option value="3Somes FFM">3Somes FFM</option>
                                          <option value="3Somes MMF">3Somes MMF</option>
                                          <option value="A Levels">A Levels</option>
                                          <option value="A Levels (at discretion)">A Levels (at discretion)</option>
                                          <option value="15 Mins (quickie)">15 Mins (quickie)</option>
                                          <option value="Anal Play">Anal Play</option>
                                          <option value="Bareback">Bareback</option>
                                          <option value="BDSM">BDSM</option>
                                          <option value="BDSM (giving)">BDSM (giving)</option>
                                          <option value="BDSM (receiving)">BDSM (receiving)</option>
                                          <option value="Being Filmed">Being Filmed</option>
                                          <option value="Bukkake">Bukkake</option>
                                          <option value="Car Meets">Car Meets</option>
                                          <option value="CIM">CIM</option>
                                          <option value="CIM (at discretion)">CIM (at discretion)</option>
                                          <option value="Cross Dressing">Cross Dressing</option>
                                          <option value="Deep Throat">Deep Throat</option>
                                          <option value="Depilation">Depilation</option>
                                          <option value="Dinner Dates">Dinner Dates</option>
                                          <option value="Disabled Clients">Disabled Clients</option>
                                          <option value="Dogging">Dogging</option>
                                          <option value="Domination">Domination</option>
                                          <option value="Domination (giving)">Domination (giving)</option>
                                          <option value="Domination (receiving)">Domination (receiving)</option>
                                          <option value="Double Penetration">Double Penetration</option>
                                          <option value="Enema">Enema</option>
                                          <option value="Exhibitionism">Exhibitionism</option>
                                          <option value="Face Sitting">Face Sitting</option>
                                          <option value="Facials">Facials</option>
                                          <option value="Female Ejaculation">Female Ejaculation</option>
                                          <option value="Fetish">Fetish</option>

                                          <option value="Fingering/Finger Play">Fingering/Finger Play</option>
                                          <option value="Fisting">Fisting</option>
                                          <option value="Fisting (giving)">Fisting (giving)</option>
                                          <option value="Fisting (receiving)">Fisting (receiving)</option>
                                          <option value="Food Sex/Sploshing">Food Sex/Sploshing</option>
                                          <option value="Foot Worship">Foot Worship</option>
                                          <option value="French Kissing">French Kissing</option>
                                          <option value="French Kissing (discretion)">French Kissing (discretion)</option>
                                          <option value="Gang Bangs">Gang Bangs</option>
                                          <option value="Hand Relief">Hand Relief</option>
                                          <option value="Humiliation">Humiliation</option>
                                          <option value="Humiliation (giving)">Humiliation (giving)</option>
                                          <option value="Humiliation (receiving)">Humiliation (receiving)</option>
                                          <option value="Lapdancing">Lapdancing</option>
                                          <option value="Massage">Massage</option>
                                          <option value="Milking/Lactating">Milking/Lactating</option>

                                          <option value="Modeling">Modeling</option>
                                          <option value="Moresomes">Moresomes</option>
                                          <option value="Naturism/Nudism">Naturism/Nudism</option>
                                          <option value="Oral">Oral</option>
                                          <option value="Oral without (at discretion)">Oral without (at discretion)</option>
                                          <option value="Oral without Protection">Oral without Protection</option>
                                          <option value="Parties">Parties</option>
                                          <option value="Penetration (Protected)">Penetration (Protected)</option>
                                          <option value="Pole Dancing">Pole Dancing</option>
                                          <option value="Pregnant">Pregnant</option>
                                          <option value="Prostate Massage">Prostate Massage</option>
                                          <option value="Pussy Pumping">Pussy Pumping</option>
                                          <option value="Receiving Oral">Receiving Oral</option>
                                          <option value="Rimming">Rimming</option>
                                          <option value="Rimming (giving)">Rimming (giving)</option>
                                          <option value="Rimming (receiving)">Rimming (receiving)</option>
                                          <option value="Role Play & Fantasy">Role Play & Fantasy</option>
                                          <option value="Sauna / Bath Houses">Sauna / Bath Houses</option>
                                          <option value="Smoking (Fetish)">Smoking (Fetish)</option>
                                          <option value="Snowballing">Snowballing</option>
                                          <option value="Spanking">Spanking</option>
                                          <option value="Spanking (giving)">Spanking (giving)</option>
                                          <option value="Spanking (receiving)">Spanking (receiving)</option>
                                          <option value="Strap On">Strap On</option>
                                          <option value="Striptease">Striptease</option>
                                          <option value="Sub games">Sub games</option>
                                          <option value="Swallow">Swallow</option>
                                          <option value="Swallow (at discretion)">Swallow (at discretion)</option>
                                          <option value="Swinging">Swinging</option>
                                          <option value="Sybian & Machine Sex">Sybian & Machine Sex</option>
                                          <option value="Tantric">Tantric</option>
                                          <option value="Tie & Tease">Tie & Tease</option>
                                          <option value="Toys">Toys</option>
                                          <option value="Travel Companion">Travel Companion</option>
                                          <option value="Uniforms">Uniforms</option>
                                          <option value="Unprotected Sex">Unprotected Sex</option>
                                          <option value="Voyeurism">Voyeurism</option>
                                          <option value="Watersports (Giving)">Watersports (Giving)</option>
                                          <option value="Watersports (Receiving)">Watersports (Receiving)</option>
                                          </select>
                                      </div>



                                      <div id="Preferences_box" class="col-lg-12">

                                      </div>
                                  </div>
                          <!-- <select name="lookingfor" class="big_in form-control selectpicker" data-live-search="true" id="region">
                                  <option selected style="display:none">Orientation</option>
                                  <option value="Male">Male</option>
                          </select> -->





                      </div>

                      <div class="col-lg-4"></div>
                      <div class="col-lg-4 pager">
                          <button class="btn btn-sm btn-default button_class search_button" style=" min-width: 200px; " type="button">Search</button>
                      </div>


                      <script>



                      $( "body" ).on( "change", "#preferences", function() {

                       $val=$(this).val();
                       $(this).find('option:selected').attr("disabled", true);
                       $(this).find('option:eq(0)').prop('selected', true);
                       $(this).trigger('chosen:updated');
                       $(this).selectpicker('refresh');
                       $('#Preferences_box').append('<div class="preferenceslist"><input type="hidden" name="preferenceslist[]" value=\''+$val+'\' />'+$val+'<button class="removepreferenceslist" data-id=\''+$val+'\'><span class="glyphicons glyphicons-remove"></span></button></div>');
                      });

                      $( "body" ).on( "click", ".removepreferenceslist", function() {

                       $val=$(this).attr("data-id");
                       $('#preferences option[value="' + $val + '"]').removeAttr('disabled');
                       //$(".cost").each(function() {}
                       $('#preferences').selectpicker('refresh');
                      //"input[name=" + name + "]"
                       $(this).parent().remove();
                      });


                      $(document).ready(function() {
                          $('.selectpicker').selectpicker('refresh');
                          setTimeout(function(){
                              $h=$('.main_sign').height();
                              $h=$h+40;
                              $('.main_sign_img').height($h);
                          }, 200);

                     });
                     $( window ).resize(function() {
                         $h=$('.main_sign').height();
                         $h=$h+40;
                         $('.main_sign_img').height($h);
                     });
                      </script>
