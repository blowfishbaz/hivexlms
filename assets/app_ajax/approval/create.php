<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $thisId = createId('app');
  $dateNow = time();
	$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
  $type = $_POST["type"];
  $subtype = $_POST["sub_type"];
  $pid=$_POST["pid"];

  $db = new database;
  $db->Query("insert into bv_approval (id ,account_id ,pid ,type, sub_type, status, created_date) values (?,?,?,?,?,?,?)");
  $db->bind(1,$thisId);
  $db->bind(2,$user);
  $db->bind(3,$pid);
  $db->bind(4,$type);
  $db->bind(5,$subtype);
  $db->bind(6,'1');
  $db->bind(7,$dateNow);
  $db->execute();

sleep(1);
echo $thisId;
?>
