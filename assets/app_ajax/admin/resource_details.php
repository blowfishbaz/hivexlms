<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');



foreach ($_POST["resource_id"] as $key => $v) {

        $newid = createId('res');
        $rid=strtolower($_POST["resource_id"][$key]);
        $rtitle=strtolower($_POST["resource_name"][$key]);
        $rdesc=strtolower($_POST["resource_desc"][$key]);

        if($_POST["t_online"][$key]=='true'){ $isOnline = '1'; }else{ $isOnline = '0'; }

        if($_POST["t_vc"][$key]=='true'){ $isVc = '1'; } else{ $isVc = '0'; }

        if($_POST["t_f2f"][$key]=='true'){ $isf2f = '1'; } else{ $isf2f = '0'; }

        $db = new database;
        $db->Query("INSERT INTO lms_resource (id,rid,rtitle,rdesc,online,vc,f2f) values (?,?,?,?,?,?,?)");
        $db->bind(1,$newid);
        $db->bind(2,$rid);
        $db->bind(3,$rtitle);
        $db->bind(4,$rdesc);
        $db->bind(5,$isOnline);
        $db->bind(6,$isVc);
        $db->bind(7,$isf2f);
        $db->execute();

}

$db->query("select * from lms_upload where id = ?");
$db->bind(1,$_POST["resource_id"][0]);
$up = $db->single();

$returnThis = array('status' => 'ok', 'cid' => $up['cid']);
echo json_encode($returnThis);


?>
