<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');



$newid = createId('img');
$type=strtolower($_POST["uptype"]);
$cid=strtolower($_POST["cid"]);
$link=$_POST["filename"];


if($type=="featured"){


  $db->query("select * from lms_upload where cid = ? and type = ?");
	$db->bind(1, $cid);
  $db->bind(2, $type);
	$db->execute();
	$feat= $db->single();

  $path = '../../../uploads/courses/';

  unlink($path.'/'.$feat["link"]);

  $db->query("delete from lms_upload where id = ?");
	$db->bind(1,$feat["id"]);
	$db->execute();



}



$db->Query("INSERT INTO lms_upload (id,type,cid,link) values (?,?,?,?)");
$db->bind(1,$newid);
$db->bind(2,$type);
$db->bind(3,$cid);
$db->bind(4,$link);
$db->execute();


?>
