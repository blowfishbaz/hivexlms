<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $now = time();
  $id = $_POST["id"];
  $name=strtolower($_POST["name"]);
  $email=strtolower($_POST["email"]);
  $type=strtolower($_POST["type"]);
  $status=$_POST["status"];

  $db->query("update accounts set username = ?, name = ?, email = ?, type = ?, status = ? where id = ?");
  $db->bind(1,encrypt($email));
  $db->bind(2,$name);
  $db->bind(3,$email);
  $db->bind(4,$type);
  $db->bind(5,$status);
  $db->bind(6,$id);
  $db->execute();


  echo 'ok';
?>
