<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $newid = createId('job');
  $now = time();
  $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
  $title=$_POST["title"];
  $company=$_POST['company'];
  $category=strtolower($_POST["job_category"]);
  $county=strtolower($_POST["county"]);
  $job_type=$_POST["jobtype"];
  $contract_type=$_POST["contracttype"];
  $salary_type=$_POST["salarytype"];
  $per_annum=$_POST["per_annum"];
  $pro_rata=$_POST["pro_rata"];
  $per_hourly=$_POST["per_hour"];
  $overview=$_POST["overview"];
  $duties=$_POST["duties"];

  $st = 'a';

  if($salary_type == 'per annum'){
    $st = 'a';
  }else if($salary_type == 'pro rata'){
    $st = 'd';
  }else if($salary_type == 'per hour'){
    $st = 'h';
  }


  $input_xml = '<FILE>
                <ADVERT>
                <ROOT LOADTYPE="A">
          		  <JOB 	FEEDID="TEST"
                LIVEDAYS="28"
                UNIQUEJOBNO="'.$newid.'"
                DESCRIPTION="'.$overview.'"
                TITLE="'.$title.'"
                REGION="NEED"
                TOWN="NEED"
                POSTCODE="NEED"
                RATEUNIT="'.$st.'"
                SALARYMIN="25000"
                SALARYMAX="30000"
                INTERNALJOBREF="LAST INSERTED JOB NUMBER"
                RESPONSEURL="http://www.totaljobs.com/"
                HIDEFULLPOSTCODE="False" />
                		<POSTINGCOMPANY USERNAME="loadtesttj"
                PASSWORD="testonly" />
                  		<CONTACT PHONE="0151 909 2616"
                       EMAIL="admin@skyrandd.co.uk"
                       FIRSTNAME="Sky Recruitment" />
                  		<ADDITIONALJOBTYPE GROUP="'.$job_type.'" />
                  	</ROOT>
                  	</ADVERT>
                  </FILE>';


    $url = "http://59.162.33.102:9301/Avalability";

    //setting the curl parameters.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Following line is compulsary to add as it is:
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "xmlRequest=" . $input_xml);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
    $data = curl_exec($ch);
    curl_close($ch);

    //convert the XML result into array
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

    print_r('<pre>');
    print_r($array_data);
    print_r('</pre>');
