<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 function account_code($value, $code){
    if($value == 'escort'){
      return 'ES'.$code;
    }else if($value == 'agency'){
      return 'AG'.$code;
    }else if($value == 'user'){
      return 'US'.$code;
    }else if($value == 'admin'){
      return 'AD'.$code;
    }
}

 $id = $_GET['id'];

 $db->query("select bv_approval.*
 from bv_approval
 where bv_approval.id = ?");
 $db->bind(1,$id);
 $data = $db->single();

 ?>
 <?if($data['type'] == 'age'){?>
   <h3>Age Verification</h3>
   <?
   $db->query("select * from ws_uploads where id = ?");
   $db->bind(1,$data['pid']);
   $upload = $db->single();

   $db->query("select * from accounts where id = ?");
   $db->bind(1,$data['account_id']);
   $account = $db->single();

   $db->query("select * from bv_accounts_info where pid = ?");
   $db->bind(1,$data['account_id']);
   $account_info = $db->single();

   $url = $fullurl.$upload['path'].$upload['name'];
   ?>

   <style>
   table {
     font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;
    }

    td, th {
     border: 1px solid #dddddd;
     text-align: left;
     padding: 8px;
    }
   </style>
   <table>
      <tr>
        <th>Account Code</th>
        <td><?echo account_code($account['type'], $account['ac_code']);?></td>
      </tr>
      <tr>
        <th>Name</th>
        <td><?echo $account_info['first_name'].' '.$account_info['surname'];?></td>
      </tr>
      <tr>
        <th>DOB</th>
        <td><?echo date('d-m-Y', strtotime($account_info['dob']));?></td>
      </tr>
     <tr>
       <th>Screenname</th>
       <td><?echo $account['screenname'];?></td>
     </tr>
     <tr>
       <th>Email</th>
       <td><?echo $account['email'];?></td>
     </tr>
   </table>

   <ul class="list-unstyled">
     <li>
       <li class="list-group-item" style="width: 100%; display:table; margin-top:20px;">
         <span class="filetypes filetypes-jpg" aria-hidden="true" style=" font-size:44px;"></span>
         <span class="" style="display:table-cell; vertical-align:middle; width: 100%; padding-left: 10px;"><a href="<?echo $url;?>" target="_blank"><?echo $upload['name'];?></a></span>
       </li>
     </li>
   </ul>
 <?} else if($data['type'] == 'profile'){?>
   <h3>Profile Image Verification</h3>
   <?
   $db->query("select * from ws_uploads where id = ?");
   $db->bind(1,$data['pid']);
   $upload = $db->single();

   $db->query("select * from accounts where id = ?");
   $db->bind(1,$data['account_id']);
   $account = $db->single();

   $db->query("select * from bv_accounts_info where pid = ?");
   $db->bind(1,$data['account_id']);
   $account_info = $db->single();

   $url = $fullurl.$upload['path'].$upload['name'];
   ?>

   <style>
   table {
     font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;
    }

    td, th {
     border: 1px solid #dddddd;
     text-align: left;
     padding: 8px;
    }
   </style>
   <table>
      <tr>
        <th>Account Code</th>
        <td><?echo account_code($account['type'], $account['ac_code']);?></td>
      </tr>
      <tr>
        <th>Name</th>
        <td><?echo $account_info['first_name'].' '.$account_info['surname'];?></td>
      </tr>
      <tr>
        <th>DOB</th>
        <td><?echo date('d-m-Y', strtotime($account_info['dob']));?></td>
      </tr>
     <tr>
       <th>Screenname</th>
       <td><?echo $account['screenname'];?></td>
     </tr>
     <tr>
       <th>Email</th>
       <td><?echo $account['email'];?></td>
     </tr>
   </table>

   <ul class="list-unstyled">
     <li>
       <li class="list-group-item" style="width: 100%; display:table; margin-top:20px;">
         <span class="filetypes filetypes-jpg" aria-hidden="true" style=" font-size:44px;"></span>
         <span class="" style="display:table-cell; vertical-align:middle; width: 100%; padding-left: 10px;"><a href="<?echo $url;?>" target="_blank"><?echo $upload['name'];?></a></span>
       </li>
     </li>
   </ul>
 <?}?>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-danger" id="decline_approval" data-id="<?echo $id;?>">Decline</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="approval_accept" data-id="<?echo $id;?>">Approval</button>
