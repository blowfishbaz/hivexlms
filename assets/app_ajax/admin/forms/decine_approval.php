<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 $db->query("select bv_approval.*
 from bv_approval
 where bv_approval.id = ?");
 $db->bind(1,$id);
 $data = $db->single();

 ?>
 <h3>Reason for Decline</h3>
 <form id="decline_approval_form">
  <input type="hidden" name="approval_id" value="<?echo $id;?>" />
  <div class="form-group label-floating is-empty">
      <label for="decline_reason" class="control-label">Reason</label>
      <textarea class="form-control" rows="5" id="decline_reason" name="decline_reason" required></textarea>
  </div>


 </form>
 <button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
 <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="confirm_decline_approval">Confirm</button>
