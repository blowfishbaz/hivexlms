<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $newid = createId('att');
  $now = time();
  $name=strtolower($_POST["name"]);
  $email=strtolower($_POST["email"]);
  $type=strtolower($_POST["type"]);
  $status=$_POST["status"];

if($_SESSION['SESS_ACCOUNT_Type']=='admin'){
  $db = new database;
  $db->Query("INSERT INTO accounts (id ,created_date ,name, email, type, status,username,password) values (?,?,?,?,?,?,?,?)");
  $db->bind(1,$newid);
  $db->bind(2,$now);
  $db->bind(3,$name);
  $db->bind(4,$email);
  $db->bind(5,$type);
  $db->bind(6,$status);
  $db->bind(7,encrypt($email));
  $db->bind(8,encrypt($_POST["newpassword"]));
  $db->execute();

  echo 'ok';
}
?>
