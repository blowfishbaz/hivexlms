<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $id = $_POST["page_id"];
  $name=$_POST["page_name"];
  $page_info=$_POST['page_info'];




  $db = new database;
  $db->Query("UPDATE hx_pages SET name= ?, page_info= ? WHERE id = ?");
  $db->bind(1,$name);
  $db->bind(2,$page_info);
  $db->bind(3,$id);
  $db->execute();

  echo 'ok';
?>
