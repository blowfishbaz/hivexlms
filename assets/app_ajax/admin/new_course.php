<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$newid = createId('crs');

$title=strtolower($_POST["title"]);
$course_info=strtolower($_POST["course_info"]);

if($_POST['online']=="true"){
    $online_course_type=strtolower($_POST["online_course_info"]);
    $online_pass_perc=strtolower($_POST["online_pass_perc"]);
    $online_course_price=strtolower($_POST["online_course_price"]);
    $online_course_credit_price=strtolower($_POST["online_course_credit_price"]);
    $online_course_duration=strtolower($_POST["online_course_duration"]);
    $isOnline = '1';
}else{
    $online_course_type = "0";
    $online_pass_perc = "0";
    $online_course_price = "0";
    $online_course_credit_price = "0";
    $online_course_duration = "0";
    $isOnline = '0';
}

if($_POST['vc']=="true"){
    $vc_course_type=strtolower($_POST["vc_course_info"]);
    $vc_pass_perc=strtolower($_POST["vc_pass_perc"]);
    $vc_course_price=strtolower($_POST["vc_course_price"]);
    $vc_course_credit_price=strtolower($_POST["vc_course_credit_price"]);
    $vc_course_duration=strtolower($_POST["vc_course_duration"]);
    $isVc = '1';
}else{
    $vc_course_type = "0";
    $vc_pass_perc = "0";
    $vc_course_price = "0";
    $vc_course_credit_price = "0";
    $vc_course_duration = "0";
    $isVc = '0';
}

if($_POST['f2f']=="true"){
    $f2f_course_type=strtolower($_POST["f2f_course_info"]);
    $f2f_pass_perc=strtolower($_POST["f2f_pass_perc"]);
    $f2f_course_price=strtolower($_POST["f2f_course_price"]);
    $f2f_course_credit_price=strtolower($_POST["f2f_course_credit_price"]);
    $f2f_course_duration=strtolower($_POST["f2f_course_duration"]);
    $isF2f = '1';
  }else{
    $f2f_course_type = "0";
    $f2f_pass_perc = "0";
    $f2f_course_price = "0";
    $f2f_course_credit_price = "0";
    $f2f_course_duration = "0";
    $isF2f = '0';
  }

$db = new database;
$db->Query("INSERT INTO lms_course (id, title, description, online, vc, f2f, online_description, online_pass_perc, online_course_price, online_course_credit_price, online_course_duration, vc_description, vc_pass_perc, vc_course_price, vc_course_credit_price, vc_course_duration, f2f_description, f2f_pass_perc, f2f_course_price, f2f_course_credit_price, f2f_course_duration, status) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,$newid);
$db->bind(2,$title);
$db->bind(3,$course_info);
$db->bind(4,$isOnline);
$db->bind(5,$isVc);
$db->bind(6,$isF2f);
$db->bind(7,$online_course_type);
$db->bind(8,$online_pass_perc);
$db->bind(9,$online_course_price);
$db->bind(10,$online_course_credit_price);
$db->bind(11,$online_course_duration);
$db->bind(12,$vc_course_type);
$db->bind(13,$vc_pass_perc);
$db->bind(14,$vc_course_price);
$db->bind(15,$vc_course_credit_price);
$db->bind(16,$vc_course_duration);
$db->bind(17,$f2f_course_type);
$db->bind(18,$f2f_pass_perc);
$db->bind(19,$f2f_course_price);
$db->bind(20,$f2f_course_credit_price);
$db->bind(21,$f2f_course_duration);
$db->bind(22,'1');
$db->execute();

$returnThis = array('status' => 'ok', 'cid' => $newid);
echo json_encode($returnThis);

?>
