<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'id';
	$order = 'desc';
}

$filter = urldecode($_GET['filter']);
$filter = json_decode($filter);
$filterall ='';


$db->query("select * from hx_jobs where status != ?");
$db->bind(1,'42');
$db->execute();
$rowcount = $db->rowcount();

$db->query("select * from hx_jobs where status != ? order by $sort $order LIMIT ? offset ?");
$db->bind(1,'42');
$db->bind(2,(int) $limit);
$db->bind(3,(int) $offset);
$data = $db->ResultSet();
?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
