<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');





$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'bv_approval.id';
	$order = 'desc';
}

$filter = urldecode($_GET['filter']);
$filter = json_decode($filter);
$filterall ='';


$db->query("select bv_approval.*, bv_approval.id as app_id, accounts.screenname, accounts.ac_code, accounts.type as acc_type
from bv_approval
left join accounts
on accounts.id = bv_approval.account_id
where bv_approval.status = ?");
$db->bind(1,'1');
$db->execute();
$rowcount = $db->rowcount();

$db->query("select bv_approval.*, bv_approval.id as app_id, accounts.screenname, accounts.ac_code, accounts.type as acc_type
from bv_approval
left join accounts
on accounts.id = bv_approval.account_id
where bv_approval.status = ?
order by $sort $order LIMIT ? offset ?");
$db->bind(1,'1');
$db->bind(2,(int) $limit);
$db->bind(3,(int) $offset);
$data = $db->ResultSet();
?>
{

"total": <? echo  $rowcount.' '; ?>,
"rows":

<?
 echo json_encode( $data );

?>

}
