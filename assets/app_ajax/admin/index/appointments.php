<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'ws_appointments.id';
	$order = 'ASC';
}

$filter = urldecode($_GET['filter']);
$filter = json_decode($filter);
$filterall ='';

$today= strtotime(date('Y-m-d'));

$db->query("SELECT * FROM ws_appointments where appointment >= ? AND status = ?");
$db->bind(1,$today);
$db->bind(2,'1');
$db->execute();
$rowcount = $db->rowcount();

$db->query("SELECT ws_appointments.id, ws_appointments.slot, ws_appointments.app_date, ws_accounts.first_name, ws_accounts.surname FROM ws_appointments JOIN ws_accounts ON ws_appointments.pid = ws_accounts.id where ws_appointments.appointment >= ? AND ws_appointments.status = ? order by $sort $order LIMIT ? offset ?");
$db->bind(1,$today);
$db->bind(2,'1');
$db->bind(3,(int) $limit);
$db->bind(4,(int) $offset);
$data = $db->ResultSet();
?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
