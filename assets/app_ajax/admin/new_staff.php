<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $newid = createId('staff');
  $now = time();
  $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
  $staff_name=$_POST["staff_name"];
  $info=$_POST['info'];
  $photo_id=$_POST["photo_id"];




  $db = new database;
  $db->Query("INSERT INTO hx_staff (id ,created_date ,created_by, status, name, info, image) values (?,?,?,?,?,?,?)");
  $db->bind(1,$newid);
  $db->bind(2,$now);
  $db->bind(3,$user);
  $db->bind(4,'1');
  $db->bind(5,$staff_name);
  $db->bind(6,$info);
  $db->bind(7,$photo_id);

  $db->execute();

  echo 'ok';
?>
