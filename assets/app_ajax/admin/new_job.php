<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $newid = createId('job');
  $now = time();
  $user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
  $title=$_POST["title"];
  $company=$_POST['company'];
  $category=strtolower($_POST["job_category"]);
  $county=strtolower($_POST["county"]);
  $job_type=$_POST["jobtype"];
  $contract_type=$_POST["contracttype"];
  $salary_type=$_POST["salarytype"];
  $per_annum=$_POST["per_annum"];
  $pro_rata=$_POST["pro_rata"];
  $per_hourly=$_POST["per_hour"];
  $overview=$_POST["overview"];
  $duties=$_POST["duties"];
  $region=$_POST['region'];
  $postcode=$_POST['postcode'];
  $job_post = $_POST['post_job'];

  $db = new database;
  $db->Query("INSERT INTO hx_jobs (id ,created_date ,created_by, status, title, company, category,county, job_type, contract_type, salary_type, per_annum, per_hourly, overview, duties, region, postcode, job_post) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
  $db->bind(1,$newid);
  $db->bind(2,$now);
  $db->bind(3,$user);
  $db->bind(4,'2');
  $db->bind(5,$title);
  $db->bind(6,$company);
  $db->bind(7,$category);
  $db->bind(8,$county);
  $db->bind(9,$job_type);
  $db->bind(10,$contract_type);
  $db->bind(11,$salary_type);
  $db->bind(12,$per_annum.$pro_rata);
  $db->bind(13,$per_hourly);
  $db->bind(14,$overview);
  $db->bind(15,$duties);
  $db->bind(16,$region);
  $db->bind(17,$postcode);
  $db->bind(18,$job_post);
  $db->execute();

  if($job_post == 'Total Jobs & Website'){
  $db->query("select * from hx_jobs where id = ? ");
  $db->bind(1,$_GET['id']);
  $job = $db->single();

  $job_number = 'SR'.$job['job_number'];

  $st = 'a';

  if($salary_type == 'per annum'){
    $st = 'a';
  }else if($salary_type == 'per hour'){
    $st = 'h';
  }

  $salary_min = find_min_sal($salary_type,$per_annum,$per_hourly);
  $salary_max = find_max_sal($salary_type,$per_annum,$per_hourly);

  $input_xml = '<FILE>
                <ADVERT>
                <ROOT LOADTYPE="A">
          		  <JOB 	FEEDID="BLWFSH0001"
                LIVEDAYS="28"
                UNIQUEJOBNO="'.$newid.'"
                DESCRIPTION="'.$overview.'"
                TITLE="'.$title.'"
                REGION="'.$region.'"
                TOWN="'.$county.'"
                POSTCODE="'.$postcode.'"
                RATEUNIT="'.$st.'"
                SALARYMIN="'.$salary_min.'"
                SALARYMAX="'.$salary_max.'"
                INTERNALJOBREF="'.$job_number.'"
                RESPONSEURL="http://www.totaljobs.com/"
                HIDEFULLPOSTCODE="False" />
                		<POSTINGCOMPANY USERNAME="TJfeedExceptionalCa"
                PASSWORD="QelzTjl4" />
                  		<CONTACT PHONE="0151 909 2616"
                       EMAIL="admin@skyrandd.co.uk"
                       FIRSTNAME="Sky Recruitment" />
                  		<ADDITIONALJOBTYPE GROUP="'.$job_type.'" />
                  	</ROOT>
                  	</ADVERT>
                  </FILE>';

                  $url = "https://recruiter.totaljobs.com/api/job";

                  //setting the curl parameters.
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL, $url);
                  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                  // Following line is compulsary to add as it is:
                  curl_setopt($ch, CURLOPT_POSTFIELDS,
                              "xmlRequest=" . $input_xml);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                  $data = curl_exec($ch);
                  curl_close($ch);

                  //convert the XML result into array
                  $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

                  // print_r('<pre>');
                  // print_r($array_data);
                  // print_r('</pre>');

  }
  echo 'ok';
?>
