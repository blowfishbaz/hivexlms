<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');



$newid = createId('img');
$type=strtolower($_POST["uptype"]);
$cid=strtolower($_POST["cid"]);
$link=$_POST["filename"];


$db->Query("INSERT INTO lms_upload (id,type,cid,link) values (?,?,?,?)");
$db->bind(1,$newid);
$db->bind(2,$type);
$db->bind(3,$cid);
$db->bind(4,$link);
$db->execute();


$db->query("select * from lms_course where id = ?");
$db->bind(1,$_POST["cid"]);
$c = $db->single();


if($c["online"]=='1'){
  $isOnline = '1';
}else{
  $isOnline = '0';
}

if($c["vc"]=='1'){
  $isVc = '1';
} else{
  $isVc = '0';
}

if($c["f2f"]=='1'){
  $isf2f = '1';
} else{
  $isf2f = '0';
}


$newidr = createId('res');
$db->Query("INSERT INTO lms_resource (id,rid,rtitle,rdesc,online,vc,f2f) values (?,?,?,?,?,?,?)");
$db->bind(1,$newidr);
$db->bind(2,$newid);
$db->bind(3,"!!PLEASE EDIT!!");
$db->bind(4,"!!PLEASE EDIT!!");
$db->bind(5,$isOnline);
$db->bind(6,$isVc);
$db->bind(7,$isf2f);
$db->execute();


?>
