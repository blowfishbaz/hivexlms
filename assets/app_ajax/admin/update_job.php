<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $id = $_POST["id"];
  $title=$_POST["title"];
  $company=$_POST['company'];
  $category=strtolower($_POST["job_category"]);
  $county=strtolower($_POST["county"]);
  $job_type=$_POST["jobtype"];
  $contract_type=$_POST["contracttype"];
  $salary_type=$_POST["salarytype"];

  if($salary_type=='per annum'){$per_annum=$_POST["per_annum"];}
  else{$per_annum=$_POST["pro_rata"];}

  $per_hour=$_POST["per_hour"];
  $overview=$_POST["overview"];
  $duties=$_POST["duties"];

  if($per_hour==''){$per_hour=0;}

  $region=$_POST['region'];
  $postcode=$_POST['postcode'];
  $job_post = $_POST['post_job'];

  $db = new database;
  $db->Query("UPDATE hx_jobs SET title= ?, company= ?, category= ? ,county= ?, job_type= ?, contract_type= ?, salary_type= ?, per_annum= ?, per_hourly= ?, overview= ?, duties= ?, region = ?, postcode = ?, job_post = ? WHERE id = ?");
  $db->bind(1,$title);
  $db->bind(2,$company);
  $db->bind(3,$category);
  $db->bind(4,$county);
  $db->bind(5,$job_type);
  $db->bind(6,$contract_type);
  $db->bind(7,$salary_type);
  $db->bind(8,$per_annum);
  $db->bind(9,$per_hour);
  $db->bind(10,$overview);
  $db->bind(11,$duties);
  $db->bind(12,$region);
  $db->bind(13,$postcode);
  $db->bind(14,$job_post);
  $db->bind(15,$id);
  $db->execute();


  if($job_post == 'Total Jobs & Website'){
  $db->query("select * from hx_jobs where id = ? ");
  $db->bind(1,$id);
  $job = $db->single();

  $job_number = ''.$job['job_number'];

  $st = 'a';

  if($salary_type == 'per annum'){
    $st = 'a';
  }else if($salary_type == 'per hour'){
    $st = 'h';
  }

  $salary_min = find_min_sal($salary_type,$per_annum,$per_hour);
  $salary_max = find_max_sal($salary_type,$per_annum,$per_hour);

  $input_xml = '<FILE>
                <ADVERT>
                <ROOT LOADTYPE="U">
          		  <JOB 	FEEDID="BLWFSH0001"
                LIVEDAYS="28"
                UNIQUEJOBNO="'.$id.'"
                DESCRIPTION="'.$overview.'"
                TITLE="'.$title.'"
                REGION="'.$region.'"
                TOWN="'.$county.'"
                POSTCODE="'.$postcode.'"
                RATEUNIT="'.$st.'"
                SALARYMIN="'.$salary_min.'"
                SALARYMAX="'.$salary_max.'"
                INTERNALJOBREF="'.$job_number.'"
                RESPONSEURL="http://www.totaljobs.com/"
                HIDEFULLPOSTCODE="False" />
                		<POSTINGCOMPANY USERNAME="TJfeedExceptionalCa"
                PASSWORD="QelzTjl4" />
                  		<CONTACT PHONE="0151 909 2616"
                       EMAIL="admin@skyrandd.co.uk"
                       FIRSTNAME="Sky Recruitment" />
                  		<ADDITIONALJOBTYPE GROUP="'.$job_type.'" />
                  	</ROOT>
                  	</ADVERT>
                  </FILE>';

                  $url = "https://recruiter.totaljobs.com/api/job";

                  //setting the curl parameters.
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL, $url);
                  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                  // Following line is compulsary to add as it is:
                  curl_setopt($ch, CURLOPT_POSTFIELDS,
                              "xmlRequest=" . $input_xml);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                  $data = curl_exec($ch);
                  curl_close($ch);

                  //convert the XML result into array
                  $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

                  // print_r('<pre>');
                  // print_r($array_data);
                  // print_r('</pre>');

  }


  echo 'ok';
?>
