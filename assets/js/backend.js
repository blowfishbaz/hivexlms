var $dateclicked;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Users///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$( "body" ).on( "click", ".viewuser", function(e) {
   $user = $(this).data('id');
   $("#BaseModalLContent").html($Loading);
   $('#BaseModalL').modal('show');

            $.ajax({
                type: "POST",
                url: $fullurl+"assets/app_forms/admin/users.php?id="+$user,
                data: "id:1",
                success: function(msg){
                   $("#BaseModalLContent").delay(1000)
                    .queue(function(n) {
                        $(this).html(msg);
                        n();
                    }).fadeIn("slow").queue(function(n) {
                         $.material.init();
                        n();
                    });
                }
          });
});

$( "body" ).on( "click", "#editAccFormBut", function() {
    var HasError = 0;
    var formid = '#editAccForm';

  $(formid).find('select').each(function(){
          $(this).siblings().removeClass('has-error');
          Messenger().hideAll();
          if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                   if(!$(this).prop('required')){}
                   else {

                HasError = 1;
                $(this).siblings().addClass('has-error');
              }
            }
    });
    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){}
               else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if (HasError == 1) {
      Messenger().post({
          message: 'Please make sure all required elements of the form are filled out.',
          type: 'error',
          showCloseButton: false
      });
    }
    else{
      var FRMdata = $(formid).serialize(); // get form data
      $("#BaseModalLContent").html($Loading);
    $.ajax({
          type: "POST",
          url: $fullurl+"assets/app_ajax/admin/update_user.php",
          data: FRMdata, // serializes the form's elements.
          success: function(msg){
              $message=$.trim(msg);
              Messenger().post({
                      message: 'User Updated.',
                      showCloseButton: false
              });
              setTimeout(function(){
                $('#BaseModalL').modal('hide');
                $('#table').bootstrapTable('refresh');
              },600);

         },error: function (xhr, status, errorThrown) {
                setTimeout(function(){alert('Error');},300);
              }
    });
  }
});


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Jobs////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$( "body" ).on( "click", "#add_new_job", function(e) {
   $("#BaseModalLContent").html($Loading);
   $('#BaseModalL').modal({backdrop: 'static'});
   //$('#BaseModalL').modal('show');

                    $.ajax({
                        type: "POST",
                        url: $fullurl+"assets/app_forms/admin/new_job.php",
                        data: "id:1",
                        success: function(msg){

                           //alert(msg);
                           $("#BaseModalLContent").delay(1000)
                            .queue(function(n) {
                                $(this).html(msg);
                                n();
                            }).fadeIn("slow").queue(function(n) {
                                             $.material.init();
                                            n();
                                            $('#county').selectpicker();
                                            $('#job_category').selectpicker();
                                            $('#region').selectpicker();

                                            $('#overview').summernote({
                                              height: 300,                 // set editor height
                                              minHeight: null,             // set minimum height of editor
                                              maxHeight: null,             // set maximum height of editor
                                              placeholder:'Duties...',
                                              toolbar: [
                                                ['font', ['bold', 'italic', 'underline']],
                                                ['color', ['color']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'hr']],
                                                ['help', ['help']]
                                                ]
                                            });
                                            $('#duties').summernote({
                                              height: 300,                 // set editor height
                                              minHeight: null,             // set minimum height of editor
                                              maxHeight: null,             // set maximum height of editor
                                              placeholder:'Duties...',
                                              toolbar: [
                                                ['font', ['bold', 'italic', 'underline']],
                                                ['color', ['color']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'hr']],
                                                ['help', ['help']]
                                                ]
                                            });
                            });
                        }
                  });
});

$( "body" ).on( "change", ".salarytype", function(e) {
  var $v=$(this).val();

  $('#per_annum').val('');
  $('#pro_rata').val('');
  $('#per_hour').val('');

  $("#per_annum").attr('required',false);
  $("#pro_rata").attr('required',false);
  $("#per_hour").attr('required',false);

  $('#per_annum').parent().hide('slow');
  $('#per_hour').parent().hide('slow');

  if($v=='per annum'){$('#per_annum').parent().show('slow'); $('#pro_rata').parent().hide('slow');$("#per_annum").attr('required',true);}
  else if($v=='pro rata'){$('#pro_rata').parent().show('slow');$("#pro_rata").attr('required',true);}
  else if($v=='per hour'){
    $('#per_hour').parent().show('slow');
    $('#pro_rata').parent().show('slow');
    $("#per_hour").attr('required',true);
    $("#pro_rata").attr('required',true);
  }
  else{$('#pro_rata').parent().hide('slow');}
});

$( "body" ).on( "click", "#NewJobFormBut", function() {

            var formid = '#NewJobForm';
            var FRMdata = $(formid).serialize(); // get form data

            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/new_job.php",
                    data: FRMdata, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Job Added.',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){
                                       $('#BaseModalL').modal('hide');
                                     $('#table').bootstrapTable('refresh');
                                                //location.reload();
                                    },1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    }
            });

});


$( "body" ).on( "click", "#edit_job", function(e) {
   $("#BaseModalLContent").html($Loading);
   $('#BaseModalL').modal({backdrop: 'static'});
   //$('#BaseModalL').modal('show');
   $id= $(this).attr("data-id");

                    $.ajax({
                        type: "POST",
                        url: $fullurl+"assets/app_forms/admin/edit_job.php?id="+$id,
                        data: "id:1",
                        success: function(msg){

                           //alert(msg);
                           $("#BaseModalLContent").delay(1000)
                            .queue(function(n) {
                                $(this).html(msg);
                                n();
                            }).fadeIn("slow").queue(function(n) {
                                             $.material.init();
                                            n();
                                            $('#county').selectpicker();
                                            $('#overview').summernote({
                                              height: 300,                 // set editor height
                                              minHeight: null,             // set minimum height of editor
                                              maxHeight: null,             // set maximum height of editor
                                              placeholder:'Duties...',
                                              toolbar: [
                                                ['font', ['bold', 'italic', 'underline']],
                                                ['color', ['color']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'hr']],
                                                ['help', ['help']]
                                                ]
                                            });
                                            $('#duties').summernote({
                                              height: 300,                 // set editor height
                                              minHeight: null,             // set minimum height of editor
                                              maxHeight: null,             // set maximum height of editor
                                              placeholder:'Duties...',
                                              toolbar: [
                                                ['font', ['bold', 'italic', 'underline']],
                                                ['color', ['color']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', 'hr']],
                                                ['help', ['help']]
                                                ]
                                            });
                            });
                        }
                  });
});


$( "body" ).on( "click", "#EditJobFormBut", function() {

            var formid = '#EditJobForm';
            var FRMdata = $(formid).serialize(); // get form data

            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/update_job.php",
                    data: FRMdata, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Job Edited.',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){
                                       //$('#BaseModalL').modal('hide');
                                     //$('#table').bootstrapTable('refresh');
                                                location.reload();
                                    },1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    }
            });

});

$( "body" ).on( "click", "#publish_job", function() {
            $id= $(this).attr("data-id");
            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/publish_job.php",
                    data: {id:$id}, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Job Published',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){location.reload();},1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    },error: function (xhr, status, errorThrown) {
                           setTimeout(function(){alert('Error');},300);
                         }
            });

});

$( "body" ).on( "click", "#unpublish_job", function() {
            $id= $(this).attr("data-id");
            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/unpublish_job.php",
                    data: {id:$id}, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Job Unpublished',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){location.reload();},1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    },error: function (xhr, status, errorThrown) {
                           setTimeout(function(){alert('Error');},300);
                         }
            });

});

$( "body" ).on( "click", "#Close_Job", function() {
            $id= $(this).attr("data-id");
            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/close_job.php",
                    data: {id:$id}, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               //$("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Job Closed',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){location.reload();},1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    },error: function (xhr, status, errorThrown) {
                           setTimeout(function(){alert('Error');},300);
                         }
            });

});
$( "body" ).on( "click", "#Open_Job", function() {
            $id= $(this).attr("data-id");
            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/unpublish_job.php",
                    data: {id:$id}, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Job Opened',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){location.reload();},1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    },error: function (xhr, status, errorThrown) {
                           setTimeout(function(){alert('Error');},300);
                         }
            });

});

$( "body" ).on( "click", ".job_app_unsuccessful", function(e) {
    $id= $(this).attr("data-id");

    $.ajax({
            type: "POST",
            url: $fullurl+"assets/app_ajax/backend/update_job_application.php",
            data: {id:$id,status:'0'}, // serializes the form's elements.
            success: function(data){

                       $("#BaseModalLContent").html($Loading);
                            Messenger().post({
                                   message: 'Job Application Status Updated',
                                   showCloseButton: false
                            });
                          setTimeout(function(){location.reload();},1000);
            },error: function (xhr, status, errorThrown) {
              Messenger().post({
                      message: 'An Error Has occured.',
                      type: 'error',
                      showCloseButton: false
              });
                 }
    });

  });

  $( "body" ).on( "click", ".job_app_maybe", function(e) {
      $id= $(this).attr("data-id");

      $.ajax({
              type: "POST",
              url: $fullurl+"assets/app_ajax/backend/update_job_application.php",
              data: {id:$id,status:'2'}, // serializes the form's elements.
              success: function(data){

                         $("#BaseModalLContent").html($Loading);
                              Messenger().post({
                                     message: 'Job Application Status Updated',
                                     showCloseButton: false
                              });
                            setTimeout(function(){location.reload();},1000);
              },error: function (xhr, status, errorThrown) {
                Messenger().post({
                        message: 'An Error Has occured.',
                        type: 'error',
                        showCloseButton: false
                });
                   }
      });

    });

    $( "body" ).on( "click", ".job_app_successful", function(e) {
      $id= $(this).attr("data-id");

      $.ajax({
              type: "POST",
              url: $fullurl+"assets/app_ajax/backend/update_job_application.php",
              data: {id:$id,status:'3'}, // serializes the form's elements.
              success: function(data){

                         $("#BaseModalLContent").html($Loading);
                              Messenger().post({
                                     message: 'Job Application Status Updated',
                                     showCloseButton: false
                              });
                            setTimeout(function(){location.reload();},1000);
              },error: function (xhr, status, errorThrown) {
                Messenger().post({
                        message: 'An Error Has occured.',
                        type: 'error',
                        showCloseButton: false
                });
                   }
      });
    });

$( "body" ).on( "click", ".job_more_info", function(e) {
  $(this).parent().next('.job_user_info').show('slow');
  $(this).hide('slow');
});

$( "body" ).on( "click", ".job_less_info", function(e) {
  $(this).parent().parent().parent().hide('slow');
  $(this).parent().parent().parent().prev('.col-lg-12').children('.job_more_info').show('slow');
});


$( "body" ).on( "click", ".job_ref_received", function(e) {
  $id= $(this).attr("data-id");

  $.ajax({
          type: "POST",
          url: $fullurl+"assets/app_ajax/backend/update_job_references.php",
          data: {id:$id,reference:'2'}, // serializes the form's elements.
          success: function(data){

                     $("#BaseModalLContent").html($Loading);
                          Messenger().post({
                                 message: 'Job References Status Updated',
                                 showCloseButton: false
                          });
                        setTimeout(function(){location.reload();},1000);
          },error: function (xhr, status, errorThrown) {
            Messenger().post({
                    message: 'An Error Has occured.',
                    type: 'error',
                    showCloseButton: false
            });
               }
  });
});

$( "body" ).on( "click", ".job_not_ref_received", function(e) {
  $id= $(this).attr("data-id");

  $.ajax({
          type: "POST",
          url: $fullurl+"assets/app_ajax/backend/update_job_references.php",
          data: {id:$id,reference:'1'}, // serializes the form's elements.
          success: function(data){

                     $("#BaseModalLContent").html($Loading);
                          Messenger().post({
                                 message: 'Job References Status Updated',
                                 showCloseButton: false
                          });
                        setTimeout(function(){location.reload();},1000);
          },error: function (xhr, status, errorThrown) {
            Messenger().post({
                    message: 'An Error Has occured.',
                    type: 'error',
                    showCloseButton: false
            });
               }
  });
});



$( "body" ).on( "click", "#edit_appointment", function() {
             $id=$(this).attr('data-id');
              $("#BaseModalLContent").html($Loader);
              $('#BaseModalL').modal('show');

                   $.ajax({
                      type: "POST",
                      url: $fullurl+"assets/app_form/edit_appointment.php",
                      data: {id:$id},
                      success: function(msg){
                        $("#BaseModalLContent").delay(600)
                         .queue(function(n) {
                             $(this).html(msg);
                             n();
                         }).fadeIn("slow").queue(function(n) {
                                      $.material.init();
                                     n();
                         });
                        }
                 });
      });


      $( "body" ).on( "click", "#Remove_Appointment", function() {
                   $id=$(this).attr('data-id');
                    $("#BaseModalLContent").html($Loader);
                    //$('#BaseModalL').modal('show');

                         $.ajax({
                            type: "POST",
                            url: $fullurl+"assets/app_form/delete_appointment.php",
                            data: {id:$id},
                            success: function(msg){
                              $("#BaseModalLContent").delay(600)
                               .queue(function(n) {
                                   $(this).html(msg);
                                   n();
                               }).fadeIn("slow").queue(function(n) {
                                            $.material.init();
                                           n();
                               });
                              }
                       });
            });

$( "body" ).on( "click", "#Remove_Appointment_Ok", function(e) {
$id=$(this).attr('data-id');
$("#BaseModalLContent").html($Loader);

 $.ajax({
         type: "POST",
         url: $fullurl+"assets/app_ajax/backend/update_member_appointment.php",
         data: {id:$id},
         success: function(msg){
             $message=$.trim(msg);
             Messenger().post({
                     message: 'Appointment Updated.',
                     showCloseButton: false
             });
             setTimeout(function(){
               window.location.replace($fullurl+'admin/appointments/index.php');
             },600);

          },error: function (xhr, status, errorThrown) {
         setTimeout(function(){alert('Error');},300);
       }
 });
});


$( "body" ).on( "click", "#Edit_Appointment", function(e) {
$id=$(this).attr('data-id');
$("#BaseModalLContent").html($Loader);
$('#BaseModalL').modal('show');
$.ajax({
type: "POST",
url: $fullurl+"assets/app_form/appointment.php",
data: {id:$id},
success: function(msg){
 $("#BaseModalLContent").delay(600)
  .queue(function(n) {
      $(this).html(msg);
      n();
  }).fadeIn("slow").queue(function(n) {
               $.material.init();
              n();

              $('#calendar').fullCalendar({
                 header: {
                   left: 'prev,next today',
                   center: 'title',
                   right: 'month'
                 },
                 dayClick: function(date, jsEvent, view) {
                   $dateclicked=date.format();
                   var $weekday = date.weekday();
                   var $today=moment($('#calendar').fullCalendar('getDate')).format('YYYY-MM-DD');
                   if($dateclicked <= $today){
                     // Previous Date
                     $('.timebox').hide('slow');
                     $('.time_picker').attr('disabled',true);
                     $('#BookUserApp').hide('slow');
                   }
                   else{// Future Date
                     if($weekday!='5' & $weekday!='6'){// Weekday
                       BookingApp($dateclicked); // Get appointments slots
                       $('.timebox').show('slow');
                       $('.time_picker').attr('disabled',true);
                       $('#BookUserApp').hide('slow');
                     }else{
                       $('.timebox').hide('slow');
                       $('.time_picker').attr('disabled',true);
                       $('#BookUserApp').hide('slow');
                     }//Weekend
                   }
                 },
                 slotEventOverlap: false,
                 //height: 582,
                 firstDay: 1,
                 editable: false,
                 eventLimit: true, // allow "more" link when too many events
                 timezone: 'Europe/London',
               });
  });
 }
});

});

function BookingApp($date){
$.ajax({
   type: "POST",
   url: $fullurl+"assets/website/app_ajax/check_appointment_availability.php",
   data: {date:$date},
   // dataType: "json",
   success: function(msg){
     $('.time_picker').attr('disabled',false);
      $slots=$.trim(msg);
      $slots=$.parseJSON($slots);

     $.each($slots, function( index, value ) {
       console.log(index + ": " + value);

      $('.time_picker[data-id='+value+']').attr('disabled',true);
     });
   },error: function (xhr, status, errorThrown) {
          Messenger().post({
              message: 'An error has occurred please try again.',
              type: 'error',
              showCloseButton: false
          });
        }
});
}

$( "body" ).on( "click", ".time_picker", function() {
$time=$(this).attr('data-id');
$('#BookUserApp').show('slow');
});

$( "body" ).on( "click", "#BookUserApp", function() {
$id=$(this).attr('data-id');
if($time !='' & $dateclicked != ''){
$.ajax({
   type: "POST",
   url: $fullurl+"assets/app_ajax/backend/new_appointment.php",
   data: {id:$id,date:$dateclicked,time:$time},
   success: function(msg){
     $msg=$.trim(msg);

     if($msg=='ok'){
       location.reload();
     }
     else{
       Messenger().post({
           message: 'An error has occurred please try again.',
           type: 'error',
           showCloseButton: false
       });}

     }
});
}
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////pages////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$( "body" ).on( "click", "#EditPageFormBut", function(e) {

  var formid = '#EditPageForm';
  var FRMdata = $(formid).serialize(); // get form data



     $.ajax({
             type: "POST",
             url: $fullurl+"assets/app_ajax/admin/update_page.php",
             data: FRMdata, // serializes the form's elements.
             success: function(msg){
                 $message=$.trim(msg);
                 Messenger().post({
                         message: 'Page Saved.',
                         showCloseButton: false
                 });
                 setTimeout(function(){
                   window.location.replace($fullurl+'admin/pages/index.php');
                 },600);

              },error: function (xhr, status, errorThrown) {
             setTimeout(function(){alert('Error');},300);
           }
     });



});


$( "body" ).on( "click", "#NewStaffFormBut", function(e) {

  var formid = '#NewStaffForm';
  var FRMdata = $(formid).serialize(); // get form data



     $.ajax({
             type: "POST",
             url: $fullurl+"assets/app_ajax/admin/new_staff.php",
             data: FRMdata, // serializes the form's elements.
             success: function(msg){
                 $message=$.trim(msg);
                 Messenger().post({
                         message: 'Page Saved.',
                         showCloseButton: false
                 });
                 setTimeout(function(){
                   //window.location.replace($fullurl+'admin/pages/index.php');
                 },600);

              },error: function (xhr, status, errorThrown) {
             setTimeout(function(){alert('Error');},300);
           }
     });



});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////meet the team////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$( "body" ).on( "click", "#add_new_staff", function(e) {
   $("#BaseModalLContent").html($Loading);
   $('#BaseModalL').modal('show');

                    $.ajax({
                        type: "POST",
                        url: $fullurl+"assets/app_forms/admin/add_staff.php",
                        data: "id:1",
                        success: function(msg){

                           //alert(msg);
                           $("#BaseModalLContent").delay(1000)
                            .queue(function(n) {
                                $(this).html(msg);
                                n();
                            }).fadeIn("slow").queue(function(n) {
                                             $.material.init();
                                            n();
                            });
                        }
                  });
});

$( "body" ).on( "click", ".viewstaff", function(e) {
  var id = $(this).data('id');
   $("#BaseModalLContent").html($Loading);
   $('#BaseModalL').modal('show');
   //You need to get the id from the DATA ID
   //append the id to the below url (Use the W3School example)
   //Your get will work then.
                    $.ajax({
                        type: "POST",
                        url: $fullurl+"assets/app_forms/admin/edit_staff.php?id="+id,
                        data: "id:1",
                        success: function(msg){

                           //alert(msg);
                           $("#BaseModalLContent").delay(1000)
                            .queue(function(n) {
                                $(this).html(msg);
                                n();
                            }).fadeIn("slow").queue(function(n) {
                                             $.material.init();
                                            n();
                            });
                        }
                  });
});

$( "body" ).on( "click", "#DeleteStaffFormBut", function() {
            $id= $(this).attr("data-id");
            $.ajax({
                    type: "POST",
                    url: $fullurl+"assets/app_ajax/admin/delete_staff.php",
                    data: {id:$id}, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'Staff Member Deleted',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){location.reload();},1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    },error: function (xhr, status, errorThrown) {
                      Messenger().post({
                              message: 'An Error Has occured.',
                              type: 'error',
                              showCloseButton: false
                      });
                         }
            });

});


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////Member/////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$( "body" ).on( "click", ".remove_qualification", function() {$(this).parent().parent().remove();});

$( "body" ).on( "click", ".add_qualification", function() {
   $(this).parent().before('<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group"> <label class="control-label visible-sm visible-xs">Qualifications</label> <input type="text" class="form-control" name="qualifications[]" value="" required><span class="material-input"></span><input type="hidden" name="qual_id[]" value="new"> </div> <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label visible-sm visible-xs">Grade</label> <input type="text" class="form-control" name="grade[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group"> <label class="control-label visible-sm visible-xs">Date</label> <input type="date" class="form-control" name="date[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> <button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible-sm visible-xs"> <div class="clearfix"></div> <hr class="btn-info" /> </div> </div> </div>');
});


$( "body" ).on( "click", ".add_employer", function() {
 $(this).parent().before('<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">Employer Name</label> <input type="text" class="form-control" name="emp_name[]" value="" required><span class="material-input"></span><input type="hidden" name="emp_id[]" value="new"> </div> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">Job Title</label> <input type="text" class="form-control" name="emt_title[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label">Date From</label> <input type="date" class="form-control" name="emp_date_start[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label">Date To</label> <input type="date" class="form-control" name="emp_date_end[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group"> <label class="control-label">Employer Address</label> <textarea name="emp_address[]" class="form-control" rows="5" id="emp_address" required></textarea></div> <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer2 visible-md visible-lg"></div> <button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="clearfix"></div> <hr class="btn-info" /> </div> </div> </div>');
});

$( "body" ).on( "click", ".add_references", function() {
 $(this).parent().before('<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">References Name</label> <input type="text" class="form-control" name="ref_name[]" value="" required><span class="material-input"></span> <input type="hidden" name="ref_id[]" value="new"> </div> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">Contact Number</label> <input type="text" class="form-control" name="ref_contact[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label">Position Held</label> <input type="text" class="form-control" name="ref_position[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group"> <label class="control-label">Contact Address</label> <textarea name="ref_address[]" class="form-control" rows="5" id="ref_address" required></textarea></div><div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer2 visible-md visible-lg"></div> <button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="clearfix"></div> <hr class="btn-info" /> </div> </div> </div>');
});



$( "body" ).on( "click", "#edit_member_form_but", function() {
  var FRMdata = $("#edit_member_form").serialize(); // get form data
  $id=$('#member_id').val();
            $.ajax({
               type: "POST",
               url: $fullurl+"assets/app_ajax/backend/update_member.php",
               data: FRMdata,
               success: function(msg){
                      setTimeout(function(){
                        window.location.href =($fullurl+'/admin/members/member.php?id='+$id);
                      },1000);
                 }
          });
      });
