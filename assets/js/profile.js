
function 	LinkViewProvision(value, row, index) {
    return '<a href="javascript:void(0)" id="view_provision" data-id="'+row.id+'" class="text-capitalize">'+value+'</a>';
}

function 	LinkEditCompanyLearner(value, row, index) {
    return '<a href="javascript:void(0)" id="edit_company_learner" data-id="'+row.id+'" class="text-capitalize">'+value+' '+row.surname+'</a>';
}

$( "body" ).on( "click", "#view_provision", function() {



    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');

    var id = $(this).data('id');

    $.ajax({
       type: "POST",
       url: "../../../profile_tabs/app_forms/view_provision.php?id="+id,
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});

$( "body" ).on( "click", "#edit_provision", function() {
    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');

    var id = $(this).data('id');

    $.ajax({
       type: "POST",
       url: "../../../profile_tabs/app_forms/edit_provision.php?id="+id,
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});


$( "body" ).on( "click", "#edit_provision_save", function() {
    var form = $('#edit_provision_form').serialize();

    var HasError = 0;

    $('#edit_provision_form').find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
              }
            }
        });
        if (HasError == 1) {
        Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
        });
        } else {
          $.ajax({
                    type: "POST",
                    url: "../../../profile_tabs/app_ajax/save_edit_provision.php",
                    data: form,
                    success: function(msg){
                        if(msg == 'ok' || msg == ' ok'){
                            Messenger().post({
                              message: 'Provision Edited',
                              showCloseButton: false
                        });

                        setTimeout(function(){
                            window.location.reload();
                            }, 2000);


                        }else{
                            Messenger().post({
                              message: 'Please try again later',
                              type: 'error',
                              showCloseButton: false
                          });
                        }
                    }
                });
        }
});

$( "body" ).on( "click", "#add_provision", function() {
    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');
    $.ajax({
       type: "POST",
       url: "../../../profile_tabs/app_forms/new_provision.php?id="+company_id,
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});

$( "body" ).on( "click", "#save_new_provision", function() {
    var form = $('#new_provision_form').serialize();

    var HasError = 0;

    $('#new_provision_form').find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
              }
            }
        });
        if (HasError == 1) {
        Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
        });
        } else {
          $.ajax({
                    type: "POST",
                    url: "../../../profile_tabs/app_ajax/save_new_provision.php",
                    data: form,
                    success: function(msg){
                        if(msg == 'ok' || msg == ' ok'){
                            Messenger().post({
                              message: 'New Provision Created',
                              showCloseButton: false
                        });

                        setTimeout(function(){
                            window.location.reload();
                            }, 2000);


                        }else{
                            Messenger().post({
                              message: 'Please try again later',
                              type: 'error',
                              showCloseButton: false
                          });
                        }
                    }
                });
        }
});

$( "body" ).on( "click", "#add_learner", function() {
    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');

    $.ajax({
       type: "POST",
       url: "../../../profile_tabs/app_forms/company_add_learner.php?id="+company_id,
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});

$( "body" ).on( "click", "#save_new_learner_company", function() {
    var form = $('#new_learner_company_form').serialize();

    var HasError = 0;

    $('#new_learner_company_form').find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
              }
            }
        });
        if (HasError == 1) {
        Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
        });
        } else {
          $.ajax({
                    type: "POST",
                    url: "../../../profile_tabs/app_ajax/save_company_learner.php",
                    data: form,
                    success: function(msg){
                        if(msg == 'ok' || msg == ' ok'){
                            Messenger().post({
                              message: 'New Learner Created',
                              showCloseButton: false
                        });

                        setTimeout(function(){
                            window.location.reload();
                            }, 2000);


                        }else{
                            Messenger().post({
                              message: 'Please try again later',
                              type: 'error',
                              showCloseButton: false
                          });
                        }
                    }
                });
        }
});

$( "body" ).on( "click", "#edit_company_learner", function() {
    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');

    var id = $(this).data('id');

    $.ajax({
       type: "POST",
       url: "../../../profile_tabs/app_forms/edit_company_learner.php?id="+id,
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});

$( "body" ).on( "click", "#save_edit_company_learner", function() {
    var form = $('#edit_company_learner_form').serialize();

    var HasError = 0;

    $('#edit_company_learner_form').find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
              }
            }
        });
        if (HasError == 1) {
        Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
        });
        } else {
          $.ajax({
                    type: "POST",
                    url: "../../../profile_tabs/app_ajax/save_edit_company_learner.php",
                    data: form,
                    success: function(msg){
                        if(msg == 'ok' || msg == ' ok'){
                            Messenger().post({
                              message: 'Learned Updated',
                              showCloseButton: false
                        });

                        setTimeout(function(){
                            window.location.reload();
                            }, 2000);


                        }else{
                            Messenger().post({
                              message: 'Please try again later',
                              type: 'error',
                              showCloseButton: false
                          });
                        }
                    }
                });
        }
});
