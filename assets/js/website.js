var $dateclicked;
function name($input) {
  $input=$input.replace("'","")
  if (/[A-Za-z]+$/.test($input)) {return 0;}
  else {return 1;}
}

function email($input) {
  $input = $input.trim();
  var isAlphNum = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($input);
  var deciCount = $input.replace(/[^'.']/g,"").length;
  var atCount = $input.replace(/[^'@']/g,"").length;
  //if((atCount ==1) && (deciCount >=1) && (isAlphNum)){
if ((atCount ==1 && deciCount >=1 && isAlphNum) && $input.length!=0){return  0;}
  else{return 1;}
}

function password($input,$min,$max) {
  if ($input.length < $min) {return 1;}
  else if ($input.length > $max) {return 1;}
  else if ($input.search(/\d/) == -1) {  return 1;}
  else if ($input.search(/[a-z]/) == -1) {return 1;}
  else if ($input.search(/[A-Z]/) == -1) {return 1;}
  else if ($input.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {return 1;}
  else{return 0;}
}


$( "body" ).on( "click", "#Forgot_Password", function() {
  $('#form-signin').hide('slow');
  $('#form-forgot').show('slow');

});


$( "body" ).on( "click", ".form-forgot_but", function() {

  var formid = '#form-forgot';
  var $error = 0;

  if(email($('#email').val())){
    $error=1;
    $('#email').parent().addClass('has-error');
  }
if ($error == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
  });
} else {

      $(formid).hide('slow');
      $('.loader').html($Loader);
      $('.loader').show('slow');

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"assets/app_ajax/website/forgot_password.php",
              data: FRMdata,
              success: function(msg){
                setTimeout(function(){
                  $('.loader').html('<h3 class="text-center" style="display:block;">An e-mail has been sent to <span class="color1">'+$('#email').val()+'</span> if the account exists</h3>');
                },600);
             },error: function (xhr, status, errorThrown) {
                   setTimeout(function(){
                       $(formid).show('slow');
                       $('.loader').hide('slow');
                       Messenger().post({
                          message: 'An error has occurred please try again.',
                          type: 'error',
                          showCloseButton: false
                      });
                    },600);
                  }
        });
    }
});


$( "body" ).on( "click", "#Reset_Password", function() {

  var formid = '#form-reset';
  var $error = 0;

  if(password($('#new_password').val(),6,16)){
    $error=1;
    $('#new_password').parent().addClass('has-error');
  }
  if($('#new_password').val()!=$('#confirm_new_password').val()){
    $error=1;
    $('#confirm_new_password').parent().addClass('has-error');
  }
if ($error == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
  });
} else {

      $(formid).hide('slow');
      $('.loader').html($Loader);
      $('.loader').show('slow');

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"assets/app_ajax/website/reset_password.php",
              data: FRMdata,
              success: function(msg){

                $message=$.trim(msg);

                if($message=='ok'){
                    Messenger().post({
                        message: 'Password has been reset',
                        type: 'info',
                        showCloseButton: false
                    });
                    setTimeout(function(){
                      window.location.replace($fullurl+'logon.php');
                    },1000);
              }else{
                setTimeout(function(){
                    $(formid).show('slow');
                    $('.loader').hide('slow');
                    Messenger().post({
                       message: 'An error has occurred please try again.',
                       type: 'error',
                       showCloseButton: false
                   });
                 },600);
              }
             },error: function (xhr, status, errorThrown) {
                   setTimeout(function(){
                       $(formid).show('slow');
                       $('.loader').hide('slow');
                       Messenger().post({
                          message: 'An error has occurred please try again.',
                          type: 'error',
                          showCloseButton: false
                      });
                    },600);
                  }
        });
    }
});


$( "body" ).on( "click", ".logon_button", function() {
  $('#Notifications_box').show();
  var url = $fullurl+"assets/app_php/logon.php"; // PHP save page
  $.ajax({
      type: "POST",
      url: url,
      success: function(msg){
        //alert(msg);
        $("#Notifications_box").delay(300)
         .queue(function(n) {
             $(this).html(msg);
             n();
         }).fadeIn("slow").queue(function(n) {
                  $.material.init();
                 n();
         });
       }
  });
});

$( "body" ).on( "click", "#menu-mobile_link", function() {
        $('#mobile_menu').css("right", "0px");
});

$( "body" ).on( "click", "#close-menu-mobile_link", function() {
        $('#mobile_menu').css("right", "-19.625em");
});
//Sign_Up

$( "body" ).on( "click", "#Sign_Up", function() {
  $('#Sign_Up').prop('disabled', true);
  setTimeout(function(){$('#Sign_Up').prop('disabled', false);},1000);
  $error=0;
  $('.has-error').removeClass('has-error');

if(name($('#first_name').val())){
  $error=1;
  $('#first_name').parent().addClass('has-error');
}
if(name($('#surname').val())){
  $error=1;
  $('#surname').parent().addClass('has-error');
}
if(email($('#email').val())){
  $error=1;
  $('#email').parent().addClass('has-error');
}
if(password($('#new_password').val(),6,16)){
  $error=1;
  $('#new_password').parent().addClass('has-error');
}
if($('#new_password').val()!=$('#confirm_new_password').val()){
  $error=1;
  $('#confirm_new_password').parent().addClass('has-error');
}


if($error==1){}
else{
  var FRMdata = $('#registration-form').serialize(); // get form data
    $.ajax({
          type: "POST",
          url: $fullurl+"assets/app_ajax/website/register.php",
          data: FRMdata,
          success: function(msg){
            setTimeout(function(){
              $message=$.trim(msg);
              if($message=='email already exists'){
                alert('email already exists');
              }else if($message=='id already exists'){
                alert('id already exists');
              }else{
                $email=$('#email').val();
                $password=$('#new_password').val();
                $key=$('#key').val();

                $.ajax({
                      type: "POST",
                      url: $fullurl+"assets/app_ajax/website/auth.php?action=logon",
                      data: {login:$email,password:$password,key:$key},
                      success: function(msg){
                          $message=$.trim(msg);
                          if($message=='acount'){
                            alert('acount');
                          }else if($message=='locked out'){
                            alert('locked out');
                          }else if($message=='fail'){
                            alert('fail');
                          }else{
                            location.reload();
                          }
                     },error: function (xhr, status, errorThrown) {
                            setTimeout(function(){alert('Error');},300);
                          }
                });
              }
            },600);
         },error: function (xhr, status, errorThrown) {
                setTimeout(function(){alert('Failed Registration, Try again');},300);
              }
    });
}
  });

  $( "body" ).on( "click", ".next_registration", function() {
          $step=$(this).attr('data-id'); //gets step
          $step1='.step'+$step; //class of this step
          $step= 1+parseInt($step);//add 1
          $step2='.step'+$step;//class of next step

          if($(this).attr('data-id')=='1'){

            var formid = '#registration-form';
          	var HasError = 0;

          	$(formid).find('input').each(function(){
          		$(this).parent().removeClass('has-error');
          		Messenger().hideAll();

          		if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                       if(!$(this).prop('required')){
          		    } else {
          			    HasError = 1;
          			    $(this).parent().addClass('has-error');
          		    }
             		}
          	});
            if (HasError == 1) {
          		Messenger().post({
          			  message: 'Please make sure all required elements of the form are filled out.',
          			  type: 'error',
          			  showCloseButton: false
          		});
          	} else {
                  $('.step1').hide('slow'); // hide this step
                  $('.step2').show('slow'); // show next step
                }
          }else if($(this).attr('data-id')=='2'){
            $('.has-error').removeClass('has-error');

            var formid = '#registration-form';
            var HasError = 0;

            $(formid).find('input').each(function(){
              $(this).parent().removeClass('has-error');
              Messenger().hideAll();

              if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                       if(!$(this).prop('required')){
                  } else {
                    HasError = 1;
                    $(this).parent().addClass('has-error');
                  }
                }
            });
            $(formid).find('select').each(function(){
              $(this).parent().removeClass('has-error');
              Messenger().hideAll();

              if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                       if(!$(this).prop('required')){
                  } else {
                    HasError = 1;
                    $(this).parent().addClass('has-error');
                  }
                }
            });
            //check that one of Temporary or Permanent is checked
            if(!$('.jobtype').is(':checked')){
              HasError = 1;
              $('.jobtype').parent().parent().addClass('has-error');
            }
            //check that if cv then Something has been uploaded
            if($('#infotype').val()=='cv' && $('#cv_upload').val()==''){
                  $('#cv_upload').parent().addClass('has-error');
                }
            ///check that if conviction is yes then information has been entered
            if($('#conviction').val()=='yes' && $('#conviction_info').val()==''){
                  $('#conviction_info').parent().addClass('has-error');
                }


            if (HasError == 1) {
              Messenger().post({
                  message: 'Please make sure all required elements of the form are filled out.',
                  type: 'error',
                  showCloseButton: false
              });
            } else {

              ///save form

                  $($step1).hide('slow'); // hide this step
                  $($step2).show('slow'); // show next step
                }
            }else if($(this).attr('data-id')=='3'){
              $('.step3').hide('slow'); // hide this step
              $('.step4').show('slow'); // show next step
              $('.step4').html($Loader); // show next step
            }else{
            $($step1).hide('slow'); // hide this step
            $($step2).show('slow'); // show next step
          }

  });



  $( "body" ).on( "click", "#save_registration", function() {
    $('.has-error').removeClass('has-error');

    var formid = '#registration-form';
    var HasError = 0;

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    $(formid).find('textarea').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });


    //check that the confirm infomation box is checked
    if(!$('.confirm').is(':checked')){
      HasError = 1;
      $('.confirm').parent().parent().addClass('has-error');
    }



    if (HasError == 1) {
      Messenger().post({
          message: 'Please make sure all required elements of the form are filled out.',
          type: 'error',
          showCloseButton: false
      });
    } else {

      ///save form
          $('.step2').hide('slow'); // hide this step
          $('.step3').hide('slow'); // show next step
          $('.step4').show('slow'); // show next step
          $('.step4').html($Loader); // show next step

          var FRMdata = $(formid).serialize(); // get form data
            $.ajax({
                  type: "POST",
                  url: $fullurl+"assets/app_ajax/website/register2.php",
                  data: FRMdata,
                  success: function(msg){
                    setTimeout(function(){
                      window.location.replace($fullurl+'jobs.php');
                    },600);
                 },error: function (xhr, status, errorThrown) {
                        $('.step4').hide('slow'); // hide this step
                        $('.step1').show('slow'); // show next step
                        Messenger().post({
                            message: 'An error has occurred please try again.',
                            type: 'error',
                            showCloseButton: false
                        });
                      }
            });
        }
  });



  $( "body" ).on( "change", "#infotype", function() {
          $s=$(this).val();
          if($s=='cv'){
            //$('.cv_upload_lable').show('slow');
            $('.cv_upload').show('slow');
          }
          else{
            //$('.cv_upload_lable').hide('slow');
            $('.cv_upload').hide('slow');
          }
  });



  $( "body" ).on( "click", "#cv_upload_but", function() {
  							$("#BaseModalLContent").html($Loader);
  						  $('#BaseModalL').modal('show');
  						$.ajax({
  							 type: "POST",
  							 url: $fullurl+"assets/website/app_form/uploads/cv.php",
  							 success: function(msg){
  								 $("#BaseModalLContent").delay(1000)
  									.queue(function(n) {
  											$(this).html(msg);
  											n();
  									}).fadeIn("slow").queue(function(n) {
  															 $.material.init();
  															n();
  									});
  								 }
  					});
  			});

        $( "body" ).on( "click", "#dbs_upload_but", function() {
        							$("#BaseModalLContent").html($Loader);
        						  $('#BaseModalL').modal('show');
        						$.ajax({
        							 type: "POST",
        							 url: $fullurl+"assets/website/app_form/uploads/dbs.php",
        							 success: function(msg){
        								 $("#BaseModalLContent").delay(1000)
        									.queue(function(n) {
        											$(this).html(msg);
        											n();
        									}).fadeIn("slow").queue(function(n) {
        															 $.material.init();
        															n();
        									});
        								 }
        					});
        			});



 $( "body" ).on( "click", ".remove_qualification", function() {$(this).parent().parent().remove();});

$( "body" ).on( "click", ".add_qualification", function() {
    $(this).parent().before('<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group"> <label class="control-label visible-sm visible-xs">Qualifications</label> <input type="text" class="form-control" name="qualifications[]" value="" required><span class="material-input"></span><input type="hidden" name="qual_id[]" value="new"> </div> <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label visible-sm visible-xs">Grade</label> <input type="text" class="form-control" name="grade[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group"> <label class="control-label visible-sm visible-xs">Date</label> <input type="date" class="form-control" name="date[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> <button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible-sm visible-xs"> <div class="clearfix"></div> <hr class="btn-default" /> </div> </div> </div>');
});


$( "body" ).on( "click", ".add_employer", function() {
  $(this).parent().before('<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">Employer Name</label> <input type="text" class="form-control" name="emp_name[]" value="" required><span class="material-input"></span><input type="hidden" name="emp_id[]" value="new"> </div> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">Job Title</label> <input type="text" class="form-control" name="emt_title[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label">Date From</label> <input type="date" class="form-control" name="emp_date_start[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label">Date To</label> <input type="date" class="form-control" name="emp_date_end[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group"> <label class="control-label">Employer Address</label> <textarea name="emp_address[]" class="form-control" rows="5" id="emp_address" required></textarea></div> <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer2 visible-md visible-lg"></div> <button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="clearfix"></div> <hr class="btn-default" /> </div> </div> </div>');
});

$( "body" ).on( "click", ".add_references", function() {
  $(this).parent().before('<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">References Name</label> <input type="text" class="form-control" name="ref_name[]" value="" required><span class="material-input"></span> <input type="hidden" name="ref_id[]" value="new"> </div> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group"> <label class="control-label">Contact Number</label> <input type="text" class="form-control" name="ref_contact[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12 form-group"> <label class="control-label">Position Held</label> <input type="text" class="form-control" name="ref_position[]" value="" required><span class="material-input"></span> </div> <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group"> <label class="control-label">Contact Address</label> <textarea name="ref_address[]" class="form-control" rows="5" id="ref_address" required></textarea></div><div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer visible-md visible-lg"></div> <div class="page_spacer2 visible-md visible-lg"></div> <button type="button" class="btn btn-raised btn-danger pull-right remove_qualification" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="clearfix"></div> <hr class="btn-default" /> </div> </div> </div>');
});

 $( "body" ).on( "change", "#conviction", function() {
   $s=$(this).val();
   if($s=='yes'){
     $('#conviction_info').parent().show('slow');
   }
   else{
     $('#conviction_info').parent().hide('slow');
   }
 });


 $( "body" ).on( "click", ".job_apply", function() {
              $id=$(this).attr('data-id');
               $("#BaseModalLContent").html($Loader);
               $('#BaseModalL').modal('show');


             $.ajax({
                type: "POST",
                url: $fullurl+"assets/website/app_form/jobs/apply.php",
                data: {id:$id},
                success: function(msg){
                  $("#BaseModalLContent").delay(1000)
                   .queue(function(n) {
                       $(this).html(msg);
                       n();
                   }).fadeIn("slow").queue(function(n) {
                                $.material.init();
                               n();
                   });
                  }
           });
       });


$( "body" ).on( "click", "#job_apply", function() {
             $id=$(this).attr('data-id');
              $("#BaseModalLContent").html($Loader);

            $.ajax({
               type: "POST",
               url: $fullurl+"assets/website/app_ajax/new_job_apply.php",
               data: {id:$id},
               success: function(msg){
                 $message=$.trim(msg);
                 if($message=='application'){
                   //apply
                   $('#BaseModalL').modal('hide');
                   setTimeout(function(){
                     window.location.href =($fullurl+'jobs.php');
                   },1000);
                 }else{
                   $.ajax({
                      type: "POST",
                      url: $fullurl+"assets/website/app_form/jobs/appointment.php",
                      data: {id:$id},
                      success: function(msg){
                        $("#BaseModalLContent").delay(600)
                         .queue(function(n) {
                             $(this).html(msg);
                             n();
                         }).fadeIn("slow").queue(function(n) {
                                      $.material.init();
                                     n();

                                     $('#calendar').fullCalendar({
                                        header: {
                                          left: 'prev,next today',
                                          center: 'title',
                                          right: 'month'
                                        },
                                        dayClick: function(date, jsEvent, view) {
                                          $dateclicked=date.format();
                                          var $weekday = date.weekday();
                                          var $today=moment($('#calendar').fullCalendar('getDate')).format('YYYY-MM-DD');
                                          if($dateclicked <= $today){
                                            // Previous Date
                                            $('.timebox').hide('slow');
                                            $('.time_picker').attr('disabled',true);
                                            $('#BookApp').hide('slow');
                                          }
                                          else{// Future Date
                                            if($weekday!='5' & $weekday!='6'){// Weekday
                                              BookingApp($dateclicked); // Get appointments slots
                                              $('.timebox').show('slow');
                                              $('.time_picker').attr('disabled',true);
                                              $('#BookApp').hide('slow');
                                            }else{
                                              $('.timebox').hide('slow');
                                              $('.time_picker').attr('disabled',true);
                                              $('#BookApp').hide('slow');
                                            }//Weekend
                                          }
                                        },
                                        slotEventOverlap: false,
                                        //height: 582,
                                        firstDay: 1,
                                        editable: false,
                                        eventLimit: true, // allow "more" link when too many events
                                        timezone: 'Europe/London',
                                      });
                         });
                        }
                 });
                 }

                 },error: function (xhr, status, errorThrown) {
                        Messenger().post({
                            message: 'An error has occurred please try again.',
                            type: 'error',
                            showCloseButton: false
                        });
                      }
          });
      });


$( "body" ).on( "click", ".time_picker", function() {
$time=$(this).attr('data-id');

$('#BookApp').show('slow');

});

$( "body" ).on( "click", "#BookApp", function() {

if($time !='' & $dateclicked != ''){
    $.ajax({
       type: "POST",
       url: $fullurl+"assets/website/app_ajax/new_appointment.php",
       data: {id:$id,date:$dateclicked,time:$time},
       success: function(msg){
         $msg=$.trim(msg);

         if($msg=='ok'){
           location.reload();
         }
         else{
           Messenger().post({
               message: 'An error has occurred please try again.',
               type: 'error',
               showCloseButton: false
           });}

         }
    });
}
});

$( "body" ).on( "click", "#BookApp", function() {

if($time !='' & $dateclicked != ''){
    $.ajax({
       type: "POST",
       url: $fullurl+"assets/website/app_ajax/new_appointment_user.php",
       data: {id:$id,date:$dateclicked,time:$time},
       success: function(msg){
         $msg=$.trim(msg);

         if($msg=='ok'){
           location.reload();
         }
         else{
           Messenger().post({
               message: 'An error has occurred please try again.',
               type: 'error',
               showCloseButton: false
           });}

         }
    });
}
});


    function BookingApp($date){
        $.ajax({
           type: "POST",
           url: $fullurl+"assets/website/app_ajax/check_appointment_availability.php",
           data: {date:$date},
           // dataType: "json",
           success: function(msg){
             $('.time_picker').attr('disabled',false);
              $slots=$.trim(msg);
              $slots=$.parseJSON($slots);

             $.each($slots, function( index, value ) {
               console.log(index + ": " + value);

              $('.time_picker[data-id='+value+']').attr('disabled',true);
             });
           },error: function (xhr, status, errorThrown) {
                  Messenger().post({
                      message: 'An error has occurred please try again.',
                      type: 'error',
                      showCloseButton: false
                  });
                }
      });
    }

    $( "body" ).on( "click", "#edit_profile_form_but", function() {
      var FRMdata = $("#edit_profile_form").serialize(); // get form data
                $.ajax({
                   type: "POST",
                   url: $fullurl+"assets/app_ajax/website/update_user.php",
                   data: FRMdata,
                   success: function(msg){
                          setTimeout(function(){
                            window.location.href =($fullurl+'profile.php');
                          },1000);
                     }
              });
          });





          $( "body" ).on( "click", "#register_cv_home", function() {

          							$("#BaseModalLContent").html($Loader);
          						  $('#BaseModalL').modal('show');
          						$.ajax({
          							 type: "POST",
          							 url: $fullurl+"assets/website/app_form/uploads/register_cv.php",
          							 success: function(msg){
          								 $("#BaseModalLContent").delay(1000)
          									.queue(function(n) {
          											$(this).html(msg);
          											n();
          									}).fadeIn("slow").queue(function(n) {
          															 $.material.init();
          															n();
          									});
          								 }
          					});
          			});


                $( "body" ).on( "click", "#register_cv_confirm", function() {
                  console.log('here');
                  var formid = '#register_cv_form';
                  var HasError = 0;

                  $(formid).find('input').each(function(){
                    $(this).parent().removeClass('has-error');
                    Messenger().hideAll();

                    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                             if(!$(this).prop('required')){
                        } else {
                          HasError = 1;
                          $(this).parent().addClass('has-error');
                        }
                      }
                  });
                  if (HasError == 1) {
                    Messenger().post({
                        message: 'Please make sure all required elements of the form are filled out.',
                        type: 'error',
                        showCloseButton: false
                    });
                  } else {
                    var FRMdata = $(formid).serialize(); // get form data
                      $.ajax({
                            type: "POST",
                            url: $fullurl+"assets/website/app_ajax/send_cv.php",
                            data: FRMdata,
                            success: function(msg){
                              setTimeout(function(){
                                console.log('here1');
                                $('#BaseModalLContent').html('<h3 class="text-center" style="display:block;">You CV has been submitted to Sky Recruitment.</h3>');
                              },600);
                           },error: function (xhr, status, errorThrown) {
                                 setTimeout(function(){
                                     $(formid).show('slow');
                                     $('.loader').hide('slow');
                                     Messenger().post({
                                        message: 'An error has occurred please try again.',
                                        type: 'error',
                                        showCloseButton: false
                                    });
                                  },600);
                                }
                      });
                  }
                });
