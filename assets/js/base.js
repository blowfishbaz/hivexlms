 $.material.init();

$( document ).ready(function() {
    $(".select").dropdown({"optionClass": "withripple"});

    $('#addon3a').keyup(function(e){
  if (e.which == 13) {
    var string = $(this).val();
    var stringTrim = $.trim(string);

    var HasError = 0;

    if(stringTrim == ""){
      HasError = 1;
      $(this).parent().addClass('has-error');
    }

    if(HasError == 0){ //Do something
      $('.searchResults').html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader"  /><br>Loading...</div>');
      var url = "../../../assets/app_forms/returnSearch.php"; // PHP save page
      $.ajax({
          type: "POST",
          data : {string:string},
          url: url,
          success: function(msg){
            //alert(msg);
            $(".searchResults").delay(1000)
             .queue(function(n) {
                 $(this).html(msg);
                 n();
             }).fadeIn("slow").queue(function(n) {
                      //$.material.init();
                     n();
             });
           }
      });
    }
  }
});
});

  $(function () {
  $('[data-toggle="popover"]').popover()
})


$(".menu-toggle-new").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
      $("#logo").toggleClass("toggled");
       $("#menu-toggle").toggleClass("toggled");
       $("#menu-selectbottom").toggleClass("toggled");
       $("#menu-selecttop").toggleClass("toggled");
       $(".hivex-footer").toggleClass("toggled");

       $(window).trigger('resize');
  });


	$(".menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#logo").toggleClass("toggled");
         $("#menu-toggle").toggleClass("toggled");
         $("#menu-selectbottom").toggleClass("toggled");
         $("#menu-selecttop").toggleClass("toggled");


    });

/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////
$menutype = $('.menutype').attr("data-id");

  switch($menutype) {
      case 'RST':
          $('#crmmenu').hide();
          $( "#rstmenu" ).show();

          break;
      case 'CRM':
          $('#crmmenu').show();
          $( "#rstmenu" ).hide();

      break;
      default:


  }
/////////////////// menu logic /////////////////////
/////////////////// menu logic /////////////////////

//Show Main Menu
    $(".selectTOPmenu").click(function(e) {
        e.preventDefault();

	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
				$( "#topmenu" ).fadeIn( "fast" );
				$("#borderleft").css({"border-left": "3px #009688 solid"});//000000
	  });

});


//Show CRM Menu
    $("#show_CRM_menu").click(function(e) {
        e.preventDefault();

	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
				$( "#crmmenu" ).fadeIn( "fast" );

				$("#borderleft").css({"border-left": "3px #009688 solid"});

	  });

});


//Show Products Menu
  $("#show_PRODUCTS_menu").click(function(e) {
          e.preventDefault();
  	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
  				$( "#productsmenu" ).fadeIn( "fast" );
  				$("#borderleft").css({"border-left": "3px #009688 solid"});//5070ff
  	  });
  });

  $("#show_SALES_menu").click(function(e) {
          e.preventDefault();
  	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
  				$( "#salessmenu" ).fadeIn( "fast" );
  				$("#borderleft").css({"border-left": "3px #009688 solid"});//5070ff
  	  });
  });

  $("#show_MARKETING_menu").click(function(e) {
          e.preventDefault();
  	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
  				$( "#marketingmenu" ).fadeIn( "fast" );
  				$("#borderleft").css({"border-left": "3px #009688 solid"});//5070ff
  	  });
  });

  $("#show_HR_menu").click(function(e) {
          e.preventDefault();
  	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
  				$( "#hrmenu" ).fadeIn( "fast" );
  				$("#borderleft").css({"border-left": "3px #009688 solid"});//5070ff
  	  });
  });
  $("#show_REPORTS_menu").click(function(e) {
          e.preventDefault();
  	  $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
  				$( "#reportsmenu" ).fadeIn( "fast" );
  				$("#borderleft").css({"border-left": "3px #009688 solid"});//5070ff
  	  });
  });

    //Show rst Menu
    $("#show_RST_menu").click(function(e) {
          e.preventDefault();
          $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
                  $( "#rstmenu" ).fadeIn( "fast" );
                  $("#borderleft").css({"border-left": "3px #009688 solid"});//8cc053
          });
    });


    $("#show_UMAN_menu").click(function(e) {
          e.preventDefault();
          $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
                  $( "#umanmenu" ).fadeIn( "fast" );
                  $("#borderleft").css({"border-left": "3px #009688 solid"});//8cc053
          });
    });

    $("#show_DMAN_menu").click(function(e) {
          e.preventDefault();
          $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
                  $( "#dmanmenu" ).fadeIn( "fast" );
                  $("#borderleft").css({"border-left": "3px #009688 solid"});//8cc053
          });
    });


    $("#show_GROUPS_menu").click(function(e) {
          e.preventDefault();
          $( ".leftmenus" ).fadeOut( "fast").promise().done(function() {
                  $( "#groupsmenu" ).fadeIn( "fast" );
                  $("#borderleft").css({"border-left": "3px #009688 solid"});//8cc053
          });
    });



$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('#searchmenu').tooltip();
  $('#profilemenu').tooltip();
  $('#addTime').tooltip();
  $('#addExpense').tooltip();
  $('#addDisbursement').tooltip();
})


  //Show rst Hold job

$( "body" ).on( "click", ".job-action", function() {
  $('#Holdjob_modal').modal('show');


          $('#Holdjob_modal').modal('show');


        $.ajax({
           type: "POST",
           url: "../../../assets/app_forms/rst/pauesjob/Form.php?id="+$id+"",
           data: {id:$id},
           success: function(msg){
             //alert(msg);
             $("#HoldjobContent").delay(1000)
              .queue(function(n) {
                  $(this).html(msg);
                  n();
              }).fadeIn("slow").queue(function(n) {
                           $.material.init();
                           $(".chosen-select").chosen();
                          n();
              });
             }
      });

  });


  $( "body" ).on( "click", ".viewnotes", function() {

    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

      $.ajax({
       type: "POST",
       url: "../../../../assets/app_forms/jobnote/Form.php?id="+$jobid+"",
       data: {id:1},
       success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
      });

  });

  $( "body" ).on( "click", "#add_tasks2", function() {
    $thisjobid=$(this).attr("data-id");
    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

    $.ajax({
        type: "POST",
        url: "../../../assets/app_ajax/todo/addtodomodal.php?job_id="+$thisjobid,
        success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                    $.material.init();
                   n();

                   $('#itm_desc').summernote({
                     height: 300,                 // set editor height
                     minHeight: null,             // set minimum height of editor
                     maxHeight: null,             // set maximum height of editor
                     focus: true,                  // set focus to editable area after initializing summernote
                     placeholder:'Description...',
                     toolbar: [
                       ['font', ['bold', 'italic', 'underline']],
                       ['color', ['color']],
                       ['para', ['ul', 'ol', 'paragraph']],
                       ['table', ['table']],
                       ['insert', ['link', 'hr']],
                       ['help', ['help']]
                       ]
                   });
                   $(".modal-search").chosen({disable_search_threshold: 10});
               $('#newDueDate').bootstrapMaterialDatePicker({ weekStart : 1, time: true, format : 'DD-MM-YYYY HH:mm', minDate: new Date()});
           });
         }
    });
  });

  $( "body" ).on( "click", ".PauseJob", function() {

    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

      $.ajax({
       type: "POST",
       url: "../../../../assets/app_forms/pausejob/Form.php?id="+$jobid+"",
       data: {id:1},
       success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
      });
  });

  $( "body" ).on( "click", ".PauseJob2", function() {
    $thisjobid=$(this).attr("data-id");
    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

      $.ajax({
       type: "POST",
       url: "../../../../assets/app_forms/pausejob/Form.php?id="+$thisjobid+"",
       data: {id:1},
       success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
      });
  });

  $( "body" ).on( "click", ".UnpauseJob", function() {
    $thisjobid=$(this).attr("data-id");
    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

      $.ajax({
       type: "POST",
       url: "../../../../assets/app_forms/unpausejob/Form.php?id="+$thisjobid+"",
       data: {id:1},
       success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
      });
  });

  $( "body" ).on( "click", ".unpause_job", function() {

    var formid = '#UnPauseJobForm';
    var formdata = $(formid).serialize();
    var HasError = 0;
$(formid).find('textarea').each(function(){
  $(this).parent().removeClass('has-error');
  Messenger().hideAll();

  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){}
           else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
});

if (HasError == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
  });
}
else {
          $.ajax({
                type: "POST",
                url: "../../../assets/app_ajax/alljobs/unpause.php",
                data: formdata,
                success: function(msg){
                  $('#BaseModalL').modal('hide');
                  reloadtab();
               }
          });
        }

  });

  $( "body" ).on( "click", ".killJob", function() {
    $thisjobid=$(this).attr("data-id");
    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

      $.ajax({
       type: "POST",
       url: "../../../../assets/app_forms/killjob/Form.php?id="+$thisjobid+"",
       data: {id:1},
       success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
      });
  });
  $( "body" ).on( "click", ".kill_job", function() {

    var formid = '#KillJobForm';
    var formdata = $(formid).serialize();
    var HasError = 0;
    $(formid).find('select').each(function(){

      $(this).siblings().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){}
               else {
            HasError = 1;
            $(this).siblings().addClass('has-error');
          }
        }
    });

$(formid).find('textarea').each(function(){
  $(this).parent().removeClass('has-error');
  Messenger().hideAll();

  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){}
           else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
});

if (HasError == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
  });
}
else {
          $.ajax({
                type: "POST",
                url: "../../../assets/app_ajax/alljobs/kill.php",
                data: formdata,
                success: function(msg){
                  $('#BaseModalL').modal('hide');
                  reloadtab();
               }
          });
        }

  });

  $( "body" ).on( "click", ".unkillJob", function() {
    $thisjobid=$(this).attr("data-id");
    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');

      $.ajax({
       type: "POST",
       url: "../../../../assets/app_forms/unkilljob/Form.php?id="+$thisjobid+"",
       data: {id:1},
       success: function(msg){
         //alert(msg);
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
      });
  });

  $( "body" ).on( "click", ".unkill_job", function() {

    var formid = '#UnKillJobForm';
    var formdata = $(formid).serialize();
    var HasError = 0;
$(formid).find('textarea').each(function(){
  $(this).parent().removeClass('has-error');
  Messenger().hideAll();

  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){}
           else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
});

if (HasError == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
  });
}
else {
          $.ajax({
                type: "POST",
                url: "../../../assets/app_ajax/alljobs/unkill.php",
                data: formdata,
                success: function(msg){
                  $('#BaseModalL').modal('hide');
                  reloadtab();
               }
          });
        }

  });

  $( "body" ).on( "click", ".pause_job", function() {

    var formid = '#PauseJobForm';
    var formdata = $(formid).serialize();
    var HasError = 0;
$(formid).find('select').each(function(){

  $(this).siblings().removeClass('has-error');
  Messenger().hideAll();

  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){}
           else {
        HasError = 1;
        $(this).siblings().addClass('has-error');
      }
    }
});

$(formid).find('textarea').each(function(){

  $(this).parent().removeClass('has-error');
  Messenger().hideAll();

  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){}
           else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
});

if (HasError == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false

  });
}
else {
          $.ajax({
                type: "POST",
                url: "../../../assets/app_ajax/alljobs/pause.php",
                data: formdata,
                success: function(msg){
                  window.location = "index.php";
               }
          });
        }

  });

  $( "body" ).on( "click", ".del-note", function() {
    $("#BaseModalLContent").html($Loading);
    $('#BaseModalL').modal('show');
            $note_id=$(this).attr("data-id");

      $.ajax({
        type: "POST",
        url: "../../../assets/app_forms/jobnote/delNoteForm.php",
        data: {id:$note_id},
        success: function(msg){
          $("#BaseModalLContent").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
           });


        }
   });
  });

  $( "body" ).on( "click", "#del_note", function() {
    $note_id=$(this).attr("data-id");
            $.ajax({
                  type: "POST",
                  url: "../../../assets/app_forms/jobnote/delnote.php",
                  data: {id:$note_id},
                  success: function(msg){
                    location.reload();
                 }
            });
});

$( "body" ).on( "click", ".addnewnote", function() {
  $("#BaseModalLContent").html($Loading);
  $('#BaseModalL').modal('show');

   $.ajax({
       type: "POST",
       url: "../../../assets/app_forms/jobnote/addnew.php?id="+$jobid+"",
       data: {id:1},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
  });
});

$( "body" ).on( "click", ".submitnewnote", function() {
var formid = '#NewJobNoteForm';
var HasError = 0;

$(formid).find('textarea').each(function(){
  $(this).parent().removeClass('has-error');
  Messenger().hideAll();

  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){}
           else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
});

if (HasError == 1) {
  Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false

  });
}
else {

  $note= $('#notes').val();

          $.ajax({
                type: "POST",
                url: "../../../assets/app_ajax/jobnotes/newnote.php?id="+$jobid,
                data: {note:$note},
                success: function(msg){

                  $("#BaseModalLContent").html($Loading);

                   $('#BaseModalLContent').html('<h3>Would you like to arrange a new call back?</h3><button type="button" class="btn btn-success btn-raised pull-left" id="add_tasks">Yes</button><button type="button" class="btn btn-danger btn-raised pull-right nocallback">No</button><div class="clearfix"></div>').delay(1000);

               }
          });
}
});

$( "body" ).on( "click", ".nocallback", function() {
  $("#BaseModalLContent").html($Loading);
  $('#BaseModalL').modal('show');

    $.ajax({
     type: "POST",
     url: "../../../../assets/app_forms/jobnote/Form.php?id="+$jobid+"",
     data: {id:1},
     success: function(msg){
       //alert(msg);
       $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
       }
    });
});

$( "body" ).on( "click", "#add_tasks", function() {
  $("#BaseModalLContent").html($Loading);
  $('#BaseModalL').modal('show');

  $.ajax({
      type: "POST",
      url: "../../../assets/app_ajax/todo/addtodomodal.php?job_id="+$jobid,
      success: function(msg){
        //alert(msg);
        $("#BaseModalLContent").delay(1000)
         .queue(function(n) {
             $(this).html(msg);
             n();
         }).fadeIn("slow").queue(function(n) {
                  $.material.init();
                 n();

                 $('#itm_desc').summernote({
                   height: 300,                 // set editor height
                   minHeight: null,             // set minimum height of editor
                   maxHeight: null,             // set maximum height of editor
                   focus: true,                  // set focus to editable area after initializing summernote
                   placeholder:'Description...',
                   toolbar: [
                     ['font', ['bold', 'italic', 'underline']],
                     ['color', ['color']],
                     ['para', ['ul', 'ol', 'paragraph']],
                     ['table', ['table']],
                     ['insert', ['link', 'hr']],
                     ['help', ['help']]
                     ]
                 });
                 $(".modal-search").chosen({disable_search_threshold: 10});
             $('#newDueDate').bootstrapMaterialDatePicker({ weekStart : 1, time: true, format : 'DD-MM-YYYY HH:mm', minDate: new Date()});
         });
       }
  });
});

$( "body" ).on( "click", "#newAddTodo", function() {
 var job_id = $('#add_todo_project_id').val();
    var form = $('#addForm').serialize();
    //console.log(form);
    var HasError = 0;
    //console.log(form);
    $('#addForm').find('input').each(function() {
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if (!$.trim(this.value).length) { // zero-length string AFTER a trim
        if (!$(this).prop('required')) {
        } else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
    });
    $('#addForm').find('select').each(function() {
      $(this).parent().children('label').removeClass('has-error');
      Messenger().hideAll();
      if (!$.trim(this.value).length) { // zero-length string AFTER a trim
        if (!$(this).prop('required')) {
        } else {
          HasError = 1;
          $(this).parent().children('label').addClass('has-error');
        }
      }
    });
    if (HasError == 1) {
      Messenger().post({
        message: 'Please make sure all required elements of the form are filled out.',
        type: 'error',
        showCloseButton: false
      });
    } else {
        $('.modal').delay(200).queue(function(n) {
          $('.modal').modal('hide');
          n();
        });
        var url = "../../../assets/app_ajax/todo/newtodo.php"; // PHP save page
        $.ajax({
          type: "POST",
          url: url,
          data: form, // serializes the form's elements.
          success: function(data) {
            if (data == 'ok' || data == " ok") {
              //appRed(s[1])
              Messenger().post({
                message: 'A new todo has been created.',
                type: 'info'
              });
              setTimeout(function(){
                window.location.reload();
                  }, 2000);
            } else {
              Messenger().post({
                message: 'An Error Has occured.',
                type: 'error',
                showCloseButton: false
              });
            }
          }
        });
    }
});

$( "body" ).on( "change", "#task_type", function() {
 var $subject=$(this).val();
 var date = new Date();
 switch ($subject) {
    case 'Fee Proposal – Collate':
    case 'Proof Read Document':
    case 'Peer Review Document':
    $('#priority').val('3');
    $('#priority').change();
    $('#priority').trigger('chosen:updated');
    break;

    case 'Leisure maps':
    case 'Competitors analysis':
    case 'Financial Modelling':
    $('#priority').val('2');
    $('#priority').change();
    $('#priority').trigger('chosen:updated');
    break;

    case 'Call Client':
    case 'Set up Inception Meeting':
    case 'Commission Third Parties':
    case 'Research':
    case 'Client Call':
    case 'Mosaic Maps':
    case 'MOSAIC drivetimes':
    case 'Traffic Flow Maps':
    case 'VOA postcodes':
    case 'VOA':
    $('#priority').val('1');
    $('#priority').change();
    $('#priority').trigger('chosen:updated');
    break;
default:
   }

   if($subject == 'Other'){
       $('.other_subject_holder').show();
       $('#subject').attr('required');
   }else{
       $('.other_subject_holder').hide();
       $('#subject').removeAttr(attributeName)('required');
   }
});

  $( "body" ).on( "click", "#top_menu_reminders", function() {
    $('#Notifications_box').show();
    var url = "../../../assets/app_ajax/notifications.php"; // PHP save page
    $.ajax({
        type: "POST",
        url: url,
        success: function(msg){
          //alert(msg);
          $("#Notifications_box").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                    $.material.init();
                   n();
           });
         }
    });
  });

  $(document).mouseup(function (e){
  var container = $("#Notifications_box");
  if (!container.is(e.target)&& container.has(e.target).length === 0){container.hide();}
});

  $( "body" ).on( "click", ".change_notifications", function() {
    $type=$(this).attr("data-id");

    $('#Notifications_box').removeClass('headline');
    $('#Notifications_box').removeClass('details');
    $('#Notifications_box').addClass($type);

    $('.notification_header_hidden').toggle();
    });



    $( "body" ).on( "click", "#notifications_checkbox", function() {
      var id = $(this).val();

      $('.modal').delay(200).queue(function(n) {
        $('.modal').modal('hide');
        n();
      });
      var url = "../../../assets/app_ajax/notifications_update.php"; // PHP save page
      $.ajax({
        type: "POST",
        url: url,
        data: {id:id}, // serializes the form's elements.
        success: function(data) {
          if (data == 'ok' || data == " ok") {
            Messenger().post({
              message: 'Task Completed.',
              type: 'info'
            });
            setTimeout(function(){
              //location.reload();
                }, 2000);
          } else {
            Messenger().post({
              message: 'An Error Has occured.',
              type: 'error',
              showCloseButton: false
            });
          }
        }
      });
    });


    $( "body" ).on( "click", "#add_priority", function() {

      $("#BaseModalLContent").html($Loading);
      $('#BaseModalL').modal('show');

        $.ajax({
         type: "POST",
         url: "../../../../assets/app_forms/priority/Form.php?id="+$jobid+"",
         data: {id:1},
         success: function(msg){
           //alert(msg);
           $("#BaseModalLContent").delay(1000)
            .queue(function(n) {
                $(this).html(msg);
                n();
            }).fadeIn("slow").queue(function(n) {
                         $.material.init();
                        n();
            });
           }
        });
    });
    $( "body" ).on( "click", ".add_priority2", function() {
        $thisjobid=$(this).attr("data-id");
      $("#BaseModalLContent").html($Loading);
      $('#BaseModalL').modal('show');

        $.ajax({
         type: "POST",
         url: "../../../../assets/app_forms/priority/Form.php?id="+$thisjobid+"",
         data: {id:1},
         success: function(msg){
           //alert(msg);
           $("#BaseModalLContent").delay(1000)
            .queue(function(n) {
                $(this).html(msg);
                n();
            }).fadeIn("slow").queue(function(n) {
                         $.material.init();
                        n();
            });
           }
        });
    });

    $( "body" ).on( "click", ".confirm_priority", function() {

      var formid = '#PauseJobForm';
      var formdata = $(formid).serialize();
      var HasError = 0;
      $(formid).find('select').each(function(){
        $(this).siblings().removeClass('has-error');
        Messenger().hideAll();

        if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                 if(!$(this).prop('required')){}
                 else {
              HasError = 1;
              $(this).siblings().addClass('has-error');
            }
          }
      });

  $(formid).find('textarea').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
             if(!$(this).prop('required')){}
             else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
  });

  if (HasError == 1) {
    Messenger().post({
        message: 'Please make sure all required elements of the form are filled out.',
        type: 'error',
        showCloseButton: false
    });
  }
  else {
            $.ajax({
                  type: "POST",
                  url: "../../../assets/app_ajax/alljobs/priority.php",
                  data: formdata,
                  success: function(msg){
                    $('#BaseModalL').modal('hide');

                 }
            });
          }

    });


    //user_info
    $( "body" ).on( "click", "#edit_own_password", function() {
        $('.user_info').hide();
        $('.edit_own_password').show();
    });

    $( "body" ).on( "click", "#saveNewOwnPassword", function() {
       $pass1=$('#current_own_password').val();
       $pass2=$('#new_own_password').val();
       $pass3=$('#confirm_new_own_password').val();

            if ($pass2==$pass3 && $.trim($pass2).length) {
              var url = "../../../assets/app_ajax/hr/checkpassword.php"; // PHP save page
              $.ajax({
                      type: "POST",
                      url: url,
                      data: {password:$pass1}, // serializes the form's elements.
                      success: function(data){
                        if($.trim(data)=='1'){
                          var formid = '#edit_own_passwordForm';
                          var FRMdata = $(formid).serialize(); // get form data
                          var url = "../../../assets/app_ajax/hr/editownpassword.php"; // PHP save page
                          $.ajax({
                                  type: "POST",
                                  url: url,
                                  data: FRMdata, // serializes the form's elements.
                                  success: function(data){
                                          if (data == 'ok'||data == ' ok') {
                                             $(".edit_own_password").html($Loading);
                                                  Messenger().post({
                                                         message: 'Password Updated.',
                                                         showCloseButton: false
                                                  });
                                                  setTimeout(function(){
                                                      location.reload();
                                                  },1000);
                                          } else {
                                          Messenger().post({
                                                  message: 'An Error Has occured.',
                                                  type: 'error',
                                                  showCloseButton: false
                                          });
                                     }
                                  }
                          });
                        }
                        else{
                          Messenger().post({
                           message: "Current Password Wrong",
                           type: 'error',
                           showCloseButton: false
                    });}
                      }
                });
            }else{
               Messenger().post({
                message: "New Passwords Don't Match",
                type: 'error',
                showCloseButton: false
         });
            }
    });

    ////////////////////////////////////////////////////////////////////////////
    /////////////////////Disable all Buttons when clicked //////////////////////
    ////////////////////////////////////////////////////////////////////////////

    // $("body").on("click",".btn-raised",function(){
    // $but=$(this);
    //
    // if($($but).hasClass( "dropdown-toggle" )||$($but).hasClass( "no_delay" )){
    // }else{
    //   $($but).attr("disabled", "disabled");
    // setTimeout(function(){
    //   $($but).removeAttr("disabled");
    // }, 2000);
    // }
    //
    // });

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////



    $( "body" ).on( "click", "#add_notification_message", function() {

      $('#Notifications_box').hide();

      $("#BaseModalLContent").html($Loading);
      $('#BaseModalL').modal('show');

        $.ajax({
         type: "POST",
         url: "../../../../assets/app_forms/add_notification.php",
         success: function(msg){
           //alert(msg);
           $("#BaseModalLContent").delay(1000)
            .queue(function(n) {
                $(this).html(msg);
                n();
            }).fadeIn("slow").queue(function(n) {
                         $.material.init();
                        n();

                        $('#notification_information').summernote({
                          height: 300,                 // set editor height
                          minHeight: null,             // set minimum height of editor
                          maxHeight: null,             // set maximum height of editor
                          focus: true,                  // set focus to editable area after initializing summernote
                          placeholder:'Description...',
                          toolbar: [
                            ['font', ['bold', 'italic', 'underline']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'hr']],
                            ['help', ['help']]
                            ]
                        });
            });
           }
        });
    });



$( "body" ).on( "click", "#save_notification", function() {
  var formid = '#add_notification_form';
  var HasError = 0;

  $(formid).find('input').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
             if(!$(this).prop('required')){
        } else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
  });
  $(formid).find('select').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
             if(!$(this).prop('required')){
        } else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
  });

  if(HasError == 0){

    var FRMdata = $(formid).serialize(); // get form data
      $.ajax({
            type: "POST",
            url: $fullurl+"assets/app_ajax/save_notification.php",
            data: FRMdata,
            success: function(msg){
              $message=$.trim(msg);
              if($message=='ok'){
                  Messenger().post({
                      message: 'Service Updated.',
                      type: 'error',
                      showCloseButton: false
                  });
                setTimeout(function(){
                  //window.location.replace($fullurl+'jobs.php');

                  //Refresh the Page
                  window.location.reload();

                },1000);
              }

           },error: function (xhr, status, errorThrown) {
                  Messenger().post({
                      message: 'An error has occurred please try again.',
                      type: 'error',
                      showCloseButton: false
                  });
                }
      });
  }
});

$( "body" ).on( "click", ".admin-menu-dropdown", function() {
  $('#rota_menu_mobile_table').toggleClass('show_table');
});
