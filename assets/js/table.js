$( document ).ready(function() {
    var $table = $('#table');
    	 var $sixty = $( document ).height();
        $sixty = ($sixty/100)*70;
         $table.bootstrapTable( 'resetView' , {height: $sixty} );

    $(function () {
        $table.on('click-row.bs.table', function (e, row, $element) {
            $('.success').removeClass('success');
            $($element).addClass('success');
        });
    });

    //  $(function () {
    //     $table.on('dbl-click-row.bs.table', function (e, row, $element) {
    //        var id = row['id'];
    //         $("#BaseModalLContent").html($Loading);
    //         $('#BaseModalL').modal('show');
    //         var url = "../../../assets/app_forms/hr/edituser.php?id="+id;
    //         $thisApp = $(this).attr("href");
    //         e.preventDefault();
    //                          $.ajax({
    //                              type: "POST",
    //                              url: url,
    //                              data: "id:1",
    //                              success: function(msg){
    //
    //                                 //alert(msg);
    //                                 $("#BaseModalLContent").delay(1000)
    //                                  .queue(function(n) {
    //                                      $(this).html(msg);
    //                                      n();
    //                                  }).fadeIn("slow").queue(function(n) {
    //                                                   $.material.init();
    //                                                  n();
    //                                  });
    //                              }
    //                        });
    //     });
    // });

  $(function () {
        $('#toolbar').find('select').change(function () {
            $table.bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
    });

    $( window ).resize(function() {
		 var $sixty = $( document ).height();
        $sixty = ($sixty/100)*70;
         $table.bootstrapTable( 'resetView' , {height: $sixty} );
	});

});

function DateTimeFormatter(value, row, index) {
  var a = new Date(value * 1000);
  var a = new Date(a - 3600000);/* daylight saving???*/
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();

  if(hour < 10){
    hour = '0'+hour;
  }

  if(min < 10){
    min = '0'+min;
  }

  if(date < 10){
    date = '0'+date;
  }

  if(year=='1970'){var time ='-'}else{
      var time =  hour + ':' + min+ ' ' + date + ' ' + month + ' ' + year;
  }
  return time;
}

function DateTimeFormatterAccounts(value, row, index) {
  var a = new Date(value * 1000);
  var a = new Date(a - 3600000);/* daylight saving???*/
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();

  if(hour < 10){
    hour = '0'+hour;
  }

  if(min < 10){
    min = '0'+min;
  }

  if(date < 10){
    date = '0'+date;
  }

  if(year=='1970'){var time ='-'}else{
      var time =  hour + ':' + min+ ' ' + date + ' ' + month + ' ' + year;
  }
  return "<span id='account_time'>"+time+'</span><a href="/admin/rota/account/view_profile.php?id='+row.acc_id+'" class="btn btn-sm btn-raised btn-warning" id="view_account" data-id="'+row.id+'"  style="margin:0px; display:none;">View / Edit</a>';
}

function DateFormatter(value, row, index) {
  var a = new Date(value * 1000);
  var a = new Date(a - 3600000);/* daylight saving???*/
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();

  if(year=='1970'){var time ='-'}else{
  var time =  date + ' ' + month + ' ' + year;
}
  return time;
}

function 	Capitalize(value, row, index) {
    return "<span class='text-capitalize'>"+value+"</span>";
}

function StatusFormatter(value, row, index) {
 if(value=='1'){
    return "<span class='label label-success'>Enable</span>";
  }
  else if(value=='0'){
     return "<span class='label label-danger'>Disable</span>";
   }
  else{
    return "<span class='label label-danger'>Error</span>";
  }
}

function JobStatusFormatter(value, row, index) {
 if(value=='1'){
    return "<span class='label label-success'>Live</span>";
  }
  else if(value=='2'){
     return "<span class='label label-warning'>Draft</span>";
   }
   else if(value=='0'){
      return "<span class='label label-danger'>Closed</span>";
    }
  else{
    return "<span class='label label-danger'>Error</span>";
  }
}



function 	JobNumberFormatter(value, row, index) {
    return 'SR'+value+'</a>';
}
function 	LinkUserFormatter(value, row, index) {
    return '<a href="javascript:void(0)" data-id="'+row.id+'" class="text-capitalize viewuser">'+value+'</a>';
}

function 	LinkStaffFormatter(value, row, index) {
    return '<a href="javascript:void(0)" data-id="'+row.id+'" class="text-capitalize viewstaff">'+value+'</a>';
}

function 	LinkJobFormatter(value, row, index) {
    return '<a href="'+$fullurl+'admin/jobs/job.php?id='+row.id+'" class="text-capitalize">'+value+'</a>';
}

function 	LinkPageFormatter(value, row, index) {
    return '<a href="edit.php?id='+row.id+'" class="text-capitalize">'+value+'</a>';
}

function 	LinkAppointmentsFormatter(value, row, index) {
    return '<a href="appointment.php?id='+row.id+'" class="text-capitalize">'+value+'</a>';
}

function 	LinkNewsFormatter(value, row, index) {
    return '<a href="#" data-id="'+row.id+'" class="text-capitalize edit_news">'+value+'</a>';
}

function 	AppointmentSlotFormatter(value, row, index) {
  switch (value) {
    case 'a1': $time='9:00';break;
    case 'a2': $time='10:00';break;
    case 'a3': $time='11:00';break;
    case 'a4': $time='12:00';break;
    case 'a5': $time='13:00';break;
    case 'a6': $time='14:00';break;
    case 'a7': $time='15:00';break;
    case 'a8': $time='16:00';break;
    default:

  }
    return $time;
}

function 	LinkMemberFormatter(value, row, index) {
    return '<a href="member.php?id='+row.id+'" class="text-capitalize">'+value+'</a>';
}

function 	MemberStatusFormatter(value, row, index) {
  if(value=='1'){
     return "<span class='label label-success'>Live</span>";
   }
   else if(value=='2'){
      return "<span class='label label-info'>Registered</span>";
    }
    else if(value=='0'){
       return "<span class='label label-danger'>Deleted</span>";
     }
   else{
     return "<span class='label label-warning'>Error</span>";
   }
}
//<button type="button" class="btn btn-sm btn-raised btn-warning pull-right"id="view_account_approval" data-id="'+row.id+'" style="margin:0px; display:none;">View / Edit</button>
function LinkAccountApproveFormatter(value, row, index){
  return value;
}


function DateTimeFormatterAccountApproval(value, row, index) {
  var a = new Date(value * 1000);
  var a = new Date(a - 3600000);/* daylight saving???*/
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();

  if(hour < 10){
    hour = '0'+hour;
  }

  if(min < 10){
    min = '0'+min;
  }

  if(date < 10){
    date = '0'+date;
  }

  if(year=='1970'){var time ='-'}else{
      var time =  hour + ':' + min+ ' ' + date + ' ' + month + ' ' + year;
  }

  if(row.user_status == 2){
    return "<span id='account_time'>"+time+'</span><a href="/admin/rota/account/view_profile.php?id='+row.id+'" class="btn btn-sm btn-raised btn-warning pull-right" id="view_account_approval" style="margin:0px; display:none;">View</a>';
  }else{
    return "<span id='account_time'>"+time+'</span><button type="button" class="btn btn-sm btn-raised btn-warning pull-right"id="view_account_approval" data-id="'+row.id+'" style="margin:0px; display:none;">View / Edit</button>';
  }


}

// function LinkViewServiceFormatter(value, row, index){
//   return '<a href="javascript:void(0)" class="text-capitalize" id="view_service" data-id="'+row.id+'">'+value+'</a>';
// }

function LinkViewServiceFormatter(value, row, index){
  return value+'<button type="button" class="btn btn-sm btn-raised btn-warning pull-right" id="view_service" data-id="'+row.id+'"  style="margin:0px; display:none;">View / Edit</button>';
}

function LinkViewBidFormatter(value, row, index){
  return '<span id="">'+value+'</span><button type="button" class="btn btn-sm btn-raised btn-warning pull-right" id="view_bid" data-id="'+row.id+'" style="margin:0px; display:none;">View / Edit</button>';
  //return '<a href="javascript:void(0)" class="text-capitalize" id="view_bid" data-id="'+row.id+'">'+value+'</a>';
}

function rotaBidStatusFormatter(value, row, index){
  if(value == '0'){
    return '<span id="bid_status">Oustanding</span>';
  }else if(value == '1'){
    return '<span id="bid_status">Completed</span>';
  }else if(value == '2'){
    return '<span id="bid_status">Refill Needed</span>';
  }else if(value == '3'){
    return '<span id="bid_status">Reoccurring - Waiting</span>';
  }else{
    return '<span id="bid_status">Error</span>';
  }
}

function LinkViewProfileFormatter(value, row, index){
  return '<a href="/admin/rota/account/view_profile.php?id='+row.acc_id+'" class="text-capitalize" id="view_profile" data-id="'+row.id+'">'+value+'</a>';
}

function LinkViewPendingShifts(value, row, index){
  return '<a href="/admin/rota/account/shift_response.php?id='+row.id+'&account='+row.account_id+'" class="text-capitalize" id="view_profile" data-id="'+row.id+'">'+value+'</a>';
}

function LinkViewPCNFormatter(value, row, index){
  return '<span id="">'+value+'</span><button type="button" class="btn btn-sm btn-raised btn-warning pull-right" id="view_pcn" data-id="'+row.id+'" style="margin:0px; display:none;">View / Edit</button>';
}

function LinkEditSystemUserFormatter(value, row, index){
  return '<span id="">'+value+'</span><button type="button" class="btn btn-sm btn-raised btn-warning pull-right" id="edit_system_user" data-id="'+row.id+'" style="margin:0px; display:none;">View / Edit</button>';
}
