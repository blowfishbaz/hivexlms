
/////////////////////////////////////////////////////////
//////////////////////////Time///////////////////////////
/////////////////////////////////////////////////////////
$("body").on("click","#addTime",function(){
  $("#TimeExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
  $('#TimeExpenseModal').modal('show');
   $.ajax({
     type: "POST",
     url: "../../../assets/app_forms/time_attendance/AddTimeLog.php",
     data: {id:$tedid},
     success: function(msg){
       //alert(msg);
       $("#TimeExpenseModalContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        $('#start_date').bootstrapMaterialDatePicker({ weekStart : 1, time: false, format : 'DD-MM-YYYY'});

        $tedid_type = $tedid.substring(0, 7);

        if($tedid_type =='id_chx_' || $tedid_type=='id_phx_' || $tedid_type =='id_pros'){
        $('#client').chosen();
        $('#project').chosen();

        $('#act_client_type').chosen();
        $('#stage').chosen();
        $('#act_holding').chosen();
        $('#department').chosen();
      }else{

      }

        });

     }
});
});

$("body").on("click",".phone_number",function(event){
  event.stopImmediatePropagation();
  $('.modal').modal('hide');
  $number_id=$(this).attr("data-id");

  $("#TimeExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
  $('#TimeExpenseModal').modal('show');
   $.ajax({
     type: "POST",
     url: "../../../assets/app_forms/time_attendance/AddTimeLog.php",
     data: {id:$tedid,num:$number_id},
     success: function(msg){
       //alert(msg);
       $("#TimeExpenseModalContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        $('#start_date').bootstrapMaterialDatePicker({ weekStart : 1, time: false, format : 'DD-MM-YYYY'});

        $tedid_type = $tedid.substring(0, 7);

        if($tedid_type =='id_chx_' || $tedid_type=='id_phx_' || $tedid_type =='id_pros'){
        $('#client').chosen();
        $('#project').chosen();

        $('#act_client_type').chosen();
        $('#stage').chosen();
        $('#act_holding').chosen();
        $('#department').chosen();
        $('#contact').chosen();

      }else{

      }

        });

     }
});

});

$("body").on("click","#ExportTimeExpense",function(){
  $("#TimeExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
  $('#TimeExpenseModal').modal('show');
   $.ajax({
     type: "POST",
     url: "../../../assets/app_forms/time_attendance/ExportTimeExpense.php",
     data: "id:1",
     success: function(msg){
       //alert(msg);
       $("#TimeExpenseModalContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
     }
});
});

$("body").on("change","#customer",function(){
  var id = $(this).val();
  $('#stage').val('');
  $("#stage").attr('disabled', true);
  $("#stage").trigger("chosen:updated");
  $.ajax({
    url: '../../../assets/app_ajax/time_attendance/log_time/get_job.php',
    type: 'POST',
    dataType: "json",
    beforeSend: function(){
      $('#job').empty();
      $('#job').prop('disabled', false);
     },
     data: {id: id},
    success: function(msg) {
        $('#job').append('<option value="">Select Project</option>');
        $.map( msg, function( item ) {
                   $('#job').append('<option value="'+item.id+'">RUR'+item.job_number+' - '+item.address1+' '+item.postcode+'</option>');
          });
          $("#job").trigger("chosen:updated");
    }
  });
});

$("body").on("change","#job",function(){
  var id = $(this).val();

  $.ajax({
    url: '../../../assets/app_ajax/time_attendance/log_time/get_stage.php',
    type: 'POST',
    dataType: "json",
    beforeSend: function(){
      $('#stage').empty();
      $('#stage').prop('disabled', false);
     },
     data: {id: id},
    success: function(msg) {
        $('#stage').append('<option value="">Select Stage</option>');
        $.map( msg, function( item ) {
                  var remain = item.hourly_rate_cap - item.cost;
                  if(remain <0){remain = 0;}
                   $('#stage').append('<option value="'+item.id+'" data-remain="'+remain+'" data-type="'+item.rate_type+'" data-hr="'+item.hourly_rate+'" data-limit="'+item.hourly_rate_cap+'">'+item.stage_name+'</option>');

          });
          $("#stage").trigger("chosen:updated");
    }
  });
});

$("body").on("change","#hours",function(){
   var hour_taken = $(this).val();
   var minutes_taken =  $('#minutes').val();
   var total_time = parseInt(hour_taken)*60+parseInt(minutes_taken);

   if(total_time>=0){
   $('#total_time').val(total_time);
   $('#total_time').change();
 }
});


$("body").on("change","#minutes",function(){
  var hour_taken = $('#hours').val();
  var minutes_taken =  $(this).val();
  var total_time = parseInt(hour_taken)*60+parseInt(minutes_taken);
  if(total_time>=0){
  $('#total_time').val(total_time);
  $('#total_time').change();
}
});


function Epoch(date) {
    return Math.round(new Date(date).getTime() / 1000.0);
}

function difference(date1, date2){
	return (Epoch(parse_date(date2)) - Epoch(parse_date(date1))) / 60
}

function parse_date(date){
        var from = date.split(/[- :]/);
        var f = new Date(from[2], from[1] - 1, from[0], from[3], from[4]);
        return f;
}

$("body").on("click","#save_log",function(){
  var form = $('#log_time_form').serialize();
  var type = $( "#stage option:selected" ).data('type');
  var remain = $( "#stage option:selected" ).data('remain');
  var hr = $( "#stage option:selected" ).data('hr');
  var limit = $( "#stage option:selected" ).data('limit');
  var total_minutes = $('#total_time').val();
  if(type == 'hourly_rate' && limit != ''){

          var per_minute = hr / 60;
          var hours_worked = per_minute * total_minutes;

          if(remain <= hours_worked){
            var HasError = 0;
            $('#log_time_form').find('input').each(function() {
              $(this).parent().removeClass('has-error');
              Messenger().hideAll();
              if (!$.trim(this.value).length) { // zero-length string AFTER a trim
                if (!$(this).prop('required')) {
                } else {
                  HasError = 1;
                  $(this).parent().addClass('has-error');
                }
              }
            });
            if (HasError == 1) {
              Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
              });
            } else {
                $('.modal').delay(200).queue(function(n) {
                  $('.modal').modal('hide');
                  n();
                });
                var url = "../../../assets/app_ajax/time_attendance/log_time_commission.php"; // PHP save page
                $.ajax({
                  type: "POST",
                  url: url,
                  data: form, // serializes the form's elements.
                  success: function(data) {
                    if (data == 'ok' || data == " ok") {
                      //appRed(s[1])
                      Messenger().post({
                        message: 'Time Logged Against Job. Please note the budget has been reached',
                        type: 'info'
                      });
                      setTimeout(function(){
                        window.location.reload();
                          }, 2000);
                    } else {
                      Messenger().post({
                        message: 'An Error Has occured.',
                        type: 'error',
                        showCloseButton: false
                      });
                    }
                  }
                });
            }
          }else{
            var HasError = 0;
            $('#log_time_form').find('input').each(function() {
              $(this).parent().removeClass('has-error');
              Messenger().hideAll();
              if (!$.trim(this.value).length) { // zero-length string AFTER a trim
                if (!$(this).prop('required')) {
                } else {
                  HasError = 1;
                  $(this).parent().addClass('has-error');
                }
              }
            });
            if (HasError == 1) {
              Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
              });
            } else {
                $('.modal').delay(200).queue(function(n) {
                  $('.modal').modal('hide');
                  n();
                });
                var url = "../../../assets/app_ajax/time_attendance/log_time_commission.php"; // PHP save page
                $.ajax({
                  type: "POST",
                  url: url,
                  data: form, // serializes the form's elements.
                  success: function(data) {
                    if (data == 'ok' || data == " ok") {
                      //appRed(s[1])
                      Messenger().post({
                        message: 'Time Logged Against Job.',
                        type: 'info'
                      });
                      setTimeout(function(){
                        window.location.reload();
                          }, 2000);
                    } else {
                      Messenger().post({
                        message: 'An Error Has occured.',
                        type: 'error',
                        showCloseButton: false
                      });
                    }
                  }
                });
            }
          }
  }else{
    var HasError = 0;
    $('#log_time_form').find('input').each(function() {
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if (!$.trim(this.value).length) { // zero-length string AFTER a trim
        if (!$(this).prop('required')) {
        } else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
    });
    if (HasError == 1) {
      Messenger().post({
        message: 'Please make sure all required elements of the form are filled out.',
        type: 'error',
        showCloseButton: false
      });
    } else {
        $('.modal').delay(200).queue(function(n) {
          $('.modal').modal('hide');
          n();
        });
        var url = "../../../assets/app_ajax/time_attendance/log_time_commission.php"; // PHP save page
        $.ajax({
          type: "POST",
          url: url,
          data: form, // serializes the form's elements.
          success: function(data) {
            if (data == 'ok' || data == " ok") {
              //appRed(s[1])
              Messenger().post({
                message: 'Time Logged Against Job.',
                type: 'info'
              });
              setTimeout(function(){
                window.location.reload();
                  }, 2000);
            } else {
              Messenger().post({
                message: 'An Error Has occured.',
                type: 'error',
                showCloseButton: false
              });
            }
          }
        });
    }
  }
});

$("body").on("change","#act_type",function(){
  var type = $(this).val();
  $('#act_client_commissioned').parent().hide();
  $('#act_client_precommissioning').parent().hide();
  $('#act_client_commissioned').val('');
  $('#act_client_precommissioning').val('');
  $('#act_holding').parent().hide();
  $('.timer_holder').hide();
  $('#work_location').parent().hide();
  //$('.note_holder').hide();
  //$('.note_holder').val('');

  $('#act_rural').removeAttr('required');
  $('#act_client_type').removeAttr('required');

  $('#act_holding').removeAttr('required');
  $('#act_client_precommissioning').removeAttr('required');
  $('#act_client_commissioned').removeAttr('required');
  $('#client').removeAttr('required');
  $('#project').removeAttr('required');
  $('#stage').removeAttr('required');
  $('#department').removeAttr('required');
  //$('#notes').removeAttr('required');


  $("#stage").empty();
  $('#stage').append('<option value="" selected disabled>Select</option>');
  $('#stage').attr("disabled", true);
  $('#stage').trigger('chosen:updated');

  $('#act_client_type').find('option:eq(0)').prop('selected', true);
  $('#act_client_type').attr("disabled", true);
  $('#act_client_type').trigger('chosen:updated');

  $('#act_client_precommissioning').find('option:eq(0)').prop('selected', true);
  $('#act_client_precommissioning').attr("disabled", true);
  $('#act_client_precommissioning').trigger('chosen:updated');
  $('#act_client_precommissioning').parent().hide();

  $('#act_client_commissioned').find('option:eq(0)').prop('selected', true);
  $('#act_client_commissioned').attr("disabled", true);
  $('#act_client_commissioned').trigger('chosen:updated');
  $('#act_client_commissioned').parent().hide();

  $('#act_holding').find('option:eq(0)').prop('selected', true);
  $('#act_holding').attr("disabled", true);
  $('#act_holding').trigger('chosen:updated');
  $('#act_holding').parent().show();


  $("#department").empty();
  $('#department').append('<option value="" selected disabled>Select</option>');
  $('#department').attr("disabled", true);
  $('#department').trigger('chosen:updated');

  $('#act_client_activity').parent().parent().hide();
  $('#act_client_activity').empty();
  $('#act_client_activity').attr("disabled", true);
  $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
  $('#act_client_activity').removeAttr('required');
  $('#act_client_activity').trigger('chosen:updated');

  $('#department').empty();
  $('#department').attr("disabled", true);
  $('#department').append('<option value="" selected disabled>Select</option>');
  $('#department').removeAttr('required');
  $('#department').trigger('chosen:updated');

  $("#project").empty();
  $('#project').attr("disabled", true);
  $('#project').append('<option value="" selected disabled>Select</option>');
  $('#project').append('<option value="Relationship Management">Relationship Management</option>');
  $('#project').removeAttr('required');
  $('#project').trigger('chosen:updated');

  $('#client').find('option:eq(0)').prop('selected', true);
  $('#client').attr("disabled", true);
  $('#client').trigger('chosen:updated');

  $('#act_rural_type').parent().hide();
  $("#act_rural_type").empty();
  $('#act_rural_type').attr("disabled", true);
  $('#act_rural_type').append('<option value="" selected disabled>Select</option>');
  $('#act_rural_type').attr('required');
  $('#act_rural_type').trigger('chosen:updated');


  if(type == 'Clients'){
    $('#act_client_type').parent().show();
    $('#act_rural').parent().hide();
    $('#act_rural').val('');
    $('#act_holding').parent().show();
    $('.project_holder').show();
    $('.timer_holder').show();
    $('#work_location').parent().show();

    $('#act_client_type').chosen();
    $('#act_holding').chosen();
    $('#client').chosen();
    $('#project').chosen();
    $('#stage').chosen();
    $('#department').chosen();

    $('#act_client_type').prop('required', true);
    $('#client').prop('required', true);
    $('#project').prop('required', true);
    $('#stage').prop('required', true);
    $('#department').prop('required', true);

    $('#client').attr("disabled", false);
    $('#client').trigger('chosen:updated');

  }else{
    $('.timer_holder').show();
    $('#work_location').parent().show();
    $('#act_rural').parent().show();
    $('#act_client_type').parent().hide();
    $('#act_client_type').val('');
    $('.project_holder').hide();
    $('.timer_holder').show();
    $('#act_rural').chosen();

    $('#act_rural').prop('required', true);
  }
});

$("body").on("change","#act_client_type",function(){
  var type = $(this).val();
  $('#act_holding').parent().hide();
  $('#act_client_commissioned').val('');
  $('#act_client_precommissioning').val('');
  //$('.note_holder').hide();
  //$('.note_holder').val('');

  $('#act_holding').removeAttr('required');
  $('#act_client_precommissioning').removeAttr('required');
  $('#act_client_commissioned').removeAttr('required');
  //$('#notes').removeAttr('required');

  $('#act_client_activity').parent().parent().hide();
  $('#act_client_activity').empty();
  $('#act_client_activity').attr("disabled", true);
  $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
  $('#act_client_activity').removeAttr('required');
  $('#act_client_activity').trigger('chosen:updated');

  $("#contact").empty();
  $('#contact').append('<option value="" selected disabled>Select</option>');
  $('#contact').parent().hide();
  $('#contact').attr("disabled", true);
  $('#contact').trigger('chosen:updated');

  if(type == 'Pre Commissioning'){
    $('#act_client_precommissioning').removeAttr("disabled");
    $('#act_client_precommissioning').trigger('chosen:updated');

    $('#act_client_precommissioning').parent().show();
    $('#act_client_commissioned').parent().hide();
    $('#act_client_precommissioning').chosen();
    $('#act_client_precommissioning').prop('required', true);

    $("#stage").empty();
    $("#stage").attr("disabled", true);
    $("#stage").append('<option value="" selected disabled>Select</option>');
    $("#stage").removeAttr('required');
    $("#stage").trigger('chosen:updated');

    var department = $('#department_add_time').val();

    var dep_add = '';
    var dep_pln = '';
    var dep_des = '';

    if(department == 'Advisory'){
      dep_add = 'selected';
    }
    if(department == 'Planning'){
      dep_pln = 'selected';
    }
    if(department == 'Design'){
      dep_des = 'selected';
    }


           $("#department").empty();
           $('#department').append('<option value="">Select</option>');
           $('#department').append('<option '+dep_add+' value="Advisory">Advisory</option>');
           $('#department').append('<option '+dep_pln+' value="Planning">Planning</option>');
           $('#department').append('<option '+dep_des+' value="Design">Design</option>');
           $('#department').removeAttr('disabled');
           $('#department').trigger('chosen:updated');

  }else{
    $('#act_client_commissioned').removeAttr("disabled");
    $('#act_client_commissioned').trigger('chosen:updated');

    $('#act_client_commissioned').parent().show();
    $('#act_client_precommissioning').parent().hide();
    $('#act_client_commissioned').chosen();

    $('#act_client_commissioned').prop('required', true);

    $('#department').empty();
    $('#department').attr("disabled", true);
    $('#department').append('<option value="" selected disabled>Select</option>');
    $('#department').removeAttr('required');
    $('#department').trigger('chosen:updated');

    var project = $('#project').val();
    var count = 0;
    var stage_select = '';
    $.ajax({
      url: '../../../assets/app_ajax/time_attendance/get_stages.php?id='+project,
      type: 'GET',
      dataType: "json",
      beforeSend: function() {
        $("#stage").empty();
      },
      data: {id: project},
      success: function(msg) {
        $('#stage').append('<option value=""selected disabled>Select</option>');
        $.map(msg, function(item) {
          count++;
          stage_select = item.id;
          console.log(count);
          console.log(stage_select);
          $('#stage').append('<option value="'+item.id+'">'+item.stage_name+'</option>');
           });
           if(count == 1){
             $('#stage').val(stage_select);
           }
           $('#stage').removeAttr('disabled');
           $('#stage').trigger('chosen:updated');
      }
    });

  }
});

$("body").on("change","#expense",function(){
  var type = $(this).val();

  if(type == 'id_exp_20180604_141318347100_71720'){
    $('#third_party').parent().show();
    $('#third_party').prop('required', true);
  }else{
    $('#third_party').parent().hide();
    $('#third_party').removeAttr('required');
  }
});

// $("body").on("change","#act_rural_type",function(){
//   var type = $(this).val();
//   if(type == 'General Admin - General Research' || type == 'General Admin - All Other Admin' || type == 'HR - All Other HR' || type == 'Marketing - All other Marketing / PR'){
//     $('.note_holder').show();
//     $('.note_holder').val('');
//   }else{
//     $('.note_holder').hide();
//     $('.note_holder').val('');
//   }
// });

$("body").on("change","#act_rural",function(){
  var type = $(this).val();
  //console.log(type);
  //$('#notes').removeAttr('required');
  // if(type == 'Accounting / Book Keeping' || type == 'Exective Management'){
  //   $('.note_holder').show();
  //   $('.note_holder').val('');
  // }else{
  //   $('.note_holder').hide();
  //   $('.note_holder').val('');
  // }


  if(type == 'Absence'){
    $('#act_rural_type').parent().show();
    $('#act_rural_type').chosen();
    $("#act_rural_type").empty();
    $('#act_rural_type').attr("disabled", false);
    $('#act_rural_type').append('<option value="" selected disabled>Select</option>');
    $('#act_rural_type').append('<option value="Absence - Annual Leave">Absence - Annual Leave</option>');
    $('#act_rural_type').append('<option value="Absence - Sickness">Absence - Sickness</option>');
    $('#act_rural_type').append('<option value="Absence - Private Appointment">Absence - Private Appointment</option>');
    $('#act_rural_type').append('<option value="Absence - Bank Holiday">Absence - Bank Holiday</option>');
    $('#act_rural_type').append('<option value="Absence - Non-working Hours">Absence - Non-working Hours</option>');
    $('#act_rural_type').attr('required');
    $('#act_rural_type').trigger('chosen:updated');
  }
  else if(type == 'General Admin'){
    $('#act_rural_type').parent().show();
    $('#act_rural_type').chosen();
    $("#act_rural_type").empty();
    $('#act_rural_type').attr("disabled", false);
    $('#act_rural_type').append('<option value="" selected disabled>Select</option>');
    $('#act_rural_type').append('<option value="General Admin - Expenses">General Admin - Expenses</option>');
    $('#act_rural_type').append('<option value="General Admin - General Research">General Admin - General Research</option>');
    $('#act_rural_type').append('<option value="General Admin - IT Issues">General Admin - IT Issues</option>');
    $('#act_rural_type').append('<option value="General Admin - Internal Meetings">General Admin - Internal Meetings</option>');
    $('#act_rural_type').append('<option value="General Admin - All Other Admin">General Admin - All Other Admin</option>');
    $('#act_rural_type').attr('required');
    $('#act_rural_type').trigger('chosen:updated');
  }
  else if(type == 'HR'){
  $('#act_rural_type').parent().show();
  $('#act_rural_type').chosen();
  $("#act_rural_type").empty();
  $('#act_rural_type').attr("disabled", false);
  $('#act_rural_type').append('<option value="" selected disabled>Select</option>');
  $('#act_rural_type').append('<option value="HR - Appraisals">HR - Appraisals</option>');
  $('#act_rural_type').append('<option value="HR - Continuous Professional Development / Training">HR - Continuous Professional Development / Training</option>');
  $('#act_rural_type').append('<option value="HR - Recruitment">HR - Recruitment</option>');
  $('#act_rural_type').append('<option value="HR - All Other HR">HR - All Other HR</option>');
  $('#act_rural_type').attr('required');
  $('#act_rural_type').trigger('chosen:updated');
}
  else if(type == 'Marketing'){
    $('#act_rural_type').parent().show();
    $('#act_rural_type').chosen();
    $("#act_rural_type").empty();
    $('#act_rural_type').attr("disabled", false);
    $('#act_rural_type').append('<option value="" selected disabled>Select</option>');
    $('#act_rural_type').append('<option value="Marketing - Client Hospitality">Marketing - Client Hospitality</option>');
    $('#act_rural_type').append('<option value="Marketing - RSL Events">Marketing - RSL Events</option>');
    $('#act_rural_type').append('<option value="Marketing - All other Marketing / PR">Marketing - All other Marketing / PR</option>');
    $('#act_rural_type').attr('required');
    $('#act_rural_type').trigger('chosen:updated');
  }
    else{
      $('#act_rural_type').parent().hide();
      $("#act_rural_type").empty();
      $('#act_rural_type').attr("disabled", true);
      $('#act_rural_type').append('<option value="" selected disabled>Select</option>');
      $('#act_rural_type').attr('required');
      $('#act_rural_type').trigger('chosen:updated');
    }
});

$("body").on("change","#act_client_precommissioning",function(){
  var type = $(this).val();

  //$('#notes').removeAttr('required');

  $("#contact").empty();
  $('#contact').parent().hide();
  $('#contact').attr("disabled", true);
  $('#contact').trigger('chosen:updated');

  if(type == 'Research and Preperation' || type == 'Proposal Generation' || type == 'Internal Meeting (BD)' ){
    //$('.note_holder').show();
    //$('.note_holder').val('');
    //$('#notes').prop('required', true);

    $('#act_client_activity').parent().parent().hide();
    $('#act_client_activity').empty();
    $('#act_client_activity').attr("disabled", true);
    $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
    $('#act_client_activity').removeAttr('required');
    $('#act_client_activity').trigger('chosen:updated');
  }else if(type == 'Client Communication'){
    //$('.note_holder').show();
    //$('.note_holder').val('');
    //$('#notes').prop('required', true);

    $('#act_client_activity').parent().parent().hide();
    $('#act_client_activity').empty();
    $('#act_client_activity').attr("disabled", true);
    $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
    $('#act_client_activity').removeAttr('required');
    $('#act_client_activity').trigger('chosen:updated');

    var cid = $("#client").val();
    $.ajax({
      url: '../../../assets/app_ajax/time_attendance/get_contact.php?id='+cid,
      type: 'GET',
      dataType: "json",
      beforeSend: function() {
        $("#contact").empty();
      },
      data: {id: cid},
      success: function(msg) {
        $('#contact').append('<option value="" selected disabled>Select</option>');
        $.map(msg, function(item) {
                          if(item.title == 'Esq.'){
                              $t1 = ''; $t2 = item.title;
                             }else{
                              $t1 = item.title; $t2 = '';
                             }

                             if($t1 == '' || $t1 == null){
                              $t1 = '';
                             }
                             if($t2 == '' || $t2 == null){
                              $t2 = '';
                             }
                             $name = $t1+' '+item.name1+' '+item.name2+' '+$t2;
          $('#contact').append('<option value="'+item.contact_id+'">'+$name+'</option>');
           });
           $('#contact').parent().show();
           $('#contact').chosen();
           $('#contact').removeAttr('disabled');
           $('#contact').trigger('chosen:updated');
      }
    });
  }else if (type =='Client Meeting'){
      $('#act_client_activity').parent().parent().show();
      $('#act_client_activity').chosen();
      $('#act_client_activity').empty();
      $('#act_client_activity').attr("disabled", false);
      $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
      $('#act_client_activity').append('<option value="Client Meeting - Preparation">Preparation</option>');
      $('#act_client_activity').append('<option value="Client Meeting - Duration of Meeting">Duration of Meeting</option>');
      $('#act_client_activity').append('<option value="Client Meeting - Travel Time">Travel Time</option>');
      $('#act_client_activity').attr('required');
      $('#act_client_activity').trigger('chosen:updated');
  }
  else{
    $('#act_client_activity').parent().parent().hide();
    $('#act_client_activity').empty();
    $('#act_client_activity').attr("disabled", true);
    $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
    $('#act_client_activity').removeAttr('required');
    $('#act_client_activity').trigger('chosen:updated');

    //$('.note_holder').hide();
    //$('.note_holder').val('');
  }
});

$("body").on("change","#act_client_commissioned",function(){
  var type = $(this).val();
  //$('#notes').removeAttr('required');

  $("#contact").empty();
  $('#contact').parent().hide();
  $('#contact').attr("disabled", true);
  $('#contact').trigger('chosen:updated');

  if(type == 'Research and Preperation' || type == 'Proposal Generation' || type == 'Internal Meeting (BD)' ){
    //$('.note_holder').show();
    //$('.note_holder').val('');
    //$('#notes').prop('required', true);
    $('#act_client_activity').parent().parent().hide();
    $('#act_client_activity').empty();
    $('#act_client_activity').attr("disabled", true);
    $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
    $('#act_client_activity').removeAttr('required');
    $('#act_client_activity').trigger('chosen:updated');
  }else if(type == 'Client Communication'){
    //$('.note_holder').show();
    //$('.note_holder').val('');
    //$('#notes').prop('required', true);
    $('#act_client_activity').parent().parent().hide();
    $('#act_client_activity').empty();
    $('#act_client_activity').attr("disabled", true);
    $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
    $('#act_client_activity').removeAttr('required');
    $('#act_client_activity').trigger('chosen:updated');

    var cid = $("#client").val();
    $.ajax({
      url: '../../../assets/app_ajax/time_attendance/get_contact.php?id='+cid,
      type: 'GET',
      dataType: "json",
      beforeSend: function() {
        $("#contact").empty();
      },
      data: {id: cid},
      success: function(msg) {
        $('#contact').append('<option value="" selected disabled>Select</option>');
        $.map(msg, function(item) {
          if(item.title == 'Esq.'){
              $t1 = ''; $t2 = item.title;
             }else{
              $t1 = item.title; $t2 = '';
             }

             if($t1 == '' || $t1 == null){
              $t1 = '';
             }
             if($t2 == '' || $t2 == null){
              $t2 = '';
             }
             $name = $t1+' '+item.name1+' '+item.name2+' '+$t2;
             $('#contact').append('<option value="'+item.contact_id+'">'+$name+'</option>');
           });
           $('#contact').parent().show();
           $('#contact').chosen();
           $('#contact').removeAttr('disabled');
           $('#contact').trigger('chosen:updated');
      }
    });

  }else if (type =='Meeting'){
      $('#act_client_activity').parent().parent().show();
      $('#act_client_activity').chosen();
      $('#act_client_activity').empty();
      $('#act_client_activity').attr("disabled", false);
      $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
      $('#act_client_activity').append('<option value="Meeting - Project Meeting with Client">Project Meeting with Client</option>');
      $('#act_client_activity').append('<option value="Meeting - Site Meeting">Site Meeting</option>');
      $('#act_client_activity').append('<option value="Meeting - Project Meeting">Project Meeting</option>');
      $('#act_client_activity').append('<option value="Meeting - Inception Meeting">Inception Meeting</option>');
      $('#act_client_activity').append('<option value="Meeting - Attendance at Appeal/Inquiry">Attendance at Appeal/Inquiry</option>');
      $('#act_client_activity').append('<option value="Meeting - Preperation">Preperation</option>');
      $('#act_client_activity').append('<option value="Meeting - Travel Time">Travel Time</option>');
      $('#act_client_activity').attr('required');
      $('#act_client_activity').trigger('chosen:updated');
  }else{
    $('#act_client_activity').parent().parent().hide();
    $('#act_client_activity').empty();
    $('#act_client_activity').attr("disabled", true);
    $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
    $('#act_client_activity').removeAttr('required');
    $('#act_client_activity').trigger('chosen:updated');

    //$('.note_holder').hide();
    //$('.note_holder').val('');
  }
});


$("body").on("change","#act_holding",function(){
  var type = $(this).val();
  //$('#notes').removeAttr('required');

  if(type == 'Client Communication'){
    //$('.note_holder').show();
    //$('.note_holder').val('');
    //$('#notes').prop('required', true);

    var cid = $("#client").val();
    $.ajax({
      url: '../../../assets/app_ajax/time_attendance/get_contact.php?id='+cid,
      type: 'GET',
      dataType: "json",
      beforeSend: function() {
        $("#contact").empty();
      },
      data: {id: cid},
      success: function(msg) {
        $('#contact').append('<option value="" selected disabled>Select</option>');
        $.map(msg, function(item) {
          if(item.title == 'Esq.'){
              $t1 = ''; $t2 = item.title;
             }else{
              $t1 = item.title; $t2 = '';
             }

             if($t1 == '' || $t1 == null){
              $t1 = '';
             }
             if($t2 == '' || $t2 == null){
              $t2 = '';
             }
             $name = $t1+' '+item.name1+' '+item.name2+' '+$t2;
             $('#contact').append('<option value="'+item.contact_id+'">'+$name+'</option>');
           });
           $('#contact').parent().show();
           $('#contact').chosen();
           $('#contact').removeAttr('disabled');
           $('#contact').trigger('chosen:updated');
      }
    });

  }else{
    $("#contact").empty();
    $('#contact').append('<option value="" selected disabled>Select</option>');
    $('#contact').parent().hide();
    $('#contact').attr("disabled", true);
    $('#contact').trigger('chosen:updated');

    //$('.note_holder').hide();
    //$('.note_holder').val('');
  }
});

$("body").on("change","#client",function(){
  var id = $(this).val();
  $("#stage").empty();
  $('#stage').append('<option value="" selected disabled>Select</option>');
  $('#stage').attr("disabled", true);
  $('#stage').trigger('chosen:updated');

  $('#act_client_type').find('option:eq(0)').prop('selected', true);
  $('#act_client_type').attr("disabled", true);
  $('#act_client_type').trigger('chosen:updated');

  $('#act_client_precommissioning').find('option:eq(0)').prop('selected', true);
  $('#act_client_precommissioning').attr("disabled", true);
  $('#act_client_precommissioning').trigger('chosen:updated');
  $('#act_client_precommissioning').parent().hide();

  $('#act_client_commissioned').find('option:eq(0)').prop('selected', true);
  $('#act_client_commissioned').attr("disabled", true);
  $('#act_client_commissioned').trigger('chosen:updated');
  $('#act_client_commissioned').parent().hide();

  $('#act_holding').find('option:eq(0)').prop('selected', true);
  $('#act_holding').attr("disabled", true);
  $('#act_holding').trigger('chosen:updated');
  $('#act_holding').parent().show();

  $("#contact").empty();
  $('#contact').append('<option value="" selected disabled>Select</option>');
  $('#contact').parent().hide();
  $('#contact').attr("disabled", true);
  $('#contact').trigger('chosen:updated');

  $("#department").empty();
  $('#department').append('<option value="" selected disabled>Select</option>');
  $('#department').attr("disabled", true);
  $('#department').trigger('chosen:updated');

  $('#act_client_activity').parent().parent().hide();
  $('#act_client_activity').empty();
  $('#act_client_activity').attr("disabled", true);
  $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
  $('#act_client_activity').removeAttr('required');
  $('#act_client_activity').trigger('chosen:updated');

  $('#department').empty();
  $('#department').attr("disabled", true);
  $('#department').append('<option value="" selected disabled>Select</option>');
  $('#department').removeAttr('required');
  $('#department').trigger('chosen:updated');

  $.ajax({
    url: '../../../assets/app_ajax/time_attendance/get_project.php?id='+id,
    type: 'GET',
    dataType: "json",
    beforeSend: function() {
      $("#project").empty();
    },
    data: {id: id},
    success: function(msg) {
      $('#project').append('<option value="" selected disabled>Select</option>');
      $.map(msg, function(item) {
        $('#project').append('<option value="'+item.id+'" data-status="'+item.job_status+'">RUR'+item.job_number+' - '+item.address1+' '+item.postcode+'</option>');
         });
         $('#project').append('<option value="Relationship Management">Relationship Management</option>');
         $('#project').removeAttr('disabled');
         $('#project').trigger('chosen:updated');
    }
  });
});
$("body").on("change","#project",function(){
  var id = $(this).val();
  var $status = $(this).find(':selected').data("status");
  var selected_cat = 'No';
  $('#act_client_type').find('option:eq(0)').prop('selected', true);
  if($status<='3'){
    $('#act_client_type').find('option:eq(1)').prop('disabled', false);
  }
  else{
    $('#act_client_type').find('option:eq(1)').prop('disabled', true);
  }
  $("#stage").empty();
  $('#stage').append('<option value="" selected disabled>Select</option>');
  $('#stage').attr("disabled", true);
  $('#stage').trigger('chosen:updated');

  //$('#act_client_type').find('option:eq(0)').prop('selected', true);
  $('#act_client_type').removeAttr('disabled');
  $('#act_client_type').trigger('chosen:updated');

  $('#act_client_precommissioning').find('option:eq(0)').prop('selected', true);
  $('#act_client_precommissioning').attr("disabled", true);
  $('#act_client_precommissioning').trigger('chosen:updated');
  $('#act_client_precommissioning').parent().hide();

  $('#act_client_commissioned').find('option:eq(0)').prop('selected', true);
  $('#act_client_commissioned').attr("disabled", true);
  $('#act_client_commissioned').trigger('chosen:updated');
  $('#act_client_commissioned').parent().hide();

  $('#act_holding').find('option:eq(0)').prop('selected', true);
  $('#act_holding').attr("disabled", true);
  $('#act_holding').trigger('chosen:updated');
  $('#act_holding').parent().show();

  $("#contact").empty();
  $('#contact').append('<option value="" selected disabled>Select</option>');
  $('#contact').parent().hide();
  $('#contact').attr("disabled", true);
  $('#contact').trigger('chosen:updated');

  $("#department").empty();
  $('#department').append('<option value="" selected disabled>Select</option>');
  $('#department').attr("disabled", true);
  $('#department').trigger('chosen:updated');

  $('#act_client_activity').parent().parent().hide();
  $('#act_client_activity').empty();
  $('#act_client_activity').attr("disabled", true);
  $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
  $('#act_client_activity').removeAttr('required');
  $('#act_client_activity').trigger('chosen:updated');

  if(id=='Relationship Management'){
    $('#act_client_type').attr("disabled", true);
    $('#act_client_type').trigger('chosen:updated');

    $('#act_holding').find('option:eq(0)').prop('selected', true);
    $('#act_holding').attr("disabled", false);
    $('#act_holding').trigger('chosen:updated');
  }else{
    if($status<='3'){
      $('#act_client_type').find('option:eq(2)').removeAttr('selected');
    }
    else{
      $('#act_client_type').find('option:eq(2)').prop('selected', true);
    }
    $('#act_client_type').removeAttr('disabled');
    $('#act_client_type').trigger('chosen:updated');

  }

});

$("body").on("change","#stage",function(){
 var id = $(this).val();

 $('#act_client_precommissioning').find('option:eq(0)').prop('selected', true);
 $('#act_client_precommissioning').trigger('chosen:updated');

 $('#act_client_commissioned').find('option:eq(0)').prop('selected', true);
 $('#act_client_commissioned').trigger('chosen:updated');

 $('#act_client_activity').parent().parent().hide();
 $('#act_client_activity').empty();
 $('#act_client_activity').attr("disabled", true);
 $('#act_client_activity').append('<option value="" selected disabled>Select</option>');
 $('#act_client_activity').removeAttr('required');
 $('#act_client_activity').trigger('chosen:updated');
 $.ajax({
   url: '../../../assets/app_ajax/sales/commission/get_departments.php?id='+id,
   type: 'GET',
   dataType: "json",
   beforeSend: function() {
     $("#department").empty();
   },
   data: {id: id},
   success: function(msg) {
     $('#department').append('<option value="">Select</option>');
     $.map(msg, function(item) {
             if(item.design_rate == null){
                     var arch_status = 'disabled';
             }
             if(item.planning_rate == null){
                     var plan_status = 'disabled';
             }
             if(item.advisory_rate == null){
                     var add_status = 'disabled';
             }
             var department = $('#department_add_time').val();

             var dep_add = '';
             var dep_pln = '';
             var dep_des = '';

             if(department == 'Advisory'){
               dep_add = 'selected';
             }
             if(department == 'Planning'){
               dep_pln = 'selected';
             }
             if(department == 'Design'){
               dep_des = 'selected';
             }
       $('#department').append('<option '+dep_add+' value="Advisory" data-type="'+item.advisory_rate+'" '+add_status+'>Advisory</option>');
       $('#department').append('<option '+dep_pln+' value="Planning" data-type="'+item.planning_rate+'" '+plan_status+'>Planning</option>');
       $('#department').append('<option '+dep_des+' value="Design" data-type="'+item.design_rate+'" '+arch_status+'>Design</option>');
       $('#department').removeAttr('disabled');
       $('#department').trigger('chosen:updated');
        });
   }
 });
});


///////////////////////////
//Save Function Goes Here//
///////////////////////////


$("body").on("click","#save_log_time",function(){
  $(this).attr("disabled", "disabled");
  var form = $('#log_time_form').serialize();
  var HasError = 0;
  $('#log_time_form').find('input').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  $('#log_time_form').find('select').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  $('#log_time_form').find('textarea').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  if (HasError == 1) {
    Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
    });
  } else {
      var url = "../../../assets/app_ajax/time_attendance/log_time/log_time.php"; // PHP save page
      $.ajax({
        type: "POST",
        url: url,
        data: form, // serializes the form's elements.
        success: function(data) {
          if ($.trim(data) == 'ok') {
            //appRed(s[1])
            Messenger().post({
              message: 'Time & Attendance Logged.',
              type: 'info'
            });

             $("#TimeExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
            $('#TimeExpenseModal').modal('hide');

            ///////////my time page/////////////
            // setTimeout(function(){
            //   $("#MyTimeandExpenses").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
            //     $dateto= $('#date-to').val();
            //     $datefrom=$('#date-from').val();
            //     $.ajax({
            //          type: "POST",
            //          url: "../../../assets/app_forms/time_attendance/MyTimeAndExpensesPage.php?start="+$datefrom+"&end="+$dateto,
            //          data: "id:1",
            //          success: function(msg){
            //            $("#MyTimeandExpenses").delay(1000)
            //            .queue(function(n) {
            //                 $(this).html(msg);
            //                 n();
            //             })
            //          }
            //     });
            //   }, 1000);
            ///////////my time page/////////////
          } else {
            Messenger().post({
              message: 'An Error Has occured.',
              type: 'error',
              showCloseButton: false
            });
          }
        }
      });
  }
  $(this).removeAttr("disabled");
});
/////////////////////////////////////////////////////////
//////////////////////Expenses///////////////////////////
/////////////////////////////////////////////////////////

$("body").on("click","#addExpense",function(){
  $("#ExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
  $('#ExpenseModal').modal('show');
   $.ajax({
     type: "POST",
     url: "../../../assets/app_forms/time_attendance/AddExpenses.php",
     data: {id:$tedid},
     success: function(msg){
       //alert(msg);
       $("#ExpenseModalContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();

            $('#client_expense').chosen();
            $('#project_expense').chosen();

            $tedid_type = $tedid.substring(0, 7);
            if($tedid_type =='id_chx_' || $tedid_type=='id_phx_' || $tedid_type =='id_pros'){}else{
            $('#client_expense').parent().parent().hide();
            $('#project_expense').parent().parent().hide();
          }

            $('#expense_date').bootstrapMaterialDatePicker({ weekStart : 1, time: false, format : 'DD-MM-YYYY'});
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
});
     }
});
});

$("body").on("change","#expense",function(){
 var option = $(this).find(':selected').data('id');

 if(option == 'Mileage'){
  $('.mileage_holder').show();
  $('.cost_holder').hide();

  $('#mileage').prop('required', true);
  $('#cost').removeAttr('required');
 }else{
  $('.mileage_holder').hide();
  $('.cost_holder').show();

  $('#cost').prop('required', true);
  $('#mileage').removeAttr('required');
 }
});

$("body").on("change","#client_expense",function(){
  var id = $(this).val();
 if(id!='N/A'){
  $.ajax({
    url: '../../../assets/app_ajax/time_attendance/get_project.php?id='+id,
    type: 'GET',
    dataType: "json",
    beforeSend: function() {
      $("#project_expense").empty();
    },
    data: {id: id},
    success: function(msg) {
      $('#project_expense').append('<option value="">Select</option><option value="N/A">Not Related to Project</option>');
      $.map(msg, function(item) {
        $('#project_expense').append('<option value="'+item.id+'">RUR'+item.job_number+' - '+item.address1+' '+item.postcode+'</option>');

         });
         $('#project_expense').removeAttr('disabled');
        $('#project_expense').trigger('chosen:updated');
    }
  });
}else{
  $('#project_expense').removeAttr('disabled');
    $("#project_expense").empty();
    $('#project_expense').append('<option value="">Select</option><option value="N/A">Not Related to Project</option>');

    $('#project_expense').trigger('chosen:updated');
  }
});


function UpdatePrice($price, $Quantity, $vat) {
	$total_cost= $price*$Quantity;
  $total1p=($price/100)
	$total_vat=parseFloat($total1p*$vat).toFixed(2)*$Quantity;

  $total_cost = parseFloat($total_cost).toFixed(2);
	$total_vat = parseFloat($total_vat).toFixed(2);

  $total= +$total_cost + +$total_vat;
  $total=parseFloat($total).toFixed(2);


  $current_row=$(".current_row");
  $cost=$($current_row).find(".cost");
  $vat_cost=$($current_row).find(".vat_cost");
  $row_total=$($current_row).find(".row_total");

	$($cost).val($total_cost);
	$($vat_cost).val($total_vat);
  $($row_total).val($total);
		return 'ok';
}

function UpdateTotal() {
$total=0;
$total_vat=0;
$total_cost=0;
  $(".row_total").each(function() {
    $row_total=parseFloat($(this).val()).toFixed(2);
    $total= +$total+ +$row_total;
  });
  $(".vat_cost").each(function() {
    $vat_total=parseFloat($(this).val()).toFixed(2);
    $total_vat= +$total_vat+ +$vat_total;
  });
  $(".cost").each(function() {
    $cost_total=parseFloat($(this).val()).toFixed(2);
    $total_cost= +$total_cost+ +$cost_total;
  });

  $total=parseFloat($total).toFixed(2);
  $total_vat=parseFloat($total_vat).toFixed(2);
  $total_cost=parseFloat($total_cost).toFixed(2);
  $('.total_total').val($total);
  $('.vat_total').val($total_vat);
  $('.cost_total').val($total_cost);

  return 'ok';
}

$("body").on("change", ".units, .units_cost, .vat", function() {
  $(".current_row").removeClass("current_row");
  $(this).parent().parent().parent().addClass("current_row");
  $current_row=$(".current_row");
  $units=$($current_row).find(".units");
  $units_cost=$($current_row).find(".units_cost");
  $vat=$($current_row).find(".vat");

  $price=$($units_cost).val();
  $Quantity=$($units).val();
  $vat=$($vat).val();
  UpdatePrice($price, $Quantity, $vat);
  UpdateTotal();
});

$("body").on("change", ".vat_cost", function() {
  $current_row=$(".current_row");

  $cost=$($current_row).find(".cost").val();
  $vat_cost=$($current_row).find(".vat_cost").val();
  $row_total=$($current_row).find(".row_total");
  $total= +$cost + +$vat_cost;
  $total=parseFloat($total).toFixed(2);
  $vat_cost=parseFloat($vat_cost).toFixed(2);
  $(this).val($vat_cost);
  $($row_total).val($total);
  UpdateTotal();
});

$("body").on("change", "#chargeable_type", function() {

  if($(this).val()=='Clients'){
    $('#client_expense_chargeable').parent().parent().show();
    $('#client_expense_chargeable').prop('required', true);

    $('#client_expense').parent().parent().show();
    $('#client_expense').prop('required', true);
    $('#client_expense').find('option:eq(0)').prop('selected', true);
    $('#client_expense').trigger('chosen:updated');

    $('#project_expense').parent().parent().show();
    $('#project_expense').prop('required', true);
    $('#project_expense').find('option:eq(0)').prop('selected', true);
    $('#project_expense').trigger('chosen:updated');
  }else{
    $('#client_expense_chargeable').parent().parent().hide();
    $('#client_expense_chargeable').find('option:eq(0)').prop('selected', true);
    $('#client_expense_chargeable').prop('required', false);

    $('#client_expense').parent().parent().hide();
    $('#client_expense').prop('required', false);
    $('#client_expense').find('option:eq(0)').prop('selected', true);
    $('#client_expense').trigger('chosen:updated');

    $('#project_expense').parent().parent().hide();
    $('#project_expense').prop('required', false);
    $('#project_expense').find('option:eq(0)').prop('selected', true);
    $('#project_expense').trigger('chosen:updated');
  }
});

$("body").on("click","#save_expense_form",function(){
  var form = $('#expense_form');

  var HasError = 0;
  $(form).find('input').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  $(form).find('select').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  $(form).find('textarea').each(function(){
        $(this).parent().removeClass('has-error');
        Messenger().hideAll();
        if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
              HasError = 1;
                    $(this).parent().addClass('has-error');
              }
        }
  });
  if (HasError == 1) {
    Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
    });
  } else {
    $(form).find('.expense').each(function() {$(this).attr("disabled", false);});
    var formdata = $(form).serialize();
      var url = "../../../assets/app_ajax/time_attendance/expense/index/log_expenses.php"; // PHP save page
      $.ajax({
        type: "POST",
        url: url,
        data: formdata, // serializes the form's elements.
        success: function(data) {
          if (data == 'ok' || data == " ok") {
            //appRed(s[1])
            Messenger().post({
              message: 'Expenses Logged.',
              type: 'info'
            });

             $("#ExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
             $('#ExpenseModal').modal('hide');
          } else {
            Messenger().post({
              message: 'An Error Has occured.',
              type: 'error',
              showCloseButton: false
            });
          }
        }
      });
  }
});



$("body").on("click","#Add_ExpensesRequest_Row",function(){
    $('.bottom').before('<div class="table_rows current_row_new"> <div class="table_column column1"> <div class="form-group label-floating is-empty"> <label for="description_ExpensesRequest[]" class="control-label">Description</label> <textarea class="form-control description_ExpensesRequest" rows="6" name="description_ExpensesRequest[]" required></textarea> </div></div> <div class="table_column column2"> <div class="form-group is-empty"><label class="control-label" for="expense[]">Type: </label> <select class="form-control expense" name="expense[]" required>'+$TypeList+' </select> <span class="material-input"></span> </div> </div> <div class="table_column column3"> <div class="form-group label-floating cost_holder"> <label for="units" class="control-label">Units</label> <input type="number" step="1" class="form-control units" name="units[]" value="1" required><span class="material-input"></span> </div> </div> <div class="table_column column4"> <div class="form-group label-floating cost_holder"> <label for="units_cost" class="control-label">Units Cost (£)</label> <input type="number" step="0.01" class="form-control units_cost" name="units_cost[]" value="0" required><span class="material-input"></span> </div> </div> <div class="table_column column5"> <div class="form-group label-floating cost_holder"> <label for="vat" class="control-label">Vat Rate(%)</label> <input type="number" step="0.5" class="form-control vat" name="vat[]" list="vatlist" value="0" required><span class="material-input"></span> </div> </div> <div class="table_column column6"> <div class="form-group label-floating cost_holder"> <label for="cost" class="control-label">Cost (£)</label> <input type="number" step="0.01" class="form-control cost" name="cost[]" value="0" readonly required><span class="material-input"></span> </div> </div> <div class="table_column column7"> <div class="form-group label-floating cost_holder"> <label for="vat_cost" class="control-label">Vat</label> <input type="number" step="0.01" class="form-control vat_cost" name="vat_cost[]" value="0" required><span class="material-input"></span> </div> </div> <div class="table_column column8"> <div class="form-group label-floating cost_holder"> <label for="row_total" class="control-label">Total</label> <input type="number" class="form-control row_total" name="row_total[]" value="0" readonly required><span class="material-input"></span> </div> </div> <div class="table_column column9"> <button class="btn btn-raised btn-danger pull-right RemoveExpensesRequestRow" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> </div>');
    setTimeout(function(){
      $(".current_row_new").removeClass("current_row_new");
    }, 4000);
});


$("body").on("click",".RemoveExpensesRequestRow",function(){
  $(this).parent().parent().remove();
  UpdateTotal();
  });


  /////////////////////////////////////////////////////////
  //////////////////////Disbursement///////////////////////
  /////////////////////////////////////////////////////////

  $("body").on("click","#addDisbursement",function(){
    $("#ExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
    $('#ExpenseModal').modal('show');
     $.ajax({
       type: "POST",
       url: "../../../assets/app_forms/time_attendance/AddDisbursement.php",
       data: {id:$tedid},
       success: function(msg){
         //alert(msg);
         $("#ExpenseModalContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();

              $('#client_expense').chosen();
              $('#project_expense').chosen();
              $('#third_party').chosen();

              $('#third_party').parent().parent().hide();


              $('#expense_date').bootstrapMaterialDatePicker({ weekStart : 1, time: false, format : 'DD-MM-YYYY'});
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
  });


       }
  });
  });

  $("body").on("change", "#disbursement_type", function() {
    if($(this).val()=='Third Party Fee'){
      $('#third_party').parent().parent().show();
      $('#third_party').prop('required', true);
      $('#third_party').find('option:eq(0)').prop('selected', true);
      $('#third_party').trigger('chosen:updated');

      $('#local_authority_disbursement').parent().parent().hide();
      $('#local_authority_disbursement').prop('required', false);
      $('#local_authority_disbursement').val('');

    }else{
      $('#third_party').parent().parent().hide();
      $('#third_party').prop('required', false);
      $('#third_party').find('option:eq(0)').prop('selected', true);
      $('#third_party').trigger('chosen:updated');

      $('#local_authority_disbursement').parent().parent().show();
      $('#local_authority_disbursement').prop('required', true);
      $('#local_authority_disbursement').val('');
    }
  });

  $("body").on("click","#Add_DisbursementRequest_Row",function(){
      $('.bottom').before('<div class="table_rows current_row_new"> <div class="table_column column1"> <div class="form-group label-floating is-empty"> <label for="description_ExpensesRequest[]" class="control-label">Description</label> <textarea class="form-control description_ExpensesRequest" rows="6" name="description_ExpensesRequest[]" required></textarea> </div></div><div class="table_column column3"> <div class="form-group label-floating cost_holder"> <label for="units" class="control-label">Units</label> <input type="number" step="1" class="form-control units" name="units[]" value="1" required><span class="material-input"></span> </div> </div> <div class="table_column column4"> <div class="form-group label-floating cost_holder"> <label for="units_cost" class="control-label">Units Cost (£)</label> <input type="number" step="0.01" class="form-control units_cost" name="units_cost[]" value="0" required><span class="material-input"></span> </div> </div> <div class="table_column column5"> <div class="form-group label-floating cost_holder"> <label for="vat" class="control-label">Vat Rate(%)</label> <input type="number" step="0.5" class="form-control vat" name="vat[]" list="vatlist" value="0" required><span class="material-input"></span> </div> </div> <div class="table_column column6"> <div class="form-group label-floating cost_holder"> <label for="cost" class="control-label">Cost (£)</label> <input type="number" step="0.01" class="form-control cost" name="cost[]" value="0" readonly required><span class="material-input"></span> </div> </div> <div class="table_column column7"> <div class="form-group label-floating cost_holder"> <label for="vat_cost" class="control-label">Vat</label> <input type="number" step="0.01" class="form-control vat_cost" name="vat_cost[]" value="0" required><span class="material-input"></span> </div> </div> <div class="table_column column8"> <div class="form-group label-floating cost_holder"> <label for="row_total" class="control-label">Total</label> <input type="number" class="form-control row_total" name="row_total[]" value="0" readonly required><span class="material-input"></span> </div> </div> <div class="table_column column9"> <button class="btn btn-raised btn-danger pull-right RemoveExpensesRequestRow" style="padding: 0 2px;"><span class=" glyphicons glyphicons-bin" aria-hidden="true"></span></button> </div> </div>');
      setTimeout(function(){
        $(".current_row_new").removeClass("current_row_new");
      }, 4000);

  });
  $("body").on("click",".RemoveDisbursementRequestRow",function(){
    $(this).parent().parent().remove();
    });


$("body").on("click","#save_disbursement_form",function(){
  var form = $('#disbursement_form');

  var HasError = 0;
  $(form).find('input').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  $(form).find('select').each(function() {
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();
    if (!$.trim(this.value).length) { // zero-length string AFTER a trim
      if (!$(this).prop('required')) {
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
  });
  $(form).find('textarea').each(function(){
        $(this).parent().removeClass('has-error');
        Messenger().hideAll();
        if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
              HasError = 1;
                    $(this).parent().addClass('has-error');
              }
        }
  });
  if (HasError == 1) {
    Messenger().post({
      message: 'Please make sure all required elements of the form are filled out.',
      type: 'error',
      showCloseButton: false
    });
  } else {
    var formdata = $(form).serialize();
      var url = "../../../assets/app_ajax/time_attendance/disbursement/index/log_disbursement.php"; // PHP save page
      $.ajax({
        type: "POST",
        url: url,
        data: formdata, // serializes the form's elements.
        success: function(data) {
          if (data == 'ok' || data == " ok") {
            //appRed(s[1])
            Messenger().post({
              message: 'Disbursement Logged.',
              type: 'info'
            });

             $("#ExpenseModalContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
             $('#ExpenseModal').modal('hide');
             ///////////my time page/////////////
            // setTimeout(function(){
            //   $("#MyTimeandExpenses").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading...</div>');
            //     $dateto= $('#date-to').val();
            //     $datefrom=$('#date-from').val();
            //     $.ajax({
            //          type: "POST",
            //          url: "../../../assets/app_forms/time_attendance/MyTimeAndExpensesPage.php?start="+$datefrom+"&end="+$dateto,
            //          data: "id:1",
            //          success: function(msg){
            //            $("#MyTimeandExpenses").delay(1000)
            //            .queue(function(n) {
            //                 $(this).html(msg);
            //                 n();
            //             })
            //          }
            //     });
            //   }, 1000);
            ///////////my time page/////////////
          } else {
            Messenger().post({
              message: 'An Error Has occured.',
              type: 'error',
              showCloseButton: false
            });
          }
        }
      });
  }
});
