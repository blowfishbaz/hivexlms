


$( "body" ).on( "click", "#add_new_user", function(e) {
            $("#BaseModalLContent").html($Loading);
            $('#BaseModalL').modal('show');
            $.ajax({
                type: "POST",
                url: $fullurl+"assets/app_forms/admin/new_users.php",
                data: "id:1",
                success: function(msg){

                   //alert(msg);
                   $("#BaseModalLContent").delay(1000)
                    .queue(function(n) {
                        $(this).html(msg);
                        n();
                    }).fadeIn("slow").queue(function(n) {
                                     $.material.init();
                                    n();
                    });
                }
          });
});

$( "body" ).on( "click", "#save_new_account", function() {
                  var HasError = 0;
                  var formid = '#create_new_user';
                  var FRMdata = $(formid).serialize(); // get form data
                  $pass1=$('#newpassword').val();
                  $pass2=$('#confirmnewpassword').val();

                    $(formid).find('input').each(function(){
                      $(this).parent().removeClass('has-error');
                      Messenger().hideAll();
                      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                               if(!$(this).prop('required')){}
                               else {
                            HasError = 1;
                            $(this).parent().addClass('has-error');
                          }
                        }
                    });
                    if (HasError == 1) {
                      Messenger().post({
                          message: 'Please make sure all required elements of the form are filled out.',
                          type: 'error',
                          showCloseButton: false
                      });
                    }
                    else{
                      if ($pass1==$pass2 && $.trim($pass1).length) {
                        //alert('save');
                     $.ajax({
                             type: "POST",
                             url: $fullurl+"admin/rota/ajax/save_new_account.php",
                             data: FRMdata, // serializes the form's elements.
                             success: function(msg){
                                 $message=$.trim(msg);
                                 Messenger().post({
                                         message: 'New User Created.',
                                         showCloseButton: false
                                 });
                                 setTimeout(function(){
                                   $('#BaseModalL').modal('hide');
                                   $('#table').bootstrapTable('refresh');
                                 },600);

                              },error: function (xhr, status, errorThrown) {
                             setTimeout(function(){alert('Error');},300);
                           }
                     });
                      }
                      else{
                         Messenger().post({
                          message: 'Password Error',
                          type: 'error',
                          showCloseButton: false
                   });
                      }
                  }
            });


$( "body" ).on( "click", "#PasswordBut", function() {
  var id = $(this).data('id');
  $("#BaseModalLContent").html($Loading);
    $.ajax({
    type: "POST",
    url: "../../../assets/app_forms/admin/password.php?id="+id,
    success: function(msg){
      $("#BaseModalLContent").delay(1000)
       .queue(function(n) {
           $(this).html(msg);
           n();
       }).fadeIn("slow").queue(function(n) {
                    $.material.init();
                   n();
                  });
              }
          });
});


$( "body" ).on( "click", "#saveNewPassword", function() {
   $pass1=$('#newpassword').val();
   $pass2=$('#confirmnewpassword').val();

        if ($pass1==$pass2 && $.trim($pass1).length) {
            var formid = '#editpasswordForm';
            var FRMdata = $(formid).serialize(); // get form data
            var url = "../../../assets/app_ajax/admin/update_user_password.php"; // PHP save page
            $.ajax({
                    type: "POST",
                    url: url,
                    data: FRMdata, // serializes the form's elements.
                    success: function(data){
                            if (data == ' ok') {
                               $("#BaseModalLContent").html($Loading);
                                    Messenger().post({
                                           message: 'User Updated.',
                                           showCloseButton: false
                                    });
                                    setTimeout(function(){
                                       $('#BaseModalL').modal('hide');
                                     $('#table').bootstrapTable('refresh');
                                                //location.reload();
                                    },1000);
                            } else {
                            Messenger().post({
                                    message: 'An Error Has occured.',
                                    type: 'error',
                                    showCloseButton: false
                            });
                       }
                    }
            });
        }else{
           Messenger().post({
            message: 'Password Error',
            type: 'error',
            showCloseButton: false
     });
        }
});
