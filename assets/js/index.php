<!--
/**
 * 
	*  __    __   __  ____    ____  _______ ___   ___ 
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  / 
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /  
	* |   __   | |  |   \      /   |   __|    >   <   
	* |  |  |  | |  |    \    /    |  |____  /  .  \  
	* |__|  |__| |__|     \__/     |_______|/__/ \__\   
 *
 * @copyright  1997-2016 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 --->
 
 
<?php

/**
 * 
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___ 
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  / 
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /  
	* |   __   | |  |   \      /   |   __|    >   <   
	* |  |  |  | |  |    \    /    |  |____  /  .  \  
	* |__|  |__| |__|     \__/     |_______|/__/ \__\                                       
 *
 *						BMS System
 *
 *
 *				   	
 * @Filename    application.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */
 
 
 
 // VARIABLES
 
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
 
 header('Location: '.$fullurl);
 
 ?>