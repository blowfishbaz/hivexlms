 <?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    application.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 session_start();

 // VARIABLES

 $fullurl = 'http://'.$_SERVER['HTTP_HOST'].'/';
 $fullpath = $_SERVER['DOCUMENT_ROOT'].'/';

 $url = $_SERVER['SCRIPT_NAME'];
 $parts = explode('/', $url);

 //Constants

 	//Application Version
	define('version', '1.2');

	//How Long Will Persitence Last Default 30 days set it minutes.
	define("CookieLength", 43200); //


 // Load Composer Elements
 //require $fullpath.'vendor/autoload.php';

 //Load Encryption Functions
  include ($fullpath."assets/app_php/crypt.php");


 //Load DB Class
 include ($fullpath."assets/app_classes/bftdb.class.php");

 $db = new database;

 //Load Form Class
  include ($fullpath."assets/app_classes/bftform.class.php");

  $form = new form;

  // Load Functions
   include ($fullpath."assets/app_php/web_functions.php");


   if(decrypt($_SESSION['SESS_ACCOUNT_ID']) == 'id_member_20161103_110736904700_82639' || decrypt($_SESSION['SESS_ACCOUNT_ID']) == 'id_att_20201203_092323085600_19124'){

   }else{
     //header("Location: http://www.skyrandd.co.uk/landing.php");
     //exit();
   }


if($parts['3']=='auth.php' || $parts['4']=='auth.php' || $parts['4']=='endpoint.php' || $parts['3']=='endpoint.php'){}
  else{
   if(isset($_SESSION['SESS_ACCOUNT_ID'])){
   checklogin(decrypt($_SESSION['SESS_ACCOUNT_ID']));
   }
   else{
        checkCookie();
   }

 }
 ?>
