<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/

ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 //Page Title
 $page_title = 'Register';

 $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);
	$id = $uid;
 $db = new database;
  $db->query("select * from ws_accounts where id = ? ");
  $db->bind(1,$uid);
  $user = $db->single();



	$db->query("select * from ws_addresses where pid = ? ");
  $db->bind(1,$uid);
  $user_address = $db->single();

	function get_my_company($id){
		$db = new database;
		$db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
		$db->bind(1,$id);
		$me = $db->single();

		return $me['company_id'];
	}

 $company_id = get_my_company(decrypt($_SESSION['SESS_ACCOUNT_ID']));



 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">
<style>
.nav-tabs {
  background-color: #5487b2;
}
</style>



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>HiveX LMS</title>


<style>



/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style='  background-color: #5bc3ff;'>
		<div class="col-lg-2 col-md-2"></div>
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="page_spacer"></div>

				<h2 class="text-center media-heading color"><?echo $user['first_name'].' '.$user['surname'];?></h2>

					<div class="page_spacer"></div>
			</div>
	</div>

	<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">


			<div class="row">
			<ul class="nav nav-tabs" role="tablist">
						<li role="presentation active"><a href="<? echo $fullurl; ?>/profile_tabs/dashboard.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" >Overview</a></li>
						<li role="presentation"><a href="<? echo $fullurl; ?>profile_tabs/provisions.php?id=<? echo $id; ?>" data-target="#contactstab" id="maintabx" role="tab" data-toggle="tabajax">Provisions</a></li>
						<li role="presentation"><a href="<? echo $fullurl; ?>profile_tabs/learners.php?id=<? echo $id; ?>" data-target="#contactstab" id="maintabx" role="tab" data-toggle="tabajax">Learners</a></li>
						<li role="presentation"><a href="<? echo $fullurl; ?>profile_tabs/hierarchy.php?id=<? echo $id; ?>" data-target="#contactstab" id="contacttabbutton" role="tab" data-toggle="tabajax">Hierarchy View</a></li>
						<li role="presentation"><a href="<? echo $fullurl; ?>profile_tabs/training_matrix.php?id=<? echo $id; ?>" data-target="#contactstab" id="contacttabbutton" role="tab" data-toggle="tabajax">Training Matrix</a></li>
						<li role="presentation"><a href="<? echo $fullurl; ?>profile_tabs/training_matrix.php?id=<? echo $id; ?>" data-target="#contactstab" id="contacttabbutton" role="tab" data-toggle="tabajax">Reports</a></li>
						<li role="presentation"><a href="<? echo $fullurl; ?>profile_tabs/training_matrix.php?id=<? echo $id; ?>" data-target="#contactstab" id="contacttabbutton" role="tab" data-toggle="tabajax">Your Courses</a></li>
						
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="contactstab"></div>
				<div role="tabpanel" class="tab-pane" id="addressestab"></div>
				<div role="tabpanel" class="tab-pane" id="notestab"></div>
				<div role="tabpanel" class="tab-pane" id="taskstab"></div>
				<!--<div role="tabpanel" class="tab-pane" id="casestab"></div>-->
				<div role="tabpanel" class="tab-pane" id="salesstab"></div>
				<div role="tabpanel" class="tab-pane" id="quotestab"></div>
				<div role="tabpanel" class="tab-pane" id="orderstab"></div>
				<div role="tabpanel" class="tab-pane" id="calendartab"></div>
				<div role="tabpanel" class="tab-pane" id="reportstab"></div>
				<div role="tabpanel" class="tab-pane" id="socialtab"></div>
			</div>








		</div>













			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="page_spacer"></div>


				<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">





			</div>

		</div>
		<div class="page_spacer"></div>
	</div>
	</div>
	<div class="clearfix"></div>
<div class="clearfix"></div>
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/footer.php'); ?>
</div>
</div>
</div><!--wrapper-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>

<script src="<? echo $fullurl; ?>assets/js/profile.js"></script>

<script>
var company_id = '<?echo $company_id;?>';
setTimeout(function(){
$('.cca_holder').empty();
var $this = $('#maintab'),
	loadurl = $this.attr('href'),
	targ = $this.attr('data-target');
	 $.get(loadurl, function(data) {
				$(targ).html(data);
	 });
	 $this.tab('show');
	 },100);

$('#myTabs a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})


$('[data-toggle="tabajax"]').click(function(e) {
		var $this = $(this),
				loadurl = $this.attr('href'),
				targ = $this.attr('data-target');

		$.get(loadurl, function(data) {
				$(targ).html(data);
		});
//console.log("show");
		$this.tab('show');
		return false;
});

</script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_web_modals.php'); ?>
</body>

</html>
