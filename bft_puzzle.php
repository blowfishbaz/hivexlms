<?php
$green_count = 1;
for($i = 1; $i <= 100; $i++){
    $return = 'None';
    if($i % 15 == 0) { $return = 'bft'; }
    elseif ($i % 5 == 0) {
      if($green_count == 3){ $green_count = 1; $color = 'green'; }
      else{ $green_count++; $color = 'purple'; }
      $return = 'fishfish';
    } elseif ($i % 3 == 0) {
      if($i % 2 == 0){ $color = 'red'; }
      else{ $color = 'orange'; }
        $return = 'Blowfish';
    }
    if($return == 'None' || $return == 'bft'){
        if($i <= 25){ $color = 'blue'; }
        else if($i <= 50){ $color = '#a0a01e'; }
        else if($i <= 75){ $color = 'brown'; }
        else if($i >= 76){ $color = 'gray'; }
    }
    echo $i.'. <span style="color:'.$color.';">'.$return.'</span><BR />';
}
