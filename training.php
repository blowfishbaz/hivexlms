<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/

ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 //Page Title
 $page_title = 'Home';


if(isset($_GET['ref'])&&isset($_GET['signup'])){

	$db->query("select * from bv_signup where random_string = ? and status = ?");
	$db->bind(1, $_GET['ref']);
  $db->bind(2, '1');
	$db->execute();
	$signup = $db->single();

	if($signup['id']!=''){
		header('Location: '.$fullurl.'registration.php?id='.$_GET['ref'].'');
	}

}

$db->query("select page_info from hx_pages where id = ? and status = ?");
$db->bind(1, '10');
$db->bind(2, '1');
$page = $db->single();
 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Sky Recruitment</title>


<style>
.roundbox {
    height: 150px;
    width: 150px;
		padding: 0 10px;
		box-sizing: border-box;
    border-radius: 50%;
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;
}

/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>





	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background1">
		<div class="col-lg-2 col-md-2"></div>
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="page_spacer"></div>
				<h1 class="text-center media-heading color">Training</h1>
				<div class="page_spacer"></div>
			</div>
	</div>

	<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="page_spacer"></div>

				<div class="row">
				<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 <h1 style=" margin-top: 0;">We offer <span class="color1">training</span> within the following areas</h1>
					<h4>These are just some of the training courses we can provide and we have access to many more!</h4>
			</div> -->

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<!-- <p>
					<? echo $page['page_info'];?>
				</p> -->
		</div>
			<div class="clearfix"></div>
			<div class="page_spacer"></div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="text-center background1 color roundbox">Mandatory Induction</div><div class="page_spacer2"></div></div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="text-center background1 color roundbox">Patient Handling</div><div class="page_spacer2"></div></div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="text-center background1 color roundbox">Safeguarding of Vulnerable Adults</div><div class="page_spacer2"></div></div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="text-center background1 color roundbox">Health and Safety in the Care Home</div><div class="page_spacer2"></div></div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="text-center background1 color roundbox">Infection Controland Food Hygiene</div><div class="page_spacer2"></div></div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="text-center background1 color roundbox">Emergency First Aid Awareness</div><div class="page_spacer2"></div></div>
		</div>
			<div class="clearfix"></div>
			<div class="page_spacer"></div>


	</div>

		</div>
		<div class="page_spacer"></div>
	</div>


<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


	<div class="clearfix"></div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background4">
		<div class="page_spacer"></div>
		<div class="col-lg-1 col-md-1"></div>
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pager color">
			<h2>HAVE A QUESTION?</h2>
			<p>We are here to help. Call us on 0151 909 2616 or email us on admin@skyrandd.co.uk</p>
			<a href="<? echo $fullurl;?>contact.php" style="margin-top:15px" type="button" class="btn btn-lg btn-info">Contact Us</a>
		</div>
		<div class="clearfix"></div>
		<div class="page_spacer"></div>
	</div>

<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->



<div class="clearfix"></div>
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/footer.php'); ?>
</div>
</div>
</div><!--wrapper-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');
?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_web_modals.php'); ?>
</body>

</html>
