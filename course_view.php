<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 //Page Title
 $page_title = 'Home';


	$id = decrypt($_SESSION['SESS_ACCOUNT_ID']);
	$booked_id = $_GET['course_id'];

	$db->Query("select * from ws_booked_on where id = ?");
	$db->bind(1,$booked_id);
	$data = $db->single();

	$course_id = $data['course_id'];

	$completed = $_GET['completed'];


	$db->Query("select * from ws_courses_content where coures_id = ? and status = 1");
	$db->bind(1,$course_id);
	$course_data = $db->single();

	$db->Query("select * from ws_accounts where id = ?");
	$db->bind(1,$id);
	$my_account = $db->single();

 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Sky Recruitment</title>


<style>

/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>



	<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="page_spacer"></div>


				<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<div>

							<!-- How are we storing this information / where is it going to go? -->

							<?if(isset($completed)){?>
								<h3>Course Completed</h3>
							<?}else{?>
								<?
									$course_url = $fullurl.$course_data['launch_path'].$course_data['launch_name'];
									$activity_id = $course_data['activity_id'];
									$activity_id_encode = urlencode($activity_id);
									$actor_mailbox = $my_account['email'];
									$actor_name = $my_account['first_name'].' '.$my_account['surname'];
									$actor_final = '{"mbox":"mailto:'.$actor_mailbox.'","name":"'.$actor_name.'","objectType":"Agent"}';
									$actor_encode = urlencode($actor_final);
									$final_url = "".$course_url."?actor=".$actor_encode."&amp;auth=Basic%20MTQtZDVlMzBhZjc1ODZmOWJiOjFkZjk3Nzc0YmFjYjk4OGU3YTVhMDEwZmU%3D&amp;endpoint=http%3A%2F%2F45.152.254.215%2Fxapi%2Fendpoint.php&amp;registration=03e6b2c5-bb92-4289-9f00-87a70e9aff23&amp;activity_id=".$activity_id_encode."";
								?>


								<?if(empty($course_url) || empty($activity_id) || empty($actor_mailbox) || empty($actor_name)){?>
									<h3>Error Please Try Again</h3>
								<?}else{?>
									<iframe class="grassblade_iframe"
									frameborder="0"
									data-src="<?echo $final_url;?>"
									style="margin: 0px; position: relative; height: 516px;" width="100%" height="516"
									webkitallowfullscreen=""
									mozallowfullscreen=""
									allowfullscreen=""
									allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
									src="<?echo $final_url;?>&amp;h=516&amp;w=686&amp;course_id=<?echo $course_id;?>&amp;user_id=<?echo $id;?>"></iframe>
								<?}?>

							<?}?>



					</div>


			</div>

		</div>
		<div class="page_spacer"></div>
	</div>
	<div class="clearfix"></div>

<?if(!isset($completed)){?>
	<div class="row" style="background-color: #54c4ff;">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="page_spacer"></div>

			<button type="button" class="btn" style="width: 100%; background-color: white; border-radius: 0px;" id="mark_as_complete">Mark as Complete</button>

			<div class="page_spacer"></div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
		<div class="clearfix"></div>
	</div>
<?}?>

	</div>
	<div class="clearfix"></div>
<div class="clearfix"></div>
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/footer.php'); ?>
</div>
</div>
</div><!--wrapper-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>

<script>
	var course_id = '<?echo $course_id;?>';
	var account_id = '<?echo $id;?>';
	var booked_id = '<?echo $booked_id;?>';

	$( "body" ).on( "click", "#mark_as_complete", function() {
		mark_online_course_complete(booked_id);
	});

	function mark_online_course_complete(booked_id){

		$.ajax({
							type: "POST",
							url: "../../../xapi/mark_as_complete.php?id="+booked_id,
							success: function(msg){
								console.log(msg);
									if($.trim(msg) == 'ok'){
									setTimeout(function(){
											window.location.href="course_view.php?course_id="+booked_id+'&completed';
											}, 2000);


									}else{
											alert('Error - Please try again.');
									}
							}
					});

	}

	<?if(isset($completed)){?>
		jQuery(document).ready(function($) {
			$.ajax({
								type: "POST",
								url: "../../../admin/pdfs/certificate_single.php?id="+booked_id,
								success: function(msg){
									console.log('Certificate');
								}
						});
		});

	<?}?>

</script>
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_web_modals.php'); ?>
</body>

</html>
