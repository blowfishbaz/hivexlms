<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    order/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 if($_SESSION['SESS_ACCOUNT_Type'] != 'admin'){
    // header('Location: /index.php');
    // exit;
    //echo 'not admin';
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>
    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>
<!-- On Page CSS -->
</head>
<body>

   <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>
                        <div class="clearfix">
                           <!--  -->
                        </div>
                        <div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-tick" aria-hidden="true"></span> Approvals</div>
            				<!--- PAGE CONTENT HERE ---->
                        <div class="panel panel-default">
                           <div class="panel-body">
                              <table id="table" class="striped"
                              data-toggle="table"
                              data-show-export="true"
                              data-click-to-select="true"
                              data-filter-control="true"
                              data-show-columns="true"
                              data-show-refresh="true"

                              data-url="../../../assets/app_ajax/admin/index/approval.php"
                              data-height="400"
                              data-side-pagination="server"
                              data-pagination="true"
                              data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                              data-search="true"
                              data-page-size="100"
                               >
                               <!--
                               es - Escort
                               us - User
                               ag - Agent
                             -->
                                  <thead>
                                       <tr>
                                          <th data-field="app_id" data-sortable="true" data-visible="false">ID</th>
                                          <th data-field="screenname" data-sortable="true" data-visible="true" data-formatter="link">Account Name</th>
                                          <th data-field="ac_code" data-sortable="true" data-visible="true" data-formatter="account_code">Account Code</th>
                                          <th data-field="type" data-sortable="true" data-visible="true" data-formatter="jsUcfirst">Type</th>
                                          <th data-field="created_date" data-sortable="true" data-visible="true" data-formatter="date_time" >Created Date</th>
                                       </tr>
                                  </thead>
                               </table>
                           </div>
                        </div>
            				<!--- END OF PAGE CONTENT -->
                </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
<script src="<? echo $fullurl ?>assets/app_scripts/admin/approcal/index.js"></script>
<script>

</script>
