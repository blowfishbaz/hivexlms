<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Jobs';

 $db = new database;
 $db->query("select * from hx_jobs where id = ? ");
 $db->bind(1,$_GET['id']);
 $job = $db->single();

 $db->query("SELECT * FROM ws_job_applications where pid = ? and (status = ? OR status = ? OR status = ?) ORDER BY status DESC");
 $db->bind(1,$job['id']);
 $db->bind(2,'1');
 $db->bind(3,'2');
 $db->bind(4,'3');
 $applications = $db->ResultSet();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>

<!-- On Page CSS -->

</head>

<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>

                        <?switch ($job['status']) {
                            case '0':$status='<span class="label label-danger">Closed</span>';break;
                            case '1':$status='<span class="label label-success">Publish</span>';break;
                            case '2':$status='<span class="label label-warning">Unpublish</span>';break;
                            default:$status='';break;
                        }?>
            				<!--- PAGE CONTENT HERE ---->
						<div class="page_title text-capitalize">
              <span class="menuglyph glyphicons glyphicons-building" aria-hidden="true"></span><span class="menuText">Job Applicants- SR<?echo $job['job_number'];?></span> <?echo $status;?>
              <?if($job['status']=='0'){?>
                    <button type="button" id="Open_Job" class="btn btn-success btn-raised pull-right" data-id="<?echo $job['id'];?>">Open Job</button>
                  <?}else{?>
                      <button type="button" id="Close_Job" class="btn btn-danger btn-raised pull-right" data-id="<?echo $job['id'];?>">Close Job</button>
                      <?}?>
          </div>
                        <div class="clearfix"></div>


                        <div class="panel panel-default userlist">
                               <div class="panel-body">
                                   <strong>Title</strong> <?echo $job['title'];?><br />
                                   <strong>Company</strong> <?echo $job['company'];?><br />
                                   <strong>Category</strong> <?echo ucwords($job['category']);?><br />
                                   <strong>County</strong> <?echo ucwords($job['county']);?><br />
                                   <strong>Job Type</strong> <?echo ucwords($job['job_type']);?><br />
                                   <strong>Contract Type</strong> <?echo ucwords($job['contract_type']);?><br />

                                   <?
                                   if($job['salary_type']=='per annum'){
                                        echo '<strong>Salary</strong> £'.number_format($job['per_annum']).' Per Annum<br />';
                                   }else if($job['salary_type']=='pro rata'){
                                        echo '<strong>Salary</strong> £'.number_format($job['per_annum']).' Per Rata<br />';
                                        echo '<strong>Salary</strong> £'.number_format($job['per_hourly']).' Per Hour<br />';
                                    }else if($job['salary_type']=='per hour'){
                                        echo '<strong>Salary</strong> £'.number_format($job['per_hourly'],2).' Per Hour<br />';
                                    }else{echo '<strong>Salary</strong> ERROR';}
                                    ?>
                                   <br /><strong>Overview</strong><br /><?echo $job['overview'];?><br />
                                   <strong>Duties</strong><br /><?echo $job['duties'];?><br />



                                   <?
                                       foreach ($applications as $key => $app) {


                                   $db = new database;
                                   $db->query("select * from ws_accounts where id = ? ");
                                   $db->bind(1,$app['user_id']);
                                   $user = $db->single();

                                   $db->query("select * from ws_accounts_info where pid = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'1');
                                  $info = $db->single();

                                  $db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'cv');
                                  $db->bind(3,'1');
                                  $cv = $db->single();

                                  $db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'bds');
                                  $db->bind(3,'1');
                                  $bds = $db->single();

                                  $db->query("select * from ws_addresses where pid = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'1');
                                  $addresses = $db->single();


                                  $db->query("select * from ws_accounts_qualifications where pid = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'1');
                                  $qualifications = $db->ResultSet();

                                  $db->query("select * from ws_accounts_employment where pid = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'1');
                                  $employment = $db->ResultSet();

                                  $db->query("select * from ws_accounts_references where pid = ? and status = ?");
                                  $db->bind(1,$user['id']);
                                  $db->bind(2,'1');
                                  $references = $db->ResultSet();
                                  ?>

                                  <div class="row">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3>User Information</h3></div>
                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                  <table class="table table-bordered table-striped">
                                              <tbody>

                                                  <tr>
                                                    <th>Name</th>
                                                    <td><?echo $user['first_name'].' '.$user['surname'];?></td>
                                                  </tr>
                                               <tr>
                                                 <th>Address1</th>
                                                 <td><?echo $addresses['address1'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>address2</th>
                                                 <td><?echo $addresses['address2'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>address3</th>
                                                 <td><?echo $addresses['address3'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>city</th>
                                                 <td><?echo $addresses['city'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>county</th>
                                                 <td><?echo $addresses['county'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>postcode</th>
                                                 <td><?echo $addresses['postcode'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>telephone</th>
                                                 <td><?echo $addresses['telephone'];?></td>
                                               </tr>
                                               <tr>
                                                 <th>mobile</th>
                                                 <td><?echo $addresses['mobile'];?></td>
                                               </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <table class="table table-bordered table-striped">
                                                <tbody>

                                                    <?if($info['infotype']=='talk'){?>
                                                      <tr>
                                                        <th>CV</th>
                                                        <td>Talk to our team</td>
                                                      </tr>
                                                      <?}else {?>
                                                    <tr>
                                                      <th>CV</th>
                                                      <td><a href="<?echo $fullurl.$cv['path'].$cv['name'];?>" target="_blank"><?echo $cv['name']?></a></td>
                                                    </tr>
                                                    <?}?>
                                                    <?if($bds['name']!=''){?>
                                                    <tr>
                                                      <th>DBS</th>
                                                      <td><a href="<?echo $fullurl.$bds['path'].$bds['name'];?>" target="_blank"><?echo $bds['name']?></a></td>
                                                    </tr>
                                                    <?}?>
                                                    <tr>
                                                      <th>Driving License</th>
                                                      <td><?echo $info['license'];?></td>
                                                    </tr>

                                                    <tr>
                                                      <th>Conviction</th>
                                                      <td><?echo $info['conviction'];?></td>
                                                    </tr>
                                                    <? if($info['conviction']=='yes'){?>
                                                    <tr>
                                                      <th>Conviction</th>
                                                      <td><?echo $info['conviction_info'];?></td>
                                                    </tr>
                                                   <? } ?>
                                                </tbody>
                                              </table>
                                          </div>
                                          <div class="clearfix"></div>
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                               <table class="table table-bordered table-striped">
                                                           <tbody>
                                                               <tr>
                                                                 <th>Applicants Status</th>
                                                                 <td><? if($app['status']=='1'){echo '<span class="label label-info">Waiting</span>';}//
                                                                      else if($app['status']=='2'){echo '<span class="label label-info">Maybe</span>';}//
                                                                      else if($app['status']=='3'){echo '<span class="label label-success">Successful</span>';}//
                                                                      else if($app['status']=='0'){echo '<span class="label label-danger">Unsuccessful</span>';}//
                                                                      else{}
                                                                 ?></td>
                                                                 <th>References Status</th>
                                                                 <td><? if($app['reference']=='1'){echo '<span class="label label-info">Waiting</span>';}
                                                                      else if($app['reference']=='2'){echo '<span class="label label-success">Received</span>';}
                                                                      else{}
                                                                 ?></td>
                                                               </tr>
                                                           </tbody>
                                                         </table>
                                                         <button type="button" class="btn btn-info btn-raised job_more_info">More Information</button>
                                           </div>

                                          <div class="row job_user_info" style="display:none;">
                                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                              <h3>Qualifications</h3>
                                      <table class="table table-bordered table-striped">
                                                  <tbody>
                                                      <tr>
                                                        <th>Qualification</th>
                                                         <th>Grade</th>
                                                          <th>Date</th>
                                                      </tr>
                                                      <?foreach ($qualifications as $key => $qual) {?>
                                                          <tr>
                                                            <td><?echo $qual['qualifications'];?></td>
                                                            <td><?echo $qual['grade'];?></td>
                                                            <td><?echo date('d-m-Y',$qual['qdate']);?></td>
                                                          </tr>
                                                          <?}?>
                                                  </tbody>
                                                </table>

                                            </div>




                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <h3>Employment History</h3>
                                        <table class="table table-bordered table-striped">
                                                    <tbody>
                                                        <tr>
                                                          <th>Employer</th>
                                                           <th>Position</th>
                                                            <th>Date</th>
                                                            <th>Address</th>
                                                        </tr>
                                                        <?foreach ($employment as $key => $emp) {?>
                                                            <tr>
                                                              <td><?echo $emp['name'];?></td>
                                                              <td><?echo $emp['title'];?></td>
                                                              <td><?
                                                              echo date('d-m-Y',$emp['date_start']).' To ';
                                                              if($emp['date_end']=='0'){echo 'Present';}else{echo date('d-m-Y',$emp['date_end']);}
                                                              ?></td>
                                                              <td><?echo nl2br($emp['address']);?></td>
                                                            </tr>
                                                            <?}?>
                                                    </tbody>
                                                  </table>
                                              </div>

                                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <h3>References</h3>
                                          <table class="table table-bordered table-striped">
                                                      <tbody>
                                                          <tr>
                                                            <th>Name</th>
                                                             <th>Contact</th>
                                                              <th>Position</th>
                                                              <th>Address</th>
                                                          </tr>
                                                          <?foreach ($references as $key => $ref) {?>
                                                              <tr>
                                                                <td><?echo $ref['name'];?></td>
                                                                <td><?echo $ref['contact'];?></td>
                                                                <td><?echo $ref['position'];?></td>
                                                                <td><?echo nl2br($ref['address']);?></td>
                                                              </tr>
                                                              <?}?>
                                                      </tbody>
                                                    </table>
                                            </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <button type="button" class="btn btn-info btn-raised job_less_info">Less Information</button>
                                                    <?if($app['reference']=='1'){?>
                                                        <button type="button" class="btn btn-success btn-raised pull-right job_ref_received" data-id="<?echo $app['id'];?>">Reference Received</button>
                                                        <?} else if($app['reference']=='2'){?>
                                                        <button type="button" class="btn btn-warning btn-raised pull-right job_not_ref_received" data-id="<?echo $app['id'];?>">Reference Not Received</button>
                                                        <?} ?>
                                                    </div>
                                       </div>
                                   </div>
                          </div>

                                       <?}?>
                               </div>

                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl ?>assets/js/font.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
