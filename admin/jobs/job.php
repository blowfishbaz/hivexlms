<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Jobs';

 $db = new database;
 $db->query("select * from hx_jobs where id = ? ");
 $db->bind(1,$_GET['id']);
 $job = $db->single();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>

<!-- On Page CSS -->

</head>

<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>

            				<!--- PAGE CONTENT HERE ---->

                            <?switch ($job['status']) {
                                case '0':$status='<span class="label label-danger">Closed</span>';break;
                                case '1':$status='<span class="label label-success">Publish</span>';break;
                                case '2':$status='<span class="label label-warning">Unpublish</span>';break;
                                default:$status='';break;
                            }?>
						<div class="page_title text-capitalize">
              <span class="menuglyph glyphicons glyphicons-building" aria-hidden="true"></span><span class="menuText">Job - SR<?echo $job['job_number'];?></span> <?echo $status;?>
              <button type="button" class="btn btn-primary btn-raised pull-right" id="edit_job" data-id="<?echo $_GET['id'];?>">Edit Job</button>
              <?if($job['status']=='2'){?>
              <button type="button" class="btn btn-success btn-raised pull-right" id="publish_job" data-id="<?echo $_GET['id'];?>">Publish</button>
              <?}else if($job['status']=='1'){?>
              <button type="button" class="btn btn-warning btn-raised pull-right" id="unpublish_job" data-id="<?echo $_GET['id'];?>">Unpublish</button>
              <?}else{}?>
          </div>
                        <div class="clearfix"></div>


                        <div class="panel panel-default userlist">
                               <div class="panel-body">
                                   <strong>Title</strong> <?echo $job['title'];?><br />
                                   <strong>Company</strong> <?echo $job['company'];?><br />
                                   <strong>Category</strong> <?echo ucwords($job['category']);?><br />
                                   <strong>County</strong> <?echo ucwords($job['county']);?><br />
                                   <strong>Job Type</strong> <?echo ucwords($job['job_type']);?><br />
                                   <strong>Contract Type</strong> <?echo ucwords($job['contract_type']);?><br />

                                   <?
                                   if($job['salary_type']=='per annum'){
                                        echo '<strong>Salary</strong> £'.number_format($job['per_annum']).' Per Annum<br />';
                                   }else if($job['salary_type']=='pro rata'){
                                        echo '<strong>Salary</strong> £'.number_format($job['per_annum']).' Per Rata<br />';
                                        echo '<strong>Salary</strong> £'.number_format($job['per_hourly']).' Per Hour<br />';
                                    }else if($job['salary_type']=='per hour'){
                                        echo '<strong>Salary</strong> £'.number_format($job['per_hourly'],2).' Per Hour<br />';
                                    }else{echo '<strong>Salary</strong> ERROR';}
                                    ?>
                                   <br /><strong>Overview</strong><br /><?echo $job['overview'];?><br />
                                   <strong>Duties</strong><br /><?echo $job['duties'];?><br />

                               </div>
                               <a href="<?echo $fullurl.'admin/jobs/applicants.php?id='.$_GET['id'].'';?>" class="btn btn-info btn-raised pull-left" data-id="<?echo $_GET['id'];?>">Job Applicants</a>

                               <a href="<?echo $fullurl.'job.php?id='.$_GET['id'].'&preview';?>" target="_blank" class="btn btn-info btn-raised pull-right" data-id="<?echo $_GET['id'];?>">Preview</a>
                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl ?>assets/js/font.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
