<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

 $db->query("select * from accounts where id = ?");
 $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
 $my_account = $db->single();

 $my_account_type = $my_account['account_type'];
 $extra='';

 if($my_account_type == 'user'){
     $extra = '&id='.$my_account['id'].'';
 }

 $wirral_show = 1;
 $sefton_show = 1;
 $liverpool_show = 1;

 if($my_account_type == 'subadmin'){
     $sub_array = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

     if(!in_array('Wirral',$sub_array)){
         $wirral_show = 0;
     }
     if(!in_array('Sefton',$sub_array)){
         $sefton_show = 0;
     }
     if(!in_array('Liverpool',$sub_array)){
         $liverpool_show = 0;
     }
 }

if(!empty($_GET['week'])){
    $week_number = $_GET['week'];
}else{
    $week_number = date('W');
}

if(!empty($_GET['year'])){
    $year = $_GET['year'];
}else{
    $year = date('Y');
}



if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
 $subadmin_query_pcn = sub_admin_query('pcn_id');
 $subadmin_query_prac = sub_admin_query('practice_id','OR');
 $subadmin_search = '';
 if(!empty($subadmin_query_pcn) && !empty($subadmin_query_prac)){
   $subadmin_search = " and ($subadmin_query_pcn $subadmin_query_prac)";
 }
}




?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?	//Base CSS Include
    	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>



<style>

.ledgend_wrapper{
height: 35px;
width: auto;
float: left;
margin-left: 25px;
}

.ledgend_wrapper:first-of-type {
margin:0px;
}

.ledgend_calendar{
height: 25px;
width: 25px;
position: relative;
float: left;
border-radius: 5px;
margin-right: 25px;
}

.legend_text{
position: relative;
float: left;
margin-top: 3px;
}

.fc-prev-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-left.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-prev-button span {
    display: none;
}

.fc-next-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-right.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-next-button span {
    display: none;
}

.ledgend_wrapper{
    height: 35px;
    width: auto;
    float:left;
}

.ledgend_calendar{
  height: 25px;
  width: 25px;
  position: relative;
  float: left;
  border-radius: 5px;
  margin-right: 25px;
}

.legend_text{
      position: relative;
      float: left;
      margin-top: 3px;
  }


.list-view-table{
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;

}

.list-view-table th, .list-view-table td{
    border: 1px solid #ddd;
    padding: 8px;
}

.list-view-table-holder{
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.25);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.25);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.25);
    margin-bottom:20px;
}

</style>

</head>
<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>

                      <!--- PAGE CONTENT HERE ---->
                      <div class="page_title text-capitalize"><img src="/staff-box/images/pink.png" height="75" /><span class="menuText">Calendar - List View</span></div>

                      <div class="col-md-3 col-sm-3">
                          <div class="form-group" style="margin-top:0px;">
                            <label class="control-label pull-left" for="week_number">Week Commencing: </label>
                            <select class="form-control chosen-select" id="week_number" name="week_number" required>

                              <?for ($i=1; $i <= 52; $i++) {?>
                                  <?
                                      $gendateselect = new DateTime();
                                      $gendateselect->setISODate($year,$i,'01');
                                  ?>
                                  <option value="<?echo $i;?>" <?if($week_number == $i){echo 'selected';}?>>w/c - <?echo $gendateselect->format('d M');?></option>
                              <?}?>
                            </select>
                          </div>
                      </div>
                      <div class="col-md-3 col-sm-3">
                          <div class="form-group" style="margin-top:0px;">
                            <label class="control-label pull-left" for="year_number">Year: </label>
                            <select class="form-control chosen-select" id="year_number" name="year_number" required>
                                <option <?if($year == date('Y') - 2){echo 'selected';}?> value="<?echo date('Y') - 2;?>"><?echo date('Y') - 2;?></option>
                                <option <?if($year == date('Y') - 1){echo 'selected';}?> value="<?echo date('Y') - 1;?>"><?echo date('Y') - 1;?></option>
                                <option <?if($year == date('Y')){echo 'selected';}?> value="<?echo date('Y');?>"><?echo date('Y');?></option>
                                <option <?if($year == date('Y') + 1){echo 'selected';}?> value="<?echo date('Y') + 1;?>"><?echo date('Y') + 1;?></option>
                                <option <?if($year == date('Y') + 2){echo 'selected';}?> value="<?echo date('Y') + 2?>"><?echo date('Y') + 2;?></option>
                            </select>
                          </div>
                      </div>
                      <div class="col-md-3 col-sm-3">

                      </div>
                      <div class="col-md-3 col-sm-3">
                          <a href="<?echo $fullurl;?>admin/rota/rota_list.php" class="btn btn-sm btn-raised btn-warning pull-right">This Week</a>
                      </div>

                      <div class="clearfix"> </div>

                      <?for ($day=1; $day <= 7; $day++) {?>
                          <?
                            $gendate = new DateTime();
                            $gendate->setISODate($year,$week_number,$day);

                            $db->query("select * from ws_rota_bid where start_date = ? and status = 1 and completed != 1 $subadmin_search order by start_time asc");
                            $db->bind(1,$gendate->format('Y-m-d'));
                            $rota_bid = $db->resultSet();


                            $db->query("select * from ws_rota_bid where start_date = ? and status = 1 and completed != 1 $subadmin_search order by start_time asc");
                            $db->bind(1,$gendate->format('Y-m-d'));
                            $rota_bid = $db->resultSet();

                            $already_in_array = array();
                          ?>


                          <div class="list-view-table-holder">
                              <table class="list-view-table" id="scroll_to_<?echo $gendate->format('l');?>">
                                  <tr>
                                      <th colspan="8" style="text-align:right;">

                                          <button type="button" class="btn btn-warning btn-raised pull-left" id="add_new_bid_manual" style="margin: 0px; padding: 4px 10px 4px 10px; margin-right:10px;" data-date="<?echo $gendate->format('U')*1000;?>">Manual Shift</button>
                                          <button type="button" class="btn btn-info btn-raised pull-left" id="add_new_bid" style="margin: 0px; padding: 4px 10px 4px 10px;" data-date="<?echo $gendate->format('U')*1000;?>">Send Shift</button>


                                          <?echo $gendate->format('l dS F Y');?>
                                      </th>
                                  </tr>
                                  <tr style="background-color:#f0f0f0;">
                                      <th style="width:7%;">Start Time</th>
                                      <th style="width:7%;">End Time</th>
                                      <th style="width:18%;">Location</th>
                                      <th style="width:18%;">Who</th>
                                      <th style="width:18%;">Service</th>
                                      <th style="width:18%;">Role</th>
                                      <th style="width:7%;">Status</th>
                                      <th style="width:7%;">Action</th>
                                  </tr>

                                  <?foreach ($rota_bid as $rb) {?>
                                      <?
                                          $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1 and completed = 0");
                                          $db->bind(1,$rb['id']);
                                          $rota_amounts = $db->resultSet();

                                          $db->query("select * from ws_service where id = ?");
                                          $db->bind(1,$rb['service_id']);
                                          $service = $db->single();

                                          $db->query("select * from hx_rota where bid_id = ? and dna = 0 and status = 1 and start_date = ? order by start_time asc");
                                          $db->bind(1,$rb['id']);
                                          $db->bind(2, $gendate->format('d-m-Y'));
                                          $oustanding_rota = $db->resultSet();
                                      ?>
                                          <?foreach ($rota_amounts as $ra) {?>
                                              <?
                                                    $db->query("select ws_roles.title
                                                        from ws_roles
                                                        left join ws_service_roles on ws_service_roles.job_role = ws_roles.id
                                                        where ws_service_roles.id = ?");
                                                    $db->bind(1,$ra['service_role_id']);
                                                    $role = $db->single();

                                                    $db->query("select count(id) as counter from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and accepted_admin = 1 AND accepted_user = 1");
                                                    $db->bind(1,$rb['id']);
                                                    $db->bind(2,$ra['id']);
                                                    $requst_accepts = $db->single();

                                                    $ra_amount = 1;

                                                    if($ra['amount'] != 0 ){
                                                        $ra_amount = $ra['amount'];
                                                    }
                                              ?>

                                              <tr style="background-color:#fdebf3;">
                                                  <td><?echo $rb['start_time'];?></td>
                                                  <td><?echo $rb['end_time'];?></td>
                                                  <td>
                                                      <?if(!empty($service['address1'])){echo $service['address1'];}?>
                                                      <?if(!empty($service['address2'])){echo ','.$service['address2'];}?>
                                                      <?if(!empty($service['address3'])){echo ','.$service['address3'];}?>
                                                      <?if(!empty($service['city'])){echo ','.$service['city'];}?>
                                                      <?if(!empty($service['county'])){echo ','.$service['county'];}?>
                                                      <?if(!empty($service['postcode'])){echo ','.$service['postcode'];}?>
                                                  </td>
                                                  <td>Oustanding Positions - <?echo $ra_amount - $requst_accepts['counter'];?></td>
                                                  <td><?echo $service['title'];?></td>
                                                  <td><?echo $role['title'];?></td>
                                                  <td><?if($rb['completed'] == 0){echo 'Oustanding';}else if($rb['completed'] == 2){echo 'Refill Needed';}else if($rb['completed'] == 3){echo 'Reoccurring - Waiting';}?></td>
                                                  <td><button type="button" class="btn btn-sm btn-raised btn-success update_event" data-id="<?echo $ra['id'];?>" data-type="bid">Update</button> </td>
                                              </tr>
                                          <?}?>
                                          <?foreach ($oustanding_rota as $lr) {?>
                                              <?
                                                  $db->query("select ws_roles.title
                                                      from ws_roles
                                                      left join ws_service_roles on ws_service_roles.job_role = ws_roles.id
                                                      where ws_service_roles.id = ?");
                                                  $db->bind(1,$lr['service_role_id']);
                                                  $role = $db->single();

                                                  $db->query("select name from accounts where id = ?");
                                                  $db->bind(1,$lr['member_id']);
                                                  $account = $db->single();

                                                  array_push($already_in_array, $lr['id']);
                                              ?>
                                              <tr>
                                                  <td><?echo $lr['start_time'];?></td>
                                                  <td><?echo $lr['end_time'];?></td>
                                                  <td>
                                                      <?if(!empty($service['address1'])){echo $service['address1'];}?>
                                                      <?if(!empty($service['address2'])){echo ','.$service['address2'];}?>
                                                      <?if(!empty($service['address3'])){echo ','.$service['address3'];}?>
                                                      <?if(!empty($service['city'])){echo ','.$service['city'];}?>
                                                      <?if(!empty($service['county'])){echo ','.$service['county'];}?>
                                                      <?if(!empty($service['postcode'])){echo ','.$service['postcode'];}?>
                                                  </td>
                                                  <td><?echo $account['name'];?></td>
                                                  <td><?echo $service['title'];?></td>
                                                  <td><?echo $role['title'];?></td>
                                                  <td>Admin Accpeted</td>
                                                  <td><button type="button" class="btn btn-sm btn-raised btn-success update_event" data-id="<?echo $lr['id'];?>" data-type="rota_oustanding">Update</button></td>
                                              </tr>
                                          <?}?>
                                          <tr> <th colspan="8" style="background-color:#edafc8;"> </th> </tr>
                                  <?}?>

                                  <!-- This is where we would get the rest of the rota's -->
                                  <?
                                        $id_search = '("0",';
                                        foreach ($already_in_array as $key => $aia) {
                                            $id_search .= '"'.$aia.'",';
                                        }
                                        $id_search .= '"0")';

                                          $db->query("select * from hx_rota where dna = 0 and status = 1 and start_date = ? and id not in $id_search $subadmin_search order by start_time asc");
                                          $db->bind(1, $gendate->format('d-m-Y'));
                                          $live_rota = $db->resultSet();
                                  ?>


                                  <?foreach ($live_rota as $lr) {?>
                                      <?
                                          $db->query("select ws_roles.title
                                              from ws_roles
                                              left join ws_service_roles on ws_service_roles.job_role = ws_roles.id
                                              where ws_service_roles.id = ?");
                                          $db->bind(1,$lr['service_role_id']);
                                          $role = $db->single();

                                          $db->query("select name from accounts where id = ?");
                                          $db->bind(1,$lr['member_id']);
                                          $account = $db->single();

                                          array_push($already_in_array, $lr['id']);

                                          $db->query("select * from ws_service where id = ?");
                                          $db->bind(1,$lr['service_id']);
                                          $service = $db->single();
                                      ?>
                                      <tr>
                                          <td><?echo $lr['start_time'];?></td>
                                          <td><?echo $lr['end_time'];?></td>
                                          <td>
                                              <?if(!empty($service['address1'])){echo $service['address1'];}?>
                                              <?if(!empty($service['address2'])){echo ','.$service['address2'];}?>
                                              <?if(!empty($service['address3'])){echo ','.$service['address3'];}?>
                                              <?if(!empty($service['city'])){echo ','.$service['city'];}?>
                                              <?if(!empty($service['county'])){echo ','.$service['county'];}?>
                                              <?if(!empty($service['postcode'])){echo ','.$service['postcode'];}?>
                                          </td>
                                          <td><?echo $account['name'];?></td>
                                          <td><?echo $service['title'];?></td>
                                          <td><?echo $role['title'];?></td>
                                          <td>Live Rota</td>
                                          <td><button type="button" class="btn btn-sm btn-raised btn-success update_event" data-id="<?echo $lr['id'];?>" data-type="rota_complete">Update</button></td>
                                      </tr>
                                  <?}?>

                              </table>

                          </div>

                      <?}?>



                        </div>
                </div>




        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>
    </div>

<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->

<!-- <script src="<? echo $fullurl ?>assets/app_scripts/crm/customer/index.js"></script> -->
<script src="<? echo $fullurl ?>admin/rota/js/index.js"></script>
<script src="../../../assets/js/picturefill.min.js"></script>
<script src="../../../assets/js/lightgallery.js"></script>
<script src="../../../assets/js/lg-fullscreen.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>

<script>

jQuery(document).ready(function($) {
    $('.selectpicker').selectpicker();

    var width = $('body').width();
    console.log(width);
    if(width > 980){
        $('#menu-toggle').click();
    }else{
         $('#calendar').fullCalendar('option', 'height', 'auto');
    }


    <?if($week_number == date('W') && $year == date('Y')){?>
      $([document.documentElement, document.body]).animate({
        scrollTop: $("#scroll_to_<?echo date('l');?>").offset().top - 75
    }, 400);
   <?}?>

});




$( "body" ).on( "change", "#service", function() {
    var id = $(this).val();

    $('.service_table_holder').html($Loader);
    $('.bid_amount_holder').empty();

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service_table.php?id="+id,
       success: function(msg){
         $(".service_table_holder").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
     });

     $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/form/add_bid_amount.php?id="+id,
        success: function(msg){
          $(".bid_amount_holder").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
                       $('.bid_info_holder').show();
                       $('.add_bid_message').hide();
                       $('#save_add_bid').show();
           });
          }
      });
});

function isValidDate(dateString){
   //alert(dateString);
   if(!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(dateString)){
      if(/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)){
         var parts = dateString.split("-");
         var day = parseInt(parts[2], 10);
         var month = parseInt(parts[1], 10);
         var year = parseInt(parts[0], 10);
      }else{
         //alert('1');
         return false;
      }
   }else{
      var parts = dateString.split("-");
      var day = parseInt(parts[0], 10);
      var month = parseInt(parts[1], 10);
      var year = parseInt(parts[2], 10);
   }

   if(year < 1000 || year > 3000 || month == 0 || month > 12){
      //alert('2');
      return false;
   }

   var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
   if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;
   return day > 0 && day <= monthLength[month - 1];
};

function isValidTime(timeString){
   if(!/([01][0-9]|[02][0-3]):[0-5][0-9]/.test(timeString)){
      return false;
   }else{
		return true;
	}
}

$( "body" ).on( "click", "#save_add_bid", function() {

    var formid = '#add_bid_form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();

    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }
     if(isValidTime(start_time) === false){
       TimeError = 1;
    }

    if(isValidTime(end_time) === false){
     TimeError = 1;
  }

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_bid.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid save.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occur#b90000 please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }else if(DateError == 1){
       Messenger().post({
           message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
           type: 'error',
           showCloseButton: false
       });
    }else if(TimeError == 1){
       Messenger().post({
           message: 'Please make sure the time are formatted (hh:mm).',
           type: 'error',
           showCloseButton: false
       });
    }

});


$( "body" ).on( "change", "#week_number, #year_number", function() {
    var week = $('#week_number').val();
    var year = $('#year_number').val();

    window.location.href = '<?echo $fullurl;?>/admin/rota/rota_list.php?year='+year+'&week='+week;
});


$( "body" ).on( "click", "#add_new_bid_manual", function() {

});

$( "body" ).on( "click", "#add_new_bid", function() {
    var date = $(this).data('date');


    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/add_bid.php?date="+date,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }

});
});

$( "body" ).on( "change", "#start_date, #end_date, #start_time, #end_time", function() {
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  var start_time = $('#start_time').val();
  var end_time = $('#end_time').val();
  var service = $('#service').val();

  console.log('change');

  $('.available_bidder_holder').html($Loader);

  $.ajax({
     type: "POST",
     url: $fullurl+"admin/rota/form/get_available_bidder_holder.php",
     data:{ start_date:start_date, end_date:end_date, start_time:start_time, end_time:end_time, service:service },
     success: function(msg){
       $(".available_bidder_holder").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
       }
});
});

$( "body" ).on( "click", "#add_new_bid_manual", function() {
    var date = $(this).data('date');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/add_bid_manual.php?date="+date,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});


$( "body" ).on( "change", "#service_manual", function() {
    var id = $(this).val();

    $('.service_table_holder').html($Loader);
    $('.bid_amount_holder_manual').empty();

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service_table.php?id="+id,
       success: function(msg){
         $(".service_table_holder").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
     });

     $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/form/add_bid_amount_manual.php?id="+id,
        success: function(msg){
          $(".bid_amount_holder_manual").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
                       $('.bid_info_holder').show();
                       $('.add_bid_message').hide();
                       $('#save_add_bid_manual').show();
                       $('.selectpicker').selectpicker();
           });
          }
      });
});

$( "body" ).on( "click", "#save_add_bid_manual", function() {

    var formid = '#add_bid_manual_form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();


    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }
     if(isValidTime(start_time) === false){
      TimeError = 1;
    }

    if(isValidTime(end_time) === false){
     TimeError = 1;
  }

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_bid_manual.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid save.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }else if(DateError == 1){
      Messenger().post({
          message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
          type: 'error',
          showCloseButton: false
      });
   }else if(TimeError == 1){
      Messenger().post({
          message: 'Please make sure the time are formatted (hh:mm).',
          type: 'error',
          showCloseButton: false
      });
   }

});

$( "body" ).on( "click", ".update_event", function() {
    var id = $(this).data('id');
    var type = $(this).data('type');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/edit-calendar-events.php",
       data: {'id':id, 'type':type},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});




$( "body" ).on( "click", "#confirm-edit-calendar", function() {
    var formid = '#edit-calendar-event-form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();

    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }
     if(isValidTime(start_time) === false){
      TimeError = 1;
    }

    if(isValidTime(end_time) === false){
     TimeError = 1;
  }

    $(formid).find('input').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
    });
    $(formid).find('select').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
           if(!$(this).prop('required')){
      } else {
        HasError = 1;
        $(this).parent().addClass('has-error');
      }
    }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

    var FRMdata = $(formid).serialize(); // get form data
    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/save-edit-calendar-event.php",
          data: FRMdata,
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Service Updated.',
                    type: 'error',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //Refresh the Page
                window.location.reload();

              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });
    }else if(DateError == 1){
    Messenger().post({
        message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
        type: 'error',
        showCloseButton: false
    });
}else if(TimeError == 1){
    Messenger().post({
        message: 'Please make sure the time are formatted (hh:mm).',
        type: 'error',
        showCloseButton: false
    });
}
});

$( "body" ).on( "click", "#delete-calendar-event", function() {
   var id = $(this).data('id');
   var type = $(this).data('type');

   $("#BaseModalLContent").html($Loader);
   $('#BaseModalL').modal('show');

   $.ajax({
      type: "POST",
      url: $fullurl+"admin/rota/form/delete-shift-bid-confirm.php",
      data: {'id':id, 'type':type},
      success: function(msg){
        $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
             $(this).html(msg);
             n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                     n();
                     $('.selectpicker').selectpicker();
        });
        }
});
});


$( "body" ).on( "click", "#confirm-save-delete-calendar", function() {
   var id = $(this).data('id');
   var type = $(this).data('type');


      $.ajax({
            type: "POST",
            url: $fullurl+"admin/rota/ajax/confirm-delete-calendar-event.php",
            data: {'id':id, 'type':type},
            success: function(msg){
              $message=$.trim(msg);
              if($message=='ok'){
                  Messenger().post({
                      message: 'Shift Deleted.',
                      type: 'error',
                      showCloseButton: false
                  });
                setTimeout(function(){
                  //window.location.replace($fullurl+'jobs.php');

                  //Refresh the Page
                  window.location.reload();

                },1000);
              }

           },error: function (xhr, status, errorThrown) {
                  Messenger().post({
                      message: 'An error has occurred please try again.',
                      type: 'error',
                      showCloseButton: false
                  });
                }
      });
});

$( "body" ).on( "click", ".reoccuring_shift", function() {
   $('.reoccuring_shift_holder').toggle();
});

$( "body" ).on( "click", ".reoccure_freq", function() {
   var value = $(this).val();
   if(value == 'weekly'){
      $('#reoccur_table').show();
      $('.reoccur_monthly').hide();
   }else{
      $('#reoccur_table').hide();
      $('.reoccur_monthly').show();
   }
});
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
