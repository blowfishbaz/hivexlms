<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

 $db->query("select * from accounts where id = ?");
 $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
 $my_account = $db->single();

 $my_account_type = $my_account['account_type'];
 $extra='';

 if($my_account_type == 'user'){
     $extra = '&id='.$my_account['id'].'';
 }

 $wirral_show = 1;
 $sefton_show = 1;
 $liverpool_show = 1;

 if($my_account_type == 'subadmin'){
     $sub_array = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

     if(!in_array('Wirral',$sub_array)){
         $wirral_show = 0;
     }
     if(!in_array('Sefton',$sub_array)){
         $sefton_show = 0;
     }
     if(!in_array('Liverpool',$sub_array)){
         $liverpool_show = 0;
     }
 }





?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?	//Base CSS Include
    	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>



<style>

.ledgend_wrapper{
height: 35px;
width: auto;
float: left;
margin-left: 25px;
}

.ledgend_wrapper:first-of-type {
margin:0px;
}

.ledgend_calendar{
height: 25px;
width: 25px;
position: relative;
float: left;
border-radius: 5px;
margin-right: 25px;
}

.legend_text{
position: relative;
float: left;
margin-top: 3px;
}

.fc-prev-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-left.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-prev-button span {
    display: none;
}

.fc-next-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-right.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-next-button span {
    display: none;
}

.ledgend_wrapper{
    height: 35px;
    width: auto;
    float:left;
}

.ledgend_calendar{
  height: 25px;
  width: 25px;
  position: relative;
  float: left;
  border-radius: 5px;
  margin-right: 25px;
}

.legend_text{
      position: relative;
      float: left;
      margin-top: 3px;
  }

</style>

</head>
<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>

                      <!--- PAGE CONTENT HERE ---->
                      <div class="page_title text-capitalize"><img src="/staff-box/images/pink.png" height="75" /><span class="menuText">Calendar</span></div>

                      <div style="width:100%;">
                          <div class="ledgend_wrapper">
                              <div class="ledgend_calendar" style="background-color:#ff5722;">

                              </div>
                              <div class="legend_text">
                                  <strong>Did Not Attend</strong>
                              </div>
                          </div>
                          <div class="ledgend_wrapper">
                              <div class="ledgend_calendar" style="background-color:#b90000;">

                              </div>
                              <div class="legend_text">
                                  <strong>Oustanding Shift</strong>
                              </div>
                          </div>
                          <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?>
                            <div class="ledgend_wrapper">
                                <div class="ledgend_calendar" style="background-color:#46bbc7;">

                                </div>
                                <div class="legend_text">
                                    <strong>Wirral</strong>
                                </div>
                            </div>
                            <div class="ledgend_wrapper">
                                <div class="ledgend_calendar" style="background-color:#f5a11c;">

                                </div>
                                <div class="legend_text">
                                    <strong>Liverpool</strong>
                                </div>
                            </div>
                            <div class="ledgend_wrapper">
                                <div class="ledgend_calendar" style="background-color:#5e4292;">

                                </div>
                                <div class="legend_text">
                                    <strong>Sefton</strong>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group">
                              <label class="control-label">Filter</label>
                              <select name="calendar_filter" class="form-control selectpicker" data-live-search="true" id="calendar_filter">
                                  <option value=""> Show All </option>
                                  <?
                                  $db->query("select * from ws_pcn where status = 1");
                                  $pcn = $db->ResultSet();

                                  $db->query("select * from ws_practice where pcn_location = '' and status = 1");
                                  $stand_alone_practices = $db->ResultSet();
                                  ?>
                                  <?foreach ($pcn as $p) {?>
                                    <option value="<?echo $p['id'];?>"><?echo $p['pcn_title'];?></option>
                                    <?
                                    $db->query("select * from ws_practice where pcn_location = ? and status = 1 $subadmin_query");
                                    $db->bind(1,$p['id']);
                                    $practice = $db->ResultSet();
                                    ?>
                                    <?foreach ($practice as $prac) {?>
                                      <option value="<?echo $prac['id'];?>"><?echo $prac['title'];?></option>
                                    <?}?>
                                    <option disabled>----------------</option>
                                  <?}?>

                                  <?foreach ($stand_alone_practices as $sap) {?>
                                    <option value="<?echo $sap['id'];?>"><?echo $sap['title'];?></option>
                                  <?}?>
                            </select>
                          </div>
                          <?}else{?>
                              <?$my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));?>
                            <?if(in_array('Wirral',$my_areas)){?>
                              <div class="ledgend_wrapper">
                                  <div class="ledgend_calendar" style="background-color:#46bbc7;">

                                  </div>
                                  <div class="legend_text">
                                      <strong>Wirral</strong>
                                  </div>
                              </div>
                            <?}?>
                            <?if(in_array('Sefton',$my_areas)){?>
                              <div class="ledgend_wrapper">
                                  <div class="ledgend_calendar" style="background-color:#f5a11c;">

                                  </div>
                                  <div class="legend_text">
                                      <strong>Liverpool</strong>
                                  </div>
                              </div>
                            <?}?>
                            <?if(in_array('Liverpool',$my_areas)){?>
                              <div class="ledgend_wrapper">
                                  <div class="ledgend_calendar" style="background-color:#5e4292;">

                                  </div>
                                  <div class="legend_text">
                                      <strong>Sefton</strong>
                                  </div>
                              </div>
                            <?}?>
                          <?}?>
                          <a href="rota_list.php" class="btn btn-sm btn-raised btn-success pull-right">Advanced View</a>
                      </div>





                    <div class="clearfix"></div>

                          <div class="calendar" id="calendar">

                          </div>
                        </div>
                </div>




        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>
    </div>

<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->

<!-- <script src="<? echo $fullurl ?>assets/app_scripts/crm/customer/index.js"></script> -->
<script src="<? echo $fullurl ?>admin/rota/js/index.js"></script>
<script src="../../../assets/js/picturefill.min.js"></script>
<script src="../../../assets/js/lightgallery.js"></script>
<script src="../../../assets/js/lg-fullscreen.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>

<script>

jQuery(document).ready(function($) {
    $('.selectpicker').selectpicker();

    var width = $('body').width();
    console.log(width);
    if(width > 980){
        $('#menu-toggle').click();
    }else{
         $('#calendar').fullCalendar('option', 'height', 'auto');
    }

});

var wirral_url = '../../../admin/rota/ajax/calendar.php?location=wirral<?echo $extra;?>&show=<?echo $wirral_show;?>';
var sefton_url = '../../../admin/rota/ajax/calendar.php?location=sefton<?echo $extra;?>&show=<?echo $sefton_show;?>';
var liverpool_url = '../../../admin/rota/ajax/calendar.php?location=liverpool<?echo $extra;?>&show=<?echo $liverpool_show;?>';

var wirral_url_dna = '../../../admin/rota/ajax/calendar.php?location=wirral&dna=1&show=<?echo $wirral_show;?>';
var sefton_url_dna = '../../../admin/rota/ajax/calendar.php?location=sefton&dna=1&show=<?echo $sefton_show;?>';
var liverpool_url_dna = '../../../admin/rota/ajax/calendar.php?location=liverpool&dna=1&show=<?echo $liverpool_show;?>';

var wirral_url_pending = '../../../admin/rota/ajax/calendar_pending.php?location=wirral&dna=1&show=<?echo $wirral_show;?>';
var sefton_url_pending = '../../../admin/rota/ajax/calendar_pending.php?location=sefton&dna=1&show=<?echo $sefton_show;?>';
var liverpool_url_pending = '../../../admin/rota/ajax/calendar_pending.php?location=liverpool&dna=1&show=<?echo $liverpool_show;?>';

var wirral_url_pending_recurr = '../../../admin/rota/ajax/calendar_pending_recurr.php?location=wirral&dna=1&show=<?echo $liverpool_show;?>';
var sefton_url_pending_recurr = '../../../admin/rota/ajax/calendar_pending_recurr.php?location=sefton&dna=1&show=<?echo $liverpool_show;?>';
var liverpool_url_pending_recurr = '../../../admin/rota/ajax/calendar_pending_recurr.php?location=liverpool&dna=1&show=<?echo $liverpool_show;?>';




$('.calendar').fullCalendar({
header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month,agendaWeek,agendaDay'
},
editable: false,
timezoneParam:'GMT',
eventLimit: true,
aspectRatio: 2,
buttonText: {
                today: 'Go To Today',
                month:    'Month',
                week:     'Week',
                day:      'Day'
            },
buttonIcons: {
    prev: 'left-double-arrow',
    next: 'right-double-arrow',
},
eventSources: [

  {
      url: wirral_url, // use the `url` property
      color: '#46bbc7',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
  },
  {
      url: sefton_url, // use the `url` property
      color: '#5e4292',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
  },
  {
      url: liverpool_url, // use the `url` property
      color: '#f5a11c',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
    },
{
    url: wirral_url_dna, // use the `url` property
    color: '#ff5722',    // an option!
    textColor: 'white',  // an option!
    className: 'reminders',
    id: 'id'
  },
  {
      url: sefton_url_dna, // use the `url` property
      color: '#ff5722',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
    },
    {
        url: liverpool_url_dna, // use the `url` property
        color: '#ff5722',    // an option!
        textColor: 'white',  // an option!
        className: 'reminders',
        id: 'id'
      }


      ,
      {
          url: wirral_url_pending, // use the `url` property
          color: '#b90000',    // an option!
          textColor: 'white',  // an option!
          className: 'reminders',
          id: 'id'
        }
        ,
        {
            url: sefton_url_pending, // use the `url` property
            color: '#b90000',    // an option!
            textColor: 'white',  // an option!
            className: 'reminders',
            id: 'id'
          }
          ,
          {
              url: liverpool_url_pending, // use the `url` property
              color: '#b90000',    // an option!
              textColor: 'white',  // an option!
              className: 'reminders',
              id: 'id'
            }


            ,
            {
                url: wirral_url_pending_recurr, // use the `url` property
                color: '#d2d2d2',    // an option!
                textColor: 'black',  // an option!
                className: 'reminders',
                id: 'id'
              }
              ,
              {
                  url: sefton_url_pending_recurr, // use the `url` property
                  color: '#d2d2d2',    // an option!
                  textColor: 'black',  // an option!
                  className: 'reminders',
                  id: 'id'
                }
                ,
                {
                    url: liverpool_url_pending_recurr, // use the `url` property
                    color: '#d2d2d2',    // an option!
                    textColor: 'black',  // an option!
                    className: 'reminders',
                    id: 'id'
                  }









],
eventClick: function(calEvent, jsEvent, view) {
  var appointmentID = calEvent.id;
    console.log(appointmentID);
    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');
    $.ajax({
        type: "POST",
        url: "../../../admin/rota/ajax/calendar_view_appointment.php?id="+appointmentID+"",
        success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();

                       $('#notes').summernote({
                     height: 300,                 // set editor height
                     minHeight: null,             // set minimum height of editor
                     maxHeight: null,             // set maximum height of editor
                     focus: true,                  // set focus to editable area after initializing summernote
                     toolbar: [
                       ['font', ['bold', 'italic', 'underline']],
                       ['color', ['color']],
                       ['para', ['ul', 'ol', 'paragraph']],
                       ['table', ['table']],
                       ['insert', ['link', 'hr']],
                       ['help', ['help']]
                       ]
                   });
           });
         }
    });
},    dayClick: function(date, jsEvent, view) {

    <?if($my_account_type != 'user'){?>

        $("#BaseModalLContent").html($Loader);
        $('#BaseModalL').modal('show');

        $.ajax({
           type: "POST",
           url: $fullurl+"admin/rota/form/add_bid.php?date="+date,
           success: function(msg){
             $("#BaseModalLContent").delay(1000)
              .queue(function(n) {
                  $(this).html(msg);
                  n();
              }).fadeIn("slow").queue(function(n) {
                           $.material.init();
                          n();
                          $('.selectpicker').selectpicker();
              });
             }

    });

    <?}?>


  }

});

$( "body" ).on( "change", "#calendar_filter", function() {
    var id = $(this).val();

    //Remove Calendar Events
    $('.calendar').fullCalendar( 'removeEventSource',  { url: wirral_url} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: sefton_url} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: liverpool_url} );

    $('.calendar').fullCalendar( 'removeEventSource',  { url: wirral_url_dna} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: sefton_url_dna} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: liverpool_url_dna} );

    $('.calendar').fullCalendar( 'removeEventSource',  { url: wirral_url_pending} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: sefton_url_pending} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: liverpool_url_pending} );

    $('.calendar').fullCalendar( 'removeEventSource',  { url: wirral_url_pending_recurr} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: sefton_url_pending_recurr} );
    $('.calendar').fullCalendar( 'removeEventSource',  { url: liverpool_url_pending_recurr} );

    //Re add calendar events
    $('.calendar').fullCalendar( 'addEventSource', { url: wirral_url+"&pp="+id+"",  color: '#46bbc7',  textColor: 'white',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: sefton_url+"&pp="+id+"",  color: '#5e4292',  textColor: 'white',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: liverpool_url+"&pp="+id+"",color: '#f5a11c',  textColor: 'white', className: 'reminders', id: 'id' });

    $('.calendar').fullCalendar( 'addEventSource', { url: wirral_url_dna+"&pp="+id+"",  color: '#ff5722',  textColor: 'white',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: sefton_url_dna+"&pp="+id+"",  color: '#ff5722',  textColor: 'white',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: liverpool_url_dna+"&pp="+id+"",color: '#ff5722',  textColor: 'white', className: 'reminders', id: 'id' });

    $('.calendar').fullCalendar( 'addEventSource', { url: wirral_url_pending+"&pp="+id+"",  color: '#b90000',  textColor: 'white',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: sefton_url_pending+"&pp="+id+"",  color: '#b90000',  textColor: 'white',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: liverpool_url_pending+"&pp="+id+"",color: '#b90000',  textColor: 'white', className: 'reminders', id: 'id' });

    $('.calendar').fullCalendar( 'addEventSource', { url: wirral_url_pending_recurr+"&pp="+id+"",  color: '#d2d2d2',  textColor: 'black',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: sefton_url_pending_recurr+"&pp="+id+"",  color: '#d2d2d2',  textColor: 'black',  className: 'reminders', id: 'id' });
    $('.calendar').fullCalendar( 'addEventSource', { url: liverpool_url_pending_recurr+"&pp="+id+"",color: '#d2d2d2',  textColor: 'black', className: 'reminders', id: 'id' });
});

$( "body" ).on( "click", "#did_not_attend", function() {
  var id = $(this).data('id');
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  $.ajax({
      type: "POST",
      url: "../../../admin/rota/form/did_not_attend.php?id="+id+"",
      success: function(msg){
        //alert(msg);
        $("#BaseModalLContent").delay(1000)
         .queue(function(n) {
             $(this).html(msg);
             n();
         }).fadeIn("slow").queue(function(n) {
                      $.material.init();
                     n();

                     $('#dna_message').summernote({
                   height: 200,                 // set editor height
                   minHeight: null,             // set minimum height of editor
                   maxHeight: null,             // set maximum height of editor
                   focus: true,                  // set focus to editable area after initializing summernote
                   toolbar: [
                     ['font', ['bold', 'italic', 'underline']],
                     ['color', ['color']],
                     ['para', ['ul', 'ol', 'paragraph']],
                     ['table', ['table']],
                     ['insert', ['link', 'hr']],
                     ['help', ['help']]
                     ]
                 });
         });
       }
  });
});

$( "body" ).on( "click", "#confirm_did_not_attend", function() {
  var form = $('#did_not_attend_form').serialize();

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/did_not_attend.php",
        data: form,
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
                          Messenger().post({
                                  message: 'Shift marked as Did Not Attend.',
                                  type: 'error',
                                  showCloseButton: false
                          });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
                              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occur#b90000 please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});


$( "body" ).on( "click", "#did_attend", function() {
  var id = $(this).data('id');

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/did_attend.php",
        data: {id:id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
                          Messenger().post({
                                  message: 'Shift marked as Did Attend.',
                                  type: 'error',
                                  showCloseButton: false
                          });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
                              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occur#b90000 please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});

$( "body" ).on( "change", "#service", function() {
    var id = $(this).val();

    $('.service_table_holder').html($Loader);
    $('.bid_amount_holder').empty();

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service_table.php?id="+id,
       success: function(msg){
         $(".service_table_holder").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
     });

     $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/form/add_bid_amount.php?id="+id,
        success: function(msg){
          $(".bid_amount_holder").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
                       $('.bid_info_holder').show();
                       $('.add_bid_message').hide();
                       $('#save_add_bid').show();
           });
          }
      });
});

function isValidDate(dateString){
   //alert(dateString);
   if(!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(dateString)){
      if(/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)){
         var parts = dateString.split("-");
         var day = parseInt(parts[2], 10);
         var month = parseInt(parts[1], 10);
         var year = parseInt(parts[0], 10);
      }else{
         //alert('1');
         return false;
      }
   }else{
      var parts = dateString.split("-");
      var day = parseInt(parts[0], 10);
      var month = parseInt(parts[1], 10);
      var year = parseInt(parts[2], 10);
   }

   if(year < 1000 || year > 3000 || month == 0 || month > 12){
      //alert('2');
      return false;
   }

   var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
   if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;
   return day > 0 && day <= monthLength[month - 1];
};

function isValidTime(timeString){
   if(!/([01][0-9]|[02][0-3]):[0-5][0-9]/.test(timeString)){
      return false;
   }else{
		return true;
	}
}

$( "body" ).on( "click", "#save_add_bid", function() {

    var formid = '#add_bid_form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();

    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }
     if(isValidTime(start_time) === false){
       TimeError = 1;
    }

    if(isValidTime(end_time) === false){
     TimeError = 1;
  }

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_bid.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid save.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occur#b90000 please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }else if(DateError == 1){
       Messenger().post({
           message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
           type: 'error',
           showCloseButton: false
       });
    }else if(TimeError == 1){
       Messenger().post({
           message: 'Please make sure the time are formatted (hh:mm).',
           type: 'error',
           showCloseButton: false
       });
    }

});
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
