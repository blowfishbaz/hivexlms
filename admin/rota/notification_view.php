<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

$id = $_GET['id'];

$db->query("select * from ws_notifications where id = ?");
$db->bind(1,$id);
$notifications = $db->single();

$db->query("select * from accounts where id = ?");
$db->bind(1,$notifications['created_by']);
$created_by = $db->single();




?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?	//Base CSS Include
    	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>



<style>

.ledgend_wrapper{
height: 35px;
width: auto;
float: left;
margin-left: 25px;
}

.ledgend_wrapper:first-of-type {
margin:0px;
}

.ledgend_calendar{
height: 25px;
width: 25px;
position: relative;
float: left;
border-radius: 5px;
margin-right: 25px;
}

.legend_text{
position: relative;
float: left;
margin-top: 3px;
}

.fc-prev-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-left.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-prev-button span {
    display: none;
}

.fc-next-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-right.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-next-button span {
    display: none;
}

</style>

</head>
<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>

                      <!--- PAGE CONTENT HERE ---->
                    <div class="page_title text-capitalize"><img src="/staff-box/images/pink.png" height="75" /><span class="menuText">Notification View - <?echo $notifications['title'];?></span></div>


                    <div class="well">
                        <?echo $notifications['message'];?>
                    </div>
                    <small>Created by - <?echo $created_by['name'];?> <?echo date('d-m-Y H:i', $notifications['created_date']);?></small>

                    <div class="clearfix"></div>


                        </div>
                </div>




        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>
    </div>

<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->

<!-- <script src="<? echo $fullurl ?>assets/app_scripts/crm/customer/index.js"></script> -->
<script src="<? echo $fullurl ?>admin/rota/js/index.js"></script>
<script src="../../../assets/js/picturefill.min.js"></script>
<script src="../../../assets/js/lightgallery.js"></script>
<script src="../../../assets/js/lg-fullscreen.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>




<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
