<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Account Approval';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>
.form-control {
/* margin-bottom: 7px; */
-webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
padding: 0px 20px;
border-radius: 10px;
/* margin: 10px 0px; */
background-image: none;
width: 300px;
}
.form-control::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: black;
            opacity: 1; /* Firefox */
}

.form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: black;
}

.form-control::-ms-input-placeholder { /* Microsoft Edge */
            color: black;
 }

 .search_table_div{
     padding-right: 20px!important;
    width: 20%;
    float: left!important;
    margin-top:10px!important;
 }

 .search_table_div select{
     margin-bottom: 7px!important;

    -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%)!important;
    -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75)!important;
    box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%)!important;
    padding: 0px 20px!important;
    border-radius: 10px!important;
    /* margin: 10px 0px; */
    background-image: none!important;
 }

 .th-inner{
     background-color: #F0F0F0;
 }
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                            <div class="page_title text-capitalize">
                                <img src="/staff-box/images/pink.png" height="75" />
                                <span class="menuText">Users</span>


                            </div>




								<div class="clearfix"></div>



                                              <div class="form-group search_table_div" style="margin:0px; width: auto;">
                                                <select name="location" class="form-control" id="location" required>
                                                    <option value="All">All Locations</option>
                                                    <option value="Wirral">Wirral</option>
                                                    <option value="Sefton">Sefton</option>
                                                    <option value="Liverpool">Liverpool</option>


                                                </select>
                                              </div>



                                            <table id="table" class="striped"
                                                        data-toggle="table"
                                                        data-show-export="false"
                                                        data-export-types="['excel']"
                                                        data-click-to-select="true"
                                                        data-filter-control="false"
                                                        data-show-columns="false"
                                                        data-show-refresh="false"
                                                        data-url="<? echo $fullurl; ?>admin/rota/ajax/table/view_all_profiles.php?location=All"
                                                        data-height="400"
                                                        data-side-pagination="server"
                                                        data-pagination="true"
                                                        data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                        data-search="true"
                                                        data-search-align="left"
                                                        data-row-attributes="rowAttributes">
                                                    <thead>
                                                        <tr>
                                                                <th data-field="acc_id" data-sortable="true" data-visible="false">ID</th>
                                                                <th data-field="name" data-sortable="true" data-halign="center">Name</th>
                                                                <th data-field="email" data-sortable="true" data-halign="center">Email</th>
                                                                <th data-field="telephone" data-sortable="true" data-halign="center">Telephone</th>
                                                                <th data-field="mobile_number" data-sortable="true" data-halign="center">Mobile</th>
                                                                <th data-field="created_d" data-sortable="true" data-visible="true" data-formatter="DateTimeFormatterAccounts" data-halign="center">Created Date</th>


                                                        </tr>
                                                    </thead>
                                            </table>






            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>
    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>

<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->



<script>

$(document).ready(function () {
    var timer;
    $(document).on('mouseenter', '#table tr', function () {
        var that = $(this);
        timer = setTimeout(function(){
            that.find("#view_account").fadeIn(100);
            that.find("#account_time").hide();
        }, 500);


    }).on('mouseleave', '#table tr', function () {
        var that = $(this);
        $(this).find("#view_account").fadeOut(200);
        setTimeout(function(){
            that.find("#account_time").fadeIn(200);
        }, 200);

        clearTimeout(timer);
    });
});



	$( "body" ).on( "change", "#location", function() {
        var value = $(this).val();

        var url1 = '<?echo $fullurl;?>admin/rota/ajax/table/view_all_profiles.php?location='+value
        $('#table').bootstrapTable('refresh', {url: url1});
	});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
