<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $id = $_GET['id'];

 $db->query("select * from ws_rota_bid where id = ?");
 $db->bind(1,$id);
 $bid_info = $db->single();

 // $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
 // $db->bind(1,$id);
 // $bid_req_info = $db->resultset();

 $db->query("select * from ws_rota_bid_request where pid = ? and status = 1");
 $db->bind(1,$id);
 $bid_req_info_all = $db->resultset();

 $db->query("select * from ws_service where id = ?");
 $db->bind(1,$bid_info['service_id']);
 $service = $db->single();

 $db->query("select ws_service_roles.*, ws_roles.title from ws_service_roles left join ws_roles on ws_roles.id = ws_service_roles.job_role where ws_service_roles.pid = ? AND ws_service_roles.status = 1");
 $db->bind(1,$bid_info['service_id']);
 $service_roles = $db->resultset();

 $db->query("select * from ws_pcn where id = ?");
 $db->bind(1,$bid_info['pcn_id']);
 $pcn = $db->single();

 $db->query("select * from ws_practice where id = ?");
 $db->bind(1,$bid_info['practice_id']);
 $practice = $db->single();

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
}

td{
      color: #46bbc7;
      font-family: 'DiariaPro' !important;
}

#role_info_table > thead > tr > th:last-of-type{
  border-right:0px;
}

#role_info_table > tbody > tr > td{
  border-right:1px solid #dddddd;
}

#role_info_table > tbody > tr > td:last-of-type{
  border-right:0px;
}

#bid_req_table{
  margin-bottom:10px;
}

#bid_req_table td{
  border: 1px solid #dddddd;
}

#bid_req_table th{
  border: 1px solid #dddddd;
}

#top_table{
  border-radius: 20px 20px 20px 20px;
  -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
  -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
  margin-bottom:40px;
}

#bottom_table{

}

#bottom_table td:not(:first-child){
  border-left:1px solid #ddd;
}


.bottom_table_holder{
  border-radius: 20px 20px 20px 20px;
  -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
  -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.3);
  overflow: scroll;
}

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                    <div class="page_title text-capitalize">
                                        <img src="/staff-box/images/pink.png" height="75" />
                                        <span class="menuText">Manage Shift Responses</span>


                                    </div>



								<div class="clearfix"></div>
                                <div class="row">
                                    <div class="row">
                                            <div class="panel-body">

                                              <div class="col-lg-12">
                                                <?if($bid_info['completed'] == 2 || $bid_info['completed'] == 0){?>
                                                  <button type="button" class="btn btn-sm btn-raised btn-danger pull-right" id="recalculate_shift" data-id="<?echo $id;?>">Recalculate Shift</button>
                                                  <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="resend_shift" data-id="<?echo $id;?>">Resend Shift</button>
                                                  <button type="button" class="btn btn-sm btn-raised btn-warning pull-right" id="resend_shift_manual" data-id="<?echo $id;?>">Resend Shift Manual</button>

                                                  <div class="clearfix"></div>
                                                <?}?>
                                                <?if($bid_info['completed'] == 3){?>
                                                  <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="send_shift" data-id="<?echo $id;?>">Send Shift</button>
                                                <?}?>
                                                <table id="top_table">
                                                  <tr>
                                                    <th colspan="4" style="border-right:0px; text-align:center;">
                                                      Shift Details
                                                    </th>
                                                  </tr>
                                                  <?if($bid_info['completed'] == 1){?>
                                                    <tr>
                                                      <th colspan="4" style="border-right:0px;">Completed</th>
                                                    </tr>
                                                  <?}?>
                                                  <?if($bid_info['completed'] == 2){?>
                                                    <tr>
                                                      <th colspan="4" style="border-right:0px;">REFILL NEEDED</th>
                                                    </tr>
                                                  <?}?>
                                                  <?if($bid_info['completed'] == 3){?>
                                                    <tr>
                                                      <th colspan="4" style="border-right:0px;">Reocurring Shift</th>
                                                    </tr>
                                                  <?}?>
                                                    <tr>
                                                        <th colspan="2">Title</th>
                                                        <td colspan="2"><?echo $service['title'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Description</th>
                                                        <td colspan="2"><?echo $service['description'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Location</th>
                                                        <td colspan="2"><?echo $service['location'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Start Date / Time</th>
                                                        <td colspan="2"><?echo date('d-m-Y H:i',$bid_info['start_final']);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">End Date / Time</th>
                                                        <td colspan="2"><?echo date('d-m-Y H:i',$bid_info['end_final']);?></td>
                                                    </tr>


                                                    <tr>
                                                        <th colspan="2">PCN Name</th>
                                                        <td colspan="2"><?echo $pcn['pcn_title'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Practice Name</th>
                                                        <td colspan="2"><?echo $practice['title'];?></td>
                                                    </tr>


                                                    <tr>
                                                      <th colspan="4" style="border-right:0px; text-align:center;">
                                                        Role Details
                                                      </th>
                                                    </tr>
                                                    <?foreach ($service_roles as $sr) {?>
                                                      <?
                                                      $db->query("select * from ws_rota_bid_amount where pid = ? and role_id = ? and status = 1");
                                                      $db->bind(1,$id);
                                                      $db->bind(2,$sr['job_role']);
                                                      $bid_req_info = $db->single();

                                                      $db->query("select COALESCE(sum(accepted_admin),0) as total from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and status = 1 and accepted_admin = 1");
                                                      $db->bind(1,$id);
                                                      $db->bind(2,$bid_req_info['id']);
                                                      $accepted_amount = $db->single();
                                                      ?>
                                                      <tr>
                                                          <th colspan="2"><?echo $sr['title'];?></th>
                                                          <td colspan="2">£<?echo $sr['hourly_rate'];?> Per Hour</td>
                                                      </tr>
                                                      <tr>
                                                          <th style="width:25%;">Available Positions </th>
                                                          <td style="border-right:1px solid #eee; width:25%;"><?echo $bid_req_info['amount']?></td>
                                                          <th style="width:25%;">Accepted Bids</th>
                                                          <td style=" width:25%;"><?echo $accepted_amount['total'];?></td>
                                                      </tr>
                                                    <?}?>
                                                </table>


                                                <div class="bottom_table_holder">


                                                <table id="bottom_table" class="">
                                                  <tr>
                                                    <th colspan="6" style="border-right:0px; text-align:center;">
                                                      Shift response
                                                    </th>
                                                  </tr>
                                                  <tr style="background-color:#eee;">
                                                    <th>Account Name</th>
                                                    <th>Role</th>
                                                    <th>Responce Status</th>
                                                    <th>Seen Date</th>
                                                    <th>Approval Date</th>
                                                    <th>Action</th>
                                                  </tr>
                                                  <?foreach ($bid_req_info_all as $br) {?>
                                                    <?
                                                      $db->query("select * from ws_accounts where id = ?");
                                                      $db->bind(1,$br['account_id']);
                                                      $account_info = $db->single();

                                                      $db->query("select * from ws_rota_bid_amount where id = ? and pid = ? and status = 1");
                                                      $db->bind(1,$br['bid_amount_pid']);
                                                      $db->bind(2,$id);
                                                      $bid_amount_single = $db->single();

                                                      $db->query("select * from ws_roles where id = ?");
                                                      $db->bind(1,$br['role_id']);
                                                      $bid_role = $db->single();
                                                    ?>
                                                    <tr>
                                                      <td><a href="<?echo $fullurl;?>admin/rota/admin/view_responses_profile.php?id=<?echo $account_info['id'];?>&bid=<?echo $id;?>"><?echo $account_info['first_name'].' '.$account_info['surname'];?></a></td>
                                                      <td><?echo $bid_role['title'];?></td>
                                                      <td>
                                                        <?if($br['accepted_user'] == 0){ echo 'Pending'; }else if($br['accepted_user'] == 3){ echo 'Did Not Attend'; }else if($br['accepted_user'] == 1){ if($br['accepted_admin'] == 1){ echo 'Accepted - Admin'; }else if($br['accepted_admin'] == 2){ echo 'Declined - Admin'; }else{ echo 'Accepted - User'; } }else if($br['accepted_user'] == 2){ echo 'Declined'; }else{ echo 'Error'; }?></td>
                                                      <td><?if($br['seen'] == 1){echo date('d-m-Y H:i', $br['seen_date']);}?></td>
                                                      <td><?if($br['accepted_user'] == 1 || $br['accepted_user'] == 2){ echo date('d-m-Y H:i', $br['choice_date']); }?></td>
                                                      <td>
                                                        <?if($bid_amount_single['completed'] == 1){echo 'Role Position Filled';}?>
                                                        <?if($br['accepted_admin'] != 2 && $bid_amount_single['completed'] == 0 && $br['accepted_user'] == 1){?>
                                                        <button type="button" class="btn btn-sm btn-raised btn-warning pull-right admin_response" style="margin:0px;" data-id="<?echo $br['id'];?>" data-type="decline">Decline</button>
                                                        <?}?>
                                                        <?if($br['accepted_admin'] != 1 && $bid_amount_single['completed'] == 0 && $br['accepted_user'] == 1){?>
                                                        <button type="button" class="btn btn-sm btn-raised btn-success pull-right admin_response" style="margin:0px;" data-id="<?echo $br['id'];?>" data-type="accept">Accept</button>
                                                        <?}?>
                                                      </td>
                                                    </tr>
                                                    <?}?>
                                                </table>
                                              </div>






                                              </div>





                                            </div>
                                    </div>
                                </div>



            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>


</body>
</html>

<!-- PAGE JS -->



<script>

var bid_id = '<?echo $id;?>';

$( "body" ).on( "click", ".admin_response", function() {
    var type = $(this).data('type');
    var data = $(this).data('id');

    // alert(type);
    // alert(data);

    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/admin_save_response.php",
          data: {'pid':data,"type":type,'bid_id':bid_id},
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Response Updated',
                    type: 'info',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //Refresh the Page
                window.location.reload();

              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });

});

$( "body" ).on( "click", "#resend_shift", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  $.ajax({
     type: "POST",
     url: $fullurl+"admin/rota/form/resend_shift_confirm.php",
     success: function(msg){
       $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
       }
});
});


$( "body" ).on( "click", "#confirm_resend_shift", function() {
  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/confirm_resend_shift.php",
        data: {'bid_id':bid_id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
              Messenger().post({
                  message: 'Shift Resent',
                  type: 'info',
                  showCloseButton: false
              });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');

              //Refresh the Page
              window.location.reload();

            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});


$( "body" ).on( "click", "#resend_shift_manual", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  $.ajax({
     type: "POST",
     url: $fullurl+"admin/rota/form/resend_shift_manual.php",
     data: {'bid_id':bid_id},
     success: function(msg){
       $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
       }
});
});




$( "body" ).on( "click", ".confirm-manual-add", function() {
  var id = $(this).data('id');

  var user_account = $('.'+id+'').val();

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/confirm_manual_shift.php",
        data: {'bid_id':bid_id,'bid_am':id,'user':user_account},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
              Messenger().post({
                  message: 'Shift Assigned',
                  type: 'info',
                  showCloseButton: false
              });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');

              //Refresh the Page
              window.location.reload();

            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });

});


$( "body" ).on( "click", "#send_shift", function() {

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/send_shift.php",
        data: {'bid_id':bid_id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
              Messenger().post({
                  message: 'Shift Sent',
                  type: 'info',
                  showCloseButton: false
              });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');

              //Refresh the Page
              window.location.reload();

            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });

});


$( "body" ).on( "click", "#recalculate_shift", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  $.ajax({
     type: "POST",
     url: $fullurl+"admin/rota/form/recalculate_shift_confirm.php",
     success: function(msg){
       $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
       }
});
});


$( "body" ).on( "click", "#confirm_recalculate_shift", function() {

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/confirm_recalculate_shift.php",
        data: {'bid_id':bid_id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
              Messenger().post({
                  message: 'Shift Recalculated.',
                  type: 'info',
                  showCloseButton: false
              });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');

              //Refresh the Page
              window.location.reload();

            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });

});
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
