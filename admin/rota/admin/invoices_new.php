<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Invoices';

 $db->query("select * FROM ws_rota_bid where status = 1");
 $data = $db->resultset();



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  margin-bottom:15px;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                    <div class="page_title text-capitalize">
                                        <span class="menuglyph glyphicons glyphicons-calendar page-title-glyph rota_background" aria-hidden="true"></span>
                                        <span class="menuText">Invoices</span>


                                    </div>




								<div class="clearfix"></div>



                                            <?foreach ($data as $d) {?>
                                              <?
                                                $db->query("select * from ws_rota_bid_request where pid = ? and accepted_admin = ? and status = 1");
                                                $db->bind(1,$d['id']);
                                                $db->bind(2,'1');
                                                $accepted_bids = $db->resultset();
                                                //I would loop through those and then I would need to add up the total


                                              ?>
                                              <?if(!empty($accepted_bids)){?>
                                                <table>
                                                    <tr style="background-color:#f0f0f0;">
                                                        <th>Date</th>
                                                        <th>Contractor</th>
                                                        <th>Status</th>
                                                        <th>Paid Date</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                    <?foreach ($accepted_bids as $abid) {?>
                                                      <?
                                                        $db->query("select * from ws_accounts where id = ?");
                                                        $db->bind(1,$abid['account_id']);
                                                        $account = $db->single();

                                                        $db->query("select * from ws_service_roles where id = ?");
                                                        $db->bind(1,$abid['service_role_id']);
                                                        $service_role = $db->single();

                                                        //How many hours
                                                        $hourly_rate = $service_role['hourly_rate'];
                                                        $time1 = $d['start_final'];
                                                        $time2 = $d['end_final'];
                                                        $difference = round(abs($time2 - $time1) / 3600,2);
                                                      ?>
                                                      <tr>
                                                        <td><?echo date('d-m-Y',$d['start_final']);?></td>
                                                        <td><?echo $account['first_name'].' '.$account['surname'];?></td>
                                                        <td><?if($d['invoice_paid'] == 0){echo 'Not Paid';}else{echo 'Paid';}?></td>
                                                        <td><?if(!empty($d['paid_date'])){echo date('d-m-Y', $d['paid_date']);}?></td>
                                                        <td>£<?echo $difference * $hourly_rate;?></td>
                                                      </tr>
                                                    <?}?>
                                                    <tr>
                                                      <td colspan="5">
                                                        <?if($d['invoice_paid'] == 0){?>
                                                          <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="pay_invoice" data-id="<?echo $d['id'];?>">Pay</button>
                                                        <?}?>

                                                      </td>
                                                    </tr>
                                                </table>

                                                <?}?>
                                            <?}?>






            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>


</body>
</html>

<!-- PAGE JS -->



<script>


$( "body" ).on( "click", "#pay_invoice", function() {
  var id = $(this).data('id');

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/invoice_paid.php",
        data: {'id':id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
            Messenger().post({
                message: 'Invoice Paid.',
                type: 'error',
                showCloseButton: false
            });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});


</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
