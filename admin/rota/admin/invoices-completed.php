<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Invoices';

 $today_minus_72 = strtotime(" - 3 day");

 $subadmin_search = '';
 if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
   $subadmin_search = ' and location in ("0",';
   $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

   foreach ($my_areas as $ma) {
     $subadmin_search .='"'.$ma.'",';
   }

   $subadmin_search .= '"0")';

   $subadmin_query_pcn = sub_admin_query('pcn_id');
   $subadmin_query_practice = sub_admin_query('practice_id','or');



   if(!empty($subadmin_query_pcn) && !empty($subadmin_query_practice)){
     $subadmin_search .= " and($subadmin_query_pcn $subadmin_query_practice)";
   }
 }


 $db->query("select distinct member_id from hx_rota where status = 1 and invoice_paid = 1 and dna = 0 and end_ts < ? $subadmin_search");
 $db->bind(1,$today_minus_72);
 $data = $db->resultset();




 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  margin-bottom:15px;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

.bottom_table_holder{
  overflow: scroll;
}

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->


                    <div class="page_title text-capitalize"><img src="/staff-box/images/pink.png" height="75" /> <span class="menuText">Invoices - Completed</span> </div>
                    <div class="clearfix"></div>

                    <?if(empty($data)){?>
                      <table>
                        <tr>
                          <th style="text-align:center;">
                            No Oustanding Invoices to be Paid.
                          </th>
                        </tr>
                      </table>
                    <?}?>

                    <?foreach ($data as $d) {?>
                      <?
                      $db->query("select * from hx_rota where status = 1 and invoice_paid = 0 and dna = 0 and end_ts < ? and member_id = ?");
                      $db->bind(1,$today_minus_72);
                      $db->bind(2,$d['member_id']);
                      $rota_info = $db->resultset();


                      $db->query("select * from ws_accounts where id = ?");
                      $db->bind(1,$d['member_id']);
                      $account = $db->single();

                      $running_total = 0;
                      $show = 0;
                      foreach ($rota_info as $ri) {
                        $db->query("select * from ws_roles where id = ?");
                        $db->bind(1,$ri['role']);
                        $role = $db->single();

                        if($role['id'] != 9){
                          $show = 1;
                        }
                      }

                      ?>
                      <?if($show == 1){?>
                      <div class="bottom_table_holder">
                        <table>
                          <tr style="background-color:#eee;">
                            <th>Contractor</th>
                            <th>Service</th>
                            <th>Role</th>
                            <th>Start Date / Time</th>
                            <th>End Date / Time</th>
                            <th>Hourly Rate</th>
                            <th>Amount</th>
                          </tr>
                          <?foreach ($rota_info as $ri) {?>
                            <?
                              $db->query("select * from ws_service where id = ?");
                              $db->bind(1,$ri['service_id']);
                              $service = $db->single();

                              $db->query("select * from ws_roles where id = ?");
                              $db->bind(1,$ri['role']);
                              $role = $db->single();

                              $db->query("select * from ws_service_roles where pid = ? and job_role = ?");
                              $db->bind(1,$ri['service_id']);
                              $db->bind(2,$ri['role']);
                              $service_role = $db->single();

                              $hourly_rate = preg_replace("/[^0-9.]/", "",$service_role['hourly_rate']);
                              $time1 = $ri['start_ts'];
                              $time2 = $ri['end_ts'];
                              $difference = round(abs($time2 - $time1) / 3600,2);



                              $running_total = $running_total + ($difference * $hourly_rate);
                            ?>
                            <tr>
                              <td><?echo $account['first_name'].' '.$account['surname'];?></td>
                              <td><?echo $service['title'];?></td>
                              <td><?echo $role['title'];?></td>
                              <td><?echo date('d-m-Y H:i',$ri['start_ts'])?></td>
                              <td><?echo date('d-m-Y H:i',$ri['end_ts'])?></td>
                              <td><?echo $service_role['hourly_rate'];?> Per Hour</td>
                              <td>£<?echo $difference * $hourly_rate;?></td>
                            </tr>
                          <?}?>
                          <tr>
                            <td colspan="5"> </td>
                            <th>Total:</th>
                            <td>£<?echo $running_total;?></td>
                          </tr>
                        </table>
                      </div>
                    <?}}?>






            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>
<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>

</body>
</html>

<!-- PAGE JS -->



<script>


$( "body" ).on( "click", "#pay_invoice", function() {
  var id = $(this).data('id');

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/invoice_paid.php",
        data: {'id':id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
            Messenger().post({
                message: 'Invoice Paid.',
                type: 'error',
                showCloseButton: false
            });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});


$( "body" ).on( "click", "#mark_as_paid", function() {
  var id = $(this).data('id');

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/invoice_paid_new.php",
        data: {'id':id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
            Messenger().post({
                message: 'Invoice Paid.',
                type: 'error',
                showCloseButton: false
            });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});

$( "body" ).on( "click", "#view_bank_details", function() {
  $("#BaseModalMContent").html($Loader);
  $('#BaseModalM').modal('show');

  var account_id = $(this).data('id');

	  $.ajax({
	     type: "POST",
	     url: $fullurl+"admin/rota/form/view_bd.php?id="+account_id,
	     success: function(msg){
	       $("#BaseModalMContent").delay(1000)
	        .queue(function(n) {
	            $(this).html(msg);
	            n();
	        }).fadeIn("slow").queue(function(n) {
	                     $.material.init();
	                    n();
											// setTimeout(function(){
											// 	$('.modal').modal('hide');
											// },10000);
	        });
	       }
	});
});


</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
