<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Staff Box - Services';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize">
                                    <span class="menuglyph glyphicons glyphicons-calendar page-title-glyph rota_background" aria-hidden="true"></span>
                                    <span class="menuText">Services</span>


                                </div>





								<div class="clearfix"></div>

                                <button type="button" class="btn btn-primary pull-left rota_background main_button" id="add_new_service">Add Service</button>

                                            <table id="table" class="striped"
                                                    data-toggle="table"
                                                    data-show-export="false"
                                                    data-export-types="['excel']"
                                                    data-click-to-select="true"
                                                    data-filter-control="false"
                                                    data-show-columns="false"
                                                    data-show-refresh="false"
                                                    data-url="<? echo $fullurl; ?>admin/rota/ajax/table/view_services.php"
                                                    data-height="400"
                                                    data-side-pagination="server"
                                                    data-pagination="true"
                                                    data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                    data-search="true">
                                                <thead>
                                                    <tr>
                                                            <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                                                            <th data-field="title" data-sortable="true" data-formatter="LinkViewServiceFormatter">Title</th>
                                                            <th data-field="created_date" data-sortable="true" data-visible="true" data-formatter="DateTimeFormatter">Created Date</th>

                                                    </tr>
                                                </thead>
                                        </table>





            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>


</body>
</html>

<!-- PAGE JS -->



<script>

$( "body" ).on( "click", "#add_new_service", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/add_service.php",
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});

$( "body" ).on( "change", "#job_role", function() {
    var values = $(this).val();

    $('.hourly_rate_holders').hide();
    $('.hourly_rate_holders').children('input').removeAttr('required');

    $.each(values, function( index, value ) {
        $('.hourly_rate_holder_'+value+'').show();
        $('#hourly_rate_'+value+'').prop('required',true);
    });

});


$( "body" ).on( "click", "#save_add_service", function() {
    var formid = '#add_service_form';
    var HasError = 0;

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_service.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Service Created.',
                        type: 'error',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }
});


$( "body" ).on( "click", "#view_service", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service.php?id="+id,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});

$( "body" ).on( "click", "#edit_service", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/edit_service.php?id="+id,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});

$( "body" ).on( "click", "#save_edit_service", function() {
    var formid = '#edit_service_form';
    var HasError = 0;

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_edit_service.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Service Updated.',
                        type: 'error',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }
});

$( "body" ).on( "click", "#delete_service", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/delete_service.php?id="+id,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});


$( "body" ).on( "click", "#confirm_delete_service", function() {
        var id = $(this).data('id');

        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/delete_service.php",
              data: {'id':id},
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Service Deleted.',
                        type: 'error',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });

});
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
