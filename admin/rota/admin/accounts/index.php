<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Account Approval';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>
.form-control {
/* margin-bottom: 7px; */
-webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
padding: 0px 20px;
border-radius: 10px;
/* margin: 10px 0px; */
background-image: none;
width: 300px;
}
.form-control::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: black;
            opacity: 1; /* Firefox */
}

.form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: black;
}

.form-control::-ms-input-placeholder { /* Microsoft Edge */
            color: black;
 }
 .th-inner{
     background-color: #F0F0F0;
 }
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                            <div class="page_title text-capitalize">
                                <img src="/staff-box/images/pink.png" height="75" />
                                <span class="menuText">System Users</span>


                            </div>




								<div class="clearfix"></div>


                                    <button type="button" class="btn btn-primary  rota_background main_button pull-right" id="add_new_user_p">Add User<span class="glyphicons glyphicons-plus" style="margin-left:0px;"></span></button>


                                            <table id="table" class="striped"
                                                        data-toggle="table"
                                                        data-show-export="false"
                                                        data-export-types="['excel']"
                                                        data-click-to-select="true"
                                                        data-filter-control="false"
                                                        data-show-columns="false"
                                                        data-show-refresh="false"
                                                        data-url="<? echo $fullurl; ?>admin/rota/ajax/table/view_all_users.php"
                                                        data-height="400"
                                                        data-side-pagination="server"
                                                        data-pagination="true"
                                                        data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                        data-search="true"
                                                        data-search-align="left"
                                                        data-row-attributes="rowAttributes">
                                                    <thead>
                                                        <tr>
                                                                <th data-field="acc_id" data-sortable="true" data-visible="false">ID</th>
                                                                <th data-field="name" data-sortable="true" data-halign="center" data-formatter="LinkEditSystemUserFormatter">Name</th>
                                                                <th data-field="email" data-sortable="true" data-halign="center">Email</th>
                                                                <th data-field="account_type" data-sortable="true" data-halign="center">Account Type</th>
                                                        </tr>
                                                    </thead>
                                            </table>






            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>
    <script src="<? echo $fullurl; ?>assets/js/table.js?1"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>

<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->



<script>

$(document).ready(function () {
    var timer;
    $(document).on('mouseenter', '#table tr', function () {
        var that = $(this);
        timer = setTimeout(function(){
            that.find("#view_account").fadeIn(100);
            that.find("#account_time").hide();
        }, 500);


    }).on('mouseleave', '#table tr', function () {
        var that = $(this);
        $(this).find("#view_account").fadeOut(200);
        setTimeout(function(){
            that.find("#account_time").fadeIn(200);
        }, 200);

        clearTimeout(timer);
    });
});

$(document).ready(function () {
    var timer;
    $(document).on('mouseenter', '#table tr', function () {
        var that = $(this);
        timer = setTimeout(function(){
            that.find(":button").fadeIn(100);
        }, 500);


    }).on('mouseleave', '#table tr', function () {
        $(this).find(":button").fadeOut(200);
        clearTimeout(timer);
    });
});

$( "body" ).on( "click", "#add_new_user_p", function() {
    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
        type: "POST",
        url: "../../../../admin/rota/form/add_new_user.php",
        success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();

                       $('#dna_message').summernote({
                     height: 200,                 // set editor height
                     minHeight: null,             // set minimum height of editor
                     maxHeight: null,             // set maximum height of editor
                     focus: true,                  // set focus to editable area after initializing summernote
                     toolbar: [
                       ['font', ['bold', 'italic', 'underline']],
                       ['color', ['color']],
                       ['para', ['ul', 'ol', 'paragraph']],
                       ['table', ['table']],
                       ['insert', ['link', 'hr']],
                       ['help', ['help']]
                       ]
                   });

                   $('#location_multiple').selectpicker();
           });
         }
    });
});


$( "body" ).on( "change", "#type", function() {
    var option = $(this).val();

    if(option == 'admin'){
        $("#location_multiple").val([]);
        $('#location_multiple').attr('disabled','disabled');
    }else{
        $('#location_multiple').removeAttr('disabled');
    }

    $('#location_multiple').selectpicker('refresh');
});




$( "body" ).on( "change", "#location_multiple", function() {

  var selected = $(this).val();
  console.log(selected);



    $('.pcn-practice-div').show();
    $('.pcn-practice-div').html($Loader);

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/get_pcn_practice_user.php",
       data: {'location':selected},
       success: function(msg){
         $(".pcn-practice-div").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();

                      $('#pcn_practice').selectpicker();
          });
         }
     });

});






$( "body" ).on( "click", "#NewUserFormBut", function() {
                  var HasError = 0;
                  var formid = '#NewUserForm';
                  var FRMdata = $(formid).serialize(); // get form data
                  $pass1=$('#newpassword').val();
                  $pass2=$('#confirmnewpassword').val();

                    $(formid).find('input').each(function(){
                      $(this).parent().removeClass('has-error');
                      Messenger().hideAll();
                      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                               if(!$(this).prop('required')){}
                               else {
                            HasError = 1;
                            $(this).parent().addClass('has-error');
                          }
                        }
                    });
                    if (HasError == 1) {
                      Messenger().post({
                          message: 'Please make sure all required elements of the form are filled out.',
                          type: 'error',
                          showCloseButton: false
                      });
                    }
                    else{
                      if ($pass1==$pass2 && $.trim($pass1).length) {
                     $.ajax({
                             type: "POST",
                             url: $fullurl+"admin/rota/admin/accounts/index.php",
                             data: FRMdata, // serializes the form's elements.
                             success: function(msg){
                                 $message=$.trim(msg);
                                 Messenger().post({
                                         message: 'New User Created.',
                                         showCloseButton: false
                                 });
                                 setTimeout(function(){
                                   $('#BaseModalL').modal('hide');
                                   $('#table').bootstrapTable('refresh');
                                 },600);

                              },error: function (xhr, status, errorThrown) {
                             setTimeout(function(){alert('Error');},300);
                           }
                     });
                      }
                      else{
                         Messenger().post({
                          message: 'Password Error',
                          type: 'error',
                          showCloseButton: false
                   });
                      }
                  }
            });


            $( "body" ).on( "click", "#edit_system_user", function() {
                $("#BaseModalLContent").html($Loader);
                $('#BaseModalL').modal('show');

                var id = $(this).data('id');


                $.ajax({
                    type: "POST",
                    url: "../../../../admin/rota/form/edit_new_user.php?id="+id,
                    success: function(msg){
                      //alert(msg);
                      $("#BaseModalLContent").delay(1000)
                       .queue(function(n) {
                           $(this).html(msg);
                           n();
                       }).fadeIn("slow").queue(function(n) {
                                    $.material.init();
                                   n();

                                   $('#dna_message').summernote({
                                 height: 200,                 // set editor height
                                 minHeight: null,             // set minimum height of editor
                                 maxHeight: null,             // set maximum height of editor
                                 focus: true,                  // set focus to editable area after initializing summernote
                                 toolbar: [
                                   ['font', ['bold', 'italic', 'underline']],
                                   ['color', ['color']],
                                   ['para', ['ul', 'ol', 'paragraph']],
                                   ['table', ['table']],
                                   ['insert', ['link', 'hr']],
                                   ['help', ['help']]
                                   ]
                               });

                               $('#location_multiple').selectpicker();
                               $('#pcn_practice').selectpicker();
                       });
                     }
                });
            });



            $( "body" ).on( "click", "#edit_new_account", function() {
                              var HasError = 0;
                              var formid = '#edit_new_user';
                              var FRMdata = $(formid).serialize(); // get form data

                                $(formid).find('input').each(function(){
                                  $(this).parent().removeClass('has-error');
                                  Messenger().hideAll();
                                  if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                                           if(!$(this).prop('required')){}
                                           else {
                                        HasError = 1;
                                        $(this).parent().addClass('has-error');
                                      }
                                    }
                                });
                                if (HasError == 1) {
                                  Messenger().post({
                                      message: 'Please make sure all required elements of the form are filled out.',
                                      type: 'error',
                                      showCloseButton: false
                                  });
                                }
                                else{

                                 $.ajax({
                                         type: "POST",
                                         url: $fullurl+"admin/rota/ajax/update_system_account.php",
                                         data: FRMdata, // serializes the form's elements.
                                         success: function(msg){
                                             $message=$.trim(msg);
                                             Messenger().post({
                                                     message: 'System User Updated.',
                                                     showCloseButton: false
                                             });
                                             setTimeout(function(){
                                               $('#BaseModalL').modal('hide');
                                               $('#table').bootstrapTable('refresh');
                                             },600);

                                          },error: function (xhr, status, errorThrown) {
                                         setTimeout(function(){alert('Error');},300);
                                       }
                                 });

                              }
                        });
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
