<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Account Approval';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>
.form-control {
/* margin-bottom: 7px; */
-webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
padding: 0px 20px;
border-radius: 10px;
/* margin: 10px 0px; */
background-image: none;
/* width: 300px; */
}
.form-control::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: black;
            opacity: 1; /* Firefox */
}

.form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: black;
}

.form-control::-ms-input-placeholder { /* Microsoft Edge */
            color: black;
 }

 .th-inner{
     background-color: #F0F0F0;
 }
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                            <div class="page_title text-capitalize">
                                <img src="/staff-box/images/pink.png" height="75" />
                                <span class="menuText">Accounts Approval</span>


                            </div>

								<div class="clearfix"></div>



                                            <table id="table" class="striped"
                                                    data-toggle="table"
                                                    data-show-export="false"
                                                    data-export-types="['excel']"
                                                    data-click-to-select="true"
                                                    data-filter-control="false"
                                                    data-show-columns="false"
                                                    data-show-refresh="false"
                                                    data-url="<? echo $fullurl; ?>admin/rota/ajax/table/account_approval.php"
                                                    data-height="400"
                                                    data-side-pagination="server"
                                                    data-pagination="true"
                                                    data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                    data-search="true"
                                                    data-search-align="left"
                                                    data-row-attributes="rowAttributes">
                                                <thead>
                                                    <tr>
                                                            <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                                                            <th data-field="name" data-sortable="true" data-formatter="LinkAccountApproveFormatter" data-halign="center">Name</th>
                                                            <th data-field="email" data-sortable="true" data-visible="true" data-halign="center">Email</th>
                                                            <th data-field="email" data-sortable="true" data-visible="false" data-halign="center">Telephone</th>
                                                            <th data-field="user_status" data-sortable="true" data-visible="false" data-halign="center">Status</th>
                                                            <th data-field="created_date" data-sortable="true" data-visible="true" data-formatter="DateTimeFormatterAccountApproval" data-halign="center">Created Date</th>

                                                    </tr>
                                                </thead>
                                        </table>





            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    </div>
<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>

</body>
</html>

<!-- PAGE JS -->



<script>
$(document).ready(function () {
    var timer;
    $(document).on('mouseenter', '#table tr', function () {
        var that = $(this);
        timer = setTimeout(function(){
            that.find("#view_account_approval").fadeIn(100);
            that.find("#account_time").hide();
        }, 500);


    }).on('mouseleave', '#table tr', function () {
        var that = $(this);
        $(this).find("#view_account_approval").fadeOut(200);
        setTimeout(function(){
            that.find("#account_time").fadeIn(200);
        }, 200);

        clearTimeout(timer);
    });
});


$( "body" ).on( "click", "#view_account_approval", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/ajax/view_approval_account.php",
       data: {'id':id},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
  });
});

$( "body" ).on( "click", "#decline_rota_account", function() {
    var id = $(this).data('id');

    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/reject_account.php?id="+id,
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Account Rejected.',
                    type: 'error',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //window.location.replace($fullurl+'admin/index2.php?account_waiting');
                //We show a message at the topo fo the page for the user so they know what is happening.
                window.location.reload();
              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });
});

$( "body" ).on( "click", "#approval_rota_account", function() {
    var id = $(this).data('id');

    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/approve_account.php?id="+id,
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Account Approved.',
                    type: 'success',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //window.location.replace($fullurl+'admin/index2.php?account_waiting');
                //We show a message at the topo fo the page for the user so they know what is happening.
                window.location.reload();
              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });
});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
