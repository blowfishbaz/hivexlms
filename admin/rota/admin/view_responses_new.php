<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $id = $_GET['id'];

 $db->query("select * from ws_rota_bid where id = ?");
 $db->bind(1,$id);
 $bid_info = $db->single();

 // $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
 // $db->bind(1,$id);
 // $bid_req_info = $db->resultset();

 $db->query("select * from ws_rota_bid_request where pid = ? and status = 1");
 $db->bind(1,$id);
 $bid_req_info_all = $db->resultset();

 $db->query("select * from ws_service where id = ?");
 $db->bind(1,$bid_info['service_id']);
 $service = $db->single();

 $db->query("select ws_service_roles.*, ws_roles.title from ws_service_roles left join ws_roles on ws_roles.id = ws_service_roles.job_role where ws_service_roles.pid = ?");
 $db->bind(1,$bid_info['service_id']);
 $service_roles = $db->resultset();

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
}

#role_info_table > thead > tr > th:last-of-type{
  border-right:0px;
}

#role_info_table > tbody > tr > td{
  border-right:1px solid #dddddd;
}

#role_info_table > tbody > tr > td:last-of-type{
  border-right:0px;
}

#bid_req_table{
  margin-bottom:10px;
}

#bid_req_table td{
  border: 1px solid #dddddd;
}

#bid_req_table th{
  border: 1px solid #dddddd;
}

#top_table{
  border-radius: 20px 20px 20px 20px;
  -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.8);
  -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.8);
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.8);
  margin-bottom:40px;
}

#bottom_table{
  border-radius: 20px 20px 20px 20px;
  -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.8);
  -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.8);
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.8);
}
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-parents" aria-hidden="true"></span><span class="menuText">View Responses</span></div>





								<div class="clearfix"></div>
                                <div class="row">
                                    <div class="row">
                                            <div class="panel-body">

                                              <div class="col-lg-12">
                                                <table id="top_table">
                                                  <tr>
                                                    <th colspan="4" style="border-right:0px; text-align:center;">
                                                      Shift Details
                                                    </th>
                                                  </tr>
                                                  <?if($bid_info['completed'] == 1){?>
                                                    <tr>
                                                      <th colspan="4" style="border-right:0px;">Completed</th>
                                                    </tr>
                                                  <?}?>
                                                    <tr>
                                                        <th colspan="2">Title</th>
                                                        <td colspan="2"><?echo $service['title'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Description</th>
                                                        <td colspan="2"><?echo $service['description'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Location</th>
                                                        <td colspan="2"><?echo $service['location'];?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Start Date / Time</th>
                                                        <td colspan="2"><?echo date('d-m-Y H:i',$bid_info['start_final']);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">End Date / Time</th>
                                                        <td colspan="2"><?echo date('d-m-Y H:i',$bid_info['end_final']);?></td>
                                                    </tr>
                                                    <tr>
                                                      <th colspan="4" style="border-right:0px; text-align:center;">
                                                        Role Details
                                                      </th>
                                                    </tr>
                                                    <?foreach ($service_roles as $sr) {?>
                                                      <?
                                                      $db->query("select * from ws_rota_bid_amount where pid = ? and role_id = ? and status = 1");
                                                      $db->bind(1,$id);
                                                      $db->bind(2,$sr['job_role']);
                                                      $bid_req_info = $db->single();

                                                      $db->query("select COALESCE(sum(accepted_admin),0) as total from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and status = 1 and accepted_admin = 1");
                                                      $db->bind(1,$id);
                                                      $db->bind(2,$bid_req_info['id']);
                                                      $accepted_amount = $db->single();
                                                      ?>
                                                      <tr>
                                                          <th colspan="2"><?echo $sr['title'];?></th>
                                                          <td colspan="2">£<?echo $sr['hourly_rate'];?> Per Hour</td>
                                                      </tr>
                                                      <tr>
                                                          <th>Available Positions </th>
                                                          <td style="border-right:1px solid #eee;"><?echo $bid_req_info['amount']?></td>
                                                          <th>Accepted Bids</th>
                                                          <td><?echo $accepted_amount['total'];?></td>
                                                      </tr>
                                                    <?}?>
                                                </table>



                                                <table id="bottom_table">
                                                  <tr>
                                                    <th colspan="6" style="border-right:0px; text-align:center;">
                                                      Shift response
                                                    </th>
                                                  </tr>
                                                  <tr>
                                                    <th>Account Name</th>
                                                    <th>Role</th>
                                                    <th>Responce Status</th>
                                                    <th>Seen Date</th>
                                                    <th>Approval Date</th>
                                                    <th>Action</th>
                                                  </tr>
                                                  <?foreach ($bid_req_info_all as $br) {?>
                                                    <?
                                                      $db->query("select * from ws_accounts where id = ?");
                                                      $db->bind(1,$br['account_id']);
                                                      $account_info = $db->single();

                                                      $db->query("select * from ws_rota_bid_amount where id = ? and pid = ? and status = 1");
                                                      $db->bind(1,$br['bid_amount_pid']);
                                                      $db->bind(2,$id);
                                                      $bid_amount_single = $db->single();

                                                      $db->query("select * from ws_roles where id = ?");
                                                      $db->bind(1,$br['role_id']);
                                                      $bid_role = $db->single();
                                                    ?>
                                                    <tr>
                                                      <td><?echo $account_info['first_name'].' '.$account_info['surname'];?></td>
                                                      <td><?echo $bid_role['title'];?></td>
                                                      <td><?if($br['accepted_user'] == 0){echo 'Pending';}else if($br['accepted_user'] == 1){if($br['accepted_admin'] == 1){echo 'Accepted - Admin';}else if($br['accepted_admin'] == 2){echo 'Declined - Admin';}else{echo 'Accepted - User';} }else if($br['accepted_user'] == 2){echo 'Declined';}else{echo 'Error';}?></td>
                                                      <td><?if($br['seen'] == 1){echo date('d-m-Y H:i', $br['seen_date']);}?></td>
                                                      <td><?if($br['accepted_user'] == 1 || $br['accepted_user'] == 2){ echo date('d-m-Y H:i', $br['choice_date']); }?></td>
                                                      <td>
                                                        <?if($bid_amount_single['completed'] == 1){echo 'Role Position Filled';}?>
                                                        <?if($br['accepted_admin'] != 2 && $bid_amount_single['completed'] == 0 && $br['accepted_user'] == 1){?>
                                                        <button type="button" class="btn btn-sm btn-raised btn-warning pull-right admin_response" style="margin:0px;" data-id="<?echo $br['id'];?>" data-type="decline">Decline</button>
                                                        <?}?>
                                                        <?if($br['accepted_admin'] != 1 && $bid_amount_single['completed'] == 0 && $br['accepted_user'] == 1){?>
                                                        <button type="button" class="btn btn-sm btn-raised btn-success pull-right admin_response" style="margin:0px;" data-id="<?echo $br['id'];?>" data-type="accept">Accept</button>
                                                        <?}?>
                                                      </td>
                                                    </tr>
                                                    <?}?>
                                                </table>
                                              </div>





                                            </div>
                                    </div>
                                </div>



            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>


</body>
</html>

<!-- PAGE JS -->



<script>

var bid_id = '<?echo $id;?>';

$( "body" ).on( "click", ".admin_response", function() {
    var type = $(this).data('type');
    var data = $(this).data('id');

    // alert(type);
    // alert(data);

    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/admin_save_response.php",
          data: {'pid':data,"type":type,'bid_id':bid_id},
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Response Updated',
                    type: 'info',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //Refresh the Page
                window.location.reload();

              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });

});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
