<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Account Approval';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>
.form-control {
/* margin-bottom: 7px; */
-webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
padding: 0px 20px;
border-radius: 10px;
/* margin: 10px 0px; */
background-image: none;
/* width: 300px; */
}
.form-control::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: black;
            opacity: 1; /* Firefox */
}

.form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: black;
}

.form-control::-ms-input-placeholder { /* Microsoft Edge */
            color: black;
 }
 .glyphicons-plus:before{
     padding-right:0px;
 }

 .th-inner{
     background-color: #F0F0F0;
 }

 #reoccur_table td{
   border: 1px solid #333;
 }
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                            <div class="page_title text-capitalize">
                                <img src="/staff-box/images/pink.png" height="75" />
                                <span class="menuText">New Rota</span>


                            </div>

								<div class="clearfix"></div>
                                    <button type="button" class="btn btn-warning btn-raised pull-right main_button" id="add_new_bid_manual">Manual Shift <span class="glyphicons glyphicons-plus" style="margin-left:0px;"></span></button>
                                    <button type="button" class="btn btn-info btn-raised pull-right main_button" id="add_new_bid">Send Shift <span class="glyphicons glyphicons-plus" style="margin-left:0px;"></span></button>
                                    <button type="button" class="btn btn-danger btn-raised pull-right main_button" id="recalculate_all_shifts" style="color:white!important;">Recalculate All <span class="glyphicons glyphicons-refresh" style="margin-left:0px; color:white;"></span></button>

                                            <table id="table" class="striped"
                                                    data-toggle="table"
                                                    data-show-export="false"
                                                    data-export-types="['excel']"
                                                    data-click-to-select="true"
                                                    data-filter-control="false"
                                                    data-show-columns="false"
                                                    data-show-refresh="false"
                                                    data-url="<? echo $fullurl; ?>admin/rota/ajax/table/view_rota_bid.php"
                                                    data-height="400"
                                                    data-side-pagination="server"
                                                    data-pagination="true"
                                                    data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                    data-search="true"
                                                    data-search-align="left"
                                                    data-row-attributes="rowAttributes">
                                                <thead>
                                                    <tr>
                                                            <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                                                            <th data-field="title" data-sortable="true" data-formatter="LinkViewBidFormatter" data-width="500" data-halign="center">Service Title</th>
                                                            <th data-field="location" data-sortable="true" data-halign="center">Location</th>
                                                            <th data-field="start_final" data-sortable="true" data-formatter="DateTimeFormatter" data-halign="center">Start Date</th>
                                                            <th data-field="end_final" data-sortable="true" data-visible="false" data-formatter="DateTimeFormatter" data-halign="center">End Date</th>
                                                            <th data-field="accepted" data-sortable="true" data-visible="false" data-halign="center"># Accepts</th>
                                                            <th data-field="created_date" data-sortable="true" data-visible="false" data-formatter="DateTimeFormatter" data-halign="center">Created Date</th>
                                                            <th data-field="completed" data-sortable="true" data-visible="true" data-formatter="rotaBidStatusFormatter" data-halign="center">Status</th>

                                                    </tr>
                                                </thead>
                                        </table>





            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>
<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>

</body>
</html>

<!-- PAGE JS -->




<script>

$(document).ready(function () {
    var timer;
    $(document).on('mouseenter', '#table tr', function () {
        var that = $(this);
        timer = setTimeout(function(){
            that.find("#view_bid").fadeIn(100);
            //that.find("#bid_status").hide();
        }, 500);


    }).on('mouseleave', '#table tr', function () {
        var that = $(this);
        $(this).find("#view_bid").fadeOut(200);
        setTimeout(function(){
            //that.find("#bid_status").fadeIn(200);
        }, 200);

        clearTimeout(timer);
    });
});

$( "body" ).on( "click", "#add_new_bid", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/add_bid.php",
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});


$( "body" ).on( "change", "#service", function() {
    var id = $(this).val();

    $('.service_table_holder').html($Loader);
    $('.bid_amount_holder').empty();

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service_table.php?id="+id,
       success: function(msg){
         $(".service_table_holder").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
     });

     $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/form/add_bid_amount.php?id="+id,
        success: function(msg){
          $(".bid_amount_holder").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
                       $('.bid_info_holder').show();
                       $('.add_bid_message').hide();
                       $('#save_add_bid').show();
           });
          }
      });
});

function isValidDate(dateString){
   //alert(dateString);
   if(!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(dateString)){
      if(/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)){
         var parts = dateString.split("-");
         var day = parseInt(parts[2], 10);
         var month = parseInt(parts[1], 10);
         var year = parseInt(parts[0], 10);
      }else{
         //alert('1');
         return false;
      }
   }else{
      var parts = dateString.split("-");
      var day = parseInt(parts[0], 10);
      var month = parseInt(parts[1], 10);
      var year = parseInt(parts[2], 10);
   }

   if(year < 1000 || year > 3000 || month == 0 || month > 12){
      //alert('2');
      return false;
   }

   var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
   if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;
   return day > 0 && day <= monthLength[month - 1];
};

function isValidTime(timeString){
   if(!/([01][0-9]|[02][0-3]):[0-5][0-9]/.test(timeString)){
      return false;
   }else{
		return true;
	}
}


$( "body" ).on( "click", "#save_add_bid", function() {

    var formid = '#add_bid_form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();


    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }

     if(isValidTime(start_time) === false){
        TimeError = 1;
    }

    if(isValidTime(end_time) === false){
      TimeError = 1;
   }





    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_bid.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid save.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }else if(DateError == 1){
      Messenger().post({
          message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
          type: 'error',
          showCloseButton: false
      });
   }else if(TimeError == 1){
      Messenger().post({
          message: 'Please make sure the time are formatted (hh:mm).',
          type: 'error',
          showCloseButton: false
      });
   }

});


$( "body" ).on( "click", "#view_bid", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_bid.php?id="+id,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});



$( "body" ).on( "click", "#edit_view_bid", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/edit_bid.php?id="+id,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});


$( "body" ).on( "click", "#save_edit_bid", function() {

    var formid = '#edit_bid_form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();


    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }
     if(isValidTime(start_time) === false){
       TimeError = 1;
    }

    if(isValidTime(end_time) === false){
     TimeError = 1;
  }

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_edit_bid.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid Updated.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }else if(DateError == 1){
      Messenger().post({
          message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
          type: 'error',
          showCloseButton: false
      });
   }else if(TimeError == 1){
      Messenger().post({
          message: 'Please make sure the time are formatted (hh:mm).',
          type: 'error',
          showCloseButton: false
      });
   }

});





$( "body" ).on( "click", "#add_new_bid_manual", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/add_bid_manual.php",
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});

$( "body" ).on( "change", "#service_manual", function() {
    var id = $(this).val();

    $('.service_table_holder').html($Loader);
    $('.bid_amount_holder_manual').empty();

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service_table.php?id="+id,
       success: function(msg){
         $(".service_table_holder").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
     });

     $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/form/add_bid_amount_manual.php?id="+id,
        success: function(msg){
          $(".bid_amount_holder_manual").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
                       $('.bid_info_holder').show();
                       $('.add_bid_message').hide();
                       $('#save_add_bid_manual').show();
                       $('.selectpicker').selectpicker();
           });
          }
      });
});


$( "body" ).on( "click", "#save_add_bid_manual", function() {

    var formid = '#add_bid_manual_form';
    var HasError = 0;
    var DateError = 0;
    var TimeError = 0;

    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();


    if(isValidDate(start_date) === false){
      $('#start_date').parent().addClass('has-error');
      DateError = 1;
   }

      if(isValidDate(end_date) === false){
         $('#end_date').parent().addClass('has-error');
         DateError = 1;
     }
     if(isValidTime(start_time) === false){
      TimeError = 1;
    }

    if(isValidTime(end_time) === false){
     TimeError = 1;
  }

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0 && DateError == 0 && TimeError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_bid_manual.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid save.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }else if(DateError == 1){
      Messenger().post({
          message: 'Please make sure the dates are formatted (dd-mm-yyyy).',
          type: 'error',
          showCloseButton: false
      });
   }else if(TimeError == 1){
      Messenger().post({
          message: 'Please make sure the time are formatted (hh:mm).',
          type: 'error',
          showCloseButton: false
      });
   }

});


$( "body" ).on( "click", "#delete_bid", function() {
    var id = $(this).data('id');

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/delete_bid.php?id="+id,
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});




$( "body" ).on( "click", "#confirm_delete_bid", function() {
    var id = $(this).data('id');
    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/confirm_delete_bid.php",
          data: {'id':id},
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Bid Deleted.',
                    type: 'info',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //Refresh the Page
                window.location.reload();

              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });
});

$( "body" ).on( "change", "#start_date, #end_date, #start_time, #end_time", function() {
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  var start_time = $('#start_time').val();
  var end_time = $('#end_time').val();
  var service = $('#service').val();

  console.log('change');

  $('.available_bidder_holder').html($Loader);

  $.ajax({
     type: "POST",
     url: $fullurl+"admin/rota/form/get_available_bidder_holder.php",
     data:{ start_date:start_date, end_date:end_date, start_time:start_time, end_time:end_time, service:service },
     success: function(msg){
       $(".available_bidder_holder").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
        });
       }
});
});

$( "body" ).on( "click", ".reoccuring_shift", function() {
   $('.reoccuring_shift_holder').toggle();
});

$( "body" ).on( "click", ".reoccure_freq", function() {
   var value = $(this).val();
   if(value == 'weekly'){
      $('#reoccur_table').show();
      $('.reoccur_monthly').hide();
   }else{
      $('#reoccur_table').hide();
      $('.reoccur_monthly').show();
   }
});



$( "body" ).on( "click", "#recalculate_all_shifts", function() {
   $("#BaseModalLContent").html($Loader);
   $('#BaseModalL').modal('show');

   $.ajax({
      type: "POST",
      url: $fullurl+"admin/rota/form/recalculate_all_shifts.php",
      success: function(msg){
        $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
             $(this).html(msg);
             n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                     n();
                     $('.selectpicker').selectpicker();
        });
        }
});
});

$( "body" ).on( "click", "#confirm_recalculate_all_shifts", function() {
   $.ajax({
         type: "POST",
         url: $fullurl+"admin/rota/ajax/confirm-recalculate-all.php",
         success: function(msg){
           $message=$.trim(msg);
           if($message=='ok'){
               Messenger().post({
                   message: 'All Shifts Recalculated.',
                   type: 'info',
                   showCloseButton: false
               });
             setTimeout(function(){
               //window.location.replace($fullurl+'jobs.php');

               //Refresh the Page
               window.location.reload();

             },1000);
           }

        },error: function (xhr, status, errorThrown) {
               Messenger().post({
                   message: 'An error has occurred please try again.',
                   type: 'error',
                   showCloseButton: false
               });
             }
   });
});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
