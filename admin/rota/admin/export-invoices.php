<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Invoices';

   $fN = date("Y-m-d-H-i-s");
   $fN = 'export_invoices_'.$fN;

   header("Content-Type: application/xls");
   header("Content-Disposition: attachment; filename=".$fN.".xls");
   header("Pragma: no-cache");
   header("Expires: 0");

 $today_minus_72 = strtotime(" - 3 day");

 $subadmin_search = '';
 if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
   $subadmin_search = ' and location in ("0",';
   $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

   foreach ($my_areas as $ma) {
     $subadmin_search .='"'.$ma.'",';
   }

   $subadmin_search .= '"0")';

   $subadmin_query_pcn = sub_admin_query('pcn_id');
   $subadmin_query_practice = sub_admin_query('practice_id','or');



   if(!empty($subadmin_query_pcn) && !empty($subadmin_query_practice)){
     $subadmin_search .= " and($subadmin_query_pcn $subadmin_query_practice)";
   }
 }


 $db->query("select distinct member_id from hx_rota where status = 1 and invoice_paid = 0 and dna = 0 and end_ts < ? $subadmin_search and end_ts > ?");
 $db->bind(1,$today_minus_72);
 $db->bind(2,'1620643344');
 $data = $db->resultset();



 ?>

 <table border="1">
   <tr style="background-color:#eee;">
     <th>Contractor</th>
     <th>Service</th>
     <th>Role</th>
     <th>Start Date / Time</th>
     <th>End Date / Time</th>
     <th>Hourly Rate</th>
     <th>Amount</th>

     <th>Bank Name</th>
     <th>Account Number</th>
     <th>Sort Code</th>
   </tr>
   <?foreach ($data as $d) {?>
     <?
     $db->query("select * from hx_rota where status = 1 and invoice_paid = 0 and dna = 0 and end_ts < ? and member_id = ?");
     $db->bind(1,$today_minus_72);
     $db->bind(2,$d['member_id']);
     $rota_info = $db->resultset();


     $db->query("select * from ws_accounts where id = ?");
     $db->bind(1,$d['member_id']);
     $account = $db->single();

     $db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
     $db->bind(1,$d['member_id']);
     $account_extra = $db->single();

     $running_total = 0;
     ?>
     <?foreach ($rota_info as $ri) {?>
       <?
         $db->query("select * from ws_service where id = ?");
         $db->bind(1,$ri['service_id']);
         $service = $db->single();

         $db->query("select * from ws_roles where id = ?");
         $db->bind(1,$ri['role']);
         $role = $db->single();

         $db->query("select * from ws_service_roles where pid = ? and job_role = ?");
         $db->bind(1,$ri['service_id']);
         $db->bind(2,$ri['role']);
         $service_role = $db->single();

         $hourly_rate = preg_replace("/[^0-9.]/", "",$service_role['hourly_rate']);
         $time1 = $ri['start_ts'];
         $time2 = $ri['end_ts'];
         $difference = round(abs($time2 - $time1) / 3600,2);

         if($ri['role'] != 9){

         $running_total = $running_total + ($difference * $hourly_rate);
       ?>
       <tr>
         <td><?echo $account['first_name'].' '.$account['surname'];?></td>
         <td><?echo $service['title'];?></td>
         <td><?echo $role['title'];?></td>
         <td><?echo date('d-m-Y H:i',$ri['start_ts'])?></td>
         <td><?echo date('d-m-Y H:i',$ri['end_ts'])?></td>
         <td><?echo $service_role['hourly_rate'];?> Per Hour</td>
         <td>£<?echo $difference * $hourly_rate;?></td>

         <td><?echo decrypt($account_extra['bank_name']);?></td>
         <td><?echo decrypt($account_extra['bank_account_number']);?></td>
         <td><?echo decrypt($account_extra['bank_sort_code']);?></td>
       </tr>
     <?}}?>

   <?}?>
 </table>
