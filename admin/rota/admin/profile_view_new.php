<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Rota - Account Approval';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

                            <div class="page_title text-capitalize">
                                <span class="menuglyph glyphicons glyphicons-calendar page-title-glyph rota_background" aria-hidden="true"></span>
                                <span class="menuText">Profile view</span>


                            </div>




								<div class="clearfix"></div>





                                            <table id="table" class="striped"
                                                        data-toggle="table"
                                                        data-show-export="false"
                                                        data-export-types="['excel']"
                                                        data-click-to-select="true"
                                                        data-filter-control="false"
                                                        data-show-columns="false"
                                                        data-show-refresh="false"
                                                        data-url="<? echo $fullurl; ?>admin/rota/ajax/table/view_all_profiles.php"
                                                        data-height="400"
                                                        data-side-pagination="server"
                                                        data-pagination="true"
                                                        data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                        data-search="true">
                                                    <thead>
                                                        <tr>
                                                                <th data-field="acc_id" data-sortable="true" data-visible="false">ID</th>
                                                                <th data-field="name" data-sortable="true" data-formatter="LinkViewProfileFormatter">Name</th>
                                                                <th data-field="email" data-sortable="true">Email</th>
                                                                <th data-field="telephone" data-sortable="true">Telephone</th>
                                                                <th data-field="mobile_number" data-sortable="true">Mobile</th>
                                                                <th data-field="created_d" data-sortable="true" data-visible="true" data-formatter="DateTimeFormatter">Created Date</th>


                                                        </tr>
                                                    </thead>
                                            </table>






            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>
    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>


</body>
</html>

<!-- PAGE JS -->



<script>



</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
