<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Staff Box - Add Notification';

 if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){
   $db->query("select * from accounts where user_status = 1");
   $accoutns = $db->resultset();
 }else if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
   $db->Query("select * accounts_service_area where pid = ? and status = 1");
   $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
   $locations = $db->resultset();

   $subadmin_search = ' and location_id in ("0",';

   foreach ($locations as $l) {
     $subadmin_search .='"'.$l.'",';
   }

   $subadmin_search .= '"0")';


   $db->query("select distinct ws_accounts.id
               from ws_accounts
               join ws_accounts_locations
               on ws_accounts_locations.account_id = ws_accounts.id $subadmin_search");
   $accoutns = $db->resultset();
 }

 $db->query("select * from ws_roles where status = 1");
$roles = $db->resultset();

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize">
                                    <img src="/staff-box/images/pink.png" height="75" />
                                    <span class="menuText">Add Notification</span>


                                </div>


                                <form id="add_new_notification">

                                    <div class="form-group label-floating is-empty">
                                      <label for="not_title" class="control-label">Title*</label>
                                      <input id="not_title" type="text" name="not_title" class="form-control" value="" required>
                                      <span class="material-input"></span>
                                    </div>

                                    <div class="form-group">
                                        <label class="">Notification*</label>
                                        <textarea name="notification" class="form-control" rows="5" id="notification_information"></textarea>
                                    </div>

                                    <div class="form-group is-empty">
                                        <label for="" class="control-label" style="margin-top:0px;">Notify Who?</label>
                                        <select class="form-control" name="notify_who" style="" id="notify_who">
                                            <option value="all" selected>All User</option>
                                            <option value="specific">Specific Users</option>
                                            <option value="location">Location</option>
                                            <option value="role">Role</option>
                                        </select>
                                        <span class="material-input"></span>
                                    </div>

                                    <div class="form-group is-empty specific_user_select" style="display:none;">
                                        <label for="" class="control-label" style="margin-top:0px;">Specific User</label>
                                        <select class="form-control selectpicker" name="specific_users[]" style="" id="specific_users" multiple data-live-search="true">
                                            <?foreach ($accoutns as $accs) {?>
                                                <option value="<?echo $accs['id'];?>"><?echo $accs['name'];?></option>
                                            <?}?>
                                        </select>
                                        <span class="material-input"></span>
                                    </div>

                                    <div class="form-group is-empty location_select" style="display:none;">
                                        <label for="" class="control-label" style="margin-top:0px;">Location</label>
                                        <select class="form-control selectpicker" name="location[]" style="" id="location" multiple data-live-search="true">
                                           <option value="Wirral">Wirral</option>
                                           <option value="Liverpool">Liverpool</option>
                                           <option value="Sefton">Sefton</option>
                                        </select>
                                        <span class="material-input"></span>
                                    </div>

                                    <div class="form-group is-empty roles_select" style="display:none;">
                                        <label for="" class="control-label" style="margin-top:0px;">Roles</label>
                                        <select class="form-control selectpicker" name="roles[]" style="" id="roles" multiple data-live-search="true">
                                           <?foreach ($roles as $r) {?>
                                              <option value="<?echo $r['id'];?>"><?echo $r['title'];?></option>
                                           <?}?>
                                        </select>
                                        <span class="material-input"></span>
                                    </div>

                                    <a href="<?echo $fullurl;?>admin/rota/index.php" class="btn btn-raised btn-success btn-sm btn-warning">Cancel</a>
                                    <button type="button" id="send_notification" class="btn btn-raised btn-success btn-sm btn-success pull-right">Save</button>

                                </form>


								<div class="clearfix"></div>







            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>
<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>

</body>
</html>

<!-- PAGE JS -->



<script>

    jQuery(document).ready(function($) {
        $('#notification_information').summernote({
          height: 300,                 // set editor height
          minHeight: null,             // set minimum height of editor
          maxHeight: null,             // set maximum height of editor
          focus: true,                  // set focus to editable area after initializing summernote
          placeholder:'Description...',
          toolbar: [
            ['font', ['bold', 'italic', 'underline']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'hr']],
            ['help', ['help']]
            ]
        });
    });


    $( "body" ).on( "change", "#notify_who", function(e) {

          var value = $(this).val();

          $('.specific_user_select').hide();
          $('.location_select').hide();
          $('.roles_select').hide();

          if(value == 'specific'){
             $('.specific_user_select').show();
          }else if(value == 'location'){
             $('.location_select').show();
          }else if(value == 'role'){
             $('.roles_select').show();
          }

    });

    $( "body" ).on( "click", "#send_notification", function() {
      var formid = '#add_new_notification';
      var HasError = 0;

      $(formid).find('input').each(function(){
        $(this).parent().removeClass('has-error');
        Messenger().hideAll();

        if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                 if(!$(this).prop('required')){
            } else {
              HasError = 1;
              $(this).parent().addClass('has-error');
            }
          }
      });
      $(formid).find('select').each(function(){
        $(this).parent().removeClass('has-error');
        Messenger().hideAll();

        if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                 if(!$(this).prop('required')){
            } else {
              HasError = 1;
              $(this).parent().addClass('has-error');
            }
          }
      });

      if(HasError == 0){

        var FRMdata = $(formid).serialize(); // get form data
          $.ajax({
                type: "POST",
                url: $fullurl+"assets/app_ajax/save_new_notification.php",
                data: FRMdata,
                success: function(msg){
                  $message=$.trim(msg);
                  if($message=='ok'){
                      Messenger().post({
                          message: 'Notification Sent.',
                          type: 'error',
                          showCloseButton: false
                      });
                    setTimeout(function(){
                      //window.location.replace($fullurl+'jobs.php');

                      //Refresh the Page
                      window.location.reload();

                    },1000);
                  }

               },error: function (xhr, status, errorThrown) {
                      Messenger().post({
                          message: 'An error has occurred please try again.',
                          type: 'error',
                          showCloseButton: false
                      });
                    }
          });
      }
    });


</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
