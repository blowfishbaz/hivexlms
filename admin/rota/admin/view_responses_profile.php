<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $id = $_GET['id'];

 $db->query("select * from ws_accounts where id = ?");
 $db->bind(1,$id);
 $data = $db->single();

 $db->query("select * from accounts where id = ?");
 $db->bind(1,$id);
 $data_account = $db->single();

 $db->query("select coalesce(count(id)) as counted from hx_rota where member_id = ? and end_ts > ? and dna = 0");
 $db->bind(1,$id);
 $db->bind(2,time());
 $completed_shifts = $db->single();


 $db->query("select coalesce(count(id)) as counted from hx_rota where member_id = ? and dna = 1");
 $db->bind(1,$id);
 $dna_shifts = $db->single();

 $document_list = get_document_list($id);

 $db->query("select * from ws_accounts_upload where account_id = ? and status = 1");
 $db->bind(1,$id);
 $misc_uploads = $db->resultset();

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    table-layout: fixed;
    width: 100%;
  }

  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    overflow-wrap: break-word;
  }

  #personal_table{
    width:auto;
    border-collapse: separate;
    border-spacing: 0 1em;
  }

  #personal_table th{
    border:0px;
    font-weight: 100;
  }
  #personal_table tr{
    margin-top:10px;
    margin-bottom:10px;
  }

  #personal_table td{
    border:0px;
    color:#74ccd5;
    border-radius: 10px;
    -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.6);
    -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.6);
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.6);
  }

  .profile_pic_holder{
    max-width:300px;
    width: 100%;
    background-color:#edaac6;
    border-radius: 20px;
    margin:0 auto;
    display: table;
    /* cursor: pointer; */
  }

  .profile_pic_holder img{
    position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        height: 90%;
        <?if(!empty($data_account['profilepic'])){?>border-radius: 100%;<?}?>
        /* border: 15px solid white; */
        height: 70%;
        width: auto;
  }

  .profile_pic_holder_inner{
        width: 75%;
        background-color: white;
        border-radius: 100%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        padding-top: 75%;
        -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
        -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
    }

  .small_dash_icon{
    min-height:120px;
  }

  .info_text_pink{
    color: #edaac6;
    font-size: 30px;
    font-weight: 500;
  }

  .th-inner{
      background-color: #F0F0F0;
  }
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize"><span class="menuText">View Responses - <?echo $data['first_name'].' '.$data['surname'];?></span></div>





								<div class="clearfix"></div>
                                <div class="row">
                                    <div class="row">
                                            <div class="panel-body">


                                              <div class="col-lg-12" style="">
                                                <h3 class="text-pink pull-left">Personal Details</h3>
                                                <a href="<?echo $fullurl;?>admin/rota/admin/view_responses.php?id=<?echo $_GET['bid'];?>" class="btn btn-sm btn-raised btn-success pull-right">Back</a>
                                              </div>
                                              <div class="clearfix"></div>
                                              <div class="col-lg-3" style="">
                                                <div class="profile_pic_holder">
                                                  <div class="profile_pic_holder_inner">
                                                    <img src="<?echo loadProfilePic2($data_account['profilepic']);?>" />
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="col-lg-9" style="">
                                                <div class="col-lg-8" style="">
                                                  <table id="personal_table">
                                                    <tr>
                                                      <th>Full Name</th>
                                                      <td><?echo $data['first_name'].' '.$data['surname'];?></td>
                                                    </tr>
                                                    <tr>
                                                      <th>Date of Birth</th>
                                                      <td></td>
                                                    </tr>
                                                    <tr>
                                                      <th>Email Address</th>
                                                      <td><?echo $data['email'];?></td>
                                                    </tr>
                                                    <tr>
                                                      <th>Phone Number</th>
                                                      <td><?echo $data_extra['telephone'];?></td>
                                                    </tr>
                                                    <tr>
                                                      <th>Preferred Role</th>
                                                      <td></td>
                                                    </tr>
                                                    <tr>
                                                      <th>Preferred Location</th>
                                                      <td></td>
                                                    </tr>
                                                  </table>
                                                </div>
                                                <div class="col-lg-4" style="">
                                                  <div class="grey_background dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                                      <div class="dashboard_icon_inner">
                                                          <span class="dashboard_text info_text_pink"><?echo $completed_shifts['counted'];?></span><br />
                                                          <span class="dashboard_text">Complted Shifts</span>
                                                      </div>
                                                  </div>

                                                  <div class="grey_background dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                                      <div class="dashboard_icon_inner">
                                                          <span class="dashboard_text info_text_pink"><?echo $dna_shifts['counted'];?></span><br />
                                                          <span class="dashboard_text">Cancelled Shifts</span>
                                                      </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="clearfix"></div>

                                              <div class="col-lg-12">
                                                <table style="margin-top:20px;">
                                                  <tr style="background-color:#eee;">
                                                    <th>Document Type</th>
                                                    <th>Document Name</th>
                                                    <th>Date Last Updated</th>
                                                    <th style="width: 90px;">View</th>
                                                  </tr>

                                                  <?foreach ($document_list as $dl) {?>
                                                    <?
                                                      $db->query("select * from ws_accounts_upload where account_id = ? and status = ? and type = ?");
                                                      $db->bind(1,$id);
                                                      $db->bind(2,'1');
                                                      $db->bind(3,$dl);
                                                      $uploads_per = $db->resultset();
                                                    ?>

                                                      <?foreach ($uploads_per as $up) {?>
                                                        <tr>
                                                          <td><?echo document_type_full_name($dl);?></td>
                                                          <td><?echo $up['name'];?></td>
                                                          <td><?echo date('d-m-Y', $up['created_date']);?></td>
                                                          <td><a href="<?echo $fullurl.$up['path'].$up['name'];?>" download class="btn btn-sm btn-raised btn-warning" style="margin:0px;">View</a></td>
                                                        </tr>
                                                      <?}?>

                                                    <?}?>
                                                </table>
                                              </div>






                                            </div>
                                    </div>
                                </div>



            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>

    <?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->



    <script>

    jQuery(document).ready(function($) {
      var width = $('.profile_pic_holder').width();
      if(width > 300){
          width = 300;
      }
      $('.profile_pic_holder').height(width);

    });




    </script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
