<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Staff Box - Services';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>
.form-control {
/* margin-bottom: 7px; */
-webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
padding: 0px 20px;
border-radius: 10px;
/* margin: 10px 0px; */
background-image: none;
/* width: 300px; */
}
.form-control::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: black;
            opacity: 1; /* Firefox */
}

.form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: black;
}

.form-control::-ms-input-placeholder { /* Microsoft Edge */
            color: black;
 }

#view_service{
    /* -webkit-transition: opacity 2s ease-in;
-moz-transition: opacity 2s ease-in;
-o-transition: opacity 2s ease-in;
-ms-transition: opacity 2s ease-in;
transition: opacity 2s ease-in; */
}

.th-inner{
    background-color: #F0F0F0;
}
</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize">
                                    <img src="/staff-box/images/pink.png" height="75" />
                                    <span class="menuText">Practices</span>


                                </div>





								<div class="clearfix"></div>

                                <button type="button" class="btn btn-primary  rota_background main_button pull-right" id="add_new_practice">Add Practice<span class="glyphicons glyphicons-plus" style="margin-left:0px;"></span></button>
                                            <table id="table" class="striped"
                                                    data-toggle="table"
                                                    data-show-export="false"
                                                    data-export-types="['excel']"
                                                    data-click-to-select="true"
                                                    data-filter-control="false"
                                                    data-show-columns="false"
                                                    data-show-refresh="false"
                                                    data-url="<? echo $fullurl; ?>admin/rota/ajax/table/view_practice.php"
                                                    data-height="400"
                                                    data-side-pagination="server"
                                                    data-pagination="true"
                                                    data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                    data-search="true"
                                                    data-search-align="left"
                                                    data-row-attributes="rowAttributes">
                                                <thead>
                                                    <tr>
                                                            <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                                                            <th data-field="title" data-sortable="true" data-formatter="LinkViewPCNFormatter" data-halign="center" data-width="500">Title</th>
                                                            <th data-field="location_id" data-sortable="true" data-halign="center">Location</th>
                                                            <th data-field="pcn_title" data-sortable="true" data-halign="center">PCN Title</th>
                                                            <th data-field="address1" data-sortable="true" data-halign="center">Address 1</th>
                                                            <th data-field="postcode" data-sortable="true" data-halign="center">Postcode</th>
                                                            <th data-field="created_date" data-sortable="true" data-visible="true" data-width="300" data-halign="center" data-formatter="DateTimeFormatter">Created Date</th>

                                                    </tr>
                                                </thead>
                                        </table>





            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>
<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>

</body>
</html>

<!-- PAGE JS -->



<script>

$(document).ready(function () {
    var timer;
    $(document).on('mouseenter', '#table tr', function () {
        var that = $(this);
        timer = setTimeout(function(){
            that.find(":button").fadeIn(100);
        }, 500);


    }).on('mouseleave', '#table tr', function () {
        $(this).find(":button").fadeOut(200);
        clearTimeout(timer);
    });
});

$( "body" ).on( "click", "#add_new_practice", function() {

    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/add_new_practice.php",
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
                      $('.selectpicker').selectpicker();
          });
         }
  });
});


$( "body" ).on( "click", "#save_new_practice", function() {
    var formid = '#add_practice_form';
    var HasError = 0;

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_practice.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Practice Created.',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }
});

$( "body" ).on( "click", "#view_pcn", function() {
  var id = $(this).data('id');

  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  $.ajax({
     type: "POST",
     url: $fullurl+"admin/rota/form/edit_new_practice.php?id="+id,
     success: function(msg){
       $("#BaseModalLContent").delay(1000)
        .queue(function(n) {
            $(this).html(msg);
            n();
        }).fadeIn("slow").queue(function(n) {
                     $.material.init();
                    n();
                    $('.selectpicker').selectpicker();
        });
       }
});
});


$( "body" ).on( "click", "#update_new_practice", function() {
    var formid = '#edit_practice_form';
    var HasError = 0;

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/edit_add_practice.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Practice Updated.',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }
});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
