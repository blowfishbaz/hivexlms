<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

 $db->query("select * from accounts where id = ?");
 $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
 $my_account = $db->single();

 $my_account_type = $my_account['account_type'];
 $extra='';

 if($my_account_type == 'user'){
     $extra = '&id='.$my_account['id'].'';
 }

 if(decrypt($_SESSION['SESS_ACCOUNT_ID']) == 'id_member_20161103_110736904700_82639'){
   $extra = '&id=id_member_20210322_095001550100_38917';
 }

 $wirral_show = 1;
 $sefton_show = 1;
 $liverpool_show = 1;

 if($my_account_type == 'subadmin'){
     $sub_array = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

     if(!in_array('Wirral',$sub_array)){
         $wirral_show = 0;
     }
     if(!in_array('Sefton',$sub_array)){
         $sefton_show = 0;
     }
     if(!in_array('Liverpool',$sub_array)){
         $liverpool_show = 0;
     }
 }





?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?	//Base CSS Include
    	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>



<style>

.ledgend_wrapper{
height: 35px;
width: auto;
float: left;
margin-left: 25px;
}

.ledgend_wrapper:first-of-type {
margin:0px;
}

.ledgend_calendar{
height: 25px;
width: 25px;
position: relative;
float: left;
border-radius: 5px;
margin-right: 25px;
}

.legend_text{
position: relative;
float: left;
margin-top: 3px;
}

.fc-prev-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-left.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-prev-button span {
    display: none;
}

.fc-next-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-right.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-next-button span {
    display: none;
}

</style>

</head>
<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>

                      <!--- PAGE CONTENT HERE ---->
                      <div class="page_title text-capitalize"><img src="/staff-box/images/pink.png" height="75" /><span class="menuText">My Calendar</span></div>




                    <div class="clearfix"></div>

                          <div class="calendar" id="calendar">

                          </div>
                        </div>
                </div>




        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>
    </div>

<?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>
</body>
</html>

<!-- PAGE JS -->

<!-- <script src="<? echo $fullurl ?>assets/app_scripts/crm/customer/index.js"></script> -->
<script src="<? echo $fullurl ?>admin/rota/js/index.js"></script>
<script src="../../../assets/js/picturefill.min.js"></script>
<script src="../../../assets/js/lightgallery.js"></script>
<script src="../../../assets/js/lg-fullscreen.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>

<script>

jQuery(document).ready(function($) {
    var width = $('body').width();
    console.log(width);
    if(width > 980){
        $('#menu-toggle').click();
    }else{
         $('#calendar').fullCalendar('option', 'height', 'auto');
    }

});


$('.calendar').fullCalendar({
header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month,agendaWeek,agendaDay'
},
editable: false,
timezoneParam:'GMT',
eventLimit: true,
aspectRatio: 2,
buttonText: {
                today: 'Go To Today',
                month:    'Month',
                week:     'Week',
                day:      'Day'
            },
buttonIcons: {
    prev: 'left-double-arrow',
    next: 'right-double-arrow',
},
eventSources: [

  {
      url: '../../../admin/rota/ajax/calendar.php?location=wirral<?echo $extra;?>&show=<?echo $wirral_show;?>', // use the `url` property
      color: '#46bbc7',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
  },
  {
      url: '../../../admin/rota/ajax/calendar.php?location=sefton<?echo $extra;?>&show=<?echo $sefton_show;?>', // use the `url` property
      color: '#5e4292',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
  },
  {
      url: '../../../admin/rota/ajax/calendar.php?location=liverpool<?echo $extra;?>&show=<?echo $liverpool_show;?>', // use the `url` property
      color: '#f5a11c',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
    },
{
    url: '../../../admin/rota/ajax/calendar.php?location=wirral<?echo $extra;?>&dna=1&show=<?echo $wirral_show;?>', // use the `url` property
    color: '#ff5722',    // an option!
    textColor: 'white',  // an option!
    className: 'reminders',
    id: 'id'
  },
  {
      url: '../../../admin/rota/ajax/calendar.php?location=sefton<?echo $extra;?>&dna=1&show=<?echo $sefton_show;?>', // use the `url` property
      color: '#ff5722',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
    },
    {
        url: '../../../admin/rota/ajax/calendar.php?location=liverpool<?echo $extra;?>&dna=1&show=<?echo $liverpool_show;?>', // use the `url` property
        color: '#ff5722',    // an option!
        textColor: 'white',  // an option!
        className: 'reminders',
        id: 'id'
      }

],
eventClick: function(calEvent, jsEvent, view) {
  var appointmentID = calEvent.id;
    console.log(appointmentID);
    $("#BaseModalLContent").html($Loader);
    $('#BaseModalL').modal('show');
    $.ajax({
        type: "POST",
        url: "../../../admin/rota/ajax/calendar_view_appointment.php?id="+appointmentID+"",
        success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();

                       $('#notes').summernote({
                     height: 300,                 // set editor height
                     minHeight: null,             // set minimum height of editor
                     maxHeight: null,             // set maximum height of editor
                     focus: true,                  // set focus to editable area after initializing summernote
                     toolbar: [
                       ['font', ['bold', 'italic', 'underline']],
                       ['color', ['color']],
                       ['para', ['ul', 'ol', 'paragraph']],
                       ['table', ['table']],
                       ['insert', ['link', 'hr']],
                       ['help', ['help']]
                       ]
                   });
           });
         }
    });
},    dayClick: function(date, jsEvent, view) {

    <?if($my_account_type != 'user'){?>

        $("#BaseModalLContent").html($Loader);
        $('#BaseModalL').modal('show');

        $.ajax({
           type: "POST",
           url: $fullurl+"admin/rota/form/add_bid.php?date="+date,
           success: function(msg){
             $("#BaseModalLContent").delay(1000)
              .queue(function(n) {
                  $(this).html(msg);
                  n();
              }).fadeIn("slow").queue(function(n) {
                           $.material.init();
                          n();
                          $('.selectpicker').selectpicker();
              });
             }

    });

    <?}?>


  }

});



$( "body" ).on( "click", "#did_not_attend", function() {
  var id = $(this).data('id');
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  $.ajax({
      type: "POST",
      url: "../../../admin/rota/form/did_not_attend.php?id="+id+"",
      success: function(msg){
        //alert(msg);
        $("#BaseModalLContent").delay(1000)
         .queue(function(n) {
             $(this).html(msg);
             n();
         }).fadeIn("slow").queue(function(n) {
                      $.material.init();
                     n();

                     $('#dna_message').summernote({
                   height: 200,                 // set editor height
                   minHeight: null,             // set minimum height of editor
                   maxHeight: null,             // set maximum height of editor
                   focus: true,                  // set focus to editable area after initializing summernote
                   toolbar: [
                     ['font', ['bold', 'italic', 'underline']],
                     ['color', ['color']],
                     ['para', ['ul', 'ol', 'paragraph']],
                     ['table', ['table']],
                     ['insert', ['link', 'hr']],
                     ['help', ['help']]
                     ]
                 });
         });
       }
  });
});

$( "body" ).on( "click", "#confirm_did_not_attend", function() {
  var form = $('#did_not_attend_form').serialize();

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/did_not_attend.php",
        data: form,
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
                          Messenger().post({
                                  message: 'Shift marked as Did Not Attend.',
                                  type: 'error',
                                  showCloseButton: false
                          });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
                              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});


$( "body" ).on( "click", "#did_attend", function() {
  var id = $(this).data('id');

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/did_attend.php",
        data: {id:id},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
                          Messenger().post({
                                  message: 'Shift marked as Did Attend.',
                                  type: 'error',
                                  showCloseButton: false
                          });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
                              window.location.reload();
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});

$( "body" ).on( "change", "#service", function() {
    var id = $(this).val();

    $('.service_table_holder').html($Loader);
    $('.bid_amount_holder').empty();

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/form/view_service_table.php?id="+id,
       success: function(msg){
         $(".service_table_holder").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
     });

     $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/form/add_bid_amount.php?id="+id,
        success: function(msg){
          $(".bid_amount_holder").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();
                       $('.bid_info_holder').show();
                       $('.add_bid_message').hide();
                       $('#save_add_bid').show();
           });
          }
      });
});


$( "body" ).on( "click", "#save_add_bid", function() {

    var formid = '#add_bid_form';
    var HasError = 0;

    $(formid).find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });
    $(formid).find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();

      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
               if(!$(this).prop('required')){
          } else {
            HasError = 1;
            $(this).parent().addClass('has-error');
          }
        }
    });

    if(HasError == 0){

      var FRMdata = $(formid).serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"admin/rota/ajax/save_add_bid.php",
              data: FRMdata,
              success: function(msg){
                $message=$.trim(msg);
                if($message=='ok'){
                    Messenger().post({
                        message: 'Bid save.',
                        type: 'info',
                        showCloseButton: false
                    });
                  setTimeout(function(){
                    //window.location.replace($fullurl+'jobs.php');

                    //Refresh the Page
                    window.location.reload();

                  },1000);
                }

             },error: function (xhr, status, errorThrown) {
                    Messenger().post({
                        message: 'An error has occurred please try again.',
                        type: 'error',
                        showCloseButton: false
                    });
                  }
        });
    }

});
</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
