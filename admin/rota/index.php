<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

if(empty($_POST)){


    }
    else{


}


$next_booked_sessions = 0;
$session_requests = 0;
$invoices_to_pay = 0;
$notifications = 0;

$db->query("select COALESCE(count(id)) as total from hx_rota where member_id = ? and start_ts > ?");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(2, time());
$next_booked_sessions_data = $db->single();

$next_booked_sessions = $next_booked_sessions_data['total'];


$db->query("select COALESCE(count(id)) as total from ws_rota_bid_request where account_id = ? and accepted_user = ?");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(2, 0);
$session_requests_data = $db->single();

$session_requests = $session_requests_data['total'];




if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'user'){
  $today_minus_72 = strtotime(" - 3 day");

  $db->query("select count(id) as total from hx_rota where status = 1 and invoice_paid = 0 and dna = 0 and end_ts < ? and member_id = ?");
  $db->bind(1,$today_minus_72);
  $db->bind(2,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $invoices_to_pay_data = $db->single();

  $invoices_to_pay = $invoices_to_pay_data['total'];
}else{
   $today_minus_72 = strtotime(" - 3 day");

   if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
     $subadmin_search = ' and location in ("0",';
     $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

     foreach ($my_areas as $ma) {
       $subadmin_search .='"'.$ma.'",';
     }

     $subadmin_search .= '"0")';

     $subadmin_query_pcn = sub_admin_query('pcn_id');
     $subadmin_query_practice = sub_admin_query('practice_id','or');



     if(!empty($subadmin_query_pcn) && !empty($subadmin_query_practice)){
       $subadmin_search .= " and($subadmin_query_pcn $subadmin_query_practice)";
     }
   }

   $db->query("select count(distinct member_id) as total from hx_rota where status = 1 and invoice_paid = 0 and dna = 0 and end_ts < ? $subadmin_search and end_ts > ?");
   $db->bind(1,$today_minus_72);
   $db->bind(2,'1620643344');
   $invoices_to_pay_data = $db->single();


   if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
     $invoices_to_pay_data['total'] = 0;
   }

   $invoices_to_pay = $invoices_to_pay_data['total'];
}











$PW = generateRandomString(7);

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?
//Base CSS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->

</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize"><img src="/staff-box/images/pink.png" height="75" /><span class="menuText">My Dashboard - DEV</span></div>



								<div class="clearfix"></div>
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <a href="<?echo $fullurl;?>admin/rota/<?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'user'){?>my_calendar<?}else{?>rota<?}?>.php">
                                            <div class="rota_background dashboard_icons main_dash_icon" style="">
                                                <div class="dashboard_icon_inner">
                                                    <!-- IconCalendar.png -->
                                                    <span class="glyphicons glyphicons-calendar dashboard-glyphicon-large" aria-hidden="true"></span><br />
                                                    <span class="dashboard_text" style="font-size:25px;">Manage Rota</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-lg-2 dashboard-icon-holder">
                                        <div class="grey_background dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                            <div class="dashboard_icon_inner">
                                                <img class="img-rounded" alt="140x140" src="<? echo $fullurl?>assets/images/IconNews.png" data-holder-rendered="true" style="width: 55px; height: 55px;" data-placement="bottom" title="Notifications" data-original-title="Notifications" id="top_menu_reminders"><br />
                                                <span class="dashboard_text">News & Announcements</span>
                                            </div>
                                        </div>


                                        <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'user'){?>
                                            <a href="<? echo $fullurl?>admin/rota/account/view_profile.php?id=<?echo decrypt($_SESSION['SESS_ACCOUNT_ID']);?>" class=""style="color:black;">

                                                <div class="grey_background dashboard_icons small_dash_icon" style="">
                                                    <div class="dashboard_icon_inner">
                                                        <img class="img-rounded" alt="140x140" src="<? echo $fullurl?>assets/images/IconMyAccount.png" data-holder-rendered="true" style="width: 55px; height: 55px;"><br />
                                                        <span class="dashboard_text">My Account</span>
                                                    </div>
                                                </div>
                                            </a>
                                        <?}else{?>
                                            <a href="javascript:void(0)" class="" data-toggle="modal" data-target="#ProfileModal" data-placement="bottom" title="" data-original-title="Profile" id="profilemenu" style="color:black;">

                                                <div class="grey_background dashboard_icons small_dash_icon" style="">
                                                    <div class="dashboard_icon_inner">
                                                        <img class="img-rounded" alt="140x140" src="<? echo $fullurl?>assets/images/IconMyAccount.png" data-holder-rendered="true" style="width: 55px; height: 55px;"><br />
                                                        <span class="dashboard_text">My Account</span>
                                                    </div>
                                                </div>
                                            </a>
                                        <?}?>


                                        <div class="clearfix"></div>
                                    </div>


                                    <div class="col-lg-2 dashboard-icon-holder">
                                        <div class="hr_backgorund dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                            <div class="dashboard_icon_inner">
                                                <span class="dashboard_number"><?echo $next_booked_sessions;?></span><br /> <span class="dashboard_text">Next Booked Sessions</span>
                                            </div>
                                        </div>
                                        <div class="black_background dashboard_icons small_dash_icon" style="">
                                            <div class="dashboard_icon_inner">
                                            <span class="dashboard_number"><?echo $invoices_to_pay;?></span> <br /> <span class="dashboard_text"><?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'user'){echo 'Invoices <br />Oustanding';}else{echo 'Invoices to Pay';}?></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>


                                    <div class="col-lg-2 dashboard-icon-holder">
                                        <div class="lxp_background dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                            <div class="dashboard_icon_inner">
                                            <span class="dashboard_number"><?echo $session_requests;?></span> <br /> <span class="dashboard_text">Sessions Requests</span>
                                            </div>
                                        </div>
                                        <div class="goven_background dashboard_icons small_dash_icon" style="" data-holder-rendered="true" style="width: 55px; height: 55px;" data-placement="bottom" title="Notifications" data-original-title="Notifications" id="top_menu_reminders">
                                            <div class="dashboard_icon_inner">
                                            <span class="dashboard_number"><?echo $notifications_counter;?></span> <br /> <span class="dashboard_text">Notifications</span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            <!-- Data -->




            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>


    <?include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/hivex-footer.php'); ?>


</body>
</html>

<!-- PAGE JS -->



<script>

jQuery(document).ready(function($) {
    var width = $('.main_dash_icon').width();


    console.log(width);

    if(width > 380){
        width = 380;
        $('.main_dash_icon').width(width);
    }

    $('.main_dash_icon').height(width);

    var half_height = (width - 20) / 2;
    //alert(half_height);
    $('.small_dash_icon').height(half_height);


});


</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
