<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');
//This will come from the email and will be the bid request id and the users id merged pspell_config_runtogether
//32e371e8ca6b3df2f6ef92d55b386361db2b8d8a6394d6bd49207aeb27a2017b54e2f1d839451f6a3056dd9c6984f864ff6273c2ce3dca048fc1f36f73de4c8be1bd28732bc492030584bf33e434ffb6

//32e371e8ca6b3df2f6ef92d55b386361f07a140d91c61bf2af7f62d4f01ac764be38e05f3f88b4ef9ef67fe3e56f97eedb9a902e1f4630a2fb29382e355825a92de0aad3d340e952556c374e1b08ccc3
//echo encrypt('id_bid_rq_20210429_134159213300_17022'.'-'.'id_member_20210331_092518379300_76037');
 //Page Title
$page_title = '';
$now = time();
$myid= decrypt($_GET['ref']);

$pieces = explode("-", $myid);

$pid = $pieces[0];
$account_id = $pieces[1];

$db->query("select * from accounts where id = ?");
$db->bind(1,$account_id);
$account_data = $db->single();

$db->query("select * from ws_rota_bid_request where id = ?");
$db->bind(1,$pid);
$bid_req_info = $db->single();

$db->query("select * from ws_rota_bid where id = ?");
$db->bind(1,$bid_req_info['pid']);
$bid_info = $db->single();

$db->query("select * from ws_service where id = ?");
$db->bind(1,$bid_info['service_id']);
$service = $db->single();

$db->query("select * from ws_service_roles where pid = ? and job_role = ? and status = 1");
$db->bind(1,$service['id']);
$db->bind(2,$bid_req_info['role_id']);
$service_amount = $db->single();

$db->query("select * from ws_roles where id = ?");
$db->bind(1,$bid_req_info['role_id']);
$role = $db->single();


if($bid_req_info['seen'] == 0){
    $db->Query("update ws_rota_bid_request set seen = 1, seen_date = ? where id = ?");
    $db->bind(1,time());
    $db->bind(2,$pid);
    $db->execute();
}

$db->query("select * from ws_pcn where id = ?");
$db->bind(1,$bid_info['pcn_id']);
$pcn = $db->single();

$db->query("select * from ws_practice where id = ?");
$db->bind(1,$bid_info['practice_id']);
$practice = $db->single();


 $db->query("select * from ws_rota_bid_amount where id = ? and pid = ? and status = 1");
 $db->bind(1,$bid_req_info['bid_amount_pid']);
 $db->bind(2,$bid_info['id']);
 $bid_amount_single = $db->single();

 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>


    <?
    //Base CSS Include
    include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->


<!-- Dashboard CSS -->
<link href="<? echo $fullurl ?>assets/css/gridstack.css" rel="stylesheet">
<style>
.grid-stack-item-content {
	background-color: white;
	border: 3px black solid;
	border-radius: 3px;
	padding: 3px;
}
#wrapper{
    padding-left: 0;
}
#logo {
    margin-left: 34px;
}
/* .glyphicons:before{
    padding: 6px 8px 6px 0px;
} */

.fc-past,.fc-future {
    background-color: #efefef;
}
.fc-unthemed .fc-today {
    background: #ddd;
}
</style>
<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
}

.map{
    background-color:#eee;
    width:100%;
    min-height:50px;
    margin-bottom:20px;
    border-radius: 20px;
}
</style>
</head>

<body>

    <div id="wrapper">

		<?  //include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  //include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>

                        <div id="page_spacer"></div>



           				<!--- PAGE CONTENT HERE ---->
<?

//var_dump(session_id()); For Stephen

?>
<div class="page_title text-capitalize" style="margin-top:0px;">
    <span class="menuglyph glyphicons glyphicons-calendar page-title-glyph rota_background" aria-hidden="true"></span>
    <span class="menuText">New Shift Bid</span>


</div>
<div class="row">

    <div class="col-lg-12">


        <?if($bid_req_info['accepted_user'] == 1 && $bid_req_info['accepted_admin'] == 0){?>
        <div class="alert btn-pink" role="alert">
          You have said you wish to attend this session. Once the admin team has responded to your application we will let you know.
        </div>
        <?}?>

        <?if($bid_req_info['accepted_admin'] == 1){?>
        <div class="alert btn-pink" role="alert">
          You have been accepted onto the below shift. Check your emails for confirmation.
        </div>
        <?}?>

        <?if($bid_req_info['accepted_admin'] == 2){?>
        <div class="alert btn-black" role="alert">
          You have been declined for the shift.
        </div>
        <?}?>

        <?if($bid_req_info['accepted_user'] == 2){?>
        <div class="alert btn-black" role="alert">
          You have declined the shift.
        </div>
        <?}?>

        <div class="col-lg-12" style="-webkit-box-shadow: 0px 0px 16px 0px rgb(0 0 0 / 80%); -moz-box-shadow: 0px 0px 16px 0px rgba(0,0,0,0.8); box-shadow: 0px 0px 16px 0px rgb(0 0 0 / 80%); border-radius: 20px;">
            <table style="margin-bottom:50px;">
                <tr>
                    <th colspan="2" style="border-right:0px; text-align:center;">
                        Service Bid Details
                    </th>
                </tr>
                <tr>
                  <th>Title</th>
                  <td><?echo $service['title'];?></td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td><?echo $service['description'];?></td>
                </tr>
                <tr>
                  <th>Location</th>
                  <td><?echo $service['location'];?></td>
                </tr>
                <tr>
                  <th>Role</th>
                  <td><?echo $role['title'];?></td>
                </tr>
                <tr>
                  <th>Per Hour</th>
                  <td><?echo $service_amount['hourly_rate'];?></td>
                </tr>

                <tr>
                  <th>PCN Location</th>
                  <td><?echo $pcn['pcn_title'];?></td>
                </tr>
                <tr>
                  <th>Practice Location</th>
                  <td><?echo $practice['title'];?></td>
                </tr>
                <tr>
                  <th>Address</th>
                  <td>
                    <?if(!empty($service['address1'])){echo $service['address1'];}?>
                    <?if(!empty($service['address2'])){echo ','.$service['address2'];}?>
                    <?if(!empty($service['address3'])){echo ','.$service['address3'];}?>
                    <?if(!empty($service['city'])){echo ','.$service['city'];}?>
                    <?if(!empty($service['county'])){echo ','.$service['county'];}?>
                    <?if(!empty($service['postcode'])){echo ','.$service['postcode'];}?>
                  </td>
                </tr>

                <tr>
                    <th colspan="2" style="border-right:0px; text-align:center;">Shift Response</th>
                </tr>
                <tr>
                    <th>Your Response</th>
                    <td><?if($bid_req_info['accepted_user'] == 0){echo 'Pending';}else if($bid_req_info['accepted_user'] == 1){echo 'Accepted';}else if($bid_req_info['accepted_user'] == 2){echo 'Declined';}else{echo 'Error';}?></td>
                </tr>
                <tr>
                    <th>Your Response Date</th>
                    <td><?if($bid_req_info['accepted_user'] == 1 || $bid_req_info['accepted_user'] == 2){ echo date('d-m-Y H:i', $bid_req_info['choice_date']); }?></td>
                </tr>
                <tr>
                    <th>Admin Response</th>
                    <td><?if($bid_req_info['accepted_admin'] == 0){echo 'Pending';}else if($bid_req_info['accepted_admin'] == 1){echo 'Accepted';}else if($bid_req_info['accepted_admin'] == 2){echo 'Declined';}else{echo 'Error';}?></td>
                </tr>
            </table>

            <div class="col-lg-3">
                <div class="col-lg-6">
                    <span class="glyphicons glyphicons-calendar x3" aria-hidden="true" style="margin: 0 auto; display: table; border-radius: 113px; -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 80%); -moz-box-shadow: 0px 0px 16px 0px rgba(0,0,0,0.8); box-shadow: 0px 0px 16px 0px rgb(0 0 0 / 80%);"></span>

                    <p style="text-align:center; margin-top:10px;">
                        <span style="font-weight: 500;">Start Date</span><br /><span style="color:#edaac6; font-weight: 500;"><?echo date('d-m-Y',$bid_info['start_final']);?></span>
                    </p>
                </div>
                <div class="col-lg-6">
                    <span class="glyphicons glyphicons-calendar x3" aria-hidden="true" style="margin: 0 auto; display: table; border-radius: 113px; -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 80%); -moz-box-shadow: 0px 0px 16px 0px rgba(0,0,0,0.8); box-shadow: 0px 0px 16px 0px rgb(0 0 0 / 80%);"></span>
                    <p style="text-align:center; margin-top:10px;">
                        <span style="font-weight: 500;">End Date</span><br /><span style="color:#edaac6; font-weight: 500;"><?echo date('d-m-Y',$bid_info['end_final']);?></span>
                    </p>

                </div>
            </div>
            <div class="col-lg-3">
                <div class="col-lg-6">
                    <span class="glyphicons glyphicons-alarm x3" aria-hidden="true" style="margin: 0 auto; display: table; border-radius: 113px; -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 80%); -moz-box-shadow: 0px 0px 16px 0px rgba(0,0,0,0.8); box-shadow: 0px 0px 16px 0px rgb(0 0 0 / 80%);"></span>

                    <p style="text-align:center; margin-top:10px;">
                        <span style="font-weight: 500;">Start Time</span><br /><span style="color:#edaac6; font-weight: 500;"><?echo date('H:i',$bid_info['start_final']);?></span>
                    </p>
                </div>
                <div class="col-lg-6">
                    <span class="glyphicons glyphicons-clock x3" aria-hidden="true" style="margin: 0 auto; display: table; border-radius: 113px; -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 80%); -moz-box-shadow: 0px 0px 16px 0px rgba(0,0,0,0.8); box-shadow: 0px 0px 16px 0px rgb(0 0 0 / 80%);"></span>
                    <p style="text-align:center; margin-top:10px;">
                        <span style="font-weight: 500;">End Time</span><br /><span style="color:#edaac6; font-weight: 500;"><?echo date('H:i',$bid_info['end_final']);?></span>
                    </p>

                </div>
            </div>
            <div class="col-lg-1">

            </div>
            <div class="col-lg-5">
                <!-- <div class="map">
                    <p style="text-align:center;">
                        MAP COMING SOON
                    </p>
                </div> -->
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-12">
                <?if($bid_info['status'] == 1 && $bid_req_info['accepted_admin'] == 0 && $bid_amount_single['completed'] == 0){?>
                    <?if($bid_req_info['accepted_user'] != 1){?>
                        <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="accept_shift">Accept</button>
                    <?}?>
                    <button type="button" class="btn btn-sm btn-raised btn-warning">Close</button>
                    <?if($bid_req_info['accepted_user'] != 2){?>
                        <button type="button" class="btn btn-sm btn-raised btn-warning" id="reject_shift">Decline</button>
                    <?}?>
               <?}?>




            </div>







        </div>

    </div>


</div>

        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

</body>
</html>
    <!-- PAGE JS -->
<script>

jQuery(document).ready(function($) {
    var width = $('.map').width() - 200;
    $(".map").css({
        "height": width*(9/14)
    });
});


var bid_id = '<?echo $pid;?>';

$( "body" ).on( "click", "#accept_shift", function() {

    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/save_reponse.php",
          data: {'pid':bid_id,"type":"accept"},
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Response Updated',
                    type: 'info',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //Refresh the Page
                window.location.reload();

              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });

});

$( "body" ).on( "click", "#reject_shift", function() {

    $.ajax({
          type: "POST",
          url: $fullurl+"admin/rota/ajax/save_reponse.php",
          data: {'pid':bid_id,"type":"decline"},
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
                Messenger().post({
                    message: 'Response Updated',
                    type: 'info',
                    showCloseButton: false
                });
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');

                //Refresh the Page
                window.location.reload();

              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });

});

</script>
    <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
