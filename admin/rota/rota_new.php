<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

 $db->query("select * from accounts where id = ?");
 $db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
 $my_account = $db->single();

 $my_account_type = $my_account['account_type'];
 $extra='';

 if($my_account_type == 'user'){
     $extra = '&id='.$my_account['id'].'';
 }

 if($my_account_type == 'subadmin'){
     $extra = '&area='.$my_account['service_area'].'';
 }





?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?	//Base CSS Include
    	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>



<style>

.ledgend_wrapper{
height: 35px;
width: auto;
float: left;
margin-left: 25px;
}

.ledgend_wrapper:first-of-type {
margin:0px;
}

.ledgend_calendar{
height: 25px;
width: 25px;
position: relative;
float: left;
border-radius: 5px;
margin-right: 25px;
}

.legend_text{
position: relative;
float: left;
margin-top: 3px;
}

</style>

</head>
<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>

                      <!--- PAGE CONTENT HERE ---->
                      <div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-calendar page-title-glyph rota_background" aria-hidden="true"></span><span class="menuText">Manage Rota</span></div>




                    <div class="clearfix"></div>

                          <div class="calendar" id="calendar">

                          </div>
                        </div>
                </div>




        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>


</body>
</html>

<!-- PAGE JS -->

<!-- <script src="<? echo $fullurl ?>assets/app_scripts/crm/customer/index.js"></script> -->
<script src="<? echo $fullurl ?>admin/rota/js/index.js"></script>
<script src="../../../assets/js/picturefill.min.js"></script>
<script src="../../../assets/js/lightgallery.js"></script>
<script src="../../../assets/js/lg-fullscreen.js"></script>


<script>

$('.calendar').fullCalendar({
header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month,agendaWeek,agendaDay'
},
editable: false,
timezoneParam:'GMT',
eventLimit: true,
aspectRatio: 2,
buttonText: {
                today: 'Go To Today',
            },
buttonIcons: {
    prev: 'left-double-arrow',
    next: 'right-double-arrow',
},
eventSources: [

  {
      url: '../../../admin/rota/ajax/calendar.php?location=wirral<?echo $extra;?>', // use the `url` property
      color: '#46bbc7',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
  },
  {
      url: '../../../admin/rota/ajax/calendar.php?location=sefton<?echo $extra;?>', // use the `url` property
      color: '#5e4292',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
  },
  {
      url: '../../../admin/rota/ajax/calendar.php?location=liverpool<?echo $extra;?>', // use the `url` property
      color: '#f5a11c',    // an option!
      textColor: 'white',  // an option!
      className: 'reminders',
      id: 'id'
    }
],
eventClick: function(calEvent, jsEvent, view) {
  var appointmentID = calEvent.id;
    console.log(appointmentID);
    $("#BaseModalLContent").html($Loader);
$('#BaseModalL').modal('show');
    $.ajax({
        type: "POST",
        url: "../../../admin/rota/ajax/calendar_view_appointment.php?id="+appointmentID+"",
        success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
           .queue(function(n) {
               $(this).html(msg);
               n();
           }).fadeIn("slow").queue(function(n) {
                        $.material.init();
                       n();

                       $('#notes').summernote({
                     height: 300,                 // set editor height
                     minHeight: null,             // set minimum height of editor
                     maxHeight: null,             // set maximum height of editor
                     focus: true,                  // set focus to editable area after initializing summernote
                     toolbar: [
                       ['font', ['bold', 'italic', 'underline']],
                       ['color', ['color']],
                       ['para', ['ul', 'ol', 'paragraph']],
                       ['table', ['table']],
                       ['insert', ['link', 'hr']],
                       ['help', ['help']]
                       ]
                   });
           });
         }
    });
}

});

</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
