<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['id'];

?>

<h3>New Signature</h3>
<div id="signature"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-info pull-right" id="save_signature" data-id="<?echo $id;?>">Save Siganture</button>

<script>

$("#signature").jSignature();
$("#signature").resize();



$( "body" ).on( "click", "#save_signature", function() {
  var $signature=$("#signature").jSignature("getData");

  $.ajax({
      type: "POST",
      url: "../../../admin/rota/ajax/save_signature.php",
      data: {'id':'<?echo $id;?>',signature:$signature},
      success: function(msg){
        reloadtab(); $('.modal').modal('hide');
        //alert('sent');
     }
});
});

</script>
