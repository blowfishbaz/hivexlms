<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_accounts where id = ?");
$db->bind(1,$id);
$data = $db->single();


$db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
$db->bind(1,$id);
$data_extra = $db->single();

$db->query("select * from ws_accounts_roles where account_id = ? and status = 1");
$db->bind(1,$id);
$roles = $db->resultset();

$db->query("select * from ws_accounts_locations where account_id = ? and status = 1");
$db->bind(1,$id);
$locations = $db->resultset();

$db->query("select * from ws_roles where status = 1 order by title asc");
$db->bind(1,$id);
$all_roles = $db->resultset();

$my_roles = array();
$my_location = array();

foreach ($roles as $r) {
  array_push($my_roles, $r['role_id']);
}

foreach ($locations as $l) {
  array_push($my_location, $l['location_id']);
}

?>

<h3>Edit Account</h3>


<form id="edit_account_form">
  <input type="hidden" name="id" value="<?echo $id;?>" />
  <input type="hidden" name="id_extra" value="<?echo $data_extra['id'];?>" />


  <!-- First Name -->
  <div class="form-group label-floating ">
              <label for="first_name" class="control-label">First Name*</label>
              <p class="input_error_message">Please Enter your First Name</p>
              <input id="rota_first_name" type="text" name="first_name" class="form-control" value="<?echo $data['first_name'];?>" required>
            <span class="material-input"></span>
  </div>

  <!-- Surname -->
  <div class="form-group label-floating ">
              <label for="surname" class="control-label">Surname*</label>
              <p class="input_error_message">Please Enter your Surname</p>
              <input id="rota_surname" type="text" name="surname" class="form-control" value="<?echo $data['surname'];?>" required>
            <span class="material-input"></span>
  </div>

  <!-- Email -->
  <div class="form-group label-floating ">
              <label for="email" class="control-label">Email*</label>
              <p class="input_error_message">Please Enter your Email</p>
              <input id="rota_email" type="text" name="email" class="form-control" value="<?echo $data['email'];?>" required>
            <span class="material-input"></span>
  </div>

  <!-- Telephone -->
  <div class="form-group label-floating ">
              <label for="telephone" class="control-label">Telephone</label>
              <p class="input_error_message">1</p>
              <input id="rota_telephone" type="text" name="telephone" class="form-control" value="<?echo $data_extra['telephone'];?>">
            <span class="material-input"></span>
  </div>

  <!-- Mobile -->
  <div class="form-group label-floating ">
              <label for="mobile_number" class="control-label">Mobile Number</label>
              <p class="input_error_message">1</p>
              <input id="rota_mobile_number" type="text" name="mobile_number" class="form-control" value="<?echo $data_extra['mobile_number'];?>">
            <span class="material-input"></span>
  </div>

  <!-- Job Role -->
  <div class="form-group">
    <label class="control-label">Job Role*</label>
    <select name="job_role[]" class="form-control selectpicker" data-live-search="true" id="job_role" required multiple>
      <?foreach ($all_roles as $r) {?>
        <option value="<?echo $r['id'];?>" <?if(in_array($r['id'],$my_roles)){echo 'selected';}?>><?echo $r['title'];?></option>
      <?}?>
  </select>
</div>

<!-- Locations -->
<div class="form-group">
  <label class="control-label">Locations*</label>
  <select name="location[]" class="form-control selectpicker" data-live-search="true" id="location" required multiple>
    <option <?if(in_array('Wirral',$my_location)){echo 'selected';}?> value="Wirral">Wirral</option>
    <option <?if(in_array('Sefton',$my_location)){echo 'selected';}?> value="Sefton">Sefton</option>
    <option <?if(in_array('Liverpool',$my_location)){echo 'selected';}?> value="Liverpool">Liverpool</option>
</select>
</div>



<?if($id == decrypt($_SESSION['SESS_ACCOUNT_ID']) || my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?>
<input type="hidden" name="bank_check" value="yes" />
<div class="form-group label-floating ">
            <label for="bank_name" class="control-label">Bank Name*</label>
            <p class="input_error_message">Please Enter your Bank Name</p>
            <input id="rota_surname" type="text" name="bank_name" class="form-control" value="<?echo decrypt($data_extra['bank_name']);?>" required>
          <span class="material-input"></span>
</div>

<div class="form-group label-floating ">
            <label for="bank_account_number" class="control-label">Account Number*</label>
            <p class="input_error_message">Please Enter your Account Number</p>
            <input id="rota_surname" type="text" name="bank_account_number" class="form-control" value="<?echo decrypt($data_extra['bank_account_number']);?>" required>
          <span class="material-input"></span>
</div>

<div class="form-group label-floating ">
            <label for="bank_sort_code" class="control-label">Sort Code*</label>
            <p class="input_error_message">Please Enter your Sort Code</p>
            <input id="rota_surname" type="text" name="bank_sort_code" class="form-control" value="<?echo decrypt($data_extra['bank_sort_code']);?>" required>
          <span class="material-input"></span>
</div>
<?}else{?>
<input type="hidden" name="bank_check" value="no" />
<?}?>
</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_edit_account">Save</button>
