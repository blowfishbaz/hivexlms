<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_pcn where id = ?");
$db->bind(1,$id);
$data = $db->single();

?>

<h3>Edit PCN</h3>

<form id="edit_pcn_form">
  <input type="hidden" value="<?echo $id;?>" name="id" />
  <div class="form-group label-floating">
      <label for="pcn_name" class="control-label">PCN Name*</label>
      <input id="pcn_name" type="text" name="pcn_name" class="form-control" value="<?echo $data['pcn_title'];?>" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating">
      <label for="pcn_address" class="control-label">Address 1*</label>
      <input id="pcn_address" type="text" name="pcn_address" class="form-control" value="<?echo $data['address1'];?>" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating">
      <label for="pcn_address2" class="control-label">Address 2</label>
      <input id="pcn_address2" type="text" name="pcn_address2" class="form-control" value="<?echo $data['address2'];?>">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating">
      <label for="pcn_address3" class="control-label">Address 3</label>
      <input id="pcn_address3" type="text" name="pcn_address3" class="form-control" value="<?echo $data['address3'];?>">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating">
      <label for="pcn_city" class="control-label">City</label>
      <input id="pcn_city" type="text" name="pcn_city" class="form-control" value="<?echo $data['city'];?>">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating">
      <label for="pcn_county" class="control-label">County</label>
      <input id="pcn_county" type="text" name="pcn_county" class="form-control" value="<?echo $data['county'];?>">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating">
      <label for="pcn_postcode" class="control-label">Postcode*</label>
      <input id="pcn_postcode" type="text" name="pcn_postcode" class="form-control" value="<?echo $data['postcode'];?>" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Locations*</label>
    <select name="pcn_location" class="form-control" id="pcn_location" required>
      <option value="" selected disabled>Select</option>
      <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
        $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

        ?>
        <?if(in_array('Wirral',$my_areas)){?><option <?if($data['location_id'] == 'Wirral'){echo 'selected';}?> value="Wirral">Wirral</option><?}?>
        <?if(in_array('Sefton',$my_areas)){?><option <?if($data['location_id'] == 'Sefton'){echo 'selected';}?> value="Sefton">Sefton</option><?}?>
        <?if(in_array('Liverpool',$my_areas)){?><option <?if($data['location_id'] == 'Liverpool'){echo 'selected';}?> value="Liverpool">Liverpool</option><?}?>
      <?}else{?>
        <option <?if($data['location_id'] == 'Wirral'){echo 'selected';}?> value="Wirral">Wirral</option>
        <option <?if($data['location_id'] == 'Sefton'){echo 'selected';}?> value="Sefton">Sefton</option>
        <option <?if($data['location_id'] == 'Liverpool'){echo 'selected';}?> value="Liverpool">Liverpool</option>
      <?}?>

    </select>
  </div>

</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="update_new_pcn">Save</button>
