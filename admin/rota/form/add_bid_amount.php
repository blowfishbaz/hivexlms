<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_service where id = ?");
$db->bind(1,$id);
$service = $db->single();

$db->query("select ws_service_roles.id, ws_service_roles.hourly_rate, ws_roles.title from ws_service_roles left join ws_roles on ws_roles.id = ws_service_roles.job_role where ws_service_roles.pid = ? and ws_service_roles.status = 1");
$db->bind(1,$id);
$roles = $db->ResultSet();

?>
<div class="clearfix"></div>
<div class="row">
<?foreach ($roles as $r) {?>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="rota_amount_needed_<?echo $r['id'];?>" class="control-label"><?echo $r['title'];?> Needed?*</label>
        <input id="rota_amount_needed_<?echo $r['id'];?>" type="number" name="rota_amount_needed_<?echo $r['id'];?>" class="form-control" value="" step="1" required placeholder="How many staff needed?">
      <span class="material-input"></span>
    </div>
  </div>
<?}?>
</div>
<div class="clearfix"></div>
