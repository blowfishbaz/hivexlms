<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$service = $_POST['service'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$start_time = $_POST['start_time'];
$end_time = $_POST['end_time'];

$avail_array = array();

$db->query("select id, location, pcn_id, practice_id from ws_service where id = ?");
$db->bind(1,$service);
$service_data = $db->single();

$db->query("select id, job_role, pid from ws_service_roles where pid = ? and status = 1");
$db->bind(1,$service);
$roles = $db->ResultSet();

foreach ($roles as $r) {
  $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id
  from ws_accounts_locations
  left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
  where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1");
  $db->bind(1,$service_data['location']);
  $db->bind(2,$r['job_role']);
  $list = $db->ResultSet();

  foreach ($list as $l) {
    $db->query("SELECT * FROM ws_availability WHERE account_id = ? and start_ts <= ? and end_ts >= ? and status = 1 ORDER BY start_ts ASC");
    $db->bind(1,$l['account_id']);
    $db->bind(2,strtotime($start_date.' '.$start_time));
    $db->bind(3,strtotime($end_date.' '.$end_time));
    $availability = $db->resultset();
    $availability_count = $db->rowcount();

    if($availability_count >=1){
      $db->query("select * from accounts where id = ?");
      $db->bind(1,$l['account_id']);
      $account = $db->single();

      $db->query("select * from ws_roles where id = ?");
      $db->bind(1,$r['job_role']);
      $role_check = $db->single();
      array_push($avail_array, array('name'=>$account['name'],'role'=>$role_check['title']));
    }


  }
}





?>
<?if(!empty($start_date) && !empty($end_date) && !empty($start_time) && !empty($end_time)){?>
  <h3>Staff Availability</h3>
<table>
  <tr>
    <th>Account Name</th>
    <th style="border-right:none;">Role</th>
  </tr>
  <?if(!empty($avail_array)){?>
  <?foreach ($avail_array as $aa) {?>
    <tr>
      <td>
        <?echo $aa['name'];?>
      </td>
      <td>
        <?echo $aa['role'];?>
      </td>
    </tr>
  <?}?>
  <?}else{echo '<tr> <th colspan="2" style="border-right:none;"> No Available Accounts </th> </tr>';}?>
</table>
<?}?>
