<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select * from ws_pcn where status = 1");
$pcn = $db->resultset();

?>

<h3>Add Practice</h3>

<form id="add_practice_form">

  <div class="form-group label-floating is-empty">
      <label for="practice_name" class="control-label">Practice Name*</label>
      <input id="practice_name" type="text" name="practice_name" class="form-control" value="" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="practice_address" class="control-label">Address 1*</label>
      <input id="practice_address" type="text" name="practice_address" class="form-control" value="" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="practice_address2" class="control-label">Address 2</label>
      <input id="practice_address2" type="text" name="practice_address2" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="practice_address3" class="control-label">Address 3</label>
      <input id="practice_address3" type="text" name="practice_address3" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="practice_city" class="control-label">City</label>
      <input id="practice_city" type="text" name="practice_city" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="practice_county" class="control-label">County</label>
      <input id="practice_county" type="text" name="practice_county" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="practice_postcode" class="control-label">Postcode*</label>
      <input id="practice_postcode" type="text" name="practice_postcode" class="form-control" value="" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Locations*</label>
    <select name="practice_location" class="form-control" id="practice_location" required>
      <option value="" selected disabled>Select</option>
      <option value="Wirral">Wirral</option>
      <option value="Liverpool">Liverpool</option>
      <option value="Sefton">Sefton</option>
      <option disabled>----------------</option>
      <?foreach ($pcn as $p) {?>
        <option value="<?echo $p['id'];?>"><?echo $p['pcn_title'];?></option>
      <?}?>

    </select>
  </div>

</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_new_practice">Save</button>
