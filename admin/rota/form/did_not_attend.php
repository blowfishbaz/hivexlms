<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];


 ?>

 <h3>Did not Attend</h3>

 <form id="did_not_attend_form">
   <input type="hidden" name="id" value="<?echo $id;?>" />

   <div class="form-group label-floating is-empty">
      <label for="dna_message" class="control-label">Reason for DNA*</label>
      <p class="input_error_message">Please type a message</p>
      <textarea rows="4" type="textarea" class="form-control" id="dna_message" name="dna_message" required="yes"> </textarea>
   </div>
 </form>

 <button type="button" class="btn btn-raised btn-warning btn-sm" data-dismiss="modal">Cancel</button>
 <button type="button" class="btn btn-raised btn-success btn-sm pull-right" id="confirm_did_not_attend">Confirm</button>
