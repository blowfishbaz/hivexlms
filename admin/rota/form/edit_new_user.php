<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from accounts where id = ?");
$db->bind(1,$id);
$data = $db->single();

?>

<h3>Edit New User</h3>

<form id="edit_new_user">

  <input type="hidden" name="id" value="<?echo $id;?>" />

<div class="col-md-6 col-sm-6">
<div class="form-group label-floating">
<label for="name" class="control-label">Name*</label>
<input type="text" class="form-control" id="name" name="name" required="yes" value="<?echo $data['name'];?>"><span class="material-input"></span></div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group label-floating">
<label for="email" class="control-label">Email*</label>
<input type="email" class="form-control" id="email" name="email" required="yes" value="<?echo $data['email'];?>"><span class="material-input"></span></div>
</div>

<?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?>
<div class="clearfix"></div>

<?

  $my_areas = subadmin_areas($id);

  $my_prac_pcn = myPractPCNArray($id);

?>

<div class="col-md-6 col-sm-6">
<div class="form-group label-floating">
<label for="email" class="control-label">Account Type*</label>
<select class="form-control chosen-select" id="type" name="type" required="yes">
<?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?><option value="admin" <?if($data['account_type'] == 'admin'){echo 'selected';}?>>Admin</option><?}?>
<option value="subadmin" <?if($data['account_type'] == 'subadmin'){echo 'selected';}?>>Sub Admin</option>
</select>
</div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group" style="margin-top:0px;">
<label for="location" class="control-label" style="margin-top:13px;">Account Location</label>
<select class="form-control chosen-select" id="location_multiple" name="location[]" multiple <?if($data['account_type'] == 'admin'){?>disabled<?}?>>

  <option value="Wirral" <?if(in_array('Wirral',$my_areas)){echo 'selected';}?>>Wirral</option>
  <option value="Sefton" <?if(in_array('Sefton',$my_areas)){echo 'selected';}?>>Sefton</option>
  <option value="Liverpool" <?if(in_array('Liverpool',$my_areas)){echo 'selected';}?>>Liverpool</option>


</select>
</div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12">
  <!-- PCN & Practice -->
  <div class="form-group pcn-practice-div" <?if($data['account_type'] == 'admin'){echo 'style="display:none;"';}?>>
    <label class="control-label">PCN &amp; Practice*</label>
    <select name="pcn_practice[]" class="form-control" id="pcn_practice" multiple>

      <?
      //echo $subadmin_query;

      foreach ($my_areas as $ma) {

          $db->query("select * from ws_pcn where location_id = ? and status = 1");
          $db->bind(1,$ma);
          $pcn = $db->ResultSet();

          $db->query("select * from ws_practice where pcn_location = '' and location_id = ? and status = 1");
          $db->bind(1,$ma);
          $stand_alone_practices = $db->ResultSet();

          if(empty($pcn)){
            $db->query("select * from ws_practice where location_id = ? and status = 1");
            $db->bind(1,$ma);
            $stand_alone_practices = $db->ResultSet();
          }

          ?>

          <?foreach ($pcn as $p) {?>
            <option value="<?echo $p['id'];?>" <?if(in_array($p['id'],$my_prac_pcn)){echo 'selected';}?>><?echo $p['pcn_title'];?></option>
            <?
            $db->query("select * from ws_practice where pcn_location = ? and location_id = ? and status = 1");
            $db->bind(1,$p['id']);
            $db->bind(2,$ma);
            $practice = $db->ResultSet();
            ?>
            <?foreach ($practice as $prac) {?>
              <option value="<?echo $prac['id'];?>" <?if(in_array($prac['id'],$my_prac_pcn)){echo 'selected';}?>><?echo $prac['title'];?></option>
            <?}?>
            <option disabled>----------------</option>
          <?}?>

          <?foreach ($stand_alone_practices as $sap) {?>
            <option value="<?echo $sap['id'];?>" <?if(in_array($sap['id'],$my_prac_pcn)){echo 'selected';}?>><?echo $sap['title'];?></option>
          <?}?>
      <?}?>
    </select>
  </div>
</div>
<div class="clearfix"></div>

<?}?>
</form>

<!--

Name
email

account type
locations
password
confirm password

 -->

 <div class="clearfix"></div>

 <button type="button" class="btn btn-sm btn-warning btn-raised" data-dismiss="modal">Close</button>

 <button type="button" class="btn btn-sm btn-success btn-raised pull-right" id="edit_new_account">Update</button>
