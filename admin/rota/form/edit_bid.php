<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_rota_bid where id = ?");
$db->bind(1,$id);
$bid_data = $db->single();

$db->query("select * from ws_service where id = ?");
$db->bind(1,$bid_data['service_id']);
$service = $db->single();

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
$db->bind(1,$bid_data['id']);
$bid_amount = $db->resultset();

?>

<h3>Edit Bid - <?echo $service['title'];?></h3>

<form id="edit_bid_form">

  <input type="hidden" name="id" value="<?echo $id;?>" />

  <div class="bid_info_holder">
    <div class="row">

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="start_date" class="control-label">Start Date*</label>
            <input id="start_date" type="date" name="start_date" class="form-control" value="<?echo date('Y-m-d',strtotime($bid_data['start_date']));?>" placeholder="dd-mm-yyyy" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="end_date" class="control-label">End Date*</label>
            <input id="end_date" type="date" name="end_date" class="form-control" value="<?echo date('Y-m-d',strtotime($bid_data['end_date']));?>" placeholder="dd-mm-yyyy" required>
          <span class="material-input"></span>
        </div>
      </div>

    </div>
    <div class="row">

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="start_time" class="control-label">Start Time*</label>
            <input id="start_time" type="time" name="start_time" class="form-control" value="<?echo $bid_data['start_time'];?>" placeholder="hh:mm" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="end_time" class="control-label">End Time*</label>
            <input id="end_time" type="time" name="end_time" class="form-control" value="<?echo $bid_data['end_time'];?>" placeholder="hh:mm" required>
          <span class="material-input"></span>
        </div>
      </div>

    </div>

    <!-- Loop through for the hourly rates and how many people are needed for it -->
    <div class="bid_amount_holder">
      <div class="clearfix"></div>
      <div class="row">

        <?foreach ($bid_amount as $ba) {?>
          <?
          $db->query("select * from ws_roles where id = ?");
          $db->bind(1,$ba['role_id']);
          $role = $db->single();
          ?>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="rota_amount_needed_<?echo $ba['id'];?>" class="control-label"><?echo $role['title'];?> Needed?*</label>
                <input id="rota_amount_needed_<?echo $ba['id'];?>" type="number" name="rota_amount_needed_<?echo $ba['id'];?>" class="form-control" value="<?echo $ba['amount'];?>" step="1" required placeholder="How many staff needed?">
              <span class="material-input"></span>
            </div>
          </div>
        <?}?>


      </div>
      <div class="clearfix"></div>
    </div>



  </div>

</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_edit_bid">Save</button>
