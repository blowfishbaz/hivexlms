<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and location in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")';

  $prac_search = sub_admin_query('rb.pcn_id');
  $pcn_search = sub_admin_query('rb.practice_id','or');

  if(!empty($prac_search) && !empty($pcn_search)){
    $subadmin_search .=" and ($prac_search $pcn_search)";
  }
}

$recalc_needed = 0;


$db->query("select * from ws_pcn where id = ?");
$db->bind(1,$bid_info['pcn_id']);
$pcn = $db->single();

$db->query("select * from ws_practice where id = ?");
$db->bind(1,$bid_info['practice_id']);
$practice = $db->single();


$db->query("select rb.*, ws_service.title, ws_service.location, ws_pcn.pcn_title as pcn_title, ws_practice.title as prac_title
from ws_rota_bid rb
left join ws_service on ws_service.id = rb.service_id
left join ws_pcn on ws_pcn.id = rb.pcn_id
left join ws_practice on ws_practice.id = rb.practice_id
where rb.status = ? and rb.completed = 0 and start_final > ? $subadmin_search order by start_final asc");
$db->bind(1,'1');
$db->bind(2,strtotime('yesterday'));
$data = $db->ResultSet();

?>
<style>
  #recalc_table{
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  #recalc_table td, #recalc_table th{
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }

  #recalc_table th{

  }
</style>

<h3>Recalculate All Shifts</h3>
<p>Are you sure you want to recalculate all shifts?<br />The below shifts will be recalculated.</p>




<table id="recalc_table">
  <tr style="background-color:#F0F0F0;">
    <th>Service Title</th>
    <th>Location</th>
    <th>PCN / Practice</th>
    <th>Start Date</th>
  </tr>
  <?foreach ($data as $d) {?>
    <?
      $db->query("select * from ws_service where id = ?");
      $db->bind(1,$d['service_id']);
      $service = $db->single();

      $db->query("select ws_rota_bid_amount.*, ws_roles.title as role_title from ws_rota_bid_amount left join ws_roles on ws_roles.id = ws_rota_bid_amount.role_id where ws_rota_bid_amount.pid = ? and ws_rota_bid_amount.status = 1 and ws_rota_bid_amount.completed = 0");
      $db->bind(1,$d['id']);
      $bid_req_info_all = $db->resultset();

      $id_already = '("0",';

      foreach ($bid_req_info_all as $bria) {
        $db->query("select * from ws_rota_bid_request where pid = ? and service_role_id = ?");
        $db->bind(1,$d['id']);
        $db->bind(2,$bria['service_role_id']);
        $bid_req_all = $db->resultset();

        foreach ($bid_req_all as $request) {
          //Send Email
          $db->query("select * from accounts where id = ?");
          $db->bind(1,$request['account_id']);
          $user = $db->single();

          $id_already .='"'.$request['account_id'].'",';

        }
      }

      $id_already .= '"0")';


    ?>
    <tr>
      <td><?echo $d['title'];?></td>
      <td><?echo $d['location'];?></td>
      <td><?echo $d['pcn_title'].' '.$d['prac_title'];?></td>
      <td><?echo date('d-m-Y H:i',$d['start_final']);?></td>
    </tr>
    <?foreach ($bid_req_info_all as $bria) {?>
      <?
        $recalc_inner_needed = 0;
        $job_role = $bria['role_id'];
        $location_id = $service['location'];

        $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id, ws_accounts.status , ws_accounts.first_name, ws_accounts.surname
        from ws_accounts_locations
        left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
        left join ws_accounts on ws_accounts.id = ws_accounts_locations.account_id
        where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1 and ws_accounts.status = 1 and ws_accounts.id not in $id_already");
        $db->bind(1,$location_id);
        $db->bind(2,$job_role);
        $list = $db->ResultSet();

        foreach ($list as $l) {
          $recalc_needed = 1;
          $recalc_inner_needed = 1;
      ?>
      <tr>
        <td colspan="2"><?echo $bria['role_title'];?></td>
        <td colspan="2"><?echo $l['first_name'].' '.$l['surname'];?></td>
      </tr>
    <?}?>
  <?}?>
      <?if($recalc_inner_needed == 0){?><tr><td colspan="4">No Recalculation Needed</td></tr><?}?>
    <tr style="background-color:#F0F0F0;">
      <th colspan="4"> </th>
    </tr>
  <?}?>

</table>












<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<?if($recalc_needed == '1'){?><button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="confirm_recalculate_all_shifts">Confirm</button><?}?>
