<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


?>

<h3>Add PCN</h3>

<form id="add_pcn_form">

  <div class="form-group label-floating is-empty">
      <label for="pcn_name" class="control-label">PCN Name*</label>
      <input id="pcn_name" type="text" name="pcn_name" class="form-control" value="" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="pcn_address" class="control-label">Address 1*</label>
      <input id="pcn_address" type="text" name="pcn_address" class="form-control" value="" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="pcn_address2" class="control-label">Address 2</label>
      <input id="pcn_address2" type="text" name="pcn_address2" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="pcn_address3" class="control-label">Address 3</label>
      <input id="pcn_address3" type="text" name="pcn_address3" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="pcn_city" class="control-label">City</label>
      <input id="pcn_city" type="text" name="pcn_city" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="pcn_county" class="control-label">County</label>
      <input id="pcn_county" type="text" name="pcn_county" class="form-control" value="">
    <span class="material-input"></span>
  </div>

  <div class="form-group label-floating is-empty">
      <label for="pcn_postcode" class="control-label">Postcode*</label>
      <input id="pcn_postcode" type="text" name="pcn_postcode" class="form-control" value="" required>
    <span class="material-input"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Locations*</label>
    <select name="pcn_location" class="form-control" id="pcn_location" required>
      <option value="" selected disabled>Select</option>
      <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
        $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

        ?>
        <?if(in_array('Wirral',$my_areas)){?><option value="Wirral">Wirral</option><?}?>
        <?if(in_array('Sefton',$my_areas)){?><option value="Sefton">Sefton</option><?}?>
        <?if(in_array('Liverpool',$my_areas)){?><option value="Liverpool">Liverpool</option><?}?>
      <?}else{?>
        <option value="Wirral">Wirral</option>
        <option value="Sefton">Sefton</option>
        <option value="Liverpool">Liverpool</option>
      <?}?>

    </select>
  </div>

</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_new_pcn">Save</button>
