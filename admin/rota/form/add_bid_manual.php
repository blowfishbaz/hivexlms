<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
  $subadmin_search = ' and location in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")';

  $subadmin_query_pcn = sub_admin_query('pcn_id');
  $subadmin_query_practice = sub_admin_query('practice_id','or');



  if(!empty($subadmin_query_pcn) && !empty($subadmin_query_practice)){
    $subadmin_search .= " and($subadmin_query_pcn $subadmin_query_practice)";
  }
}


$db->query("select * from ws_service where status = 1 $subadmin_search");
$services = $db->resultset();

$date = $_GET['date'];

?>

<h3>Add Bid - Manual</h3>

<p class="add_bid_message">Please select a service to continue</p>
<form id="add_bid_manual_form">

  <input type="hidden" name="id" value="<?echo createid('bid');?>" />

  <!-- Select Service -->
  <div class="form-group">
      <label class="control-label">Service*</label>
      <select name="service" class="form-control" data-live-search="true" id="service_manual" required>
        <option value="" selected disabled>Select</option>
        <?foreach ($services as $s) {?>
          <option value="<?echo $s['id'];?>"><?echo $s['title'];?></option>
        <?}?>
    </select>
  </div>

  <div class="service_table_holder">

  </div>

  <div class="bid_info_holder" style="display:none;">
    <div class="row">

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="start_date" class="control-label">Start Date*</label>
            <input id="start_date" type="date" name="start_date" class="form-control" value="<?if(!empty($date)){ echo date("Y-m-d", ($date / 1000) ); }?>" placeholder="dd-mm-yyyy" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="end_date" class="control-label">End Date*</label>
            <input id="end_date" type="date" name="end_date" class="form-control" value="<?if(!empty($date)){ echo date("Y-m-d", ($date / 1000) ); }?>" placeholder="dd-mm-yyyy" required>
          <span class="material-input"></span>
        </div>
      </div>

    </div>
    <div class="row">

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="start_time" class="control-label">Start Time*</label>
            <input id="start_time" type="time" name="start_time" class="form-control" value="" placeholder="hh:mm" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="end_time" class="control-label">End Time*</label>
            <input id="end_time" type="time" name="end_time" class="form-control" value="" placeholder="hh:mm" required>
          <span class="material-input"></span>
        </div>
      </div>

    </div>

    <!-- Loop through for the hourly rates and how many people are needed for it -->
    <div class="bid_amount_holder_manual">

    </div>



  </div>

</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" style="display:none;" id="save_add_bid_manual">Save</button>
