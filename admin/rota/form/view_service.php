<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_service where id = ?");
$db->bind(1,$id);
$service = $db->single();

$db->query("select ws_service_roles.hourly_rate, ws_roles.title from ws_service_roles left join ws_roles on ws_roles.id = ws_service_roles.job_role where ws_service_roles.pid = ? and ws_service_roles.status = 1");
$db->bind(1,$id);
$roles = $db->ResultSet();

?>
<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
  text-align: center;
      width: 220px;
}
</style>



<table>
  <tr>
    <th colspan="2" style="border-right:0px; text-align:center;">
      Service Details
    </th>
  </tr>
  <tr>
    <th>Title</th>
    <td><?echo $service['title'];?></td>
  </tr>
  <tr>
    <th>Description</th>
    <td><?echo $service['description'];?></td>
  </tr>
  <tr>
    <th>Location</th>
    <td><?echo $service['location'];?></td>
  </tr>
  <tr>
    <th>Invoice Address</th>
    <td><?echo $service['invoice_address'];?></td>
  </tr>
  <tr>
    <th colspan="2" style="border-right:0px; text-align:center;">
      Job Roles
    </th>
  </tr>
  <?foreach ($roles as $r) {?>
    <tr>
      <th><?echo $r['title'];?></th>
      <td><?echo $r['hourly_rate'];?> Per Hour</td>
    </tr>
  <?}?>
</table>

<button type="button" class="btn btn-sm btn-raised btn-warning " data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-danger " id="delete_service" data-id="<?echo $id;?>">Delete</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right " id="edit_service" data-id="<?echo $id;?>">Edit</button>
