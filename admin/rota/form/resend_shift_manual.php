<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['bid_id'];

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1 and completed = 0");
$db->bind(1,$id);
$bid_req_info_all = $db->resultset();

?>

<?foreach ($bid_req_info_all as $bid_amount) {?>
  <?
  $db->query("select ws_roles.title
              from ws_roles
              left join ws_service_roles on ws_service_roles.job_role = ws_roles.id
              where ws_service_roles.id = ?");
  $db->bind(1,$bid_amount['service_role_id']);
  $role = $db->single();

  $db->query("select accounts.*
              from ws_rota_bid_request
              join accounts on ws_rota_bid_request.account_id = accounts.id
              where pid = ? and service_role_id = ? and accepted_user != 3 and accepted_admin = 0");
  $db->bind(1,$id);
  $db->bind(2,$bid_amount['service_role_id']);
  $users = $db->resultset();
  ?>
  <table style="margin-bottom:20px;">
    <tr>
      <th>Role</th>
      <th style="border-right:0px;">User</th>
    </tr>
    <tr>
      <td style="border-right: 1px solid #eee;"><?echo $role['title'];?></td>
      <td>
        <div class="form-group is-empty" style="    margin-top: 0px;">
            <label class="control-label">Service*</label>
            <select name="service" class="form-control <?echo $bid_amount['id'];?>" data-live-search="true" id="service_manual" required="">
              <option value="" selected="" disabled="">Select</option>
              <?foreach ($users as $u) {?>
                <option value="<?echo $u['id'];?>"><?echo $u['name'];?></option>
              <?}?>

            </select>
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <button type="button" class="btn btn-sm btn-raised btn-success pull-right confirm-manual-add" data-id="<?echo $bid_amount['id'];?>">Confirm Manual Add</button>
      </td>
    </tr>
  </table>
<?}?>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
