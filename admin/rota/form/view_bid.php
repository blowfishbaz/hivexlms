<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_rota_bid where id = ?");
$db->bind(1,$id);
$bid_data = $db->single();

$db->query("select * from ws_service where id = ?");
$db->bind(1,$bid_data['service_id']);
$service = $db->single();

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
$db->bind(1,$bid_data['id']);
$bid_amount = $db->resultset();



?>
<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
}
</style>
<h3>View Bid</h3>
<table>
  <tr>
    <th>Service Title</th>
    <td><?echo $service['title'];?></td>
  </tr>
  <tr>
    <th>Start Date</th>
    <td><?echo $bid_data['start_date'];?></td>
  </tr>
  <tr>
    <th>Start Time</th>
    <td><?echo $bid_data['start_time'];?></td>
  </tr>
  <tr>
    <th>End Date</th>
    <td><?echo $bid_data['end_date'];?></td>
  </tr>
  <tr>
    <th>End Time</th>
    <td><?echo $bid_data['end_time'];?></td>
  </tr>
</table>
<div class="clearfix" style="margin-top:10px;"></div>
  <table>
<?foreach ($bid_amount as $ba) {?>
  <?
    $db->query("select * from ws_roles where id = ?");
    $db->bind(1,$ba['role_id']);
    $role = $db->single();

    $db->query("select COALESCE(sum(seen),0) as total from ws_rota_bid_request where seen = 1 and pid = ? and bid_amount_pid = ? and (accepted_user = 1 or accepted_user = 2)");
    $db->bind(1,$id);
    $db->bind(2,$ba['id']);
    $current = $db->single();
  ?>

    <tr>
      <th><?echo $ba['amount'];?> - <?echo $role['title'];?> Needed</th>
      <td>Responses - <?echo $current['total'];?></td>
    </tr>

<?}?>
  </table>

  <button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
  <a href="<?echo $fullurl;?>admin/rota/admin/view_responses.php?id=<?echo $id;?>" class="btn btn-sm btn-raised btn-info" id="view_responses" data-id="<?echo $id;?>">View Responses<a>

    <?if($bid_data['completed'] == 0){?>
      <button type="button" class="btn btn-sm btn-raised btn-info pull-right" id="edit_view_bid" data-id="<?echo $id;?>">Edit</button>
      <button type="button" class="btn btn-sm btn-raised btn-danger" id="delete_bid" data-id="<?echo $id;?>">Delete</button>
    <?}?>
