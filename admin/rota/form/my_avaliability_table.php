<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $account_id = $_POST['id'];
 $week_num = $_POST['week_comm'];

 $db->query("select * from ws_availability where account_id = ? and week_number = ? and status = 1 ORDER BY CASE WHEN day = 'monday' THEN 1 WHEN day = 'tuesday' THEN 2 WHEN day = 'wednesday' THEN 3 WHEN day = 'thursday' THEN 4 WHEN day = 'friday' THEN 5 WHEN day = 'saturday' THEN 6 WHEN day = 'sunday' THEN 7 END ASC");
 $db->bind(1,$account_id);
 $db->bind(2,$week_num);
 $data = $db->resultset();

 $array_final = array('day_check'=>array(),'data'=>array());

 foreach ($data as $d) {
   array_push($array_final['day_check'], $d['day']);

   $array_final['data'][''.$d['day'].''] = array('start'=>$d['start_time'],'end'=>$d['end_time']);
 }

 ?>

 <tr>
   <th>Monday</th>
   <td> <label class="checkbox-inline"> <input type="checkbox" value="monday" name="days_available[]" class="days_available" data-id="monday" <?if(in_array('monday',$array_final['day_check'])){echo 'checked';}?>> Available</label> </td>
 </tr>
 <tr class="monday_available_time" <?if(!in_array('monday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_monday" class="control-label">Available From</label> <input id="start_time_monday" type="time" name="start_time_monday" class="form-control" value="<?echo $array_final['data']['monday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_monday" class="control-label">Available To</label> <input id="end_time_monday" type="time" name="end_time_monday" class="form-control" value="<?echo $array_final['data']['monday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
 <tr>
   <th>Tuesday</th>
   <td><label class="checkbox-inline"> <input type="checkbox" value="tuesday" name="days_available[]" class="days_available" data-id="tuesday" <?if(in_array('tuesday',$array_final['day_check'])){echo 'checked';}?>> Available</label></td>
 </tr>
 <tr class="tuesday_available_time" <?if(!in_array('tuesday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_tuesday" class="control-label">Available From</label> <input id="start_time_tuesday" type="time" name="start_time_tuesday" class="form-control" value="<?echo $array_final['data']['tuesday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_tuesday" class="control-label">Available To</label> <input id="end_time_tuesday" type="time" name="end_time_tuesday" class="form-control" value="<?echo $array_final['data']['tuesday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
 <tr>
   <th>Wednesday</th>
   <td><label class="checkbox-inline"> <input type="checkbox" value="wednesday" name="days_available[]" class="days_available" data-id="wednesday" <?if(in_array('wednesday',$array_final['day_check'])){echo 'checked';}?>> Available</label></td>
 </tr>
 <tr class="wednesday_available_time" <?if(!in_array('wednesday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_wednesday" class="control-label">Available From</label> <input id="start_time_wednesday" type="time" name="start_time_wednesday" class="form-control" value="<?echo $array_final['data']['wednesday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_wednesday" class="control-label">Available To</label> <input id="end_time_wednesday" type="time" name="end_time_wednesday" class="form-control" value="<?echo $array_final['data']['wednesday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
 <tr>
   <th>Thursday</th>
   <td><label class="checkbox-inline"> <input type="checkbox" value="thursday" name="days_available[]" class="days_available" data-id="thursday" <?if(in_array('thursday',$array_final['day_check'])){echo 'checked';}?>> Available</label></td>
 </tr>
 <tr class="thursday_available_time" <?if(!in_array('thursday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_thursday" class="control-label">Available From</label> <input id="start_time_thursday" type="time" name="start_time_thursday" class="form-control" value="<?echo $array_final['data']['thursday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_thursday" class="control-label">Available To</label> <input id="end_time_thursday" type="time" name="end_time_thursday" class="form-control" value="<?echo $array_final['data']['thursday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
 <tr>
   <th>Friday</th>
   <td><label class="checkbox-inline"> <input type="checkbox" value="friday" name="days_available[]" class="days_available" data-id="friday" <?if(in_array('friday',$array_final['day_check'])){echo 'checked';}?>> Available</label></td>
 </tr>
 <tr class="friday_available_time" <?if(!in_array('friday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_friday" class="control-label">Available From</label> <input id="start_time_friday" type="time" name="start_time_friday" class="form-control" value="<?echo $array_final['data']['friday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_friday" class="control-label">Available To</label> <input id="end_time_friday" type="time" name="end_time_friday" class="form-control" value="<?echo $array_final['data']['friday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
 <tr>
   <th>Saturday</th>
   <td><label class="checkbox-inline"> <input type="checkbox" value="saturday" name="days_available[]" class="days_available" data-id="saturday" <?if(in_array('saturday',$array_final['day_check'])){echo 'checked';}?>> Available</label></td>
 </tr>
 <tr class="saturday_available_time" <?if(!in_array('saturday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_saturday" class="control-label">Available From</label> <input id="start_time_saturday" type="time" name="start_time_saturday" class="form-control" value="<?echo $array_final['data']['saturday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_saturday" class="control-label">Available To</label> <input id="end_time_saturday" type="time" name="end_time_saturday" class="form-control" value="<?echo $array_final['data']['saturday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
 <tr>
   <th>Sunday</th>
   <td><label class="checkbox-inline"> <input type="checkbox" value="sunday" name="days_available[]" class="days_available" data-id="sunday" <?if(in_array('sunday',$array_final['day_check'])){echo 'checked';}?>> Available</label></td>
 </tr>
 <tr class="sunday_available_time" <?if(!in_array('sunday',$array_final['day_check'])){echo 'style="display:none;"';}?>>
    <td colspan="2">
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="start_time_sunday" class="control-label">Available From</label> <input id="start_time_sunday" type="time" name="start_time_sunday" class="form-control" value="<?echo $array_final['data']['sunday']['start'];?>"> <span class="material-input"></span> </div> </div>
       <div class="col-lg-6"> <div class="form-group" style="margin-top:0px;"> <label for="end_time_sunday" class="control-label">Available To</label> <input id="end_time_sunday" type="time" name="end_time_sunday" class="form-control" value="<?echo $array_final['data']['sunday']['end'];?>"> <span class="material-input"></span> </div> </div>
    </td>
 </tr>
