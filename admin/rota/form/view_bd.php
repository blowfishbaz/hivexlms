<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
$db->bind(1,$id);
$data_extra = $db->single();

?>

<h3>View Details</h3>

<table>
  <tr>
    <th>Bank Name</th>
    <td><?echo decrypt($data_extra['bank_name']);?></td>
  </tr>
  <tr>
    <th>Bank Account Number</th>
    <td><?echo decrypt($data_extra['bank_account_number']);?></td>
  </tr>
  <tr>
    <th>Sort Code</th>
    <td><?echo decrypt($data_extra['bank_sort_code']);?></td>
  </tr>
</table>

<button type="button" class="btn btn-raised btn-success" data-dismiss="modal">Close</button>
