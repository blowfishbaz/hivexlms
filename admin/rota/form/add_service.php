<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$db->query("select title,id from ws_roles where status = 1 order by title asc");
$roles = $db->resultset();

if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

}
?>

<h3>Add Service</h3>

<form id="add_service_form">

  <input type="hidden" name="id" value="<?echo createid('serv');?>" />

  <!-- Title -->
  <div class="form-group label-floating is-empty">
              <label for="title" class="control-label">Title*</label>
              <input id="title" type="text" name="title" class="form-control" value="" required>
            <span class="material-input"></span>
  </div>

  <!-- Description -->
  <div class="form-group label-floating is-empty">
              <label for="description" class="control-label">Description*</label>
              <input id="description" type="text" name="description" class="form-control" value="" required>
            <span class="material-input"></span>
  </div>

  <!-- Location -->
  <div class="form-group">
    <label class="control-label">Locations*</label>
    <select name="location" class="form-control location_add" id="location" required>
      <option value="" selected disabled>Select</option>
      <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
        $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

        ?>
        <?if(in_array('Wirral',$my_areas)){?><option value="Wirral">Wirral</option><?}?>
        <?if(in_array('Sefton',$my_areas)){?><option value="Sefton">Sefton</option><?}?>
        <?if(in_array('Liverpool',$my_areas)){?><option value="Liverpool">Liverpool</option><?}?>
      <?}else{?>
        <option value="Wirral">Wirral</option>
        <option value="Sefton">Sefton</option>
        <option value="Liverpool">Liverpool</option>
      <?}?>
    </select>
  </div>

  <!-- Description -->
  <div class="form-group label-floating is-empty">
              <label for="address1" class="control-label">Address 1*</label>
              <input id="address1" type="text" name="address1" class="form-control" value="" required>
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="address2" class="control-label">Address 2</label>
              <input id="address2" type="text" name="address2" class="form-control" value="" >
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="address3" class="control-label">Address 3</label>
              <input id="address3" type="text" name="address3" class="form-control" value="" >
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="city" class="control-label">City</label>
              <input id="city" type="text" name="city" class="form-control" value="" >
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="county" class="control-label">County</label>
              <input id="county" type="text" name="county" class="form-control" value="" >
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="postcode" class="control-label">Postcode*</label>
              <input id="postcode" type="text" name="postcode" class="form-control" value="" required>
            <span class="material-input"></span>
  </div>


  <!-- PCN & Practice -->
  <div class="form-group pcn-practice-div" style="display:none;">
    <label class="control-label">PCN &amp; Practice*</label>
    <select name="pcn_practice" class="form-control" id="pcn_practice" required>
      <option value="" selected disabled>Select</option>



    </select>
  </div>

  <!-- Roles -->
  <div class="form-group">
      <label class="control-label">Job Role*</label>
      <select name="job_role[]" class="form-control selectpicker" data-live-search="true" id="job_role" required multiple>
        <?foreach ($roles as $r) {?>
          <option value="<?echo $r['id'];?>"><?echo $r['title'];?></option>
        <?}?>
    </select>
  </div>

  <!-- Hourly Rates -->
  <?foreach ($roles as $r_hr) {?>
    <div class="form-group label-floating is-empty hourly_rate_holders hourly_rate_holder_<?echo $r_hr['id'];?>" style="display:none;">
                <label for="hourly_rate_<?echo $r_hr['id'];?>" class="control-label">Hourly Rate - <?echo $r_hr['title'];?>*</label>
                <input id="hourly_rate_<?echo $r_hr['id'];?>" type="text" name="hourly_rate_<?echo $r_hr['id'];?>" class="form-control" value="">
              <span class="material-input"></span>
    </div>
  <?}?>


  <!-- Invoice Address -->
  <div class="form-group label-floating is-empty">
              <label for="invoice_address" class="control-label">Invoice Address*</label>
              <input id="invoice_address" type="text" name="invoice_address" class="form-control" value="" required>
            <span class="material-input"></span>
  </div>




    <hr />
    <div style="display:none;">


    <h4>Xero Link</h4>

    <div class="well">
      <strong>Setting up the Xero Link</strong><br />
      <ol class="xero_link_list">
        <li>Login to Xero <a href="https://developer.xero.com/myapps"  target="_blank">developer center</a></li>
        <li>Click "New App" link</li>
        <li>Enter your App name (Staff Box Rota), company url (<?echo $fullurl;?>), privacy policy url (<?echo $fullurl;?>privacy-policy.php).</li>
        <li>Enter the redirect URI (<?echo $fullurl;?>assets/app_ajax/xero/callback.php)</li>
        <li>Agree to terms and condition and click "Create App".</li>
        <li>Click "Generate a secret" button.</li>
        <li>Copy your client id and client secret and save for use later.</li>
        <li>Click the "Save" button. Your secret is now hidden.</li>
      </ol>
      <small>Please note if you have already created an "New App" before use the same information as then. Also please keep a secure record of the information needed below as it might also been needed in the future.</small>
    </div>

  <div class="form-group label-floating is-empty">
              <label for="client_id" class="control-label">Xero - Client ID*</label>
              <input id="client_id" type="text" name="client_id" class="form-control" value="">
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="client_secret" class="control-label">Xero - Client Secret*</label>
              <input id="client_secret" type="text" name="client_secret" class="form-control" value="">
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="account_number" class="control-label">Xero - Account Number (Chart of Accounts Code)*</label>
              <input id="account_number" type="text" name="account_number" class="form-control" value="">
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating">
              <label for="payment_terms" class="control-label">Xero - Payment Terms*</label>
              <input id="payment_terms" type="nunmber" min="1" name="payment_terms" class="form-control" value="30">
            <span class="material-input"></span>
  </div>
  <div class="form-group label-floating is-empty">
              <label for="payee_id" class="control-label">Xero - Payee Xero ID (The ID of who is going to be invoiced)*</label>
              <input id="payee_id" type="text" name="payee_id" class="form-control" value="">
            <span class="material-input"></span>
  </div>

</div>

</form>
<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_add_service">Save</button>
