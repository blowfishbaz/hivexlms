<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$id = $_POST['id'];
$type = $_POST['type'];

?>

<h3>Are you sure you want to delete this Shift?</h3>

<?if($type == 'bid'){?><strong>Warning - This will delete the New Rota Shift Bid as a whole.</strong><?}?>

<div class="clearfix">

</div>
<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Cancel</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="confirm-save-delete-calendar" data-id="<?echo $id;?>" data-type="<?echo $type;?>">Confirm</button>
