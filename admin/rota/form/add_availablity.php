<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    tab_tasks.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

$today_w = date('W', strtotime('this week'));
$week1 = $today_w + 1;
$week2 = $today_w + 2;
$week3 = $today_w + 3;

//How am I going to do this? I don't know. I need to display the start day of the week...
 ?>

 <h3>Add Availablity</h3>

 <form id="set_available_dates_form">

    <div class="form-group">
     <label class="control-label">Week Commencing*</label>
     <select name="week_comm_date" class="form-control" id="week_comm_date" required>
        <option value="" selected disabled>Select</option>
        <option value="<?echo date('W', strtotime('this week'));?>" data-id="<?echo date('Y',strtotime('this week'));?>"><?echo date('D dS M', strtotime('this week'));?></option>
        <option value="<?echo $week1;?>" data-id="<?echo date('Y',strtotime('Monday'));?>"><?echo date('D dS M', strtotime('Monday'));?></option>
        <option value="<?echo $week2;?>" data-id="<?echo date('Y',strtotime('+1 week Monday'));?>"><?echo date('D dS M', strtotime('+1 week Monday'));?></option>
        <option value="<?echo $week3;?>" data-id="<?echo date('Y',strtotime('+2 week Monday'));?>"><?echo date('D dS M', strtotime('+2 week Monday'));?></option>
     </select>
    </div>

    <input type="hidden" name="account_id" value="<?echo $id;?>" />
    <input type="hidden" id="avail_date_year" name="year" value="" />


 <table id="avail_table">

 </table>

 </form>



 <button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
 <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_availablility">Save</button>
