<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['id'];

?>


<h3>Upload New Profile Picture</h3>

<form id="profile_pic">
  <div class="panel-body">
    <input type="hidden" name="account_id" id="account_id"  value="<?echo encrypt($id);?>" />
  </div>
  <div class="col-lg-12">
    <div id="image-editor">
      <div class="cropit-image-preview" style="width:64px; height: 64px; margin: auto;"></div>
      <div class="btns-wrapper">
        <div class="btns">
         <div class="image-size-label">
         <input type="range" class="cropit-image-zoom-input" id="rangeinput">
         </div>
         <div class="text-center">
           <input accept="image/*" class="cropit-image-input custom" type="file">
           <div class="btn select-image-btn btn-primary">
           <span class="icon icon-image"></span>Upload new image
           </div>
           <div class="btn download-btn export btn-warning">
           <span class="icon icon-box-add"></span>Save image
           </div>
         </div>
        </div>
      </div>
      <div class="loading" id="resize_loader" style="display:none; margin:0 auto; padding:10px;">
      <img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" style="margin:0 auto; display:table;" /><br>
      </div>
    </div>
  </div>
<div class="clearfix">
</div>
</form>

<script>
$(function() {
  $('#image-editor').cropit({
      //allowDragNDrop: true,
      exportZoom: 1,
      initialZoom:'middle',
      onImageLoading:function() {
   $("#resize_loader").show();
   console.log("loading");
   },
  onImageLoaded:function() {
   $("#resize_loader").hide();
   },
    onImageError: function() {
   alert("Please use an image that's at least " + $(".cropit-image-preview").outerWidth() * 2 + "px in width and " + $(".cropit-image-preview").outerHeight() * 2 + "px in height.")
   $("#resize_loader").hide();
   }
  });
     $( "body" ).on( "click", ".select-image-btn", function() {

$('.cropit-image-input').click();

});
  var _rotation = 0;

  var $editor = $('#image-editor');
$( "body" ).on( "click", ".export", function() {
 var user_id = $('#account_id').val();


   // Get cropping information
    var imgSrc = $editor.cropit('imageSrc');
    var offset = $editor.cropit('offset');
    var zoom = $editor.cropit('zoom');
    var previewSize = $editor.cropit('previewSize');
    var exportZoom = $editor.cropit('exportZoom');
    var img = new Image();
    img.src = imgSrc;
    // Draw image in original size on a canvas
    var originalCanvas = document.createElement('canvas');
    originalCanvas.width = previewSize.width / zoom;
    originalCanvas.height = previewSize.height / zoom;
    var ctx = originalCanvas.getContext('2d');

    ctx.drawImage(img, offset.x / zoom, offset.y / zoom);

    //ctx.rotate((Math.PI/180)*_rotation);
    // Use pica to resize image and paint on destination canvas
    var zoomedCanvas = document.createElement('canvas');
    zoomedCanvas.width = previewSize.width * exportZoom;
    zoomedCanvas.height = previewSize.height * exportZoom;
    pica.resizeCanvas(originalCanvas, zoomedCanvas, {
      // Pica options, see https://github.com/nodeca/pica
    }, function(err) {
      if (err) { return console.log(err); }
      // Resizing completed
      // Read resized image data
      var picaImageData = zoomedCanvas.toDataURL();

      console.log(encodeURIComponent(picaImageData));
      //Upload Image
      id = user_id; // Id maybe needed.
      console.log(id);

      if(!$.trim(id).length){
        Messenger().post({
          message: 'Please select a users account',
          type: 'error',
          showCloseButton: false
      });
    }else{
      $("#resize_loader").show();
          $.ajax({
      type: "POST",
      url: "../../../admin/rota/ajax/upload_profile_pic.php",
      data: {id: id, img: encodeURIComponent(picaImageData),other:1},
      contentType: "application/x-www-form-urlencoded;charset=UTF-8",
      success: function(msg){
      console.log(msg);

      //location.href = "editprofile.php";
       $('.modal').modal('hide');
      var msg;

               msg = Messenger().post({
                 message: 'The Users Profile Pic has been changed.',
                 type: 'info',
                 actions: {
                   cancel: {
                     label: 'Refresh Page',
                     action: function() {
                       return msg.update({
                         message: 'Refreshing',
                         type: 'success',
                         actions: location.reload()
                       });
                     }
                   }
                 }
               });

                  setTimeout(function(){

                        location.reload();

                     },3000);


      }
      });
    }




    });
    });
        /*
#####################################

End of Proper Uploader

#####################################
*/
});
</script>
