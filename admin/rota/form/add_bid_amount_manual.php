<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->query("select * from ws_service where id = ?");
$db->bind(1,$id);
$service = $db->single();

$db->query("select ws_service_roles.id, ws_service_roles.hourly_rate, ws_roles.title, ws_service_roles.job_role from ws_service_roles left join ws_roles on ws_roles.id = ws_service_roles.job_role where ws_service_roles.pid = ? and ws_service_roles.status = 1");
$db->bind(1,$id);
$roles = $db->ResultSet();


//This will be a drop down of all the people that can do this role for this location.


?>
<div class="clearfix"></div>
<div class="row">
<?foreach ($roles as $r) {?>



<?

$db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id, ws_accounts.first_name, ws_accounts.surname
from ws_accounts_locations
left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
left join ws_accounts on ws_accounts.id = ws_accounts_roles.account_id
where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1 and ws_accounts.status = 1 order by ws_accounts.first_name asc");
$db->bind(1,$service['location']);
$db->bind(2,$r['job_role']);
$list = $db->ResultSet();

?>



  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
      <label class="control-label"><?echo $r['title'];?> - Staff<?echo $service['location'].' '.$r['job_role'];?></label>
      <select name="job_role_<?echo $r['id'];?>[]" class="form-control selectpicker" data-live-search="true" id="job_role_<?echo $r['id'];?>" required multiple>
        <?foreach ($list as $l) {?>
          <option value="<?echo $l['account_id'];?>"><?echo $l['first_name'].' '.$l['surname'];?></option>
        <?}?>
    </select>
  </div>

  </div>
<?}?>
</div>
<div class="clearfix"></div>
