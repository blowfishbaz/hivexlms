<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 ?>

 <h3>Delete Bid</h3>
 <p>
   Are you sure you want to delete this bid?
 </p>

 <button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-danger pull-right" id="confirm_delete_bid" data-id="<?echo $id;?>">Confirm</button>
