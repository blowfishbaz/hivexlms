<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

?>

<h3>Add New User</h3>

<form id="create_new_user">

<div class="col-md-6 col-sm-6">
<div class="form-group label-floating is-empty">
<label for="name" class="control-label">Name*</label>
<input type="text" class="form-control" id="name" name="name" required="yes" value=""><span class="material-input"></span></div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group label-floating is-empty">
<label for="email" class="control-label">Email*</label>
<input type="email" class="form-control" id="email" name="email" required="yes" value=""><span class="material-input"></span></div>
</div>

<div class="clearfix"></div>

<?
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));
}
?>

<div class="col-md-6 col-sm-6">
<div class="form-group label-floating">
<label for="email" class="control-label">Account Type*</label>
<select class="form-control chosen-select" id="type" name="type" required="yes">
<?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?><option value="admin" selected>Admin</option><?}?>
<option value="subadmin">Sub Admin</option>
</select>
</div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group" style="margin-top:0px;">
<label for="location" class="control-label" style="margin-top:13px;">Account Location</label>
<select class="form-control chosen-select" id="location_multiple" name="location[]" multiple <?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?>disabled<?}?>>
<?if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?>
  <option value="Wirral">Wirral</option>
  <option value="Sefton">Sefton</option>
  <option value="Liverpool">Liverpool</option>
<?}else{?>
  <?if(in_array('Wirral',$my_areas)){?><option value="Wirral">Wirral</option><?}?>
  <?if(in_array('Sefton',$my_areas)){?><option value="Sefton">Sefton</option><?}?>
  <?if(in_array('Liverpool',$my_areas)){?><option value="Liverpool">Liverpool</option><?}?>
<?}?>

</select>
</div>
</div>

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12">
  <!-- PCN & Practice -->
  <div class="form-group pcn-practice-div" style="display:none;">
    <label class="control-label">PCN &amp; Practice*</label>
    <select name="pcn_practice" class="form-control" id="pcn_practice" multiple>


    </select>
  </div>
</div>
<div class="clearfix"></div>

<div class="col-md-6 col-sm-6">
<div class="form-group label-floating is-empty">
<label for="newpassword" class="control-label">New Password*</label>
<input type="password" class="form-control" id="newpassword" name="newpassword" value="" required="yes"><span class="material-input"></span>
</div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group label-floating is-empty">
<label for="confirmnewpassword" class="control-label">Confirm Password*</label>
<input type="password" class="form-control" id="confirmnewpassword" name="confirmnewpassword" value="" required="yes"><span class="material-input"></span>
</div>
</div>

</form>

<!--

Name
email

account type
locations
password
confirm password

 -->

 <div class="clearfix"></div>

 <button type="button" class="btn btn-sm btn-warning btn-raised" data-dismiss="modal">Close</button>

 <button type="button" class="btn btn-sm btn-success btn-raised pull-right" id="save_new_account">Save</button>
