<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $locations = $_POST['location'];

  $subadmin_query = sub_admin_query('id','and');


?>

<label class="control-label">PCN &amp; Practice*</label>
<select name="pcn_practice[]" class="form-control" id="pcn_practice" multiple>

<?foreach ($locations as $loc) {?>
  <?
    $db->query("select * from ws_pcn where location_id = ? and status = 1 $subadmin_query");
    $db->bind(1,$loc);
    $pcn = $db->ResultSet();

    $db->query("select * from ws_practice where pcn_location = '' and location_id = ? and status = 1 $subadmin_query");
    $db->bind(1,$loc);
    $stand_alone_practices = $db->ResultSet();

    if(empty($pcn)){
      $db->query("select * from ws_practice where location_id = ? and status = 1 $subadmin_query");
      $db->bind(1,$loc);
      $stand_alone_practices = $db->ResultSet();
    }
  ?>

  <?foreach ($pcn as $p) {?>
    <option value="<?echo $p['id'];?>"><?echo $p['pcn_title'];?></option>
    <?
    $db->query("select * from ws_practice where pcn_location = ? and location_id = ? and status = 1 $subadmin_query");
    $db->bind(1,$p['id']);
    $db->bind(2,$loc);
    $practice = $db->ResultSet();
    ?>
    <?foreach ($practice as $prac) {?>
      <option value="<?echo $prac['id'];?>"><?echo $prac['title'];?></option>
    <?}?>
    <option disabled>----------------</option>
  <?}?>
  <?foreach ($stand_alone_practices as $sap) {?>
    <option value="<?echo $sap['id'];?>"><?echo $sap['title'];?></option>
  <?}?>
<?}?>

</select>
