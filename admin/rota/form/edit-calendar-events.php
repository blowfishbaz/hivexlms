<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$id = $_POST['id'];
$type = $_POST['type'];

if($type == 'bid'){
  $db->query("select * from ws_rota_bid_amount where id = ?");
  $db->bind(1,$id);
  $rota_amount = $db->single();

  $db->query("select * from ws_rota_bid where id = ?");
  $db->bind(1,$rota_amount['pid']);
  $rota = $db->single();

  $db->query("select * from ws_service_roles where id = ?");
  $db->bind(1,$rota_amount['service_role_id']);
  $service_role = $db->single();

  $db->query("select * from ws_service where id = ?");
  $db->bind(1,$rota['service_id']);
  $service = $db->single();

  if($rota['completed'] == 3){
    $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id, ws_accounts.status, accounts.name
    from ws_accounts_locations
    left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
    left join ws_accounts on ws_accounts.id = ws_accounts_locations.account_id
    left join accounts on accounts.id = ws_accounts_locations.account_id
    where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1 and ws_accounts.status = 1");
    $db->bind(1,$service['location']);
    $db->bind(2,$service_role['job_role']);
    $accounts = $db->ResultSet();
  }else{
    $db->query("select accounts.name, accounts.id from ws_rota_bid_request left join accounts on accounts.id = ws_rota_bid_request.account_id where ws_rota_bid_request.pid = ? and ws_rota_bid_request.bid_amount_pid = ? and ws_rota_bid_request.accepted_admin = 0 and ws_rota_bid_request.status = 1");
    $db->bind(1,$rota['id']);
    $db->bind(2,$id);
    $accounts = $db->resultset();
  }



  $ra_amount = 1;

  if($rota_amount['amount'] != 0 ){
      $ra_amount = $rota_amount['amount'];
  }

  $db->query("select count(id) as counter from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and accepted_admin = 1 AND accepted_user = 1");
  $db->bind(1,$rota['id']);
  $db->bind(2,$id);
  $requst_accepts = $db->single();


}else if($type == 'rota_oustanding' || $type == 'rota_complete'){
  $db->query("select * from hx_rota where id = ?");
  $db->bind(1,$id);
  $rota = $db->single();

  $db->query("select accounts.name, accounts.id from ws_rota_bid_request left join accounts on accounts.id = ws_rota_bid_request.account_id where ws_rota_bid_request.pid = ? and ws_rota_bid_request.status = 1");
  $db->bind(1,$rota['bid_id']);
  $accounts = $db->resultset();

}

?>

<h3>Edit Rota</h3>
<?if($rota['completed'] == 3){?><p style="color:red;"><u>To Manually Assign, you need to Send the Shift out first.</u></p><p> This can be done by clicking View Responses</p><?}?>
<form id="edit-calendar-event-form">
  <input type="hidden" name="id" value="<?echo $id;?>"/>

  <input type="hidden" name="bid_id" value="<?echo $rota['id'];?>"/>
  <input type="hidden" name="type" value="<?echo $type?>"/>


  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
          <label for="start_date" class="control-label">Start Date*</label>
          <input id="start_date" type="date" name="start_date" class="form-control" value="<?echo date('Y-m-d',strtotime($rota['start_date']));?>" placeholder="dd-mm-yyyy" required>
        <span class="material-input"></span>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
          <label for="end_date" class="control-label">End Date*</label>
          <input id="end_date" type="date" name="end_date" class="form-control" value="<?echo date('Y-m-d',strtotime($rota['end_date']));?>" placeholder="dd-mm-yyyy" required>
        <span class="material-input"></span>
      </div>
    </div>
  </div>
  <div class="row">

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
          <label for="start_time" class="control-label">Start Time*</label>
          <input id="start_time" type="time" name="start_time" class="form-control" value="<?echo $rota['start_time'];?>" placeholder="hh:mm" required>
        <span class="material-input"></span>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
          <label for="end_time" class="control-label">End Time*</label>
          <input id="end_time" type="time" name="end_time" class="form-control" value="<?echo $rota['end_time'];?>" placeholder="hh:mm" required>
        <span class="material-input"></span>
      </div>
    </div>

  </div>

  <?if($type == 'bid'){?>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="amount_needed" class="control-label">Amount Needed</label>
            <input id="amount_needed" type="number" name="amount_needed" class="form-control" value="<?echo $rota_amount['amount'];?>" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <!-- <div class="form-group">
            <label for="pay_needed" class="control-label">Pay Amount (£ Per Hour)</label>
            <input id="pay_needed" type="number" name="pay_needed" class="form-control" value="<?echo $service_role['hourly_rate'];?>" required>
          <span class="material-input"></span>
        </div> -->
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="control-label">Assign Role(Oustanding Positions - <?echo $ra_amount - $requst_accepts['counter'];?>)</label>
            <select name="assign_to[]" class="form-control selectpicker" data-live-search="true" id="assign_to" multiple>
              <?foreach ($accounts as $acc) {?>
                <option value="<?echo $acc['id'];?>"><?echo $acc['name'];?></option>
              <?}?>
          </select>
        </div>
      </div>
    </div>

  <?}?>
  <?if($type == 'rota_oustanding' || $type == 'rota_complete'){?>

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="control-label">Assigned To</label>
            <select name="assign_to" class="form-control selectpicker" data-live-search="true" id="assign_to">
              <?foreach ($accounts as $acc) {?>
                <option value="<?echo $acc['id'];?>" <?if($acc['id'] == $rota['member_id']){echo 'selected';}?>><?echo $acc['name'];?></option>
              <?}?>
          </select>
        </div>
      </div>
    </div>

  <?}?>
</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Cancel</button>
<button type="button" class="btn btn-sm btn-raised btn-danger" id="delete-calendar-event" data-id="<?echo $rota['id'];?>" data-type="<?echo $type;?>">Delete</button>


<?if($type == 'rota_oustanding' || $type == 'rota_complete' || $rota['completed'] == 0){?><button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="confirm-edit-calendar">Confirm</button><?}?>
<?if($type == 'bid'){?><a href="<?echo $fullurl;?>admin/rota/admin/view_responses.php?id=<?echo $rota['id'];?>" class="btn btn-sm btn-raised btn-warning pull-right">View Responses</a><?}?>
