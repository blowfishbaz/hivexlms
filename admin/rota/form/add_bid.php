<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and location in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")';

	$subadmin_query_pcn = sub_admin_query('pcn_id');
	$subadmin_query_practice = sub_admin_query('practice_id','or');
  if(!empty($subadmin_query_pcn) && !empty($subadmin_query_practice)){
	   $subadmin_search .= " and($subadmin_query_pcn $subadmin_query_practice)";
   }
}

$db->query("select * from ws_service where status = 1 $subadmin_search");
$services = $db->resultset();

$date = $_GET['date'];

?>

<h3>Add Bid</h3>

<p class="add_bid_message">Please select a service to continue</p>
<form id="add_bid_form">

  <input type="hidden" name="id" value="<?echo createid('bid');?>" />
  <?if(!empty($date)){?>
    <input type="hidden" id="calendar_click_date" value="<?echo $date;?>" />
  <?}?>

  <!-- Select Service -->
  <div class="form-group">
      <label class="control-label">Service*</label>
      <select name="service" class="form-control" data-live-search="true" id="service" required>
        <option value="" selected disabled>Select</option>
        <?foreach ($services as $s) {?>
          <option value="<?echo $s['id'];?>"><?echo $s['title'];?></option>
        <?}?>
    </select>
  </div>

  <div class="service_table_holder"></div>

	<p>

	</p>

  <div class="bid_info_holder" style="display:none;">

			<label class="checkbox-inline">
				<input type="checkbox" value="yes" name="reoccuring_shift" class="reoccuring_shift">
				<span class="checkbox-material"></span> Is this a reoccuring shift?
			</label>
      <div class="clearfix"></div>
      <div class="reoccuring_shift_holder" style="display:none;">
        <div class="clearfix" style="margin-top:10px; "></div>
        <div class="well">
          <p>Please select which days this shift will reoccure on.</p>
          <!-- Need some clarity on this and what is indeed needed. -->

          <div class="radio" style="margin-top: 0;">
                    <label><input type="radio" value="weekly" name="reoccure_freq" class="reoccure_freq" checked><span class="checkbox-material"><span class="check"></span></span>Weekly</label>
          </div>
          <div class="radio" style="margin-top: 0;">
                    <label><input type="radio" value="monthly" name="reoccure_freq" class="reoccure_freq"><span class="checkbox-material"><span class="check"></span></span>Monthly</label>
          </div>

          <table id="reoccur_table">
            <tr>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="monday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Monday </label>
              </td>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="tuesday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Tuesday </label>
              </td>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="wednesday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Wednesday </label>
              </td>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="thursday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Thursday </label>
              </td>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="friday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Friday </label>
              </td>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="saturday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Saturday </label>
              </td>
              <td>
                <label class="checkbox-inline"> <input type="checkbox" value="sunday" name="reoccuring_shift_day[]" class="reoccuring_shift_day"> <span class="checkbox-material"></span> Sunday </label>
              </td>
            </tr>
          </table>


          <div class="form-group reoccur_monthly" style="display:none;">
              <label for="any_date">Date of Month</label>
              <select class="form-control dropdown" id="day_of_month" name="day_of_month">
                  <option>Select : </option>
                  <option value="1">01</option>
                  <option value="2">02</option>
                  <option value="3">03</option>
                  <option value="4">04</option>
                  <option value="5">05</option>
                  <option value="6">06</option>
                  <option value="7">07</option>
                  <option value="8">08</option>
                  <option value="9">09</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
                  <option value="17">17</option>
                  <option value="18">18</option>
                  <option value="19">19</option>
                  <option value="20">20</option>
                  <option value="21">21</option>
                  <option value="22">22</option>
                  <option value="23">23</option>
                  <option value="24">24</option>
                  <option value="25">25</option>
                  <option value="26">26</option>
                  <option value="27">27</option>
                  <option value="28">28</option>
                  <!-- <option value="lastDay">Last Day Of Month</option> -->
              </select>
          </div>

        </div>

      </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="start_date" class="control-label">Start Date*</label>
            <input id="start_date" type="date" name="start_date" class="form-control" value="<?if(!empty($date)){ echo date("Y-m-d", ($date / 1000) ); }?>" placeholder="dd-mm-yyyy" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="end_date" class="control-label">End Date*</label>
            <input id="end_date" type="date" name="end_date" class="form-control" value="<?if(!empty($date)){ echo date("Y-m-d", ($date / 1000) ); }?>" placeholder="dd-mm-yyyy" required>
          <span class="material-input"></span>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="start_time" class="control-label">Start Time*</label>
            <input id="start_time" type="time" name="start_time" class="form-control" value="" placeholder="hh:mm" required>
          <span class="material-input"></span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="end_time" class="control-label">End Time*</label>
            <input id="end_time" type="time" name="end_time" class="form-control" value="" placeholder="hh:mm" required>
          <span class="material-input"></span>
        </div>
      </div>
    </div>

    <!-- Loop through for the hourly rates and how many people are needed for it -->
    <div class="bid_amount_holder"></div>

		<div class="available_bidder_holder"></div>
  </div>
</form>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-sm btn-raised btn-success pull-right" style="display:none;" id="save_add_bid">Save</button>
