<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Customers List';

 $db->query("select * from accounts where status = 1 order by name asc");
 $accounts = $db->resultSet();

if(empty($_POST)){


    }
    else{


}

$PW = generateRandomString(7);

 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    <?
//Base CSS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->

</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-calendar page-title-glyph rota_background" aria-hidden="true"></span><span class="menuText">My Dashboard</span></div>



								<div class="clearfix"></div>
                                <div class="col-lg-12">

                                    <div class="col-lg-4">
                                        <a href="<?echo $fullurl;?>admin/rota/rota_new.php">
                                            <div class="rota_background dashboard_icons main_dash_icon" style="">
                                                <div class="dashboard_icon_inner">
                                                    <span class="glyphicons glyphicons-calendar dashboard-glyphicon-large" aria-hidden="true"></span><br />
                                                    <span class="dashboard_text" style="font-size:25px;">Manage Rota</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="grey_background dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                            <div class="dashboard_icon_inner">
                                                <span class="glyphicons glyphicons-bullhorn dashboard-glyphicon" aria-hidden="true"></span><br />
                                                <span class="dashboard_text">News & Annoucments</span>
                                            </div>
                                        </div>
                                        <div class="grey_background dashboard_icons small_dash_icon" style="">
                                            <div class="dashboard_icon_inner">
                                                <span class="glyphicons glyphicons-user dashboard-glyphicon" aria-hidden="true"></span><br />
                                                <span class="dashboard_text">My Account</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="hr_backgorund dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                            <div class="dashboard_icon_inner">
                                                <span class="dashboard_number">186</span><br /> <span class="dashboard_text">Next Booked Sessions</span>
                                            </div>
                                        </div>
                                        <div class="black_background dashboard_icons small_dash_icon" style="">
                                            <div class="dashboard_icon_inner">
                                            <span class="dashboard_number">32</span> <br /> <span class="dashboard_text">Invoices to Pay</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="lxp_background dashboard_icons small_dash_icon" style="margin-bottom:20px;">
                                            <div class="dashboard_icon_inner">
                                            <span class="dashboard_number">4</span> <br /> <span class="dashboard_text">Sessions Requests</span>
                                            </div>
                                        </div>
                                        <div class="goven_background dashboard_icons small_dash_icon" style="">
                                            <div class="dashboard_icon_inner">
                                            <span class="dashboard_number">17</span> <br /> <span class="dashboard_text">Notifications</span>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            <!-- Data -->




            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>


    </div>


</body>
</html>

<!-- PAGE JS -->



<script>

jQuery(document).ready(function($) {
    var width = $('.main_dash_icon').width();


    console.log(width);

    if(width > 380){
        width = 380;
        $('.main_dash_icon').width(width);
    }

    $('.main_dash_icon').height(width);

    var half_height = (width - 20) / 2;
    //alert(half_height);
    $('.small_dash_icon').height(half_height);


});


</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
