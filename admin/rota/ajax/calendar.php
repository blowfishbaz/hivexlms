<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    contacts/contact_view/calendar/load_calendar_appointments.php
 * @author    Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $location = $_GET['location'];
 $id = $_GET['id'];
 $area = $_GET['area'];
 $dna = $_GET['dna'];

 $id_search = '';
 $dna_search = '';

 if(!empty($id)){
   $id_search = ' and hx_rota.member_id = "'.$id.'" and hx_rota. start_ts > "'.strtotime(date('d-m-Y 23:59:59', strtotime('yesterday'))).'"';
 }

 if(!empty($area)){
   $id_search = ' and hx_rota.location = "'.$area.'"';
 }

 if(empty($dna)){
   $dna_search = ' and dna = 0';
 }else{
   $dna_search = ' and dna = 1';
 }
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
 $subadmin_query_pcn = sub_admin_query('pcn_id');
 $subadmin_query_prac = sub_admin_query('practice_id','OR');
 $subadmin_search = '';
 if(!empty($subadmin_query_pcn) && !empty($subadmin_query_prac)){
   $subadmin_search = " and ($subadmin_query_pcn $subadmin_query_prac)";
 }
}

$pp_search = '';
if(!empty($_GET['pp'])){
  $pp_search = ' and (pcn_id = "'.$_GET['pp'].'" or practice_id = "'.$_GET['pp'].'")';
}


 error_log('********************************************************************************************************************');
  error_log('********************************************************************************************************************');
   error_log('********************************************************************************************************************');
    error_log('********************************************************************************************************************');
   $db->query("select hx_rota.*, ws_accounts.first_name, ws_accounts.surname, hx_rota.id as rota_id from hx_rota left join ws_accounts on ws_accounts.id = hx_rota.member_id where hx_rota.status = 1 and hx_rota.location = '$location' $id_search $dna_search $subadmin_search $pp_search");
   $data = $db->resultset();

  ?>
  [
   <? foreach($data as $d) {

     //Persons Name

     if($_GET['show'] == 1){

        ?>

     {
            "id": "<?echo $d['rota_id'];?>",
           "title": "<? echo $d['first_name'].' '.$d['surname']; ?>",
           "start": "<? echo date('c', $d['start_ts']);?>",
           "end": "<? echo date('c', $d['end_ts']);?>"
     },
   <? }
 } ?>
      {
     "title": "",
     "start": "2150-06-28"
   }
    ]
