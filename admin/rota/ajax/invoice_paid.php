<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/assets/app_ajax/xero/storage.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/assets/app_ajax/xero/hivex_xero.php');

function get_account_number($name){
    //We might not need this, but depending on who set up what we might need this.
        switch ($name) {
            case 'Sales':
                $r =  "200";
                break;
            case 'Other Revenue':
                $r =  "260";
                break;
            case 2:
                $r =  "";
                break;
        }

        return $r;

}



function init_hivex_xero($clientId,$clientSecret,$redirectUri){

				$db = new database;
				$db->query("select * from hx_xero where id = ?");
				$db->bind(1,'1');
				$db_store = $db->single();
				$storage = new StorageClass();
				$storage->setToken(
									$db_store['access_token'],
									$db_store['access_token_expiry'],
									$db_store['tenant_id'],
									$db_store['refresh_token'],
									$db_store['connection_id']
				);



				// ALL methods are demonstrated using this class
				$ex = new ExampleClass();

				$xeroTenantId = (string)$storage->getSession()['tenant_id'];




				// Check if Access Token is expired
				// if so - refresh token
				if ($storage->getHasExpired()) {
					$provider = new \League\OAuth2\Client\Provider\GenericProvider([
					'clientId'                => $clientId,
					'clientSecret'            => $clientSecret,
					'redirectUri'             => $redirectUri,
					'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
					'urlAccessToken'          => 'https://identity.xero.com/connect/token',
					'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
					]);

						$newAccessToken = $provider->getAccessToken('refresh_token', [
								'refresh_token' => $storage->getRefreshToken()
						]);


						// Save my token, expiration and refresh token
							 // Save my token, expiration and refresh token
							$storage->setToken(
												$newAccessToken->getToken(),
												$newAccessToken->getExpires(),
												$xeroTenantId,
												$newAccessToken->getRefreshToken(),
												$newAccessToken->getValues()["id_token"]
							);


							$db->Query("delete from hx_xero where id = ? ");
							$db->bind(1,'1');
							//$db->execute();

							$newid = '1';
							$db->query("insert into hx_xero (id, access_token, access_token_expiry, tenant_id ,refresh_token, connection_id) values (?,?,?,?,?,?)");
							$db->bind(1,$newid);
							$db->bind(2,$_SESSION['oauth2']["token"]);
							$db->bind(3,$_SESSION['oauth2']["expires"]);
							$db->bind(4,$_SESSION['oauth2']["tenant_id"]);
							$db->bind(5,$_SESSION['oauth2']["refresh_token"]);
							$db->bind(6,$_SESSION['oauth2']["id_token"]);
							//$db->execute();

				}


 return $storage;

}

$id = $_POST['id'];
$today_minus_72 = strtotime(" - 3 day");

$db->Query("update ws_rota_bid set invoice_paid = ?, paid_date = ?, paid_by = ? where id = ?");
$db->bind(1,'1');
$db->bind(2,time());
$db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(4,$id);
$db->execute();

$db->query("select * from hx_rota where status = 1 and invoice_paid = 0 and dna = 0 and end_ts < ? and member_id = ?");
$db->bind(1,$today_minus_72);
$db->bind(2,$id);
$rota_info = $db->resultset();

$db->query("select * from ws_accounts where id = ?");
$db->bind(1,$id);
$account = $db->single();

foreach ($rota_info as $ri) {
    $invoice_lines = array();

    $db->query("select * from ws_service where id = ?");
    $db->bind(1,$ri['service_id']);
    $service = $db->single();

    //The service would have the invoice information on it.

    //Client ID
    //Client Secret
    //Redirect URI - I will set that
    //account number

    $db->query("select * from ws_roles where id = ?");
    $db->bind(1,$ri['role']);
    $role = $db->single();

    $db->query("select * from ws_service_roles where pid = ? and job_role = ?");
    $db->bind(1,$ri['service_id']);
    $db->bind(2,$ri['role']);
    $service_role = $db->single();

    $hourly_rate = $service_role['hourly_rate'];
    $time1 = $ri['start_ts'];
    $time2 = $ri['end_ts'];
    $difference = round(abs($time2 - $time1) / 3600,2);

    $running_total = $running_total + ($difference * $hourly_rate);

    $client_id = decrypt($service['client_id']);
    $client_secret = decrypt($service['client_secret']);
    $account_number = decrypt($service['account_number']);
    $payment_term = $service['payment_term'];
    $xero_id = decrypt($service['xero_id']);

    $invoice_type = $service['title'].' - '.$role['title'];
    $quantity = 1;
    $price = $difference * $hourly_rate;
    $account_type = $account_number;

    // echo '*************************************'.'<br />';
    // echo '<b>Serv ID</b> - '.$service['id'].'<br />';
    // echo '<b>Name</b> - '.$account['first_name'].' '.$account['surname'].'<br />';
    // echo '<b>S Title</b> - '.$service['title'].'<br />';
    // echo '<b>R Title</b> - '.$role['title'].'<br />';
    // echo '<b>S Date</b> - '.date('d-m-Y H:i',$ri['start_ts']).'<br />';
    // echo '<b>E Date</b> - '.date('d-m-Y H:i',$ri['end_ts']).'<br />';
    // echo '<b>H Rate</b> - '.$service_role['hourly_rate'].'<br />';
    // echo '<b>T Diff</b> - '.$difference.'<br />';
    // echo '<b>Inv Am</b> - '.$difference * $hourly_rate.'<br /><br />';
    // echo '***** Xero *****<br />';
    // echo '<b>Clie ID</b> - '.$client_id.'<br />';
    // echo '<b>Secret</b> - '.$client_secret.'<br />';
    // echo '<b>Acc Num</b> - '.$account_number.'<br />';
    // echo '<b>P Term</b> - '.$payment_term.'<br />';
    // echo '<b>Xero ID</b> - '.$xero_id.'<br /><br />';

    array_push($invoice_lines, array($invoice_type,$account_type,$quantity,$price));

    //Would we send each line on one invoice, or would be send in bulk...
    //I think this depends on where the Xero information is stored.


		if(empty($client_id) || empty($client_secret) || empty($account_number) || empty($xero_id)){
			//echo '***** Xero Details Empty *****<br />';
		}else{
			//echo '***** Xero Details *****<br />';


    $clientId = decrypt($service['client_id']);
    $clientSecret = decrypt($service['client_secret']);
    $redirectUri = $fullurl.'assets/app_ajax/xero/callback.php';

    putenv("CLIENT_ID=$clientId");
    putenv("CLIENT_SECRET=$clientSecret");
    putenv("REDIRECT_URI=$redirectUri");

    $storage = init_hivex_xero($clientId,$clientSecret,$redirectUri);
    $ex = new ExampleClass();

    $xeroTenantId = (string)$storage->getSession()['tenant_id'];
    $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
    $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
        new GuzzleHttp\Client(),
        $config
    );
    $assetApi = new XeroAPI\XeroPHP\Api\AssetApi(
        new GuzzleHttp\Client(),
        $config
    );

    $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
        new GuzzleHttp\Client(),
        $config
    );

    $projectApi = new XeroAPI\XeroPHP\Api\ProjectApi(
        new GuzzleHttp\Client(),
        $config
    );

    $payrollAuApi = new XeroAPI\XeroPHP\Api\PayrollAuApi(
        new GuzzleHttp\Client(),
        $config
    );

    $ex->hivex_createInvoices($xeroTenantId,$service['payment_term'],$accountingApi,false,decrypt($service['xero_id']),$allLinesArray);

		putenv("CLIENT_ID");
		putenv("CLIENT_SECRET");
		putenv("REDIRECT_URI");
	}

}

echo 'ok';
