<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['id'];

//Account
$db->query("select * from accounts where id = ?");
$db->bind(1,$id);
$user = $db->single();

// if($user['user_status'] == 2){
//   header('Location: /admin/rota/account/view_profile.php?id='.$id.'');
//   exit();
// }

//Account
$db->query("select * from ws_accounts where pid = ? and id = ?");
$db->bind(1,$id);
$db->bind(2,$id);
$user_ws = $db->single();

//Extra Info
$db->query("select * from ws_accounts_extra_info where account_id = ? and status = ?");
$db->bind(1,$id);
$db->bind(2,'1');
$user_info = $db->single();

//Rolls
$db->query("select ws_roles.title from ws_accounts_roles left join ws_roles on ws_roles.id = ws_accounts_roles.role_id where ws_accounts_roles.account_id = ? and ws_accounts_roles.status = ?");
$db->bind(1,$id);
$db->bind(2,'1');
$user_roles = $db->resultset();

//Location
$db->query("select * from ws_accounts_locations where account_id = ? and status = ?");
$db->bind(1,$id);
$db->bind(2,'1');
$user_locations = $db->resultset();

//Uploads - ws_accounts_upload
$db->query("select * from ws_accounts_upload where account_id = ? and status = ?");
$db->bind(1,$id);
$db->bind(2,'1');
$uploads = $db->resultset();



$document_list = get_document_list($id);



?>

<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
}

.gallery_images {
height: 200px;
width: 200px;
background-size: cover;
background-position: center;
}

    .install-gallery {
         padding-bottom: 10px;
    }
    .install-gallery > ul {
      margin-bottom:0;
    }
    .install-gallery > ul > li {
         float: left;
         margin-top: 15px;
         margin-left: 20px;
         width: 200px;
    }

    .install-gallery > ul > li a {
      border: 3px solid #000;
      border-radius: 3px;
      display: block;
      overflow: hidden;
      position: relative;
      float: left;
    }
    .install-gallery > ul > li > a >.gallery_images:hover {
      -webkit-transform: scale(1.2);
      transform: scale(1.2);
      cursor: pointer;
      transition: 0.3s;
    }
</style>

<h3>Account Approval</h3>

<table>
  <tr>
    <th>Name</th>
    <td><?echo $user_ws['first_name'].' '.$user_ws['surname'];?></td>
  </tr>
  <tr>
    <th>Email</th>
    <td><?echo $user_ws['email'];?></td>
  </tr>
  <tr>
    <th>Telephone</th>
    <td><?echo $user_info['telephone'];?></td>
  </tr>
  <tr>
    <th>Mobile</th>
    <td><?echo $user_info['mobile_number'];?></td>
  </tr>

  <tr>
    <th>Location(s)</th>
    <td>
      <?$loc_count = count($user_locations); $loc_counter = 1;?>
      <?foreach($user_locations as $loc){?>
        <?
          echo $loc['location_id'];
          if($loc_count > 1){if($loc_counter != $loc_count){echo ', ';}$loc_counter++;}
        ?>
      <?}?>
    </td>
  </tr>

  <tr>
    <th>Roll(s)</th>
    <td>
      <?$role_count = count($user_roles); $role_counter = 1;?>
      <?foreach($user_roles as $role){?>
        <?
          echo $role['title'];
          if($role_count > 1){if($role_counter != $role_count){echo ', ';}$role_counter++;}
        ?>
      <?}?>
    </td>
  </tr>
</table>

<h4>Documents</h4>

<?foreach ($document_list as $dl) {?>
  <ul class="list-group">
    <li class="list-group-item">
      <?echo document_type_full_name($dl);?>
    </li>
    <?
      $db->query("select * from ws_accounts_upload where account_id = ? and status = ? and type = ?");
      $db->bind(1,$id);
      $db->bind(2,'1');
      $db->bind(3,$dl);
      $uploads_per = $db->resultset();
    ?>
    <?foreach ($uploads_per as $up) {?>
      <li class="list-group-item">
        <a href="<?echo $fullurl.$up['path'].$up['name'];?>" download><?echo $up['name'];?></a>
      </li>
    <?}?>
  </ul>
<?}?>

<!-- <ul class="list-group">


<?foreach ($uploads as $up) {?>

  <li class="list-group-item">
    <a href="<?echo $fullurl.$up['path'].$up['name'];?>" download><?echo $up['name'];?></a>
  </li>
<?}?>

</ul> -->



<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="approval_rota_account" data-id="<?echo $id;?>">Approve</button>
<button type="button" class="btn btn-sm btn-raised btn-danger pull-right" id="decline_rota_account" data-id="<?echo $id;?>">Decline</button>
