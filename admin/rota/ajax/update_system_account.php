<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['id'];
$name = $_POST['name'];
$email = $_POST['email'];

$account_type = $_POST['type'];
$account_location = $_POST['location'];
$pcn_practice = $_POST['pcn_practice'];

$db = new database;
$db->Query("update accounts set name = ?, email = ?, username = ?, account_type = ? where id = ? ");
$db->bind(1,$name);
$db->bind(2,$email);
$db->bind(3,encrypt($email));
$db->bind(4,$account_type);
$db->bind(5,$id);
$db->execute();


if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){
  $db = new database;
  $db->Query("update accounts_service_area set status = 0 where pid = ? and status = 1");
  $db->bind(1,$id);
  $db->execute();

  $db = new database;
  $db->Query("update accounts_service_area set status = 0 where pid = ? and status = 1");
  $db->bind(1,$id);
  $db->execute();

  $db = new database;
  $db->Query("update accounts_sub_pcn set status = 0 where account_id = ? and status = 1");
  $db->bind(1,$id);
  $db->execute();

  $db = new database;
  $db->Query("update accounts_sub_practice set status = 0 where account_id = ? and status = 1");
  $db->bind(1,$id);
  $db->execute();




  foreach ($_POST['location'] as $key => $location) {
    $db = new database;
    $db->Query("INSERT INTO accounts_service_area (id, pid, status, area) values (?,?,?,?)");
    $db->bind(1,createid('acc_area'));
    $db->bind(2,$id);
    $db->bind(3,1);
    $db->bind(4,$location);
    $db->execute();
  }

  foreach ($_POST['pcn_practice'] as $key => $pcnpract) {
    if(id_checker($pcnpract, 'prac')){
      $db = new database;
      $db->Query("INSERT INTO accounts_sub_practice (id, account_id, practice_id) values (?,?,?)");
      $db->bind(1,createid('s_prac'));
      $db->bind(2,$id);
      $db->bind(3,$pcnpract);
      $db->execute();
    }else{
      $db = new database;
      $db->Query("INSERT INTO accounts_sub_pcn (id, account_id, pcn_id) values (?,?,?)");
      $db->bind(1,createid('s_pcn'));
      $db->bind(2,$id);
      $db->bind(3,$pcnpract);
      $db->execute();
    }
  }

}

echo 'ok';
