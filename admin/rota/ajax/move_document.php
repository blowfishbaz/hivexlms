<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['id'];

 $document_list = get_document_list($_POST['user']);

?>

<h3>Move Document</h3>

<form id="document_move_form">
  <input type="hidden" name="id" value="<?echo $id;?>" />

  <div class="form-group">
    <label class="control-label pull-left" for="new_doc_type">Move To: </label>
    <select class="form-control chosen-select" id="new_doc_type" name="new_doc_type" required>
      <option value="" selected="" disabled="">Select</option>
      <?foreach ($document_list as $dl) {?>
        <option value="<?echo $dl;?>"><?echo document_type_full_name($dl);?></option>
      <?}?>
    </select>
  </div>

</form>


<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Cancel</button>
<button type="button" class="btn btn-sm btn-raised btn-danger pull-right" id="confirm_move_document" data-id="<?echo $id;?>">Move</button>
