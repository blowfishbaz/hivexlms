<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $pid = $_POST['pid'];
 $type = $_POST['type'];

 $now = time();

 if($type == 'accept'){
   $db->Query("update ws_rota_bid_request set accepted_user = 1, choice_date = ? where id = ?");
   $db->bind(1,$now);
   $db->bind(2,$pid);
   $db->execute();

   //Get the shift and notify the admin who added it to send a notification.

   $db->query("select * from ws_rota_bid_request where id = ?");
   $db->bind(1,$pid);
   $rota_bid_request = $db->single();

   $db->query("select * from ws_rota_bid where id = ?");
   $db->bind(1,$rota_bid_request['pid']);
   $rota_bid = $db->single();

   $db->query("select * from accounts where id = ?");
   $db->bind(1,$rota_bid_request['account_id']);
   $account = $db->single();

   $db->query("select * from ws_service where id = ?");
   $db->bind(1,$rota_bid_request['service_id']);
   $service = $db->single();

   $db->query("select * from ws_roles where id = ?");
   $db->bind(1,$rota_bid_request['service_role_id']);
   $role = $db->single();

   $notification = ''.$account['name'].', has accepted the shift - '.$service['title'].', on '.$rota_bid['start_date'].'.';



   //Who this is going to send to
   //----------------------------------------
   //If practice sent this out then use them

   //If PCN has sent them out use them

   //If One Wirral sent this out then use admin

   if(empty($rota_bid['pcn_id']) && empty($rota_bid['practice_id'])){
     //One Wirral
     $db->query("select * from accounts where account_type = ?");
     $db->bind(1,'admin');
     $admin_accounts = $db->resultset();

     foreach ($admin_accounts as $aa) {
       $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status,pid) values (?,?,?,?,?,?,?,?,?)");
       $db->bind(1,createid('not'));
       $db->bind(2,$aa['id']);
       $db->bind(3,$rota_bid_request['account_id']);
       $db->bind(4,time());
       $db->bind(5,'Shift Bid Accepted');
       $db->bind(6,$notification);
       $db->bind(7,'user_shift_response');
       $db->bind(8,'1');
       $db->bind(9,$rota_bid['id']);
       $db->execute();
     }

   }else if(!empty($rota_bid['pcn_id']) && empty($rota_bid['practice_id'])){
     //PCN
     $db->query("select distinct accounts.*
                  from accounts
                  left join accounts_sub_pcn on accounts_sub_pcn.account_id = accounts.id and accounts_sub_pcn.status = 1
                  where accounts.account_type = ? and accounts_sub_pcn.pcn_id = ?");
     $db->bind(1,'subadmin');
     $db->bind(2,$rota_bid['pcn_id']);
     $admin_accounts = $db->resultset();

     foreach ($admin_accounts as $aa) {
       $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status,pid) values (?,?,?,?,?,?,?,?,?)");
       $db->bind(1,createid('not'));
       $db->bind(2,$aa['id']);
       $db->bind(3,$rota_bid_request['account_id']);
       $db->bind(4,time());
       $db->bind(5,'Shift Bid Accepted');
       $db->bind(6,$notification);
       $db->bind(7,'user_shift_response');
       $db->bind(8,'1');
       $db->bind(9,$rota_bid['id']);
       $db->execute();
     }

   }else if(empty($rota_bid['pcn_id']) && !empty($rota_bid['practice_id'])){
     //Practice
     $db->query("select distinct accounts.*
                  from accounts
                  left join accounts_sub_practice on accounts_sub_practice.account_id = accounts.id and accounts_sub_practice.status = 1
                  where accounts.account_type = ? and accounts_sub_practice.practice_id = ?");
     $db->bind(1,'subadmin');
     $db->bind(2,$rota_bid['practice_id']);
     $admin_accounts = $db->resultset();

     foreach ($admin_accounts as $aa) {
       $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status,pid) values (?,?,?,?,?,?,?,?,?)");
       $db->bind(1,createid('not'));
       $db->bind(2,$aa['id']);
       $db->bind(3,$rota_bid_request['account_id']);
       $db->bind(4,time());
       $db->bind(5,'Shift Bid Accepted');
       $db->bind(6,$notification);
       $db->bind(7,'user_shift_response');
       $db->bind(8,'1');
       $db->bind(9,$rota_bid['id']);
       $db->execute();
     }
   }else{
     $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status,pid) values (?,?,?,?,?,?,?,?,?)");
     $db->bind(1,createid('not'));
     $db->bind(2,$rota_bid['created_by']);
     $db->bind(3,$rota_bid_request['account_id']);
     $db->bind(4,time());
     $db->bind(5,'Shift Bid Accepted');
     $db->bind(6,$notification);
     $db->bind(7,'user_shift_response');
     $db->bind(8,'1');
     $db->bind(9,$rota_bid['id']);
     $db->execute();
   }



 }else if($type == 'decline'){
   $db->Query("update ws_rota_bid_request set accepted_user = 2, choice_date = ? where id = ?");
   $db->bind(1,$now);
   $db->bind(2,$pid);
   $db->execute();
 }



 echo 'ok';
