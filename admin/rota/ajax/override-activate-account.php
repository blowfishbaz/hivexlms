
<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];

$db->Query("update accounts set user_status = 1 where id = ? ");
$db->bind(1,$id);
$db->execute();

$db->Query("update ws_accounts set status = 1 where id = ? and pid = ?");
$db->bind(1,$id);
$db->bind(2,$id);
$db->execute();

$db->query("select * from accounts where id = ? ");
$db->bind(1,$id);
$user = $db->single();

echo 'ok';
