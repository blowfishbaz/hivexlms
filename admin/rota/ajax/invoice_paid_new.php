<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_POST['id'];

$db->Query("update hx_rota set invoice_paid = ?, invoice_paid_date = ?, invoice_paid_by = ? where member_id = ? and status = 1 and invoice_paid = 0 and dna = 0");
$db->bind(1,'1');
$db->bind(2,time());
$db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(4,$id);
$db->execute();

echo 'ok';
