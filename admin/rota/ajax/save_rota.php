<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');



$thisSaveId = createid('sv');


      foreach ($_POST['start_time'] as $key => $value) {
                  if($value!=''){


                        $thisId = createid('shift');
                        $thisHome = $_POST['home_id'][$key];
                        $thisMemberId = $_POST['member_id'][$key];

                        $thisStartDate = $_POST['start_date'][$key];
                        $thisStartTime = $_POST['start_time'][$key];

                        $thisEndDate = $_POST['end_date'][$key];
                        $thisEndTime = $_POST['end_time'][$key];

                        $thisStart_ts = date('U', strtotime($thisStartDate.' '.$thisStartTime));
                        $thisEnd_ts = date('U', strtotime($thisEndDate.' '.$thisEndTime));

                        $thisWeekNum = $_POST['week_number'][$key];
                        $thisYear =  date('Y', strtotime($thisStartDate));

                        if($thisStartDate==$thisEndDate){
                          $thisType='in_day';
                        }
                        else{
                          $thisType='span_day';
                        }


                        $db->query("insert into hx_rota (save_id,id,member_id,home_id,start_date,start_time,end_date,end_time,start_ts,end_ts,week_num,year,type) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                        $db->bind(1, $thisSaveId);
                        $db->bind(2, $thisId);
                        $db->bind(3, $thisMemberId);
                        $db->bind(4, $thisHome);
                        $db->bind(5, $thisStartDate);
                        $db->bind(6, $thisStartTime);
                        $db->bind(7, $thisEndDate);
                        $db->bind(8, $thisEndTime);
                        $db->bind(9, $thisStart_ts);
                        $db->bind(10, $thisEnd_ts);
                        $db->bind(11, $thisWeekNum);
                        $db->bind(12, $thisYear);
                        $db->bind(13, $thisType);
                        $db->execute();


                  }

      }





?>
