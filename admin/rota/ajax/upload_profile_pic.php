<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    assets/app_ajax/profile/profile_view/upload_profile.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$profileID = decrypt($_POST['id']);


			//saveImage(utf8_decode($_POST['img']));
			$base64img = urldecode($_POST['img']);
			$base64img = str_replace('data:image/png;base64,', '', $base64img);
			$data = base64_decode($base64img);
			$file = $fullpath."uploads/temp/" . uniqid();
			$success = file_put_contents($file, $data);

			$sha1file = sha1_file($file);
			rename($file, $fullpath.'uploads/profiles/'.$sha1file.'.png');



		//Query for old

		$db->Query('select profilepic from accounts where id = ?');
		$db->bind(1, $profileID);
		$dbresult = $db->single();
		$oldDigest = $dbresult["profilepic"];

		//update with new digest
	 	$db->Query('update accounts set profilepic = ? where id = ?');
		$db->bind(1, $sha1file);
		$db->bind(2, $profileID);
		$db->execute();

    // if(isset($_POST['other']) && $_POST['other'] == 1) {
		//
    // } else {
    //   $_SESSION['SESS_PROFILEPIC'] = $sha1file;
    // }


		usleep(1000000);
		//search for old digest again.

		$db->Query('select * from accounts where profilepic = ?');
		$db->bind(1,$oldDigest);
		$data = $db->single();
		$countRes = $db->rowcount();


			if ($countRes == 0) {
				unlink($fullpath.'/uploads/profiles/'.$oldDigest.'.png');
			}


echo($data['id'].','.$oldDigest.','.$countRes.','.$sha1file);








?>
