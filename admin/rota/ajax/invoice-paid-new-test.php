<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/assets/app_ajax/xero/storage.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/assets/app_ajax/xero/hivex_xero.php');

function init_hivex_xero($clientId,$clientSecret,$redirectUri){

				// $db = new database;
				// $db->query("select * from hx_xero where id = ?");
				// $db->bind(1,'1');
				// //$db_store = $db->single();
				// $storage = new StorageClass();
				// $storage->setToken(
				// 					$db_store['access_token'],
				// 					$db_store['access_token_expiry'],
				// 					$db_store['tenant_id'],
				// 					$db_store['refresh_token'],
				// 					$db_store['connection_id']
				// );



				// ALL methods are demonstrated using this class
				$ex = new ExampleClass();

				$xeroTenantId = (string)$storage->getSession()['tenant_id'];




				// Check if Access Token is expired
				// if so - refresh token
				if ($storage->getHasExpired()) {
					$provider = new \League\OAuth2\Client\Provider\GenericProvider([
					'clientId'                => $clientId,
					'clientSecret'            => $clientSecret,
					'redirectUri'             => $redirectUri,
					'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
					'urlAccessToken'          => 'https://identity.xero.com/connect/token',
					'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
					]);

						$newAccessToken = $provider->getAccessToken('refresh_token', [
								'refresh_token' => $storage->getRefreshToken()
						]);


						// Save my token, expiration and refresh token
							 // Save my token, expiration and refresh token
							$storage->setToken(
												$newAccessToken->getToken(),
												$newAccessToken->getExpires(),
												$xeroTenantId,
												$newAccessToken->getRefreshToken(),
												$newAccessToken->getValues()["id_token"]
							);


							$db->Query("delete from hx_xero where id = ? ");
							$db->bind(1,'1');
							//$db->execute();

							$newid = '1';
							$db->query("insert into hx_xero (id, access_token, access_token_expiry, tenant_id ,refresh_token, connection_id) values (?,?,?,?,?,?)");
							$db->bind(1,$newid);
							$db->bind(2,$_SESSION['oauth2']["token"]);
							$db->bind(3,$_SESSION['oauth2']["expires"]);
							$db->bind(4,$_SESSION['oauth2']["tenant_id"]);
							$db->bind(5,$_SESSION['oauth2']["refresh_token"]);
							$db->bind(6,$_SESSION['oauth2']["id_token"]);
							//$db->execute();

				}


 return $storage;

}

$invoice_lines = array();
$invoice_type = $service['title'].' - '.$role['title'];
$quantity = 1;
$price = $difference * $hourly_rate;
$account_type = $account_number;
array_push($invoice_lines, array($invoice_type,$account_type,$quantity,$price));

// $clientId = decrypt('089834e7f332cbb9f00920439a176cdbda5e11e32fc9a2316f9ac3d837bab810');
// $clientSecret = decrypt('36d995e444ce60069f8904f93ea477fc3d0785316057af53c7a25f761ccb4be17920c55900e8b68333200bf1b78c90c0');
// $redirectUri = $fullurl.'assets/app_ajax/xero/callback.php';
//
// putenv("CLIENT_ID=$clientId");
// putenv("CLIENT_SECRET=$clientSecret");
// putenv("REDIRECT_URI=$redirectUri");
//
// $storage = init_hivex_xero($clientId,$clientSecret,$redirectUri);
// $ex = new ExampleClass();
//
// $xeroTenantId = (string)$storage->getSession()['tenant_id'];
// $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
// $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
//     new GuzzleHttp\Client(),
//     $config
// );
// $assetApi = new XeroAPI\XeroPHP\Api\AssetApi(
//     new GuzzleHttp\Client(),
//     $config
// );
//
// $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
//     new GuzzleHttp\Client(),
//     $config
// );
//
// $projectApi = new XeroAPI\XeroPHP\Api\ProjectApi(
//     new GuzzleHttp\Client(),
//     $config
// );
//
// $payrollAuApi = new XeroAPI\XeroPHP\Api\PayrollAuApi(
//     new GuzzleHttp\Client(),
//     $config
// );
//
// $ex->hivex_createInvoices($xeroTenantId,$service['payment_term'],$accountingApi,false,'',$invoice_lines);

// putenv("CLIENT_ID");
// putenv("CLIENT_SECRET");
// putenv("REDIRECT_URI");
