<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_POST['id'];
 $message = $_POST['dna_message'];

  $db->Query("update hx_rota set dna = ?, dna_reason = ?, dna_date = ? where id = ?");
  $db->bind(1,'1');
  $db->bind(2,$message);
  $db->bind(3,time());
  $db->bind(4,$id);
  $db->execute();


  $db->query("select * from hx_rota where id = ?");
  $db->bind(1,$id);
  $my_rota = $db->single();

  error_log('*******************************************');
  error_log('*******************************************');
  error_log('*******************************************');

  error_log(print_r($my_rota,true));

  $service_id = $my_rota['service_id'];
  $service_role_id = $my_rota['service_role_id'];
  $bid_id = $my_rota['bid_id'];
  $bid_request_id = $my_rota['bid_request_id'];
  $account_id = $my_rota['member_id'];

  $db->Query("update ws_rota_bid set completed = ? where id = ?");
  $db->bind(1,'2');
  $db->bind(2,$bid_id);
  $db->execute();

  $db->Query("update ws_rota_bid_amount set completed = 0 where pid = ? and service_role_id = ?");
  $db->bind(1,$bid_id);
  $db->bind(2,$service_role_id);
  $db->execute();

  $db->Query("update ws_rota_bid_request set accepted_user = 3 where id = ? and account_id = ?");
  $db->bind(1,$bid_request_id);
  $db->bind(2,$account_id);
  $db->execute();

 echo 'ok';
