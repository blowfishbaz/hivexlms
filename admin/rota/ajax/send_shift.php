<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $bid_id = $_POST['bid_id'];
 $created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
 $now = time();


 $db->query("select * from ws_rota_bid where id = ?");
 $db->bind(1,$bid_id);
 $bid_info = $db->single();

 $db->query("select * from ws_rota_bid_request where pid = ? and status = 1");
 $db->bind(1,$bid_id);
 $bid_req_info_all = $db->resultset();

 $db->query("select id, location, pcn_id, practice_id from ws_service where id = ?");
 $db->bind(1,$bid_info['service_id']);
 $service_data = $db->single();

 $db->query("select id, job_role, pid from ws_service_roles where pid = ? and status = 1");
 $db->bind(1,$bid_info['service_id']);
 $roles = $db->ResultSet();

   $db->Query("update ws_rota_bid set completed = 0 where id = ?");
   $db->bind(1,$bid_id);
   $db->execute();


 foreach ($roles as $r) {
   $db->query("select * from ws_rota_bid_amount where pid = ? and role_id = ? and status = 1");
   $db->bind(1,$bid_id);
   $db->bind(2,$r['job_role']);
   $bid_req_info = $db->single();



   $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id
   from ws_accounts_locations
   left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
   left join ws_accounts on ws_accounts.id = ws_accounts_locations.account_id
   where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1 and ws_accounts.status = 1");
   $db->bind(1,$service_data['location']);
   $db->bind(2,$r['job_role']);
   $list = $db->ResultSet();


   foreach ($list as $l) {
     $bid_request_id = createid('bid_rq');
     $db->Query("insert into ws_rota_bid_request (id, pid, account_id, created_date, created_by, role_id, bid_amount_pid, service_role_id, service_id) values (?,?,?,?,?,?,?,?,?)");
     $db->bind(1,$bid_request_id);
     $db->bind(2,$bid_id);
     $db->bind(3,$l['account_id']);
     $db->bind(4,$now);
     $db->bind(5,$created_by);
     $db->bind(6,$l['role_id']);
     $db->bind(7,$bid_req_info['id']);
     $db->bind(8,$r['id']);
     $db->bind(9,$r['pid']);
     $db->execute();

     $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status, pid) values (?,?,?,?,?,?,?,?,?)");
     $db->bind(1,createid('not'));
     $db->bind(2,$l['account_id']);
     $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
     $db->bind(4,time());
     $db->bind(5,'New Rota Shift');
     $db->bind(6,'A New Rota Shift has become avaiable, click on the notification to view');
     $db->bind(7,'new_shift');
     $db->bind(8,'1');
     $db->bind(9,$bid_request_id);
     $db->execute();
   }

 }

 echo 'ok';
