<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_POST['id'];

 $db->Query("update ws_rota_bid set status = 0 where id = ?");
 $db->bind(1,$id);
 $db->execute();

 $db->Query("update hx_rota set status = 0 where bid_id = ?");
 $db->bind(1,$id);
 $db->execute();

 $db->Query("update ws_rota_bid_request set status = 0 where pid = ?");
 $db->bind(1,$id);
 $db->execute();

 echo 'ok';
