<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

$id = $_GET['id'];

$db->query("select * from ws_accounts_upload where account_id = ? and type = ? and status = 1 order by id desc limit 1");
$db->bind(1,$id);
$db->bind(2,'signature');
$data = $db->single();

?>

<div class="col-lg-12" style="">
  <h3 class="text-pink">My Signature</h3>
</div>
<div class="clearfix"></div>
<div class="panel panel-default" style="border-radius:20px;">
  <div class="panel-body">


    <div class="clearfix"></div>

    <?if(!empty($data)){?>
      <img src="<?echo $fullurl.'/'.$data['path'].$data['name'];?>" style="margin:0 auto; display:table; width:100%; height:auto;" />
    <?}else{?>
      <p>
        No Signature Uploaded
      </p>
    <?}?>

  </div>
</div>

<div class="clearfix"></div>

<?if($id != decrypt($_SESSION['SESS_ACCOUNT_ID'])){?><button type="button" class="btn btn-raised btn-danger main_button" id="delete_signature" style="color:white!important;">Delete Signature</button><?}?>
<button type="button" class="btn btn-raised btn-success pull-right main_button" id="add_signature">Add Signature</button>

<div class="clearfix"></div>

<script>




</script>
