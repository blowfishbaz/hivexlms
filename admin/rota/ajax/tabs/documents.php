<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 $document_list = get_document_list($id);

 $db->query("select * from ws_accounts_upload where account_id = ? and status = 1 and type='manual'");
 $db->bind(1,$id);
 $misc_uploads = $db->resultset();

 ?>

 <style>
 table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
 }

 td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
 }

 </style>

 <div class="col-lg-12" style="">
   <h3 class="text-pink">My Documents</h3>
 </div>
 <div class="clearfix"></div>

<table>


  <?foreach ($document_list as $dl) {?>
    <?
      $db->query("select * from ws_accounts_upload where account_id = ? and status = ? and type = ?");
      $db->bind(1,$id);
      $db->bind(2,'1');
      $db->bind(3,$dl);
      $uploads_per = $db->resultset();
    ?>

    <tr>
      <th style="background-color:<?if(empty($uploads_per)){echo '#f44336;';}else{echo '#f0f0f0;';}?>" colspan="3">
        <?echo document_type_full_name($dl);?>
        <button type="button" class="btn btn-sm btn-info pull-right add_document" style="margin: 0px; color: black; padding-top:0px; padding-bottom:0px;" id="add_document" data-id="<?echo $dl?>">Add</button>
        <div class="clearfix"></div>
      </th>
    </tr>
    <tr>
      <th>Document Name</th>
      <th style="width:10%;">Action</th>
    </tr>
      <?foreach ($uploads_per as $up) {?>
        <tr>
          <td><a href="<?echo $fullurl.str_replace($up['name'],'',$up['path']).$up['name'];?>" download><?echo $up['name'];?></a></td>
          <td><button type="button" class="btn btn-sm btn-info btn-raised pull-right delete_document" style="margin: 0px; color: black;" id="" data-id="<?echo $up['id']?>">Delete</button></td>
        </tr>
      <?}?>
    <?}?>

</table>





    <ul class="list-group">
      <?foreach ($misc_uploads as $mupload) {?>
        <li class="list-group-item">
          <a href="<?echo $fullurl.$mupload['path'];?>" download><?echo $mupload['name'];?></a>

          <button type="button" class="btn btn-sm btn-info pull-right delete_document" style="margin: 0px; color: black; padding-top:0px; padding-bottom:0px;" id="" data-id="<?echo $mupload['id']?>">Delete</button>
          <button type="button" class="btn btn-sm btn-info pull-right move_document" style="margin: 0px; color: black; padding-top:0px; padding-bottom:0px;" id="" data-id="<?echo $mupload['id']?>">Move Document</button>
          <div class="clearfix"></div>
        </li>
      <?}?>
    </ul>










<script>

$( "body" ).on( "click", ".add_document", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  var new_id = '<?echo $id;?>';
  var document_type = $(this).data('id');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/upload_documents.php",
       data: {'new_id':new_id,"document_type":document_type, type:'account',loc:'user'},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
  });
});

$( "body" ).on( "click", ".delete_document", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  var document_type = $(this).data('id');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/ajax/delete_upload.php",
       data: {'id':document_type},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
  });
});


$( "body" ).on( "click", "#confirm_document_delete", function() {

  var document_type = $(this).data('id');

  $.ajax({
        type: "POST",
        url: $fullurl+"admin/rota/ajax/confirm_delete_document.php",
        data: {'id':document_type},
        success: function(msg){
          $message=$.trim(msg);
          if($message=='ok'){
            Messenger().post({
                message: 'Document Deleted.',
                type: 'error',
                showCloseButton: false
            });
            setTimeout(function(){
              //window.location.replace($fullurl+'jobs.php');
              //We show a message at the topo fo the page for the user so they know what is happening.
              reloadtab(); $('.modal').modal('hide');
            },1000);
          }

       },error: function (xhr, status, errorThrown) {
              Messenger().post({
                  message: 'An error has occurred please try again.',
                  type: 'error',
                  showCloseButton: false
              });
            }
  });
});

$( "body" ).on( "click", ".move_document", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  var document_type = $(this).data('id');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/rota/ajax/move_document.php",
       data: {'id':document_type,"user":"<?echo $id;?>"},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
  });
});

$( "body" ).on( "click", "#confirm_move_document", function() {
  var HasError = 0;
  var formid = '#document_move_form';
  var FRMdata = $(formid).serialize(); // get form data

    if (HasError == 1) {
      Messenger().post({
          message: 'Please make sure all required elements of the form are filled out.',
          type: 'error',
          showCloseButton: false
      });
    }
    else{

     $.ajax({
             type: "POST",
             url: $fullurl+"admin/rota/ajax/confirm_move_document.php",
             data: FRMdata, // serializes the form's elements.
             success: function(msg){
                 $message=$.trim(msg);
                 Messenger().post({
                         message: 'Document Moved.',
                         showCloseButton: false
                 });
                 setTimeout(function(){
                   reloadtab(); $('.modal').modal('hide');
                 },600);

              },error: function (xhr, status, errorThrown) {
             setTimeout(function(){alert('Error');},300);
           }
     });

  }
});


</script>
