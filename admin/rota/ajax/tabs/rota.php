<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 ?>

<style>
.fc-prev-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-left.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-prev-button span {
    display: none;
}

.fc-next-button {
    background-image: url('<?echo $fullurl;?>assets/images/arrow-right.png') !important;
    background-size: 100% 100%;
    background-position: center;
    background-size: auto;
    width: 30px;
    background-repeat: no-repeat;
}
.fc-next-button span {
    display: none;
}
</style>

<div class="col-lg-12" style="">
  <h3 class="text-pink">My Rota</h3>
</div>
<div class="clearfix"></div>

      <div class="calendar">

      </div>



 <script>

 jQuery(document).ready(function($) {
   var width = $('body').width();
   console.log(width);
   if(width > 980){
      $('.calendar').fullCalendar('option', 'aspectRatio', '2');
   }else{
        $('.calendar').fullCalendar('option', 'height', 'auto');
   }
 });


 $('.calendar').fullCalendar({
 header: {
   left: 'prev,next today',
   center: 'title',
   right: 'month,agendaWeek,agendaDay'
 },
 editable: false,
 timezoneParam:'GMT',
 eventLimit: true,
 buttonText: {
                 today: 'Go To Today',
                 month:    'Month',
                 week:     'Week',
                 day:      'Day'
             },
 eventSources: [

   {
       url: '../../../admin/rota/ajax/my_calendar.php?id=<?echo $id;?>', // use the `url` property
       color: '#039BE5',    // an option!
       textColor: 'white',  // an option!
       className: 'reminders',
       id: 'id'
   }
 ],
 eventClick: function(calEvent, jsEvent, view) {
   var appointmentID = calEvent.id;
     console.log(appointmentID);
     $("#BaseModalLContent").html($Loader);
 $('#BaseModalL').modal('show');
     $.ajax({
         type: "POST",
         url: "../../../admin/rota/ajax/calendar_view_appointment.php?id="+appointmentID+"",
         success: function(msg){
           //alert(msg);
           $("#BaseModalLContent").delay(1000)
            .queue(function(n) {
                $(this).html(msg);
                n();
            }).fadeIn("slow").queue(function(n) {
                         $.material.init();
                        n();

                        $('#notes').summernote({
                      height: 300,                 // set editor height
                      minHeight: null,             // set minimum height of editor
                      maxHeight: null,             // set maximum height of editor
                      focus: true,                  // set focus to editable area after initializing summernote
                      toolbar: [
                        ['font', ['bold', 'italic', 'underline']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'hr']],
                        ['help', ['help']]
                        ]
                    });
            });
          }
     });
 }

 });

 </script>
