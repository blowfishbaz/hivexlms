<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];
$today_minus_72 = strtotime(" - 3 day");

 ?>
 <style>

 table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   margin-bottom:15px;
 }

 td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
 }

 .bottom_table_holder{
   overflow: scroll;
 }

 </style>
 <?
 $db->query("select * from hx_rota where status = 1 and dna = 0 and end_ts < ? and member_id = ? order by start_ts desc");
 $db->bind(1,$today_minus_72);
 $db->bind(2,$id);
 $rota_info = $db->resultset();



 $db->query("select * from ws_accounts where id = ?");
 $db->bind(1,$id);
 $account = $db->single();

 $running_total = 0;

 ?>

 <?if(decrypt($_SESSION['SESS_ACCOUNT_ID']) == 'id_member_20161103_110736904700_82639'){?>
   <!-- <pre>
     <?//print_r($rota_info);?>
   </pre> -->
 <?}?>

 <div class="bottom_table_holder">
   <table>
     <tr style="background-color:#eee;">
       <th>Service</th>
       <th>Role</th>
       <th>Start Date / Time</th>
       <th>End Date / Time</th>
       <th>Hourly Rate</th>
       <th>Amount</th>
       <th>Status</th>
     </tr>
     <?foreach ($rota_info as $ri) {?>
       <?
         $db->query("select * from ws_service where id = ?");
         $db->bind(1,$ri['service_id']);
         $service = $db->single();

         $db->query("select * from ws_roles where id = ?");
         $db->bind(1,$ri['role']);
         $role = $db->single();

         $db->query("select * from ws_service_roles where pid = ? and job_role = ? order by id desc");
         $db->bind(1,$ri['service_id']);
         $db->bind(2,$ri['role']);
         $service_role = $db->single();

         $hourly_rate = preg_replace("/[^0-9.]/", "",$service_role['hourly_rate']);
         $time1 = $ri['start_ts'];
         $time2 = $ri['end_ts'];
         $difference = round(abs($time2 - $time1) / 3600,2);
         if($ri['invoice_paid'] == 0){
            $running_total = $running_total + ($difference * $hourly_rate);
         }
       ?>
       <tr>
         <td><?echo $service['title'];?></td>
         <td><?echo $role['title'];?></td>
         <td><?echo date('d-m-Y H:i',$ri['start_ts'])?></td>
         <td><?echo date('d-m-Y H:i',$ri['end_ts'])?></td>
         <td><?echo $service_role['hourly_rate'];?> Per Hour</td>
         <td>£<?echo $difference * $hourly_rate;?></td>
         <td><?if($ri['invoice_paid'] == 0){echo 'Not Paid';}else{echo 'Paid';}?></td>
       </tr>
     <?}?>
     <tr>
       <td colspan="5"></td>
       <th>Total:</th>
       <td>£<?echo $running_total;?></td>
     </tr>
   </table>
 </div>
