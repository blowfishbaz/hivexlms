<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $id = $_GET['id'];


  $db->query("select * from ws_accounts where id = ?");
  $db->bind(1,$id);
  $data = $db->single();

  $db->query("select * from accounts where id = ?");
  $db->bind(1,$id);
  $data_account = $db->single();


  $db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
  $db->bind(1,$id);
  $data_extra = $db->single();

  $db->query("select * from ws_accounts_roles where account_id = ? and status = 1");
  $db->bind(1,$id);
  $roles = $db->resultset();

  $db->query("select * from ws_accounts_locations where account_id = ? and status = 1");
  $db->bind(1,$id);
  $locations = $db->resultset();

  $db->query("select * from ws_roles where status = 1 order by title asc");
  $db->bind(1,$id);
  $all_roles = $db->resultset();

  $my_roles = array();
  $my_location = array();

  foreach ($roles as $r) {
    array_push($my_roles, $r['role_id']);
  }

  foreach ($locations as $l) {
    array_push($my_location, $l['location_id']);
  }


  $db->query("select hx_rota.*, ws_rota_bid_request.role_id, ws_roles.title
              from hx_rota
              left join ws_rota_bid_request on ws_rota_bid_request.id = hx_rota.bid_request_id
              left join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
              where hx_rota.member_id = ? and start_ts > ? and hx_rota.status = 1");
  $db->bind(1,$id);
  $db->bind(2,strtotime(date('d-m-Y 00:00:00')));
  $upcoming_rota = $db->resultset();

  $db->query("select hx_rota.*, ws_rota_bid_request.role_id, ws_roles.title
              from hx_rota
              left join ws_rota_bid_request on ws_rota_bid_request.id = hx_rota.bid_request_id
              left join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
              where hx_rota.member_id = ? and start_ts < ? and hx_rota.status = 1 limit 10");
  $db->bind(1,$id);
  $db->bind(2,strtotime(date('d-m-Y 00:00:00')));
  $previous_rota = $db->resultset();

?>
<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }

  #personal_table{
    width:auto;
    border-collapse: separate;
    border-spacing: 0 1em;
  }

  #personal_table th{
    border:0px;
    font-weight: 100;
  }
  #personal_table tr{
    margin-top:10px;
    margin-bottom:10px;
  }

  #personal_table td{
    border:0px;
    color:#74ccd5;
    border-radius: 10px;
    -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.6);
    -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.6);
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.6);

    font-family: 'DiariaPro' !important;
  }

  .dashboard-table th{
    text-align: center;
  }
  .dashboard-table td{
    text-align: center;
  }

  .profile_pic_holder{
    max-width:300px;
    width: 100%;
    background-color:#edaac6;
    border-radius: 20px;
    margin:0 auto;
    display: table;
    cursor: pointer;
  }

  .profile_pic_holder img{
    position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        height: 90%;
        <?if(!empty($data_account['profilepic'])){?>border-radius: 100%;<?}?>
        /* border: 15px solid white; */
        height: 70%;
        width: auto;
  }

  .profile_pic_holder_inner{
        width: 75%;
        background-color: white;
        border-radius: 100%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        padding-top: 75%;
        -webkit-box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
        -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 8px 0px rgb(0 0 0 / 75%);
    }
  }
</style>

    <div class="col-lg-12" style="">
      <h3 class="text-pink">Personal Details
        <?if($id == decrypt($_SESSION['SESS_ACCOUNT_ID']) || my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'admin'){?> <button type="button" class="btn btn-sm btn-raised btn-success pull-right view_bd">View Bank Details</button> <?}?>
      </h3>
    </div>
    <div class="col-lg-3" style="">
      <div class="profile_pic_holder">
        <div class="profile_pic_holder_inner">
          <img src="<?echo loadProfilePic2($data_account['profilepic']);?>" />
        </div>
      </div>
    </div>
    <div class="col-lg-9" style="">
      <table id="personal_table">
        <tr>
          <th>Full Name</th>
          <td><?echo $data['first_name'].' '.$data['surname'];?></td>
        </tr>
        <!-- <tr>
          <th>Date of Birth</th>
          <td></td>
        </tr> -->
        <tr>
          <th>Email Address</th>
          <td><?echo $data['email'];?></td>
        </tr>
        <tr>
          <th>Phone Number</th>
          <td><?echo $data_extra['telephone'];?></td>
        </tr>
        <tr>
          <th>Mobile Number</th>
          <td><?echo $data_extra['mobile_number'];?></td>
        </tr>
        <!-- <tr>
          <th>Preferred Role</th>
          <td></td>
        </tr>
        <tr>
          <th>Preferred Location</th>
          <td></td>
        </tr> -->
      </table>
    </div>

    <div class="col-lg-12" style="">
      <h3 class="text-pink">Role Details</h3>
    </div>

    <div class="clearfix"></div>

    <div class="col-lg-6">
      <table class="dashboard-table">
        <tr>
          <th style="width:50%; background-color:#f0f0f0;">Roles</th>
        </tr>

        <?foreach ($all_roles as $ar) {?>
          <?if(in_array($ar['id'],$my_roles)){?>
          <tr>
            <td><?echo $ar['title'];?></td>
          </tr>
          <?}?>
        <?}?>
      </table>
    </div>
    <div class="col-lg-6">
      <table class="dashboard-table">
        <tr>
          <th style="width:50%; background-color:#f0f0f0;">Locations</th>
        </tr>
        <?if(in_array('Wirral',$my_location)){?><tr><td>Wirral</td></tr><?}?>
        <?if(in_array('Sefton',$my_location)){?><tr><td>Sefton</td></tr><?}?>
        <?if(in_array('Liverpool',$my_location)){?><tr><td>Liverpool</td></tr><?}?>
      </table>
    </div>

    <div class="clearfix"></div>

    <div class="col-lg-12 dashboard-table" style="">
      <h3 class="text-pink">Upcoming Session Details</h3>
    </div>

    <div class="col-lg-12">
      <table class="dashboard-table">
        <tr style="background-color:#f0f0f0;">
          <th>Start Date</th>
          <th>End Date</th>
          <th>Start Time</th>
          <th>End Time</th>
          <th>Role Title</th>
          <th>Location</th>
        </tr>
        <?if(empty($upcoming_rota)){?>
          <tr>
            <td colspan="6">
              No Upcoming Shifts
            </td>
          </tr>
        <?}else{?>
          <?foreach ($upcoming_rota as $ur) {?>
            <tr>
              <td><?echo date('l dS F Y', $ur['start_ts']);?></td>
              <td><?echo date('l dS F Y', $ur['end_ts']);?></td>
              <td><?echo date('H:i', $ur['start_ts']);?></td>
              <td><?echo date('H:i', $ur['end_ts']);?></td>
              <td><?echo $ur['title'];?></td>
              <td><?echo $ur['location'];?></td>
            </tr>
          <?}?>
        <?}?>
      </table>

    </div>

    <div class="clearfix"></div>

    <div class="col-lg-12 dashboard-table" style="">
      <h3 class="text-pink">Past Session Details</h3>
    </div>

    <div class="col-lg-12">
      <table class="dashboard-table">
        <tr style="background-color:#f0f0f0;">
          <th>Start Date</th>
          <th>End Date</th>
          <th>Start Time</th>
          <th>End Time</th>
          <th>Role Title</th>
          <th>Location</th>
        </tr>
        <?if(empty($previous_rota)){?>
          <tr>
            <td colspan="6">
              No Previous Shifts
            </td>
          </tr>
        <?}else{?>
          <?foreach ($previous_rota as $pr) {?>
            <tr>
              <td><?echo date('l dS F Y', $pr['start_ts']);?></td>
              <td><?echo date('l dS F Y', $pr['end_ts']);?></td>
              <td><?echo date('H:i', $pr['start_ts']);?></td>
              <td><?echo date('H:i', $pr['end_ts']);?></td>
              <td><?echo $pr['title'];?></td>
              <td><?echo $pr['location'];?></td>
            </tr>
          <?}?>
        <?}?>
      </table>

    </div>



    <script>

    jQuery(document).ready(function($) {
      var width = $('.profile_pic_holder').width();
      if(width > 300){
          width = 300;
      }
      $('.profile_pic_holder').height(width);

    });




    </script>
