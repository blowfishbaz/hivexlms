<?php
/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/

include($_SERVER['DOCUMENT_ROOT'].'/application.php');

function get_week_dates($date = null){

        $given_week = date( 'W',strtotime($date));
        $given_year = date( 'Y',strtotime($date));
        $week = array();
        $m_morn = date('d-m-Y H:i:01',strtotime($given_year.'W'.$given_week));
        $m_night = date('d-m-Y 23:59:59',strtotime($given_year.'W'.$given_week));
        array_push($week,$m_morn);
        array_push($week,$m_night );
        $t_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +1 days'));
        $t_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +1 days'));
        array_push($week,$t_morn);
        array_push($week,$t_night );
        $w_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +2 days'));
        $w_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +2 days'));
        array_push($week,$w_morn);
        array_push($week,$w_night );
        $th_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +3 days'));
        $th_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +3 days'));
        array_push($week,$th_morn);
        array_push($week,$th_night );
        $f_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +4 days'));
        $f_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +4 days'));
        array_push($week,$f_morn);
        array_push($week,$f_night );
        $sa_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +5 days'));
        $sa_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +5 days'));
        array_push($week,$sa_morn);
        array_push($week,$sa_night );
        $su_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +6 days'));
        $su_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +6 days'));
        array_push($week,$su_morn);
        array_push($week,$su_night );

        return $week;
}
$given_date = $_GET['date'];
$w = get_week_dates($given_date);
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
$json = file_get_contents('public_holiday.json');
$json_a = json_decode($json,true);

function public_holidays($ph_array,$this_date){
          ///england wales only
          $eng = $ph_array['england-and-wales']['events'];
          $in_eng_wal = false;
          foreach($eng as $row){
                if($row['date']==$this_date){
                      $in_eng_wal = true;
                }
          }
          //scotland
          $sco = $ph_array['scotland']['events'];
          $in_sco = false;
          foreach($sco as $row){
                if($row['date']==$this_date){
                      $in_sco = true;
                }
          }
          //northern ireland
          $nir = $ph_array['northern-ireland']['events'];
          $in_nir = false;
          foreach($nir as $row){
                if($row['date']==$this_date){
                      $in_nir = true;
                }
          }
          if($in_eng_wal== true || $in_sco == true || $in_nir == true){
                          if($in_eng_wal==true && $in_sco == true && $in_nir == true){
                                               $flag = 'All UK';
                          }
                          else{
                                              if($in_eng_wal==true && $in_sco == false && $in_nir == false){
                                                         $flag = 'Bank Holiday * England & Wales Only';
                                              }

                                              elseif($in_eng_wal==true && $in_sco == true && $in_nir == false){
                                                         $flag = 'Bank Holiday * England, Wales & Scotland Only ';
                                              }

                                              elseif($in_eng_wal==true && $in_sco == false && $in_nir == true){
                                                         $flag = 'Bank Holiday * England, Wales & N Ireland Only';
                                              }

                                              elseif($in_eng_wal==false && $in_sco == true && $in_nir == true){
                                                         $flag = 'Bank Holiday * Scotland & N Ireland Only';
                                              }

                                              elseif($in_eng_wal==false && $in_sco == true && $in_nir == false){
                                                         $flag = 'Bank Holiday * Scotland Only';
                                              }

                                              elseif($in_eng_wal==false && $in_sco == false && $in_nir == true){
                                                         $flag = 'Bank Holiday * N Ireland Only';
                                              }else{
                                                         $flag = '';
                                              }
                          }
          }else{
                         $flag = '';
          }
          return $flag;
}
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //
// --------------------------- public holidays --------------------------------------- //// --------------------------- public holidays --------------------------------------- //



$db->query("select * from hx_homes where id = ?");
$db->bind(1,$_GET['id']);
//$thisHome = $db->single();


$monday_f = date('U',strtotime($w[0]));
$monday_n = date('U',strtotime($w[1]));
$tuesday_f = date('U',strtotime($w[2]));
$tuesday_n = date('U',strtotime($w[3]));
$wednesday_f = date('U',strtotime($w[4]));
$wednesday_n = date('U',strtotime($w[5]));
$thursday_f = date('U',strtotime($w[6]));
$thursday_n = date('U',strtotime($w[7]));
$friday_f = date('U',strtotime($w[8]));
$friday_n = date('U',strtotime($w[9]));
$saturday_f = date('U',strtotime($w[10]));
$saturday_n = date('U',strtotime($w[11]));
$sunday_f = date('U',strtotime($w[12]));
$sunday_n = date('U',strtotime($w[13]));


$monday_d = date('Y-m-d',strtotime($w[0]));
$tuesday_d = date('Y-m-d',strtotime($w[2]));
$wednesday_d = date('Y-m-d',strtotime($w[4]));
$thursday_d = date('Y-m-d',strtotime($w[6]));
$friday_d = date('Y-m-d',strtotime($w[8]));
$saturday_d = date('Y-m-d',strtotime($w[10]));
$sunday_d = date('Y-m-d',strtotime($w[12]));

$md = public_holidays($json_a,$monday_d);
$td = public_holidays($json_a,$tuesday_d);
$wd = public_holidays($json_a,$wednesday_d);
$thd = public_holidays($json_a,$thursday_d);
$fd = public_holidays($json_a,$friday_d);
$sad = public_holidays($json_a,$saturday_d);
$sud = public_holidays($json_a,$sunday_d);




$today_d = date('Y-m-d',time());

$m_today = date('Y-m-d',strtotime($w[0]));
$t_today = date('Y-m-d',strtotime($w[2]));
$w_today = date('Y-m-d',strtotime($w[4]));
$th_today = date('Y-m-d',strtotime($w[6]));
$f_today = date('Y-m-d',strtotime($w[8]));
$sa_today = date('Y-m-d',strtotime($w[10]));
$su_today = date('Y-m-d',strtotime($w[12]));


?>
<div class="row"></div>


<?

$the_date_now = date('d-m-Y',time());
$end_of_week_date = date('d-m-Y',strtotime($w[12]));

if($the_date_now > $end_of_week_date){
    $active_date = 'main_add_past';
}
else{
    $active_date = 'main_add';
}



?>


<h4 class="rota_num">Rota Week #<? echo date("W", strtotime($given_date)); ?></h4>


<input type="hidden" value="<? echo $_GET['id']; ?>" id="homeId">

<div class="col-lg-12">
        <div class="row">
                <div class="c_panel_top">
                      <div class="cur_info">
                            <span class="w_dat">Current Date: <strong><? echo date('d-m-Y',time()); ?></strong></span>
                            <span class="w_num">Current Week: <strong>#<? echo date('W',time()); ?></strong></span>
                      </div>
                </div>
        </div>
</div>

<div class="col-lg-12">
        <div class="row">

          <?
          $prev = date('Y-m-d',strtotime($_GET['date'].' -7 days'));
          $now = date('Y-m-d',time());
          $next = date('Y-m-d',strtotime($_GET['date'].' +7 days'));
          ?>

              <div class="c_panel">
                    <div class="cur_info">
                      <input class="form-control week_pick" type="week" id="week" name="week" value="2020-W<? echo date("W", strtotime($given_date)); ?>">

                    </div>
                    <div class="cur_info_opts">
                        <button class="btn btn-info btn-raised btn-sm add_home" data-date="<? echo $prev; ?>">< Previous</button>
                        <button class="btn btn-info btn-raised btn-sm add_home" data-date="<? echo $now; ?>"> Current Week</button>
                        <button class="btn btn-info btn-raised btn-sm add_home" data-date="<? echo $next; ?>"> Next ></button>
                    </div>
            </div>
      </div>
</div>

<!-- rota_holder -->
<div class="col-lg-12 rota_holder">
<!-- rota_holder -->



          <div class="row">
                <div class="col-md-1 day_head">Staff Name<br /><span class="s_date"></span></div>
                <div class="col-md-1 day_head">Monday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[0])); ?><br /></span></div>
                <div class="col-md-1 day_head">Tuesday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[2])); ?><br /></span></div>
                <div class="col-md-1 day_head">Wednesday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[4])); ?><br /></span></div>
                <div class="col-md-1 day_head">Thursday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[6])); ?><br /></span></div>
                <div class="col-md-1 day_head">Friday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[8])); ?><br /></span></div>
                <div class="col-md-1 day_head">Saturday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[10])); ?><br /></span></div>
                <div class="col-md-1 day_head">Sunday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[12])); ?><br /></span></div>
          </div>


          <!-- ROTA ROWS  -->
          <div id="rota_rows">
          <!-- ROTA ROWS  -->


          <?


          function get_holiday_for_day($userId, $dayStart, $dayEnd){

              $userid = $userId;
              $daystart = $dayStart;
              $dayend = $dayEnd;

               $db = new database;
               $db->query("SELECT * from hx_holiday where holiday_for = ? AND ((holiday_from < ? AND holiday_to > ?)OR(holiday_from < ? AND
                           holiday_to > ? AND holiday_to < ?) OR (holiday_from > ? AND holiday_from < ? AND holiday_to > ?) OR (holiday_from > ? AND
                           holiday_from < ? AND holiday_to > ? AND holiday_to < ?))");
                           $db->bind(1,$userid);
                           $db->bind(2,$daystart);
                           $db->bind(3,$dayend);
                           $db->bind(4,$daystart);
                           $db->bind(5,$daystart);
                           $db->bind(6,$dayend);
                           $db->bind(7,$daystart);
                           $db->bind(8,$dayend);
                           $db->bind(9,$dayend);
                           $db->bind(10,$daystart);
                           $db->bind(11,$dayend);
                           $db->bind(12,$daystart);
                           $db->bind(13,$dayend);
                           //$hols = $db->single();

               return $hols;

          }



          // $db->query("SELECT accounts.*, hx_home_users.type as hometype FROM accounts
          //             LEFT JOIN hx_home_users
          //             ON hx_home_users.user_id = accounts.id
          //             WHERE hx_home_users.home_id = ? order by accounts.name ASC");
          //             $db->bind(1,$_GET['id']);
          //             $peeps = $db->resultSet();

          $db->query("select ws_accounts.id as id, first_name, surname, email, account_type
          from ws_accounts
          left join ws_accounts_extra_info
          on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
           order by first_name asc");

          $peeps = $db->ResultSet();

          foreach ($peeps as $key => $v) {

            $db_weeknum =date("W", strtotime($given_date));

            $db->query("select * from hx_rota where member_id = ? and home_id = ? and week_num = ? and status = ?");
            $db->bind(1,$v['id']);
            $db->bind(2,$_GET['id']);
            $db->bind(3,$db_weeknum);
            $db->bind(4,'1');
            $editOeNot = $db->resultSet();
            $rowcount = $db->rowcount();

            if($rowcount=='0'){
              $but_type = 'new';
            }
            else{
              $but_type = 'edit';
            }

          ?>

                      <!-- day worker  -->
                      <div class="row day_worker">
                      <!-- day worker  -->




                              <!--  -->
                              <!-- NAME TAG -->
                              <!--  -->
                                                  <div class="col-md-1 wrow name_tag">
                                                              <div class="day_add <? echo $active_date; ?>" data-but="<? echo $but_type; ?>" data-week="<? echo date("W", strtotime($given_date)); ?>"  data-home="<? echo $_GET['id']; ?>" data-staff_name="<? echo $v['name']; ?>" data-staff="<? echo $v['id']; ?>"  data-bdate="<? echo $monday_f; ?>" data-edate="<? echo $sunday_n; ?>" ><span class="glyphicons glyphicons-plus-sign"></span></div>
                                                              <p><? echo $v['first_name'].' '.$v['surname']; ?></p>
                                                              <p class="info_p"></p>
                                                              <p class="info_p"></p>
                                                              <?
                                                              if($v['hometype']){}

                                                              ?>
                                                              <p class="info_p">Cover This Home: <span class="info_val"><span class="glyphicons glyphicons-tick"></span></span></p>

                                                  </div>
                              <!--  -->
                              <!-- NAME TAG -->
                              <!--  -->

                              <!--  -->
                              <!-- MONDAY-->
                              <!--  -->
                                                  <?
                                                  $daystart = $monday_f;
                                                  $dayend = $monday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);

                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }

                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?> <? if($md!=''){ echo 'hol';} if($today_d==$m_today){ echo ' today_box';}?> ">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->


                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $md; ?></span>
                                                  <!-- Default  -->

                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->



<!-- b4 shifts -->
<!-- b4 shifts -->
<?
$thisDay_db = date('d-m-Y',$daystart);
$db->query("SELECT * from hx_rota where member_id = ? and end_date = ?  and start_date != ? and status = ? order by  end_time ASC");
$db->bind(1,$v['id']);
$db->bind(2,$thisDay_db);
$db->bind(3,$thisDay_db);
$db->bind(4,'1');
$b4_shifts = $db->resultSet();


foreach ($b4_shifts as $key => $b4) {

                echo  '<div class="span_day before_day">'.$b4['start_time'].' - '.$b4['end_time'].'</div>';

}

?>
<!-- b4 shifts -->
<!-- b4 shifts -->



                                                  <!-- <div class="in_day"> 10.00am - 11.59am</div>
                                                  <div class="span_day"> 18.00pm - 23.28pm</div>
                                                  <div class="span_day before_day">...02.30am</div>
                                                  <div class="span_day" data-placement="top" data-toggle="tooltip" title="18.00pm - 02.00am"> 18.00pm...</div>
                                                  <div class="other_day"> 10.00am - 11.59am</div>
                                                  <div class="other_span_day"> 18.00pm - 23.28pm</div>
                                                  <div class="other_span_day before_day"> ...02.30am</div>
                                                  <div class="other_span_day" data-placement="top" data-toggle="tooltip" title="18.00pm - 02.00am"> 18.00pm...</div> -->


                                                  </div>





                              <!--  -->
                              <!-- MONDAY-->
                              <!--  -->

                              <!--  -->
                              <!-- TUESDAY-->
                              <!--  -->

                                                  <?
                                                  $daystart = $tuesday_f;
                                                  $dayend = $tuesday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);
                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }
                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?><? if($td!=''){ echo 'hol';} if($today_d==$t_today){ echo ' today_box';}?> ">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->


                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $td; ?></span>
                                                  <!-- Default  -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->


                                                  </div>
                              <!--  -->
                              <!-- TUESDAY-->
                              <!--  -->

                              <!--  -->
                              <!-- WEDNESDAY-->
                              <!--  -->


                                                  <?
                                                  $daystart = $wednesday_f;
                                                  $dayend = $wednesday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);
                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }
                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?><? if($wd!=''){ echo 'hol';} if($today_d==$w_today){ echo ' today_box';}?> ">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->

                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $wd; ?></span>
                                                  <!-- Default  -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  </div>
                              <!--  -->
                              <!-- WEDNESDAY-->
                              <!--  -->

                              <!--  -->
                              <!-- THURSDAY-->
                              <!--  -->

                                                  <?
                                                  $daystart = $thursday_f;
                                                  $dayend = $thursday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);
                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }

                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?><? if($thd!=''){ echo 'hol';} if($today_d==$th_today){ echo ' today_box';}  ?>  ">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->

                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $thd; ?></span>
                                                  <!-- Default  -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  </div>
                              <!--  -->
                              <!-- THURSDAY-->
                              <!--  -->

                              <!--  -->
                              <!-- FRIDAY-->
                              <!--  -->

                                                  <?
                                                  $daystart = $friday_f;
                                                  $dayend = $friday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);
                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }
                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?><? if($fd!=''){ echo 'hol';} if($today_d==$f_today){ echo ' today_box';} ?>">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->
                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $fd; ?></span>
                                                  <!-- Default  -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  </div>
                              <!--  -->
                              <!-- FRIDAY-->
                              <!--  -->

                              <!--  -->
                              <!-- SATURDAY-->
                              <!--  -->

                                                  <?
                                                  $daystart = $saturday_f;
                                                  $dayend = $saturday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);
                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }
                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?><? if($sad!=''){ echo 'hol';} if($today_d==$sa_today){ echo ' today_box';}?>">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->
                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $sad; ?></span>
                                                  <!-- Default  -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  </div>

                              <!--  -->
                              <!-- SATURDAY-->
                              <!--  -->

                              <!--  -->
                              <!-- SUNDAY-->
                              <!--  -->

                                                  <?
                                                  $daystart = $sunday_f;
                                                  $dayend = $sunday_n;
                                                  $dayHols = get_holiday_for_day($v['id'],$daystart,$dayend);
                                                  if($dayHols['id']!=''){ if($dayHols['days_taken']!='0.5'){ $h = 'my_hol'; $half_hol = ''; }
                                                  else{ $h = ''; $half_hol = '<div class="in_day half_hol">xxxxxxxxxxxxxxx</div>'; } }
                                                  else{ $h = ''; $half_hol = ''; }
                                                  ?>
                                                  <div class="col-md-1 wrow <? echo $h; ?><? if($sud!=''){ echo 'hol';} if($today_d==$su_today){ echo ' today_box';}?>">
                                                    <!-- <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div> -->
                                                  <!-- Default  -->
                                                  <? echo $half_hol; ?>
                                                  <span class="p_hol"><? echo $sud; ?></span>
                                                  <!-- Default  -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <?
                                                  $thisDay_db = date('d-m-Y',$daystart);
                                                  $db->query("SELECT * from hx_rota where member_id = ? and start_date = ? and status = ? order by start_time ASC");
                                                  $db->bind(1,$v['id']);
                                                  $db->bind(2,$thisDay_db);
                                                  $db->bind(3,'1');
                                                  $shifts = $db->resultSet();
                                                  $rowcount = $db->rowcount();
                                                  if($rowcount!=0){
                                                      foreach ($shifts as $key => $s) {
                                                            //if the shift is at this home
                                                            if($s['home_id']==$_GET['id']){
                                                                    echo '<div class="'.$s['type'].'"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                            }else{
                                                                    if($s['type']=='in_day'){
                                                                    echo '<div class="other_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                                    else{
                                                                            echo '<div class="other_span_day"> '.$s['start_time'].' - '.$s['end_time'].'</div>';
                                                                    }
                                                            }
                                                      }
                                                  }
                                                  ?>
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  <!-- shifts -->
                                                  </div>
                              <!--  -->
                              <!-- SUNDAY-->
                              <!--  -->



                      <!-- day worker  -->
                      </div>
                      <!-- day worker  -->


              <?
              }
              ?>


          <!-- ROTA ROWS  -->
          </div>
          <!-- ROTA ROWS  -->

<!-- rota_holder -->
</div>
<!-- rota_holder -->
