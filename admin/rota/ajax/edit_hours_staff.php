<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');



// echo '<pre>';
// print_r($_GET);
// echo '</pre>';


function get_holiday_for_day($userId, $dayStart, $dayEnd){

    $userid = $userId;
    $daystart = $dayStart;
    $dayend = $dayEnd;

     $db = new database;
     $db->query("SELECT * from hx_holiday where holiday_for = ? AND ((holiday_from < ? AND holiday_to > ?)OR(holiday_from < ? AND
                 holiday_to > ? AND holiday_to < ?) OR (holiday_from > ? AND holiday_from < ? AND holiday_to > ?) OR (holiday_from > ? AND
                 holiday_from < ? AND holiday_to > ? AND holiday_to < ?))");
                 $db->bind(1,$userid);
                 $db->bind(2,$daystart);
                 $db->bind(3,$dayend);
                 $db->bind(4,$daystart);
                 $db->bind(5,$daystart);
                 $db->bind(6,$dayend);
                 $db->bind(7,$daystart);
                 $db->bind(8,$dayend);
                 $db->bind(9,$dayend);
                 $db->bind(10,$daystart);
                 $db->bind(11,$dayend);
                 $db->bind(12,$daystart);
                 $db->bind(13,$dayend);
                 $hols = $db->single();

     return $hols;

}


function get_week_dates($date = null){

        $given_week = date( 'W',strtotime($date));
        $given_year = date( 'Y',strtotime($date));
        $week = array();
        $m_morn = date('d-m-Y H:i:01',strtotime($given_year.'W'.$given_week));
        $m_night = date('d-m-Y 23:59:59',strtotime($given_year.'W'.$given_week));
        array_push($week,$m_morn);
        array_push($week,$m_night );
        $t_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +1 days'));
        $t_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +1 days'));
        array_push($week,$t_morn);
        array_push($week,$t_night );
        $w_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +2 days'));
        $w_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +2 days'));
        array_push($week,$w_morn);
        array_push($week,$w_night );
        $th_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +3 days'));
        $th_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +3 days'));
        array_push($week,$th_morn);
        array_push($week,$th_night );
        $f_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +4 days'));
        $f_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +4 days'));
        array_push($week,$f_morn);
        array_push($week,$f_night );
        $sa_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +5 days'));
        $sa_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +5 days'));
        array_push($week,$sa_morn);
        array_push($week,$sa_night );
        $su_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +6 days'));
        $su_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +6 days'));
        array_push($week,$su_morn);
        array_push($week,$su_night );

        return $week;
}


$z = date('Y-m-d',$_GET['week_s']);

$weekEnd = date('W',$_GET['week_e']);


$given_date = $z;
$w = get_week_dates($given_date);


$monday_f = date('U',strtotime($w[0]));
$monday_n = date('U',strtotime($w[1]));
$tuesday_f = date('U',strtotime($w[2]));
$tuesday_n = date('U',strtotime($w[3]));
$wednesday_f = date('U',strtotime($w[4]));
$wednesday_n = date('U',strtotime($w[5]));
$thursday_f = date('U',strtotime($w[6]));
$thursday_n = date('U',strtotime($w[7]));
$friday_f = date('U',strtotime($w[8]));
$friday_n = date('U',strtotime($w[9]));
$saturday_f = date('U',strtotime($w[10]));
$saturday_n = date('U',strtotime($w[11]));
$sunday_f = date('U',strtotime($w[12]));
$sunday_n = date('U',strtotime($w[13]));


$monday_d = date('Y-m-d',strtotime($w[0]));
$tuesday_d = date('Y-m-d',strtotime($w[2]));
$wednesday_d = date('Y-m-d',strtotime($w[4]));
$thursday_d = date('Y-m-d',strtotime($w[6]));
$friday_d = date('Y-m-d',strtotime($w[8]));
$saturday_d = date('Y-m-d',strtotime($w[10]));
$sunday_d = date('Y-m-d',strtotime($w[12]));


$m_today = date('d-m-Y',strtotime($w[0]));
$t_today = date('d-m-Y',strtotime($w[2]));
$w_today = date('d-m-Y',strtotime($w[4]));
$th_today = date('d-m-Y',strtotime($w[6]));
$f_today = date('d-m-Y',strtotime($w[8]));
$sa_today = date('d-m-Y',strtotime($w[10]));
$su_today = date('d-m-Y',strtotime($w[12]));


$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,$_GET['id']);
$user = $db->single();

$_GET['id']

?>
<style>
/* table, th, td {
  border: 1px solid black;
} */



.day_p{
  margin-bottom: 8px;
  font-weight: bold;
  float: left;
  width: 100%;
}


.day_tr{
  margin-bottom: 25px;
  width: 100%;
  float: left;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  /* -webkit-border-top-left-radius: 20px;
  -moz-border-radius-topleft: 20px;
  border-top-left-radius: 20px; */
}

.day_tr:nth-child(odd){
  background-color: #e7f5f7;
  padding: 10px;
}

.day_tr:nth-child(even){
  padding: 10px;
}

.day_tr > td > button{
  float: left;
  margin-right: 5px;
}

.clear_table{
  min-width:100%;
  float: left;
  min-height: 15px;
}

</style>



<h4 class="rota_h4">Edit Week Rota </h3>
<br />

<!-- ------------------------ BLANK -------------------------- -->
<!-- ------------------------ BLANK -------------------------- -->
<!-- ------------------------ BLANK -------------------------- -->
<!-- ------------------------ BLANK -------------------------- -->
<form id="add_rota_week_form">

<input type="hidden" name="week_number_input" id="week_number_input" value="<? echo $weekEnd; ?>" />



<div class="day_rota_add_form" style="">


<h4 style="margin-bottom:15px; margin-left:19px; font-weight:bold;">Staff: <? echo $user['name']; ?></h4>

          <table style="width:94%; margin-left:3%;">

<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $monday_f;
$dayend = $monday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = true; }
else{$show = false; } }
else{ $show = true; }

$thisDayLoop = $m_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Monday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]"  class="span_d" value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>

<!-- day --><!-- day -->
<!-- day --><!-- day -->


<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $tuesday_f;
$dayend = $tuesday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = false; }
else{$show = true; } }
else{ $show = true; }


$thisDayLoop = $t_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Tuesday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]"  class="span_d"  value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>
<!-- day --><!-- day -->
<!-- day --><!-- day -->

<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $wednesday_f;
$dayend = $wednesday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = false; }
else{$show = true; } }
else{ $show = true; }

$thisDayLoop = $w_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Wednesday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]" class="span_d" value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>
<!-- day --><!-- day -->
<!-- day --><!-- day -->

<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $thursday_f;
$dayend = $thursday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = false; }
else{$show = true; } }
else{ $show = true; }

$thisDayLoop = $th_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Thursday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]" class="span_d" value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>
<!-- day --><!-- day -->
<!-- day --><!-- day -->

<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $friday_f;
$dayend = $friday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = false; }
else{$show = true; } }
else{ $show = true; }

$thisDayLoop = $f_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Friday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]" class="span_d" value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>
<!-- day --><!-- day -->
<!-- day --><!-- day -->

<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $saturday_f;
$dayend = $saturday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = false; }
else{$show = true; } }
else{ $show = true; }

$thisDayLoop = $sa_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Saturday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]" class="span_d" value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>
<!-- day --><!-- day -->
<!-- day --><!-- day -->

<!-- day --><!-- day -->
<!-- day --><!-- day -->
<?
$daystart = $sunday_f;
$dayend = $sunday_n;
$dayHols = get_holiday_for_day($user['id'],$daystart,$dayend);
if($dayHols['id']!=''){
if($dayHols['days_taken']!='0.5'){
$show = false; }
else{$show = true; } }
else{ $show = true; }

$thisDayLoop = $su_today;

?>

<? if($show){ ?>
                    <tr class="day_tr">
                        <td class="day_p">Sunday - <? echo $thisDayLoop; ?> </td>
                            <td class="clear_table"></td>
                            <td class="small_date"><span class="small_date_td"><? echo date("D d-m-Y ", strtotime($thisDayLoop)); ?></span> <span class="small_date_nd"> <? echo date("D d-m-Y ", strtotime($thisDayLoop ." + 1 day")); ?></span></td>
                            <td class="clear_table"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="start_input" style="padding: 10px;width:95%; margin:0px;"></td>
                            <td style="width:25%">
                                  <input type="time" name="monday[]" class="end_input"  style="padding: 10px;width:95%; margin:0px;">
                                  <input type="hidden" name="thisDate[]" class="span_d" value="<? echo $thisDayLoop; ?>">
                                  <input type="hidden" name="thisSpan[]" class="span_v" value="0">
                                </td>

                            <td style="width:34%" class="but_control">
                                  <button type="button" class="btn btn-info btn-raised btn-sm span_but" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-moon"></span></button>
                                  <button type="button" class="btn btn-defualt btn-raised btn-sm disabled" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;" ><span class="glyphicons glyphicons-minus-sign"></span></button>
                                  <button type="button" class="btn btn-success btn-sm nowait add_new_day_row" data-uday="monday" data-udate="<? echo $thisDayLoop; ?>" style=" padding: 9px; font-size:14px; margin:0px; margin-left:15px; margin-bottom:5px;"><span class="glyphicons glyphicons-plus-sign"></span></button>
                            </td>
                      </tr>

<? } ?>
<!-- day --><!-- day -->
<!-- day --><!-- day -->
          </table>

          <div class="clearfix"></div>
          <div class="col-md-12">
            <p class="empty_rota_warning" style="color:red; font-weight:bold; display:none;">
              Nothing To Save
            </p>
          </div>

          <div class="clearfix"></div>
          <div class="col-md-12">
            <p class="time_rota_warning" style="color:red; font-weight:bold; display:none;">
            Time Issues
            </p>
          </div>

        <div class="clearfix"></div>
        <div class="col-md-12">
              <button type="button" class="btn btn-warning btn-raised minimise_rota_week" data-member_id="<? echo $_GET['id'];?>" data-dismiss="modal">Minimise</button>
              <button type="button" class="btn btn-primary btn-raised pull-right save_week_rota_but" data-sweek="<? echo $z; ?>" data-member_id="<? echo $_GET['id'];?>">Save</button>
              <!-- <button type="button" class="btn btn-primary btn-raised pull-right test_but">Test</button> -->
        </div>
        <div class="clearfix"></div>


</div>

</form>

<!-- ------------------------ BLANK -------------------------- -->
<!-- ------------------------ BLANK -------------------------- -->
<!-- ------------------------ BLANK -------------------------- -->
<!-- ------------------------ BLANK -------------------------- -->




<form class="hidden_form" style="display:none;" >
</form>

<form class="rota_sent" style="display:none;" >
  <div id="FRMloader"><img src="<? echo $fullurl ?>/assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading</div>
</form>
