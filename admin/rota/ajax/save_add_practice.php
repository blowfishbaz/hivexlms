<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$id = createid('prac');
$title = $_POST['practice_name'];
$address1 = $_POST['practice_address'];
$address2 = $_POST['practice_address2'];
$address3 = $_POST['practice_address3'];
$city = $_POST['practice_city'];
$county = $_POST['practice_county'];
$postcode = $_POST['practice_postcode'];
$location_id = $_POST['practice_location'];
$location = '';

if($location_id == 'wirral' || $location_id == 'liverpool' || $location_id == 'sefton'){
  $location = $location_id;
  $location_id = '';
}else{
  $db->query("select * from ws_pcn where id = ?");
  $db->bind(1,$location_id);
  $pcn = $db->single();

  $location = $pcn['location_id'];
}


$db->Query("insert into ws_practice (id, created_by, created_date, title, pcn_location, location_id, address1, address2, address3, city, county, postcode) values (?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,$id);
$db->bind(2,$created_by);
$db->bind(3,$now);
$db->bind(4,$title);
$db->bind(5,$location_id);
$db->bind(6,$location);
$db->bind(7,$address1);
$db->bind(8,$address2);
$db->bind(9,$address3);
$db->bind(10,$city);
$db->bind(11,$county);
$db->bind(12,$postcode);
$db->execute();

echo 'ok';
