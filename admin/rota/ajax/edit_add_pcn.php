<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$title = $_POST['pcn_name'];
$address1 = $_POST['pcn_address'];
$address2 = $_POST['pcn_address2'];
$address3 = $_POST['pcn_address3'];
$city = $_POST['pcn_city'];
$county = $_POST['pcn_county'];
$postcode = $_POST['pcn_postcode'];
$location_id = $_POST['pcn_location'];


$db->Query("update ws_pcn set pcn_title = ?, location_id = ?, address1 = ?, address2 = ?, address3 = ?, city = ?, county = ?, postcode = ? where id = ?");
$db->bind(1,$title);
$db->bind(2,$location_id);
$db->bind(3,$address1);
$db->bind(4,$address2);
$db->bind(5,$address3);
$db->bind(6,$city);
$db->bind(7,$county);
$db->bind(8,$postcode);
$db->bind(9,$_POST['id']);
$db->execute();

echo 'ok';
