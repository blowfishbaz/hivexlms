<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


  $bid_id = $_POST['bid_id'];
  $bid_am = $_POST['bid_am'];
  $user_account = $_POST['user'];

  $db->query("select * from ws_rota_bid where id = ?");
  $db->bind(1,$bid_id);
  $bid_info = $db->single();

  $db->query("select * from ws_service where id = ?");
  $db->bind(1,$bid_info['service_id']);
  $service = $db->single();

  $db->query("select * from ws_rota_bid_request WHERE pid = ? and account_id = ? and bid_amount_pid = ?");
  $db->bind(1,$bid_id);
  $db->bind(2,$user_account);
  $db->bind(3,$bid_am);
  $my_rota_bid = $db->single();

  $db->Query("update ws_rota_bid_request set accepted_admin = ?, choice_date_admin = ?, accepted_admin_id = ?, accepted_user = ?, choice_date = ?, seen = 1, seen_date = ? where id = ?");
  $db->bind(1,'1');
  $db->bind(2,time());
  $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $db->bind(4,'1');
  $db->bind(5,time());
  $db->bind(6,time());
  $db->bind(7,$my_rota_bid['id']);
  $db->execute();


  $thisSaveId = createid('sv');
  $thisId = createid('shift');
  $thisMemberId = $user_account;
  $thisHome = 'N/A';
  $thisStartDate = date('d-m-Y',strtotime($bid_info['start_date'])); //d-m-Y
  $thisStartTime = $bid_info['start_time']; //H:i
  $thisEndDate = date('d-m-Y',strtotime($bid_info['end_date'])); //d-m-Y
  $thisEndTime = $bid_info['end_time']; //H:i
  $thisStart_ts = $bid_info['start_final'];
  $thisEnd_ts = $bid_info['end_final'];
  $thisWeekNum = date('W',strtotime($bid_info['start_date']));
  $thisYear = date('Y',strtotime($bid_info['start_date']));
  $thisType = 'in_day';
  $role = $my_rota_bid['role_id'];
  $location = $service['location'];
  $bid_request_id = $my_rota_bid['id'];
  $service_role_id = $my_rota_bid['service_role_id'];

  $db->query("insert into hx_rota (save_id,id,member_id,home_id,start_date,start_time,end_date,end_time,start_ts,end_ts,week_num,year,type,role,location,service_id,bid_id,bid_request_id,service_role_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
  $db->bind(1, $thisSaveId);
  $db->bind(2, $thisId);
  $db->bind(3, $thisMemberId);
  $db->bind(4, $thisHome);
  $db->bind(5, $thisStartDate);
  $db->bind(6, $thisStartTime);
  $db->bind(7, $thisEndDate);
  $db->bind(8, $thisEndTime);
  $db->bind(9, $thisStart_ts);
  $db->bind(10, $thisEnd_ts);
  $db->bind(11, $thisWeekNum);
  $db->bind(12, $thisYear);
  $db->bind(13, $thisType);
  $db->bind(14, $role);
  $db->bind(15, $location);
  $db->bind(16, $service['id']);//service id
  $db->bind(17, $bid_id);
  $db->bind(18, $bid_request_id);
  $db->bind(19, $service_role_id);
  $db->execute();

  $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
  $db->bind(1,$bid_id);
  $bid_amount_info = $db->resultset();

  foreach ($bid_amount_info as $ba) {
    $amount = $ba['amount'];
    $role = $ba['role_id'];
    $ba_id = $ba['id'];

    $check_accepted = 0;
    $check_declined = 0;
    $check_outstanding = 0;

    //Then get all the ones who have accepted this role?
    $db->query("select * from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and status = 1");
    $db->bind(1,$bid_id);
    $db->bind(2,$ba['id']);
    $bid_req_info = $db->resultset();

    foreach ($bid_req_info as $bri) {
      if($bri['accepted_admin'] == 1){
        $check_accepted++;
      }else if($bri['accepted_admin'] == 2){
        $check_declined++;
      }else if($bri['accepted_admin'] == 0){
        $check_outstanding++;
      }
    }
    if($amount <= $check_accepted){
      //Then we can completed this one

      $db->Query("update ws_rota_bid_amount set completed = ? where id = ?");
      $db->bind(1,'1');
      $db->bind(2,$ba_id);
      $db->execute();

    }
  }

  $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
  $db->bind(1,$bid_id);
  $bid_amount_check = $db->resultset();

  $check_completed_bac = 0;
  $check_outstanding_bac = 0;

  foreach ($bid_amount_check as $bac) {
    if($bac['completed'] == 1){
      $check_completed_bac = 1;
    }else if($bac['completed'] == 0){
      $check_outstanding_bac = 1;
    }
  }

  if($check_completed_bac == 1 && $check_outstanding_bac == 0){
    $db->Query("update ws_rota_bid set completed = ? where id = ?");
    $db->bind(1,'1');
    $db->bind(2,$bid_id);
    $db->execute();
  }

echo 'ok';
