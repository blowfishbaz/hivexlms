<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$id = $_GET['id'];


function view_info_type($id){
  $array = explode("_", $id);

    if($array['1'] == 'bid'){
        return 'bid';
    }else if($array['1'] == 'rota' || $array['1'] == 'shift'){
        return 'rota';
    }else{
        return $array;
    }
}


if(view_info_type($id) == 'bid'){
  $db->query("select * from ws_rota_bid where id = ?");
  $db->bind(1,$id);
  $rota = $db->single();

  $db->query("select * from ws_service where id = ?");
  $db->bind(1,$rota['service_id']);
  $service = $db->single();

  $db->query("select ws_rota_bid_amount.*, ws_roles.title from ws_rota_bid_amount left join ws_roles on ws_roles.id = ws_rota_bid_amount.role_id where ws_rota_bid_amount.pid = ? and ws_rota_bid_amount.status = 1");
  $db->bind(1,$id);
  $roles = $db->resultSet();
}else{
  $db->query("select * from hx_rota where id = ?");
  $db->bind(1,$id);
  $rota = $db->single();

  $db->query("select * from ws_roles where id = ?");
  $db->bind(1,$rota['role']);
  $role = $db->single();

  $db->query("select * from accounts where id = ?");
  $db->bind(1,$rota['member_id']);
  $account = $db->single();

  $db->query("select * from ws_service where id = ?");
  $db->bind(1,$rota['service_id']);
  $service = $db->single();
}


?>

<style>


table {

    border-collapse: collapse;
    width: 100%;
}

td, th {
  border:0px;
    border-bottom: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

th{
  border-right: 1px solid #dddddd;
}
</style>
<h3>View Shift Info</h3>
<table>
  <tr>
    <th>Start Date</th>
    <td><?echo date('l dS F Y',strtotime($rota['start_date']));?></td>
  </tr>
  <tr>
    <th>Start Time</th>
    <td><?echo $rota['start_time'];?></td>
  </tr>
  <tr>
    <th>End Date</th>
    <td><?echo date('l dS F Y',strtotime($rota['end_date']));?></td>
  </tr>
  <tr>
    <th>End Time</th>
    <td><?echo $rota['end_time'];?></td>
  </tr>
  <?if(view_info_type($id) != 'bid'){?>
  <tr>
    <th>Account Name</th>
    <td><?echo $account['name'];?></td>
  </tr>
  <tr>
    <th>Role</th>
    <td><?echo $role['title'];?></td>
  </tr>
  <tr>
    <th>Location</th>
    <td><?echo $rota['location'];?></td>
  </tr>
  <tr>
    <th>Address</th>
    <td>
      <?if(!empty($service['address1'])){echo $service['address1'];}?>
      <?if(!empty($service['address2'])){echo ','.$service['address2'];}?>
      <?if(!empty($service['address3'])){echo ','.$service['address3'];}?>
      <?if(!empty($service['city'])){echo ','.$service['city'];}?>
      <?if(!empty($service['county'])){echo ','.$service['county'];}?>
      <?if(!empty($service['postcode'])){echo ','.$service['postcode'];}?>
    </td>
  </tr>
  <?}else{?>
    <tr>
      <th>Service</th>
      <td>
        <?echo $service['title'];?>
      </td>
    </tr>
    <tr><th></th><td> </td></tr>
    <tr>
      <th>
        Role
      </th>
      <th>
        Amount
      </th>
    </tr>
    <?foreach ($roles as $r) {?>
      <tr>
        <th><?echo $r['title'];?></th>
        <td><?echo $r['amount'];?></td>
      </tr>
    <?}?>
  <?}?>
  <?if($rota['dna'] == 1){?>
  <tr>
    <th colspan="2" style="border-right:0px;">Did Not Attend</th>
  </tr>
  <tr>
    <th>Date</th>
    <td><?echo date('d-m-Y H:i',$rota['dna_date']);?></td>
  </tr>
  <tr>
    <th>Reason</th>
    <td><?echo $rota['dna_reason'];?></td>
  </tr>
  <?}?>
</table>


<?if(view_info_type($id) == 'rota'){?>
  <?if($rota['dna'] == 0){?>
    <button type="button" class="btn btn-warning btn-raised btn-sm" id="did_not_attend" data-id="<?echo $id;?>">Did not Attend</button>
  <?}?>
  <?if($rota['dna'] == 1){?>
    <button type="button" class="btn btn-success btn-raised btn-sm" id="did_attend" data-id="<?echo $id;?>">Did Attend</button>
  <?}?>
<?}?>
