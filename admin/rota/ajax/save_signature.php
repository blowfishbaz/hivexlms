<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$id = $_POST['id'];
$signature = $_POST['signature'];

list($type, $signature) = explode(';', $signature);
list(, $signature)      = explode(',', $signature);
$imgs_customer = base64_decode($signature);

$upload_id = createid('img');

$name_customer = $upload_id.'.png';

$path = 'uploads/users/'.$id.'/signature/'.$upload_id.'/';

if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$path)){
   mkdir($_SERVER['DOCUMENT_ROOT'].'/'.$path, 0777, true);
}else{}

file_put_contents($_SERVER['DOCUMENT_ROOT'].'/'.$path.$name_customer, $imgs_customer);


$db->Query("insert into ws_accounts_upload (id,account_id, type, name, path, created_date, created_by) values (?,?,?,?,?,?,?)");
$db->bind(1,$upload_id);
$db->bind(2,$id);
$db->bind(3,'signature');
$db->bind(4,$name_customer);
$db->bind(5,$path);
$db->bind(6,time());
$db->bind(7,$id);
$db->execute();

echo 'ok';
