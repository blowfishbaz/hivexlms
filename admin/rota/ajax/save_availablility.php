<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 function day_to_number($day){
   switch ($day) {
     case 'monday': return '1'; break; case 'tuesday': return '2'; break; case 'wednesday': return '3'; break; case 'thursday': return '4'; break; case 'friday': return '5'; break; case 'saturday': return '6'; break; case 'sunday': return 7; break;
   }
 }



 $account_id = $_POST['account_id'];
 $week_num = $_POST['week_comm_date'];
 $year = $_POST['year'];

 $db->Query("update ws_availability set status = 0 where account_id = ? and week_number = ?");
 $db->bind(1,$account_id);
 $db->bind(2,$week_num);
 $db->execute();

 foreach ($_POST['days_available'] as $key => $days_available) {

    $db->Query("insert into ws_availability (id, account_id, week_number, day, start_time, end_time, start_ts, end_ts) values (?,?,?,?,?,?,?,?)");
    $db->bind(1,createid('avail'));
    $db->bind(2,$account_id);
    $db->bind(3,$week_num);
    $db->bind(4,$days_available);
    $db->bind(5,$_POST['start_time_'.$days_available.'']);
    $db->bind(6,$_POST['end_time_'.$days_available.'']);
    $db->bind(7, strtotime(date('d-m-Y '.$_POST['start_time_'.$days_available.''].'',strtotime($year.'W'.$week_num.day_to_number($days_available)))));
    $db->bind(8, strtotime(date('d-m-Y '.$_POST['end_time_'.$days_available.''].'',strtotime($year.'W'.$week_num.day_to_number($days_available)))));
    $db->execute();



 }

 echo 'ok';
