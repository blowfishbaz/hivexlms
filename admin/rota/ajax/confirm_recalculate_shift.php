<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$bid_id = $_POST['bid_id'];
//$bid_id = 'id_bid_20210429_102550371400_94001';

$db->query("select * from ws_rota_bid where id = ?");
$db->bind(1,$bid_id);
$data = $db->single();

$db->query("select * from ws_service where id = ?");
$db->bind(1,$data['service_id']);
$service = $db->single();

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1 and completed = 0");
$db->bind(1,$bid_id);
$bid_req_info_all = $db->resultset();

$id_already = '("0",';


foreach ($bid_req_info_all as $bria) {
  $db->query("select * from ws_rota_bid_request where pid = ? and service_role_id = ?");
  $db->bind(1,$bid_id);
  $db->bind(2,$bria['service_role_id']);
  $bid_req_all = $db->resultset();

  foreach ($bid_req_all as $request) {
    //Send Email
    $db->query("select * from accounts where id = ?");
    $db->bind(1,$request['account_id']);
    $user = $db->single();

    $id_already .='"'.$request['account_id'].'",';

  }
}

$id_already .= '"0")';

//echo $id_already;
$location_id = $service['location'];


foreach ($bid_req_info_all as $bria) {
  //need the location id
  //Need the job role id
  $job_role = $bria['role_id'];


  $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id, ws_accounts.status
  from ws_accounts_locations
  left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
  left join ws_accounts on ws_accounts.id = ws_accounts_locations.account_id
  where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1 and ws_accounts.status = 1 and ws_accounts.id not in $id_already");
  $db->bind(1,$location_id);
  $db->bind(2,$job_role);
  $list = $db->ResultSet();

  foreach ($list as $l) {
    //echo 'insert new';
    $db->query("select * from ws_service_roles where pid = ? and job_role = ? and status = 1");
    $db->bind(1,$service['id']);
    $db->bind(2,$l['role_id']);
    $service_role = $db->single();

    $bid_request_id = createid('bid_rq');
    $db->Query("insert into ws_rota_bid_request (id, pid, account_id, created_date, created_by, role_id, bid_amount_pid, service_role_id, service_id) values (?,?,?,?,?,?,?,?,?)");
    $db->bind(1,$bid_request_id);
    $db->bind(2,$bid_id);
    $db->bind(3,$l['account_id']);
    $db->bind(4,$now);
    $db->bind(5,$created_by);
    $db->bind(6,$l['role_id']);
    $db->bind(7,$bria['id']);
    $db->bind(8,$bria['service_role_id']);//Service Role Id
    $db->bind(9,$service['id']); //Service ID
    $db->execute();

    $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status, pid) values (?,?,?,?,?,?,?,?,?)");
    $db->bind(1,createid('not'));
    $db->bind(2,$l['account_id']);
    $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $db->bind(4,time());
    $db->bind(5,'New Rota Shift');
    $db->bind(6,'A New Rota Shift has become avaiable, click on the notification to view');
    $db->bind(7,'new_shift');
    $db->bind(8,'1');
    $db->bind(9,$bid_request_id);
    $db->execute();

    $db->Query("update ws_rota_bid set email_sent = 0 where id = ?");
    $db->bind(1,$bid_id);
    //$db->execute();

    //echo 'On New Shift';
  }


}

echo 'ok';
