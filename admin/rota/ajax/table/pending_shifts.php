<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'ws_rota_bid_request.id';
	$order = 'desc';
}


//where ws_rota_bid_request.account_id = ? and accepted_admin = ?

$db->query("select ws_rota_bid_request.id, ws_roles.title as role_title, ws_service.title as service_title, ws_service.location as service_location, ws_rota_bid.start_date, ws_rota_bid.start_time, ws_rota_bid_request.account_id
from ws_rota_bid_request
join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
join ws_service on ws_rota_bid_request.service_id = ws_service.id
join ws_rota_bid on ws_rota_bid.id = ws_rota_bid_request.pid
where ws_rota_bid_request.account_id = ? and accepted_admin = ?");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(2,0);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select ws_rota_bid_request.id, ws_roles.title as role_title, ws_service.title as service_title, ws_service.location as service_location, ws_rota_bid.start_date, ws_rota_bid.start_time, ws_rota_bid_request.account_id
from ws_rota_bid_request
join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
join ws_service on ws_rota_bid_request.service_id = ws_service.id
join ws_rota_bid on ws_rota_bid.id = ws_rota_bid_request.pid
where ws_rota_bid_request.account_id = ? and accepted_admin = ? order by $sort $order LIMIT ? offset ?");
$db->bind(1,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->bind(2,0);
$db->bind(3,(int) $limit);
$db->bind(4,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
