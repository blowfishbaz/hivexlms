<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'name';
	$order = 'desc';
}


$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and (select count(id) as counter from accounts_service_area where pid = accounts.id and area in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")) > 0';

  $subadmin_query_prac = sub_admin_query('practice_id');
  $subadmin_query_pcn = sub_admin_query('pcn_id');

  if(!empty($subadmin_query_prac) && !empty($subadmin_query_pcn)){
    $subadmin_search .= " and ((select count(id) as counter from accounts_sub_pcn where account_id = accounts.id and $subadmin_query_pcn) > 0";
    $subadmin_search .= " or (select count(id) as counter from accounts_sub_practice where account_id = accounts.id and $subadmin_query_prac) > 0)";
  }

}

$db->query("select * from accounts where status = 1 and account_type != 'user' and user_status = 1 and (name like ? or email like ?) $subadmin_search");
$db->bind(1,$search);
$db->bind(2,$search);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select * from accounts where status = 1 and account_type != 'user' and user_status = 1 and (name like ? or email like ?) $subadmin_search order by $sort $order LIMIT ? offset ?");
$db->bind(1,$search);
$db->bind(2,$search);
$db->bind(3,(int) $limit);
$db->bind(4,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
