<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'name';
	$order = 'asc';
}


$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and (select count(id) as counter from ws_accounts_locations where account_id = accounts.id and location_id in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")) > 0';

}

$location_search = '';

if($_GET['location'] == 'All'){
	$location_search = '';
}else{
	$location_search = ' and (select count(id) from ws_accounts_locations where account_id = accounts.id and status = 1 and location_id = "'.$_GET['location'].'")';
}

$db->query("select ws_accounts.*, ws_accounts_extra_info.*, accounts.name, ws_accounts.created_date as created_d, accounts.id as acc_id
from accounts
left join ws_accounts on ws_accounts.id = accounts.id
left join ws_accounts_extra_info on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
where accounts.account_type = ? and user_status = '1' and ws_accounts.rota = 1 and (accounts.name like ? or accounts.email like?) $subadmin_search $location_search");
$db->bind(1,'user');
$db->bind(2,$search);
$db->bind(3,$search);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select ws_accounts.*, ws_accounts_extra_info.*, accounts.name, ws_accounts.created_date as created_d, accounts.id as acc_id
from accounts
left join ws_accounts on ws_accounts.id = accounts.id
left join ws_accounts_extra_info on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
where accounts.account_type = ? and user_status = '1' and ws_accounts.rota = 1 and (accounts.name like ? or accounts.email like?) $subadmin_search $location_search order by $sort $order LIMIT ? offset ?");
$db->bind(1,'user');
$db->bind(2,$search);
$db->bind(3,$search);
$db->bind(4,(int) $limit);
$db->bind(5,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
