<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'title';
	$order = 'desc';
}

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and location in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")';

	$prac_search = sub_admin_query('pcn_id');
	$pcn_search = sub_admin_query('practice_id','or');

	if(!empty($prac_search) && !empty($pcn_search)){
		$subadmin_search .=" and ($prac_search $pcn_search)";
	}


}

$db->query("select * from ws_service where status = ? and (title like ? or location like ?) $subadmin_search ");
$db->bind(1,'1');
$db->bind(2,$search);
$db->bind(3,$search);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select * from ws_service where status = ? and (title like ? or location like ?)  $subadmin_search  order by $sort $order LIMIT ? offset ?");
$db->bind(1,'1');
$db->bind(2,$search);
$db->bind(3,$search);
$db->bind(4,(int) $limit);
$db->bind(5,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
