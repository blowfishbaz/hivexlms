<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'id';
	$order = 'desc';
}

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and location in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")';
}

$db->query("select * from ws_pcn where status = ? and (pcn_title like ?) ");
$db->bind(1,'1');
$db->bind(2,$search);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select * from ws_pcn where status = ? and (pcn_title like ?)  order by $sort $order LIMIT ? offset ?");
$db->bind(1,'1');
$db->bind(2,$search);
$db->bind(3,(int) $limit);
$db->bind(4,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
