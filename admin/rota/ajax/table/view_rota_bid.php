<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'id';
	$order = 'desc';
}

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and location in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")';

  $prac_search = sub_admin_query('rb.pcn_id');
  $pcn_search = sub_admin_query('rb.practice_id','or');

  if(!empty($prac_search) && !empty($pcn_search)){
    $subadmin_search .=" and ($prac_search $pcn_search)";
  }
}


$db->query("select rb.*, ws_service.title, ws_service.location, (select COALESCE(sum(seen),0) from ws_rota_bid_request where ws_rota_bid_request.pid = rb.id and accepted_user = 1) as accepted
from ws_rota_bid rb
left join ws_service on ws_service.id = rb.service_id
where rb.status = ? and (ws_service.title like ? or ws_service.location like ?) $subadmin_search");
$db->bind(1,'1');
$db->bind(2,$search);
$db->bind(3,$search);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select rb.*, ws_service.title, ws_service.location, (select COALESCE(sum(seen),0) from ws_rota_bid_request where ws_rota_bid_request.pid = rb.id and accepted_user = 1) as accepted
from ws_rota_bid rb
left join ws_service on ws_service.id = rb.service_id
where rb.status = ?  and (ws_service.title like ? or ws_service.location like ?) $subadmin_search order by $sort $order LIMIT ? offset ?");
$db->bind(1,'1');
$db->bind(2,$search);
$db->bind(3,$search);
$db->bind(4,(int) $limit);
$db->bind(5,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
