<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'name';
	$order = 'desc';
}

$subadmin_search = '';
if(my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID'])) == 'subadmin'){
	$subadmin_search = ' and (select count(id) as counter from ws_accounts_locations where account_id = accounts.id and location_id in ("0",';
  $my_areas = subadmin_areas(decrypt($_SESSION['SESS_ACCOUNT_ID']));

  foreach ($my_areas as $ma) {
    $subadmin_search .='"'.$ma.'",';
  }

  $subadmin_search .= '"0")) > 0';
}


$db->query("select * from accounts where account_type = ? and (user_status = ? or user_status = ?) and (name like ? or email like ?) $subadmin_search");
$db->bind(1,'user');
$db->bind(2,'2');
$db->bind(3,'3');
$db->bind(4,$search);
$db->bind(5,$search);
$db->execute();
$rowcount = $db->rowcount();


$db->query("select * from accounts where account_type = ? and (user_status = ? or user_status = ?) and (name like ? or email like ?) $subadmin_search order by $sort $order LIMIT ? offset ?");
$db->bind(1,'user');
$db->bind(2,'2');
$db->bind(3,'3');
$db->bind(4,$search);
$db->bind(5,$search);
$db->bind(6,(int) $limit);
$db->bind(7,(int) $offset);
$data = $db->ResultSet();


?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
