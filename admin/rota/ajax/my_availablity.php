<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    contacts/contact_view/calendar/load_calendar_appointments.php
 * @author    Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];


 $db->query("select * from ws_availability where account_id = ? and week_number = ? and status = 1 ORDER BY CASE WHEN day = 'monday' THEN 1 WHEN day = 'tuesday' THEN 2 WHEN day = 'wednesday' THEN 3 WHEN day = 'thursday' THEN 4 WHEN day = 'friday' THEN 5 WHEN day = 'saturday' THEN 6 WHEN day = 'sunday' THEN 7 END ASC");
 $db->bind(1,$id);
 $db->bind(2,date('W', strtotime($_GET['start'])));
 $data = $db->resultset();

  ?>
  [
   <? foreach($data as $d) {

        ?>

     {
            "id": "",
           "title": "My Availablity",
           "start": "<? echo date('c', $d['start_ts']);?>",
           "end": "<? echo date('c', $d['end_ts']);?>"
     },
     <? } ?>
      {
     "title": "",
     "start": "2150-06-28"
   }
    ]
