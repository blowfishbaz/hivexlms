<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');
use PHPMailer\PHPMailer\PHPMailer;
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/Exception.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/SMTP.php');

$bid_id = $_POST['bid_id'];

$db->Query("update ws_rota_bid set completed = 0 where id = ?");
$db->bind(1,$bid_id);
//$db->execute();

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1 and completed = 0");
$db->bind(1,$bid_id);
$bid_req_info_all = $db->resultset();

$db->query("select * from ws_rota_bid where id = ?");
$db->bind(1,$bid_id);
$data = $db->single();

foreach ($bid_req_info_all as $bria) {
  $db->query("select * from ws_rota_bid_request where pid = ? and service_role_id = ? and accepted_user = 0");
  $db->bind(1,$bid_id);
  $db->bind(2,$bria['service_role_id']);
  $bid_req_all = $db->resultset();

  foreach ($bid_req_all as $request) {
    //Send Email
    $db->query("select * from accounts where id = ?");
    $db->bind(1,$request['account_id']);
    $user = $db->single();

    $db->query("select ws_service_roles.*, ws_roles.title as role_title, ws_service.title as service_title, ws_service.location
                from ws_service_roles
                left join ws_roles on ws_roles.id = ws_service_roles.job_role
                left join ws_service on ws_service.id = ws_service_roles.pid
                where ws_service_roles.id = ?");
    $db->bind(1,$request['service_role_id']);
    $info = $db->single();

    //echo $request['service_role_id'].' '.$user['name'].'<br />';


    $subject='Rota Session Bid';

    $email_title = 'Rota Session Bid';
    $main_body_text = 'Hi '.ucfirst ($user['name']).' A shift has been published. Please see below and respond to the shift invite.';
    $head='';
    $body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #fff;margin-top: -18px;"> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> <!--<![endif]--> <div class="wrapper"> <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""> <tbody> <tr> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 100vw;"> <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#dedede;" align="center"> </div> <div style="Margin-top: 50px;"> <div style="line-height:20px;text-align:center;padding: 5px;background-color: #edaac6;color: white;"> <h1 style="margin:40px 0px; line-height:normal; font-size:55px;">'.$email_title.'</h1> </div> </div> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> </tbody> </table>';

    $body2 = '<table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 80vw;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;"> '.$main_body_text.'</p><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 75px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div></td></tr></tbody></table>';



    $body3='
    <table style="width:80%; margin:0 auto; display:table;">
    <tr>
      <th style="border-bottom:2px solid #eee; border-right:2px solid #eee;">Service</th>
      <th style="border-bottom:2px solid #eee; border-right:2px solid #eee;">Location</th>
      <th style="border-bottom:2px solid #eee; border-right:2px solid #eee;">Role</th>
      <th style="border-bottom:2px solid #eee; border-right:2px solid #eee;">Date / Time</th>
      <th style="border-bottom:2px solid #eee;">View</th>
    </tr>';
          $body3 .= '<tr>
            <td style="border-bottom:2px solid #eee; border-right:2px solid #eee; color:#46bbc7;">'.$info['service_title'].'</td>
            <td style="border-bottom:2px solid #eee; border-right:2px solid #eee; color:#46bbc7;">'.$info['location'].'</td>
            <td style="border-bottom:2px solid #eee; border-right:2px solid #eee; color:#46bbc7;">'.$info['role_title'].'</td>
            <td style="border-bottom:2px solid #eee; border-right:2px solid #eee; color:#46bbc7;">'.date('d-m-Y',$us['start_final']).' '.$us['start_time'].'</td>
            <td style="border-bottom:2px solid #eee;"><a href="'.$fullurl.'admin/rota/shift_response.php?ref='.encrypt($us['request_id'].'-'.$user['id']).'">View Session</a></td>
          </tr>';
    $body3 .=' </table>';

    $footer='<!-- Callout Panel --> <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede; width:100vw;" align="center" emb-background-style=""> <tbody> <tr> <td colspan="3"> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 15px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> <tr> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <a href="tel:0151 294 3322"><img src="http://staff-box.co.uk/assets/images/emails/IconPhone.png" style="margin-left:20px;margin-right:40px;" /></a> <a href="mailto:info@onewirral.co.uk"><img src="http://staff-box.co.uk/assets/images/emails/IconEmail.png" style="margin-left:20px;margin-right:40px;" /></a> <a href="https://goo.gl/maps/BRrWziseA1VgmjVB7"><img src="http://staff-box.co.uk/assets/images/emails/IconLocation_cc.png" style="margin-left:20px;margin-right:40px;" /></a> </td> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <p class="size-16" style="Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;">&copy; Staffbox Copyright '.date("Y").'</p><!-- Callout Panel --> </td> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <img src="http://staff-box.co.uk/assets/images/emails/Staff_Box_Logo.png" style="float:right; margin-right:20px;" /> </td> </tr> <tr> <td colspan="3"> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 15px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> </tbody> </table></div></body>';

    $htmlContent = $head.$body1.$body2.$body3.$footer;
    /////////////send html mail


    $mail = new PHPMailer();
    $mail->SMTPDebug = 0;
    $mail->isSMTP();
    $mail->Host = 'smtp.office365.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'rota@staff-box.co.uk'; //paste one generated by Mailtrap
    $mail->Password = 'Lux03898'; //paste one generated by Mailtrap
    $mail->Port = 587;
    $mail->setFrom('rota@staff-box.co.uk', 'Rota');
    $mail->addAddress($user['email'], $user['name']);
    //$mail->addBCC('josh@blowfishtechnology.com', 'Josh McCarthy');
    $mail->Subject = $subject;
    $mail->isHTML(true);
    $mail->Body = $htmlContent;

    // echo $user['email'].'<br />';
    //
    // error_log('*************************');
    // error_log('*************************');
    // error_log('*************************');
    //
    // error_log($user['email']);

    // if($mail->send()){
    //     // echo 'Message has been sent';
    // }else{
    //     // echo 'Message could not be sent.';
    //     // echo 'Mailer Error: ' . $mail->ErrorInfo;
    // }
  }
}

echo 'ok';
