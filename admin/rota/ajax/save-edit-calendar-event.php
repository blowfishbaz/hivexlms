<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$id = $_POST['id'];
$bid_id = $_POST['bid_id'];
$type = $_POST['type'];

$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$start_time = $_POST['start_time'];
$end_time = $_POST['end_time'];

$start_date = date('Y-m-d',strtotime($start_date));
$end_date = date('Y-m-d',strtotime($end_date));

if($type == 'bid'){
  $db->query("select * from ws_rota_bid_amount where id = ?");
  $db->bind(1,$id);
  $rota_amount = $db->single();

  $db->query("select * from ws_rota_bid where id = ?");
  $db->bind(1,$rota_amount['pid']);
  $rota = $db->single();

  $db->query("select * from ws_service_roles where id = ?");
  $db->bind(1,$rota_amount['service_role_id']);
  $service_role = $db->single();

  $amount_needed = $_POST['amount_needed'];
  $payment_amount = $_POST['pay_needed'];
  $assign_roles = $_POST['assign_to'];

  $db->Query("update ws_rota_bid set modified_date = ?, modified_by = ?, start_date = ?, end_date = ?, start_time = ?, end_time = ?, start_final = ?, end_final = ? where id = ?");
  $db->bind(1,$now);
  $db->bind(2,$created_by);
  $db->bind(3,$start_date);
  $db->bind(4,$end_date);
  $db->bind(5,$start_time);
  $db->bind(6,$end_time);
  $db->bind(7,strtotime($start_date.' '.$start_time));
  $db->bind(8,strtotime($end_date.' '.$end_time));
  $db->bind(9,$bid_id);
  $db->execute();


  $db->Query("update ws_rota_bid_amount set modified_date = ?, modified_by = ?, amount = ? where id = ?");
  $db->bind(1,$now);
  $db->bind(2,$created_by);
  $db->bind(3,$amount_needed);
  $db->bind(4,$id);
  $db->execute();

  if($service_role['hourly_rate'] != $payment_amount){
    // $db->Query("update ws_rota_bid_amount set modified_pay = ? where id = ?");
    // $db->bind(1,$payment_amount);
    // $db->bind(2,$id);
    // $db->execute();
  }

  $db->query("select * from ws_rota_bid where id = ?");
  $db->bind(1,$rota_amount['pid']);
  $bid_info = $db->single();

  $db->query("select * from ws_service where id = ?");
  $db->bind(1,$bid_info['service_id']);
  $service = $db->single();

  foreach ($assign_roles as $key => $ar) {
    //Update the current bid request to be accepted for both

    //We should check if the rota bid is a reoccuring - Waiting
    //If reoccuring then we should make it a live one
    //Actually we would have to make it live, then we would have to select who it would be.

    $db->query("select * from ws_rota_bid_request where account_id = ? and pid = ? and status = 1 and bid_amount_pid = ?");
    $db->bind(1,$ar);
    $db->bind(2,$bid_id);
    $db->bind(3,$id);
    $rota_request = $db->single();


    $db->Query("update ws_rota_bid_request set seen = ?, seen_date = ?, accepted_user = ?, choice_date = ?, accepted_admin = ?, choice_date_admin = ?, accepted_admin_id = ? where id = ?");
    $db->bind(1,1);
    $db->bind(2,$now);
    $db->bind(3,1);
    $db->bind(4,$now);
    $db->bind(5,1);
    $db->bind(6,$now);
    $db->bind(7,$created_by);
    $db->bind(8,$rota_request['id']);
    $db->execute();

    $thisSaveId = createid('sv');
    $thisId = createid('shift');
    $thisMemberId = $ar;
    $thisHome = 'N/A';
    $thisStartDate = date('d-m-Y',strtotime($bid_info['start_date'])); //d-m-Y
    $thisStartTime = $bid_info['start_time']; //H:i
    $thisEndDate = date('d-m-Y',strtotime($bid_info['end_date'])); //d-m-Y
    $thisEndTime = $bid_info['end_time']; //H:i
    $thisStart_ts = $bid_info['start_final'];
    $thisEnd_ts = $bid_info['end_final'];
    $thisWeekNum = date('W',strtotime($bid_info['start_date']));
    $thisYear = date('Y',strtotime($bid_info['start_date']));
    $thisType = 'in_day';
    $role = $rota_request['role_id'];
    $location = $service['location'];
    $bid_request_id = $rota_request['id'];
    $service_role_id = $rota_request['service_role_id'];

    $db->query("insert into hx_rota (save_id,id,member_id,home_id,start_date,start_time,end_date,end_time,start_ts,end_ts,week_num,year,type,role,location,service_id,bid_id,bid_request_id,service_role_id,pcn_id, practice_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $db->bind(1, $thisSaveId);
    $db->bind(2, $thisId);
    $db->bind(3, $thisMemberId);
    $db->bind(4, $thisHome);
    $db->bind(5, $thisStartDate);
    $db->bind(6, $thisStartTime);
    $db->bind(7, $thisEndDate);
    $db->bind(8, $thisEndTime);
    $db->bind(9, $thisStart_ts);
    $db->bind(10, $thisEnd_ts);
    $db->bind(11, $thisWeekNum);
    $db->bind(12, $thisYear);
    $db->bind(13, $thisType);
    $db->bind(14, $role);
    $db->bind(15, $location);
    $db->bind(16, $service['id']);//service id
    $db->bind(17, $bid_id);
    $db->bind(18, $bid_request_id);
    $db->bind(19, $service_role_id);
    $db->bind(20,$service['pcn_id']);
    $db->bind(21,$service['practice_id']);
    $db->execute();


    //Then add onto the rota
  }

  $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
  $db->bind(1,$bid_id);
  $bid_amount_info = $db->resultset();

  foreach ($bid_amount_info as $ba) {
    $amount = $ba['amount'];
    $ba_id = $ba['id'];

    $check_accepted = 0;
    $check_declined = 0;
    $check_outstanding = 0;

    //Then get all the ones who have accepted this role?
    $db->query("select * from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and status = 1");
    $db->bind(1,$bid_id);
    $db->bind(2,$ba['id']);
    $bid_req_info = $db->resultset();

    foreach ($bid_req_info as $bri) {
      if($bri['accepted_admin'] == 1){
        $check_accepted++;
      }else if($bri['accepted_admin'] == 2){
        $check_declined++;
      }else if($bri['accepted_admin'] == 0){
        $check_outstanding++;
      }
    }
    if($amount <= $check_accepted){
      //Then we can completed this one

      $db->Query("update ws_rota_bid_amount set completed = ? where id = ?");
      $db->bind(1,'1');
      $db->bind(2,$ba_id);
      $db->execute();

    }
  }

  $db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
  $db->bind(1,$bid_id);
  $bid_amount_check = $db->resultset();

  $check_completed_bac = 0;
  $check_outstanding_bac = 0;

  foreach ($bid_amount_check as $bac) {
    if($bac['completed'] == 1){
      $check_completed_bac = 1;
    }else if($bac['completed'] == 0){
      $check_outstanding_bac = 1;
    }
  }

  if($check_completed_bac == 1 && $check_outstanding_bac == 0){
    $db->Query("update ws_rota_bid set completed = ? where id = ?");
    $db->bind(1,'1');
    $db->bind(2,$bid_id);
    $db->execute();
  }

}else if($type == 'rota_oustanding' || $type == 'rota_complete'){
  $db->Query("update hx_rota set start_date = ?, end_date = ?, start_time = ?, end_time = ?, start_ts = ?, end_ts = ?, member_id = ? where id = ?");
  $db->bind(1,date('d-m-Y',strtotime($start_date)));
  $db->bind(2,date('d-m-Y',strtotime($end_date)));
  $db->bind(3,$start_time);
  $db->bind(4,$end_time);
  $db->bind(5,strtotime($start_date.' '.$start_time));
  $db->bind(6,strtotime($end_date.' '.$end_time));
  $db->bind(7,$_POST['assign_to']);
  $db->bind(8,$bid_id);
  $db->execute();
}


echo 'ok';
