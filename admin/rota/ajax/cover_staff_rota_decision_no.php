<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$dateNow=time();

$db->query("SELECT * from hx_rota where id = ?");
$db->bind(1,$_GET['id']);
$shift = $db->single();


$db->query("SELECT * FROM accounts WHERE id = ?");
$db->bind(1,$_GET['user']);
$staff = $db->single();

$new_id = createid('rcov');

$db->query("insert into hx_rota_cover (id,pid,member_id,decision,covered,created_date,created_by) values (?,?,?,?,?,?,?)");
$db->bind(1,$new_id);
$db->bind(2,$shift['id']);
$db->bind(3,$staff['id']);
$db->bind(4,'no');
$db->bind(5,$staff['covered']);
$db->bind(6,$dateNow);
$db->bind(7,decrypt($_SESSION['SESS_ACCOUNT_ID']));
$db->execute();

$db->query("update accounts set covered = ? where id = ?");
$db->bind(1,'0');
$db->bind(2,$staff['id']);
$db->execute();


?>
