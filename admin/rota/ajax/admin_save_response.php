<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');
use PHPMailer\PHPMailer\PHPMailer;
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/Exception.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/SMTP.php');

$id = $_POST['pid'];
$type = $_POST['type'];
$bid_id = $_POST['bid_id'];

$db->query("select * from ws_rota_bid where id = ?");
$db->bind(1,$bid_id);
$bid_info = $db->single();

$db->query("select * from ws_rota_bid_request where pid = ? and status = 1");
$db->bind(1,$bid_id);
$bid_req_info_all = $db->resultset();

$db->query("select * from ws_service where id = ?");
$db->bind(1,$bid_info['service_id']);
$service = $db->single();

$db->query("select ws_service_roles.*, ws_roles.title from ws_service_roles left join ws_roles on ws_roles.id = ws_service_roles.job_role where ws_service_roles.pid = ?");
$db->bind(1,$bid_info['service_id']);
$service_roles = $db->resultset();

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
$db->bind(1,$bid_id);
$bid_amount_info = $db->resultset();

$db->query("select * from ws_rota_bid_request where id = ?");
$db->bind(1,$id);
$my_rota_bid = $db->single();


if($type == 'accept'){

  $db->Query("update ws_rota_bid_request set accepted_admin = ?, choice_date_admin = ?, accepted_admin_id = ? where id = ?");
  $db->bind(1,'1');
  $db->bind(2,time());
  $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $db->bind(4,$id);
  $db->execute();

  //Send out the email to say you are on that shift

  //Add into the rota table to show this.
  $thisSaveId = createid('sv');
  $thisId = createid('shift');
  $thisMemberId = $my_rota_bid['account_id'];
  $thisHome = 'N/A';
  $thisStartDate = date('d-m-Y',strtotime($bid_info['start_date'])); //d-m-Y
  $thisStartTime = $bid_info['start_time']; //H:i
  $thisEndDate = date('d-m-Y',strtotime($bid_info['end_date'])); //d-m-Y
  $thisEndTime = $bid_info['end_time']; //H:i
  $thisStart_ts = $bid_info['start_final'];
  $thisEnd_ts = $bid_info['end_final'];
  $thisWeekNum = date('W',strtotime($bid_info['start_date']));
  $thisYear = date('Y',strtotime($bid_info['start_date']));
  $thisType = 'in_day';
  $role = $my_rota_bid['role_id'];
  $location = $service['location'];
  $bid_request_id = $id;
  $service_role_id = $my_rota_bid['service_role_id'];

  $db->query("insert into hx_rota (save_id,id,member_id,home_id,start_date,start_time,end_date,end_time,start_ts,end_ts,week_num,year,type,role,location,service_id,bid_id,bid_request_id,service_role_id,pcn_id, practice_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
  $db->bind(1, $thisSaveId);
  $db->bind(2, $thisId);
  $db->bind(3, $thisMemberId);
  $db->bind(4, $thisHome);
  $db->bind(5, $thisStartDate);
  $db->bind(6, $thisStartTime);
  $db->bind(7, $thisEndDate);
  $db->bind(8, $thisEndTime);
  $db->bind(9, $thisStart_ts);
  $db->bind(10, $thisEnd_ts);
  $db->bind(11, $thisWeekNum);
  $db->bind(12, $thisYear);
  $db->bind(13, $thisType);
  $db->bind(14, $role);
  $db->bind(15, $location);
  $db->bind(16, $service['id']);//service id
  $db->bind(17, $bid_id);
  $db->bind(18, $bid_request_id);
  $db->bind(19, $service_role_id);
  $db->bind(20,$service['pcn_id']);
  $db->bind(21,$service['practice_id']);
  $db->execute();


  $db->query("select * from accounts where id = ? ");
  $db->bind(1,$thisMemberId);
  $user = $db->single();

  $subject='Shift Bid - Sucessful';
  $email_title = 'Shift Bid Response';
  $main_body_text = 'Hi '.ucfirst ($user['name']).'<br />You have been successful on your Shift Bid.';
  $head='';
  $body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #fff;margin-top: -18px;"> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> <!--<![endif]--> <div class="wrapper"> <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""> <tbody> <tr> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 100vw;"> <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#dedede;" align="center"> </div> <div style="Margin-top: 50px;"> <div style="line-height:20px;text-align:center;padding: 5px;background-color: #edaac6;color: white;"> <h1 style="margin:40px 0px; line-height:normal; font-size:55px;">'.$email_title.'</h1> </div> </div> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> </tbody> </table>';

  $body2 = '<table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 80vw;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;"> '.$main_body_text.'</p><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 75px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div></td></tr></tbody></table>';

  $body3 = '';

  $footer='<!-- Callout Panel --> <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede; width:100vw;" align="center" emb-background-style=""> <tbody> <tr> <td colspan="3"> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 15px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> <tr> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <a href="tel:0151 294 3322"><img src="http://staff-box.co.uk/assets/images/emails/IconPhone.png" style="margin-left:20px;margin-right:40px;" /></a> <a href="mailto:info@onewirral.co.uk"><img src="http://staff-box.co.uk/assets/images/emails/IconEmail.png" style="margin-left:20px;margin-right:40px;" /></a> <a href="https://goo.gl/maps/BRrWziseA1VgmjVB7"><img src="http://staff-box.co.uk/assets/images/emails/IconLocation_cc.png" style="margin-left:20px;margin-right:40px;" /></a> </td> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <p class="size-16" style="Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;">&copy; Staffbox Copyright '.date("Y").'</p><!-- Callout Panel --> </td> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <img src="http://staff-box.co.uk/assets/images/emails/Staff_Box_Logo.png" style="float:right; margin-right:20px;" /> </td> </tr> <tr> <td colspan="3"> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 15px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> </tbody> </table></div></body>';


  $htmlContent = $head.$body1.$body2.$body3.$footer;
  /////////////send html mail


  $mail = new PHPMailer();
  $mail->SMTPDebug = 0;
  $mail->isSMTP();
  $mail->Host = 'smtp.office365.com';
  $mail->SMTPAuth = true;
  $mail->Username = 'rota@staff-box.co.uk'; //paste one generated by Mailtrap
  $mail->Password = 'Lux03898'; //paste one generated by Mailtrap
  $mail->Port = 587;
  $mail->setFrom('rota@staff-box.co.uk', 'Rota');
  $mail->addAddress($user['email'], $user['name']);
  $mail->Subject = $subject;
  $mail->isHTML(true);
  $mail->Body = $htmlContent;

  if($mail->send()){
      // echo 'Message has been sent';
  }else{
      // echo 'Message could not be sent.';
      // echo 'Mailer Error: ' . $mail->ErrorInfo;
  }


}else if($type == 'decline'){

  $db->Query("update ws_rota_bid_request set accepted_admin = ?, choice_date_admin = ?, accepted_admin_id = ? where id = ?");
  $db->bind(1,'2');
  $db->bind(2,time());
  $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
  $db->bind(4,$id);
  $db->execute();

}



foreach ($bid_amount_info as $ba) {
  $amount = $ba['amount'];
  $role = $ba['role_id'];
  $ba_id = $ba['id'];

  $check_accepted = 0;
  $check_declined = 0;
  $check_outstanding = 0;

  //Then get all the ones who have accepted this role?
  $db->query("select * from ws_rota_bid_request where pid = ? and bid_amount_pid = ? and status = 1");
  $db->bind(1,$bid_id);
  $db->bind(2,$ba['id']);
  $bid_req_info = $db->resultset();

  foreach ($bid_req_info as $bri) {
    if($bri['accepted_admin'] == 1){
      $check_accepted++;
    }else if($bri['accepted_admin'] == 2){
      $check_declined++;
    }else if($bri['accepted_admin'] == 0){
      $check_outstanding++;
    }
  }
  if($amount <= $check_accepted){
    //Then we can completed this one
    $db->query("select * from ws_rota_bid_amount where id = ?");
    $db->bind(1,$ba['id']);
    $bid_am_checker = $db->single();


    $db->Query("update ws_rota_bid_amount set completed = ? where id = ?");
    $db->bind(1,'1');
    $db->bind(2,$ba_id);
    $db->execute();

  }
}

//We check whether the whole bid can be closed down now.

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
$db->bind(1,$bid_id);
$bid_amount_check = $db->resultset();

$check_completed_bac = 0;
$check_outstanding_bac = 0;

foreach ($bid_amount_check as $bac) {
  if($bac['completed'] == 1){
    $check_completed_bac = 1;
  }else if($bac['completed'] == 0){
    $check_outstanding_bac = 1;
  }
}

if($check_completed_bac == 1 && $check_outstanding_bac == 0){
  $db->Query("update ws_rota_bid set completed = ? where id = ?");
  $db->bind(1,'1');
  $db->bind(2,$bid_id);
  $db->execute();

  //The Notification should go in here.
  //Get all the users which that bid amount and that havn't been acceped

  //Loop through and send a notification out to them



    $db->query("select ws_rota_bid_request.account_id, ws_rota_bid.start_date, ws_rota_bid.start_time, ws_roles.title
                from ws_rota_bid_request
                left join ws_rota_bid on ws_rota_bid.id = ws_rota_bid_request.pid
                left join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
                where ws_rota_bid_request.pid = ? and ws_rota_bid_request.status = 1");
    $db->bind(1,$bid_id);
    $bid_req_no_noti = $db->resultset();

    foreach ($bid_req_no_noti as $brnn) {
      $message_title = 'Rota Bid - Position Filled';
      $notification = 'Hi, Thank you all for offering to work shifts at '.$service['title'].'.  We have had an overwhelming amount of response for most of the shifts so unfortunately you have not all been allocated all the shifts you applied for. <br />If you have not worked at this site before please arrive 15 minutes before your start time and make yourself know to a member of staff as you will need to have a site induction.  ';

      //To be confirmed

      $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status) values (?,?,?,?,?,?,?,?)");
      $db->bind(1,createid('not'));
      $db->bind(2,$brnn['account_id']);
      $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $db->bind(4,time());
      $db->bind(5,$message_title);
      $db->bind(6,$notification);
      $db->bind(7,'notification');
      $db->bind(8,'1');
      $db->execute();
    }

}


echo 'ok';
