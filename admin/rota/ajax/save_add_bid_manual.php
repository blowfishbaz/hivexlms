<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$id = $_POST['id'];
$service = $_POST['service'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$start_time = $_POST['start_time'];
$end_time = $_POST['end_time'];

$start_date = date('Y-m-d',strtotime($start_date));
$end_date = date('Y-m-d',strtotime($end_date));

$db->query("select id, location, pcn_id, practice_id from ws_service where id = ?");
$db->bind(1,$service);
$service_data = $db->single();

$db->query("select id, job_role, pid from ws_service_roles where pid = ? and status = 1");
$db->bind(1,$service);
$roles = $db->ResultSet();

$db->Query("insert into ws_rota_bid (id, created_date, created_by, service_id, start_date, end_date, start_time, end_time, start_final, end_final, completed, email_sent, practice_id, pcn_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,$id);
$db->bind(2,$now);
$db->bind(3,$created_by);
$db->bind(4,$service);
$db->bind(5,$start_date);
$db->bind(6,$end_date);
$db->bind(7,$start_time);
$db->bind(8,$end_time);
$db->bind(9,strtotime($start_date.' '.$start_time));
$db->bind(10,strtotime($end_date.' '.$end_time));
$db->bind(11,1);
$db->bind(12,1);
$db->bind(13,$service_data['practice_id']);
$db->bind(14,$service_data['pcn_id']);
$db->execute();



foreach ($roles as $r) {
  $bid_am = createid('bid_am');
  $db->Query("insert into ws_rota_bid_amount (id, created_date, created_by, pid, role_id, amount, service_role_id, completed) values (?,?,?,?,?,?,?,?)");
  $db->bind(1,$bid_am);
  $db->bind(2,$now);
  $db->bind(3,$created_by);
  $db->bind(4,$id);
  $db->bind(5,$r['job_role']);
  $db->bind(6,'0');
  $db->bind(7,$r['id']);
  $db->bind(8,1);
  $db->execute();

  foreach ($_POST['job_role_'.$r['id'].''] as $key => $user) {


    $thisSaveId = createid('sv');
    $thisId = createid('shift');
    $thisMemberId = $user;
    $thisHome = 'N/A';
    $thisStartDate = date('d-m-Y',strtotime($start_date)); //d-m-Y
    $thisStartTime = $start_time; //H:i
    $thisEndDate = date('d-m-Y',strtotime($end_date)); //d-m-Y
    $thisEndTime = $end_time; //H:i
    $thisStart_ts = strtotime($start_date.' '.$start_time);
    $thisEnd_ts = strtotime($end_date.' '.$end_time);
    $thisWeekNum = date('W',strtotime($start_date));
    $thisYear = date('Y',strtotime($start_date));
    $thisType = 'in_day';
    $role = $r['job_role'];
    $location = $service_data['location'];
    $bid_request_id = createid('bid_rq');

    $service_role_id = $r['id'];

    $db->Query("insert into ws_rota_bid_request (id, pid, account_id, created_date, created_by, role_id, bid_amount_pid, service_role_id, service_id, seen, seen_date, accepted_user, choice_date, accepted_admin, choice_date_admin, accepted_admin_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $db->bind(1,$bid_request_id);
    $db->bind(2,$id);
    $db->bind(3,$thisMemberId);
    $db->bind(4,$now);
    $db->bind(5,$created_by);
    $db->bind(6,$r['job_role']);
    $db->bind(7,$bid_am);
    $db->bind(8,$r['id']);
    $db->bind(9,$r['pid']);
    $db->bind(10,1);
    $db->bind(11,$now);
    $db->bind(12,1);
    $db->bind(13,$now);
    $db->bind(14,1);
    $db->bind(15,$now);
    $db->bind(16,$created_by);
    $db->execute();

    $db->query("insert into hx_rota (save_id,id,member_id,home_id,start_date,start_time,end_date,end_time,start_ts,end_ts,week_num,year,type,role,location,service_id,bid_id,bid_request_id,service_role_id,pcn_id, practice_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $db->bind(1, $thisSaveId);
    $db->bind(2, $thisId);
    $db->bind(3, $thisMemberId);
    $db->bind(4, $thisHome);
    $db->bind(5, $thisStartDate);
    $db->bind(6, $thisStartTime);
    $db->bind(7, $thisEndDate);
    $db->bind(8, $thisEndTime);
    $db->bind(9, $thisStart_ts);
    $db->bind(10, $thisEnd_ts);
    $db->bind(11, $thisWeekNum);
    $db->bind(12, $thisYear);
    $db->bind(13, $thisType);
    $db->bind(14, $role);
    $db->bind(15, $location);
    $db->bind(16, $service_data['id']);
    $db->bind(17, $id);
    $db->bind(18, $bid_request_id);
    $db->bind(19, $service_role_id);
    $db->bind(20,$service_data['pcn_id']);
    $db->bind(21,$service_data['practice_id']);
    $db->execute();


  }


}








echo 'ok';
