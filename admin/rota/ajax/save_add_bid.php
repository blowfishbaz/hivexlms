<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

function day_to_number($day){
  switch ($day) {
    case 'monday': return '1'; break; case 'tuesday': return '2'; break; case 'wednesday': return '3'; break; case 'thursday': return '4'; break; case 'friday': return '5'; break; case 'saturday': return '6'; break; case 'sunday': return 7; break;
  }
}


$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$id = $_POST['id'];
$service = $_POST['service'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$start_time = $_POST['start_time'];
$end_time = $_POST['end_time'];

$start_date = date('Y-m-d',strtotime($start_date));
$end_date = date('Y-m-d',strtotime($end_date));

$recurring = $_POST['reoccuring_shift'];
$recurring_type = $_POST['reoccure_freq'];
$recurring_days = $_POST['reoccuring_shift_day'];
$recurring_month_day = $_POST['day_of_month'];

$db->query("select id, location, pcn_id, practice_id from ws_service where id = ?");
$db->bind(1,$service);
$service_data = $db->single();

$db->query("select id, job_role, pid from ws_service_roles where pid = ? and status = 1");
$db->bind(1,$service);
$roles = $db->ResultSet();

if($recurring == 'yes'){

  if($recurring_type == 'weekly'){
    $begin = new DateTime($start_date);
    $end = new DateTime($end_date);
    $interval = DateInterval::createFromDateString('1 week');
    $period = new DatePeriod($begin, $interval, $end);

    foreach ($period as $dt) {
      foreach ($recurring_days as $key => $rd) {
        $make_date = date('Y-m-d',strtotime($dt->format("Y").'W'.$dt->format("W").day_to_number(strtolower($rd))));

        $id = createid('bid');

        $db->Query("insert into ws_rota_bid (id, created_date, created_by, service_id, start_date, end_date, start_time, end_time, start_final, end_final, practice_id, pcn_id, completed) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $db->bind(1,$id);
        $db->bind(2,$now);
        $db->bind(3,$created_by);
        $db->bind(4,$service);
        $db->bind(5,$make_date);
        $db->bind(6,$make_date);
        $db->bind(7,$start_time);
        $db->bind(8,$end_time);
        $db->bind(9,strtotime($make_date.' '.$start_time));
        $db->bind(10,strtotime($make_date.' '.$end_time));
        $db->bind(11,$service_data['practice_id']);
        $db->bind(12,$service_data['pcn_id']);
        $db->bind(13,'3');
        $db->execute();

        foreach ($roles as $r) {
          $bid_am = createid('bid_am');
          $amount = $_POST['rota_amount_needed_'.$r['id'].''];
          $db->Query("insert into ws_rota_bid_amount (id, created_date, created_by, pid, role_id, amount, service_role_id) values (?,?,?,?,?,?,?)");
          $db->bind(1,$bid_am);
          $db->bind(2,$now);
          $db->bind(3,$created_by);
          $db->bind(4,$id);
          $db->bind(5,$r['job_role']);
          $db->bind(6,$amount);
          $db->bind(7,$r['id']);
          $db->execute();
        }

      }
    }
  }else if($recurring_type == 'monthly'){
    $begin = new DateTime($start_date);
    $end = new DateTime($end_date);
    $interval = DateInterval::createFromDateString('1 month');
    $period = new DatePeriod($begin, $interval, $end);

    foreach ($period as $dt) {



        $make_date = date('Y-m-d',strtotime($dt->format("Y").'-'.$dt->format("m").'-'.$recurring_month_day));

        $id = createid('bid');

        $db->Query("insert into ws_rota_bid (id, created_date, created_by, service_id, start_date, end_date, start_time, end_time, start_final, end_final, practice_id, pcn_id, completed) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $db->bind(1,$id);
        $db->bind(2,$now);
        $db->bind(3,$created_by);
        $db->bind(4,$service);
        $db->bind(5,$make_date);
        $db->bind(6,$make_date);
        $db->bind(7,$start_time);
        $db->bind(8,$end_time);
        $db->bind(9,strtotime($make_date.' '.$start_time));
        $db->bind(10,strtotime($make_date.' '.$end_time));
        $db->bind(11,$service_data['practice_id']);
        $db->bind(12,$service_data['pcn_id']);
        $db->bind(13,'3');
        $db->execute();

        foreach ($roles as $r) {
          $bid_am = createid('bid_am');
          $amount = $_POST['rota_amount_needed_'.$r['id'].''];
          $db->Query("insert into ws_rota_bid_amount (id, created_date, created_by, pid, role_id, amount, service_role_id) values (?,?,?,?,?,?,?)");
          $db->bind(1,$bid_am);
          $db->bind(2,$now);
          $db->bind(3,$created_by);
          $db->bind(4,$id);
          $db->bind(5,$r['job_role']);
          $db->bind(6,$amount);
          $db->bind(7,$r['id']);
          $db->execute();
        }


    }
  }



}else{

$db->Query("insert into ws_rota_bid (id, created_date, created_by, service_id, start_date, end_date, start_time, end_time, start_final, end_final, practice_id, pcn_id) values (?,?,?,?,?,?,?,?,?,?,?,?)");
$db->bind(1,$id);
$db->bind(2,$now);
$db->bind(3,$created_by);
$db->bind(4,$service);
$db->bind(5,$start_date);
$db->bind(6,$end_date);
$db->bind(7,$start_time);
$db->bind(8,$end_time);
$db->bind(9,strtotime($start_date.' '.$start_time));
$db->bind(10,strtotime($end_date.' '.$end_time));
$db->bind(11,$service_data['practice_id']);
$db->bind(12,$service_data['pcn_id']);
$db->execute();

foreach ($roles as $r) {
  $bid_am = createid('bid_am');
  $amount = $_POST['rota_amount_needed_'.$r['id'].''];
  $db->Query("insert into ws_rota_bid_amount (id, created_date, created_by, pid, role_id, amount, service_role_id) values (?,?,?,?,?,?,?)");
  $db->bind(1,$bid_am);
  $db->bind(2,$now);
  $db->bind(3,$created_by);
  $db->bind(4,$id);
  $db->bind(5,$r['job_role']);
  $db->bind(6,$amount);
  $db->bind(7,$r['id']);
  $db->execute();

  for ($i=0; $i < $amount; $i++) {
    $db->Query("insert into ws_rota_bid_amount_details (id, bid_id, bid_amount_id, service_id, service_role_id) values (?,?,?,?,?)");
    $db->bind(1,createid('bid_am_d'));
    $db->bind(2,$id);
    $db->bind(3,$bid_am);
    $db->bind(4,$service);
    $db->bind(5,$r['id']);
    $db->execute();
  }

  $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id, ws_accounts.status
  from ws_accounts_locations
  left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
  left join ws_accounts on ws_accounts.id = ws_accounts_locations.account_id
  where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id = ? and ws_accounts_roles.status = 1 and ws_accounts.status = 1");
  $db->bind(1,$service_data['location']);
  $db->bind(2,$r['job_role']);
  $list = $db->ResultSet();

  foreach ($list as $l) {
    ///can they work this time?
    $db->query("SELECT * FROM ws_availability WHERE account_id = ? and start_ts <= ? and end_ts >= ? and status = 1 ORDER BY start_ts ASC");
    $db->bind(1,$l['account_id']);
    $db->bind(2,strtotime($start_date.' '.$start_time));
    $db->bind(3,strtotime($end_date.' '.$end_time));
    $availability = $db->resultset();
    $availability_count = $db->rowcount();

    //if($availability_count >=1){
    $bid_request_id = createid('bid_rq');
    $db->Query("insert into ws_rota_bid_request (id, pid, account_id, created_date, created_by, role_id, bid_amount_pid, service_role_id, service_id) values (?,?,?,?,?,?,?,?,?)");
    $db->bind(1,$bid_request_id);
    $db->bind(2,$id);
    $db->bind(3,$l['account_id']);
    $db->bind(4,$now);
    $db->bind(5,$created_by);
    $db->bind(6,$l['role_id']);
    $db->bind(7,$bid_am);
    $db->bind(8,$r['id']);
    $db->bind(9,$r['pid']);
    $db->execute();

    $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status, pid) values (?,?,?,?,?,?,?,?,?)");
    $db->bind(1,createid('not'));
    $db->bind(2,$l['account_id']);
    $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $db->bind(4,time());
    $db->bind(5,'New Rota Shift');
    $db->bind(6,'A New Rota Shift has become avaiable, click on the notification to view');
    $db->bind(7,'new_shift');
    $db->bind(8,'1');
    $db->bind(9,$bid_request_id);
    $db->execute();

    //}
  }
}

//Need to get all the users in the area and that can do the role.

  $search_list="'0'";
  foreach ($roles as $r) {
    $search_list= $search_list.",'".$r['job_role']."'";
  }

  $db->query("select distinct ws_accounts_locations.account_id, ws_accounts_roles.role_id
  from ws_accounts_locations
  left join ws_accounts_roles on ws_accounts_roles.account_id = ws_accounts_locations.account_id
  where ws_accounts_locations.location_id = ? and ws_accounts_locations.status = 1 and ws_accounts_roles.role_id in ($search_list) and ws_accounts_roles.status = 1");
  $db->bind(1,$service_data['location']);
  $list = $db->ResultSet();

  foreach ($list as $l) {
    $db->Query("insert into ws_rota_bid_request (id, pid, account_id, created_date, created_by, role_id) values (?,?,?,?,?,?)");
    $db->bind(1,createid('bid_rq'));
    $db->bind(2,$id);
    $db->bind(3,$l['account_id']);
    $db->bind(4,$now);
    $db->bind(5,$created_by);
    $db->bind(6,$l['role_id']);
    //$db->execute();
  }
}


echo 'ok';
