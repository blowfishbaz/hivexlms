<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    contacts/contact_view/calendar/load_calendar_appointments.php
 * @author    Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $now = time();
 $account = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 $id = $_POST['id'];
 $extra_id = $_POST['id_extra'];
 $firstname = $_POST['first_name'];
 $surname = $_POST['surname'];
 $email = $_POST['email'];
 $telephone = $_POST['telephone'];
 $mobile = $_POST['mobile_number'];
 $job_role = $_POST['job_role'];
 $location = $_POST['location'];

 $db->query("update accounts set name = ?, email = ? where id = ?");
 $db->bind(1,$firstname.' '.$surname);
 $db->bind(2,$email);
 $db->bind(3,$id);
 $db->execute();

 $db->query("update ws_accounts set first_name = ?, surname = ?, email = ? where id = ?");
 $db->bind(1,$firstname);
 $db->bind(2,$surname);
 $db->bind(3,$email);
 $db->bind(4,$id);
 $db->execute();

 $db->query("update ws_accounts_extra_info set mobile_number = ?, telephone = ? where account_id = ? and status = 1");
 $db->bind(1,$mobile);
 $db->bind(2,$telephone);
 $db->bind(3,$id);
 $db->execute();



 $db->query("update ws_accounts_locations set status = 0 where account_id = ?");
 $db->bind(1,$id);
 $db->execute();

 $db->query("update ws_accounts_roles set status = 0 where account_id = ?");
 $db->bind(1,$id);
 $db->execute();


 foreach ($job_role as $jb) {
   $db->Query("INSERT INTO ws_accounts_roles (id, role_id, account_id) VALUES (?,?,?)");
   $db->bind(1,createid('a_role'));
   $db->bind(2,$jb);
   $db->bind(3,$id);
   $db->execute();
 }

 //ws_accounts_locations
 foreach ($location as $loc) {
   $db->Query("INSERT INTO ws_accounts_locations (id, location_id, account_id) VALUES (?,?,?)");
   $db->bind(1,createid('a_loc'));
   $db->bind(2,$loc);
   $db->bind(3,$id);
   $db->execute();
 }

 if($_POST['bank_check'] == 'yes'){
   $db->query("update ws_accounts_extra_info set bank_name = ?, bank_account_number = ?, bank_sort_code = ? where account_id = ? and status = 1");
   $db->bind(1,encrypt($_POST['bank_name']));
   $db->bind(2,encrypt($_POST['bank_account_number']));
   $db->bind(3,encrypt($_POST['bank_sort_code']));
   $db->bind(4,$id);
   $db->execute();
 }





 echo 'ok';
