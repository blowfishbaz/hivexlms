<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$id = $_POST['id'];
$title = $_POST['title'];
$description = $_POST['description'];
$location = $_POST['location'];
$job_role = $_POST['job_role'];
$invoice_address = $_POST['invoice_address'];

$address1 = $_POST['address1'];
$address2 = $_POST['address2'];
$address3 = $_POST['address3'];
$city = $_POST['city'];
$county = $_POST['county'];
$postcode = $_POST['postcode'];

$client_id = encrypt($_POST['client_id']);
$client_secret = encrypt($_POST['client_secret']);
$account_number = encrypt($_POST['account_number']);
$payment_terms = $_POST['payment_terms'];
$payee_id = encrypt($_POST['payee_id']);

// Loop through for the hourly rates

$db->Query("update ws_service set title = ?, description = ?, location = ?, invoice_address = ?, modified_by = ?, modified_date = ?, client_id = ?, client_secret = ?, account_number = ?, payment_term = ?, xero_id = ?, address1 =?,address2 =?,address3 =?,city =?,county =?,postcode =? where id = ?");
$db->bind(1,$title);
$db->bind(2,$description);
$db->bind(3,$location);
$db->bind(4,$invoice_address);
$db->bind(5,$created_by);
$db->bind(6,$now);
$db->bind(7,$client_id);
$db->bind(8,$client_secret);
$db->bind(9,$account_number);
$db->bind(10,$payment_terms);
$db->bind(11,$payee_id);
$db->bind(12,$address1);
$db->bind(13,$address2);
$db->bind(14,$address3);
$db->bind(15,$city);
$db->bind(16,$county);
$db->bind(17,$postcode);
$db->bind(18,$id);
$db->execute();


$db->Query("update ws_service_roles set status = ?, modified_date = ?, modified_by = ? where pid = ?");
$db->bind(1,'0');
$db->bind(2,$now);
$db->bind(3,$created_by);
$db->bind(4,$id);
$db->execute();

foreach ($job_role as $jb) {

  $db->Query("insert into ws_service_roles (id, pid, created_date, created_by, job_role, hourly_rate) values (?,?,?,?,?,?)");
  $db->bind(1,createid('serv_r'));
  $db->bind(2,$id);
  $db->bind(3,$now);
  $db->bind(4,$created_by);
  $db->bind(5,$jb);
  $db->bind(6,$_POST['hourly_rate_'.$jb.'']);
  $db->execute();

}






echo 'ok';
