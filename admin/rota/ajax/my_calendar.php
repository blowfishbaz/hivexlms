<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    contacts/contact_view/calendar/load_calendar_appointments.php
 * @author    Baz Kika
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];


   $db->query("select * from hx_rota where status = 1 and member_id = ?");
   $db->bind(1,$id);
   $data = $db->resultset();

  ?>
  [
   <? foreach($data as $d) {

     $db->query("select * from ws_roles where id = ?");
     $db->bind(1,$d['role']);
     $role = $db->single();

        ?>

     {
            "id": "<?echo $d['bid_id'];?>",
           "title": "<? echo $d['location']; ?> - <?echo $role['title'];?>",
           "start": "<? echo date('c', $d['start_ts']);?>",
           "end": "<? echo date('c', $d['end_ts']);?>"
     },
     <? } ?>
      {
     "title": "",
     "start": "2150-06-28"
   }
    ]
