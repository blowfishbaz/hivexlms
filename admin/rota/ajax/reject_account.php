<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');
use PHPMailer\PHPMailer\PHPMailer;
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/Exception.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/SMTP.php');
$id = $_GET['id'];

$db->Query("update accounts set user_status = 42 where id = ? ");
$db->bind(1,$id);
$db->execute();

$db->Query("update ws_accounts set status = 42 where id = ? and pid = ?");
$db->bind(1,$id);
$db->bind(2,$id);
$db->execute();

$db->query("select * from accounts where id = ? ");
$db->bind(1,$id);
$user = $db->single();


$subject='Account Approval - Rejection';
$email_title = 'Account Approval - Rejection';
$main_body_text = 'Hi '.ucfirst ($user['name']).'<br />your account has been rejected<br />Unfortunetly your account has not been approved by our Admin Team. <br /> If you have any quieres please contact a member of our admin team';
$head='';
$body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #fff;margin-top: -18px;"> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> <!--<![endif]--> <div class="wrapper"> <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""> <tbody> <tr> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 100vw;"> <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#dedede;" align="center"> </div> <div style="Margin-top: 50px;"> <div style="line-height:20px;text-align:center;padding: 5px;background-color: #edaac6;color: white;"> <h1 style="margin:40px 0px; line-height:normal; font-size:55px;">'.$email_title.'</h1> </div> </div> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> </tbody> </table>';

$body2 = '<table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 80vw;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;"> '.$main_body_text.'</p><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 75px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div></td></tr></tbody></table>';

$body3 = '';

$footer='<!-- Callout Panel --> <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede; width:100vw;" align="center" emb-background-style=""> <tbody> <tr> <td colspan="3"> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 15px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> <tr> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <a href="tel:0151 294 3322"><img src="http://staff-box.co.uk/assets/images/emails/IconPhone.png" style="margin-left:20px;margin-right:40px;" /></a> <a href="mailto:info@onewirral.co.uk"><img src="http://staff-box.co.uk/assets/images/emails/IconEmail.png" style="margin-left:20px;margin-right:40px;" /></a> <a href="https://goo.gl/maps/BRrWziseA1VgmjVB7"><img src="http://staff-box.co.uk/assets/images/emails/IconLocation_cc.png" style="margin-left:20px;margin-right:40px;" /></a> </td> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <p class="size-16" style="Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;">&copy; Staffbox Copyright '.date("Y").'</p><!-- Callout Panel --> </td> <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 33%;"> <img src="http://staff-box.co.uk/assets/images/emails/Staff_Box_Logo.png" style="float:right; margin-right:20px;" /> </td> </tr> <tr> <td colspan="3"> <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 15px;"> <div style="line-height:20px;font-size:1px">&nbsp;</div> </div> </td> </tr> </tbody> </table></div></body>';


$htmlContent = $head.$body1.$body2.$body3.$footer;

$mail = new PHPMailer();
$mail->SMTPDebug = 0;
$mail->isSMTP();
$mail->Host = 'smtp.office365.com';
$mail->SMTPAuth = true;
$mail->Username = 'rota@staff-box.co.uk'; //paste one generated by Mailtrap
$mail->Password = 'Lux03898'; //paste one generated by Mailtrap
$mail->Port = 587;
$mail->setFrom('rota@staff-box.co.uk', 'Rota');
$mail->addAddress($user['email'], $user['name']);
$mail->Subject = $subject;
$mail->isHTML(true);
$mail->Body = $htmlContent;

// if($mail->send()){
//     // echo 'Message has been sent';
// }else{
//     // echo 'Message could not be sent.';
//     // echo 'Mailer Error: ' . $mail->ErrorInfo;
// }

echo 'ok';
