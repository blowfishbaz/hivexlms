<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$created_by = decrypt($_SESSION["SESS_ACCOUNT_ID"]);
$now = time();

$id = $_POST['id'];
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];
$start_time = $_POST['start_time'];
$end_time = $_POST['end_time'];


$start_date = date('Y-m-d',strtotime($start_date));
$end_date = date('Y-m-d',strtotime($end_date));

$db->query("select * from ws_rota_bid_amount where pid = ? and status = 1");
$db->bind(1,$id);
$bid_amount = $db->resultset();


$db->Query("update ws_rota_bid set modified_date = ?, modified_by = ?, start_date = ?, end_date = ?, start_time = ?, end_time = ?, start_final = ?, end_final = ? where id = ?");
$db->bind(1,$now);
$db->bind(2,$created_by);
$db->bind(3,$start_date);
$db->bind(4,$end_date);
$db->bind(5,$start_time);
$db->bind(6,$end_time);
$db->bind(7,strtotime($start_date.' '.$start_time));
$db->bind(8,strtotime($end_date.' '.$end_time));
$db->bind(9,$id);
$db->execute();

foreach ($bid_amount as $ba) {
  $amount = $_POST['rota_amount_needed_'.$ba['id'].''];

  $db->Query("update ws_rota_bid_amount set modified_date = ?, modified_by = ?, amount = ? where id = ?");
  $db->bind(1,$now);
  $db->bind(2,$created_by);
  $db->bind(3,$amount);
  $db->bind(4,$ba['id']);
  $db->execute();
}


echo 'ok';
