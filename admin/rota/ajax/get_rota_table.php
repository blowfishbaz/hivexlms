<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


function get_week_dates($date = null){

        $given_week = date( 'W',strtotime($date));
        $given_year = date( 'Y',strtotime($date));
        $week = array();
        $m_morn = date('d-m-Y H:i:01',strtotime($given_year.'W'.$given_week));
        $m_night = date('d-m-Y 23:59:59',strtotime($given_year.'W'.$given_week));
        array_push($week,$m_morn);
        array_push($week,$m_night );
        $t_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +1 days'));
        $t_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +1 days'));
        array_push($week,$t_morn);
        array_push($week,$t_night );
        $w_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +2 days'));
        $w_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +2 days'));
        array_push($week,$w_morn);
        array_push($week,$w_night );
        $th_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +3 days'));
        $th_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +3 days'));
        array_push($week,$th_morn);
        array_push($week,$th_night );
        $f_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +4 days'));
        $f_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +4 days'));
        array_push($week,$f_morn);
        array_push($week,$f_night );
        $sa_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +5 days'));
        $sa_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +5 days'));
        array_push($week,$sa_morn);
        array_push($week,$sa_night );
        $su_morn = date('d-m-Y H:i:01',strtotime($m_morn.' +6 days'));
        $su_night = date('d-m-Y 23:59:59',strtotime($m_morn.' +6 days'));
        array_push($week,$su_morn);
        array_push($week,$su_night );

        return $week;
}


$given_date = $_GET['date'];
$w = get_week_dates($given_date);

// $monday = date( 'd-m-Y', strtotime( 'monday this week' ) );
// $w = week_from_monday($monday);


$db->query("select * from hx_homes where id = ?");
$db->bind(1,$_GET['id']);
$thisHome = $db->single();

// echo '<pre>';
// print_r($thisHome);
// echo '</pre>';



?>
<div class="row"></div>

<h4 class="rota_num">Rota Week #<? echo date("W", strtotime($given_date)); ?></h4>

<div class="col-lg-12">
        <div class="row">
                <div class="c_panel_top">
                      <div class="cur_info">
                            <span class="w_dat">Current Date: <strong><? echo date('d-m-Y',time()); ?></strong></span>
                            <span class="w_num">Current Week: <strong>#<? echo date('W',time()); ?></strong></span>
                      </div>
                </div>
        </div>
</div>

<div class="col-lg-12">
        <div class="row">

          <?
          $prev = date('Y-m-d',strtotime($_GET['date'].' -7 days'));
          $now = date('Y-m-d',time());
          $next = date('Y-m-d',strtotime($_GET['date'].' +7 days'));
          ?>

              <div class="c_panel">
                    <div class="cur_info">
                        <input class="form-control week_pick" type="week" id="week" name="week" value="2020-W<? echo date("W", strtotime($given_date)); ?>">

                    </div>
                    <div class="cur_info_opts">
                        <button class="btn btn-info btn-raised btn-sm add_home" data-date="<? echo $prev; ?>">< Previous</button>
                        <button class="btn btn-info btn-raised btn-sm add_home" data-date="<? echo $now; ?>"> Current Week</button>
                        <button class="btn btn-info btn-raised btn-sm add_home" data-date="<? echo $next; ?>"> Next ></button>
                    </div>
            </div>
      </div>
</div>

<!-- rota_holder -->
<div class="col-lg-12 rota_holder">
<!-- rota_holder -->

          <div class="row">
                <div class="col-md-1 day_head">Staff Name<br /><span class="s_date"></span></div>
                <div class="col-md-1 day_head">Monday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[0])); ?></span></div>
                <div class="col-md-1 day_head">Tuesday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[2])); ?></span></div>
                <div class="col-md-1 day_head">Wednesday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[4])); ?></span></div>
                <div class="col-md-1 day_head">Thursday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[6])); ?></span></div>
                <div class="col-md-1 day_head">Friday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[8])); ?></span></div>
                <div class="col-md-1 day_head">Saturday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[10])); ?></span></div>
                <div class="col-md-1 day_head">Sunday<br /><span class="s_date"><? echo date('d-m-Y',strtotime($w[12])); ?></span></div>
          </div>


          <!-- ROTA ROWS  -->
          <div id="rota_rows">
          <!-- ROTA ROWS  -->


          <?

          $db->query("SELECT accounts.*, hx_home_users.type as hometype FROM accounts
                      LEFT JOIN hx_home_users
                      ON hx_home_users.user_id = accounts.id
                      WHERE hx_home_users.home_id = ? order by accounts.name ASC");
                      $db->bind(1,$_GET['id']);
                      $peeps = $db->resultSet();


          foreach ($peeps as $key => $v) {
          ?>

                      <!-- day worker  -->
                      <div class="row day_worker">
                      <!-- day worker  -->
                                        <div class="col-md-1 wrow name_tag">
                                                    <div class="day_add main_add"><span class="glyphicons glyphicons-plus-sign"></span></div>
                                                    <p><? echo $v['name']; ?></p>
                                                    <p class="info_p">Main Home: <span class="info_val"><? echo $thisHome['name']; ?></span></p>
                                                    <p class="info_p">Hours: <span class="info_val"> <? echo $v['contracted_hours']; ?>hrs / 0hrs</span></p>
                                                    <?
                                                    if($v['hometype']){}

                                                    ?>
                                                    <p class="info_p">Cover This Home: <span class="info_val"><span class="glyphicons glyphicons-tick"></span></span></p>
                                        </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"><span class="glyphicons glyphicons-plus-sign"></span></div>
                                        </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div>
                                        </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div>
                                        </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"><span class="glyphicons glyphicons-plus-sign"></span> </div>
                                        </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div>
                                        </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"><span class="glyphicons glyphicons-plus-sign"></span></div>
                                            </div>
                                        <div class="col-md-1 wrow">
                                                    <div class="day_add"> <span class="glyphicons glyphicons-plus-sign"></span> </div>
                                        </div>
                      <!-- day worker  -->
                      </div>
                      <!-- day worker  -->


              <?
              }
              ?>


          <!-- ROTA ROWS  -->
          </div>
          <!-- ROTA ROWS  -->

<!-- rota_holder -->
</div>
<!-- rota_holder -->
