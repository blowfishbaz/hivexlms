<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/



include($_SERVER['DOCUMENT_ROOT'].'/application.php');


$id = $_POST['id'];
$type = $_POST['type'];



if($type == 'bid'){
  $db->Query("update ws_rota_bid set status = ? where id = ?");
  $db->bind(1,0);
  $db->bind(2,$id);
  $db->execute();

  $db->Query("update hx_rota set status = ? where bid_id = ?");
  $db->bind(1,0);
  $db->bind(2,$id);
  $db->execute();
}else if($type == 'rota_oustanding'){

  $db->Query("update hx_rota set status = ? where id = ?");
  $db->bind(1,0);
  $db->bind(2,$id);
  $db->execute();

  $db->query("select * from hx_rota where id = ?");
  $db->bind(1,$id);
  $rota = $db->single();

  $db->query("update ws_rota_bid_request set accepted_admin = 0 where id = ? and pid = ?");
  $db->bind(1,$rota['bid_request_id']);
  $db->bind(2,$id);
  $db->execute();

}else if($type == 'rota_complete'){
  $db->Query("update hx_rota set status = ? where id = ?");
  $db->bind(1,0);
  $db->bind(2,$id);
  $db->execute();
}

echo 'ok';
