<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $newid = createId('att');
  $now = time();
  $name=strtolower($_POST["name"]);
  $email=strtolower($_POST["email"]);
  $type=strtolower($_POST["type"]);
  $status=$_POST["status"];

if($_SESSION['SESS_ACCOUNT_Type']=='admin'){
  $db = new database;
  $db->Query("INSERT INTO accounts (id ,created_date ,name, email, type, status,username,password,account_type) values (?,?,?,?,?,?,?,?,?)");
  $db->bind(1,$newid);
  $db->bind(2,$now);
  $db->bind(3,$name);
  $db->bind(4,$email);
  $db->bind(5,'admin');
  $db->bind(6,'1');
  $db->bind(7,encrypt($email));
  $db->bind(8,encrypt($_POST["newpassword"]));
  $db->bind(9,$type);
  $db->execute();

  if($type == 'subadmin'){
    foreach ($_POST['location'] as $key => $location) {
      $db = new database;
      $db->Query("INSERT INTO accounts_service_area (id, pid, status, area) values (?,?,?,?)");
      $db->bind(1,createid('acc_area'));
      $db->bind(2,$newid);
      $db->bind(3,1);
      $db->bind(4,$location);
      $db->execute();
    }

    foreach ($_POST['pcn_practice'] as $key => $pcnpract) {
      if(id_checker($pcnpract, 'prac')){
        $db = new database;
        $db->Query("INSERT INTO accounts_sub_practice (id, account_id, practice_id) values (?,?,?)");
        $db->bind(1,createid('s_prac'));
        $db->bind(2,$newid);
        $db->bind(3,$pcnpract);
        $db->execute();
      }else{
        $db = new database;
        $db->Query("INSERT INTO accounts_sub_pcn (id, account_id, pcn_id) values (?,?,?)");
        $db->bind(1,createid('s_pcn'));
        $db->bind(2,$newid);
        $db->bind(3,$pcnpract);
        $db->execute();
      }
    }
  }

  echo 'ok';
}
?>
