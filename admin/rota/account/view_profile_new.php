<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/customer_view.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 // Get Record
 $id = $_GET['id'];


 $db->query("select * from ws_accounts where id = ?");
 $db->bind(1,$id);
 $data = $db->single();


 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX <? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



		<?	//Base CSS Include
			include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>






<!--//////////////baz added/////////////-->


<!-- On Page CSS -->
<style>
#page_spacer{
  display: none;
}
	.contactBox {
		min-height:103px;
	}
	.contactBox:hover {
		cursor: pointer;
		box-shadow: 0px 0px 10px 1px #009688;
	}


 .edit_contact_link:hover {
  cursor: pointer;
  box-shadow: 0px 0px 10px 1px #009688;
 }

.upImg{
	min-height:40px;
	min-width:40px;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
background-size: 100% 100%;
display: block;
}

.upImhLi{
	margin-left:25px;
	float:left;
	width:90%;
	align-items:center;
}

.ipImtitle{
	margin-left:25px;
	line-height: 40px;
}


.withdel{
  width: 80%;
	float: left;
}
.deluser{
	float: right;
}
.userholder{
	float: left;
	width: 100%;
}

.customer_select{
  padding-top: 0px;
  padding-bottom: 0px;
  padding-left: 10px;

  width: auto;
}

.section_header {
    margin-top: 10px;
    border-bottom: 1px solid #337ab7;
    padding-bottom: 3px;
    color: #337ab7;
  }

.customer_select .filter-option{
  font-size: 28px;
}

.customer_select .selectpicker{
  padding: 0px;
  padding-right: 30px;
}

.care_of_contact_business_picker .selectpicker{
  padding: 0px;
  padding-right: 30px;
  font-weight: 100;
  text-transform: none;
  font-size: 16px;
}

.care_of_contact_business_picker.btn-group .dropdown-menu li a{
  display:block!important;
  border-radius: 0px!important;
}

.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: auto!important;
}

.selectpicker {
    padding: 0px;
}

#map {
    width: 100%;
    float: left;
    /*height: 80vh;*/
    height: calc(100vh - 280px);
    border: 3px solid #009688;
}


.map_checks {
    float: left;
    margin: 0 10px 0 0;

}
.modal {
  overflow-y:auto;
}

.careofbox {
  min-height:103px;
}
.careofbox:hover {
  cursor: pointer;
  box-shadow: 0px 0px 10px 1px #009688;


}

.client_add_label input[type=checkbox]:checked + .checkbox-material .check{
   border-color: rgb(4,169,244)!important;
 }

 .client_add_label input[type=checkbox]:checked + .checkbox-material .check:before, label.checkbox-inline input[type=checkbox]:checked + .checkbox-material .check:before{
    color: rgb(4,169,244)!important;
 }


 .prospect_add_label input[type=checkbox]:checked + .checkbox-material .check{
    border-color: rgb(243,208,0)!important;
  }

  .prospect_add_label input[type=checkbox]:checked + .checkbox-material .check:before, label.checkbox-inline input[type=checkbox]:checked + .checkbox-material .check:before{
     color: rgb(243,208,0)!important;
  }

  .target_add_label input[type=checkbox]:checked + .checkbox-material .check{
     border-color: rgb(423,144,49)!important;
   }

   .target_add_label input[type=checkbox]:checked + .checkbox-material .check:before, label.checkbox-inline input[type=checkbox]:checked + .checkbox-material .check:before{
      color: rgb(423,144,49)!important;
   }

.map_checks{
  margin: 0px;
  padding-bottom: 0px;
  margin-right:25px;
}
.myown_add_label input[type=checkbox]:checked + .checkbox-material .check{
   border-color: #81654a !important;
 }

 .myown_add_label input[type=checkbox]:checked + .checkbox-material .check:before, label.checkbox-inline input[type=checkbox]:checked + .checkbox-material .check:before{
    color: #81654a !important;
 }
</style>
</head>
<body>
    <div id="wrapper">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="row">
											<div class="col-lg-12">
																		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>
            				<!--- PAGE CONTENT HERE ---->
                    <div class="col-lg-12">
                      <div class="page_title text-capitalize"><?echo $data['first_name'].' '.$data['surname'];?></div>

                      <div class="clearfix"></div>

                    </div>
											</div>

											<div class="row" style="margin-top: 20px;">
												<div class="col-lg-12">
										    	<div>
													  <!-- Nav tabs -->
													  <!-- <ul class="nav nav-tabs" role="tablist">
															<li role="presentation active"><a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/dashboarad.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" >Overview</a></li>
															<li role="presentation active"><a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/documents.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" >Documents</a></li>
															<li role="presentation active"><a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/signature.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" >Signature</a></li>
															<li role="presentation active"><a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/rota.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" >Rota</a></li>


															<li role="presentation " style="float:right;"><a href="javascript:void(0)" class="" id="edit_account">Edit Account</a></li>





													  </ul> -->

                            <a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/dashboarad.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" class="btn btn-raised btn-profile">Dashboard</a>
														<a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/documents.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" class="btn btn-raised btn-profile">Documents</a>
														<a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/signature.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" class="btn btn-raised btn-profile">Signature</a>
														<a href="<? echo $fullurl; ?>admin/rota/ajax/tabs/rota.php?id=<? echo $id; ?>"  data-target="#contactstab" id="maintab" role="tab"  data-toggle="tabajax" class="btn btn-raised btn-profile">Rota</a>

														<a href="javascript:void(0)" class="pull-right btn btn-raised btn-success" id="edit_account">Edit Account</a>
													  <!-- Tab panes -->
													  <div class="tab-content">
													    <div role="tabpanel" class="tab-pane active" id="contactstab"></div>
													    <div role="tabpanel" class="tab-pane" id="addressestab"></div>
													    <div role="tabpanel" class="tab-pane" id="notestab"></div>
													    <div role="tabpanel" class="tab-pane" id="taskstab"></div>
													    <!--<div role="tabpanel" class="tab-pane" id="casestab"></div>-->
															<div role="tabpanel" class="tab-pane" id="salesstab"></div>
													    <div role="tabpanel" class="tab-pane" id="quotestab"></div>
															<div role="tabpanel" class="tab-pane" id="orderstab"></div>
													    <div role="tabpanel" class="tab-pane" id="calendartab"></div>
													    <div role="tabpanel" class="tab-pane" id="reportstab"></div>
													    <div role="tabpanel" class="tab-pane" id="socialtab"></div>
													  </div>
												</div>
								  </div>
									<!-- <div class="col-lg-3">
										<div class="panel panel-primary">
											<div class="panel-heading">
												Communications
												<a href="javascript:void(0)"  id="refreshCommunications" class="btn btn-default pull-right"><span class="glyphicons glyphicons-refresh"></span><div class="ripple-container"></div></a>
												<a href="javascript:void(0)"  id="addCommunications" class="btn btn-info  btn-sm  btn-raised pull-right">Add<div class="ripple-container"></div></a>
												<div class="clearfix"></div>
											</div>
										  <div class="panel-body">
												<div class="panel-body" id="CommunicationsDIV">

											  </div>
										  </div>
										</div>
										<div class="panel panel-primary">
											<div class="panel-heading">
												Activity
												<a href="javascript:void(0)"  id="refreshActivity" class="btn btn-default pull-right"><span class="glyphicons glyphicons-refresh"></span><div class="ripple-container"></div></a>
											</div>
										  <div class="panel-body" id="activityDiv">

										  </div>
											<div class="panel-footer "><a href="javascript:void(0)" class="btn btn-primary btn-sm pull-right" id="loadActivityModal"><span class="glyphicons glyphicons-resize-full"></span>View All<div class="ripple-container"></div></a><div class="clearfix"></div></div>
										</div>
									</div> -->
									<input id="cid" style="display: none;" value="<?echo $_GET['id']?>"/>
								</div>

          				<!--- END OF PAGE CONTENT -->
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->
	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>
	</body>
</html>


<!-- PAGE JS -->
<!-- The main application script -->
<script src="<? echo $fullurl; ?>assets/js/main.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl ?>assets/js/jSignature.min.js"></script>

<!--//////////////baz added/////////////-->
<!-------------------------------- uploader --------------------------------->
<!-------------------------------- uploader --------------------------------->
<!-------------------------------- uploader --------------------------------->


<!--//////////////baz added/////////////-->
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<? echo $fullurl; ?>assets/js/load-image.all.min.js"></script>


<script src="<? echo $fullurl; ?>assets/js/jquery.ui.widget.js"></script>

<script src="<? echo $fullurl; ?>assets/js/tmpl.min.js"></script>


<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<? echo $fullurl; ?>assets/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<? echo $fullurl; ?>assets/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<? echo $fullurl; ?>assets/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-ui.js"></script>


<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!--//////////////baz added/////////////-->
<!-------------------------------- uploader --------------------------------->
<!-------------------------------- uploader --------------------------------->
<!-------------------------------- uploader --------------------------------->
<script>
var cust_ind = '<?echo $data['cei'];?>';
$(document).ready(function() {


  if(cust_ind == 1){
    setTimeout(function(){
 $('.cca_holder').empty();
 var $this = $('#maintab'),
      loadurl = $this.attr('href'),
      targ = $this.attr('data-target');
       $.get(loadurl, function(data) {
            $(targ).html(data);
       });
       $this.tab('show');
       },100);
  }else{
    setTimeout(function(){
 $('.cca_holder').empty();
 var $this = $('#maintab'),
      loadurl = $this.attr('href'),
      targ = $this.attr('data-target');
       $.get(loadurl, function(data) {
            $(targ).html(data);
       });
       $this.tab('show');
       },100);
  }

});


$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})


$('[data-toggle="tabajax"]').click(function(e) {
    var $this = $(this),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');

    $.get(loadurl, function(data) {
        $(targ).html(data);
    });
//console.log("show");
    $this.tab('show');
    return false;
});

var account_id = '<?echo $id?>';




$( "body" ).on( "click", "#edit_account", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

	  $.ajax({
	     type: "POST",
	     url: $fullurl+"admin/rota/form/edit_account.php?id="+account_id,
	     success: function(msg){
	       $("#BaseModalLContent").delay(1000)
	        .queue(function(n) {
	            $(this).html(msg);
	            n();
	        }).fadeIn("slow").queue(function(n) {
	                     $.material.init();
	                    n();
	                    $('.selectpicker').selectpicker();
	        });
	       }
	});
});


$( "body" ).on( "click", "#save_edit_account", function() {

	var formid = '#edit_account_form';
  var HasError = 0;

  $(formid).find('input').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
             if(!$(this).prop('required')){
        } else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
  });
	$(formid).find('select').each(function(){
		$(this).parent().removeClass('has-error');
		Messenger().hideAll();

		if(!$.trim(this.value).length) { // zero-length string AFTER a trim
						 if(!$(this).prop('required')){
				} else {
					HasError = 1;
					$(this).parent().addClass('has-error');
				}
			}
	});

	if(HasError == 0){

	  var FRMdata = $(formid).serialize(); // get form data
	    $.ajax({
	          type: "POST",
	          url: $fullurl+"admin/rota/ajax/update_account.php",
	          data: FRMdata,
	          success: function(msg){
	            $message=$.trim(msg);
	            if($message=='ok'){
								Messenger().post({
										message: 'Account Updated.',
										type: 'error',
										showCloseButton: false
								});
	              setTimeout(function(){
	                //window.location.replace($fullurl+'jobs.php');
									window.location.reload();
	              },1000);
	            }

	         },error: function (xhr, status, errorThrown) {
	                Messenger().post({
	                    message: 'An error has occurred please try again.',
	                    type: 'error',
	                    showCloseButton: false
	                });
	              }
	    });

	}


});

</script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/customer/customer_view.php'); ?>
