<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 //Page Title
 $page_title = 'Staff Box - Services';



 ?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>



    	<?
	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>


<!-- On Page CSS -->
<style>

</style>
</head>

<body>

    <div id="wrapper">


		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">



                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?>


            				<!--- PAGE CONTENT HERE ---->

								<div class="page_title text-capitalize">
                                    <img src="/staff-box/images/pink.png" height="75" />
                                    <span class="menuText">Available Shifts</span>


                                </div>





								<div class="clearfix"></div>



                                            <table id="table" class="striped"
                                                    data-toggle="table"
                                                    data-show-export="false"
                                                    data-export-types="['excel']"
                                                    data-click-to-select="true"
                                                    data-filter-control="false"
                                                    data-show-columns="false"
                                                    data-show-refresh="false"
                                                    data-url="<? echo $fullurl; ?>admin/rota/ajax/table/pending_shifts.php"
                                                    data-height="400"
                                                    data-side-pagination="server"
                                                    data-pagination="true"
                                                    data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                                    data-search="true">
                                                <thead>
                                                    <tr>
                                                            <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                                                            <th data-field="account_id" data-sortable="true" data-visible="false">Account ID</th>

                                                            <th data-field="service_title" data-sortable="true" data-formatter="LinkViewPendingShifts">Service Name</th>
                                                            <th data-field="service_location" data-sortable="true" data-visible="true">Location</th>
                                                            <th data-field="role_title" data-sortable="true" data-visible="true">Role / Position</th>
                                                            <th data-field="start_date" data-sortable="true" data-visible="true">Start Date</th>
                                                            <th data-field="start_time" data-sortable="true" data-visible="true">Start Time</th>

                                                    </tr>
                                                </thead>
                                        </table>





            				<!--- END OF PAGE CONTENT -->

                </div>
            </div>
        </div>


        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

	<?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
    </div>


</body>
</html>

<!-- PAGE JS -->



<script>


</script>



<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
