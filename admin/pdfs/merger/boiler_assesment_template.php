<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');


use setasign\Fpdi\Fpdi;


require('../fpdf.php');
require_once('merger/autoload.php');

$pdf_left = '-10';
$pdf_top = '-10';
$pdf_width = '220';

$tick= chr(51);
$cross= chr(53);


// initiate FPDI
$pdf = new Fpdi();
//my template

$pdf->setSourceFile('BACL_1_4.pdf');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(1);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(2);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qa1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 120);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(80,11,'05/02/1991',0,L,1);

        //qa2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 135);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'17 Burton Road Coton in the Elms, Derbyshire. The Black Horse Pub, possibly the furthest pub from the sea in the UK.',0,L,1);


        //qa3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 153);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'SW1A 1AA',0,L,1);

        //qb1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 171);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


        //qb2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 178);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


        //qb3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 186);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);

        //qb4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 194);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);

        //qb5
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93.5, 202);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(130.5, 202);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        //qb6
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 211);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);

        //qb7
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 219);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);

        //qb8
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 250);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(25,4,'xxxxx',0,L,1);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(3);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qb9
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 12);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt. Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis. Proin tempus libero dolor, nec rhoncus ipsum iaculis a. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',0,L,1);

        //qb10
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 57);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis',0,L,1);

        //qc1
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 97);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 106);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        //qc1a
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 122);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt. Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis. Proin tempus libero dolor, nec rhoncus ipsum iaculis a. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',0,L,1);

        //qc2
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(92, 164.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(92, 178);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        //qc3
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(92, 196.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(92, 210);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        //qc3a
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(92, 228);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(90.5, 241.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(4);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qc3b
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 12);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt. Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis. Proin tempus libero dolor, nec rhoncus ipsum iaculis a. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',0,L,1);

        //qd1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 140);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis',0,L,1);


        //qd2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 162);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis',0,L,1);

        //qd3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 185);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis',0,L,1);

        //qd4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 207);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis',0,L,1);

        //qd5
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 235);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Praesent vel lorem eu nisl congue tristique sit amet sit amet ipsum. Fusce dignissim mi eget arcu vulputate ornare. Nulla ullamcorper nisi id tortor mollis venenatis',0,L,1);


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(5);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qd6
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 12);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);


        //qd7
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 39.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);


        //qd8
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 62);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);


        //qd9
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 91.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);


        //qd10
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 116.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);

        //qd11
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 136.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);

        //qd12
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 161);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);

        //qd13
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 186.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);


        //qd14
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 211);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(6);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qe1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 18);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'11 years.',0,L,1);


        //qe2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 31);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum. Sed at lorem sed est condimentum tincidunt.',0,L,1);

        //qe3
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 57.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(108, 57.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 71);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,6.5,'                                                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at lorem sed est condimentum tincidunt.',0,L,0);


        //qe4
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(91.5, 84.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(104.5, 84.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(116, 95);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(70,4,'Lorem ipsum dolor',0,L,1);

        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(128, 102);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(70,4,'Lorem ipsum dolor',0,L,1);


        //qe5
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 128);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(70,4,'Lorem ipsum dolor',0,L,1);


        //qe6
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 158.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 169);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO


        //qf1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 196);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qf2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 204);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qf3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 217);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);

        //qf4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 226);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qf5
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 235);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qf6
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 242.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qf7
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 252.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(7);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qg1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 18);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'11 years.',0,L,1);


        //qg2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 27);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus quam non feugiat fermentum.',0,L,1);


        //qg3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 37);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qg4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 44.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qg5
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 52.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qg6
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 66);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(25,4,'xxxxxx',0,L,1);

        //qg7
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 77);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);


        //qh1
        //1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(90, 113);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);
        //2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(90, 122);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);
        //3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(90, 132);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);
        //4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(90, 141);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);
        //5
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(90, 150);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',0,L,1);

        //qh2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(90, 159);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(22,4,'xxxxxxxx',0,L,1);

        //qi1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 181);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,5,'xxxxxxxx',0,L,1);

        //qi2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 193.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,5,'xxxxxxxx',0,L,1);


        //qi3
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 202);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(120, 202);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO

        //qi4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(88, 218);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(110,5,'xxxxxxxx',0,L,1);


        //qi5
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(108.5, 236.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(167.5, 236.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO




    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(8);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        //qj1

        //qj2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 44.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elitccc.',0,L,1);


        //qj3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 44.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elitccc.',0,L,1);


        //qj4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 65);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,6,'Lorem ipsum dolor sit amet, consectetur adipiscing elitccc.',0,L,1);


        //qk1
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 96.8);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(115,7,'21/12/2019',0,L,1);


        //qk2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 105.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


        //qk3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 114);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


        //qk4
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 123);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


        //qk5
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 131);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


        //qk6
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 140.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);

        //qk7


        //qk8
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 162.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);



        //qk9
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 181.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);



        //ql1
        //*******YES
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(93, 218.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******YES
        //*******NO
                $pdf->SetFont('ZapfDingbats','','16');
                $pdf->SetXY(120, 218.5);
                $pdf->MultiCell(7,5,$tick,0 );
        //*******NO



        //ql2
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 229.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);



        //ql3
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(84, 245.5);
        $pdf->SetFillColor(255);
        $pdf->MultiCell(118,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L,1);


    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ///////////////////write Pdf on Page//////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(9);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(10);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
        $pdf->AddPage();
        $tplIdx = $pdf->importPage(11);
        $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////Get Pdf Page////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


$pdf->Output();



?>
