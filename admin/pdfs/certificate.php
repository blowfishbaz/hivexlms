<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

$post = array(array('id'=>'id_book_20210219_125736283900_87011'), array('id'=>'id_book_20210219_125736287900_86273'));
//$post = array(array('id'=>'id_book_20210219_125736283900_87011'));
use setasign\Fpdi\Fpdi;
require('fpdf.php');

require_once('merger/autoload.php');

// $pdf = new FPDF();
// $pdf->AddPage();
// $pdf->SetFont('Arial','B',16);
// $pdf->Cell(40,10,'Hello World! 123');
// $pdf->Output();

foreach ($post as $p) {

  $db->query("select * from ws_booked_on where id = ?");
  $db->bind(1,$p['id']);
  $data = $db->single();

  $db->query("select * from ws_accounts where id = ?");
  $db->bind(1,$data['account_id']);
  $account = $db->single();

  $db->query("select * from lms_course where id = ?");
  $db->bind(1,$data['course_id']);
  $course = $db->single();


  $pdf = new Fpdi();
  //my template

  $pdf->setSourceFile('templates/dummy.pdf');
  $pdf->AddPage();
  $tplIdx = $pdf->importPage(1);
  $pdf->useTemplate($tplIdx, $pdf_left, $pdf_top, $pdf_width);

  $pdf->SetFont('Helvetica','',12);
  // $pdf->SetTextColor(0, 0, 0);
  // $pdf->SetXY(20, 80);
  // $pdf->SetFillColor(255);
  // $pdf->MultiCell(115,6,$account['first_name'].' '.$account['surname'],0,L,1);

  // $pdf->SetFont('Helvetica','',12);
  // $pdf->SetTextColor(0, 0, 0);
  // $pdf->SetXY(30, 95);
  // $pdf->SetFillColor(255);
  // $pdf->MultiCell(115,6,ucwords($course['title']),0,L,1);

  $pdf->SetXY(0, 40);
  $pdf->MultiCell(0,0, $account['first_name'].' '.$account['surname'] , $border=0, $align='C', $fill=false);

  $pdf->SetXY(0, 120);
  $pdf->MultiCell(0,0, $course['title'] , $border=0, $align='C', $fill=false);

  $pdf->SetFont('Helvetica','',12);
  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetXY(25, 265);
  $pdf->SetFillColor(255);
  $pdf->MultiCell(115,6,date('d-m-Y',$data['completed_date']),0,L,1);



  $path = $fullpath."uploads/pdf/".$account['id'].'/'.$course['id'].'/';
  $name = 'certificate_'.date('d_m_y_H_i_s').'.pdf';
  $location = $path.$name;


  if (!file_exists($path)) {
   mkdir($path, 0777, true);
   //error_log('No');+
  }else{
     //error_log('yes');
  }

  $db->query("insert into ws_pdf (id, account_id, course_id, path, file_name, full_path, download_path, created_date) VALUES (?,?,?,?,?,?,?,?)");
  $db->bind(1,createid('cert'));
  $db->bind(2,$account['id']);
  $db->bind(3,$course['id']);
  $db->bind(4,$path);
  $db->bind(5,$name);
  $db->bind(6,$location);
  $db->bind(7,"uploads/pdf/".$account['id'].'/'.$course['id'].'/');
  $db->bind(8,time());
  $db->execute();

  //echo $location;

  //$pdf->Output();
  $pdf->Output('F',$location);
}
