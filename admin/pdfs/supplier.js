$( document ).ready(function() {
    var $table = $('#table');
    	 var $sixty = $( document ).height();
        $sixty = ($sixty/100)*70;
         $table.bootstrapTable( 'resetView' , {height: $sixty} );

    $(function () {
        $table.on('click-row.bs.table', function (e, row, $element) {
            $('.success').removeClass('success');
            $($element).addClass('success');
        });
    });

     $(function () {
        $table.on('dbl-click-row.bs.table', function (e, row, $element) {
           var id = row['id'];
					 $("#BaseModalLContent").html($Loading);
				      $('#BaseModalL').modal('show');

				 		 $.ajax({
				 		       type: "POST",
				 		       url: "../../../assets/app_forms/stock_management/edit_supplier.php?id="+id,
				 		       data: {id:1},
				 		       success: function(msg){
				 						 setTimeout(function(){
				 				  	 $("#BaseModalLContent").html(msg);
				 				    },600);
				 		      }
				 		 })
        });
    });



        $(function () {
        $('#toolbar').find('select').change(function () {
            $table.bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
    });


    $(window).resize(function() {
		 var $sixty = $( document ).height();
        $sixty = ($sixty/100)*70;
         $table.bootstrapTable( 'resetView' , {height: $sixty} );
	});
});


function StatusFormatter(value, row, index) {
  if(value=='1'){
    return "<span class='label label-success'>Live</span>";
  }
  else{
    return "<span class='label label-danger'>Closed</span>";
  }
}
$( "body" ).on( "click", "#New_Supplier", function() {
  $("#BaseModalMContent").html($Loading);
     $('#BaseModalL').modal('show');

		 $.ajax({
		       type: "POST",
		       url: "../../../assets/app_forms/stock_management/new_supplier.php",
		       data: {id:1},
		       success: function(msg){
						 setTimeout(function(){
				  	 $("#BaseModalLContent").html(msg);
				    },600);
		      }
		 })


  });

$( "body" ).on( "click", "#Add_Supplier_but", function() {
										$('has-error').removeClass('has-error');
										var formid = '#Add_Supplier_Form';
										var HasError = 0;
										$(formid).find('input').each(function(){
											$(this).parent().removeClass('has-error');
											Messenger().hideAll();
											if(!$.trim(this.value).length) { // zero-length string AFTER a trim
															 if(!$(this).prop('required')){
													} else {
														HasError = 1;
														$(this).parent().addClass('has-error');
                          }
												}
										});

										if (HasError == 1) {
											Messenger().post({
													message: 'Please make sure all required elements of the form are filled out.',
													type: 'error',
													showCloseButton: false
											});
										} else {
											var FRMdata = $(formid).serialize(); // get form data
												$.ajax({
															 type: "POST",
															 url: "../../../assets/app_ajax/stock_management/new_supplier.php", // PHP save page
															 data: FRMdata, // serializes the form's elements.
															 success: function(data){
																	var msg;
																	msg = Messenger().post({
																		message: 'The supplier has been added.',
																		type: 'info'
																	});
																	setTimeout(function(){
																		location.reload();
																	},1600);
															 },error: function (xhr, status, errorThrown) {
								                      //setTimeout(function(){$('#BaseModalL').modal('hide');},300);
								                      Messenger().post({
								                         message: 'An Error Has occured.',
								                         type: 'error',
								                         showCloseButton: false
								                 });
								                    }
														 });
											 }
									});


$( "body" ).on( "click", "#Edit_Supplier_but", function() {
										$('has-error').removeClass('has-error');
										var formid = '#Add_Supplier_Form';
										var HasError = 0;
										$(formid).find('input').each(function(){
											$(this).parent().removeClass('has-error');
											Messenger().hideAll();
											if(!$.trim(this.value).length) { // zero-length string AFTER a trim
															 if(!$(this).prop('required')){
													} else {
														HasError = 1;
														$(this).parent().addClass('has-error');
                          }
												}
										});

										if (HasError == 1) {
											Messenger().post({
													message: 'Please make sure all required elements of the form are filled out.',
													type: 'error',
													showCloseButton: false
											});
										} else {
											var FRMdata = $(formid).serialize(); // get form data
												$.ajax({
															 type: "POST",
															 url: "../../../assets/app_ajax/stock_management/edit_supplier.php", // PHP save page
															 data: FRMdata, // serializes the form's elements.
															 success: function(data){
																	var msg;
																	msg = Messenger().post({
																		message: 'The supplier has been eddited.',
																		type: 'info'
																	});
																	setTimeout(function(){
																		location.reload();
																	},1600);
															 },error: function (xhr, status, errorThrown) {
								                      //setTimeout(function(){$('#BaseModalL').modal('hide');},300);
								                      Messenger().post({
								                         message: 'An Error Has occured.',
								                         type: 'error',
								                         showCloseButton: false
								                 });
								                    }
														 });
											 }
									});

									$( "body" ).on( "click", "#Remove_Supplier_but", function() {
																			var formid = '#Add_Supplier_Form';
																			var FRMdata = $(formid).serialize(); // get form data
																					$.ajax({
																								 type: "POST",
																								 url: "../../../assets/app_ajax/stock_management/remove_supplier.php", // PHP save page
																								 data: FRMdata, // serializes the form's elements.
																								 success: function(data){
																										var msg;
																										msg = Messenger().post({
																											message: 'The supplier has been Removed.',
																											type: 'info'
																										});
																										setTimeout(function(){
																											location.reload();
																										},1600);
																								 },error: function (xhr, status, errorThrown) {
																	                      //setTimeout(function(){$('#BaseModalL').modal('hide');},300);
																	                      Messenger().post({
																	                         message: 'An Error Has occured.',
																	                         type: 'error',
																	                         showCloseButton: false
																	                 });
																	                    }
																							 });

																		});
