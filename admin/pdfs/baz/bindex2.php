<?
include($_SERVER['DOCUMENT_ROOT'].'/application.php');

// use Spipu\Html2Pdf\Html2Pdf;
// use Spipu\Html2Pdf\Exception\Html2PdfException;
// use Spipu\Html2Pdf\Exception\ExceptionFormatter;
//
// require_once("b_pdf/Html2Pdf.php");
//
// $html2pdf = new Html2Pdf('P','A4','en',true,"UTF-8",array(10, 10, 15, 16));
//
// $homepage = file_get_contents('ex2.php');
//
// $html2pdf->writeHTML($homepage);
//
// $html2pdf->output('test.pdf');




function correct_date($ckdate="") {
	date_default_timezone_set("Europe/London");
	if($ckdate==""){
		$date= date('H:i d/m/Y');
	}else{
		$date= date('H:i d/m/Y',$ckdate);
	}
	date_default_timezone_set("UTC");
	return $date;
}




$db->query("select * from hx_technical_survey where id = ?");
$db->bind(1,$_GET['id']);
$TS = $db->single();


$db->query("select * from accounts where id = ?");
$db->bind(1,$TS['created_by']);
$created_by = $db->single();

$db->query("select COUNT(*) as total from hx_technical_survey_uploads where tsid = ? order by id ASC");
$db->bind(1,$TS['id']);
$TS_photos = $db->single();

$db->query("select COUNT(*) as total from hx_technical_survey_uploads where tsid = ? order by id ASC");
$db->bind(1,$TS['id'].'_b');
$TS_benefits = $db->single();

$db->query("select * from hx_technical_survey_form where pid = ? and status = '1'");
$db->bind(1,$TS['id']);
$TS_form = $db->single();

$db->query("select * from hx_technical_survey_deemed_form where pid = ? and status = '1'");
$db->bind(1,$TS['id']);
$TS_deemed = $db->single();

?>
<li>
<i class="milestone-primary glyphicon glyphicon-arrow-right milestone-tab"></i>
    Technical Survey
    <div class="panel panel-default">
      <div class="panel-heading">
        <strong>Survey Completed</strong>
        <?if (decrypt($_SESSION['SESS_ACCOUNT_ID']) =='id_member_20161122_181004522100_41319'||decrypt($_SESSION['SESS_ACCOUNT_ID'])=='id_member_20160914_093547857300_53991'){?>
        <button class="btn btn-raised btn-danger del_job_action btn-sm pull-right" data-id="" style="margin: -5px 0 0 0;">Delete Job Action</button>
        <?}?>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          Survey by:
          <strong><? echo $created_by['name'];?></strong>
        </li>
        <li class="list-group-item">
          Survey Complete Date:
          <strong></strong>
        </li>
        <li class="list-group-item">
          Survey Note:
          <strong><? echo nl2br($TS['complete_notes']);?></strong>
        </li>
      </ul>
    </div>

  <div class="panel panel-default" >
  <div class="panel-heading">
    <strong>Survey Form</strong>
  </div>
  <ul class="list-group">
    <?if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item">1	What type of boiler is currently in the property? <strong><? echo $TS_form['q1'];?></strong></li>
    <?}if($TS_form['q2']!='' && $TS_form['q2']!=NULL){?><li class="list-group-item">2	What make & model is the current boiler? <strong><? echo $TS_form['q2'];?></strong></li>
    <?}if($TS_form['q3']!='' && $TS_form['q3']!=NULL){?><li class="list-group-item">3	What kw is the existing boiler? <strong><? echo $TS_form['q3'];?></strong></li>
    <?}if($TS_form['q4']!='' && $TS_form['q4']!=NULL){?><li class="list-group-item">4	What is the boiler full serial number? <strong><? echo $TS_form['q4'];?></strong></li>
    <?}if($TS_form['q5']!='' && $TS_form['q5']!=NULL){?><li class="list-group-item">5	How old is the existing boiler? <strong><? echo $TS_form['q5'];?></strong></li>
    <?}if($TS_form['q6']!='' && $TS_form['q6']!=NULL){?><li class="list-group-item">6	Is the boiler in a cupboard or Loft or other? <strong><? echo $TS_form['q6'];?></strong></li>
    <?}if($TS_form['q6a_1_1']!='' && $TS_form['q6a_1_1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">6a.1	If Boiler in cupboard, what are the dimensions?</div>
        <div class="col-md-4 col-sm-4" style="text-align:center;">Height mm<br /><strong><? echo $TS_form['q6a_1_1'];?></strong></div>
        <div class="col-md-4 col-sm-4" style="text-align:center;">Width mm<br /><strong><? echo $TS_form['q6a_1_2'];?></strong></div>
        <div class="col-md-4 col-sm-4" style="text-align:center;">Depth mm<br /><strong><? echo $TS_form['q6a_1_3'];?></strong></div>
    </div></li>

    <?}if($TS_form['q6a_2']!='' && $TS_form['q6a_2']!=NULL){?><li class="list-group-item">6a.2	Will the new boiler fit in the cupboard or will the cupboard need to be removed/altered? Please detail if there are any other issues obstructing the new boiler installation. <strong><? echo $TS_form['q6a_2'];?></strong></li>
    <?}if($TS_form['q6b_1']!='' && $TS_form['q6b_1']!=NULL){?><li class="list-group-item">6b.1 If boiler is in a loft, can you confirm there are fixed ladders which are safe to use, adequate lighting and loft boarded for safe installation of the boiler. <strong><? echo $TS_form['q6b_1'];?></strong></li>
    <?}if($TS_form['q6b_1b']!='' && $TS_form['q6b_1b']!=NULL){?><li class="list-group-item">6b.1b	advise what customer needs to do <strong><? echo $TS_form['q6b_1b'];?></strong></li>
    <?}if($TS_form['q6b_2']!='' && $TS_form['q6b_2']!=NULL){?><li class="list-group-item">6b.2 If boiler is a combi, a separate filling loop, pressure guage and means for safe isolation of boiler will be to be located downstairs. <strong><? echo $TS_form['q6b_2'];?></strong></li>
    <?}if($TS_form['q6b_3']!='' && $TS_form['q6b_3']!=NULL){?><li class="list-group-item">6b.3 advise customer we will not be able to replace boiler until it is safe access into loft. <strong><? echo $TS_form['q6b_3'];?></strong></li>
    <?}if($TS_form['q6c']!='' && $TS_form['q6c']!=NULL){?><li class="list-group-item">6c If other please advise location <strong><? echo $TS_form['q6c'];?></strong></li>
    <?}if($TS_form['q7']!='' && $TS_form['q7']!=NULL){?><li class="list-group-item">7	Is the boiler wall or floor mounted <strong><? echo $TS_form['q7'];?></strong></li>
    <?}if($TS_form['q7a']!='' && $TS_form['q7a']!=NULL){?><li class="list-group-item">7a is their a new suitable position for wall hung boiler and confirmed with the customer? <strong><? echo $TS_form['q7a'];?></strong></li>
    <?}if($TS_form['q8']!='' && $TS_form['q8']!=NULL){?><li class="list-group-item">8 Confirm existing stop tap location and if its operational? Locate external stop tap if needed or advise customer to contact water board ASAP PRIOR to installation? <strong><? echo $TS_form['q8'];?></strong></li>
    <?}if($TS_form['q9a_1']!='' && $TS_form['q9a_1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">9 Confirm number of bathrooms and showers including shower Type</div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Bathroom Qty<br /><strong><? echo $TS_form['q9a_1'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Shower Qty<br /><strong><? echo $TS_form['q9b_1'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Shower Type<br /><strong><? echo $TS_form['q9b_2'];?></strong></div>
    </div></li>

    <?}if($TS_form['q10']!='' && $TS_form['q10']!=NULL){?><li class="list-group-item">10 What is the current flue type? <strong><? echo $TS_form['q10'];?></strong></li>
    <?}if($TS_form['q11']!='' && $TS_form['q11']!=NULL){?><li class="list-group-item">11 Are there any other boilers in the property? <strong><? echo $TS_form['q11'];?></strong></li>
    <?}if($TS_form['q11a']!='' && $TS_form['q11a']!=NULL){?><li class="list-group-item">11a	To identify the POPT score, you must clarify which is the main boiler that heats the majority of the property. Show clearly with measurements on the floor plan & identify which rooms heat up the radiators and which bathrooms are serviced by this boiler.<br /><strong><? echo nl2br($TS_form['q11a']);?></strong></li>
    <?}if($TS_form['q12']!='' && $TS_form['q12']!=NULL){?><li class="list-group-item">12 Location of second boiler? <strong><? echo $TS_form['q12'];?></strong></li>
    <?}if($TS_form['q13']!='' && $TS_form['q13']!=NULL){?><li class="list-group-item">13 Proposed new boiler type? <strong><? echo $TS_form['q13'];?></strong></li>
    <?}if($TS_form['q13a_1']!='' && $TS_form['q13a_1']!=NULL){?><li class="list-group-item">13a.1	If Regular boiler, does the pipework need upgrading to a fully pumped Y or S Plan system. <strong><? echo $TS_form['q13a_1'];?></strong></li>
    <?}if($TS_form['q13a_2']!='' && $TS_form['q13a_2']!=NULL){?><li class="list-group-item">13a.2 If upgrading to fully pumped, please confirm location for new Y/S Plan and Heating Pump<br /><strong><? echo nl2br($TS_form['q13a_2']);?></strong></li>
    <?}if($TS_form['q13a_3']!='' && $TS_form['q13a_3']!=NULL){?><li class="list-group-item">13a.3	Is the cold feed and expansion at the boiler <strong><? echo $TS_form['q13a_3'];?></strong></li>
    <?}if($TS_form['q13a_4']!='' && $TS_form['q13a_4']!=NULL){?><li class="list-group-item">13a.4 If no, please advise location<br /><strong><? echo nl2br($TS_form['q13a_4']);?></strong></li>
    <?}if($TS_form['q13a_5']!='' && $TS_form['q13a_5']!=NULL){?><li class="list-group-item">13a.5	Has the property got a cylinder? <strong><? echo $TS_form['q13a_5'];?></strong></li>
    <?}if($TS_form['q13a_6']!='' && $TS_form['q13a_6']!=NULL){?><li class="list-group-item">13a.6	Is the cylinder in good condition? <strong><? echo $TS_form['q13a_6'];?></strong></li>
    <?}if($TS_form['q13a_7']!='' && $TS_form['q13a_7']!=NULL){?><li class="list-group-item">13a.7	What type of cylinder is at the property? <strong><? echo $TS_form['q13a_7'];?></strong></li>
    <?}if($TS_form['q13a_8']!='' && $TS_form['q13a_8']!=NULL){?><li class="list-group-item">13a.8 If Direct/Primatic/leaking cylinder, it will need replacing or upgrade to Fully pumped System will not be possible. At this point choose whether new cylinder or conversion to combi is most feasible or better option. <strong><? echo $TS_form['q13a_8'];?></strong></li>
    <?}if($TS_form['q13a_9']!='' && $TS_form['q13a_9']!=NULL){?><li class="list-group-item">13a.9	Is the location of the cylinder on an external wall? <strong><? echo $TS_form['q13a_9'];?></strong></li>
    <?}if($TS_form['q13a_10']!='' && $TS_form['q13a_10']!=NULL){?><li class="list-group-item">13a.10	Is the cylinder accessible to be able to work on? <strong><? echo $TS_form['q13a_10'];?></strong></li>
    <?}if($TS_form['q13a_11']!='' && $TS_form['q13a_11']!=NULL){?><li class="list-group-item">13a.11	Is their a separate F&E Tank? <strong><? echo $TS_form['q13a_11'];?></strong></li>
    <?}if($TS_form['q13a_12']!='' && $TS_form['q13a_12']!=NULL){?><li class="list-group-item">13a.12	If No, cylinder is not Indirect, cylinder will need replacing or upgrade to Fully pumped System will not be possible. At this point choose whether new cylinder or conversion to combi is most feasible or better option. Ensure you advise of costs for both, conversion will be more expensive <strong><? echo $TS_form['q13a_12'];?></strong></li>
    <?}if($TS_form['q13a_13a']!='' && $TS_form['q13a_13a']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">13a.13	Advise of size of the cylinder required and all additional items.</div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Cylinder Size<br /><strong><? echo $TS_form['q13a_13a'];?></strong></div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Immersion Heater size<br /><strong><? echo $TS_form['q13a_13b'];?></strong></div>
    </div></li>
    <?}if($TS_form['q13a_14a']!='' && $TS_form['q13a_14a']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">13a.14	Confirm location, accessability and condition of existing heating pump/cylinder stat/wiring centre and 2 OR 3 Port Valve(s)</div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Location<br /><strong><? echo $TS_form['q13a_14a'];?></strong></div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Accessibility<br /><strong><? echo $TS_form['q13a_14b'];?></strong></div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Condition<br /><strong><? echo $TS_form['q13a_14c'];?></strong></div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Comments<br /><strong><? echo $TS_form['q13a_14d'];?></strong></div>
    </div></li>
    <?}if($TS_form['q13a_15']!='' && $TS_form['q13a_15']!=NULL){?><li class="list-group-item">13a.15 Is existing programmer working <strong><? echo $TS_form['q13a_15'];?></strong></li>
    <?}if($TS_form['q13b_1']!='' && $TS_form['q13b_1']!=NULL){?><li class="list-group-item">13b.1	If a BBU/Reg - Combi conversion, do we need to install a replacement radiator in room of decommissoned BBU? <strong><? echo $TS_form['q13b_1'];?></strong></li>
    <?}if($TS_form['q13b_2a']!='' && $TS_form['q13b_2a']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">13b.2	If yes, please advise of size, location and route of pipework?</div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Radiator (mm)<br /><strong><? echo $TS_form['q13b_2a'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Location<br /><strong><? echo $TS_form['q13b_2b'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Comment box for route and quantity of pipework<br /><strong><? echo $TS_form['q13b_2c'];?></strong></div>
    </div></li>
    <?}if($TS_form['q13b_3']!='' && $TS_form['q13b_3']!=NULL){?><li class="list-group-item">13b.3	Is water pressure suitable for combi conversion? Please ensure the water pressure is suitable before proceeding. Use flow cup to check.(Ltrs per minute) <strong><? echo $TS_form['q13b_3'];?></strong></li>
    <?}if($TS_form['q13b_4']!='' && $TS_form['q13b_4']!=NULL){?><li class="list-group-item">13b.4	If Booster pump and conversion to combi, advise customer this will have to be removed. Add additional time and materials to this job <strong><? echo $TS_form['q13b_4'];?></strong></li>
    <?}if($TS_form['q13b_5']!='' && $TS_form['q13b_5']!=NULL){?><li class="list-group-item">13b.5	Is current heating pipework a 1 or 2 pipe system? <strong><? echo $TS_form['q13b_5'];?></strong></li>
    <?}if($TS_form['q13b_6']!='' && $TS_form['q13b_6']!=NULL){?><li class="list-group-item">13b.6	Advise customer that they are required to upgrade to a 2 pipe system in order to install a combi boiler. If not possible due to cost - advise customer to leave as a regular boiler or convert BBU to Regular boiler <strong><? echo $TS_form['q13b_6'];?></strong></li>
    <?}if($TS_form['q14']!='' && $TS_form['q14']!=NULL){?><li class="list-group-item">14	Confirm new boiler location? If the boiler has to move location please explain in detail?<br /><strong><? echo nl2br($TS_form['q14']);?></strong></li>
    <?}if($TS_form['q14a']!='' && $TS_form['q14a']!=NULL){?><li class="list-group-item">14a Will the new boiler fit in the existing space with minimum clearances met ? Please detail if there are any issues obstructing the new boiler installation which requires removal or alterations? i.e., pipe boxing, boiler cupboard removal etc<br /><strong><? echo nl2br($TS_form['q14a']);?></strong></li>
    <?}if($TS_form['q14b']!='' && $TS_form['q14b']!=NULL){?><li class="list-group-item">14b	Confirm new flue type <strong><? echo $TS_form['q14b'];?></strong></li>
    <?}if($TS_form['q14c_1']!='' && $TS_form['q14c_1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">14c	If vertical, advise how many extensions, bends are required</div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Extensions Qty<br /><strong><? echo $TS_form['q14c_1'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">90 bends Qty<br /><strong><? echo $TS_form['q14c_2'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">45 Bends Qty<br /><strong><? echo $TS_form['q14c_3'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Pitched Flashing Qty<br /><strong><? echo $TS_form['q14c_4'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Flat Flashing Qty<br /><strong><? echo $TS_form['q14c_5'];?></strong></div>
    </div></li>
    <?}if($TS_form['q14d']!='' && $TS_form['q14d']!=NULL){?><li class="list-group-item">14d	Do you need to create supporting frames for the flue extensions to be clipped to? Advise in detail <strong><? echo $TS_form['q14d'];?></strong></li>
    <?}if($TS_form['q14e']!='' && $TS_form['q14e']!=NULL){?><li class="list-group-item">14e	Do we need to replace any of the existing roofing tiles i.e. If we having to relocate the flue for boundary clearances etc <strong><? echo $TS_form['q14e'];?></strong></li>
    <?}if($TS_form['q14f_1']!='' && $TS_form['q14f_1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">14f	Confirm type of colour and type of tile and quantity</div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Tile <br /><strong><? echo $TS_form['q14f_1'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Colour <br /><strong><? echo $TS_form['q14f_2'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">Quantity <br /><strong><? echo $TS_form['q14f_3'];?></strong></div>
    </div></li>
    <?}if($TS_form['q15']!='' && $TS_form['q15']!=NULL){?><li class="list-group-item">15	Can the engineer access the flue safely without the need for scaffolding, cherry picker? i.e. Vertical flue but cant use cat ladder etc <strong><? echo $TS_form['q15'];?></strong></li>
    <?}if($TS_form['q15a']!='' && $TS_form['q15a']!=NULL){?><li class="list-group-item">15a	If no, explain what we need to do and why? If cherry picker or scaffold advise customer there will be extra cost or they can source cheaper <strong><? echo $TS_form['q15a'];?></strong></li>
    <?}if($TS_form['q15a_1']!='' && $TS_form['q15a_1']!=NULL){?><li class="list-group-item">15a.1	Confirm route of new flue <strong><? echo $TS_form['q15a_1'];?></strong></li>
    <?}if($TS_form['q15a_2']!='' && $TS_form['q15a_2']!=NULL){?><li class="list-group-item">15a2	Does the flue meet boundary regulations from windows, doors, balconies, eaves, car ports or nuisance plume issues? <strong><? echo $TS_form['q15a_2'];?></strong></li>
    <?}if($TS_form['q15b_1']!='' && $TS_form['q15b_1']!=NULL){?><li class="list-group-item">15b.1	If no, explain what we need to do and why? If cherry picker or scaffold advise customer there will be extra cost or they can source cheaper <strong><? echo $TS_form['q15b_1'];?></strong></li>
    <?}if($TS_form['q15b_2a']!='' && $TS_form['q15b_2a']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">15b.2	If no, please advise if a relocation of flue or adeqaute plume kit to manufacturers specification and what materials are required?</div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">Comments <br /><strong><? echo $TS_form['q15b_2a'];?></strong></div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">Extensions Qty <br /><strong><? echo $TS_form['q15b_2b'];?></strong></div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">90 Bends Qty <br /><strong><? echo $TS_form['q15b_2c'];?></strong></div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">45 Bends Qty <br /><strong><? echo $TS_form['q15b_2d'];?></strong></div>
    </div></li>
    <?}if($TS_form['q16']!='' && $TS_form['q16']!=NULL){?><li class="list-group-item">16	Is a flue guard required? <strong><? echo $TS_form['q16'];?></strong></li>
    <?}if($TS_form['q16a']!='' && $TS_form['q16a']!=NULL){?><li class="list-group-item">16a	Is a brick up required? <strong><? echo $TS_form['q16a'];?></strong></li>
    <?}if($TS_form['q16b_a']!='' && $TS_form['q16b_a']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">16b	Please advise how many bricks, beeze blocks we need to get and type. Ensure customer is aware they need to purchase the bricks or we will supply closest matc</div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">Bricks Qty <br /><strong><? echo $TS_form['q16b_a'];?></strong></div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">Breeze Blocks Qty <br /><strong><? echo $TS_form['q16b_b'];?></strong></div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">Type of Brick	Qty <br /><strong><? echo $TS_form['q16b_c'];?></strong></div>
      <div class="col-md-3 col-sm-3" style="text-align:center;">Sand/Cement Bag Qty <br /><strong><? echo $TS_form['q16b_d'];?></strong></div>
    </div></li>
    <?}if($TS_form['q17']!='' && $TS_form['q17']!=NULL){?><li class="list-group-item">17 Do you need a stand off Bracket i.e. do pipes drop behind the boiler? <strong><? echo $TS_form['q17'];?></strong></li>
    <?}if($TS_form['q18']!='' && $TS_form['q18']!=NULL){?><li class="list-group-item">18 is there room to fit a Magnetic Filter? <strong><? echo $TS_form['q18'];?></strong></li>
    <?}if($TS_form['q18a']!='' && $TS_form['q18a']!=NULL){?><li class="list-group-item">18a	If no, please advise of best location if possible? <strong><? echo $TS_form['q18a'];?></strong></li>
    <?}if($TS_form['q19']!='' && $TS_form['q19']!=NULL){?><li class="list-group-item">19	Is the gas meter type sufficient for the replacement boiler and existing gas appliances in property and is the ECV accessible? If total gas appliances are above 60kw, customer needs to contact their energy supplier and advise of meter upgrade today. <strong><? echo $TS_form['q19'];?></strong></li>
    <?}if($TS_form['q19a']!='' && $TS_form['q19a']!=NULL){?><li class="list-group-item">19a	If no, please advise what the customer needs to do to proceed with boiler installation ASAP? <strong><? echo $TS_form['q19a'];?></strong></li>
    <?}if($TS_form['q20']!='' && $TS_form['q20']!=NULL){?><li class="list-group-item">20	What size is the gas pipe at the Gas Meter <strong><? echo $TS_form['q20'];?></strong></li>
    <?}if($TS_form['q20a']!='' && $TS_form['q20a']!=NULL){?><li class="list-group-item">20a	If Lead pipe OR 15mm pipe, confirm accordingly if a full new gas run is required or replace tail? Please explain fully what we need to do, i.e change the lead tail or new gas run? <strong><? echo $TS_form['q20a'];?></strong></li>
    <?}if($TS_form['q21']!='' && $TS_form['q21']!=NULL){?><li class="list-group-item">21	What size is the gas pipe at the boiler <strong><? echo $TS_form['q21'];?></strong></li>
    <?}if($TS_form['q22_a']!='' && $TS_form['q22_a']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">22	If required, what is the working gas pressure at the boiler? If not adequate pressure then new gas run will be needed?</div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Mbar Meter<br /><strong><? echo $TS_form['q22_a'];?></strong></div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">Mbar Boiler<br /><strong><? echo $TS_form['q22_b'];?></strong></div>
    </div></li>
    <?}if($TS_form['q23']!='' && $TS_form['q23']!=NULL){?><li class="list-group-item">23	Do we need to increase the pipe size or relocate the gas pipe for the new boiler? Comment on new route of gas pipe agreed with the customer, i.e. Internal/external or through rooms etc. <strong><? echo $TS_form['q23'];?></strong></li>
    <?}if($TS_form['q23a_1']!='' && $TS_form['q23a_1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">23a	If yes, please advise what size and how much extra we need to do, advise route of location for new gas pipe</div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">15mm Qty <br /><strong><? echo $TS_form['q23a_1'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">22mm Qty <br /><strong><? echo $TS_form['q23a_2'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">28mm Qty <br /><strong><? echo $TS_form['q23a_3'];?></strong></div>
    </div></li>
    <?}if($TS_form['q23b']!='' && $TS_form['q23b']!=NULL){?><li class="list-group-item">23b	Comment on new route of gas pipe agreed with the customer, i.e. Internal/external or through rooms etc <strong><? echo $TS_form['q23b'];?></strong></li>
    <?}if($TS_form['q24']!='' && $TS_form['q24']!=NULL){?><li class="list-group-item">24 Confirm all other gas appliances in property <strong><? echo $TS_form['q24'];?></strong></li>
    <?}if($TS_form['q25']!='' && $TS_form['q25']!=NULL){?><li class="list-group-item">25	Is their a suitable route for the blow off pipe to exit the building <strong><? echo $TS_form['q25'];?></strong></li>
    <?}if($TS_form['q25a']!='' && $TS_form['q25a']!=NULL){?><li class="list-group-item">25a	If no, please advise best option i.e condense and blow off pump or re-route? <strong><? echo $TS_form['q25a'];?></strong></li>
    <?}if($TS_form['q26']!='' && $TS_form['q26']!=NULL){?><li class="list-group-item">26	Is their a suitable route for the condense pipe to exit the building and into a suitable location <strong><? echo $TS_form['q26'];?></strong></li>
    <?}if($TS_form['q26a']!='' && $TS_form['q26a']!=NULL){?><li class="list-group-item">26a	If no, please advise best option i.e condense and blow off pump or re-route? <strong><? echo $TS_form['q26a'];?></strong></li>
    <?}if($TS_form['q27']!='' && $TS_form['q27']!=NULL){?><li class="list-group-item">27 Advise route and where the condese pipe is to be terminated <strong><? echo $TS_form['q27'];?></strong></li>
    <?}if($TS_form['q28']!='' && $TS_form['q28']!=NULL){?><li class="list-group-item">28 Confirm existing stop tap location and if its operational? Locate external stop tap if needed or advise customer to contact water board ASAP PRIOR to installation? <strong><? echo $TS_form['q28'];?></strong></li>
    <?}if($TS_form['q29']!='' && $TS_form['q29']!=NULL){?><li class="list-group-item">29 Are there any rooms/spaces which are not heated by the current boiler? <strong><? echo $TS_form['q29'];?></strong></li>

    <?}if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">29a If yes, please advise of size, location and route of pipework?</div>
      <div class="col-md-4 col-sm-4">Radiator 1 (mm) <strong><? echo $TS_form['q29a_1'];?></strong><br />Location <strong><? echo $TS_form['q29a_2'];?></strong></div>
      <div class="col-md-4 col-sm-4">Radiator 2 (mm) <strong><? echo $TS_form['q29a_3'];?></strong><br />Location <strong><? echo $TS_form['q29a_4'];?></strong></div>
      <div class="col-md-4 col-sm-4">Radiator 3 (mm) <strong><? echo $TS_form['q29a_5'];?></strong><br />Location <strong><? echo $TS_form['q29a_6'];?></strong></div>
      <div class="col-md-12 col-sm-12"><br />Comment on route and quantity of pipework?<br /><strong><? echo nl2br($TS_form['q29a_7']);?></strong></div>
    </div></li>
    <?}if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item">30 Do we need to replace or fit new TRV's (not apllicable on Towel rads and bypass rad)<strong><? echo $TS_form['q30'];?></strong></li>
    <?}if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">30a Confirm how many and what size?</div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">8/10mm Qty <br /><strong><? echo $TS_form['q30a_1'];?></strong></div>
      <div class="col-md-6 col-sm-6" style="text-align:center;">15mm Qty <br /><strong><? echo $TS_form['q30a_2'];?></strong></div>
    </div></li>

    <?}if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item">31 Confirm which room the bypass radiator is to be located in? <strong><? echo $TS_form['q31'];?></strong></li>
    <?}if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item">32 Do we need to replace or fit new Lockshield Valve? <strong><? echo $TS_form['q32'];?></strong></li>
    <?}if($TS_form['q1']!='' && $TS_form['q1']!=NULL){?><li class="list-group-item"><div class="row"><div class="col-md-12 col-sm-12">32a Confirm how many and what size?</div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">8/10mm Qty <br /><strong><? echo $TS_form['q32a_1'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">15mm Qty <br /><strong><? echo $TS_form['q32a_2'];?></strong></div>
      <div class="col-md-4 col-sm-4" style="text-align:center;">15mm x 3/4 Qty <br /><strong><? echo $TS_form['q32a_3'];?></strong></div>
    </div></li>
    <?}if($TS_form['q33']!='' && $TS_form['q33']!=NULL){?><li class="list-group-item">33 Is there an existing thermostat in the property suitable to be re-used? For combi boilers, the existing stat needs to be Boiler plus compatible. <strong><? echo $TS_form['q33'];?></strong></li>
    <?}if($TS_form['q34']!='' && $TS_form['q34']!=NULL){?><li class="list-group-item">34	Confirm location of Thermostat is in same room as the bypass radiator and agreed with the customer <strong><? echo $TS_form['q34'];?></strong></li>
    <?}if($TS_form['q35']!='' && $TS_form['q35']!=NULL){?><li class="list-group-item">35 Is the current boiler connected to a fuse spur? <strong><? echo $TS_form['q35'];?></strong></li>
    <?}if($TS_form['q36']!='' && $TS_form['q36']!=NULL){?><li class="list-group-item">36 Is the main earth bonding adequate? <strong><? echo $TS_form['q36'];?></strong></li>
    <?}if($TS_form['q37']!='' && $TS_form['q37']!=NULL){?><li class="list-group-item">37 Does the system need wiring for a existing/new Y or S Plan system <strong><? echo $TS_form['q37'];?></strong></li>
    <?}if($TS_form['q38']!='' && $TS_form['q38']!=NULL){?><li class="list-group-item">38 Any other electrical for the boiler instalation to go ahead<br /><strong><? echo nl2br($TS_form['q38']);?></strong></li>
    <?}if($TS_form['q39']!='' && $TS_form['q39']!=NULL){?><li class="list-group-item">39 Confirm measurement of current loft insulation in property <strong><? echo $TS_form['q39'];?></strong></li>
    <?}if($TS_form['q39a']!='' && $TS_form['q39a']!=NULL){?><li class="list-group-item">39a	Measure loft Hatch (mm) <strong><? echo $TS_form['q39a'];?></strong></li>
    <?}if($TS_form['q39b']!='' && $TS_form['q39b']!=NULL){?><li class="list-group-item">39b	Is there anything the customer needs to move for us to lay new insulation. If so, please advise below<br /><strong><? echo nl2br($TS_form['q39b']);?></strong></li>
    <?}if($TS_form['customer_information']!='' && $TS_form['customer_information']!=NULL){?><li class="list-group-item">Customer Information<br /><strong><? echo nl2br($TS_form['customer_information']);?></strong></li>
    <?}if($TS_form['installer_information']!='' && $TS_form['installer_information']!=NULL){?><li class="list-group-item">Installer Information<br /><strong><? echo nl2br($TS_form['installer_information']);?></strong></li><?}?>
  </ul>
</div>
