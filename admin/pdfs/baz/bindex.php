<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */


include($_SERVER['DOCUMENT_ROOT'].'/application.php');





//global $supplier;
require('../fpdf.php');

//////////////////PDF CLASS START///////////////////////////
//////////////////PDF CLASS START///////////////////////////
class PDF extends FPDF{
//////////////////PDF CLASS START///////////////////////////
//////////////////PDF CLASS START///////////////////////////

                function Footer()
                {
                    $this->SetY(-15);
                    $this->SetFont('Arial','B',8);
                    $this->Cell(20,10,'February 2019',r,0,'L');
                    $this->SetFont('Arial','',8);
                    $this->Cell(0,10,'Version 3.3',0,0,'L');
                    $this->SetTextColor(128);
                    $this->SetFont('Times','B',8);
                    $this->SetFont('Arial','B',11);
                    $this->Cell(0,10,$this->PageNo(),0,0,'C');

                }


                function ChapterBody()
                {
                                ////////////////////////////page 1//////////////////////////////////////////////////////////////////////////////
                               // $this->Image('http://10.88.88.116/uploads/temp/ChildBenefitSelf-Declaration_page1.png',-10,-10,230,310);
                               // $this->Ln(270);

                                ////////////////////////////page 2 part 1////////////////////////////////////////////////////////////////////////
                                $this->SetFont('Arial','B',14);
                                $this->SetY(12);$this->SetX(10);
                                $this->MultiCell(190,5,'Energy Company Obligation (ECO3) Boiler Assessment Checklist',0,C);
                                $this->Ln();

                               $this->Image('http://10.88.88.116/uploads/temp/BoilerAssessmentChecklist_page2.png',31,NULL,150,75);
                               $this->Ln();

                                ////////////////////////////page 2 part 2////////////////////////////////////////////////////////////////////////

                                ///Dark Question Line///
                                $this->SetFont('Arial','B',10);
                                $this->SetFillColor(100,100,100);
                                $this->SetTextColor(255);
                                $this->SetDrawColor(0,0,0);
                                $this->SetLineWidth(.3);
                                $this->Cell(7,7,'A.',1,false,L,1);
                                $this->SetFont('','B');
                                $this->Cell(NULL,7,' All boilers: Details of assessment (including FTCH measure)',1,false,L,1);
                                $this->Ln();
                                $this->SetFillColor(200);
                                $this->SetTextColor(0);
                                ///Dark Question Line///


                                ///Row 1.1///////////////
                                ///Draw Row///////////////
                                $this->Cell(7,18,'',1,false,C,1);
                                $this->Cell(48,18,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,18,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                $this->Ln(-3);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(110);$this->SetX(11);
                                $this->MultiCell(7,5,'1',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(110);$this->SetX(17);
                                $this->MultiCell(45,5,'Date of assessment (dd/mm/yyyy) ',0,L);
                                $this->SetY(110);$this->SetX(65);
                                $this->MultiCell(135,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////

                                ///Row 1.2///////////////
                                ///Draw Row///////////////
                                $this->Ln();
                                $this->Cell(7,25,'',1,false,C,1);
                                $this->Cell(48,25,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,25,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                $this->Ln(-3);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(121);$this->SetX(11);
                                $this->MultiCell(7,5,'2',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(121);$this->SetX(17);
                                $this->MultiCell(45,5,'Address:(Building number/name, Streetname, Town, City, County) ',0,L);
                                $this->SetY(121);$this->SetX(65);
                                $this->MultiCell(121,5,'sdfsdf sjdlkfj sdfsdf sdf sdf sdfsdf sdfsfd dfsg sdfg dfg dsfgsdfsdf sjdlkfj sdfsdf sdf sdf sdfsdf sdfsfd dfsg sdfg dfg dsfgsdfsdf sjdlkfj sdfsdf sdf sdf sdfsdf sdfsfd dfsg sdfg dfg dsfgsdfsdf sjdlkfj sdfsdf sdf sdf sdfsdf sdfsfd dfsg sdfg dfg dsfgsdfsdf sjdlkfj sdfsdf sdf sdf sdfsdf sdfsfd dfsg sdfg dfg dsfg',0,L);
                                ///content///////////////
                                ///Row///////////////

                                ///Row 1.3///////////////
                                ///Draw Row///////////////
                                $this->Ln(4);
                                $this->Cell(7,10,'',1,false,C,1);
                                $this->Cell(48,10,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,10,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                $this->Ln(-3);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(147);$this->SetX(11);
                                $this->MultiCell(7,5,'3',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(147);$this->SetX(17);
                                $this->MultiCell(45,5,'Postcode ',0,L);
                                $this->SetY(147);$this->SetX(65);
                                $this->MultiCell(135,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////


                                ///Dark Question Line///
                                $this->Ln(3);
                                $this->SetFont('Arial','B',10);
                                $this->SetFillColor(100,100,100);
                                $this->SetTextColor(255);
                                $this->SetDrawColor(0,0,0);
                                $this->SetLineWidth(.3);
                                $this->Cell(7,5,'B.',TLR,false,L,1);
                                $this->SetFont('','B');
                                $this->Cell(NULL,5,' All boilers: Existing boiler details (For FTCH measures, please record details of theexisting pre-main',TLR,false,L,1);
                                $this->Ln();
                                $this->Cell(7,5,'',LBR,false,L,1);
                                $this->SetFont('','B');
                                $this->Cell(NULL,5,' heating source in B10)',LBR,false,L,1);
                                $this->Ln();
                                $this->SetFillColor(200);
                                $this->SetTextColor(0);
                                ///Dark Question Line///

                                ///Row 2.1///////////////
                                ///Draw Row///////////////
                                $this->Cell(7,9,'',1,false,C,1);
                                $this->Cell(48,9,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,9,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                $this->Ln(-3);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(167);$this->SetX(11);
                                $this->MultiCell(7,5,'1',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(167);$this->SetX(17);
                                $this->MultiCell(45,5,'Brand And Model ',0,L);
                                $this->SetY(167);$this->SetX(65);
                                $this->MultiCell(135,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////

                                ///Row 2.2///////////////
                                ///Draw Row///////////////
                                $this->Ln(2);
                                $this->Cell(7,9,'',1,false,C,1);
                                $this->Cell(48,9,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,9,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(176);$this->SetX(11);
                                $this->MultiCell(7,5,'2',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(176);$this->SetX(17);
                                $this->MultiCell(45,5,'Model qualifier (if applicable) ',0,L);
                                $this->SetY(176);$this->SetX(65);
                                $this->MultiCell(135,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////


                                ///Row 2.3///////////////
                                ///Draw Row///////////////
                                $this->Ln(1);
                                $this->Cell(7,9,'',1,false,C,1);
                                $this->Cell(48,9,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,9,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(184);$this->SetX(11);
                                $this->MultiCell(7,5,'3',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(184);$this->SetX(17);
                                $this->MultiCell(45,5,'Fuel type ',0,L);
                                $this->SetY(184);$this->SetX(65);
                                $this->MultiCell(135,5,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////

                                ///Row 2.4///////////////
                                ///Draw Row///////////////
                                $this->Ln(1);
                                $this->Cell(7,13,'',1,false,C,1);
                                $this->Cell(48,13,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,13,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(193);$this->SetX(11);
                                $this->MultiCell(7,4,'4',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(193);$this->SetX(17);
                                $this->MultiCell(45,4,'Year of original commissioning (if available) ',0,L);
                                $this->SetY(193);$this->SetX(65);
                                $this->MultiCell(135,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////


                                ///Row 2.5///////////////
                                ///Draw Row///////////////
                                $this->Ln(6);
                                $this->Cell(7,9,'',1,false,C,1);
                                $this->Cell(48,9,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,9,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(205);$this->SetX(11);
                                $this->MultiCell(7,4,'5',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(205);$this->SetX(17);
                                $this->MultiCell(45,4,'Is it a combination boiler?',0,L);
                                $this->SetY(205);$this->SetX(65);
                                $this->MultiCell(135,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////


                                ///Row 2.6///////////////
                                ///Draw Row///////////////
                                $this->Ln(3);
                                $this->Cell(7,9,'',1,false,C,1);
                                $this->Cell(48,9,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,9,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(213);$this->SetX(11);
                                $this->MultiCell(7,4,'6',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(213);$this->SetX(17);
                                $this->MultiCell(48,4,'Boiler Location (PreInstallation)',0,L);
                                $this->SetY(213);$this->SetX(65);
                                $this->MultiCell(135,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////


                                ///Row 2.7///////////////
                                ///Draw Row///////////////
                                $this->Ln(4);
                                $this->Cell(7,9,'',1,false,C,1);
                                $this->Cell(48,9,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,9,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(223);$this->SetX(11);
                                $this->MultiCell(7,4,'7',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(223);$this->SetX(17);
                                $this->MultiCell(45,4,'Serial Number',0,L);
                                $this->SetY(223);$this->SetX(65);
                                $this->MultiCell(135,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////


                                ///Row 2.8///////////////
                                ///Draw Row///////////////
                                $this->Ln(3);
                                $this->Cell(7,34,'',1,false,C,1);
                                $this->Cell(48,34,'',1,false,L,1);
                                $this->SetFillColor(255);
                                $this->Cell(135,34,' ',1,false,L,1);
                                $this->SetFillColor(200);
                                ///Draw Row///////////////
                                ///content///////////////
                                $this->SetFont('Arial','B',11);
                                $this->SetY(231);$this->SetX(11);
                                $this->MultiCell(7,4,'8',0,L);
                                $this->SetFont('Arial','',9);
                                $this->SetY(231);$this->SetX(17);
                                $this->MultiCell(45,4,'Boiler efficiency (%): Provideefficiency when assessedagainst PCDB/SAP 2012(Provide annual efficiency ofthe boiler from PCDB, theefficiency from table 4a orwinter efficiency from table 4bof SAP 2012)4.',0,L);
                                $this->SetY(231);$this->SetX(65);
                                $this->MultiCell(135,4,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',0,L);
                                ///content///////////////
                                ///Row///////////////



                }




//////////////////PDF CLASS END///////////////////////////
//////////////////PDF CLASS END///////////////////////////
}
//////////////////PDF CLASS END///////////////////////////
//////////////////PDF CLASS END///////////////////////////



// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
$pdf->ChapterBody();
$title = 'Energy Company Obligation ECO3 Boiler Assessment Checklist';
$pdf->SetTitle($title);
$pdf->Output();




?>
