<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Jobs';

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->

</head>

<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>

            				<!--- PAGE CONTENT HERE ---->
						<div class="page_title text-capitalize">
              <span class="menuglyph glyphicons glyphicons-building" aria-hidden="true"></span><span class="menuText">Credit</span>
              <a href="index.php" class="btn btn-primary btn-raised pull-right">View log</a>
          </div>
                        <div class="clearfix"></div>


                        <div class="panel panel-default">
                               <div class="panel-body">

                                 <table id="table" class="striped"
                                 data-toggle="table"
                                 data-show-export="true"
                                 data-export-types="['excel']"
                                 data-click-to-select="true"
                                 data-filter-control="true"
                                 data-show-columns="true"
                                 data-show-refresh="true"
                                 data-url="<? echo $fullurl; ?>admin/accounts/app_ajax/tables/company.php"
                                 data-height="400"
                                 data-side-pagination="server"
                                 data-pagination="true"
                                 data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                 data-search="true">
                                         <thead>
                         <tr>
                                 <th data-field="company_name" data-sortable="true" data-visible="true" data-formatter="LinkCompanyFormatter2">Company Name</th>
                                 <th data-field="credit" data-sortable="true" data-visible="true">Credits</th>
                         </tr>
                                         </thead>
                                 </table>



                               </div>
                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl ?>assets/js/font.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>

<script>
function 	LinkCompanyFormatter2(value, row, index) {
    return '<a data-id="'+row.id+'" href="javascript:void(0)" class="text-capitalize manage_company_credit_">'+value+'</a>';
}


</script>
