<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Pages';

 $db = new database;
 $db->query("select * from hx_pages where id = ? ");
 $db->bind(1,$_GET['id']);
 $page = $db->single();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>


<!-- On Page CSS -->

</head>

<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>

            				<!--- PAGE CONTENT HERE ---->
						<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span><span class="menuText">page edit</span></div>
                        <div class="clearfix"></div>


                        <div class="panel panel-default userlist">
                               <div class="panel-body">

                                   <form id="EditPageForm" name="EditPageForm" method="post" enctype="application/x-www-form-urlencoded">

                                       <div class="form-group">
                                           <label class=“”>Page Name</label>
                                           <input type="text" name="page_name" class="form-control" id="page_name" value="<? echo $page['name'];?>">
                                           <input type="hidden" name="page_id" class="form-control" id="page_id" value="<? echo $page['id'];?>">
                                       </div>
                                       <div class="form-group">
                                               <label class=“”>Page Information</label>
                                               <textarea name="page_info" class="form-control" rows="5" id="page_info"><? echo $page['page_info'];?></textarea>
                                           </div>


                                   </form>



                                   <button type="button" class="btn btn-primary btn-raised pull-right" id="EditPageFormBut">Save<div class="ripple-container"></div></button>
                               </div>
                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script>
        $('#page_info').summernote({
        height: 300,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        placeholder:'Page information...',
        toolbar: [
        ['font', ['bold', 'italic', 'underline']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'hr']],
        ['help', ['help']]
        ]
        });


</script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
