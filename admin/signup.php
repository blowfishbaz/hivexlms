<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    order/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $created_date = time();

 $new_id = $_POST['rota_id'];
 $firstname = $_POST['first_name'];
 $surname = $_POST['surname'];
 $email = $_POST['email'];
 $password = $_POST['password'];

 $telephone = $_POST['telephone'];
 $mobile_number = $_POST['mobile_number'];
 $job_role = $_POST['job_role'];
 $location = $_POST['location'];

 $bank_name = $_POST['bank_name'];
 $bank_account_number = $_POST['account_number'];
 $bank_sort_code = $_POST['sort_code'];

 if(!empty($bank_name)){
    $bank_name = encrypt($bank_name);
}

if(!empty($bank_account_number)){
   $bank_account_number = encrypt($bank_account_number);
}

if(!empty($bank_sort_code)){
   $bank_sort_code = encrypt($bank_sort_code);
}


 $db->query("SELECT email FROM ws_accounts WHERE id = ? OR email = ?");
 $db->bind(1,$new_id);
 $db->bind(2,$email);
 $accounts = $db->single();
 $acount_num = $db->rowcount();

 $db->query("SELECT email FROM accounts WHERE id = ? OR email = ?");
 $db->bind(1,$new_id);
 $db->bind(2,$email);
 $accounts_main = $db->single();
 $acount_num_main = $db->rowcount();

 $acount_num = $acount_num + $acount_num_main;

 if($acount_num==0){
   $account_id = createid('member');
   $db = new database;
   $db->Query("INSERT INTO accounts (id ,created_date ,name, email, type, status,username,password, account_type, user_status) values (?,?,?,?,?,?,?,?,?,?)");
   $db->bind(1,$new_id);
   $db->bind(2,$created_date);
   $db->bind(3,$firstname.' '.$surname);
   $db->bind(4,$email);
   $db->bind(5,'admin');
   $db->bind(6,'1');
   $db->bind(7,encrypt($email));
   $db->bind(8,encrypt($password));
   $db->bind(9,'user');
   $db->bind(10,'2');
   $db->execute();


   $db = new database;
   $db->Query("INSERT INTO ws_accounts (id ,created_date ,first_name ,surname, email, username, password, attempts,lastattempt,profilepic,status,pid, rota) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
   $db->bind(1,$new_id);
   $db->bind(2,$created_date);
   $db->bind(3,$firstname);
   $db->bind(4,$surname);
   $db->bind(5,$email);
   $db->bind(6,encrypt($email));
   $db->bind(7,encrypt($password));
   $db->bind(8,'0');
   $db->bind(9,$created_date);
   $db->bind(10,'');
   $db->bind(11,'2');
   $db->bind(12,$new_id);
   $db->bind(13,'1');
   $db->execute();

   $db = new database;
   $db->Query("insert into ws_accounts_extra_info (id, account_id, company_id, main_provision, created_date, created_by, mobile_number, telephone, job_title, status, account_type, bank_name, bank_account_number, bank_sort_code) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
   $db->bind(1,createid('ainfo'));
   $db->bind(2,$new_id);
   $db->bind(3,'N/A');
   $db->bind(4,'N/A');
   $db->bind(5,$created_date);
   $db->bind(6,$new_id);
   $db->bind(7,$mobile_number);
   $db->bind(8,$telephone);
   $db->bind(9,'');
   $db->bind(10,'1');
   $db->bind(11,'Learner');
   $db->bind(12,$bank_name);
   $db->bind(13,$bank_account_number);
   $db->bind(14,$bank_sort_code);
   $db->execute();

   //ws_accounts_roles
   foreach ($job_role as $jb) {
     $db->Query("INSERT INTO ws_accounts_roles (id, role_id, account_id) VALUES (?,?,?)");
     $db->bind(1,createid('a_role'));
     $db->bind(2,$jb);
     $db->bind(3,$new_id);
     $db->execute();
   }

   //ws_accounts_locations
   foreach ($location as $loc) {
     $db->Query("INSERT INTO ws_accounts_locations (id, location_id, account_id) VALUES (?,?,?)");
     $db->bind(1,createid('a_loc'));
     $db->bind(2,$loc);
     $db->bind(3,$new_id);
     $db->execute();
   }

   echo 'ok';


 }else{
   if($accounts['email']==$email || $accounts_main['email']==$email){
     echo 'email already exists';
   }
   else{
     echo 'id already exists';
   }
 }


 //echo 'ok';
?>
