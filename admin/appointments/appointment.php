<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Appointment';

 $db = new database;
 $db->query("select * from ws_appointments where id = ? ");
 $db->bind(1,$_GET['id']);
 $app = $db->single();

 switch ($app['slot']) {
   case 'a1':$time='09:00:00';break;
   case 'a2':$time='10:00:00';break;
   case 'a3':$time='11:00:00';break;
   case 'a4':$time='12:00:00';break;
   case 'a5':$time='13:00:00';break;
   case 'a6':$time='14:00:00';break;
   case 'a7':$time='15:00:00';break;
   case 'a8':$time='16:00:00';break;
   default:$time='error';break;
 }

 $db = new database;
 $db->query("select * from ws_accounts where id = ? ");
 $db->bind(1,$app['pid']);
 $user = $db->single();

 $db->query("select * from ws_accounts_info where pid = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'1');
$info = $db->single();

$db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'cv');
$db->bind(3,'1');
$cv = $db->single();

$db->query("select * from ws_uploads where pid = ? and type = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'bds');
$db->bind(3,'1');
$bds = $db->single();

$db->query("select * from ws_addresses where pid = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'1');
$addresses = $db->single();


$db->query("select * from ws_accounts_qualifications where pid = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'1');
$qualifications = $db->ResultSet();

$db->query("select * from ws_accounts_employment where pid = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'1');
$employment = $db->ResultSet();

$db->query("select * from ws_accounts_references where pid = ? and status = ?");
$db->bind(1,$user['id']);
$db->bind(2,'1');
$references = $db->ResultSet();

$db->query("SELECT hx_jobs.*, ws_job_applications.id as app_id, ws_job_applications.status as app_status FROM ws_job_applications JOIN hx_jobs ON ws_job_applications.pid = hx_jobs.id where ws_job_applications.user_id = ? and (ws_job_applications.status = ? OR ws_job_applications.status = ? OR ws_job_applications.status = ? OR ws_job_applications.status = ?)");
$db->bind(1,$user['id']);
$db->bind(2,'1');
$db->bind(3,'2');
$db->bind(4,'3');
$db->bind(5,'0');
$jobs = $db->ResultSet();


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>

<!-- On Page CSS -->

</head>

<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>

            				<!--- PAGE CONTENT HERE ---->
						<div class="page_title text-capitalize">
              <span class="menuglyph glyphicons glyphicons-calendar" aria-hidden="true"></span><span class="menuText">Appointment - <?echo $user['first_name'].' '.$user['surname'];?></span>
              <button type="button" class="btn btn-success btn-raised pull-right" id="edit_appointment" data-id="<?echo $_GET['id'];?>">Edit Appointment</button>
          </div>
                        <div class="clearfix"></div>


                        <div class="panel panel-default userlist">
                               <div class="panel-body">
                                 <h3>Appointments</h3>
                                 <table class="table table-bordered table-striped">
                                      <tbody>
                                      <tr>
                                      <th>Appointment Date</th>
                                      <th>Appointment Time</th>
                                      <th>Appointment Status</th>
                                      </tr>


                                      <tr>
                                      <td><?echo date('d-m-Y',$app['app_date'])?></td>
                                      <td><?echo $time;?></td>
                                      <td><? if($app['status']=='1'){echo '<span class="label label-info">Booked</span>';}//
                                           else if($app['status']=='2'){echo '<span class="label label-success">Completed</span>';}//
                                           else if($app['status']=='0'){echo '<span class="label label-danger">Cancelled </span>';}//
                                           else{}
                                      ?></td>
                                      </tr>
                                      </tbody>
						</table>
                                   <h3>User Information</h3>
                                   <div class="row">
                                       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                   <table class="table table-bordered table-striped">
                                               <tbody>

                                                   <tr>
                                                     <th>Name</th>
                                                     <td><?echo $user['first_name'].' '.$user['surname'];?></td>
                                                   </tr>
                                                <tr>
                                                  <th>Address1</th>
                                                  <td><?echo $addresses['address1'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>address2</th>
                                                  <td><?echo $addresses['address2'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>address3</th>
                                                  <td><?echo $addresses['address3'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>city</th>
                                                  <td><?echo $addresses['city'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>county</th>
                                                  <td><?echo $addresses['county'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>postcode</th>
                                                  <td><?echo $addresses['postcode'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>telephone</th>
                                                  <td><?echo $addresses['telephone'];?></td>
                                                </tr>
                                                <tr>
                                                  <th>mobile</th>
                                                  <td><?echo $addresses['mobile'];?></td>
                                                </tr>
                                               </tbody>
                                             </table>
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                     <table class="table table-bordered table-striped">
                                                 <tbody>

                                                     <?if($info['infotype']=='talk'){?>
                                                       <tr>
                                                         <th>CV</th>
                                                         <td>Talk to our team</td>
                                                       </tr>
                                                       <?}else {?>
                                                     <tr>
                                                       <th>CV</th>
                                                       <td><a href="<?echo $fullurl.$cv['path'].$cv['name'];?>" target="_blank"><?echo $cv['name']?></a></td>
                                                     </tr>
                                                     <?}?>
                                                     <?if($bds['name']!=''){?>
                                                     <tr>
                                                       <th>DBS</th>
                                                       <td><a href="<?echo $fullurl.$bds['path'].$bds['name'];?>" target="_blank"><?echo $bds['name']?></a></td>
                                                     </tr>
                                                     <?}?>
                                                     <tr>
                                                       <th>Driving License</th>
                                                       <td><?echo $info['license'];?></td>
                                                     </tr>

                                                     <tr>
                                                       <th>Conviction</th>
                                                       <td><?echo $info['conviction'];?></td>
                                                     </tr>
                                                     <? if($info['conviction']=='yes'){?>
                                                     <tr>
                                                       <th>Conviction</th>
                                                       <td><?echo $info['conviction_info'];?></td>
                                                     </tr>
                                                    <? } ?>
                                                 </tbody>
                                               </table>
                                           </div>
                                           <div class="clearfix"></div>

                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                               <h3>Qualifications</h3>
                                       <table class="table table-bordered table-striped">
                                                   <tbody>
                                                       <tr>
                                                         <th>Qualification</th>
                                                          <th>Grade</th>
                                                           <th>Date</th>
                                                       </tr>
                                                       <?foreach ($qualifications as $key => $qual) {?>
                                                           <tr>
                                                             <td><?echo $qual['qualifications'];?></td>
                                                             <td><?echo $qual['grade'];?></td>
                                                             <td><?echo date('d-m-Y',$qual['qdate']);?></td>
                                                           </tr>
                                                           <?}?>
                                                   </tbody>
                                                 </table>

                                             </div>




                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                 <h3>Employment History</h3>
                                         <table class="table table-bordered table-striped">
                                                     <tbody>
                                                         <tr>
                                                           <th>Employer</th>
                                                            <th>Position</th>
                                                             <th>Date</th>
                                                             <th>Address</th>
                                                         </tr>
                                                         <?foreach ($employment as $key => $emp) {?>
                                                             <tr>
                                                               <td><?echo $emp['name'];?></td>
                                                               <td><?echo $emp['title'];?></td>
                                                               <td><?
                                                               echo date('d-m-Y',$emp['date_start']).' To ';
                                                               if($emp['date_end']=='0'){echo 'Present';}else{echo date('d-m-Y',$emp['date_end']);}
                                                               ?></td>
                                                               <td><?echo nl2br($emp['address']);?></td>
                                                             </tr>
                                                             <?}?>
                                                     </tbody>
                                                   </table>
                                               </div>

                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                   <h3>References</h3>
                                           <table class="table table-bordered table-striped">
                                                       <tbody>
                                                           <tr>
                                                             <th>Name</th>
                                                              <th>Contact</th>
                                                               <th>Position</th>
                                                               <th>Address</th>
                                                           </tr>
                                                           <?foreach ($references as $key => $ref) {?>
                                                               <tr>
                                                                 <td><?echo $ref['name'];?></td>
                                                                 <td><?echo $ref['contact'];?></td>
                                                                 <td><?echo $ref['position'];?></td>
                                                                 <td><?echo nl2br($ref['address']);?></td>
                                                               </tr>
                                                               <?}?>
                                                       </tbody>
                                                     </table>
                                                 </div>

                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                 <h3>Job Information</h3>
                                         <table class="table table-bordered table-striped">
                                                     <tbody>
                                                         <?foreach ($jobs as $key => $job) {

                                                             if($key!='0'){echo '<tr style="background-color:#ccc;"><th colspan="2">&nbsp;</th></tr>';}?>
                                                             <tr>
                                                               <th>Title</th>
                                                               <td><?echo $job['title'];?></td>
                                                             </tr>
                                                             <tr>
                                                               <th>Job Number</th>
                                                               <td>SR<?echo $job['job_number'];?></td>
                                                             </tr>
                                                             <tr>
                                                               <th>Company</th>
                                                               <td><?echo $job['company'];?></td>
                                                             </tr>
                                                             <tr>
                                                               <th>Job Type</th>
                                                               <td><?echo ucwords($job['job_type'].' '.$job['contract_type']);?></td>
                                                             </tr>


                                                             <tr>
                                                               <th>Salary</th>
                                                               <td><?if($job['salary_type']=='per annum'){?>
                         								&pound;<?echo number_format($job['per_annum']);?> Per Annum
                         								<?}else if($job['salary_type']=='pro rata'){?>
                         								&pound;<?echo number_format($job['per_annum']);?> Per Rata
                         								<?}else if($job['salary_type']=='per hour'){?>
                         									&pound;<?echo number_format($job['per_annum']);?> Per Rata,
                         									&pound;<?echo number_format($job['per_hourly'],2);?> Per Hour
                         									<?}else{}?></td>
                                                             </tr>
                                                             <tr>
                                                               <th>Job Application Status</th>
                                                               <td><? if($job['app_status']=='1'){echo '<span class="label label-info">Waiting</span>';}//
                                                                    else if($job['app_status']=='2'){echo '<span class="label label-info">Maybe</span>';}//
                                                                    else if($job['app_status']=='3'){echo '<span class="label label-success">Successful</span>';}//
                                                                    else if($job['app_status']=='0'){echo '<span class="label label-danger">Unsuccessful</span>';}//
                                                                    else{}
                                                               ?></td>
                                                             </tr>
                                                             <tr>
                                                               <th>Job Link</th>
                                                               <td class="pager">
                                                                   <button target="_blank" class="btn btn-danger btn-raised pull-left job_app_unsuccessful" data-id="<?echo $job['app_id'];?>">Unsuccessful</button>
                                                                    <button target="_blank" class="btn btn-info btn-raised job_app_maybe" data-id="<?echo $job['app_id'];?>">Maybe</button>
                                                                    <button target="_blank" class="btn btn-success btn-raised pull-right job_app_successful" data-id="<?echo $job['app_id'];?>">Successful</button>

                                                           </td>
                                                             </tr>

                                                             <tr>
                                                               <th>Job Page</th>
                                                               <td>
                                                                   <a href="<?echo $fullurl.'job.php?id='.$job['id'].'&preview';?>" target="_blank" class="btn btn-info btn-raised">Job Page</a>
                                                               </td>
                                                             </tr>



                                                         <?}?>
                                                     </tbody>
                                                   </table>

                                               </div>
                                        </div>
                               </div>

                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl ?>assets/js/font.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
