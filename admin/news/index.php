<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Pages';

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>


<!-- On Page CSS -->

</head>

<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>

            				<!--- PAGE CONTENT HERE ---->
						<div class="page_title text-capitalize"><span class="menuglyph glyphicons glyphicons-newspaper" aria-hidden="true"></span><span class="menuText">News</span>
                          <button type="button" class="btn btn-primary btn-raised pull-right" id="add_new_news">New News</button>
                      </div>
                        <div class="clearfix"></div>


                        <div class="panel panel-default userlist">
                               <div class="panel-body">
                                       <table id="table" class="striped"
                                       data-toggle="table"
                                       data-show-export="true"
                                       data-export-types="['excel']"
                                       data-click-to-select="true"
                                       data-filter-control="true"
                                       data-show-columns="true"
                                       data-show-refresh="true"
                                       data-url="app_ajax/list.php"
                                       data-height="400"
                                       data-side-pagination="server"
                                       data-pagination="true"
                                       data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
                                       data-search="true">
                                               <thead>
                               <tr>
                                       <th data-field="id" data-sortable="true" data-visible="false">ID</th>
                                       <th data-field="name" data-sortable="true" data-visible="true" data-formatter="LinkNewsFormatter">Name</th>
                                       <th data-field="news_date" data-sortable="true" data-visible="true" data-formatter="DateFormatter">Date</th>
                                       <th data-field="status" data-sortable="true" data-visible="true" data-formatter="StatusFormatter">Status</th>
                               </tr>
                                               </thead>
                                       </table>
                               </div>
                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <!-- <script src="<? echo $fullurl; ?>assets/js/table.js"></script> -->
    <script>
    $( document ).ready(function() {
        var $table = $('#table');
        	 var $sixty = $( document ).height();
            $sixty = ($sixty/100)*70;
             $table.bootstrapTable( 'resetView' , {height: $sixty} );

        $(function () {
            $table.on('click-row.bs.table', function (e, row, $element) {
                $('.success').removeClass('success');
                $($element).addClass('success');
            });
        });

        $(function () {
            $('#toolbar').find('select').change(function () {
                $table.bootstrapTable('refreshOptions', {
                    exportDataType: $(this).val()
                });
            });
        });

        $( window ).resize(function() {
    		 var $sixty = $( document ).height();
            $sixty = ($sixty/100)*70;
             $table.bootstrapTable( 'resetView' , {height: $sixty} );
    	});

    });

    function DateFormatter(value, row, index) {
      var a = new Date(value * 1000);
      //var a = new Date(a - 3600000);/* daylight saving???*/
      var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
      var year = a.getFullYear();
      var month = months[a.getMonth()];
      var date = a.getDate();

      if(year=='1970'){var time ='-'}else{
      var time =  date + ' ' + month + ' ' + year;
    }
      return time;
    }

    function 	LinkNewsFormatter(value, row, index) {
        return '<a href="#" data-id="'+row.id+'" class="text-capitalize edit_news">'+value+'</a>';
    }

    function StatusFormatter(value, row, index) {
     if(value=='1'){
        return "<span class='label label-success'>Live</span>";
      }
      else if(value=='0'){
         return "<span class='label label-warning'>Unpublish</span>";
       }
      else{
        return "<span class='label label-danger'>Error</span>";
      }
    }


$( "body" ).on( "click", "#add_new_news", function(e) {
   $("#BaseModalLContent").html($Loading);
   $('#BaseModalL').modal('show');
                    $.ajax({
                        type: "POST",
                        url: "app_ajax/new.php",
                        data: "id:1",
                        success: function(msg){

                           //alert(msg);
                           $("#BaseModalLContent").delay(1000)
                            .queue(function(n) {
                                $(this).html(msg);
                                n();
                            }).fadeIn("slow").queue(function(n) {
                                             $.material.init();
                                            n();

                                            $('#news').summernote({
                                    height: 300,                 // set editor height
                                    minHeight: null,             // set minimum height of editor
                                    maxHeight: null,             // set maximum height of editor
                                    placeholder:'Page information...',
                                    toolbar: [
                                    ['font', ['bold', 'italic', 'underline']],
                                    ['color', ['color']],
                                    ['para', ['ul', 'ol', 'paragraph']],
                                    ['table', ['table']],
                                    ['insert', ['link', 'hr']],
                                    ['help', ['help']]
                                    ]
                                    });
                            });
                        }
                  });
});

$( "body" ).on( "click", "#NewNewsFormBut", function(e) {
  var formid = '#NewNewsForm';
  var FRMdata = $(formid).serialize(); // get form data

     $.ajax({
             type: "POST",
             url: "app_ajax/save_news.php",
             data: FRMdata, // serializes the form's elements.
             success: function(msg){
                 $message=$.trim(msg);
                 Messenger().post({
                         message: 'News Saved.',
                         showCloseButton: false
                 });
                 setTimeout(function(){
                   location.reload();
                 },600);

              },error: function (xhr, status, errorThrown) {
             setTimeout(function(){alert('Error');},300);
           }
     });
});

    $( "body" ).on( "click", ".edit_news", function(e) {
       $("#BaseModalLContent").html($Loading);
       $news=$(this).attr("data-id");
       $('#BaseModalL').modal('show');

                        $.ajax({
                            type: "POST",
                            url: "app_ajax/edit.php?id="+$news,
                            data: "id:1",
                            success: function(msg){

                               //alert(msg);
                               $("#BaseModalLContent").delay(1000)
                                .queue(function(n) {
                                    $(this).html(msg);
                                    n();
                                }).fadeIn("slow").queue(function(n) {
                                                 $.material.init();
                                                n();

                                                $('#news').summernote({
                                        height: 300,                 // set editor height
                                        minHeight: null,             // set minimum height of editor
                                        maxHeight: null,             // set maximum height of editor
                                        placeholder:'Page information...',
                                        toolbar: [
                                        ['font', ['bold', 'italic', 'underline']],
                                        ['color', ['color']],
                                        ['para', ['ul', 'ol', 'paragraph']],
                                        ['table', ['table']],
                                        ['insert', ['link', 'hr']],
                                        ['help', ['help']]
                                        ]
                                        });
                                });
                            }
                      });
    });

    $( "body" ).on( "click", "#EditNewsFormBut", function(e) {
      var formid = '#EditNewsForm';
      var FRMdata = $(formid).serialize(); // get form data

         $.ajax({
                 type: "POST",
                 url: "app_ajax/edit_news.php",
                 data: FRMdata, // serializes the form's elements.
                 success: function(msg){
                     $message=$.trim(msg);
                     Messenger().post({
                             message: 'News Saved.',
                             showCloseButton: false
                     });
                     setTimeout(function(){
                       location.reload();
                     },600);

                  },error: function (xhr, status, errorThrown) {
                 setTimeout(function(){alert('Error');},300);
               }
         });
    });

    $( "body" ).on( "click", "#Unpublish_News", function(e) {
        $news=$(this).attr("data-id");
         $.ajax({
                 type: "POST",
                 url: "app_ajax/unpublish.php",
                 data: {id:$news}, // serializes the form's elements.
                 success: function(msg){
                     $message=$.trim(msg);
                     Messenger().post({
                             message: 'Unpublish',
                             showCloseButton: false
                     });
                     setTimeout(function(){
                       location.reload();
                     },600);
                  },error: function (xhr, status, errorThrown) {
                 setTimeout(function(){alert('Error');},300);
               }
         });
    });

    $( "body" ).on( "click", "#Publish_News", function(e) {

        $news=$(this).attr("data-id");

         $.ajax({
                 type: "POST",
                 url: "app_ajax/publish.php",
                 data: {id:$news}, // serializes the form's elements.
                 success: function(msg){
                     $message=$.trim(msg);
                     Messenger().post({
                             message: 'Publish',
                             showCloseButton: false
                     });
                     setTimeout(function(){
                       location.reload();
                     },600);
                  },error: function (xhr, status, errorThrown) {
                 setTimeout(function(){alert('Error');},300);
               }
         });
    });
</script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
