<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $id = createId('news');
  $name=$_POST["name"];
  $date=strtotime($_POST['date']);
  $news=$_POST["news"];
  $photo_id=$_POST['photo_id'];

  $db = new database;
  $db->Query("INSERT INTO hx_news (id ,name ,news ,news_date, img_id, status) VALUES (?,?,?,?,?,?)");
  $db->bind(1,$id);
  $db->bind(2,$name);
  $db->bind(3,$news);
  $db->bind(4,$date);
  $db->bind(5,$photo_id);
  $db->bind(6,'0');
  $db->execute();

  echo 'ok';
?>
