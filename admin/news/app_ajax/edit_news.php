<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $id = $_POST["id"];
  $name=$_POST["name"];
  $date=strtotime($_POST['date']);
  $news=$_POST["news"];
  $photo_id=$_POST['photo_id'];




  $db = new database;
  $db->Query("UPDATE hx_news SET name= ?, news= ?, news_date=?,img_id=?  WHERE id = ?");
  $db->bind(1,$name);
  $db->bind(2,$news);
  $db->bind(3,$date);
  $db->bind(4,$photo_id);
  $db->bind(5,$id);
  $db->execute();

  echo 'ok';
?>
