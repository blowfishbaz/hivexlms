<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Pages';
?>



       <form id="NewNewsForm" name="NewNewsForm" method="post" enctype="application/x-www-form-urlencoded">

           <div class="form-group">
               <label class=“”>News Title</label>
               <input type="text" name="name" class="form-control" id="name">
               <input type="hidden" name="photo_id"  id="photo_id"/>
           </div>
           <div class="form-group">
               <label class=“”>News Date</label>
               <input type="date" name="date" class="form-control" id="date" value="<? echo date('Y-m-d');?>">
           </div>
           <div class="form-group">
                   <label class=“”>News Information</label>
                   <textarea name="news" class="form-control" rows="5" id="news"></textarea>
               </div>


       </form>
       <form id="fileupload" action="<? echo $fullurl; ?>/assets/app_form/uploads/upload_path.php" method="POST" enctype="multipart/form-data">
       <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
       <div class="row fileupload-buttonbar">
        <div class="col-lg-12">
             <!-- The fileinput-button span is used to style the file input field as button -->
             <span class="btn btn-success btn-raised fileinput-button btn-sm">
                 <i class="glyphicon glyphicon-plus"></i>
                 <span>Add files...</span>
                 <input type="file" name="files">
             </span>

        </div>
        <!-- The global progress state -->
        <div class="col-lg-12 fileupload-progress fade">
             <!-- The global progress bar -->
             <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                 <div class="progress-bar progress-bar-success" style="width:0%;"></div>
             </div>
             <!-- The extended global progress state -->
             <div class="progress-extended">&nbsp;</div>
        </div>
       </div>
       <!-- The table listing the files available for upload/download -->
       <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
       </form>



       <button type="button" class="btn btn-primary btn-raised pull-right" id="NewNewsFormBut">Save<div class="ripple-container"></div></button>
<div class="clearfix"></div>
<script>
//////////okExts();

$(function () {
$('#fileupload').fileupload({
dataType: 'json',
acceptFileTypes: /(\.|\/)(<? echo okImageExts(); ?>)$/i,
maxFileSize: 10000000,
done: function (e, data) {
 att_name =data._response.result['files'][0].name;
                          if(data.textStatus=='success'){

      att_name_url="<? echo $fullurl;?>uploads/site/"+att_name+"";
      $path = 'uploads/site/';
      $name = att_name;
      $type = 'news';

          $.ajax({
          type: "POST",
          url: "../../../assets/app_ajax/uploads/db_upload.php",
          data: {path:$path,name:$name,type:$type},
          success: function(msg){

    $img_id=msg.trim();

    //$('#cv_upload').val($name);
    $('#photo_id').val($img_id);
                                        }
                    });
          }
}


});


$('#fileupload').fileupload()
.bind('fileuploadstart', function(){
// disable submit
})
.bind('fileuploadprogressall', function (e, data) {
if (data.loaded == data.total){
 // all files have finished uploading, re-enable submit

}
})

});
</script>
