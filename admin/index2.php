<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
 //Page Title
 $page_title = '';

 $db->query("select title,id from ws_roles where status = 1 order by title asc");
 $roles = $db->resultset();

 $new_member_id = createid('member');

 $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);


  $db = new database;
  $db->query("select * from accounts where id = ? ");
  $db->bind(1,$uid);
  $user = $db->single();

  if($user['account_type'] == 'user'){
    $db->query("select * from ws_accounts where pid = ? and id = ?");
    $db->bind(1,$uid);
    $db->bind(2,$uid);
    $user_ws = $db->single();
  }

  if(!empty($uid)){
    $new_member_id = $uid;
  }



 ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>HiveX - Business Management System</title>

<!-- Bootstrap Stylesheet -->
<link href=" <? echo $fullurl ?>assets/css/bootstrap.css" rel="stylesheet">
<link href=" <? echo $fullurl ?>assets/css/bootstrap-material-design.css" rel="stylesheet">
<link href=" <? echo $fullurl ?>assets/css/ripples.css" rel="stylesheet">


<!-- Fonts -->
<link href=" <? echo $fullurl ?>assets/css/fonts.css" rel="stylesheet">


<!-- Application -->
<link href=" <? echo $fullurl ?>assets/css/base.css" rel="stylesheet">
<link href=" <? echo $fullurl ?>assets/css/select.css" rel="stylesheet">

<!-- Messenger -->
<link href=" <? echo $fullurl ?>assets/css/messenger/messenger-theme-air.css" rel="stylesheet">
<link href=" <? echo $fullurl ?>assets/css/messenger/messenger.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="<? echo $fullurl ?>assets/js/html5shiv.js"></script>
        <script src="<? echo $fullurl ?>assets/js/respond.min.js"></script>
    <![endif]-->

    <? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>

<style>

</style>
<?php
if(isset($_GET['re'])){
echo '<script type="text/javascript">window.top.location.href = "http://'.$_SERVER['HTTP_HOST'].'/index.php"; </script>';
}
?>
  <style>
  body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;

}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
    color: black;
}
.form-group {
	text-align: left;
}
.form-group label {
	color:#363636;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

body {
	color: black;
  height: 100%;
}

html{
  height: 100%;
}


.logon {
	background-color: rgba(226, 226, 226, 0.95);
	border-radius: 20px;
	border: #337ab7 2px solid;
	max-width: 400px;
	text-align: center;
}

.index_cards{
  min-height: 400px;
    /* background-color: white; */
    /* border-radius: 10px; */
    border: 1px solid #337ab7;
    background-color: rgba(255,255,255,0.9);
    -webkit-box-shadow: 3px 3px 11px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 3px 3px 11px 0px rgba(0,0,0,0.75);
box-shadow: 3px 3px 11px 0px rgba(0,0,0,0.75);
}

.index_cards h3{
  position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%,-50%);
margin: 0px;
/* width: 100%; */
color: #337ab7;
}

.index_cards:hover{
  cursor: pointer;
}

.index_cards_wrapper{
  position: absolute;
width: 100%;
top: 50%;
left: 50%;
transform: translate(-50%, -60%);
max-width: 1450px;
/* display: none; */
}

.index_back, .index_login_wrapper, .index_register_wrapper{
  display: none;
  /* , .index_register_wrapper */
}

.index_register_wrapper_rota.index_register_wrapper {
    width: 60%;
    background-color: #eee;
    border: 1px solid #337ab7;
    text-align: center;
    border-radius: 10px;
    padding-bottom: 25px;
}

.cert_upload_holder{
  margin-bottom:10px;
}

@media (max-width: 992px){

  .index_cards{
    min-height: 200px;
        margin-bottom: 20px;
  }

  .index_cards_wrapper{

    position: relative;
    top: 0;
    left: 0;
    transform: none;

  }

}


  </style>

  </head>

  <body>
  <div class="wrapper">

    <? if (isset($_GET['error'])) { ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
     Username or Password Incorrect.
   </div>
   <? } ?>
   <? if (isset($_GET['account'])) { ?>
   <div class="alert alert-warning alert-dismissible" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
     Account has been locked.
   </div>
   <? } ?>



    <div class="index_back">
      <div class="col-sm-6 col-md-3 col-lg-3">
        <button type="button" class="btn btn-sm btn-primary" id="index_back_button">Back</button>
      </div>

      <div class="clearfix">

      </div>

    </div>


    <div class="index_cards_wrapper">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <? if (isset($_GET['error'])) { ?>
        <div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         Username or Password Incorrect.
       </div>
       <? } ?>
       <? if (isset($_GET['account'])) { ?>
       <div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         Account has been locked.
       </div>
       <? } ?>
        <? if (isset($_GET['account_waiting'])) { ?>
        <div class="alert alert-info alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Your account is pending verification. Once your account has been verified you will receive an email.
        </div>
        <? } ?>
      </div>

      <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="index_cards" data-id="lxp">
          <h3>LXP</h3>
        </div>
      </div>
      <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="index_cards" data-id="rota">
          <h3>Rota</h3>
        </div>
      </div>
      <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="index_cards" data-id="hr">
          <h3>HR</h3>
        </div>
      </div>
      <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="index_cards" data-id="goverance">
          <h3>Goverance</h3>
        </div>
      </div>
    </div>

    <!-- LXP LOGIN -->
    <div class="index_login_wrapper_lxp index_login_wrapper">
      <div class="container logon">
          <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="max-width:100%"><p>LXP - Sign In</p>
          <h3 style="color:#337ab7;">Coming Soon</h3>
      </div>
    </div>

    <!-- ROTA LOGIN -->
    <div class="index_login_wrapper_rota index_login_wrapper">
      <div class="container logon">
        <form class="form-signin" action="<? echo $fullurl ?>assets/app_php/auth.php?action=logon" enctype="application/x-www-form-urlencoded" method="post">
          <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="max-width:100%"><p>Rota - Sign In</p>
          <div class="form-group label-floating is-empty"> <label for="login" class="control-label">Email</label> <input type="email" name="login" class="form-control" required> <span class="material-input"></span> </div>
          <div class="form-group label-floating is-empty"><label for="password" class="control-label">Password</label><input type="password" class="form-control" name="password" required><span class="material-input"></span></div>
          <div class="checkbox"> <label> <input type="checkbox" value="1" name="remember"> Remember me </label> </div>
          <input type="hidden" name="key" value="<? echo getCurrentKey(); ?>">
          <input type="hidden" name="username">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
          <button class="btn btn-lg btn-info btn-block" type="button" id="rota_register">Register</button>
          <br><small>Version <?php echo version; ?></small>
        </form>
      </div>
    </div>

    <!-- HR LOGIN -->
    <div class="index_login_wrapper_hr index_login_wrapper">
      <div class="container logon">
        <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="max-width:100%"><p>HR - Sign In</p>
        <h3 style="color:#337ab7;">Coming Soon</h3>
      </div>
    </div>

    <!-- GOVERANCE LOGIN -->
    <div class="index_login_wrapper_goverance index_login_wrapper">
      <div class="container logon">
        <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="max-width:100%"><p>Goverance - Sign In</p>
        <h3 style="color:#337ab7;">Coming Soon</h3>
      </div>
    </div>


    <!-- ROTA REGISTER PAGE -->
    <div class="index_register_wrapper_rota index_register_wrapper container">
      <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="max-width:100%">
      <h3>Register - Rota</h3>

      <div class="col-sm-12 col-md-12 col-lg-12">
        <form id="register_rota_form">
          <input type="hidden" name="rota_id" value="<?echo $new_member_id;?>" />
          <input type="hidden" name="key_new" id="key_new" value="<? echo getCurrentKey(); ?>">

          <div class="rota_register_page_one" <?if(!empty($user_ws['status'])){echo 'style="display:none;"';}?>>



          <!-- First Name -->
          <div class="form-group label-floating is-empty">
                      <label for="first_name" class="control-label">First Name*</label>
                      <p class="input_error_message">Please Enter your First Name</p>
                      <input id="rota_first_name" type="text" name="first_name" class="form-control" value="" required>
                    <span class="material-input"></span>
          </div>

          <!-- Surname -->
          <div class="form-group label-floating is-empty">
                      <label for="surname" class="control-label">Surname*</label>
                      <p class="input_error_message">Please Enter your Surname</p>
                      <input id="rota_surname" type="text" name="surname" class="form-control" value="" required>
                    <span class="material-input"></span>
          </div>

          <!-- Email -->
          <div class="form-group label-floating is-empty">
                      <label for="email" class="control-label">Email*</label>
                      <p class="input_error_message">Please Enter your Email</p>
                      <input id="rota_email" type="text" name="email" class="form-control" value="" required>
                    <span class="material-input"></span>
          </div>

          <!-- Password -->
          <div class="form-group label-floating is-empty">
                      <label for="password" class="control-label">Password*</label>
                      <p class="input_error_message">must be between 6 and 16 characters, contain 1 uppercase and lowercase letter and contain 1 number <br />may contain special characters like !@#$%^&amp;*()_+</p>
                      <input id="rota_password" type="password" name="password" class="form-control" value="" required>
                    <span class="material-input"></span>
          </div>

          <!-- Telephone -->
          <div class="form-group label-floating is-empty">
                      <label for="telephone" class="control-label">Telephone</label>
                      <p class="input_error_message">1</p>
                      <input id="rota_telephone" type="text" name="telephone" class="form-control" value="">
                    <span class="material-input"></span>
          </div>

          <!-- Mobile -->
          <div class="form-group label-floating is-empty">
                      <label for="mobile_number" class="control-label">Mobile Number</label>
                      <p class="input_error_message">1</p>
                      <input id="rota_mobile_number" type="text" name="mobile_number" class="form-control" value="">
                    <span class="material-input"></span>
          </div>

          <!-- Job Role -->
          <div class="form-group">
            <label class="control-label">Job Role*</label>
            <select name="job_role[]" class="form-control selectpicker" data-live-search="true" id="job_role" required multiple>
              <?foreach ($roles as $r) {?>
                <option value="<?echo $r['id'];?>"><?echo $r['title'];?></option>
              <?}?>
          </select>
        </div>

        <!-- Locations -->
        <div class="form-group">
          <label class="control-label">Locations*</label>
          <select name="location[]" class="form-control selectpicker" data-live-search="true" id="location" required multiple>
            <option value="Wirral">Wirral</option>
            <option value="Sefton">Sefton</option>
            <option value="Liverpool">Liverpool</option>
        </select>
      </div>

      <button type="button" class="btn btn-raised btn-success" id="save_continue_rota">Save to Continue</button>

      </div>
      <div class="rota_register_page_two" <?if($user_ws['status'] != 2){echo 'style="display:none;"';}?>>


        <p>From the roles you have selected you must upload the relevent documents.<br />Click the upload button for each document below and upload your relevent documentaion</p>


        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder bls_certificate_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="bls_certificate">BLS Certificate - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder enhanced_dbs_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="enhanced_dbs">Enhanced DBS - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder university_qualifications_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="university_qualifications">University Qualifications - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder safeguarding_adults_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="safeguarding_adults">Safeguarding Adults - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder safeguarding_children_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="safeguarding_children">Safeguarding Children - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder cv_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="cv">CV - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder hep_b_certificate_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="hep_b_certificate">Hep B Certificate - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder photo_id_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="photo_id">Photo ID - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder university_qualification_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="university_qualification">University Qualification - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder phlebotomy_training_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="phlebotomy_training">Phlebotomy Training - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder hep_b_cert_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="hep_b_cert">Hep B Cert - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder reference_from_university_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="reference_from_university">Reference from University - Upload</button>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder gp_cct_mrcgp_jcptgp_div">
          <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="gp_cct_mrcgp_jcptgp">GP (CCT/MRCGP/JCPTGP) - Upload</button>
        </div>

        <!-- <button type="button" class="btn btn-sm btn-raised btn-info">BLS Certificate - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Enhanced DBS - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">University Qualifications - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Safeguarding Adults - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Safeguarding Children - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">CV - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Hep B Certificate - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Photo ID - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">University Qualification - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Phlebotomy Training - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Hep B Cert - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">Reference from University - Upload</button>
        <button type="button" class="btn btn-sm btn-raised btn-info">GP Qualifications (CCT/MRCGP/JCPTGP) - Upload</button> -->

          <div class="clearfix"></div>
          <button type="button" class="btn btn-raised btn-success" id="save_complete_rota">Save &amp; Complete</button>
      </div>

      <div class="rota_register_page_three" <?if($user_ws['status'] != 3){echo 'style="display:none;"';}?> >

        <h4>You're account has been created, it will need to be autherised by our staff before you are able to access the Rota System.<BR /><br />You will receive an email when your account is activated.</h4>

      </div>

      <div class="clearfix">

      </div>

      <!-- <button type="button" class="btn btn-sm btn-raised btn-info">Login</button> -->

      <div style=""></div>

        </form>
      </div>
    </div>



    <div class="preFooter">
    </div>
  </div>
    <div class="footer">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="col-sm-6 col-md-6 col-lg-6">
          <div style="float: left; margin: 0 auto;">
            <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="margin: 0 auto; vertical-align: middle; width: 120px;"/>
          </div>
          <div style="float: right; margin: 0 auto;">
            <button class="btn btn-primary support">Support</button>
          </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">
          <div style="float: left; margin: 0 auto;">
            <button class="btn btn-primary training">Training</button>
          </div>
          <div style="float: right; margin: 0 auto;">
            <!-- <img src="<? echo $fullurl ?>assets/images/skylogo.png" style="margin: 0 auto; vertical-align: middle; width: auto; height: 55px;"/> -->
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
  <!-- jQuery -->

    <?
    //JS Include
    include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


    ?>

<script src="<? echo $fullurl; ?>assets/js/select.js"></script>


       <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<? echo $fullurl ?>assets/js/jquery.backstretch.min.js"></script>
     <script>
     $fullurl = '<? echo $fullurl ?>';


     function limit_rota_roles($fullurl){

       $('.cert_upload_holder').hide();

       $.ajax({
           url: $fullurl+'assets/uploaders/rota_limit_uploader.php?id=<?echo $new_member_id;?>',
           type: 'GET',
           dataType: "json",
           success: function(msg) {
             console.dir(msg);
             $.map(msg, function(item) {
               console.log('.'+item+'_div');
               $('.'+item+'_div').show();
             });
           }
         });

       console.log($fullurl);

     }

     jQuery(document).ready(function($) {
       <?if($user_ws['status'] == 2){?>
         limit_rota_roles($fullurl);
       <?}?>

     });

     function password($input,$min,$max) {
       if ($input.length < $min) {return 1;}
       else if ($input.length > $max) {return 1;}
       else if ($input.search(/\d/) == -1) {  return 1;}
       else if ($input.search(/[a-z]/) == -1) {return 1;}
       else if ($input.search(/[A-Z]/) == -1) {return 1;}
       else if ($input.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {return 1;}
       else{return 0;}
     }

	    var images = [
    "<? echo $fullurl ?>assets/logonImages/bg1.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg2.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg3.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg4.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg5.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg6.jpg"

    ];

// The index variable will keep track of which image is currently showing
var index = 0,oldIndex;
 index = Math.floor((Math.random()*images.length));

        $.backstretch(images[index]);

    // Set an interval that increments the index and sets the new image
    // Note: The fadeIn speed set above will be inherited
    //

    setInterval(function() {
       oldIndex = index;
        while (oldIndex == index) {
            index = Math.floor((Math.random()*images.length));
        }
        $.backstretch(images[index]);
    }, 8000);

    // A little script for preloading all of the images
    // It"s not necessary, but generally a good idea
    $(images).each(function() {
        $("<img/>")[0].src = this;
    });


    $( "body" ).on( "click", ".support", function() {
            $('.warning').hide();
            $('.sentMessage').hide();
            document.getElementById("supportForm").reset();
            $('#supportModal').modal('show');
        //$('.panel-default').slideToggle();
    });

    $( "body" ).on( "click", ".training", function() {
            $('#training').modal('show');
        //$('.panel-default').slideToggle();
    });

    $( "body" ).on( "click", "#closeSupportForm", function() {
            $('#supportModal').modal('hide');
        //$('.panel-default').slideToggle();
    });

    $( "body" ).on( "click", "#tClose", function() {
            $('#training').modal('hide');
        //$('.panel-default').slideToggle();
    });

		$( document ).ready(function() {
		    $().alert('close')
		});



    $( "body" ).on( "click", "#index_back_button", function() {
      $('.index_back').fadeOut(500);
      $('.index_login_wrapper').fadeOut(500);
      $('.index_register_wrapper_rota').fadeOut(500);

      $('.index_cards_wrapper').fadeIn(500);
    });

    $( "body" ).on( "click", ".index_cards", function() {
            var type = $(this).data('id');

            $('.index_register_wrapper_rota').fadeOut(500);
            $('.index_cards_wrapper').fadeOut(500);
            $('.index_login_wrapper').fadeOut(500);

            $('.index_back').fadeIn(500);

            $('.index_login_wrapper_'+type+'').fadeIn(500);
    });




$( "body" ).on( "click", "#rota_register", function() {
  $('.index_login_wrapper').fadeOut(200);
  $('.index_register_wrapper_rota').fadeIn(400);
});


$( "body" ).on( "click", "#save_continue_rota", function() {
  var formid = '#register_rota_form';
  var HasError = 0;

  $(formid).find('input').each(function(){
    $(this).parent().removeClass('has-error');
    Messenger().hideAll();

    if(!$.trim(this.value).length) { // zero-length string AFTER a trim
             if(!$(this).prop('required')){
        } else {
          HasError = 1;
          $(this).parent().addClass('has-error');
        }
      }
  });

  if(password($('#rota_password').val(),6,16)){
    HasError=1;
    $('#rota_password').parent().addClass('has-error');
  }


if(HasError == 0){

  var FRMdata = $(formid).serialize(); // get form data
    $.ajax({
          type: "POST",
          url: $fullurl+"admin/signup.php",
          data: FRMdata,
          success: function(msg){
            $message=$.trim(msg);
            if($message=='email already exists'){
              alert('email already exists');
            }else if($message=='id already exists'){
              alert('id already exists');
            }else{
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');
                Messenger().post({
                    message: 'Account Created.',
                    type: 'error',
                    showCloseButton: false
                });


                $email=$('#rota_email').val();
                $password=$('#rota_password').val();
                $key=$('#key_new').val();


                $.ajax({
                      type: "POST",
                      url: $fullurl+"assets/app_php/auth.php?action=logon",
                      data: {login:$email,password:$password,key:$key},
                      success: function(msg){
                          $message=$.trim(msg);
                          if($message=='acount'){
                            alert('acount');
                          }else if($message=='locked out'){
                            alert('locked out');
                          }else if($message=='fail'){
                            alert('fail');
                          }else{
                            $('.rota_register_page_one').hide();
                            $('.rota_register_page_two').show();
                            limit_rota_roles($fullurl);




                          }
                     },error: function (xhr, status, errorThrown) {
                            setTimeout(function(){alert('Error');},300);
                          }
                });



              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });









}

});



$( "body" ).on( "click", "#save_complete_rota", function() {
  var formid = '#register_rota_form';
  // alert('here');


  var FRMdata = $(formid).serialize(); // get form data
    $.ajax({
          type: "POST",
          url: $fullurl+"admin/final_signup_rota.php",
          data: FRMdata,
          success: function(msg){
            $message=$.trim(msg);
            if($message=='ok'){
              setTimeout(function(){
                //window.location.replace($fullurl+'jobs.php');
                Messenger().post({
                    message: 'Account Created.',
                    type: 'error',
                    showCloseButton: false
                });
                window.location.replace($fullurl+'admin/index2.php?account_waiting');
                //We show a message at the topo fo the page for the user so they know what is happening.
                //window.location.reload();
              },1000);
            }

         },error: function (xhr, status, errorThrown) {
                Messenger().post({
                    message: 'An error has occurred please try again.',
                    type: 'error',
                    showCloseButton: false
                });
              }
    });
});


$( "body" ).on( "click", ".upload_rota_documents", function() {
  $("#BaseModalLContent").html($Loader);
  $('#BaseModalL').modal('show');

  var new_id = '<?echo $new_member_id;?>';
  var document_type = $(this).data('id');

    $.ajax({
       type: "POST",
       url: $fullurl+"admin/upload_documents.php",
       data: {'new_id':new_id,"document_type":document_type},
       success: function(msg){
         $("#BaseModalLContent").delay(1000)
          .queue(function(n) {
              $(this).html(msg);
              n();
          }).fadeIn("slow").queue(function(n) {
                       $.material.init();
                      n();
          });
         }
  });
});

    </script>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="supportModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
    <div class="modal-body " id="supportContent">
        <h3 style="padding-bottom: 2px;">Support</h3>
        <p>
          If you are having an issue with the system please fill in the form below;
        </p>
        <div class="warning alert alert-warning alert-dismissible" hidden>
          <p>
            Please insert all information into the form
          </p>
        </div>
        <div class="sentMessage alert alert-success alert-dismissible" hidden>
          <p>
            Message has been sent
          </p>
        </div>
        <?
            $form = new Form;
            $form->createForm('supportForm',null,'post','application/x-www-form-urlencoded','');
            $form->createDiv('col-md-12 col-sm-12');
                    $form->createFloatingTextField('text', 'Name*', 'name',null,'yes');
                    $form->createFloatingTextField('text', 'Phone*', 'phone',null,'yes');
                    $form->createFloatingTextField('email', 'Email*', 'email',null,'yes');
                    $form->createFloatingTextField('text','Issue*','issue',null,'yes');
            $form->endDiv();
            $form->createButton('closeSupportForm','Close','button','btn-danger btn-raised');
            $form->createButton('saveSupportForm','Submit','button','btn-primary btn-raised pull-right');
            $form->DisplayForm();
        ?>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="training">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
    <div class="modal-body " id="suppoerContent">
        <h3 style="padding-bottom: 2px;">Training</h3>
        <p>
          Email Blowfish Technology for training: <a href="mailto:hivex@blowfishtechnology.com?Subject=HiveX%20Training" target="_top">Blowfish Technology</a>
        </p>
        <button type="button"class="btn btn-danger btn-raised" id="tClose">Close</button>
        </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $(document).on('click', "#saveSupportForm", function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var issue = $('#issue').val();
    var phone = $('#phone').val();

    if(name == "" || email == "" || issue == "" || phone == ""){
      $('.warning').show();
    }else{
    $formInfo = $("#supportForm").serialize();
                 $.ajax({
                    type: "POST",
                    url: "../../../assets/app_ajax/sendSupport.php",
                    data: $formInfo,
                    success: function(msg){
                      $('.sentMessage').show();
                         setTimeout(function(){
                             $('#supportModal').modal('hide');
                             }, 2000);
                       }
                     });
                   }
                 });
}              );
</script>
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
