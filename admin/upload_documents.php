<?php
include($_SERVER['DOCUMENT_ROOT'].'/application.php');



function document_type($data){
    switch ($data) {
        case 'bls_certificate':
            return 'BLS Certificate';
            break;
        case 'enhanced_dbs':
            return 'Enhanced DBS';
            break;
        case 'university_qualifications':
            return 'University Qualifications';
            break;
        case 'safeguarding_adults':
            return 'Safeguarding Adults';
            break;
        case 'safeguarding_children':
            return 'Safeguarding Children';
            break;
        case 'cv':
            return 'CV';
            break;
        case 'hep_b_certificate':
            return 'Hep B Certificate';
            break;
        case 'photo_id':
            return 'Photo ID';
            break;
        case 'university_qualification':
            return 'University Qualification';
            break;
        case 'phlebotomy_training':
            return 'Phlebotomy Training';
            break;
        case 'hep_b_cert':
            return 'Hep B Cert';
            break;
        case 'reference_from_university':
            return 'Reference from University';
            break;
        case 'gp_cct_mrcgp_jcptgp':
            return 'GP (CCT/MRCGP/JCPTGP)';
            break;

        case 'bsc_training_prog_coll_para':
          return 'BSc in training programme approved by College of Paramedics';
          break;
        case 'health_and_care_profession_council':
          return 'Health and Care Professions Council (HCPC) registration';
          break;
        case 'registered_general_nurse_q':
          return 'Registered General Nurse qualification';
          break;
        case 'nmc_registration':
          return 'NMC registration';
          break;
        case 'bsc_advanced_clinical_practice':
          return 'BSC in advanced clinical practice (Level 7)or RCN Credentialling qualification (this should include clinical skills and diagnostics)';
          break;
        case 'ind_prescriber_qual':
          return 'Independent Prescriber qualification (V300)';
          break;
        case 'general_phara_council':
          return 'Mandatory registration with General Pharmaceutical Council (GPC)';
          break;
        case 'masters_in_pharmacy':
          return 'Masters degree in pharmacy (MPharm) (or equivalent)';
          break;
        case 'specialist_through_post_grad':
          return 'Specialist knowledge acquired through post graduate diploma level or equivalent training/experience';
          break;
        case 'independent_presriber':
          return 'Independent prescriber (preferred)';
          break;
        case 'cli_min_post_experience':
          return 'Minimum of 2 years post[1]qualification experience';
          break;
        case 'proof_of_experience_phara_tech':
          return 'Proof of experience in pharmacy technician';
          break;

        default:
            return "Error :'(";
            break;
    }
}

$path = 'uploads/users/'.$_POST['new_id'].'/'.$_POST['document_type'].'/';

?>


<div class="col-md-12 col-sm-12">
    <p><strong>Upload - <?echo document_type($_POST['document_type']);?></strong></p>
    <input type="hidden" name="featured_image_up" id="in_featured_image_up" value="empty" />
    <form id="featured_image_up" action="../../../assets/uploaders/account_documents.php?account_id=<?echo $_POST['new_id'];?>&document_type=<?echo $_POST['document_type'];?>" method="POST" enctype="multipart/form-data">
        <div class="row fileupload-buttonbar">
            <div class="col-lg-12">
                <span class="btn btn-success btn-raised fileinput-button btn-sm">
                    <span>Add Document</span>
                    <input type="file" name="files">
                </span>
                <span class="fileupload-process"></span>
            </div>
            <div class="col-lg-12 fileupload-progress fade">
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
    </form>
</div>

<div class="clearfix">

</div>
<?if(empty($_POST['loc'])){?>
  <b>
      Marking this section as complete will not allow you to uploading any more documents to this section. Please ensure all documents are uploaded.
  </b><br /><br />
  <button type="button" class="btn btn-sm btn-raised btn-warning" id="complete_doc_upload" data-id="<?echo $_POST['document_type'];?>">Complete Section</button>
<?}else{?>
  <button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>
<?}?>

<script>


$(function () {
$('#featured_image_up').fileupload({
dataType: 'json',
acceptFileTypes: /(\.|\/)(|pdf|PDF|doc|DOC|dot|DOT|docx|DOCX|dotx|DOTX|docm|DOCM|dotm|DOTM|xls|XLS|xlt|XLT|xla|XLA|xlsx|XLSX|xltx|XLTX|xlsm|XLSM|xltm|XLTM|xlam|XLAM|xlsb|XLSB|ppt|PPT|pot|POT|pps|PPS|ppa|PPA|pptx|PPTX|potx|POTX|ppsx|PPSX|ppam|PPAM|pptm|PPTM|potm|POTM|ppsm|PPSM|odt|ODT|ott|OTT|odp|ODP|otp|OTP|ods|ODS|ots|OTS|gif|GIF|png|PNG|jpe?g|JPE?G)$/i,
maxFileSize: 10000000,
autoUpload: true,
done: function (e, data) {
if(data.textStatus=='success'){

        att_name =data._response.result['files'][0].name;
        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////
        $.ajax({
              type: "POST",
              url: $fullurl+"assets/uploaders/upload_documents_save.php",
              data: {pid: '<?echo $_POST['new_id'];?>', path : '<? echo $path;?>', filename: att_name, doc_type:'<?echo $_POST['document_type'];?>'}, // serializes the form's elements.
              success: function(msg){
                <?if($_POST['type'] == 'account'){?>
                  setTimeout(function(){reloadtab(); $('.modal').modal('hide'); },300);
                <?}?>
              },error: function (xhr, status, errorThrown) {
              setTimeout(function(){alert('Error');},300);
              }
          });
          //////////////////////////////////////////////////////////
          //////////////////////////////////////////////////////////
          //////////////////////////////////////////////////////////


          // if ( $( ".template-upload" ).length>1 ) {
          //
          //     $( ".template-upload" ).first().remove();
          //
          // }

}
}
});

// $('#featured_image_up').fileupload()
// .bind('fileuploadstart', function(){
//
// })
});

</script>
