<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }

 $db->query("select * from lms_course where id = ?");
 $db->bind(1,$_GET["id"]);
 $db->execute();
 $course = $db->single();



 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->

<style>
.img-thumbnail{
  width:120px;
}
.img_td{
  text-align: center;
}

.media{
  border-bottom: 1px solid #e3e3e3;
  padding: 15px;
}

.media:nth-child(odd) {
  background: #eff7ff;
}

.media-left{
  width: 202px;
  min-width:  202px;
  max-width: 202px;
  height:  202px;
  min-height:  202px;
  max-height:  202px;
  border: 1px solid #e3e3e3;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  margin-bottom:25px;
}

.filetypes:hover{
  color:#b9dcfb;
}

.t_in{
  float: left;
  clear: both;
  width:100%;
}

</style>

</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Add Course Resources</span>
                            <!-- <button type="button" class="btn btn-primary btn-raised pull-right" id="add_new_course">Add</button> -->
                        </div>

                        <div class="clearfix"></div>

                        <div class="panel panel-default">
                          <div class="panel-body">

                                  <div class="col-md-12 col-sm-12">
                                  <form id="resource_details" method="POST" action="../../assets/app_ajax/admin/resource_details.php">
                                    <?

                                    $db->query("select * from lms_upload where cid = ? and type = ?");
                                    $db->bind(1, $_GET["id"]);
                                    $db->bind(2, "resource");
                                    $db->execute();
                                    $res= $db->resultset();

                                    $path = '../../../uploads/courses/';
                                    $fpath = $fullurl.'uploads/courses/';

                                    foreach ($res as $key => $value) {
                                        $f_t = mime_content_type("../../uploads/courses/".$value['link']);
                                        $f = getFileIcon(mime_content_type("../../uploads/courses/".$value['link']));
                                    ?>


                                    <div class="media col-md-12 col-sm-12">
                                      <div class="col-md-3" >
                                        <div class=" media-left">
                                      <?
                                      if (isset($f_t) && in_array($f_t, array("image/png", "image/jpeg", "image/gif"))) {
                                      ?>
                                      <img class="media-object" src="<? echo $path.$value['link']; ?>" width="140">
                                      <?
                                      } else {
                                      ?><a class="ficon" target="_blank" href="<? echo $path.$value['link']; ?>">
                                        <span class="<? echo $f; ?>" aria-hidden="true" style="float:left; font-size:92px;"></span>
                                        </a>
                                      <?
                                      }
                                      ?>
                                      </div>
                                      </div>
                                      <div class="col-md-9" >
                                          <div class=" media-body">
                                            <h4 class="media-heading"><strong>Add Resource Details</strong></h4>
                                            <input type="hidden" value="<? echo $value["id"]; ?>" name="resource_id[]">
                                            <div class="form-group label-floating is-empty">
                                                <label for="resource_name" class="control-label">Resource Name</label>
                                                <input type="text" class="form-control" name="resource_name[]" required="yes" value="" required >
                                                <span class="material-input"></span>
                                            </div>

                                            <div class="form-group">
                                                    <label class=“”>Resource Description</label>
                                                    <textarea name="resource_desc[]" class="form-control" rows="3" id="page_info"></textarea>
                                            </div>


                                            <div class="form-group three_cks">
                                                <h4 class="media-heading" style="margin-bottom:15px;">
                                                      <strong >Resource Availability</strong>
                                                </h4>


                                                  <? if($course['online'] == '0'){ ?>
                                                    <label class="checkbox-inline r_on_l" style="display:none;">
                                                    <input class="t_in" type="hidden" name="t_online[]" value="false" />
                                                    <input class="r_avail r_on" type="checkbox" value="0" name="online[]"> Online
                                                    </label>
                                                  <? }else { ?>
                                                    <label class="checkbox-inline r_on_l">
                                                    <input class="t_in" type="hidden" name="t_online[]" value="true" />
                                                    <input class="r_avail r_on" type="checkbox" value="0" name="online[]" checked> Online
                                                    </label>
                                                  <? } ?>

                                                  <? if($course['vc'] == '0'){ ?>
                                                    <label class="checkbox-inline r_vc_l" style="display:none;">
                                                    <input class="t_in" type="hidden" name="t_vc[]" value="false" />
                                                    <input class="r_avail r_vc" type="checkbox" value="t" name="vc[]"> Virtual Classroom
                                                    </label>
                                                  <? }else { ?>
                                                    <label class="checkbox-inline r_vc_l">
                                                    <input class="t_in" type="hidden" name="t_vc[]" value="true" />
                                                    <input class="r_avail r_vc" type="checkbox" value="1" name="vc[]" checked> Virtual Classroom
                                                    </label>
                                                  <? } ?>

                                                  <? if($course['f2f'] == '0'){ ?>
                                                    <label class="checkbox-inline r_f2_l" style="display:none;">
                                                    <input class="t_in" type="hidden" name="t_f2f[]" value="false" />
                                                    <input class="r_avail r_f2" type="checkbox" value="y" name="f2f[]"> Face to Face
                                                    </label>
                                                    <? }else { ?>
                                                    <label class="checkbox-inline r_f2_l">
                                                    <input class="t_in" type="hidden" name="t_f2f[]" value="true" />
                                                    <input class="r_avail r_f2" type="checkbox" value="1" name="f2f[]" checked> Face to Face
                                                    </label>
                                                  <? } ?>


                                            </div>



                                          </div>
                                      </div>
                                    </div>
                                    <?
                                    }
                                    ?>
                                  </form>
                                  </div>

                                  <div class="col-md-12 col-sm-12">
                                        <button class="btn btn-primary btn-raised pull-right" id="fin_but">Finish</button>
                                  </div>



                            </div>
                        </div><!--- END OF PAGE CONTENT -->


                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>

    <script>

    $( "body" ).on( "change", ".r_avail", function(e) {
          if(this.checked)
                $(this).parent().children(".t_in").val('true');
           else
                $(this).parent().children(".t_in").val('false');
    });


$( "body" ).on( "click", "#fin_but", function(e) {

    var HasError = 0;
    var form = $('#resource_details').serialize();

    $('#resource_details').find('.three_cks').each(function(){
        $(this).removeClass('has-error');
        Messenger().hideAll();

        $_on_ = $(this).children('.r_on_l').children('input').val();
        $_vc_ = $(this).children('.r_vc_l').children('input').val();
        $_f2_ = $(this).children('.r_f2_l').children('input').val();

        if($_on_=="false" && $_vc_=="false" && $_f2_=="false"){
            HasError = 1;
            $(this).addClass('has-error');
        }else{
            $(this).removeClass('has-error');
        }

    });

    $('#resource_details').find('input').each(function(){
        $(this).parent().removeClass('has-error');
        Messenger().hideAll();
        if(!$.trim(this.value).length) { // zero-length string AFTER a trim
            if(!$(this).prop('required')){
            } else {
                HasError = 1;
                $(this).parent().addClass('has-error');
            }
        }
    });

    if (HasError == 1) {
        Messenger().post({
              message: 'Please make sure all required elements of the form are filled out.',
              type: 'error',
              showCloseButton: false
        });
    } else {

              $.ajax({
                  type: "POST",
                  url: $fullurl+"assets/app_ajax/admin/resource_details.php",
                  data: form, // serializes the form's elements.
                  success: function(msg){
                        var r =  $.parseJSON(msg);
                        if(r['status']=='ok'){
                              $message=$.trim(msg);
                              Messenger().post({
                                    message: 'Resources Saved.',
                                    showCloseButton: false
                              });
                              setTimeout(function(){
                              window.location.replace('course.php?id='+r['cid']);
                              },600);
                        }
                  },error: function (xhr, status, errorThrown) {
                  setTimeout(function(){alert('Error');},300);
                  }
              });

        }



});
    </script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
