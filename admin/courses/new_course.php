<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->

<style>

.c_option{
  background-color:#f1f1f1;
  margin-bottom: 32px;
}

.c_option_hidden{
  background-color: #ffb5b5;
  padding: 25px 25px 15px 25px;
  font-size: 18px;
  display: none;
  margin-bottom: 15px;
}

</style>

</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Create New Course Template</span>
                            <!-- <button type="button" class="btn btn-primary btn-raised pull-right" id="add_new_course">Add</button> -->
                        </div>

                        <div class="clearfix"></div>

                        <div class="panel panel-default">
                          <div class="panel-body">
                              <form id="NewCourseForm" name="NewCourseForm" method="post" enctype="application/x-www-form-urlencoded">
                                          <div class="row">
                                              <div class="col-md-12 col-sm-12">
                                                  <div class="form-group" style="margin-top:0px;">
                                                      <p style="margin-bottom:15px;border-bottom: 1px solid #d0c8c8;padding-bottom: 9px;">
                                                        <strong style="margin-right:10px; font-size:18px;">Course Availability: </strong>
                                                      <label class="checkbox-inline">
                                                          <input class="c_in" type="hidden" name="c_online" value="true" />
                                                          <input class="c_avail c_online_in" type="checkbox" value="true" name="online" checked> Online
                                                      </label>
                                                      <label class="checkbox-inline">
                                                          <input class="c_in" type="hidden" name="c_vc" value="true" />
                                                          <input class="c_avail c_vc_in" type="checkbox" value="true" name="vc" checked> Virtual Classroom
                                                      </label>
                                                      <label class="checkbox-inline">
                                                          <input class="c_in" type="hidden" name="c_f2f" value="true" />
                                                          <input class="c_avail c_f2f_in" type="checkbox" value="true" name="f2f" checked> Face to Face
                                                      </label>
                                                      </p>
                                                  </div>
                                              </div>
                                          </div>

                                            <div class="col-md-12 col-sm-12">
                                                  <div class="form-group label-floating is-empty">
                                                      <label for="title" class="control-label">Course Title*</label>
                                                      <input type="text" class="form-control" id="title" name="title" required="yes" value="">
                                                      <span class="material-input"></span>
                                                  </div>
                                            </div>

                                            <!-- <div class="col-md-6 col-sm-6">
                                               <div class="form-group label-floating">
                                                    <label for="course_type" class="control-label">Course Type*</label>
                                                    <select class="form-control chosen-select" id="course_type" name="course_type" required="yes">
                                                         <option value="online" selected>Online</option>
                                                         <option value="virtual classroom">Virtual Classroom</option>
                                                         <option value="face to face">Face to Face</option>
                                                    </select>
                                              </div>
                                            </div> -->

                                            <div class="col-md-12 col-sm-12">
                                              <div class="form-group">
                                                      <label class=“”>Course Description</label>
                                                      <textarea name="course_info" class="form-control" rows="5" id="page_info"></textarea>
                                                  </div>

                                            </div>

<div class="col-md-12 col-sm-12 c_option online_only_div">

<h3 style="margin-bottom:15px;border-bottom: 1px solid #d0c8c8;padding-bottom: 9px;">Online Only Details</h3>
  <div class="col-md-12 col-sm-12">
    <div class="form-group">
            <label class=“”>Online Description</label>
            <textarea name="online_course_info" class="form-control" rows="5" id="online_only"></textarea>
        </div>

  </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="pass_perc" class="control-label">Online Pass Percentage (%)</label>
                <input type="number" class="form-control" id="online_pass_perc" name="online_pass_perc" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_price" class="control-label">Online Course Price (£)</label>
                <input type="number" class="form-control" id="online_course_price" name="online_course_price" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_credit_price" class="control-label">Online Course Credit Price (£)</label>
                <input type="number" class="form-control" id="online_course_credit_price" name="online_course_credit_price" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>


        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_duration" class="control-label">Online Course Duration</label>
                <input type="number" class="form-control" id="online_course_duration" name="online_course_duration" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>


</div>

<div class="col-md-12 col-sm-12 c_option_hidden online_only_div_hidden">
  <p> Course not availabile for online. </p>
</div>

<div class="col-md-12 col-sm-12 c_option vc_only_div">

  <h3 style="margin-bottom:15px;border-bottom: 1px solid #d0c8c8;padding-bottom: 9px;">Virtual Classroom Only Details</h3>

  <div class="col-md-12 col-sm-12">
    <div class="form-group">
            <label class=“”>Virtual Classroom Description</label>
            <textarea name="vc_course_info" class="form-control" rows="5" id="vc_only"></textarea>
        </div>

  </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="pass_perc" class="control-label">Virtual Classroom Pass Percentage (%)</label>
                <input type="number" class="form-control" id="vc_pass_perc" name="vc_pass_perc" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_price" class="control-label">Virtual Classroom Course Price (£)</label>
                <input type="number" class="form-control" id="vc_course_price" name="vc_course_price" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_credit_price" class="control-label">Virtual Classroom Course Credit Price (£)</label>
                <input type="number" class="form-control" id="vc_course_credit_price" name="vc_course_credit_price" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>


        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_duration" class="control-label">Virtual Classroom Course Duration</label>
                <input type="number" class="form-control" id="vc_course_duration" name="vc_course_duration" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

</div>

<div class="col-md-12 col-sm-12 c_option_hidden vc_only_div_hidden">
  <p> Course not availabile for Virtual Classroom. </p>
</div>


<div class="col-md-12 col-sm-12 c_option f2f_only_div">

          <h3 style="margin-bottom:15px;border-bottom: 1px solid #d0c8c8;padding-bottom: 9px;">Face To Face Only Details</h3>

          <div class="col-md-12 col-sm-12">
            <div class="form-group">
                    <label class=“”>Face To Face Description</label>
                    <textarea name="f2f_course_info" class="form-control" rows="5" id="f2f_only"></textarea>
                </div>

          </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="pass_perc" class="control-label">Face To Face Pass Percentage (%)</label>
                <input type="number" class="form-control" id="f2f_pass_perc" name="f2f_pass_perc" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_price" class="control-label">Face To Face Course Price (£)</label>
                <input type="number" class="form-control" id="f2f_course_price" name="f2f_course_price" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_credit_price" class="control-label">Face To Face Course Credit Price (£)</label>
                <input type="number" class="form-control" id="f2f_course_credit_price" name="f2f_course_credit_price" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>


        <div class="col-md-12 col-sm-12">
            <div class="form-group label-floating is-empty">
                <label for="course_duration" class="control-label">Face To Face Course Duration</label>
                <input type="number" class="form-control" id="f2f_course_duration" name="f2f_course_duration" required="yes" value="">
                <span class="material-input"></span>
            </div>
        </div>

</div>

<div class="col-md-12 col-sm-12 c_option_hidden f2f_only_div_hidden">
  <p> Course not availabile for Face To Face. </p>
</div>

                                    </form>

                                    <div class="col-md-12 col-sm-12">
                                          <button class="btn btn-primary btn-raised pull-right" id="save_new_course">Next</button>
                                    </div>

                            </div>
                        </div><!--- END OF PAGE CONTENT -->


                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>



<script>
$('#page_info, #online_only, #vc_only, #f2f_only').summernote({
    height: 180,                 // set editor height
    minHeight: null,             // set minimum height of editor
    maxHeight: null,             // set maximum height of editor
    placeholder:'Description........',
    toolbar: [
        ['font', ['bold', 'italic', 'underline']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'hr']],
        ['fontsize', ['fontsize']],
        ['help', ['help']]
    ]
});


$( "body" ).on( "change", ".c_online_in", function(e) {
      if(this.checked){
            $(this).parent().children(".c_in").val('true');
            $('.online_only_div').show();
            $('.online_only_div_hidden').hide();
            $(".online_only_div :input").removeClass("removed_in");
            $('#save_new_course').prop('disabled', false);
       }else{
            $(this).parent().children(".c_in").val('false');
            $('.online_only_div').hide();
            $('.online_only_div_hidden').show();
            $(".online_only_div :input").addClass("removed_in");
            if($('.c_vc_in').is(":checked") == 'true' || $('.c_f2f_in').is(":checked")){}
            else{ $('#save_new_course').prop('disabled', true)}

            }


});


$( "body" ).on( "change", ".c_vc_in", function(e) {
      if(this.checked){
            $(this).parent().children(".c_vc").val('true');
            $('.vc_only_div').show();
            $('.vc_only_div_hidden').hide();
            $('#save_new_course').prop('disabled', false);
            $(".vc_only_div :input").removeClass("removed_in");
       }else{
            $(this).parent().children(".c_vc").val('false');
            $('.vc_only_div').hide();
            $('.vc_only_div_hidden').show();
            $(".vc_only_div :input").addClass("removed_in");
            if($('.c_online_in').is(":checked") || $('.c_f2f_in').is(":checked")){}
            else{ $('#save_new_course').prop('disabled', true)}

          }
});


$( "body" ).on( "change", ".c_f2f_in", function(e) {
      if(this.checked){
            $(this).parent().children(".c_f2f").val('true');
            $('.f2f_only_div').show();
            $('.f2f_only_div_hidden').hide();
            $('#save_new_course').prop('disabled', false);
            $(".f2f_only_div :input").removeClass("removed_in");
       }else{
            $(this).parent().children(".c_f2f").val('false');
            $('.f2f_only_div').hide();
            $('.f2f_only_div_hidden').show();
            $(".f2f_only_div :input").addClass("removed_in");
            if($('.c_online_in').is(":checked") || $('.c_vc_in').is(":checked")){}
            else{ $('#save_new_course').prop('disabled', true)}

        }
});



$( "body" ).on( "click", "#save_new_course", function(e) {

  var HasError = 0;
  var form = $('#NewCourseForm').serialize();

  $('#NewCourseForm').find('input').each(function(){
        if (!$(this).hasClass("removed_in")) {
                $(this).parent().removeClass('has-error');
                Messenger().hideAll();
                if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                    if(!$(this).prop('required')){
                    } else {
                        HasError = 1;
                        $(this).parent().addClass('has-error');
                    }
                }
          }
  });

  $('#NewCourseForm').find('select').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
          if(!$(this).prop('required')){
          } else {
              HasError = 1;
              $(this).parent().addClass('has-error');
          }
      }
  });


  if (HasError == 1) {
      Messenger().post({
            message: 'Please make sure all required elements of the form are filled out.',
            type: 'error',
            showCloseButton: false
      });
  } else {

          $.ajax({
                type: "POST",
                url: $fullurl+"assets/app_ajax/admin/new_course.php",
                data: form, // serializes the form's elements.
                success: function(msg){
                      var r =  $.parseJSON(msg);
                      if(r['status']=='ok'){
                            $message=$.trim(msg);
                            Messenger().post({
                                  message: 'Course Saved.',
                                  showCloseButton: false
                            });
                            setTimeout(function(){
                            window.location.replace('new_course_part_2.php?id='+r['cid']);
                            },600);
                      }
                },error: function (xhr, status, errorThrown) {
                setTimeout(function(){alert('Error');},300);
                }
            });
      }

});



</script>


</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
