<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->

<style>

.c_option{
  background-color:#f1f1f1;
  margin-bottom: 32px;
}

.c_option_hidden{
  background-color: #ffb5b5;
  padding: 25px 25px 15px 25px;
  font-size: 18px;
  display: none;
  margin-bottom: 15px;
}


.time_in{
  width: 100%;
  border: none;
  border-bottom: 1px solid #ccc;
  padding-bottom: 10px;
}

</style>

</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Add Course</span>
                            <br />
                            <br />
                            <!-- <button type="button" class="btn btn-primary btn-raised pull-right" id="add_new_course">Add</button> -->
                        </div>

                        <div class="clearfix"></div>

                        <div class="panel panel-default">
                          <div class="panel-body">
<!-- content -->
<!-- content -->


<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="" class="control-label" style="margin-top:0px;">Course Template</label>
        <select class="form-control" data-show-subtext="true" data-live-search="true" style="" id="course_template">
            <option class="" value="" selected=""></option>
            <?
            $db->query("select * from lms_course where status = ? order by title asc");
            $db->bind(1,'1');
            $rows = $db->ResultSet();
            foreach ($rows as $key => $value) { ?>
            <option value="<? echo $value['id']; ?>"><? echo $value['title']; ?></option>
            <? } ?>
        </select>
        <span class="material-input"></span>
    </div>
</div>


<div class="course_template_div">

</div>



<!-- content -->
<!-- content -->
                          </div>
                        </div><!--- END OF PAGE CONTENT -->


                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>



<script>
    $( "body" ).on( "change", "#course_type", function(e) {

        if($(this).val()=="vc"){
          $('.web_link_in').show();
        }else{
          $('.web_link_in').hide();
        }

    });

    $( "body" ).on( "change", "#course_template", function(e) {

        $(".course_template_div").html('');
        $_id = $(this).val();

          $.ajax({
                type: "POST",
                url: "../../assets/app_ajax/courses/fetch_course.php?id="+$_id,
                data: {},
                success: function(msg){
                  $(".course_template_div").delay(1000)
                   .queue(function(n) {
                       $(this).html(msg);
                       n();
                   }).fadeIn("slow").queue(function(n) {
                                $.material.init();
                               n();

                               $('#page_info').summernote({
                                   height: 180,                 // set editor height
                                   minHeight: null,             // set minimum height of editor
                                   maxHeight: null,             // set maximum height of editor
                                   placeholder:'Description........',
                                   toolbar: [
                                       ['font', ['bold', 'italic', 'underline']],
                                       ['color', ['color']],
                                       ['para', ['ul', 'ol', 'paragraph']],
                                       ['table', ['table']],
                                       ['insert', ['link', 'hr']],
                                       ['fontsize', ['fontsize']],
                                       ['help', ['help']]
                                   ]
                               });
                   });

                }
          })

    });

// $( "body" ).on( "change", "input[type='radio'][name='always_dates']", function(e) {
//
//         $f =  $("input[type='radio'][name='always_dates']:checked").val();
//
//         alert($f);
//
//         if($f=="always"){
//           $('.always_opt').show();
//           $('.dates_opt').hide();
//         }
//         else{
//           $('.dates_opt').show();
//           $('.always_opt').hide();
//         }
//
// });


$l = '<span class="date_line">';
$l += '<div class="col-lg-6">';
$l += '<div class="form-group is-empty">';
$l += '<label for="date_title" class="control-label">Date Title: </label>';
$l += '<input type="text" class="form-control" name="date_title[]" required="yes">';
$l += '<span class="material-input"></span>';
$l += '</div>';
$l += '</div>';
$l += '<div class="col-lg-3">';
$l += '<div class="form-group is-empty">';
$l += '<label for="date_" class="control-label">Date: </label>';
$l += '<input type="date" class="form-control" name="date_[]" value="<? echo date("Y-m-d", time()); ?>" required="yes">';
$l += '<span class="material-input"></span>';
$l += '</div>';
$l += '</div>';
$l += '<div class="col-lg-3">';
$l += '<div class="form-group is-empty">';
$l += '<label for="date_" class="control-label">Time: </label>';
$l += '<input type="time" class="time_in" name="appt[]" min="09:00" max="22:00" value="08:00" required>';
$l += '<span class="material-input"></span>';
$l += '</div>';
$l += '</div>';
$l += '<div class="row">';
$l += '<div class="col-lg-12">';
$l += '<div class="col-lg-2 pull-right align-right">';
$l += '<button type="button" class="btn btn-raised btn-danger btn-xs pull-right del_meter"> X </button></div>';
$l += '</div>';
$l += '</div>';
$l += '</span>';



$( "body" ).on( "click", ".add_line", function(e) {

      $($l).insertBefore( ".add_row" );

});


$( "body" ).on( "click", ".del_meter", function(e) {

      $(this).parent().parent().parent().parent().remove();

});

$( "body" ).on( "click", ".save_course", function(e) {


          $c_template = $("#course_template").val();
          var HasError = 0;

          var FRMdata = $("#new_course_form").serialize();
          $("#new_course_form").find('input').each(function(){
            $(this).parent().removeClass('has-error');
            Messenger().hideAll();
            if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                     if(!$(this).prop('required')){}
                     else {
                  HasError = 1;
                  $(this).parent().addClass('has-error');
                }
              }
          });

          if (HasError == 1) {
            Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
            });
          }
          else{

                 $.ajax({
                         type: "POST",
                         url: $fullurl+"assets/app_ajax/courses/save_course_insta.php?cid="+$c_template,
                         data: FRMdata, // serializes the form's elements.
                         success: function(msg){
                             $message=$.trim(msg);
                             Messenger().post({
                                     message: 'Course Added.',
                                     showCloseButton: false
                             });
                             setTimeout(function(){
                             window.location.replace('index.php');
                             },600);
                          },error: function (xhr, status, errorThrown) {
                         setTimeout(function(){alert('Error');},300);
                       }
                 });

        }


});







</script>


</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
