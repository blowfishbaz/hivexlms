<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->
</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Add Course Resources</span>
                            <!-- <button type="button" class="btn btn-primary btn-raised pull-right" id="add_new_course">Add</button> -->
                        </div>

                        <div class="clearfix"></div>

                        <div class="panel panel-default">
                          <div class="panel-body">

                                    <hr class="seperator">
                                    <div class="col-md-12 col-sm-12">
                                        <p><strong>Add Featured Image</strong></p>
                                        <input type="hidden" name="featured_image_up" id="in_featured_image_up" value="empty" />
                                        <form id="featured_image_up" action="../../assets/app_ajax/courses/upload_file.php" method="POST" enctype="multipart/form-data">
                                            <div class="row fileupload-buttonbar">
                                                <div class="col-lg-12">
                                                    <span class="btn btn-success btn-raised fileinput-button btn-sm">
                                                        <span>Add Featured Image</span>
                                                        <input type="file" name="files">
                                                    </span>
                                                    <span class="fileupload-process"></span>
                                                </div>
                                                <div class="col-lg-12 fileupload-progress fade">
                                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                    </div>
                                                    <div class="progress-extended">&nbsp;</div>
                                                </div>
                                            </div>
                                            <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
                                        </form>
                                  </div>

                                  <hr class="seperator">
                                  <div class="col-md-12 col-sm-12">
                                      <p><strong>Add Resources</strong></p>
                                      <input type="hidden" name="resource_up" id="in_featured_image_up" value="empty" />
                                      <form id="resource_up" action="../../assets/app_ajax/courses/upload_file.php" method="POST" enctype="multipart/form-data">
                                          <div class="row fileupload-buttonbar">
                                              <div class="col-lg-12">
                                                  <span class="btn btn-success btn-raised fileinput-button btn-sm">
                                                      <span>Add Resources</span>
                                                      <input type="file" name="files[]" multiple>
                                                  </span>
                                                  <span class="fileupload-process"></span>
                                              </div>
                                              <div class="col-lg-12 fileupload-progress fade">
                                                  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                      <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                  </div>
                                                  <div class="progress-extended">&nbsp;</div>
                                              </div>
                                          </div>
                                          <table role="presentation" class="table table-striped "><tbody class="files"></tbody></table>
                                      </form>
                                </div>


                                <div class="col-md-12 col-sm-12">
                                      <a class="btn btn-primary btn-raised pull-right" href="new_course_part_3.php?id=<? echo $_GET['id']; ?>">Next</a>
                                </div>

                            </div>
                        </div><!--- END OF PAGE CONTENT -->


                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>



<script>

$(document).ready(function(){

        $(function () {
        $('#featured_image_up').fileupload({
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(|gif|GIF|png|PNG|jpe?g|JPE?G)$/i,
        maxFileSize: 10000000,
        autoUpload: true,
        done: function (e, data) {
        if(data.textStatus=='success'){






                att_name =data._response.result['files'][0].name;
                ////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////
                $.ajax({
                      type: "POST",
                      url: $fullurl+"assets/app_ajax/admin/new_image_db.php",
                      data: {uptype: 'featured', cid : '<? echo $_GET['id'];?>', filename: att_name}, // serializes the form's elements.
                      success: function(msg){
                      },error: function (xhr, status, errorThrown) {
                      setTimeout(function(){alert('Error');},300);
                      }
                  });
                  ////////////////////////////////////////////////////////////
                  ////////////////////////////////////////////////////////////
                  ////////////////////////////////////////////////////////////


                  if ( $( ".template-upload" ).length>1 ) {

                      $( ".template-upload" ).first().remove();

                  }

        }
        }
        });

        // $('#featured_image_up').fileupload()
        // .bind('fileuploadstart', function(){
        // // disable submit
        // })
        });

    $(function () {
        $('#resource_up').fileupload({
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(<? echo okExts(); ?>)$/i,
            //acceptFileTypes: /(\.|\/)(msg|MSG|pdf|PDF|doc|DOC|dot|DOT|docx|DOCX|dotx|DOTX|docm|DOCM|dotm|DOTM|xls|XLS|xlt|XLT|xla|XLA|xlsx|XLSX|xltx|XLTX|xlsm|XLSM|xltm|XLTM|xlam|XLAM|xlsb|XLSB|ppt|PPT|pot|POT|pps|PPS|ppa|PPA|pptx|PPTX|potx|POTX|ppsx|PPSX|ppam|PPAM|pptm|PPTM|potm|POTM|ppsm|PPSM|odt|ODT|ott|OTT|odp|ODP|otp|OTP|ods|ODS|ots|OTS|gif|GIF|png|PNG|jpe?g|JPE?G|zip|ZIP)$/i,
             maxFileSize: 10000000,
             autoUpload: true,
             done: function (e, data) {
          if(data.textStatus=='success'){
                    att_name =data._response.result['files'][0].name;
                    ////////////////////////////////////////////////////////////
                    ////////////////////////////////////////////////////////////
                    ////////////////////////////////////////////////////////////
                    $.ajax({
                          type: "POST",
                          url: $fullurl+"assets/app_ajax/admin/new_image_db.php",
                          data: {uptype: 'resource', cid : '<? echo $_GET['id'];?>', filename: att_name}, // serializes the form's elements.
                          success: function(msg){
                          },error: function (xhr, status, errorThrown) {
                          setTimeout(function(){alert('Error');},300);
                          }
                      });
                      ////////////////////////////////////////////////////////////
                      ////////////////////////////////////////////////////////////
                      ////////////////////////////////////////////////////////////
          }
            }
        });

        // $('#resource_up').fileupload()
        // .bind('fileuploadstart', function(){
        //     // disable submit
        //   //  alert('its done');
        // })
    });
  });

        </script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
