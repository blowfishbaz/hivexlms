<?php
/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->

<style>

.c_option{
  background-color:#f1f1f1;
  margin-bottom: 32px;
}

.c_option_hidden{
  background-color: #ffb5b5;
  padding: 25px 25px 15px 25px;
  font-size: 18px;
  display: none;
  margin-bottom: 15px;
}


.time_in{
  width: 100%;
  border: none;
  border-bottom: 1px solid #ccc;
  padding-bottom: 10px;
}

</style>

</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Add Course</span>
                            <br />
                            <br />
                            <a href="view_course.php?id=<? echo $_GET['id']; ?>" class="btn btn-primary btn-raised pull-right" > < Back</a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="panel panel-default">
                          <div class="panel-body">
<!-- content -->
<!-- content -->

<?
$db->query("select * from lms_course_inst where id = ?");
$db->bind(1,$_GET["id"]);
$cor = $db->single();

$db->query("select * from lms_course where id = ?");
$db->bind(1,$cor["c_id"]);
$ct = $db->single();

?>

<form id="new_course_form_edit">
<input type="hidden" name="insta_id" id="insta_id" value="<? echo $_GET['id']; ?>">
<input type="hidden" name="temp_id" id="temp_id" value="<? echo $cor["c_id"]; ?>">
<div class="col-lg-12">
<h3><? echo ucwords($ct['title']); ?></h3>
</div>
<div class="col-lg-12">
    <div class="form-group is-empty">
        <label for="" class="control-label" style="margin-top:0px;">Course Type</label>
        <select class="form-control" name="course_type" style="" id="course_type">
            <option></option>

            <? if($ct['online']=='1'){ ?>
            <option class="" value="online" <? if($cor['c_type']=='online'){ ?> selected="selected" <? }?>>Online</option>
            <? }?>
            <? if($ct['vc']=='1'){ ?>
            <option class="" value="vc" <? if($cor['c_type']=='vc'){ ?> selected="selected" <? }?>>Virtual Classroom</option>
            <? }?>
            <? if($ct['f2f']=='1'){ ?>
            <option class="" value="f2f" <? if($cor['c_type']=='f2f'){ ?> selected="selected" <? }?>>Face To Face</option>
            <? }?>


        </select>
        <span class="material-input"></span>
    </div>
</div>

<div class="col-lg-12 web_link_in" <? if($cor['c_type']!='vc'){ ?>  style="display:none;" <? } ?>>
  <div class="form-group is-empty">
    <label for="web_link" class="control-label">Online Webinar Link: </label>
    <input type="text" class="form-control" name="web_link" value="<? echo $cor['c_link'];?>">
    <span class="material-input"></span>
  </div>
</div>

<div class="col-md-12 col-sm-12">
  <div class="form-group">
          <label class=“”>Course Description</label>
          <textarea name="course_info" class="form-control" rows="5" id="page_info"><? echo $cor['c_desc']; ?></textarea>
      </div>

</div>


<div class="clearfix"></div>

<div class="always_opt">
    <div class="col-lg-6" >
        <div class="form-group is-empty">
              <label for="always_start" class="control-label">Start Date: </label>
              <input type="date" class="form-control" id="always_start" name="always_start" value="<? echo date("Y-m-d", $cor['c_start']); ?>" required="yes">
              <span class="material-input"></span>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group is-empty">
              <label for="always_end" class="control-label">End Date: </label>
              <input type="date" class="form-control" id="always_end" name="always_end" value="<? echo date("Y-m-d", $cor['c_end']); ?>" required="yes">
              <span class="material-input"></span>
        </div>
    </div>
</div>



<div class="clearfix"></div>

<div class="dates_opt" style="margin-top:20px;">

  <?

  $db->query("select * from lms_course_inst_date where c_id = ? order by c_date ASC");
  $db->bind(1,$cor["id"]);
  $d = $db->ResultSet();

  foreach ($d as $key => $value) {
  ?>

            <? if($value['c_date']>time()){?>

              <input type="hidden" name="date_id[]" required="yes" value="<? echo $value['id'];?>">
              <span class="date_line">
                <div class="col-lg-6">
                  <div class="form-group is-empty">
                    <label for="date_title" class="control-label">Date Title: </label>
                    <input type="text" class="form-control" name="date_title[]" required="yes" value="<? echo $value['title'];?>">
                    <span class="material-input"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group is-empty">
                    <label for="date_" class="control-label">Date: </label>
                    <input type="date" class="form-control" name="date_[]" value="<? echo date("Y-m-d", $value['c_date']); ?>" required="yes">
                    <span class="material-input"></span>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group is-empty">
                    <label for="date_" class="control-label">Time: </label>
                    <input type="time" class="time_in" name="appt[]" min="09:00" max="22:00" value="<? echo date("H:i", $value['c_date']); ?>" required>
                    <span class="material-input"></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="col-lg-2 pull-right align-right">
                  <button type="button" class="btn btn-raised btn-danger btn-xs pull-right del_meter"> X </button></div>
                </div>
              </div>
            </span>



          <? } else { ?>

            <input type="hidden" name="date_idx[]" required="yes" value="<? echo $value['id'];?>">
            <span class="date_line">
              <div class="col-lg-6">
                <div class="form-group is-empty">
                  <label for="date_titlex" class="control-label">Date Title: </label>
                  <input type="text" class="form-control" name="date_titlex[]" required="yes" value="<? echo $value['title'];?>" readonly>
                  <span class="material-input"></span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group is-empty">
                  <label for="date_x" class="control-label">Date: </label>
                  <input type="date" class="form-control" name="date_x[]" value="<? echo date("Y-m-d", $value['c_date']); ?>" required readonly>
                  <span class="material-input"></span>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group is-empty">
                  <label for="date_x" class="control-label">Time: </label>
                  <input type="time" class="time_in" name="apptx[]" min="09:00" max="22:00" value="<? echo date("H:i", $value['c_date']); ?>" required readonly>
                  <span class="material-input"></span>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="col-lg-2 pull-right align-right">
                <button type="button" class="btn btn-raised btn-danger btn-xs pull-right disabled" disabled> X </button></div>
              </div>
            </div>
          </span>



          <? } ?>


    <?
    }
    ?>


      <div class="row add_row">
          <div class="col-lg-12" >
            <br />
            <br />
                <div class="col-lg-2 pull-right align-right" >
                      <button type="button" class="btn btn-raised btn-success btn-xs pull-right add_line"> Add Date </button>
                </div>
          </div>
      </div>
</div>



<div class="row">
    <div class="col-lg-12" >
      <br />
      <br />
          <div class="col-lg-2 pull-right align-right" >
                <button type="button" class="btn btn-raised btn-success btn-lg  pull-right edit_course"> Save </button>
          </div>
    </div>
</div>

</form>


<!-- content -->
<!-- content -->
                          </div>
                        </div><!--- END OF PAGE CONTENT -->


                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
    //JS Include
    include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>

    <script>
    $('#page_info').summernote({
        height: 180,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        placeholder:'Description........',
        toolbar: [
            ['font', ['bold', 'italic', 'underline']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'hr']],
            ['fontsize', ['fontsize']],
            ['help', ['help']]
        ]
    });


    $( "body" ).on( "change", "#course_type", function(e) {

        if($(this).val()=="vc"){
          $('.web_link_in').show();
        }else{
          $('.web_link_in').hide();
        }

    });




    $l = '<span class="date_line">';
    $l += '<div class="col-lg-6">';
    $l += '<div class="form-group is-empty">';
    $l += '<label for="date_title" class="control-label">Date Title: </label>';
    $l += '<input type="text" class="form-control" name="date_title[]" required="yes">';
    $l += '<span class="material-input"></span>';
    $l += '</div>';
    $l += '</div>';
    $l += '<div class="col-lg-3">';
    $l += '<div class="form-group is-empty">';
    $l += '<label for="date_" class="control-label">Date: </label>';
    $l += '<input type="date" class="form-control" name="date_[]" value="<? echo date("Y-m-d", time()); ?>" required="yes">';
    $l += '<span class="material-input"></span>';
    $l += '</div>';
    $l += '</div>';
    $l += '<div class="col-lg-3">';
    $l += '<div class="form-group is-empty">';
    $l += '<label for="date_" class="control-label">Time: </label>';
    $l += '<input type="time" class="time_in" name="appt[]" min="09:00" max="22:00" value="08:00" required>';
    $l += '<span class="material-input"></span>';
    $l += '</div>';
    $l += '</div>';
    $l += '<div class="row">';
    $l += '<div class="col-lg-12">';
    $l += '<div class="col-lg-2 pull-right align-right">';
    $l += '<button type="button" class="btn btn-raised btn-danger btn-xs pull-right del_meter"> X </button></div>';
    $l += '</div>';
    $l += '</div>';
    $l += '</span>';



    $( "body" ).on( "click", ".add_line", function(e) {

          $($l).insertBefore( ".add_row" );

    });


    $( "body" ).on( "click", ".del_meter", function(e) {

          $(this).parent().parent().parent().parent().remove();

    });



    $( "body" ).on( "click", ".edit_course", function(e) {


              $insta_id = $("#insta_id").val();

              var HasError = 0;

              var FRMdata = $("#new_course_form_edit").serialize();
              $("#new_course_form").find('input').each(function(){
                $(this).parent().removeClass('has-error');
                Messenger().hideAll();
                if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                         if(!$(this).prop('required')){}
                         else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
                    }
                  }
              });

              if (HasError == 1) {
                Messenger().post({
                    message: 'Please make sure all required elements of the form are filled out.',
                    type: 'error',
                    showCloseButton: false
                });
              }
              else{

                     $.ajax({
                             type: "POST",
                             url: $fullurl+"assets/app_ajax/courses/edit_course_insta.php?cid="+$insta_id,
                             data: FRMdata, // serializes the form's elements.
                             success: function(msg){
                                 $message=$.trim(msg);
                                 Messenger().post({
                                         message: 'Course Added.',
                                         showCloseButton: false
                                 });
                                 setTimeout(function(){
                                 window.location.replace('index.php');
                                 },600);
                              },error: function (xhr, status, errorThrown) {
                             setTimeout(function(){alert('Error');},300);
                           }
                     });

            }


    });



    </script>


</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
