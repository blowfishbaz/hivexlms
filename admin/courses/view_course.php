<?php
/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }
 ?>
<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css_new.php'); ?>

<!-- On Page CSS -->

<style>

.c_option{
  background-color:#f1f1f1;
  margin-bottom: 32px;
}

.c_option_hidden{
  background-color: #ffb5b5;
  padding: 25px 25px 15px 25px;
  font-size: 18px;
  display: none;
  margin-bottom: 15px;
}


.time_in{
  width: 100%;
  border: none;
  border-bottom: 1px solid #ccc;
  padding-bottom: 10px;
}

.c_info{
  padding: 1em;
  background-color: #f6f4f4;
  margin: 35px 0 45px 0;
}

.c_info > div{
  padding: 1em;
  font-size:19px;
}

.label.labelTeal{
  background-color: teal;
}

.label.labelRefund{
  background-color: #02ffd0;
      color: #000
}

</style>

</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu_new.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex_new.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Course Details</span>
                            <br />
                            <br />
                            <a href="course_edit.php?id=<? echo $_GET['id']; ?>" class="btn btn-primary btn-raised pull-right" >Edit</a>
                        </div>

                        <div class="clearfix"></div>


<!-- content -->
<!-- content -->

<?

$db->query("select * from lms_course_inst where id = ?");
$db->bind(1,$_GET["id"]);
$cor = $db->single();

$db->query("select * from lms_course where id = ?");
$db->bind(1,$cor["c_id"]);
$tem = $db->single();

$db->query("select * from lms_course where id = ?");
$db->bind(1,$cor["c_id"]);
$ct = $db->single();

?>
<div class="panel panel-default">
  <div class="panel-body">


<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Details</a></li>
  <li><a data-toggle="tab" href="#menu1">Course Overview</a></li>
  <li><a data-toggle="tab" href="#menu2">Course Type Description</a></li>
  <li><a data-toggle="tab" href="#menu3">Course Description</a></li>
  <li><a data-toggle="tab" href="#menu4">Dates</a></li>
  <li><a data-toggle="tab" href="#menu6">Learners</a></li>
  <li><a data-toggle="tab" href="#menu7">Course Credit Log</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <!--home-->
    <div class="col-lg-12">
      <?

      $db->query("select * from ws_booked_on where (course_id = ? or course_id = ?) and account_id = ?");
      $db->bind(1,$_GET["id"]);
      $db->bind(2,$_GET["id"]);
      $db->bind(3,decrypt($_SESSION['SESS_ACCOUNT_ID']));
      $on_course = $db->single();

      ?>
          <h3><? echo ucwords($ct['title']); ?>
            <?if(!empty($on_course)){?>
              <a href="<?echo $fullurl;?>admin/courses/course_view.php?course_id=<?echo $on_course['id'];?>" class="pull-right btn-sm btn-raised btn-success">Take Course</a>
            <?}?>
          </h3>





          <br />
          <? if($cor['c_type']=='online'){
            $c_type = "Online";

            $c_pass = $tem['online_pass_perc'];
            $c_price = $tem['online_course_price'];
            $c_credit_price = $tem['online_course_credit_price'];
            $c_dur = $tem['online_course_duration'];
          ?>
            <h4><strong>Online</strong> | <strong>Start:</strong><? echo date("Y-m-d", $cor['c_start']); ?> <strong>End:</strong> <? echo date("Y-m-d", $cor['c_end']); ?></h4>
          <? }?>
          <? if($cor['c_type']=='vc'){
              $c_type = "Virtual Classroom";

              $c_pass = $tem['vc_pass_perc'];
              $c_price = $tem['vc_course_price'];
              $c_credit_price = $tem['vc_course_credit_price'];
              $c_dur = $tem['vc_course_duration'];
          ?>
            <h4><strong>Virtual Classroom</strong>
               | <strong>Start:</strong> <? echo date("Y-m-d", $cor['c_start']); ?> <strong>End:</strong> <? echo date("Y-m-d", $cor['c_end']); ?>|
              <? if($cor['c_link']!=""){ ?> <small><a href="https://<? echo $cor['c_link']; ?>" >Webinar Link</a></small> <? } ?>
            </h4>
          <? }?>
          <? if($cor['c_type']=='f2f'){
               $c_type = "Face To Face";

               $c_pass = $tem['f2f_pass_perc'];
               $c_price = $tem['f2f_course_price'];
               $c_credit_price = $tem['f2f_course_credit_price'];
               $c_dur = $tem['f2f_course_duration'];
          ?>
            <h4><strong>Face To Face</strong> | <strong>Start:</strong> <? echo date("Y-m-d", $cor['c_start']); ?> <strong>End:</strong> <? echo date("Y-m-d", $cor['c_end']); ?></h4>
          <? }?>

            <div class="col-lg-12 c_info">
                  <div class="col-lg-6">
                  <? echo $c_type; ?> Pass Percentage: <strong><? echo $c_pass; ?>%</strong>
                  </div>
                  <div class="col-lg-6">
                  <? echo $c_type; ?> Course Price: <strong>£<? echo $c_price; ?></strong>
                  </div>
                  <div class="col-lg-6">
                  <? echo $c_type; ?> Course Credit Price: <strong>£<? echo $c_credit_price; ?></strong>
                  </div>
                  <div class="col-lg-6">
                  <? echo $c_type; ?> Course Duration: <strong><? echo $c_dur; ?></strong>
                  </div>
            </div>
        </div>
    <!--home-->
  </div>
  <div id="menu1" class="tab-pane fade">
    <!--tab1-->
    <div class="col-lg-12">
      <h3 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Course Overview</h3>
      <br />
      <? echo $tem['description']; ?>
    </div>
    <!--tab1-->
  </div>
  <div id="menu2" class="tab-pane fade">
    <!--tab2-->
    <div class="col-lg-12">
        <h3 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Course Type Description</h3>
        <br />
        <? if($cor['c_type']=='online'){ ?>
            <? echo $tem['online_description']; ?>
        <? }?>
        <? if($cor['c_type']=='vc'){ ?>
            <? echo $tem['vc_description']; ?>
        <? }?>
        <? if($cor['c_type']=='f2f'){ ?>
            <? echo $tem['f2f_description']; ?>
        <? }?>
        <br />
    </div>
    <!--tab2-->
  </div>
  <div id="menu3" class="tab-pane fade">
    <!--tab31-->
    <div class="col-lg-12">
      <h3 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Course Description</h3>
      <br />
      <? echo $cor['c_desc']; ?>
      <br />
      <? if($cor['c_type']=='online'){ ?>
          <? echo $tem['online_description']; ?>
      <? }?>
      <? if($cor['c_type']=='vc'){ ?>
          <? echo $tem['vc_description']; ?>
      <? }?>
      <? if($cor['c_type']=='f2f'){ ?>
          <? echo $tem['f2f_description']; ?>
      <? }?>
    </div>
    <!--tab3-->
  </div>

  <div id="menu4" class="tab-pane fade">
    <!--tab4-->
    <div class="col-lg-12">
      <h3 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Other Dates</h3>
      <br />
      <?
      $db->query("select * from lms_course_inst_date where c_id = ?");
      $db->bind(1,$cor["id"]);
      $d = $db->ResultSet();
      foreach ($d as $key => $value){ ?>
        <div class="col-lg-12" style="border-bottom:1px #b8b8b8 solid; padding-bottom:10px; margin-bottom:10px;">
          <div class="col-lg-2">
            <strong><? echo date("d-m-Y", $value['c_date']); ?> <? echo date("H:i", $value['c_date']); ?></strong>
          </div>
          <div class="col-lg-10">
            <strong><? echo ucwords($value['title']);?></strong>
            <? if(time() > $value['c_date']){ ?>
                  <button type="button" class="btn btn-raised btn-info btn-xs pull-right register_but" data-id="<? echo $_GET['id'];?>" data-book="<? echo $value['id'];?>" > Register </button>
            <? } else{ ?>
                  <button type="button" class="btn btn-raised btn-info btn-xs pull-right disabled" disabled> Register </button>
            <? } ?>
          </div>
        </div>
        <?
        }
        ?>
    </div>
    <!--tab4-->
  </div>



  <div id="menu6" class="tab-pane fade">
    <!--tab4-->
    <div class="col-lg-12">
      <h3 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Learners</h3>
      <br />

      <button data-id="<? echo $_GET['id']; ?>" class="btn btn-sm btn-success btn-raised pull-right add_learning" >Add Learner</button>

<!--  -->
<!--  -->

               <table id="table" class="striped"
               data-toggle="table"
               data-show-export="false"
               data-export-types="['excel']"
               data-click-to-select="true"
               data-filter-control="true"
               data-show-columns="true"
               data-show-refresh="true"
               data-url="<? echo $fullurl; ?>assets/app_ajax/courses/fetch_learners_for_course.php?course=<? echo $_GET['id']; ?>"
               data-height="400"
               data-side-pagination="server"
               data-pagination="true"
               data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
               data-search="true">
                       <thead>
              <tr>
               <th data-field="id" data-sortable="true" data-visible="false">ID</th>
               <th data-field="first_name" data-sortable="true" data-visible="true" data-formatter="fullname">Name</th>
               <th data-field="email" data-sortable="true" data-visible="true">Email</th>
               <th data-field="pass_score" data-sortable="true" data-visible="true">Score</th>
               <th data-field="status" data-sortable="true" data-visible="true" data-formatter="CourseStatusFormatter">Status</th>
               </tr>
                       </thead>
               </table>



<!--  -->
<!--  -->
    </div>
    <!--tab4-->
  </div>


<!--  -->
<!--  -->
<!--  -->
<div id="menu7" class="tab-pane fade">
  <!--tab4-->
  <div class="col-lg-12">
    <h3 style="padding-bottom:5px; border-bottom:2px solid #2e2e2e; width:100%;">Course Credit Log</h3>
    <br />


<!--  -->
<!--  -->

             <table id="table" class="striped"
             data-toggle="table"
             data-show-export="false"
             data-export-types="['excel']"
             data-click-to-select="true"
             data-filter-control="true"
             data-show-columns="true"
             data-show-refresh="true"
             data-url="<? echo $fullurl; ?>assets/app_ajax/courses/fetch_course_log.php?course=<? echo $_GET['id']; ?>"
             data-height="400"
             data-side-pagination="server"
             data-pagination="true"
             data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
             data-search="true">
                     <thead>
            <tr>
             <th data-field="stype" data-sortable="true" data-visible="true" data-formatter="log_type">Type</th>
             <th data-field="first_name" data-sortable="true" data-visible="true" data-formatter="fullname2">Name</th>
             <th data-field="description" data-sortable="true" data-visible="true">Description</th>
              <!--<th data-field="status" data-sortable="true" data-visible="true" data-formatter="CourseStatusFormatter">Status</th> -->
             </tr>
                     </thead>
             </table>



<!--  -->
<!--  -->
  </div>
  <!--tab4-->
</div>
<!--  -->
<!--  -->
<!--  -->

</div>

<!-- content -->
<!-- content -->
                          </div>
                        </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
    //JS Include
    include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>

    <script>

    function fullname2(value, row, index) {
        return row.first_name + ' ' + row.surname;
    }


    //debited
    //credited
    //refunded

    function log_type(value, row, index) {
            if(value=='debited'){ return "<span class='label label-success'>Debited</span>";}
            else if(value=='credited'){ return "<span class='label label-info '>Credited</span>"; }
            else if(value=='refunded'){ return "<span class='label label-danger'>Refunded</span>"; }
            else{ return "<span class='label label-warning'>Error</span>";}
    }



    function fullname(value, row, index) {
          return '<a href="javascript: void(0)" data-id="'+row.id+'" class="text-capitalize course_learner_info">'+row.first_name + ' ' + row.surname+'</a>';
    }

    function CourseStatusFormatter(value, row, index) {
          if(value=='1'){ return "<span class='label label-success'>Assigned Course</span>";}
          else if(value=='2'){ return "<span class='label label-info '>In Progress</span>"; }
          else if(value=='3'){ return "<span class='label label-danger'>Finished Course</span>"; }
          else if(value=='4'){ return "<span class='label label-warning labelTeal'>Completed Course</span>"; }
          else if(value=='5'){ return "<span class='label label-warning labelRefund'>Refunded</span>"; }
          else{ return "<span class='label label-warning'>Error</span>";}
    }

    <? if(isset($_GET['reload'])){ ?>
        $('.nav-tabs a[href="#menu6"]').tab('show');
    <? } ?>

    $( "body" ).on( "click", ".add_learning", function(e) {

          $id = $(this).data('id');

          $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
          $("#BaseModalL").modal('show');
          $.ajax({
             type: "POST",
             url: "../../assets/app_ajax/courses/fetch_learners.php?id="+$id,
             success: function(msg){
                //alert(msg);
                $("#BaseModalLContent").delay(1000)
                   .queue(function(n) {
                      $(this).html(msg);
                      n();
                   }).fadeIn("slow").queue(function(n) {
                      $.material.init();
                      n();
                });
             }
          });
    });



$( "body" ).on( "click", ".save_add_learning", function(e) {


            $id = $(this).data('id');

            var formid = '#save_add_learning_form';
            var FRMdata = $(formid).serialize(); // get form data


            $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
            $("#BaseModalL").modal('show');
            $.ajax({
               type: "POST",
               data: FRMdata,
               url: "../../assets/app_ajax/courses/save_learners_on_course.php?id="+$id,
               success: function(msg){
                      $("#BaseModalL").modal('hide');
                       $message=$.trim(msg);
                       Messenger().post({
                             message: 'Learner Added',
                             showCloseButton: false
                       });
                       setTimeout(function(){
                         window.location.search += '&reload';
                       },600);
                 }
            });


});




$( "body" ).on( "click", ".course_learner_info", function(e) {
      $id = $(this).data('id');
      $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
      $("#BaseModalL").modal('show');
      $.ajax({
         type: "POST",
         url: "../../assets/app_ajax/courses/fetch_learner_info.php?id="+$id,
         success: function(msg){
            //alert(msg);
            $("#BaseModalLContent").delay(1000)
               .queue(function(n) {
                  $(this).html(msg);
                  n();
               }).fadeIn("slow").queue(function(n) {
                  $.material.init();
                  n();
            });
         }
      });
});




$( "body" ).on( "click", ".save_add_learning_edit", function(e) {

            $id = $(this).data('id');
            $book = $(this).data('book');

            var formid = '#save_add_learning_edit_form';
            var FRMdata = $(formid).serialize(); // get form data


            $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
            $("#BaseModalL").modal('show');
            $.ajax({
               type: "POST",
               data: FRMdata,
               url: "../../assets/app_ajax/courses/save_learners_on_course_edit.php?id="+$id+"&book="+$book,
               success: function(msg){
                      $("#BaseModalL").modal('hide');
                       $message=$.trim(msg);
                       Messenger().post({
                             message: 'Learner Course Edited',
                             showCloseButton: false
                       });
                       setTimeout(function(){
                        window.location.search += '&reload';
                       },600);
                 }
            });


});


$( "body" ).on( "click", ".register_but", function(e) {
      $id = $(this).data('id');
      $book = $(this).data('book');
      $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
      $("#BaseModalL").modal('show');
      $.ajax({
         type: "POST",
         url: "../../assets/app_ajax/courses/learner_register.php?id="+$id+"&book="+$book,
         success: function(msg){
            //alert(msg);
            $("#BaseModalLContent").delay(1000)
               .queue(function(n) {
                  $(this).html(msg);
                  n();
               }).fadeIn("slow").queue(function(n) {
                  $.material.init();
                  n();
            });
         }
      });
});


$( "body" ).on( "click", ".save_reg_learner", function(e) {

            $id = $(this).data('id');
            $book = $(this).data('book');

            var formid = '#save_reg_learner_form';
            var FRMdata = $(formid).serialize(); // get form data


            console.log(FRMdata);

            $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
            $("#BaseModalL").modal('show');
            $.ajax({
               type: "POST",
               data: FRMdata,
               url: "../../assets/app_ajax/courses/save_reg_learner.php?id="+$id+"&book="+$book,
               success: function(msg){
                      $("#BaseModalL").modal('hide');
                       $message=$.trim(msg);
                       Messenger().post({
                             message: 'Learners Registered',
                             showCloseButton: false
                       });
                 }
            });


});


$( "body" ).on( "click", ".remove_learning_edit", function(e) {

    $('.confirm_remove_div').toggle();

});



$( "body" ).on( "click", ".cancel_remove_learning_edit", function(e) {
      $('.confirm_remove_div').toggle();
});


$( "body" ).on( "click", ".confirm_remove_learning_edit", function(e) {

    //$('.confirm_remove_div').toggle();

    $learner = $(this).data('learner');
    $book = $(this).data('book');


    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');
    $.ajax({
       type: "POST",
       data: {learner:$learner,book:$book},
       url: "../../assets/app_ajax/courses/remove_reg_learner.php",
       // success: function(msg){
       //        $("#BaseModalL").modal('hide');
       //         $message=$.trim(msg);
       //         Messenger().post({
       //               message: 'Learners Registered',
       //               showCloseButton: false
       //         });
       //   }
    });



});




    </script>


</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
