<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
 //Page Title
 $page_title = '';

 header('Location: '.$fullurl.'login.php');
 exit();

 ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>HiveX - Business Management System</title>

<!-- Bootstrap Stylesheet -->
<link href=" <? echo $fullurl ?>assets/css/bootstrap.css" rel="stylesheet">
<link href=" <? echo $fullurl ?>assets/css/bootstrap-material-design.css" rel="stylesheet">
<link href=" <? echo $fullurl ?>assets/css/ripples.css" rel="stylesheet">


<!-- Fonts -->
<link href=" <? echo $fullurl ?>assets/css/fonts.css" rel="stylesheet">


<!-- Application -->
<link href=" <? echo $fullurl ?>assets/css/base.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="<? echo $fullurl ?>assets/js/html5shiv.js"></script>
        <script src="<? echo $fullurl ?>assets/js/respond.min.js"></script>
    <![endif]-->

<style>

</style>
<?php
if(isset($_GET['re'])){
echo '<script type="text/javascript">window.top.location.href = "http://'.$_SERVER['HTTP_HOST'].'/index.php"; </script>';
}
?>
  <style>
  body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;

}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
    color: black;
}
.form-group {
	text-align: left;
}
.form-group label {
	color:#363636;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

body {
	color: black;
  height: 100%;
}

html{
  height: 100%;
}


.logon {
	background-color: rgba(226, 226, 226, 0.95);
	border-radius: 20px;
	border: #337ab7 2px solid;
	width: 400px;
	text-align: center;
}


  </style>

  </head>

  <body>
  <div class="wrapper">

    <div class="container logon">

      <form class="form-signin" action="<? echo $fullurl ?>assets/app_php/auth.php?action=logon" enctype="application/x-www-form-urlencoded" method="post">
	      <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="max-width:100%">
	     <p>Sign In</p>
	     <? if (isset($_GET['error'])) { ?>


       <div class="alert alert-warning alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  Username or Password Incorrect.
		</div>

		<? } ?>
		<? if (isset($_GET['account'])) { ?>
		<div class="alert alert-warning alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  Account has been locked.
		</div>
		<? } ?>



		<div class="form-group label-floating is-empty">
                <label for="login" class="control-label">Email</label>
                <input type="email" name="login" class="form-control"  required>

              <span class="material-input"></span>
        </div>

        <div class="form-group label-floating is-empty">
                <label for="password" class="control-label">Password</label>
                <input type="password" class="form-control" name="password" required>

              <span class="material-input"></span>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="1" name="remember"> Remember me
          </label>
        </div>
        <input type="hidden" name="key" value="<? echo getCurrentKey(); ?>">
        <input type="hidden" name="username">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <br><small>Version <?php echo version; ?></small>
      </form>
    </div>
    <div class="preFooter">
    </div>
  </div>
    <div class="footer">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="col-sm-6 col-md-6 col-lg-6">
          <div style="float: left; margin: 0 auto;">
            <img src="<? echo $fullurl ?>assets/images/hivex200.png" style="margin: 0 auto; vertical-align: middle; width: 120px;"/>
          </div>
          <div style="float: right; margin: 0 auto;">
            <button class="btn btn-primary support">Support</button>
          </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">
          <div style="float: left; margin: 0 auto;">
            <button class="btn btn-primary training">Training</button>
          </div>
          <div style="float: right; margin: 0 auto;">
            <img src="<? echo $fullurl ?>assets/images/skylogo.png" style="margin: 0 auto; vertical-align: middle; width: auto; height: 55px;"/>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
  <!-- jQuery -->
    <script src="<? echo $fullurl ?>assets/js/jquery.min.js"></script>
    <!-- Base Plugins -->
    <script src="<? echo $fullurl ?>assets/js/bootstrap.min.js"></script>
    <script src="<? echo $fullurl ?>assets/js/material.js"></script>
    <script src="<? echo $fullurl ?>assets/js/ripples.js"></script>
    <script src="<? echo $fullurl ?>assets/js/base.js"></script>







       <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<? echo $fullurl ?>assets/js/jquery.backstretch.min.js"></script>
     <script>

	    var images = [
    "<? echo $fullurl ?>assets/logonImages/bg1.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg2.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg3.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg4.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg5.jpg",
    "<? echo $fullurl ?>assets/logonImages/bg6.jpg"

    ];

// The index variable will keep track of which image is currently showing
var index = 0,oldIndex;
 index = Math.floor((Math.random()*images.length));

        $.backstretch(images[index]);

    // Set an interval that increments the index and sets the new image
    // Note: The fadeIn speed set above will be inherited
    //

    setInterval(function() {
       oldIndex = index;
        while (oldIndex == index) {
            index = Math.floor((Math.random()*images.length));
        }
        $.backstretch(images[index]);
    }, 8000);

    // A little script for preloading all of the images
    // It"s not necessary, but generally a good idea
    $(images).each(function() {
        $("<img/>")[0].src = this;
    });


    $( "body" ).on( "click", ".support", function() {
            $('.warning').hide();
            $('.sentMessage').hide();
            document.getElementById("supportForm").reset();
            $('#supportModal').modal('show');
        //$('.panel-default').slideToggle();
    });

    $( "body" ).on( "click", ".training", function() {
            $('#training').modal('show');
        //$('.panel-default').slideToggle();
    });

    $( "body" ).on( "click", "#closeSupportForm", function() {
            $('#supportModal').modal('hide');
        //$('.panel-default').slideToggle();
    });

    $( "body" ).on( "click", "#tClose", function() {
            $('#training').modal('hide');
        //$('.panel-default').slideToggle();
    });

		$( document ).ready(function() {
		    $().alert('close')
		});

    </script>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="supportModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
    <div class="modal-body " id="supportContent">
        <h3 style="padding-bottom: 2px;">Support</h3>
        <p>
          If you are having an issue with the system please fill in the form below;
        </p>
        <div class="warning alert alert-warning alert-dismissible" hidden>
          <p>
            Please insert all information into the form
          </p>
        </div>
        <div class="sentMessage alert alert-success alert-dismissible" hidden>
          <p>
            Message has been sent
          </p>
        </div>
        <?
            $form = new Form;
            $form->createForm('supportForm',null,'post','application/x-www-form-urlencoded','');
            $form->createDiv('col-md-12 col-sm-12');
                    $form->createFloatingTextField('text', 'Name*', 'name',null,'yes');
                    $form->createFloatingTextField('text', 'Phone*', 'phone',null,'yes');
                    $form->createFloatingTextField('email', 'Email*', 'email',null,'yes');
                    $form->createFloatingTextField('text','Issue*','issue',null,'yes');
            $form->endDiv();
            $form->createButton('closeSupportForm','Close','button','btn-danger btn-raised');
            $form->createButton('saveSupportForm','Submit','button','btn-primary btn-raised pull-right');
            $form->DisplayForm();
        ?>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="training">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
    <div class="modal-body " id="suppoerContent">
        <h3 style="padding-bottom: 2px;">Training</h3>
        <p>
          Email Blowfish Technology for training: <a href="mailto:hivex@blowfishtechnology.com?Subject=HiveX%20Training" target="_top">Blowfish Technology</a>
        </p>
        <button type="button"class="btn btn-danger btn-raised" id="tClose">Close</button>
        </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $(document).on('click', "#saveSupportForm", function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var issue = $('#issue').val();
    var phone = $('#phone').val();

    if(name == "" || email == "" || issue == "" || phone == ""){
      $('.warning').show();
    }else{
    $formInfo = $("#supportForm").serialize();
                 $.ajax({
                    type: "POST",
                    url: "../../../assets/app_ajax/sendSupport.php",
                    data: $formInfo,
                    success: function(msg){
                      $('.sentMessage').show();
                         setTimeout(function(){
                             $('#supportModal').modal('hide');
                             }, 2000);
                       }
                     });
                   }
                 });
}              );
</script>
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
