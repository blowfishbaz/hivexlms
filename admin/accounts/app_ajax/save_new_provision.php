<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $account_id = createid('prov');
 $name = $_POST['prov_name'];
 $telephone = $_POST['main_telephone'];
 $address1 = $_POST['address1'];
 $address2 = $_POST['address2'];
 $address3 = $_POST['address3'];
 $city = $_POST['city'];
 $county = $_POST['county'];
 $country = $_POST['country'];
 $postcode = $_POST['postcode'];
 $company_id = $_POST['account_id'];


 $created_time = time();
 $created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db = new database;
 $db->Query("INSERT INTO ws_provision (id, company_id, created_date, created_by, provision_name, main_telephone, address1, address2, address3, city, county, country, postcode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
 $db->bind(1,$account_id);
 $db->bind(2,$company_id);
 $db->bind(3,$created_time);
 $db->bind(4,$created_by);
 $db->bind(5,$name);
 $db->bind(6,$telephone);
 $db->bind(7,$address1);
 $db->bind(8,$address2);
 $db->bind(9,$address3);
 $db->bind(10,$city);
 $db->bind(11,$county);
 $db->bind(12,$country);
 $db->bind(13,$postcode);
 $db->execute();




 echo 'ok';
