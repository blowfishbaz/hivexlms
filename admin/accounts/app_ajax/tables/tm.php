<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $final_array = array();

 $test_array = array(array('name'=>'josh', 'id' => '123', 'id_1' => '4', 'id_1_date' => '12345'),array('name'=>'josh2', 'id' => '456', 'id_1' => '4', 'id_1_date' => '12345'));

 $id = 'id_comp_20210217_101123718100_59489';

 $db->query("select ws_accounts.id as id, first_name, surname, email, account_type
 from ws_accounts
 left join ws_accounts_extra_info
 on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
 where ws_accounts_extra_info.company_id = ? order by first_name asc");
 $db->bind(1,$id);
 $learners = $db->ResultSet();


 echo $id.'<br /><br />';

 // echo '<pre>';
 // print_r($learners);
 // echo '</pre>';

 $db->query("select * from lms_course where status = 0");
 $courses = $db->resultset();

 foreach($learners as $l){
   $l_array = array();
   $course_array = array();

   $name = $l['first_name'].' '.$l['surname'];
   $l_id = $l['id'];

   $l_array['name'] = $name;
   $l_array['id'] = $l_id;

   foreach ($courses as $c) {

      //check whether they are on that course
      $db->query("select * from ws_booked_on where course_id = ? and account_id = ? and status = 1 order by id asc limit 1");
      $db->bind(1,$c['id']);
      $db->bind(2,$l_id);
      $course_check = $db->single();

      if(!empty($course_check)){
         $l_array[''.$c['id'].''] = $course_check['course_status'];

         if($course_check['course_status'] == 1){
            $l_array[''.$c['id'].'_date'] = $course_check['booked_on'];
         }else if($course_check['course_status'] == 2 || $course_check['course_status'] == 3){
            $l_array[''.$c['id'].'_date'] = $course_check['start_date'];
         }else if($course_check['course_status'] == 4){
            $l_array[''.$c['id'].'_date'] = $course_check['completed_date'];
         }else{
            $l_array[''.$c['id'].'_date'] = 'Err';
         }


      }else{
         $l_array[''.$c['id'].''] = '';
         $l_array[''.$c['id'].'_date'] = '';
      }


   }

   array_push($final_array,$l_array);
 }



 echo '<pre>';
 print_r($final_array);
 echo '</pre>';
