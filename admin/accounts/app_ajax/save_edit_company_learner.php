<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');



 $account_id = $_POST['user_id'];
 $firstname = $_POST['firstname'];
 $surname = $_POST['surname'];
 $job_title = $_POST['job_title'];
 $email = $_POST['email'];
 $password = $_POST['password'];
 $mobile = $_POST['mobile_number'];
 $telephone = $_POST['telephone_number'];

 $account_type = $_POST['account_type'];
 $provision_id = $_POST['provision_select'];
 $company_id = $_POST['company_id'];

 $created_time = time();
 $created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db = new database;
 $db->Query("update ws_accounts set first_name = ?, surname = ?, email = ?, username = ? where id = ?");
 $db->bind(1,$firstname);
 $db->bind(2,$surname);
 $db->bind(3,$email);
 $db->bind(4,encrypt($email));
 $db->bind(5,$account_id);
 $db->execute();

 $db = new database;
 $db->Query("update ws_accounts_extra_info set modified_date = ?,  modified_by = ?, status = ? where account_id = ?");
 $db->bind(1,$created_time);
 $db->bind(2,$created_by);
 $db->bind(3,'0');
 $db->bind(4,$account_id);
 $db->execute();


 $db = new database;
 $db->Query("insert into ws_accounts_extra_info (id, account_id, company_id, main_provision, created_date, created_by, mobile_number, telephone, job_title, status, account_type) values (?,?,?,?,?,?,?,?,?,?,?)");
 $db->bind(1,createid('ainfo'));
 $db->bind(2,$account_id);
 $db->bind(3,$company_id);
 $db->bind(4,$provision_id);
 $db->bind(5,$created_time);
 $db->bind(6,$created_by);
 $db->bind(7,$mobile);
 $db->bind(8,$telephone);
 $db->bind(9,$job_title);
 $db->bind(10,'1');
 $db->bind(11,$account_type);
 $db->execute();



 echo 'ok';
