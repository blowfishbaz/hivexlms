<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $id = $_GET['id'];

 $db->query("select * from ws_provision where company_id = ? and status = 1");
 $db->bind(1,$id);
 //$data = $db->single();



?>



    <div class="panel panel-default">
      <div class="panel-body">

        <!-- <h4 class="section_header">Provisions</h4> -->
        <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="add_learner">Add</button>
        <div class="clearfix">

        </div>

        <table id="address_table" class="striped"
data-toggle="table"
data-show-export="true"
data-click-to-select="true"
data-filter-control="true"
data-show-columns="true"
data-show-refresh="true"

data-url="../../../admin/accounts/app_ajax/tables/learners_tab.php?id=<?echo $id;?>"
data-height="400"
data-side-pagination="server"
data-pagination="false"
data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
data-search="true"
data-page-size="100"
>
    <thead>
        <tr>

            <th data-field="first_name" data-sortable="false" data-visible="true" data-formatter="LinkEditCompanyLearner">Name</th>
            <th data-field="surname" data-sortable="false" data-visible="false">Surname</th>
            <th data-field="email" data-sortable="false" data-visible="true">Email</th>
            <th data-field="provision_name" data-sortable="false" data-visible="true">Provision</th>
            <th data-field="account_type" data-sortable="false" data-visible="true">Account Type</th>

        </tr>
    </thead>
</table>


<script src="<? echo $fullurl ?>assets/js/bootstrap-table.js"></script>
<script src="<? echo $fullurl ?>assets/js/table-extensions/export/bootstrap-table-export.js"></script>

      </div>
    </div>
