<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $id = $_GET['id'];

 $db->query("select * from ws_provision where company_id = ? and status = 1");
 $db->bind(1,$id);
 //$data = $db->single();




 //1. Company
 $company_id = $id;
 $db->query("select * from ws_companies where id = ?");
 $db->bind(1,$id);
 $company_information = $db->single();

 //2. Provisions

 $db->query("select * from ws_provision where company_id = ? and status = 1");
 $db->bind(1,$id);
 $provisions = $db->resultset();



?>
<style>


  .sections_box {
    border: dotted 1px #bdbebd;
    margin-bottom: 2px;
    padding: 2px 0 0 15px;
    /*text-transform: capitalize;*/
    min-height:46px;
  }

  .newsections {
    text-transform: capitalize;
  }

  .addsection,
  .saveeditsection {
    margin: 0 0 0 5px;
    padding: 4px 20px;
  }

  .editsection,
  .delsection {
    margin: 15px 5px 0 0px;
    padding: 0;
    float: right;
  }

  .editsection .glyphicons,
  .delsection .glyphicons {
    font-size: x-small;
  }

  .btn-group{

    box-shadow: none!important;
    -webkit-box-shadow: none!important;
  }

  .viewEdit{
    position: relative;
    float: right;
    margin-top: 7px!important;
    margin-right: 5px!important;
  }

  .deep{
    background-color: #5487b3;
  }

  .deep1{
    background-color: #83a6c3;
  }

  .deep2{
    background-color: #d1e4f5 ;
  }

  .deep3{
    background-color: white;
  }

  label {
    color: #4a4a4a!important;
  }

</style>
<!-- glyphicons-user-structure - manager -->

    <div class="panel panel-default">
      <div class="panel-body">


          <ul class="list-group">
              <li class="list-group-item deep">
                  <span class="glyphicons glyphicons-umbrella" style="font-size:11px;"></span><?echo $company_information['company_name'];?>

          <div class="pull-right">
          </div>
          <!-- This is the inner bit -->
          <?//Admin
            $db->query("select ws_accounts.*
            from ws_accounts
            left join ws_accounts_extra_info
            on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
            where ws_accounts_extra_info.company_id = ? and ws_accounts_extra_info.main_provision = ? and ws_accounts_extra_info.account_type = 'Admin'");
            $db->bind(1,$id);
            $db->bind(2,'N/A');
            $admin_accounts = $db->resultset();

          ?>

          <?foreach ($admin_accounts as $admin) {?>
            <li class="list-group-item deep1">
                <span class="glyphicons glyphicons-family" style="font-size:11px; margin-left: 3em;"></span><?echo $admin['first_name'].' '.$admin['surname'];?>
              <div class="pull-right">Admin
              </div>
            </li>
          <?}?>

          <?//Managers
            $db->query("select ws_accounts.*
            from ws_accounts
            left join ws_accounts_extra_info
            on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
            where ws_accounts_extra_info.company_id = ? and ws_accounts_extra_info.main_provision = ? and ws_accounts_extra_info.account_type = 'Manager'");
            $db->bind(1,$id);
            $db->bind(2,'N/A');
            $manager_account = $db->resultset();
          ?>

          <?foreach ($manager_account as $manager) {?>
            <li class="list-group-item deep2">
                <span class="glyphicons glyphicons-user-structure" style="font-size:11px; margin-left: 6em;"></span><?echo $manager['first_name'].' '.$manager['surname'];?>
              <div class="pull-right">Manager
              </div>
            </li>
          <?}?>

          <?//Managers
            $db->query("select ws_accounts.*
            from ws_accounts
            left join ws_accounts_extra_info
            on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
            where ws_accounts_extra_info.company_id = ? and ws_accounts_extra_info.main_provision = ? and ws_accounts_extra_info.account_type = 'Learner'");
            $db->bind(1,$id);
            $db->bind(2,'N/A');
            $learner_accounts = $db->resultset();
          ?>
          <?foreach ($learner_accounts as $learner) {?>
            <li class="list-group-item deep3">
                <span class="glyphicons glyphicons-user" style="font-size:11px; margin-left: 9em;"></span><?echo $learner['first_name'].' '.$learner['surname'];?>
              <div class="pull-right">Learner
              </div>
            </li>
          <?}?>
          </li>
          <br />
          </ul>





          <!-- THIS IS WHERE THE SPLIT BETWEEN PROVISIONS & COMPANY LEVEL  -->





          <?foreach ($provisions as $prov) {?>

                      <ul class="list-group">
                          <li class="list-group-item deep">
                              <span class="glyphicons glyphicons-map" style="font-size:11px;"></span><?echo $prov['provision_name'];?>

                      <div class="pull-right">
                      </div>
                      </li>
                      <!-- This is the inner bit -->
                      <?//Admin
                        $db->query("select ws_accounts.*
                        from ws_accounts
                        left join ws_accounts_extra_info
                        on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
                        where ws_accounts_extra_info.company_id = ? and ws_accounts_extra_info.main_provision = ? and ws_accounts_extra_info.account_type = 'Admin'");
                        $db->bind(1,$id);
                        $db->bind(2,$prov['id']);
                        $admin_accounts = $db->resultset();

                      ?>

                      <?foreach ($admin_accounts as $admin) {?>
                        <li class="list-group-item deep1">
                            <span class="glyphicons glyphicons-family" style="font-size:11px; margin-left: 3em;"></span><?echo $admin['first_name'].' '.$admin['surname'];?>
                          <div class="pull-right">Admin
                          </div>
                        </li>
                      <?}?>

                      <?//Managers
                        $db->query("select ws_accounts.*
                        from ws_accounts
                        left join ws_accounts_extra_info
                        on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
                        where ws_accounts_extra_info.company_id = ? and ws_accounts_extra_info.main_provision = ? and ws_accounts_extra_info.account_type = 'Manager'");
                        $db->bind(1,$id);
                        $db->bind(2,$prov['id']);
                        $manager_account = $db->resultset();
                      ?>

                      <?foreach ($manager_account as $manager) {?>
                        <li class="list-group-item deep2">
                            <span class="glyphicons glyphicons-user-structure" style="font-size:11px; margin-left: 6em;"></span><?echo $manager['first_name'].' '.$manager['surname'];?>
                          <div class="pull-right">Manager
                          </div>
                        </li>
                      <?}?>

                      <?//Managers
                        $db->query("select ws_accounts.*
                        from ws_accounts
                        left join ws_accounts_extra_info
                        on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
                        where ws_accounts_extra_info.company_id = ? and ws_accounts_extra_info.main_provision = ? and ws_accounts_extra_info.account_type = 'Learner'");
                        $db->bind(1,$id);
                        $db->bind(2,$prov['id']);
                        $learner_accounts = $db->resultset();
                      ?>
                      <?foreach ($learner_accounts as $learner) {?>
                        <li class="list-group-item deep3">
                            <span class="glyphicons glyphicons-user" style="font-size:11px; margin-left: 9em;"></span><?echo $learner['first_name'].' '.$learner['surname'];?>
                          <div class="pull-right">Learner
                          </div>
                        </li>
                      <?}?>


                      <br />
                      </ul>
          <?}?>








      </div>
    </div>
