<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 $id = $_GET['id'];

 $array = explode("_", $id);

 $type = $array['1'];

?>



    <div class="panel panel-default">
      <div class="panel-body">

        <h4 class="section_header">Information</h4>

        <?if($type == 'comp'){?>
            <?
                $db->query("select * from ws_companies where id = ?");
                $db->bind(1,$id);
                $company = $db->single();

            ?>
            <span class="glyphicons glyphicons-earphone"></span> <a href="javascript:void(0)" class="phone_number"><?echo $company['main_telephone'];?></a><br />
            <span class="glyphicons glyphicons-envelope"></span> <a href="mailto:<?echo $company['main_email'];?>"><?echo $company['main_email'];?></a><br />
            <span class="glyphicons glyphicons-coins"></span> <?echo $company['credit'];?><br />
        <?}else{?>
          <?
          $db->query("select * from ws_accounts where id = ?");
          $db->bind(1,$id);
          $account = $db->single();

          $db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
          $db->bind(1,$id);
          $info = $db->single();
          ?>
          <span class="glyphicons glyphicons-security-camera"></span> <a href="javascript:void(0)" class="phone_number">Account Type - <?echo $info['account_type'];?></a><br />
          <span class="glyphicons glyphicons-menu-hamburger"></span> <a href="javascript:void(0)" class="phone_number">Job Title - <?echo $info['job_title'];?></a><br />
          <span class="glyphicons glyphicons-iphone"></span> <a href="javascript:void(0)" class="phone_number"><?echo $info['mobile_number'];?></a><br />
          <span class="glyphicons glyphicons-earphone"></span> <a href="javascript:void(0)" class="phone_number"><?echo $info['telephone'];?></a><br />
          <span class="glyphicons glyphicons-envelope"></span> <a href="mailto:<?echo $account['email'];?>"><?echo $account['email'];?></a><br />
        <?}?>

      </div>
    </div>
