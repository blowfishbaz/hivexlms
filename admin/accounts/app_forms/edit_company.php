<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 $db->query("select * from ws_companies where id = ?");
 $db->bind(1,$id);
 $data = $db->single();

?>

<h3>Edit Company</h3>

<form id="edit_company_form">
  <input type="hidden" name="company_id" value="<?echo $id;?>" />
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating">
          <label for="company_name" class="control-label">Company Name*</label>
          <input type="text" class="form-control" id="company_name" name="company_name" required="yes" value="<?echo $data['company_name'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating">
          <label for="main_email" class="control-label">Main Email</label>
          <input type="text" class="form-control" id="main_email" name="main_email" value="<?echo $data['main_email'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating">
          <label for="main_telephone" class="control-label">Main Telephone*</label>
          <input type="number" class="form-control" id="main_telephone" name="main_telephone" required="yes" value="<?echo $data['main_telephone'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating">
          <label for="main_credit" class="control-label">Credit</label>
          <input type="number" class="form-control" id="main_credit" name="main_credit" required="yes" value="<?echo $data['credit'];?>">
          <span class="material-input"></span>
      </div>
  </div>



</form>



<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_edit_company">Save</button>
