<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 $db->query("select * from ws_accounts where id = ?");
 $db->bind(1,$id);
 $data = $db->single();

 $db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
 $db->bind(1,$id);
 $info = $db->single();

?>

<h3>Edit Learner</h3>

<form id="edit_account_form">
  <input type="hidden" name="user_id" value="<?echo $id;?>" />
  <input type="hidden" name="company_id" value="<?echo $info['company_id'];?>" />
  <input type="hidden" name="provision_id" value="<?echo $info['main_provision'];?>" />
  <input type="hidden" name="account_type" value="<?echo $info['account_type'];?>" />

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="firstname" class="control-label">First Name*</label>
          <input type="text" class="form-control" id="firstname" name="firstname" required="yes" value="<?echo $data['first_name'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="surname" class="control-label">Surname*</label>
          <input type="text" class="form-control" id="surname" name="surname" required="yes" value="<?echo $data['surname'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="job_title" class="control-label">Job Title</label>
          <input type="text" class="form-control" id="job_title" name="job_title"value="<?echo $info['job_title'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="email" class="control-label">Email*</label>
          <input type="text" class="form-control" id="email" name="email" required="yes" value="<?echo $data['email'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="mobile_number" class="control-label">Mobile Number</label>
          <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="<?echo $info['mobile_number'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="telephone_number" class="control-label">Telephone Number</label>
          <input type="text" class="form-control" id="telephone_number" name="telephone_number" value="<?echo $info['telephone'];?>">
          <span class="material-input"></span>
      </div>
  </div>



</form>



<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_edit_account">Save</button>
