<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $id = $_GET['id'];

 $db->query("select * from ws_provision where status = ? and company_id = ?");
 $db->bind(1,'1');
 $db->bind(2,$_GET['id']);
 $data = $db->ResultSet();

?>

<h3>New Learner</h3>

<form id="new_learner_company_form">
  <input type="hidden" name="company_id" value="<?echo $id;?>" />
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="firstname" class="control-label">First Name*</label>
          <input type="text" class="form-control" id="firstname" name="firstname" required="yes" value="">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="surname" class="control-label">Surname*</label>
          <input type="text" class="form-control" id="surname" name="surname" required="yes" value="">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="job_title" class="control-label">Job Title</label>
          <input type="text" class="form-control" id="job_title" name="job_title"value="">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="email" class="control-label">Email*</label>
          <input type="text" class="form-control" id="email" name="email" required="yes" value="">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="password" class="control-label">Password*</label>
          <input type="text" class="form-control" id="password" name="password" required="yes" value="">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="mobile_number" class="control-label">Mobile Number</label>
          <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="telephone_number" class="control-label">Telephone Number</label>
          <input type="text" class="form-control" id="telephone_number" name="telephone_number" value="">
          <span class="material-input"></span>
      </div>
  </div>




  <div class="col-md-12 col-sm-12">
      <div class="form-group">
        <label class="control-label pull-left" for="account_type">Account Type*: </label>
        <select class="form-control chosen-select" id="account_type" name="account_type" required>
          <option value="" selected="" disabled="">Select</option>
          <option value="Admin">Admin</option>
          <option value="Manager">Manager</option>
          <option value="Learner">Learner</option>
        </select>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group">
        <label class="control-label pull-left" for="provision_select">Provision Select*: </label>
        <select class="form-control chosen-select" id="provision_select" name="provision_select" required>
          <option value="" selected="" disabled="">Select</option>
          <option value="N/A">N/A</option>
          <?foreach ($data as $d) {?>
            <option value="<?echo $d['id'];?>"><?echo $d['provision_name'];?></option>
          <?}?>
        </select>
      </div>
  </div>



</form>



<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_new_learner_company">Save</button>
