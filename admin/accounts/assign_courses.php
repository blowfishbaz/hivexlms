<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }


 function get_my_account_type($id){
     $db = new database;
     $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
     $db->bind(1,$id);
     $me = $db->single();

     return $me['account_type'];
 }

 function get_my_company($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['company_id'];
 }

 function get_my_provision($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['main_provision'];
 }

 function get_my_course_progress_tm($account_id, $course_id){
   $db = new database;
   $db->query("select * from ws_booked_on where course_id = ? and account_id = ? and status = 1 order by id asc limit 1");
   $db->bind(1,$course_id);
   $db->bind(2,$account_id);
   $course_check = $db->single();

   $completed_date = $course_check['completed_date'];



    $run_out_date = date('Y-m-d', strtotime('+12 months', $completed_date) );
    $run_out_date_epoch = strtotime($run_out_date);
    $due_date = date('Y-m-d', strtotime('-3 months', strtotime($run_out_date)) );
    $due_date_epoch = strtotime($due_date);





  if($run_out_date_epoch < time()){
      $color = 'table_expired';
  }else if($run_out_date_epoch >= time() && $due_date_epoch <= time()){
      $color = 'table_due';
  }else{
      $color = 'table_completed';
  }




   switch ($course_check['course_status']) {
     case '1':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['booked_on'],'colour'=>'table_assigned');
       break;
     case '2':
     case '3':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['start_date'],'colour'=>'table_assigned');
       break;
     case '4':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['completed_date'],'colour'=>$color);
       break;
     case '5':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['course_date'],'colour'=>'table_did_not_attend');
       break;
     default:
       return array('status'=>'Error','course_date'=>'0','colour'=>'');
       break;
   }
 }



 $db->query("select * from lms_course where status = 0");
 $courses = $db->resultset();

  $id = 'id_user_20210218_143738065300_48226';//Admin
  //$id = 'id_user_20210218_143656536000_11611';//Manager


  $my_account_type = get_my_account_type($id);
  $my_company = get_my_company($id);
  $my_provision = get_my_provision($id);

  if($my_account_type == 'Admin'){
    //Get all the learners
    $db->query("select ws_accounts.id as id, first_name, surname, email, account_type
    from ws_accounts
    left join ws_accounts_extra_info
    on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
    where ws_accounts_extra_info.company_id = ? order by first_name asc");
    $db->bind(1,$my_company);
    $learners = $db->ResultSet();
  }else if($my_account_type == 'Manager'){
    $db->query("select ws_accounts.id as id, first_name, surname, email, account_type
    from ws_accounts
    left join ws_accounts_extra_info
    on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
    where ws_accounts_extra_info.main_provision = ? order by first_name asc");
    $db->bind(1,$my_provision);
    $learners = $db->ResultSet();
  }

  //Company ID




 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>
  <style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  .chosen-container-single {
    width: 100%!important;
}

  </style>
<!-- On Page CSS -->
</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">Course Sign Up</span>

                        </div>

                        <div class="clearfix"></div>


                        <div class="panel panel-default userlist">
                               <div class="panel-body">

                                 <form id="assign_course_form">
                                   <input type="hidden" name="counter" value="1" id="counter" />

                                 <table id="course_table">
                                   <tr>
                                     <th style="width:400px;">Course</th>
                                     <th style="width:400px;">Learners</th>
                                     <th>How Many</th>
                                     <th style="width: 112px;">Remove</th>
                                   </tr>
                                   <tr>
                                     <td>
                                       <div class="form-group is-empty" style="margin-top:0px;"><label class="control-label" for="course_select_1">Course Select</label>
                                            <select class="form-control chosen_select" id="course_select_1" name="course_select_1" required>
                                                <option value="" selected disabled>Select</option>
                                                <?foreach ($courses as $c) {?>
                                                  <option value="<?echo $c['id'];?>"><?echo $c['title'];?></option>
                                                <?}?>
                                            </select>
                                            <span class="material-input"></span>
                                        </div>
                                     </td>
                                     <td>
                                       <div class="form-group is-empty" style="margin-top:0px;"><label class="control-label" for="learner_select_1">Learner/s Select</label><br />
                                            <select class="form-control chosen_select learner_select" id="learner_select_1" name="learner_select_1[]" data-id="1" required multiple>
                                                <?foreach ($learners as $l) {?>
                                                  <option value="<?echo $l['id'];?>"><?echo $l['first_name'].' '.$l['surname'];?></option>
                                                <?}?>
                                            </select>
                                            <span class="material-input"></span>
                                        </div>
                                     </td>
                                     <td>
                                       <span id="how_many_1">0</span>
                                     </td>
                                     <td></td>
                                   </tr>
                                   <div class="row_append">

                                   </div>

                                 </table>
                                 </form>
                                 <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_assign_course">Save</button>
                                 <button type="button" class="btn btn-sm btn-raised btn-info pull-right" id="add_row">Add</button>
                                 <div class="clearfix">

                                 </div>


                               </div>
                           </div><!--- END OF PAGE CONTENT -->
                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>admin/accounts/app_scripts/index.js"></script>
    <script src="<? echo $fullurl; ?>assets/js/chosen.jquery.js"></script>

    <script>

    var row_count = 2;

    jQuery(document).ready(function($) {
      $('.chosen_select').chosen({search_contains: true, enable_split_word_search: true});
    });


    $( "body" ).on( "change", ".learner_select", function() {
      var id = $(this).data('id');
      var count = 0;

      $('#learner_select_'+id+' option:selected').each(function(){
          count++;
        });

        $('#how_many_'+id+'').html(count);
    });

    $( "body" ).on( "click", "#add_row", function() {
      var row = '      <tr class="row_'+row_count+'"> <td> <div class="form-group is-empty" style="margin-top:0px;"><label class="control-label" for="course_select_'+row_count+'">Course Select</label> <select class="form-control chosen_select" id="course_select_'+row_count+'" name="course_select_'+row_count+'" required> <option value="" selected disabled>Select</option> <?foreach ($courses as $c) {?><option value="<?echo $c['id'];?>"><?echo $c['title'];?></option><?}?> </select> <span class="material-input"></span> </div> </td> <td> <div class="form-group is-empty" style="margin-top:0px;"><label class="control-label" for="learner_select_'+row_count+'">Learner/s Select</label><br /> <select class="form-control chosen_select learner_select" id="learner_select_'+row_count+'" name="learner_select_'+row_count+'[]" data-id="'+row_count+'" required multiple> <?foreach ($learners as $l) {?> <option value="<?echo $l['id'];?>"><?echo $l['first_name'].' '.$l['surname'];?></option> <?}?> </select> <span class="material-input"></span> </div> </td> <td> <span id="how_many_'+row_count+'">0</span> </td> <td><button type="button" class="btn btn-sm btn-raised btn-warning" id="remove_row" data-id="'+row_count+'">Remove</button></td> </tr>';

      $('#course_table tr:last').after(row);

      $('#counter').val(row_count);

      row_count++;
      $('.chosen_select').chosen({search_contains: true, enable_split_word_search: true});
    });

    $( "body" ).on( "click", "#remove_row", function() {
      var id = $(this).data('id');
      $('.row_'+id+'').remove();
    });

    $( "body" ).on( "click", "#save_assign_course", function() {
      var form = $('#assign_course_form').serialize();
            $.ajax({
                      type: "POST",
                      url: "../../../admin/accounts/app_ajax/save_add_course.php",
                      data: form,
                      success: function(msg){
                          if(msg == 'ok' || msg == ' ok'){
                              Messenger().post({
                                message: 'Assigned to Course',
                                showCloseButton: false
                          });

                          setTimeout(function(){
                              window.location.reload();
                              }, 2000);


                          }else{
                              Messenger().post({
                                message: 'Please try again later',
                                type: 'error',
                                showCloseButton: false
                            });
                          }
                      }
                  });
    });

    </script>
</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
