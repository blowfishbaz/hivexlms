$( document ).ready(function() {


var $table = $('#table');
    var $sixty = $( document ).height();
     $sixty = ($sixty/100)*70;
      $table.bootstrapTable( 'resetView' , {height: $sixty} );


$(function () {
     $table.on('click-row.bs.table', function (e, row, $element) {
         $('.success').removeClass('success');
         $($element).addClass('success');
     });
});
});

function 	LinkCompanyFormatter(value, row, index) {
    return '<a href="account_view.php?id='+row.id+'" class="text-capitalize">'+value+'</a>';
}



$( "body" ).on( "click", "#add_new_company", function() {

    console.log('{ressed}');

    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');
    $.ajax({
       type: "POST",
       url: "../../../admin/accounts/app_forms/new_company.php",
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});

$( "body" ).on( "click", "#save_new_comapny", function() {
    var form = $('#new_company_form').serialize();

    var HasError = 0;

    $('#new_company_form').find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
              }
            }
        });
        if (HasError == 1) {
        Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
        });
        } else {
          $.ajax({
                    type: "POST",
                    url: "../../../admin/accounts/app_ajax/save_new_company.php",
                    data: form,
                    success: function(msg){
                        if(msg == 'ok' || msg == ' ok'){
                            Messenger().post({
                              message: 'New Company Created',
                              showCloseButton: false
                        });

                        setTimeout(function(){
                            window.location.reload();
                            }, 2000);


                        }else{
                            Messenger().post({
                              message: 'Please try again later',
                              type: 'error',
                              showCloseButton: false
                          });
                        }
                    }
                });
        }
});



$( "body" ).on( "click", "#add_new_learner", function() {



    $("#BaseModalLContent").html('<div id="FRMloader"><img src="../../../assets/images/loader.gif" alt="loader" width="64" height="51" /><br>Loading Form</div>');
    $("#BaseModalL").modal('show');
    $.ajax({
       type: "POST",
       url: "../../../admin/accounts/app_forms/new_learner.php",
       success: function(msg){
          //alert(msg);
          $("#BaseModalLContent").delay(1000)
             .queue(function(n) {
                $(this).html(msg);
                n();
             }).fadeIn("slow").queue(function(n) {
                $.material.init();
                n();
          });
       }
    });
});


$( "body" ).on( "click", "#save_new_learner", function() {
    var form = $('#new_learner_form').serialize();

    var HasError = 0;

    $('#new_learner_form').find('input').each(function(){
      $(this).parent().removeClass('has-error');
      Messenger().hideAll();
      if(!$.trim(this.value).length) { // zero-length string AFTER a trim
              if(!$(this).prop('required')){
              } else {
                      HasError = 1;
                      $(this).parent().addClass('has-error');
              }
            }
        });
        if (HasError == 1) {
        Messenger().post({
                message: 'Please make sure all required elements of the form are filled out.',
                type: 'error',
                showCloseButton: false
        });
        } else {
          $.ajax({
                    type: "POST",
                    url: "../../../admin/accounts/app_ajax/save_new_learner.php",
                    data: form,
                    success: function(msg){
                        if(msg == 'ok' || msg == ' ok'){
                            Messenger().post({
                              message: 'New Learner Created',
                              showCloseButton: false
                        });

                        setTimeout(function(){
                            window.location.reload();
                            }, 2000);


                        }else{
                            Messenger().post({
                              message: 'Please try again later',
                              type: 'error',
                              showCloseButton: false
                          });
                        }
                    }
                });
        }
});
