<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 //Page Title
 $page_title = 'Users';

 if($_SESSION['SESS_ACCOUNT_Type']!='admin'){
     header('Location: '.$fullurl.'admin/dashboard.php');
 }



 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HiveX Business Management System<? if ($page_title != null) { echo ' - '.$page_title; } ?></title>

<?	//Base CSS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/hivex_base_css.php'); ?>

<!-- On Page CSS -->

<style>
.panel.panel--styled {
    background: #F4F2F3;
}
.panelTop {
    padding: 30px;
}

.panelBottom {
    border-top: 1px solid #e7e7e7;
    padding-top: 20px;
}
.btn-add-to-cart {
    background: #FD5A5B;
    color: #fff;
}
.btn.btn-add-to-cart.focus, .btn.btn-add-to-cart:focus, .btn.btn-add-to-cart:hover  {
	color: #fff;
    background: #FD7172;
	outline: none;
}
.btn-add-to-cart:active {
	background: #F9494B;
	outline: none;
}


span.itemPrice {
    font-size: 24px;
    color: #FA5B58;
}
.img-responsive{
  min-width: 350px;
  min-height: 350px;
   object-fit: cover;
}

.filetypes:hover{
  color:#b9dcfb;
}

.left_pan{
border-right: 1px #e3e3e3 solid;
}

.c_div > h2{
  text-transform: capitalize;
}

.edit_bar{
  background-color: #e6e6e6;
  margin: 25px 0px;
  padding: 6px;
}

</style>




</head>
<body>
    <div id="wrapper">
		<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/sidemenu.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenuhivex.php'); ?></div>



                        <div class="page_title text-capitalize">
                            <span class="menuglyph glyphicons glyphicons-note" aria-hidden="true"></span>
                            <span class="menuText">View Course</span>
                            <!-- <button type="button" class="btn btn-primary btn-raised pull-right" id="add_new_course">Edit</button> -->
                        </div>

                        <div class="clearfix"></div>

                        <div class="panel panel-default">
                          <div class="panel-body">

                                            <div class="col-md-12 panelTop">
                                                  <div class="col-md-4 left_pan">

                                                        <img class="img-responsive" src="../../uploads/courses/<? echo $f['link']; ?>" alt=""/>
                                                        <div class="row edit_bar">
                                                              <button type="button" class="btn btn-xs btn-info pull-right edit_resource_but" style="padding:4px; margin:0px;">
                                                                    <span class="glyphicons glyphicons-edit"></span>
                                                              </button>
                                                        </div>
                                                        <?
                                                        $db->query("select * from lms_upload where cid = ? and type = ?");
                                                        $db->bind(1,$_GET["id"]);
                                                        $db->bind(2,"resource");
                                                        $ups = $db->resultset();

                                                        foreach ($ups as $key => $v) {
                                                        $db->query("select * from lms_resource where rid = ? ");
                                                        $db->bind(1,$v["id"]);
                                                        $r = $db->single();
                                                        $f_t = mime_content_type("../../uploads/courses/".$v['link']);
                                                        $f = getFileIcon(mime_content_type("../../uploads/courses/".$v['link']));

                                                        ?>

                                                        <hr style="border-top: 1px solid #e7e7e7;}" />
                                                        <div class="media">
                                                              <div class="media-left media-middle">
                                                                    <? if (isset($f_t) && in_array($f_t, array("image/png", "image/jpeg", "image/gif"))) { ?>
                                                                    <img alt="64x64" class="media-object" src="../../uploads/courses/<? echo $v['link']; ?>"  style="width: 64px; height: 64px;">
                                                                    <? } else { ?>
                                                                    <a class="ficon" target="_blank" href="../../uploads/courses/<? echo $v['link']; ?>">
                                                                          <span class="<? echo $f; ?>" aria-hidden="true" style="float:left; font-size:64px;"></span>
                                                                    </a>
                                                                    <? } ?>
                                                              </div>
                                                              <div class="media-body">
                                                                    <h4 class="media-heading"><? echo $r['rtitle']; ?></h4>
                                                                    <p><? echo $r['rdesc']; ?></p>
                                                              </div>
                                                              <div class="col-lg-12 edit_bar">


                                                                <div class="col-lg-2" style=" margin-top: 3px;">
                                                                  <? if($c['online']==1){ ?>
                                                                          <? if($r['online']==1){ ?>
                                                                                <span class="label label-success">
                                                                                <span class="glyphicons glyphicons-ok">
                                                                                </span>Online</span>
                                                                          <? }else { ?>
                                                                                <span class="label label-danger">
                                                                                <span class="glyphicons glyphicons-remove">
                                                                                </span>Online</span>
                                                                          <? } ?>
                                                                  <? } ?>



                                                                </div>
                                                                <div class="col-lg-3" style=" margin-top: 3px;">
                                                                  <? if($c['vc']==1){ ?>
                                                                        <? if($r['vc']==1){ ?>
                                                                              <span class="label label-success">
                                                                              <span class="glyphicons glyphicons-ok">
                                                                              </span>Virtual Classroom</span>
                                                                        <? }else { ?>
                                                                              <span class="label label-danger">
                                                                              <span class="glyphicons glyphicons-remove">
                                                                              </span>Virtual Classroom</span>
                                                                        <? } ?>
                                                                  <? } ?>                    </div>
                                                                <div class="col-lg-3" style=" margin-top: 3px;">
                                                                  <? if($c['f2f']==1){ ?>
                                                                        <? if($r['f2f']==1){ ?>
                                                                              <span class="label label-success">
                                                                              <span class="glyphicons glyphicons-ok">
                                                                              </span>Face 2 Face</span>
                                                                        <? }else { ?>
                                                                              <span class="label label-danger">
                                                                              <span class="glyphicons glyphicons-remove">
                                                                              </span>Face 2 Face</span>
                                                                         <? } ?>
                                                                  <? } ?>

                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <button type="button" class="btn btn-xs btn-info pull-right edit_resource_but " data-id="<? echo $_GET['id']; ?>" style="padding:4px; margin:0px;">
                                                                          <span class="glyphicons glyphicons-edit"></span>
                                                                    </button>
                                                                </div>
                                                              </div>

                                                              <div class="col-lg-12">

                                                              </div>
                                                        </div>
                                                        <?
                                                        }
                                                        ?>


                                                        <div class="col-lg-12">
                                                              <button type="button" class="btn btn-xs btn-info pull-right" data-id="<? echo $_GET['id']; ?>" style="padding:4px; margin:0px;">
                                                                    ADD NEW RESOURCE
                                                              </button>
                                                        </div>

                                                  </div>
                                                  <div class="col-md-8 c_div">


                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="form-group" style="margin-top:0px;">
                                                            <p style="margin-bottom:15px;border-bottom: 1px solid #d0c8c8;padding-bottom: 9px;">
                                                              <strong style="margin-right:10px; font-size:18px;">Course Availability: </strong>
                                                              <? if($c['online']==1){ $_on = 'checked'; $_on_val = 'true';}else{ $_on = ''; $_on_val = 'false'; } ?>
                                                                <label class="checkbox-inline">
                                                                    <input class="c_avail c_online_inx" type="checkbox" disabled value="<? echo $_on_val; ?>" name="online" <? echo $_on; ?>> Online
                                                                </label>
                                                                <? if($c['vc']==1){ $_vc = 'checked'; $_vc_val = 'true';}else{ $_vc = ''; $_vc_val = 'false';} ?>
                                                                <label class="checkbox-inline">
                                                                    <input class="c_avail c_vc_inx" type="checkbox" disabled value="<? echo $_vc_val; ?>" name="vc" <? echo $_vc; ?>> Virtual Classroom
                                                                </label>
                                                                <? if($c['f2f']==1){ $_f2f = 'checked'; $_f2f_val = 'true';}else{ $_f2f = ''; $_f2f_val = 'false'; } ?>
                                                                <label class="checkbox-inline">
                                                                    <input class="c_avail c_f2f_inx" type="checkbox" disabled value="<? echo $_f2f_val; ?>" name="f2f" <? echo $_f2f; ?>> Face to Face
                                                                </label>
                                                                <button type="button" class="btn btn-xs btn-info pull-right edit_course_but" data-id="<? echo $_GET['id']; ?>" style="padding:4px; margin:0px;">
                                                                      <span class="glyphicons glyphicons-edit"></span>
                                                                </button>
                                                            </p>
                                                        </div>

                                                    </div>




                                                  <h2><? echo ucwords($c['title']); ?></h2>
                                                        <? echo $c['description']; ?>
                                                        <div class="col-lg-12 edit_bar">
                                                              <button type="button" class="btn btn-xs btn-info pull-right edit_course_but" data-id="<? echo $_GET['id']; ?>" style="padding:4px; margin:0px;">
                                                                    <span class="glyphicons glyphicons-edit"></span>
                                                              </button>
                                                        </div>
                                                        <? if($c['online']=="1"){ ?>
                                                        <hr style="border-top: 1px solid #e7e7e7;}" />
                                                        <h2 style="font-size: 1.5em; color: rgb(0, 0, 0); font-family: verdana, arial, helvetica, sans-serif;">
                                                              <span style="font-size: 18px;">
                                                                      <b>Online Only Content</b>
                                                              </span>
                                                        </h2>
                                                        <? echo $c['online_description']; ?>
                                                        <div class="col-lg-12 edit_bar">
                                                              <button type="button" class="btn btn-xs btn-info pull-right" style="padding:4px; margin:0px;">
                                                                    <span class="glyphicons glyphicons-edit"></span>
                                                              </button>
                                                        </div>
                                                        <? } ?>
                                                        <? if($c['vc']=="1"){ ?>
                                                        <hr style="border-top: 1px solid #e7e7e7;}" />
                                                        <h2 style="font-size: 1.5em; color: rgb(0, 0, 0); font-family: verdana, arial, helvetica, sans-serif;">
                                                              <span style="font-size: 18px;">
                                                                    <b>Virtual Classroom Only Content</b>
                                                              </span>
                                                        </h2>
                                                        <? echo $c['vc_description']; ?>
                                                        <div class="col-lg-12 edit_bar">
                                                              <button type="button" class="btn btn-xs btn-info pull-right" style="padding:4px; margin:0px;">
                                                                    <span class="glyphicons glyphicons-edit"></span>
                                                              </button>
                                                        </div>
                                                        <? } ?>
                                                        <? if($c['f2f']=="1"){ ?>
                                                        <hr style="border-top: 1px solid #e7e7e7;}" />
                                                        <h2 style="font-size: 1.5em; color: rgb(0, 0, 0); font-family: verdana, arial, helvetica, sans-serif;">
                                                              <span style="font-size: 18px;">
                                                                    <b>Face 2 Face Only Content</b>
                                                              </span>
                                                        </h2>
                                                        <? echo $c['f2f_description']; ?>
                                                        <div class="col-lg-12 edit_bar">
                                                              <button type="button" class="btn btn-xs btn-info pull-right" data-id="<? echo $_GET['id']; ?>" style="padding:4px; margin:0px;">
                                                                    <span class="glyphicons glyphicons-edit"></span>
                                                              </button>
                                                        </div>
                                                        <? } ?>
                                                  </div>
                                            </div>

                          						<div class="col-md-12 panelBottom">
                          							<!-- <div class="col-md-4 text-center">
                          								<button class="btn btn-lg btn-add-to-cart"><span class="glyphicon glyphicon-shopping-cart"></span>   Add to Cart</button>
                          							</div>
                          							<div class="col-md-4 text-left">
                          								<h5>Price <span class="itemPrice">$24.99</span></h5>
                          							</div>
                          							<div class="col-md-4">
                          								<div class="stars">
                          								 <div id="stars" class="starrr"></div>
                          								</div>
                          							</div> -->
                          						</div>
                          					</div>

                          </div>
                        </div><!--- END OF PAGE CONTENT -->


                </div>
            </div>
        </div><!-- /#page-content-wrapper -->

    </div><!-- /#wrapper -->
    <?
	//JS Include
	include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php'); ?>

  <script src="<? echo $fullurl; ?>assets/js/table.js"></script>




<script>


$(document).ready(function(){

      $( "body" ).on( "click", ".edit_resource_but", function(e) {
       $('#BaseModalL').modal('show');
      });


      $( "body" ).on( "click", ".edit_course_but", function(e) {

              $('#BaseModalL').modal('show');
              $("#BaseModalLContent").html("");
              $.ajax({
                    type: "POST",
                    url: "../../assets/app_ajax/courses/get_main_course_types.php?id=<? echo $_GET['id']; ?>",
                    data: {id:"<? echo $_GET['id']; ?>"},
                    success: function(msg){
                      $("#BaseModalLContent").delay(1000)
                       .queue(function(n) {
                           $(this).html(msg);
                           n();
                       }).fadeIn("slow").queue(function(n) {
                                    $.material.init();
                                   n();
                       });
                    }
              });


      });

});



$( "body" ).on( "change", ".c_online_in", function(e) {

        if($('.c_vc_in').is(":checked")|| $('.c_f2f_in').is(":checked")){
                  if($('.c_online_in').is(":checked")){
                        $('.no_removable').hide();
                        $('.warning_online').hide();
                        $('.c_vc_in').parent().children(".c_in").val('true');
                        $('.c_vc_in').prop( "disabled", false );
                        $('.c_f2f_in').prop( "disabled", false );
                  }else{
                        $('.c_vc_in').prop( "disabled", false );
                        $('.c_f2f_in').prop( "disabled", false );
                        $('.warning_online').hide();
                        $(this).parent().children(".c_in").val('false');
                  }

        }else{

                  if($('.c_online_in').is(":checked")){
                    $('.no_removable').hide();
                    $('.c_vc_in').parent().children(".c_in").val('true');
                    $('.c_vc_in').prop( "disabled", false );
                    $('.c_f2f_in').prop( "disabled", false );
                  }
                  else{
                    $('.c_vc_in').prop( "disabled", true );
                    $('.c_f2f_in').prop( "disabled", true );
                    $('.no_removable').show();
                  }
        }

});

// $( "body" ).on( "change", ".c_online_in", function(e) {
//
//         if($('.c_vc_in').is(":checked")|| $('.c_f2f_in').is(":checked")){
//                   if($('.c_online_in').is(":checked")){
//                         $('.no_removable').hide();
//                         $('.warning_online').hide();
//                         $('.c_vc_in').parent().children(".c_in").val('true');
//                         $('.c_vc_in').prop( "disabled", false );
//                         $('.c_f2f_in').prop( "disabled", false );
//                   }else{
//                         $('.c_vc_in').prop( "disabled", false );
//                         $('.c_f2f_in').prop( "disabled", false );
//                         $('.warning_online').hide();
//                         $(this).parent().children(".c_in").val('false');
//                   }
//
//         }else{
//                   if($('.c_online_in').is(":checked")){
//                     $('.no_removable').hide();
//                     $('.c_vc_in').parent().children(".c_in").val('true');
//                     $('.c_vc_in').prop( "disabled", false );
//                     $('.c_f2f_in').prop( "disabled", false );
//                   }
//                   else{
//                     $('.c_vc_in').prop( "disabled", true );
//                     $('.c_f2f_in').prop( "disabled", true );
//                     $('.no_removable').show();
//                   }
//         }
//
// });
//
//
// $( "body" ).on( "change", ".c_online_in", function(e) {
//
//         if($('.c_vc_in').is(":checked")|| $('.c_f2f_in').is(":checked")){
//                   if($('.c_online_in').is(":checked")){
//                         $('.no_removable').hide();
//                         $('.warning_online').hide();
//                         $('.c_vc_in').parent().children(".c_in").val('true');
//                         $('.c_vc_in').prop( "disabled", false );
//                         $('.c_f2f_in').prop( "disabled", false );
//                   }else{
//                         $('.c_vc_in').prop( "disabled", false );
//                         $('.c_f2f_in').prop( "disabled", false );
//                         $('.warning_online').hide();
//                         $(this).parent().children(".c_in").val('false');
//                   }
//
//         }else{
//                   if($('.c_online_in').is(":checked")){
//                     $('.no_removable').hide();
//                     $('.c_vc_in').parent().children(".c_in").val('true');
//                     $('.c_vc_in').prop( "disabled", false );
//                     $('.c_f2f_in').prop( "disabled", false );
//                   }
//                   else{
//                     $('.c_vc_in').prop( "disabled", true );
//                     $('.c_f2f_in').prop( "disabled", true );
//                     $('.no_removable').show();
//                   }
//         }
//
// });




$( "body" ).on( "click", "#add_new_course_cancel", function(e) {
      $('.warning_online').hide();
      $('.c_online_in').parent().children(".c_in").val('true');
      $( ".c_online_in" ).prop( "checked", true );
});



$( "body" ).on( "click", "#add_new_course_online_continue", function(e) {


      $.ajax({
            type: "POST",
            url: "../../assets/app_ajax/courses/remove_course_type.php?id=<? echo $_GET['id']; ?>&type=online",
            data: {id:"<? echo $_GET['id']; ?>"},
            success: function(msg){

                       $('#BaseModalL').modal('hide');
                       Messenger().post({
                               message: 'Online Option Removed',
                               showCloseButton: false
                       });
                       setTimeout(function(){
                         location.reload();
                       },600);


            }
      });



});

</script>


</body>
</html>
<!-- PAGE JS -->
<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
