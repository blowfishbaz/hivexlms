<?php

/**
*
*
*
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BMS System
*
*
*
* @Filename    customers/index.php
* @author     Graham Palfreyman
* @copyright  1997-2015 Blowfish Technology Ltd
* @version    1
* @Date        23/05/2016
*/


include($_SERVER['DOCUMENT_ROOT'].'/application.php');
use PHPMailer\PHPMailer\PHPMailer;
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/class.phpmailer.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/Exception.php');
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/SMTP.php');

$count = 1;


$db->query("select * from ws_rota_bid where email_sent = ? and status = ?");
$db->bind(1,'0');
$db->bind(2,'1');
$data = $db->resultset();

$user_array = array();

foreach ($data as $d) {

  $db->query("select * from ws_rota_bid_request where pid = ? and status = ?");
  $db->bind(1,$d['id']);
  $db->bind(2,'1');
  $request = $db->resultset();

  foreach ($request as $req) {

    if(!empty($req['account_id'])){
      array_push($user_array, $req['account_id']);
    }

  }

}

$user_array = array_unique($user_array);
$user_array = array_values($user_array);

//print_r($user_array);

foreach ($user_array as $key => $ua) {
  $db->query("select *,ws_rota_bid.id, ws_rota_bid_request.id as request_id
              from ws_rota_bid_request
              left join ws_rota_bid
              on ws_rota_bid.id = ws_rota_bid_request.pid
              where ws_rota_bid_request.account_id = ? and ws_rota_bid_request.status = 1 and ws_rota_bid.status = 1 and ws_rota_bid.email_sent = 0");
  $db->bind(1,$ua);
  $user_shifts = $db->resultset();


  if(!empty($user_shifts)){
    $db->query("select * from accounts where id = ?");
    $db->bind(1,$ua);
    $user = $db->single();

    $subject='Rota Session Bid';

    $head='';
    $body1='<!-- BODY --><body class="half-padding" style="margin: 0px; padding: 0px; background-color: #dedede;margin-top: -18px;"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!--<![endif]--><div class="wrapper" ><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div class="image" style="font-size: 12px;font-style: normal;font-weight: 400; background-color:#dedede;" align="center"> </div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;text-align:center;padding: 5px;background-color: #194d75;color: white;"><h1>Rota Session Bid!</h1></div></div><div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;">';

    $body2='Hi '.ucfirst ($user['name']).', <br />A shift has been published. Please see below and respond to the shift invite.</p>';
    $body3='
    <table border="1" style="width:100%;">
    <tr>
      <th>Service</th>
      <th>Location</th>
      <th>Role</th>
      <th>Date / Time</th>
      <th>View</th>
    </tr>';
    foreach ($user_shifts as $us) {
      $db->query("select ws_service_roles.*, ws_roles.title as role_title, ws_service.title as service_title, ws_service.location
                  from ws_service_roles
                  left join ws_roles on ws_roles.id = ws_service_roles.job_role
                  left join ws_service on ws_service.id = ws_service_roles.pid
                  where ws_service_roles.id = ?");
      $db->bind(1,$us['service_role_id']);
      $info = $db->single();

      if(!empty($info)){
          $body3 .= '<tr>
            <td>'.$info['service_title'].'</td>
            <td>'.$info['location'].'</td>
            <td>'.$info['role_title'].'</td>
            <td>'.date('d-m-Y',$us['start_final']).' '.$us['start_time'].'</td>
            <td><a href="'.$fullurl.'admin/rota/shift_response.php?ref='.encrypt($us['request_id'].'-'.$user['id']).'">View Session</a></td>
          </tr>';
      }
    }

    $body3 .=' </table>';

    $footer='<!-- Callout Panel --></div><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:5px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"> </div><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:5px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;"><div class="divider" style="display: block;font-size: 2px;line-height: 2px;width: 40px;background-color: #c9c2a8;Margin-left: 260px;Margin-right: 260px;">&nbsp;</div></div></td></tr></tbody></table><table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f5f4f4; border-top: 2px solid #dedede;" align="center" emb-background-style=""><tbody><tr><td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;"><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="Margin-left: 20px;Margin-right: 20px;"><p class="size-16" style="Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;"></p><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><div style="width:560px; height:2px; background-color:#dedede;"></div><div style="Margin-left: 20px;Margin-right: 20px;"><div style="line-height:20px;font-size:1px">&nbsp;</div></div><p class="size-16" style="Margin-bottom: 20px;font-size: 10px;line-height: 24px;text-align: center;">Copyright © *|'.date("Y").'|* *|DDAF Services Ltd|*, All rights reserved.</p><!-- Callout Panel --></div></td></tr></tbody></table><table class="footer" style="border-collapse: collapse;table-layout: fixed;Margin-right: auto;Margin-left: auto;border-spacing: 0;width: 560px;" align="center"><tbody><tr><td style="padding: 0 0 40px 0;"><table class="footer__right" style="border-collapse: collapse;table-layout: auto;border-spacing: 0;" align="right"><tbody><tr><td class="footer__inner" style="padding: 0;"> </td></tr></tbody></table><table class="footer__left" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;color: #b3b3b3;font-family: &quot;PT Sans&quot;,&quot;Trebuchet MS&quot;,sans-serif;width: 380px;"><tbody><tr><td class="footer__inner" style="padding: 0;font-size: 12px;line-height: 19px;"><div> </div><div class="footer__permission" style="Margin-top: 18px;"> </div></td></tr></tbody></table></td></tr></tbody></table></div></body>';

    $htmlContent = $head.$body1.$body2.$body3.$footer;

    if($count == 1){
      //echo $htmlContent;

    }

    $count++;

    $mail = new PHPMailer();
    $mail->SMTPDebug = 0;
    $mail->isSMTP();
    $mail->Host = 'smtp.office365.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'rota@staff-box.co.uk'; //paste one generated by Mailtrap
    $mail->Password = 'Lux03898'; //paste one generated by Mailtrap
    $mail->Port = 587;
    $mail->setFrom('rota@staff-box.co.uk', 'Rota');
    $mail->addAddress($user['email'], $user['name']);
    //$mail->addBCC('josh@blowfishtechnology.com', 'Josh McCarthy');
    $mail->Subject = $subject;
    $mail->isHTML(true);
    $mail->Body = $htmlContent;


    // if($mail->send()){
    //     // echo 'Message has been sent';
    // }else{
    //     // echo 'Message could not be sent.';
    //     // echo 'Mailer Error: ' . $mail->ErrorInfo;
    // }

  }

}

$db->Query("update ws_rota_bid set email_sent = 1 where email_sent = ?");
$db->bind(1,0);
$db->execute();
