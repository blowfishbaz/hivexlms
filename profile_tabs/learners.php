<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $id = $_GET['id'];

 $db->query("select * from ws_provision where company_id = ? and status = 1");
 $db->bind(1,$id);
 //$data = $db->single();

 function get_my_company($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['company_id'];
 }

 function get_my_account_type($id){
     $db = new database;
     $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
     $db->bind(1,$id);
     $me = $db->single();

     return $me['account_type'];
 }

 function get_my_provision($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['main_provision'];
 }

$company_id = get_my_company(decrypt($_SESSION['SESS_ACCOUNT_ID']));
$provision_id = '';


if(get_my_account_type($id) == 'Manager'){
   $provision_id = get_my_provision($id);
}



?>



    <div class="panel panel-default">
      <div class="panel-body">

         <!-- <h4 class="section_header">Provisions</h4> -->
         <button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="add_learner">Add</button>
         <div class="clearfix">

         </div>

         <table id="address_table" class="striped"
data-toggle="table"
data-show-export="true"
data-click-to-select="true"
data-filter-control="true"
data-show-columns="true"
data-show-refresh="true"

data-url="../../../profile_tabs/app_ajax/learners_tab.php?id=<?echo $company_id;?><?if(!empty($provision_id)){echo '&provision='.$provision_id;}?>"
data-height="400"
data-side-pagination="server"
data-pagination="false"
data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
data-search="true"
data-page-size="100"
>
    <thead>
         <tr>

             <th data-field="first_name" data-sortable="false" data-visible="true" data-formatter="LinkEditCompanyLearner">Name</th>
             <th data-field="surname" data-sortable="false" data-visible="false">Surname</th>
             <th data-field="email" data-sortable="false" data-visible="true">Email</th>
             <th data-field="provision_name" data-sortable="false" data-visible="true">Provision</th>
             <th data-field="account_type" data-sortable="false" data-visible="true">Account Type</th>

         </tr>
    </thead>
</table>


<script src="<? echo $fullurl ?>assets/js/bootstrap-table.js"></script>
<script src="<? echo $fullurl ?>assets/js/table-extensions/export/bootstrap-table-export.js"></script>

      </div>
    </div>
