<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 $id = $_GET['id'];

 $db->query("select * from ws_provision where id = ? ");
 $db->bind(1,$id);
 $data = $db->single();

?>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

</style>
<h3>View Provision</h3>

<table>
  <tr>
    <th>Provision Name</th>
    <td><?echo $data['provision_name'];?></td>
  </tr>
  <tr>
    <th>Main Telephone</th>
    <td><?echo $data['main_telephone'];?></td>
  </tr>
  <tr>
    <th>Address 1</th>
    <td><?echo $data['address1'];?></td>
  </tr>
  <tr>
    <th>Address 2</th>
    <td><?echo $data['address2'];?></td>
  </tr>
  <tr>
    <th>Address 3</th>
    <td><?echo $data['address3'];?></td>
  </tr>
  <tr>
    <th>City</th>
    <td><?echo $data['city'];?></td>
  </tr>
  <tr>
    <th>County</th>
    <td><?echo $data['county'];?></td>
  </tr>
  <tr>
    <th>Country</th>
    <td><?echo $data['country'];?></td>
  </tr>
  <tr>
    <th>Postcode</th>
    <td><?echo $data['postcode'];?></td>
  </tr>
</table>

<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" style="margin-top:10px;" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" style="margin-top:10px;" id="edit_provision" data-id="<?echo $id;?>">Edit</button>
