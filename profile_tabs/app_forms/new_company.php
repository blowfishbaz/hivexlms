<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

?>

<h3>New Company</h3>

<form id="new_company_form">

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="company_name" class="control-label">Company Name*</label>
          <input type="text" class="form-control" id="company_name" name="company_name" required="yes" value="">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="main_email" class="control-label">Main Email</label>
          <input type="text" class="form-control" id="main_email" name="main_email" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="main_telephone" class="control-label">Main Telephone*</label>
          <input type="number" class="form-control" id="main_telephone" name="main_telephone" required="yes" value="">
          <span class="material-input"></span>
      </div>
  </div>
<div class="clearfix"></div>
  <hr />
<div class="clearfix"></div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="address1" class="control-label">Address Line 1*</label>
          <input type="text" class="form-control" id="address1" name="address1" required="yes" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="address2" class="control-label">Address Line 2</label>
          <input type="text" class="form-control" id="address2" name="address2" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="address3" class="control-label">Address Line 3</label>
          <input type="text" class="form-control" id="address3" name="address3" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="city" class="control-label">City</label>
          <input type="text" class="form-control" id="city" name="city" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="county" class="control-label">County</label>
          <input type="text" class="form-control" id="county" name="county" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="country" class="control-label">Country</label>
          <input type="text" class="form-control" id="country" name="country" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating is-empty">
          <label for="postcode" class="control-label">Postcode*</label>
          <input type="text" class="form-control" id="postcode" name="postcode" required="yes" value="" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>

</form>



<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_new_comapny">Save</button>
