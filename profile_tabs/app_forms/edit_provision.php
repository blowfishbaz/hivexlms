<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 $id = $_GET['id'];

 $db->query("select * from ws_provision where id = ? ");
 $db->bind(1,$id);
 $data = $db->single();

?>

<h3>Edit Provision</h3>

<form id="edit_provision_form">
  <input type="hidden" name="provision_id" value="<?echo $id;?>" />

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="prov_name" class="control-label">Provision Name*</label>
          <input type="text" class="form-control" id="prov_name" name="prov_name" required="yes" value="<?echo $data['provision_name'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="main_telephone" class="control-label">Main Telephone</label>
          <input type="number" class="form-control" id="main_telephone" name="main_telephone" value="<?echo $data['main_telephone'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="address1" class="control-label">Address Line 1*</label>
          <input type="text" class="form-control" id="address1" name="address1" required="yes" value="<?echo $data['address1'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="address2" class="control-label">Address Line 2</label>
          <input type="text" class="form-control" id="address2" name="address2" value="<?echo $data['address2'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="address3" class="control-label">Address Line 3</label>
          <input type="text" class="form-control" id="address3" name="address3" value="<?echo $data['address3'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="city" class="control-label">City</label>
          <input type="text" class="form-control" id="city" name="city" value="<?echo $data['city'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="county" class="control-label">County</label>
          <input type="text" class="form-control" id="county" name="county" value="<?echo $data['county'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="country" class="control-label">Country</label>
          <input type="text" class="form-control" id="country" name="country" value="<?echo $data['country'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="postcode" class="control-label">Postcode*</label>
          <input type="text" class="form-control" id="postcode" name="postcode" required="yes" value="<?echo $data['postcode'];?>" maxlength="200">
          <span class="material-input"></span>
      </div>
  </div>



</form>

<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="edit_provision_save" data-id="<?echo $id;?>">Edit</button>
