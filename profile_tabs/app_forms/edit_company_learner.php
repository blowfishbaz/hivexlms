<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 $id = $_GET['id'];



 //1. ws_accounts
 $db->query("select * from ws_accounts where id = ?");
 $db->bind(1,$_GET['id']);
 $account = $db->single();

 //2. ws_accounts_extra_info
 $db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
 $db->bind(1,$_GET['id']);
 $account_info = $db->single();

 //3. ws_provision
 $db->query("select * from ws_provision where company_id = ? and status = ?");
 $db->bind(1,$account_info['company_id']);
 $db->bind(2,'1');
 $provisions = $db->resultset();

 // print_r($account);
 // print_r($account_info);
 // print_r($provisions);

 function get_my_account_type($id){
     $db = new database;
     $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
     $db->bind(1,$id);
     $me = $db->single();

     return $me['account_type'];
 }

 function get_my_provision($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['main_provision'];
 }

 $my_account_type = get_my_account_type(decrypt($_SESSION['SESS_ACCOUNT_ID']));

?>

<h3>Edit Learner</h3>

<form id="edit_company_learner_form">
  <input type="hidden" name="user_id" value="<?echo $id;?>" />
  <input type="hidden" name="company_id" value="<?echo $account_info['company_id'];?>" />
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="firstname" class="control-label">First Name*</label>
          <input type="text" class="form-control" id="firstname" name="firstname" required="yes" value="<?echo $account['first_name'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="surname" class="control-label">Surname*</label>
          <input type="text" class="form-control" id="surname" name="surname" required="yes" value="<?echo $account['surname'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="job_title" class="control-label">Job Title</label>
          <input type="text" class="form-control" id="job_title" name="job_title"value="<?echo $account_info['job_title'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="email" class="control-label">Email*</label>
          <input type="text" class="form-control" id="email" name="email" required="yes" value="<?echo $account['email'];?>">
          <span class="material-input"></span>
      </div>
  </div>

  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="mobile_number" class="control-label">Mobile Number</label>
          <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="<?echo $account_info['mobile_number'];?>">
          <span class="material-input"></span>
      </div>
  </div>
  <div class="col-md-12 col-sm-12">
      <div class="form-group label-floating ">
          <label for="telephone_number" class="control-label">Telephone Number</label>
          <input type="text" class="form-control" id="telephone_number" name="telephone_number" value="<?echo $account_info['telephone'];?>">
          <span class="material-input"></span>
      </div>
  </div>




  <div class="col-md-12 col-sm-12">
      <div class="form-group">
        <label class="control-label pull-left" for="account_type">Account Type*: </label>
        <select class="form-control chosen-select" id="account_type" name="account_type" required>
          <option value="" selected="" disabled="">Select</option>
          <?if($my_account_type == 'Admin'){?><option <?if('Admin' == $account_info['account_type']){echo 'selected';}?> value="Admin">Admin</option><?}?>
          <?if($my_account_type == 'Admin'){?><option <?if('Manager' == $account_info['account_type']){echo 'selected';}?> value="Manager">Manager</option><?}?>
          <option <?if('Learner' == $account_info['account_type']){echo 'selected';}?> value="Learner">Learner</option>
        </select>
      </div>
  </div>

<?if($my_account_type == 'Admin'){?>
  <div class="col-md-12 col-sm-12">
      <div class="form-group">
        <label class="control-label pull-left" for="provision_select">Provision Select*: </label>
        <select class="form-control chosen-select" id="provision_select" name="provision_select" required>
          <option value="" selected="" disabled="">Select</option>
          <option value="N/A">N/A</option>
          <?foreach ($provisions as $d) {?>
            <option <?if($d['id'] == $account_info['main_provision']){echo 'selected';}?> value="<?echo $d['id'];?>"><?echo $d['provision_name'];?></option>
          <?}?>
        </select>
      </div>
  </div>
  <?}else{?>
    <input type="hidden" name="provision_select" value="<?echo $account_info['main_provision'];?>" />
  <?}?>


</form>



<div class="clearfix"></div>

<button type="button" class="btn btn-sm btn-raised btn-warning" data-dismiss="modal">Close</button>

<button type="button" class="btn btn-sm btn-raised btn-success pull-right" id="save_edit_company_learner">Save</button>
