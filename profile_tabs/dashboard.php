
<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $id = $_GET['id'];

 $db->query("select * from ws_provision where company_id = ? and status = 1");
 $db->bind(1,$id);
 //$data = $db->single();

 function get_my_company($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['company_id'];
 }

 function get_my_account_type($id){
     $db = new database;
     $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
     $db->bind(1,$id);
     $me = $db->single();

     return $me['account_type'];
 }

 function get_my_provision($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['main_provision'];
 }

$company_id = get_my_company(decrypt($_SESSION['SESS_ACCOUNT_ID']));
$provision_id = '';


if(get_my_account_type($id) == 'Manager'){
   $provision_id = get_my_provision($id);
}



?>

<style>

.m_title {
text-transform: capitalize;
}
</style>

    <div class="panel panel-default">
      <div class="panel-body">

         <table id="address_table" class="striped"
data-toggle="table"
data-click-to-select="true"
data-filter-control="true"
data-url="../../../profile_tabs/app_ajax/dashboard.php?id=<?echo $company_id;?><?if(!empty($provision_id)){echo '&provision='.$provision_id;}?>"
data-height="400"
data-side-pagination="server"
data-pagination="false"
data-page-list="[  All, 10, 10, 20,  50, 100,  500]"
data-page-size="100"
>
    <thead>
         <tr>

             <th data-field="first_name" data-sortable="false" data-visible="true" data-formatter="name_combine">Name</th>
             <th data-field="surname" data-sortable="false" data-visible="false">Surname</th>
             <th data-field="provision_name" data-sortable="false" data-visible="true" data-formatter="prov_format">Provision</th>
             <th data-field="title" data-sortable="false" data-visible="true" data-formatter="cap_word">Course</th>
             <th data-field="booked_on" data-sortable="false" data-visible="true" data-formatter="date_time">Booked On Date</th>
             <th data-field="completed_date" data-sortable="false" data-visible="true" data-formatter="date_time">Completed Date</th>
             <th data-field="file_name" data-sortable="false" data-visible="true" data-formatter="cert_download_icon">Certificate</th>
             <th data-field="download_path" data-sortable="false" data-visible="false">Download Path</th>

         </tr>
    </thead>
</table>


<script src="<? echo $fullurl ?>assets/js/bootstrap-table.js"></script>
<script src="<? echo $fullurl ?>assets/js/table-extensions/export/bootstrap-table-export.js"></script>

      </div>
    </div>
<script>

function cert_download_icon(value, row, index) {
    return '<a href="/'+row.download_path+''+value+'" target="_blank"><span class="glyphicons glyphicons-download-alt"></span></a>';
}

function name_combine(value, row, index) {
    return value+" "+row.surname;
}

function date_time(value, row, index) {
   var timestamp = value;
   var date = new Date(timestamp*1000);

   var year = date.getFullYear();
   var month = date.getMonth() + 1;
   var day = date.getDate();
   var hours = date.getHours();
   var minutes = date.getMinutes();
   var seconds = date.getSeconds();

   if(day < 10){
       day = "0"+day;
   }
   if(month < 10){
       month = "0"+month;
   }

   if(hours < 10){
      hours = "0"+hours;
   }

   if(minutes < 10){
      minutes = "0"+minutes;
   }

   var date = day + "-" + month + "-" + year + " " + hours + ":"+minutes;
   if(date == "01-01-1970 00:00"){
     date = '';
   }

   return date;
}

function cap_word(value, row, index){
   return '<span class="m_title">'+value+'</span>';
}

function prov_format(value, row, index){

   if(value == '' || value == null){
      return '<span class="m_title">Admin</span>';
   }else{
      return '<span class="m_title">'+value+'</span>';
   }

}
</script>
