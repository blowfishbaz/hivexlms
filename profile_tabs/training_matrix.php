<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $id = $_GET['id'];

 function get_my_account_type($id){
     $db = new database;
     $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
     $db->bind(1,$id);
     $me = $db->single();

     return $me['account_type'];
 }

 function get_my_company($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['company_id'];
 }

 function get_my_provision($id){
   $db = new database;
   $db->Query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
   $db->bind(1,$id);
   $me = $db->single();

   return $me['main_provision'];
 }

 function get_my_course_progress_tm($account_id, $course_id){
   $db = new database;
   $db->query("select * from ws_booked_on where course_id = ? and account_id = ? and status = 1 order by id asc limit 1");
   $db->bind(1,$course_id);
   $db->bind(2,$account_id);
   $course_check = $db->single();

   $completed_date = $course_check['completed_date'];



    $run_out_date = date('Y-m-d', strtotime('+12 months', $completed_date) );
    $run_out_date_epoch = strtotime($run_out_date);
    $due_date = date('Y-m-d', strtotime('-3 months', strtotime($run_out_date)) );
    $due_date_epoch = strtotime($due_date);





  if($run_out_date_epoch < time()){
      $color = 'table_expired';
  }else if($run_out_date_epoch >= time() && $due_date_epoch <= time()){
      $color = 'table_due';
  }else{
      $color = 'table_completed';
  }




   switch ($course_check['course_status']) {
     case '1':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['booked_on'],'colour'=>'table_assigned');
       break;
     case '2':
     case '3':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['start_date'],'colour'=>'table_assigned');
       break;
     case '4':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['completed_date'],'colour'=>$color);
       break;
     case '5':
       return array('status'=>$course_check['course_status'],'course_date'=>$course_check['course_date'],'colour'=>'table_did_not_attend');
       break;
     default:
       return array('status'=>'Error','course_date'=>'0','colour'=>'');
       break;
   }
 }



 $db->query("select * from lms_course where status != 0");
 $courses = $db->resultset();



  $id = decrypt($_SESSION['SESS_ACCOUNT_ID']);//Admin
  //$id = 'id_user_20210218_143656536000_11611';//Manager


  $my_account_type = get_my_account_type($id);
  $my_company = get_my_company($id);
  $my_provision = get_my_provision($id);

  if($my_account_type == 'Admin'){
    //Get all the learners
    $db->query("select ws_accounts.id as id, first_name, surname, email, account_type
    from ws_accounts
    left join ws_accounts_extra_info
    on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
    where ws_accounts_extra_info.company_id = ? order by first_name asc");
    $db->bind(1,$my_company);
    $learners = $db->ResultSet();
  }else if($my_account_type == 'Manager'){
    $db->query("select ws_accounts.id as id, first_name, surname, email, account_type
    from ws_accounts
    left join ws_accounts_extra_info
    on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
    where ws_accounts_extra_info.main_provision = ? order by first_name asc");
    $db->bind(1,$my_provision);
    $learners = $db->ResultSet();
  }

  //Company ID

?>

<style>
.table_assigned{
  background-color: #98b7c7;
}

.table_completed{
  background-color: #4caf50;
}

.table_did_not_attend{
  background-color: #c4352b;
}

.table_due{
  background-color: #e8a933;
}

.table_expired{
  background-color: #c4352b;
}

.table_header{
  background-color: #b1d1ec;
}

.table_white{
  background-color: white;
}

.ledgend_wrapper{
height: 35px;
width: auto;
float: left;
margin-left: 25px;
}

.ledgend_wrapper:first-of-type {
margin:0px;
}

.ledgend_calendar{
height: 25px;
width: 25px;
position: relative;
float: left;
border-radius: 5px;
margin-right: 25px;
}

.legend_text{
position: relative;
float: left;
margin-top: 3px;
}

</style>

    <div class="panel panel-default">
      <div class="panel-body">


        <div>
  <div class="ledgend_wrapper">
      <div class="ledgend_calendar" style="background-color:#4caf50;">

      </div>
      <div class="legend_text">
          <strong>Completed</strong>
      </div>
  </div>
  <div class="ledgend_wrapper">
      <div class="ledgend_calendar" style="background-color:#e8a933;">

      </div>
      <div class="legend_text">
          <strong>Due for Renewal</strong>
      </div>
  </div>
  <div class="ledgend_wrapper">
      <div class="ledgend_calendar" style="background-color:#c4352b;">

      </div>
      <div class="legend_text">
          <strong>Overdue</strong>
      </div>
  </div>
  <div class="ledgend_wrapper">
      <div class="ledgend_calendar" style="background-color:#98b7c7;">

      </div>
      <div class="legend_text">
          <strong>Booked on Course</strong>
      </div>
  </div>
</div>


        <a href="apply_courses.php" class="btn btn-sm btn-raised btn-info pull-right" id="assign_courses">Assign Courses</a>

        <div class="clearfix">

        </div>



         <table data-toggle="table" data-search="true" id="table" data-toolbar="#toolbar" data-show-export="true">
         <thead class="table_header">
           <tr>
             <th class="table_white">Name</th>
             <?foreach ($courses as $c) {?>
                 <th data-field="<?echo $c['id'];?>" data-sortable="false" data-visible="true" data-width="250"><?echo $c['title'];?></th>
             <?}?>
           </tr>
         </thead>
         <tbody>

           <?foreach ($learners as $l) {?>
             <tr>
               <td><?echo $l['first_name'].' '.$l['surname'];?></td>
               <?foreach ($courses as $c) {?>
                 <?$course_info = get_my_course_progress_tm($l['id'],$c['id']);?>
                   <td class="<?echo $course_info['colour'];?>" >
                     <?if(!empty($course_info['course_date'])){echo date('d-m-Y', $course_info['course_date']);}?>
                   </td>
               <?}?>
             </tr>
           <?}?>



         </tbody>
       </table>


      </div>
    </div>
    <script src="<? echo $fullurl ?>assets/js/bootstrap-table.js"></script>
    <script src="<? echo $fullurl ?>assets/js/table-extensions/export/bootstrap-table-export.js"></script>

    <script src="<? echo $fullurl; ?>assets/js/table.js"></script>
    <script src="<? echo $fullurl; ?>admin/accounts/app_scripts/index.js"></script>
    <script>
      $(document).ready(function() {
        var $table = $('#table');
       var $sixty = $( document ).height();
        $sixty = ($sixty/100)*50;
     $table.bootstrapTable( 'resetView' , {height: $sixty} );
      });

    </script>
