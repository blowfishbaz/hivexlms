<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $company_id = createid('comp');
 $company_name = $_POST['company_name'];
 $main_email = $_POST['main_email'];
 $main_telephone = $_POST['main_telephone'];

 $address_id = createid('adx');
 $address1 = $_POST['address1'];
 $address2 = $_POST['address2'];
 $address3 = $_POST['address3'];
 $city = $_POST['city'];
 $county = $_POST['county'];
 $country = $_POST['country'];
 $postcode = $_POST['postcode'];

 $created_time = time();
 $created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db = new database;
 $db->Query("insert into ws_companies (id, created_date, created_by, company_name, main_email, main_telephone) values (?,?,?,?,?,?)");
 $db->bind(1,$company_id);
 $db->bind(2,$created_time);
 $db->bind(3,$created_by);
 $db->bind(4,$company_name);
 $db->bind(5,$main_email);
 $db->bind(6,$main_telephone);
 $db->execute();

 $db = new database;
 $db->Query("insert into ws_companies_address (id, company_id, created_date, created_by, address1, address2, address3, city, county, country, postcode, address_title) values (?,?,?,?,?,?,?,?,?,?,?,?)");
 $db->bind(1,$address_id);
 $db->bind(2,$company_id);
 $db->bind(3,$created_time);
 $db->bind(4,$created_by);
 $db->bind(5,$address1);
 $db->bind(6,$address2);
 $db->bind(7,$address3);
 $db->bind(8,$city);
 $db->bind(9,$county);
 $db->bind(10,$country);
 $db->bind(11,$postcode);
 $db->bind(12,'Main Address');
 $db->execute();


 echo 'ok';
