<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $company_id = $_POST['company_id'];
 $company_name = $_POST['company_name'];
 $main_email = $_POST['main_email'];
 $main_telephone = $_POST['main_telephone'];


 $created_time = time();
 $created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db = new database;
 $db->Query("update ws_companies set modified_date = ?, modified_by = ?, company_name = ?, main_email = ?, main_telephone = ? where id = ?");
 $db->bind(1,$created_time);
 $db->bind(2,$created_by);
 $db->bind(3,$company_name);
 $db->bind(4,$main_email);
 $db->bind(5,$main_telephone);
 $db->bind(6,$company_id);
 $db->execute();



 echo 'ok';
