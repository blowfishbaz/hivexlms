<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $account_id = createid('user');
 $firstname = $_POST['firstname'];
 $surname = $_POST['surname'];
 $job_title = $_POST['job_title'];
 $email = $_POST['email'];
 $password = $_POST['password'];
 $mobile = $_POST['mobile_number'];
 $telephone = $_POST['telephone_number'];

 $created_time = time();
 $created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 $company_id = $_POST['company_id'];
 $provision_id = $_POST['provision_select'];
 $account_type = $_POST['account_type'];


 $db = new database;
 $db->Query("INSERT INTO ws_accounts (id ,created_date ,first_name ,surname, email, username, password, attempts,lastattempt,profilepic,status) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
 $db->bind(1,$account_id);
 $db->bind(2,$created_time);
 $db->bind(3,$firstname);
 $db->bind(4,$surname);
 $db->bind(5,$email);
 $db->bind(6,encrypt($email));
 $db->bind(7,encrypt($password));
 $db->bind(8,'0');
 $db->bind(9,$created_by);
 $db->bind(10,'');
 $db->bind(11,'1');
 $db->execute();


 $db = new database;
 $db->Query("insert into ws_accounts_extra_info (id, account_id, company_id, main_provision, created_date, created_by, mobile_number, telephone, job_title, status, account_type) values (?,?,?,?,?,?,?,?,?,?,?)");
 $db->bind(1,createid('ainfo'));
 $db->bind(2,$account_id);
 $db->bind(3,$company_id);
 $db->bind(4,$provision_id);
 $db->bind(5,$created_time);
 $db->bind(6,$created_by);
 $db->bind(7,$mobile);
 $db->bind(8,$telephone);
 $db->bind(9,$job_title);
 $db->bind(10,'1');
 $db->bind(11,$account_type);
 $db->execute();


 echo 'ok';
