<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 $provision_id = $_POST['provision_id'];
 $name = $_POST['prov_name'];
 $telephone = $_POST['main_telephone'];
 $address1 = $_POST['address1'];
 $address2 = $_POST['address2'];
 $address3 = $_POST['address3'];
 $city = $_POST['city'];
 $county = $_POST['county'];
 $country = $_POST['country'];
 $postcode = $_POST['postcode'];
 $company_id = $_POST['account_id'];


 $created_time = time();
 $created_by = decrypt($_SESSION['SESS_ACCOUNT_ID']);


 $db = new database;
 $db->Query("update ws_provision set modified_date = ?, modified_by = ?, provision_name = ?, main_telephone = ?, address1 = ?, address2 = ?, address3 = ?, city = ?, county = ?, country = ?, postcode = ? where id = ?");
 $db->bind(1,$created_time);
 $db->bind(2,$created_by);
 $db->bind(3,$name);
 $db->bind(4,$telephone);
 $db->bind(5,$address1);
 $db->bind(6,$address2);
 $db->bind(7,$address3);
 $db->bind(8,$city);
 $db->bind(9,$county);
 $db->bind(10,$country);
 $db->bind(11,$postcode);
 $db->bind(12,$provision_id);
 $db->execute();




 echo 'ok';
