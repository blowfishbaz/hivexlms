<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */

 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

$offset = $_GET['offset'];
$limit = $_GET['limit'];
$provision = $_GET['provision'];
$provision_q = '';

if(!empty($provision)){
  $provision_q = ' and main_provision = "'.$provision.'" and account_type = "Learner"';
}


if (isset($_GET['search'])) {
	$search = '%'.$_GET['search'].'%';
} else {
	$search = "%";
}

if (isset($_GET['sort'])) {
	$sort = $_GET['sort'];
	$order = $_GET['order'];
} else {
	$sort = 'ws_booked_on.completed_date';
	$order = 'desc';
}

$filter = urldecode($_GET['filter']);
$filter = json_decode($filter);
$filterall ='';


$db->query("select ws_booked_on.*, ws_accounts.first_name, ws_accounts.surname, lms_course.title, ws_pdf.id, ws_provision.provision_name
from ws_booked_on
left join ws_accounts on ws_accounts.id = ws_booked_on.account_id
left join lms_course on lms_course.id = ws_booked_on.course_id
left join ws_pdf on ws_pdf.course_id = lms_course.id and ws_pdf.account_id = ws_accounts.id and ws_pdf.status = 1
left join ws_accounts_extra_info on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
left join ws_provision on ws_provision.id = ws_accounts_extra_info.main_provision
where ws_booked_on.course_status = 4 and ws_accounts_extra_info.company_id = ? $provision_q limit 20");
$db->bind(1,$_GET['id']);
$db->execute();
$rowcount = $db->rowcount();

$db->query("select ws_booked_on.*, ws_accounts.first_name, ws_accounts.surname, lms_course.title, ws_pdf.file_name, ws_pdf.download_path, ws_provision.provision_name
from ws_booked_on
left join ws_accounts on ws_accounts.id = ws_booked_on.account_id
left join lms_course on lms_course.id = ws_booked_on.course_id
left join ws_pdf on ws_pdf.course_id = lms_course.id and ws_pdf.account_id = ws_accounts.id and ws_pdf.status = 1
left join ws_accounts_extra_info on ws_accounts_extra_info.account_id = ws_accounts.id and ws_accounts_extra_info.status = 1
left join ws_provision on ws_provision.id = ws_accounts_extra_info.main_provision
where ws_booked_on.course_status = 4 and ws_accounts_extra_info.company_id = ? $provision_q order by $sort $order limit 20");
$db->bind(1,$_GET['id']);
// $db->bind(3,(int) $limit);
// $db->bind(4,(int) $offset);
$data = $db->ResultSet();
?>
{
"total": <? echo  $rowcount.' '; ?>,
"rows": <? echo json_encode($data);?>
}
