<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 include($_SERVER['DOCUMENT_ROOT'].'/application.php');




 if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');


    $_POST = $HTTP_RAW_POST_DATA = file_get_contents('php://input');

    $_POST = json_decode($_POST,true);

    error_log('****'.$_POST['username'].'****');


    if (isset($_GET['action']) && $_GET['action'] == 'logon') {
        $member_id = '';
        $logonError = 0;
        error_log('1');
        if ($_POST['username'] == '') {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $login = $login;
            //Validate no Blank Details
            if($login == '') {
                error_log('1');
                $logonError = 1;
            }
            if($password == '') {
                error_log('2');
                $logonError = 1;
            }

            if($logonError == 0) {

                error_log('3');

                $login = encrypt($login);
                $password = encrypt($password);


                $db->query("select * from accounts where username= ? AND password= ?");
                $db->bind(1,$login);
                $db->bind(2,$password);
                $member = $db->single();
                $count = $db->rowcount();

                error_log('***'.$count.'***');

                if ($count >= 1) {
                    error_log('4');
                    //Check to see if user is locked?
                    $date = date('Y-m-d H:i:s');

                    $expire_stamp = strtotime($member['lastattempt']) + (30*60);
                    $date = strtotime($date);
                    $attempts = $member['attempts'];

                    if($attempts == 3 && $date >= $expire_stamp) {
                        $locked = 0;
                    } else if ($attempts == 3 && $date < $expire_stamp) {
                        $locked = 1	;
                    } else {
                        $locked = 0;
                    }

                    if ($locked == 0) {
                        error_log('5');
                        //Username and pass correct and not locked so can logon
                        $id = $member['id'];

                        $db->query("update accounts set attempts = ? where id = ?");
                        $db->bind(1,'0');
                        $db->bind(2,$member['id']);
                        $db->execute();

                        //remove old persistent records in db
                        $db->query("delete from api_logged_in where account_id = ?");
                        $db->bind(1,$member['id']);
                        $db->execute();
                        $member_id = $member['id'];
                        //echo $logonError;
                        //$logonError = SetSessions($id,$remember);

                        $logonError = 77;

                    }
                    //Account Locked
                    else {
                        error_log('6');
                        $logonError = 2;
                    }



                }else {
                    //Cannot Find User with username and pass so see if the email exists
                    error_log('7');
                    $db->query("select * from accounts where username = ?");
                    $db->bind(1,$login);
                    $member2 = $db->single();
                    $count2 = $db->rowcount();
                    $db->console($count2);
                    //If found add attempts
                    if ($count2 >= 1) {
                        error_log('8');
                        //Check to see if user is locked?
                        $date = date('Y-m-d H:i:s');
                        $expire_stamp = strtotime($member2['lastattempt']) + (30*60);
                        $date = strtotime($date);
                        $attempts = $member2['attempts'];
                        $db->console("attempts:".$attempts);
                        if($attempts == 3 && $date >= $expire_stamp) {
                            $locked = 0;
                        } else if ($attempts == 3 && $date < $expire_stamp) {
                            $locked = 1	;
                        } else {
                            $locked = 0;
                        }

                        $db->console("locked:".$locked);

                        if ($locked == 0) {


                        $current = $attempts + 1;
                        $date = date('Y-m-d H:i:s');

                        $db->query("update accounts set attempts = ?, lastattempt = ? where id = ?");
                        $db->bind(1,$current);
                        $db->bind(2,time());
                        $db->bind(3,$member2['id']);
                        $db->execute();
                        }
                        else {
                            $logonError = 2;
                        }
                    }error_log('9');
                    if ($logonError == 0 ) { $logonError = 1; }
                }
            } else {
    			if ($logonError == 0 ) { $logonError = 1; }
    		}
            error_log('10');
            error_log($logonError.' Logon Error');
            if ($logonError == 1) {
                    //Return Error Logging In
                    echo json_encode(array('error'=>'unable to login'));error_log('11');
            } elseif ($logonError == 2) {
                    //Return Account Locked
                    echo json_encode(array('error'=>'account locked'));error_log('12');
            } elseif ($logonError == 77) {
                //Create Token
                $logged_in_token = create_api_token($member_id);

                $db->query("insert into api_logged_in (id, account_id, created_date, ip_address, expiry_date, token) values (?,?,?,?,?,?)");
                $db->bind(1,createid('api_log'));
                $db->bind(2,$member_id);
                $db->bind(3,time());
                $db->bind(4,encrypt($_SERVER['REMOTE_ADDR']));
                $db->bind(5,strtotime('+5 Days'));
                $db->bind(6,encrypt($logged_in_token));
                $db->execute();

                $result_json = array('token'=>encrypt($logged_in_token));
                // send the result now
                echo json_encode($result_json);error_log('13');

            }
            error_log('14');
        }
        error_log('15');
    }else{
        echo json_encode(array('error'=>'no logon'));
    }







    exit();

 }



?>
