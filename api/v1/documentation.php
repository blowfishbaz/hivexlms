<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

  <title>API Documentation</title>
</head>
  <body>

    <div style="background: url(/staff-box/images/Header-Image.png); margin-bottom:20px;">
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <h1><img src="https://dev.staff-box.co.uk//staff-box/images/logo.png" width="150px;"> - Api V1 Documentation</h1>
          </div>
        </div>
      </div>
    </div>


    <div class="container">
      <div class="row">
        <div class="col-sm">
          <h2>Authenticate</h2>
          <div class="accordion" id="authenticate_accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      POST - Get Token
                    </button>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#authenticate_accordion">
                  <div class="card-body">
                    <div class="card card-body bg-light">
                      https://dev.staff-box.co.uk/api/v1/auth.php?action=logon
                    </div>
                    <strong>Parameters</strong>
                    <table class="table">
                      <tr>
                        <th>Name</th>
                        <th>Required</th>
                        <th>Description</th>
                      </tr>
                      <tr>
                        <td>username (empty)</td>
                        <td></td>
                        <td>Never filled out</td>
                      </tr>
                      <tr>
                        <td>login (email)</td>
                        <td>Required</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>password (string)</td>
                        <td>Required</td>
                        <td></td>
                      </tr>
                    </table>
                    <strong>Example</strong>
                    <div class="card card-body bg-light">
                      {
                        "username":"","login":"string","password":"string"
                      }
                    </div>


                    <strong>Response - 200</strong>
                    <div class="card card-body bg-light">
                      {
                        "token":"string"
                      }
                    </div>
                    <div class="card card-body bg-light">
                      Token to be used as Bearer - "Authorization: Bearer TOKEN_XXXXX_12345";<br />Token has a 5 day limit (at the moment for development) this will likely change, tbc
                    </div>
                    <strong>Response - 400</strong>
                    <div class="card card-body bg-light">
                      {
                        "error":"string"
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <h2>Account</h2>


            <div id="accordion_ACCOUNT">
                <div class="card">
                  <div class="card-header" id="account_update_heading">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_update" aria-expanded="false" aria-controls="account_update">
                        POST - Update Account Information
                      </button>
                    </h5>
                  </div>
                  <div id="account_update" class="collapse" aria-labelledby="account_update_heading" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=update
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>first_name (String)</td> <td>Required</td> <td></td> </tr>
                        <tr> <td>surname (String)</td> <td>Required</td> <td></td> </tr>
                        <tr> <td>email (String - Email)</td> <td>Required</td> <td></td> </tr>
                        <tr> <td>mobile_number (Int)</td> <td></td> <td></td> </tr>
                        <tr> <td>telephone (Int)</td> <td></td> <td></td> </tr>
                        <tr> <td>job_title (String)</td> <td></td> <td></td> </tr>
                        <tr> <td>address1 (String)</td> <td></td> <td></td> </tr>
                        <tr> <td>address2 (String)</td> <td></td> <td></td> </tr>
                        <tr> <td>address3 (String)</td> <td></td> <td></td> </tr>
                        <tr> <td>city (String)</td> <td></td> <td></td> </tr>
                        <tr> <td>postcode (String)</td> <td></td> <td></td> </tr>
                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {
                          "Account":"Update Successful"
                        }
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="account_heading_deletedocuments">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_deletedocuments" aria-expanded="false" aria-controls="account_deletedocuments">
                        POST - Delete Document
                      </button>
                    </h5>
                  </div>
                  <div id="account_deletedocuments" class="collapse" aria-labelledby="account_heading_deletedocuments" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=deletedocument
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>id (String)</td> <td>Required</td> <td>Documents are never deleted just hidden from the user using statues</td> </tr>
                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {
                          "Account":"Document Deleted"
                        }
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="account_heading_updatemyroles">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_updatemyroles" aria-expanded="false" aria-controls="account_updatemyroles">
                        POST - Update My Roles
                      </button>
                    </h5>
                  </div>
                  <div id="account_updatemyroles" class="collapse" aria-labelledby="account_heading_updatemyroles" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=updatemyroles
                      </div>
                      <strong>Example</strong>
                      <div class="card card-body bg-light">
                        {"string","string","string","string"}
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {
                          "Account":"Roles Updated Successful"
                        }
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_updatemylocations">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_updatemylocations" aria-expanded="false" aria-controls="account_updatemylocations">
                        POST - Update My Locations
                      </button>
                    </h5>
                  </div>
                  <div id="account_updatemylocations" class="collapse" aria-labelledby="account_heading_updatemylocations" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=updatemylocation
                      </div>
                      <strong>Example</strong>
                      <div class="card card-body bg-light">
                        {"string","string","string","string"}
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {
                          "Account":"Locations Updated Successful"
                        }
                      </div>
                    </div>
                  </div>
                </div>



                <div class="card">
                  <div class="card-header" id="account_heading_getinfo">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_getinfo" aria-expanded="false" aria-controls="account_getinfo">
                        GET - Get Account Information
                      </button>
                    </h5>
                  </div>
                  <div id="account_getinfo" class="collapse" aria-labelledby="account_heading_getinfo" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=getinfo
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {<br /> "id":"string",<br /> "created_date":"int",<br /> "first_name":string"",<br /> "surname":"string",<br /> "email":"string",<br /> "mobile_number":"int",<br /> "telephone":"int",<br /> "job_title":"string",<br /> "address1":"string",<br /> "address2":"string",<br /> "address3":"string",<br /> "city":"string",<br /> "postcode":"string",<br /> }
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_myroles">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_myroles" aria-expanded="false" aria-controls="account_myroles">
                        GET - Get Account Roles
                      </button>
                    </h5>
                  </div>
                  <div id="account_myroles" class="collapse" aria-labelledby="account_heading_myroles" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=getroles
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "title":"string"<br /> },{<br /> "title":"string"<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_mylocation">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_mylocation" aria-expanded="false" aria-controls="account_mylocation">
                        GET - Get Account Location
                      </button>
                    </h5>
                  </div>
                  <div id="account_mylocation" class="collapse" aria-labelledby="account_heading_mylocation" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=getlocation
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "title":"string"<br /> },{<br /> "title":"string"<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_upcomingevents_dash">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_upcomingevents_dash" aria-expanded="false" aria-controls="account_upcomingevents_dash">
                        GET - Get Upcoming Events (Dashboard)
                      </button>
                    </h5>
                  </div>
                  <div id="account_upcomingevents_dash" class="collapse" aria-labelledby="account_heading_upcomingevents_dash" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=dashboardupcomingevents
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "id":"string",<br /> "start_date":"date",<br /> "end_date":"date",<br /> "start_time":"string",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "role_title":"string",<br /> "location_title":"string"<br /> },{<br /> "id":"string",<br /> "start_date":"date",<br /> "end_date":"date",<br /> "start_time":"string",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "role_title":"string",<br /> "location_title":"string"<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_prevevents_dash">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_prevevents_dash" aria-expanded="false" aria-controls="account_prevevents_dash">
                        GET - Get Previous Events (Dashboard)
                      </button>
                    </h5>
                  </div>
                  <div id="account_prevevents_dash" class="collapse" aria-labelledby="account_heading_prevevents_dash" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=dashboardpreviousevents
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "id":"string",<br /> "start_date":"date",<br /> "end_date":"date",<br /> "start_time":"string",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "role_title":"string",<br /> "location_title":"string"<br /> },{<br /> "id":"string",<br /> "start_date":"date",<br /> "end_date":"date",<br /> "start_time":"string",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "role_title":"string",<br /> "location_title":"string"<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_my_docs_list">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_my_docs_list" aria-expanded="false" aria-controls="account_my_docs_list">
                        GET - Document list
                      </button>
                    </h5>
                  </div>
                  <div id="account_my_docs_list" class="collapse" aria-labelledby="account_heading_my_docs_list" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=mydocumentlist
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {"string","string","string"}
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_all_documents">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_all_documents" aria-expanded="false" aria-controls="account_all_documents">
                        GET - All Documents
                      </button>
                    </h5>
                  </div>
                  <div id="account_all_documents" class="collapse" aria-labelledby="account_heading_all_documents" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=mydocumentsall
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "type":"string",<br /> "type_nice":"string",<br /> "name":"string",<br /> "path":"string",<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="account_heading_my_invoices">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#account_my_invoices" aria-expanded="false" aria-controls="account_my_invoices">
                        GET - Invoice List
                      </button>
                    </h5>
                  </div>
                  <div id="account_my_invoices" class="collapse" aria-labelledby="account_heading_my_invoices" data-parent="#accordion_ACCOUNT">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/account.php?action=getmyinvoices
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "service_title":"string",<br /> "role_title":"string",<br /> "start_date":"date",<br /> "start_time":"string",<br /> "end_date":"date",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "hourly_rate":"int",<br /> "amount":"int",<br /> "status":"string",<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

            </div>


            <h2>Calendar</h2>


            <div id="accordion_calendar">
                <div class="card">
                  <div class="card-header" id="calendar_heading_get_calendar_month">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#calendar_get_calendar_month" aria-expanded="false" aria-controls="calendar_get_calendar_month">
                        GET - Get Calendar Events
                      </button>
                    </h5>
                  </div>
                  <div id="calendar_get_calendar_month" class="collapse" aria-labelledby="calendar_heading_get_calendar_month" data-parent="#accordion_calendar">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/calendar.php?action=getcalendarmonth
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>date_from</td> <td></td> <td>&date_from=2021-01-01</td> </tr>
                        <tr> <td>date_to</td> <td></td> <td>&date_to=2021-02-01</td> </tr>

                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "rota_id":"string",<br /> "start_date":"date",<br /> "start_time":"string",<br /> "end_date":"date",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "week_num":"int",<br /> "year":"int",<br /> "dna":"int",<br /> "dna_reason":"string",<br /> "dna_date":"int"<br /> }]
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="calendar_heading_get_event">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#calendar_get_event" aria-expanded="false" aria-controls="calendar_get_event">
                        GET - Get Calendar Event Single
                      </button>
                    </h5>
                  </div>
                  <div id="calendar_get_event" class="collapse" aria-labelledby="calendar_heading_get_event" data-parent="#accordion_calendar">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/calendar.php?action=getcalendarevent
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>id</td> <td>Required</td> <td></td> </tr>
                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {<br /> "id":"string",<br /> "start_date":"date",<br /> "end_date":"date",<br /> "start_time":"string",<br /> "end_time":"string",<br /> "start_ts":"int",<br /> "end_ts":"int",<br /> "role":"string",<br /> "location":"string",<br /> "did_not_attend":"int",<br /> "did_not_attend_reason":"string",<br /> }
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="calendar_heading_didnotattend">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#calendar_didnotattend" aria-expanded="false" aria-controls="calendar_didnotattend">
                        POST - Did Not Attend
                      </button>
                    </h5>
                  </div>
                  <div id="calendar_didnotattend" class="collapse" aria-labelledby="calendar_heading_didnotattend" data-parent="#accordion_calendar">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/calendar.php?action=didnotattned
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                          <tr> <td>id</td> <td>required</td> <td></td> </tr>
                          <tr> <td>reason</td> <td>required</td> <td></td> </tr>

                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {"calendar":"Shift Marked as Did Not attend"}
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="calendar_heading_didattent">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#calendar_didattent" aria-expanded="false" aria-controls="calendar_didattent">
                        POST - Did Attend
                      </button>
                    </h5>
                  </div>
                  <div id="calendar_didattent" class="collapse" aria-labelledby="calendar_heading_didattent" data-parent="#accordion_calendar">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/calendar.php?action=didattned
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                          <tr> <td>id</td> <td>required</td> <td></td> </tr>
                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {"calendar":"Shift Marked as Did attend"}
                      </div>
                    </div>
                  </div>
                </div>


            </div>

            <h2>Notifications</h2>


            <div id="accordion_notification">
                <div class="card">
                  <div class="card-header" id="notification_heading_mynotifications">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#notification_mynotifications" aria-expanded="false" aria-controls="notification_mynotifications">
                        GET - Get Unread Notifications
                      </button>
                    </h5>
                  </div>
                  <div id="notification_mynotifications" class="collapse" aria-labelledby="notification_heading_mynotifications" data-parent="#accordion_notification">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/notification.php?action=getallnotifications
                      </div>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br /> "id":"string",<br /> "created_by":"string",<br /> "pid":"string",<br /> "title":"string",<br /> "message":"string",<br /> "type":"string",<br /> "created_date":"int"<br /> }]
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="notification_heading_single_notification">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#notification_single_notification" aria-expanded="false" aria-controls="notification_single_notification">
                        GET - Get Single Notification
                      </button>
                    </h5>
                  </div>
                  <div id="notification_single_notification" class="collapse" aria-labelledby="notification_heading_single_notification" data-parent="#accordion_notification">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/notification.php?action=getsinglenotification
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>id</td> <td>Required</td> <td>Once a notification has been opened, it will automatically be marked as viewed</td> </tr>


                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        [{<br />
                          "id":"string",<br />
                          "created_by":"string",<br />
                          "created_date":"int",<br />
                          "pid":"string",<br />
                          "title":"string",<br />
                          "message":"string",<br />
                          "type":"string"<br />
                        }]
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <h2>Shifts</h2>


            <div id="accordion_shifts">
                <div class="card">
                  <div class="card-header" id="shifts_single_shift">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#shifts_heading_single_shift" aria-expanded="false" aria-controls="shifts_single_shift">
                        GET - Available Shifts (Single)
                      </button>
                    </h5>
                  </div>
                  <div id="shifts_heading_single_shift" class="collapse" aria-labelledby="shifts_single_shift" data-parent="#accordion_shifts">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/shifts.php?action=getavailableshifts
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>type</td> <td>Required</td> <td>type=single</td> </tr>
                        <tr> <td>id</td> <td>Required</td> <td></td> </tr>
                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {<br />
                          "banner_message":"string",<br />
                          "service_title":"string",<br />
                          "service_description":"string",<br />
                          "service_location":"string",<br />
                          "role_title":"string",<br />
                          "hourly_rate":"string",<br />
                          "pcn_title":"string",<br />
                          "practice_title":"string",<br />
                          "user_response":"string",<br />
                          "admin_response":"string",<br />
                          "seen_date":"int",<br />
                          "user_response_date":"int",<br />
                          "admin_response_date":"int",<br />
                          "rota_start_date":"date",<br />
                          "rota_end_date":"date",<br />
                          "rota_start_time":"int",<br />
                          "rota_end_time":"int",<br />
                          "button_show_accept":"string",<br />
                          "button_show_decline":"string"<br />
                        }
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="shifts_multiple_shifts">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#shifts_heading_multiple_shifts" aria-expanded="false" aria-controls="shifts_multiple_shifts">
                        GET - Available Shifts (List)
                      </button>
                    </h5>
                  </div>
                  <div id="shifts_heading_multiple_shifts" class="collapse" aria-labelledby="shifts_multiple_shifts" data-parent="#accordion_shifts">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/shifts.php?action=getavailableshifts
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>type</td> <td>Required</td> <td>type=multiple</td> </tr>
                        <tr> <td>id</td> <td></td> <td>Will be ignored</td> </tr>
                      </table>
                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {<br />
                          "bid_id":"string",<br />
                          "role_title":"string",<br />
                          "service_title":"string",<br />
                          "service_location":"string",<br />
                          "start_date":"date",<br />
                          "start_time":"string"<br />
                        }
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="shifts_accept_bid">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#shifts_heading_accept_bid" aria-expanded="false" aria-controls="shifts_accept_bid">
                        POST - Accept Bid
                      </button>
                    </h5>
                  </div>
                  <div id="shifts_heading_accept_bid" class="collapse" aria-labelledby="shifts_accept_bid" data-parent="#accordion_shifts">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/shifts.php?action=acceptbid
                      </div>

                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>id</td> <td>Required</td> <td></td> </tr>
                      </table>

                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {"shift":"Bid Accept Saved"}
                      </div>
                    </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="shifts_decline_bid">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#shifts_heading_decline_bid" aria-expanded="false" aria-controls="shifts_decline_bid">
                        POST - Decline Bid
                      </button>
                    </h5>
                  </div>
                  <div id="shifts_heading_decline_bid" class="collapse" aria-labelledby="shifts_decline_bid" data-parent="#accordion_shifts">
                    <div class="card-body">

                      <div class="card card-body bg-light">
                        /api/v1/shifts.php?action=declinebid
                      </div>
                      <strong>Parameters</strong>
                      <table class="table">
                        <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                        <tr> <td>id</td> <td>Required</td> <td></td> </tr>
                      </table>

                      <strong>Response - 200</strong>
                      <div class="card card-body bg-light">
                        {"shift":"Bid Declined Saved"}
                      </div>
                    </div>
                  </div>
                </div>


            </div>

<!--
            <h2>Misc</h2>


            <div id="accordion_misc">
                <div class="card">
                  <div class="card-header" id="misc_example_call">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#misc_heading_example_call" aria-expanded="false" aria-controls="misc_example_call">
                        Example Call
                      </button>
                    </h5>
                  </div>
                  <div id="misc_heading_example_call" class="collapse" aria-labelledby="misc_example_call" data-parent="#accordion_misc">
                    <div class="card-body">
                      <strong>Example - PHP</strong>
                      <div class="card card-body bg-light">


                      </div>
                    </div>
                  </div>
                </div>




            </div> -->







            <!--
            <div class="card">
              <div class="card-header" id="misc_">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#misc_heading_" aria-expanded="false" aria-controls="misc_">
                    GET -
                  </button>
                </h5>
              </div>
              <div id="misc_heading_" class="collapse" aria-labelledby="misc_" data-parent="#accordion_misc">
                <div class="card-body">

                  <div class="card card-body bg-light">

                  </div>
                  <strong>Parameters</strong>
                  <table class="table">
                    <tr> <th>Name</th> <th>Required</th> <th>Description</th> </tr>
                    <tr> <td></td> <td></td> <td></td> </tr>
                  </table>
                  <strong>Response - 200</strong>
                  <div class="card card-body bg-light">

                  </div>
                </div>
              </div>
            </div>
          -->



        </div>
      </div>
    </div>




    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
       <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


  </body>
</html>
