<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/



 require 'functions.php';
 require 'auth_functions.php';

  header('Content-Type: application/json;charset=utf-8');
  header("HTTP/1.1 200 OK");
 $_POST = $HTTP_RAW_POST_DATA = file_get_contents('php://input');
 $_POST = json_decode($_POST,true);

 class HiveXAPIShifts extends hiveXAPI{

    public function getavailableshifts($type = 'single', $id = NULL){
      //Check if multiple or single

      //Call the other two functions if need be

      if($type == 'single'){
        return $this->getavailableshiftssingle($id);
      }else if($type == 'multiple'){
        return $this->getavailableshiftslist();
      }else{
        return 'Error';
      }
    }

     //Get Info
     public function getavailableshiftslist(){
       //This will get all the available shifts for me to view
       $db = new database;
       $db->query("select ws_rota_bid_request.id as bid_id, ws_roles.title as role_title, ws_service.title as service_title, ws_service.location as service_location, ws_rota_bid.start_date, ws_rota_bid.start_time, ws_rota_bid_request.account_id
       from ws_rota_bid_request
       join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
       join ws_service on ws_rota_bid_request.service_id = ws_service.id
       join ws_rota_bid on ws_rota_bid.id = ws_rota_bid_request.pid
       where ws_rota_bid_request.account_id = ? and accepted_admin = ? order by ws_rota_bid_request.id desc ");
       $db->bind(1,$this->$accountID);
       $db->bind(2,0);
       $data = $db->ResultSet();

       return $data;
     }

     public function getavailableshiftssingle($id){

       $request_id = $id;
       $account_id = $this->$accountID;

       $bid_req_info = $this->getRotaBidRequestSingle($request_id);
       $bid_info = $this->getRotaBidSingle($bid_req_info['pid']);
       $service = $this->getServiceSingle($bid_info['service_id']);
       $service_amount = $this->getRotaBidServiceRoleSingle($service['id'], $bid_req_info['role_id']);
       $role = $this->getRolesSingle($bid_req_info['role_id']);
       $this->roleSeen($request_id, $bid_req_info['seen']);
       $pcn = $this->getPCNSingle($bid_info['pcn_id']);
       $practice = $this->getPracticeSingle($bid_info['practice_id']);

       if($bid_req_info['accepted_user'] == 1 && $bid_req_info['accepted_admin'] == 0){$banner_message = 'You have said you wish to attend this session. Once the admin team has responded to your application we will let you know.';}
       if($bid_req_info['accepted_admin'] == 1){$banner_message = 'You have been accepted onto the below shift. Check your emails for confirmation.';}
       if($bid_req_info['accepted_admin'] == 2){$banner_message = 'You have been declined for the shift.';}

       $service_title = $service['title'];
       $service_description = $service['description'];
       $service_location = $service['location'];
       $role_title = $role['title'];
       $per_hour = $service_amount['hourly_rate'];
       $pcn_location = $pcn['pcn_title'];
       $practice_location = $practice['title'];

       //Admin Responce and all Reponses
       $user_responce = '';
       if($bid_req_info['accepted_user'] == 1){$user_responce = 'Accepted';}else if($bid_req_info['accepted_user'] == 2){$user_responce = 'Declined';}else if($bid_req_info['accepted_user'] == 0){$user_responce = 'Waiting Response';}else{$user_responce = 'Error';}

       $admin_responce = '';
       if($bid_req_info['accepted_admin'] == 1){$admin_responce = 'Accepted';}else if($bid_req_info['accepted_admin'] == 2){$admin_responce = 'Declined';}else if($bid_req_info['accepted_admin'] == 0){$admin_responce = 'Waiting Response';}else{$admin_responce = 'Error';}

       $seen_date = '';
       if(!empty($bid_req_info['seen_date'])){$seen_date = date('d-m-Y H:i',$bid_req_info['seen_date']);}

       $choice_date = '';//User
       if(!empty($bid_req_info['choice_date'])){$choice_date = date('d-m-Y H:i',$bid_req_info['choice_date']);}

       $choice_date_admin = '';
       if(!empty($bid_req_info['choice_date_admin'])){$choice_date_admin = date('d-m-Y H:i',$bid_req_info['choice_date_admin']);}

       $start_date = $bid_info['start_date'];
       $end_date = $bid_info['end_date'];
       $start_time = $bid_info['start_time'];
       $end_time = $bid_info['end_time'];

       //Which button is to be shown
       $button_to_show_accept = 'no';
       $button_to_show_reject = 'no';

       if($bid_info['status'] == 1 && $bid_req_info['accepted_admin'] == 0){
         if($bid_req_info['accepted_user'] != 1){
           $button_to_show_accept = 'show';
         }
         if($bid_req_info['accepted_user'] != 2){
           $button_to_show_reject = 'show';
         }
       }

       $return_array = array(
         'banner_message' => $banner_message,
         'service_title' => $service_title,
         'service_description' => $service_description,
         'service_location' => $service_location,
         'role_title' => $role_title,
         'hourly_rate' => $per_hour,
         'pcn_title' => $pcn_location,
         'practice_title' => $practice_location,
         'user_response' => $user_responce,
         'admin_response' => $admin_responce,
         'seen_date' => $seen_date,
         'user_response_date' => $choice_date,
         'admin_response_date' => $choice_date_admin,
         'rota_start_date' => $start_date,
         'rota_end_date' => $end_date,
         'rota_start_time' => $start_time,
         'rota_end_time' => $end_time,
         'button_show_accept' => $button_to_show_accept,
         'button_show_decline' => $button_to_show_reject
       );


       return $return_array;
     }

     public function acceptShift($id){
       //Send Noticiation to the admin
       $db = new database;
       $db->Query("update ws_rota_bid_request set accepted_user = 1, choice_date = ? where id = ?");
       $db->bind(1,time());
       $db->bind(2,$id);
       $db->execute();

       $db->query("select * from ws_rota_bid_request where id = ?");
       $db->bind(1,$id);
       $rota_bid_request = $db->single();

       $db->query("select * from ws_rota_bid where id = ?");
       $db->bind(1,$rota_bid_request['pid']);
       $rota_bid = $db->single();

       $db->query("select * from accounts where id = ?");
       $db->bind(1,$rota_bid_request['account_id']);
       $account = $db->single();

       $db->query("select * from ws_service where id = ?");
       $db->bind(1,$rota_bid_request['service_id']);
       $service = $db->single();

       $db->query("select * from ws_roles where id = ?");
       $db->bind(1,$rota_bid_request['service_role_id']);
       $role = $db->single();

       $notification = ''.$account['name'].', has accepted the shift - '.$service['title'].', on '.$rota_bid['start_date'].'.';

       //Send Notification
       $this->sendNotification($rota_bid['created_by'],$rota_bid['id'],'Shift Bid Accepted',$notification,'user_shift_response');

       return array('Shift' => 'Bid Accept Saved');

     }

     public function declineShift(){
       //Send Noticiation to the admin
       $db = new database;
       $db->Query("update ws_rota_bid_request set accepted_user = 0, choice_date = ? where id = ?");
       $db->bind(1,time());
       $db->bind(2,$id);
       $db->execute();

       return array('Shift' => 'Bid Declined Saved');
     }

     public function roleSeen($request_id = null, $seen = 1){
       if(!empty($request_id) && $seen = 0){
         $db = new database;
         $db->Query("update ws_rota_bid_request set seen = 1, seen_date = ? where id = ?");
         $db->bind(1,time());
         $db->bind(2,$request_id);
         $db->execute();
       }
     }

 }

 $bear = getBearerToken();

 if (isset($_GET['action'])) {

     $token = $bear;



     $shifts = new HiveXAPIShifts($token);

     $action = $_GET['action'];
     $type = $_GET['type'];
     $id = $_GET['id'];

     $post_data = $_POST['data'];

     if($_SERVER['REQUEST_METHOD'] === 'GET'){
       switch ($action) {
         case 'getavailableshifts':
           $return_data = $shifts->getavailableshifts($type,$id);
           break;
         default:
           $return_data = array('error'=>'Error with Request Method');
           break;
       }
     }else if($_SERVER['REQUEST_METHOD'] === 'POST'){
       switch ($action) {
         case 'acceptbid':
           $return_data = $shifts->acceptShift($post_data);
           break;
         case 'declinebid':
           $return_data = $shifts->declineShift($post_data);
           break;
         default:
           $return_data = array('error'=>'Error with Request Method');
           break;
       }
     }else{
        $return_data = array('error'=>'Error with Request Method');
     }

      // switch ($action) {
      //   case 'getavailableshifts':
      //     $return_data = $shifts->getavailableshifts($type,$id);
      //     break;
      //   case 'acceptbid':
      //     $return_data = $shifts->acceptShift($id);
      //     break;
      //   case 'declinebid':
      //     $return_data = $shifts->declineShift($id);
      //     break;
      //   default:
      //     break;
      // }


     echo json_encode($return_data);

 }else{

 }
