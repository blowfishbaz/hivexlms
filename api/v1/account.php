<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/



 require 'functions.php';
 require 'auth_functions.php';

  header('Content-Type: application/json;charset=utf-8');
  header("HTTP/1.1 200 OK");
 $_POST = $HTTP_RAW_POST_DATA = file_get_contents('php://input');
 $_POST = json_decode($_POST,true);

 class HiveXAPIAccounts extends hiveXAPI{

     //Get Info

     public function getMainInfo(){
       $db = new database;
       $db->query("select * from ws_accounts where id = ?");
       $db->bind(1,$this->$accountID);
       $member = $db->single();

       return $member;
     }

     public function getExtraInfo(){
       $db = new database;
       $db->query("select * from ws_accounts_extra_info where account_id = ? and status = 1");
       $db->bind(1,$this->$accountID);
       $member = $db->single();

       return $member;
     }

     public function getInfo(){
          //getinfo
          $data = array();
          $main_info = $this->getMainInfo();
          $extra_info = $this->getExtraInfo();

          $data['id'] = $this->$accountID;
          $data['created_date'] = $main_info['created_date'];
          $data['first_name'] = $main_info['first_name'];
          $data['surname'] = $main_info['surname'];
          $data['email'] = $main_info['email'];
          $data['mobile_number'] = $extra_info['mobile_number'];
          $data['telephone'] = $extra_info['telephone'];
          $data['job_title'] = $extra_info['job_title'];
          $data['address1'] = $extra_info['address1'];
          $data['address2'] = $extra_info['address2'];
          $data['address3'] = $extra_info['address3'];
          $data['city'] = $extra_info['city'];
          $data['postcode'] = $extra_info['postcode'];



          return $data;
     }

     public function getRoles(){
       $db = new database;
       $db->query("select ws_roles.title from ws_accounts_roles left join ws_roles on ws_roles.id = ws_accounts_roles.role_id where account_id = ? and ws_accounts_roles.status = 1");
       $db->bind(1,$this->$accountID);
       $roles = $db->resultSet();

       return $roles;

     }

     public function getLocations(){
       $db = new database;
       $db->query("select location_id as title from ws_accounts_locations where account_id = ? and status = 1");
       $db->bind(1,$this->$accountID);
       $location = $db->resultSet();

       return $location;
     }

     public function dashboardUpcomingEvents(){
       $db = new database;
       $db->query("select hx_rota.*, ws_rota_bid_request.role_id, ws_roles.title
                   from hx_rota
                   left join ws_rota_bid_request on ws_rota_bid_request.id = hx_rota.bid_request_id
                   left join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
                   where hx_rota.member_id = ? and start_ts > ?");
       $db->bind(1,$this->$accountID);
       $db->bind(2,strtotime(date('d-m-Y 23:59:59')));
       $upcoming_rota = $db->resultset();

       $return_array = array();

       foreach ($upcoming_rota as $up) {
         $new_array = array();
         $new_array['id'] = $up['id'];
         $new_array['start_date'] = $up['start_date'];
         $new_array['end_date'] = $up['end_date'];
         $new_array['start_time'] = $up['start_time'];
         $new_array['end_time'] = $up['end_time'];
         $new_array['start_ts'] = $up['start_ts'];
         $new_array['end_ts'] = $up['end_ts'];
         $new_array['role_title'] = $up['role_id'];
         $new_array['location_title'] = $up['location'];

         array_push($return_array,$new_array);
       }

       return $return_array;
     }

     public function dashboardPreviousEvents(){
       $db = new database;
       $db->query("select hx_rota.*, ws_rota_bid_request.role_id, ws_roles.title
                   from hx_rota
                   left join ws_rota_bid_request on ws_rota_bid_request.id = hx_rota.bid_request_id
                   left join ws_roles on ws_roles.id = ws_rota_bid_request.role_id
                   where hx_rota.member_id = ? and start_ts < ? limit 10");
       $db->bind(1,$this->$accountID);
       $db->bind(2,strtotime(date('d-m-Y 23:59:59')));
       $previous_rota = $db->resultset();

       $return_array = array();

       foreach ($previous_rota as $up) {
         $new_array = array();
         $new_array['id'] = $up['id'];
         $new_array['start_date'] = $up['start_date'];
         $new_array['end_date'] = $up['end_date'];
         $new_array['start_time'] = $up['start_time'];
         $new_array['end_time'] = $up['end_time'];
         $new_array['start_ts'] = $up['start_ts'];
         $new_array['end_ts'] = $up['end_ts'];
         $new_array['role_title'] = $up['role_id'];
         $new_array['location_title'] = $up['location'];

         array_push($return_array,$new_array);
       }

       return $return_array;
     }

     public function myDocumentList(){
        $document_list = get_document_list($this->$accountID);
        $final_array = array();
        foreach ($document_list as $dl) {
          array_push($final_array,document_type_full_name($dl));

        }
        return $final_array;
     }

     public function myDocumentsAll(){
       $db = new database;
       $db->query("select * from ws_accounts_upload where account_id = ? and status = ? and type != ?");
       $db->bind(1,$this->$accountID);
       $db->bind(2,'1');
       $db->bind(3,'Manual');
       $uploads_per = $db->resultset();

       $return_array = array();

       foreach ($uploads_per as $up) {
         $loop_array = array();
         $loop_array['type'] = $up['type'];
         $loop_array['type_nice'] = document_type_full_name($up['type']);
         $loop_array['name'] = $up['name'];
         $loop_array['path'] = $up['path'];

         array_push($return_array,$loop_array);
       }

       return $return_array;
     }

     public function myDocumentsType($type){
       $db = new database;
       $db->query("select * from ws_accounts_upload where account_id = ? and status = ? and type = ?");
       $db->bind(1,$this->$accountID);
       $db->bind(2,'1');
       $db->bind(3,$type);
       $uploads_per = $db->resultset();

       $return_array = array();

       foreach ($uploads_per as $up) {
         $loop_array = array();
         $loop_array['type'] = $up['type'];
         $loop_array['type_nice'] = document_type_full_name($up['type']);
         $loop_array['name'] = $up['name'];
         $loop_array['path'] = $up['path'];

         array_push($return_array,$loop_array);
       }

       return $return_array;
     }

     public function myDocumentsID(){
       //Don't think this is needed
     }

     public function getAllMyRota(){
       //This might be in another file the calendar one
     }

     public function getMyInvoices(){
        $today_minus_72 = strtotime(" - 3 day");
        $db = new database;
        $db->query("select * from hx_rota where status = 1 and dna = 0 and end_ts < ? and member_id = ? order by start_ts desc");
        $db->bind(1,$today_minus_72);
        $db->bind(2,$this->$accountID);
        $rota_info = $db->resultset();

        $final_array = array();
        foreach ($rota_info as $data) {
          $loop_array = array();
          $db = new database;
          $db->query("select * from ws_service where id = ?");
          $db->bind(1,$data['service_id']);
          $service = $db->single();

          $db = new database;
          $db->query("select * from ws_roles where id = ?");
          $db->bind(1,$data['role']);
          $role = $db->single();

          $db = new database;
          $db->query("select * from ws_service_roles where pid = ? and job_role = ?");
          $db->bind(1,$data['service_id']);
          $db->bind(2,$data['role']);
          $service_role = $db->single();

          $hourly_rate = $service_role['hourly_rate'];
          $time1 = $data['start_ts'];
          $time2 = $data['end_ts'];
          $difference = round(abs($time2 - $time1) / 3600,2);

          $running_total = $running_total + ($difference * $hourly_rate);

          $loop_array['service_title'] = $service['title'];
          $loop_array['role_title'] = $role['title'];
          $loop_array['start_date'] = $data['start_date'];
          $loop_array['start_time'] = $data['start_time'];
          $loop_array['end_date'] = $data['end_date'];
          $loop_array['end_time'] = $data['end_time'];
          $loop_array['start_ts'] = $data['start_ts'];
          $loop_array['end_ts'] = $data['end_ts'];
          $loop_array['hourly_rate'] = $service_role['hourly_rate'];
          $loop_array['amount'] = $difference * $hourly_rate;
          if($data['invoice_paid'] == 0){$loop_array['status'] = 'Not Paid';}else{$loop_array['status'] = 'Paid';}

          array_push($final_array,$loop_array);
        }

        return $final_array;
     }

     //Update Info

     public function updateAccountInfo($data){

       $firstname = $data['first_name'];
       $surname = $data['surname'];
       $email = $data['email'];
       $mobile = $data['mobile_number'];
       $telephone = $data['telephone'];
       $job_title = $data['job_title'];
       $address1 = $data['address1'];
       $address2 = $data['address2'];
       $address3 = $data['address3'];
       $city = $data['city'];
       $postcode = $data['postcode'];

       $db = new database;
       $db->query("update accounts set name = ?, email = ? where id = ?");
       $db->bind(1,$firstname.' '.$surname);
       $db->bind(2,$email);
       $db->bind(3,$this->$accountID);
       $db->execute();

       $db = new database;
       $db->query("update ws_accounts set first_name = ?, surname = ?, email = ? where id = ?");
       $db->bind(1,$firstname);
       $db->bind(2,$surname);
       $db->bind(3,$email);
       $db->bind(4,$this->$accountID);
       $db->execute();

       $db = new database;
       $db->query("update ws_accounts_extra_info set mobile_number = ?, telephone = ? where account_id = ? and status = 1");
       $db->bind(1,$mobile);
       $db->bind(2,$telephone);
       $db->bind(3,$this->$accountID);
       $db->execute();

       return array('Account'=>'Update Successful');
     }

     //Delete

     public function deleteDocument($data){
       $doc_id = $data['id'];
       $db = new database;
       $db->Query("update ws_accounts_upload set status = 0 where id = ? and account_id = ?");
       $db->bind(1,$doc_id);
       $db->bind(2,$this->$accountID);
       $db->execute();

       return array('Account'=>'Document Deleted');
     }

     public function updateMyRoles($data){
       $db->query("update ws_accounts_roles set status = 0 where account_id = ?");
       $db->bind(1,$this->$accountID);
       $db->execute();

       $job_role = $data['job_role'];
       foreach ($job_role as $jb) {
         $db->Query("INSERT INTO ws_accounts_roles (id, role_id, account_id) VALUES (?,?,?)");
         $db->bind(1,createid('a_role'));
         $db->bind(2,$jb);
         $db->bind(3,$this->$accountID);
         $db->execute();
       }
       return array('Account'=>'Roles Updated Successful');
     }

     public function updateMyLocation($data){
       $db->query("update ws_accounts_locations set status = 0 where account_id = ?");
       $db->bind(1,$this->$accountID);
       $db->execute();

       $job_role = $location['location'];
       foreach ($location as $loc) {
         $db->Query("INSERT INTO ws_accounts_locations (id, location_id, account_id) VALUES (?,?,?)");
         $db->bind(1,createid('a_loc'));
         $db->bind(2,$loc);
         $db->bind(3,$this->$accountID);
         $db->execute();
       }
       return array('Account'=>'Locations Updated Successful');
     }

 }

$bear = getBearerToken();

 if (isset($_GET['action'])) {

     $token = $bear;
     $doc_type = $_POST['doc_type'];
     $update_data = $_POST['data'];


     $user = new HiveXAPIAccounts($token);

     $action = $_GET['action'];

     if($_SERVER['REQUEST_METHOD'] === 'POST'){
       switch ($action) {
         case 'update':
           $return_data = $user->updateAccountInfo($update_data);
           break;
           case 'deletedocument':
           $return_data = $user->deleteDocument($update_data);
           break;

           case 'updatemyroles':
           $return_data = $user->updateMyRoles($update_data);
           break;
           case 'updatemylocation':
           $return_data = $user->updateMyLocation($update_data);
           break;
         default:
          $return_data = array('error'=>'Error with Request Method '.$_SERVER['REQUEST_METHOD']);
           break;
       }
     }else if($_SERVER['REQUEST_METHOD'] === 'GET'){
       switch ($action) {
         case 'getinfo':
           $return_data = $user->getInfo();
           break;
         case 'getroles':
           $return_data = $user->getRoles();
           break;
         case 'getlocation':
           $return_data = $user->getLocations();
           break;
         case 'dashboardupcomingevents':
           $return_data = $user->dashboardUpcomingEvents();
           break;
         case 'dashboardpreviousevents':
           $return_data = $user->dashboardPreviousEvents();
           break;
         case 'mydocumentlist':
           $return_data = $user->myDocumentList();
           break;
         case 'mydocumentsall':
           $return_data = $user->myDocumentsAll();
           break;
         case 'mydocumentstype':
           $return_data = $user->myDocumentsType($_GET['doc_type']);
           break;
         case 'mydocumentsid':
           $return_data = $user->myDocumentsID();
           break;
         case 'getmyinvoices':
           $return_data = $user->getMyInvoices();
           break;
         default:
          $return_data = array('error'=>'Error with Request Method '.$_SERVER['REQUEST_METHOD']);
           break;
       }
     }else{
       $return_data = array('error'=>'Error with Request Method '.$_SERVER['REQUEST_METHOD']);
     }

      // switch ($action) {
      //   case 'getinfo':
      //     $return_data = $user->getInfo();
      //     break;
      //   case 'getroles':
      //     $return_data = $user->getRoles();
      //     break;
      //   case 'getlocation':
      //     $return_data = $user->getLocations();
      //     break;
      //   case 'dashboardupcomingevents':
      //     $return_data = $user->dashboardUpcomingEvents();
      //     break;
      //   case 'dashboardpreviousevents':
      //     $return_data = $user->dashboardPreviousEvents();
      //     break;
      //   case 'mydocumentlist':
      //     $return_data = $user->myDocumentList();
      //     break;
      //   case 'mydocumentsall':
      //     $return_data = $user->myDocumentsAll();
      //     break;
      //   case 'mydocumentstype':
      //     $return_data = $user->myDocumentsType($_GET['doc_type']);
      //     break;
      //   case 'mydocumentsid':
      //     $return_data = $user->myDocumentsID();
      //     break;
      //   case 'getmyinvoices':
      //     $return_data = $user->getMyInvoices();
      //     break;
      //   case 'update':
      //     $return_data = $user->updateAccountInfo($update_data);
      //     break;
      //   default:
      //     break;
      // }


     echo json_encode($return_data);

 }else{

 }
