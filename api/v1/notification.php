<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/

require 'functions.php';
require 'auth_functions.php';

 header('Content-Type: application/json;charset=utf-8');
 header("HTTP/1.1 200 OK");
$_POST = $HTTP_RAW_POST_DATA = file_get_contents('php://input');
$_POST = json_decode($_POST,true);

 class HiveXAPINotification extends hiveXAPI{

   public function getMyNotifications(){
     $db = new database;
     $db->query("select * from ws_notifications where created_for = ? and status = ? and viewed = ? ORDER BY created_date desc");
     $db->bind(1,$this->$accountID);
     $db->bind(2,'1');
     $db->bind(3,'0');
     $notifications = $db->resultSet();

     $return_array = array();

     foreach ($notifications as $noti) {
       $db = new database;
       $db->query("select * from accounts where id = ?");
       $db->bind(1,$noti['created_by']);
       $account = $db->single();

       $not_array = array(
         'id'=>$noti['id'],
         'created_by'=>$account['name'],
         'created_date'=>$noti['created_date'],
         'pid'=>$noti['pid'],
         'title'=>$noti['title'],
         'message'=>$noti['message'],
         'type'=>$noti['type']
       );

       array_push($return_array, $not_array);
     }

     return $return_array;
   }

   public function getMySingleNotifications($id){
      $db = new database;
      $db->query("select * from ws_notifications where id = ?");
      $db->bind(1,$id);
      $notifications = $db->single();

      $db = new database;
      $db->query("select * from accounts where id = ?");
      $db->bind(1,$notifications['created_by']);
      $account = $db->single();

      $not_array = array(
        'id'=>$notifications['id'],
        'created_by'=>$account['name'],
        'created_date'=>$noti['created_date'],
        'pid'=>$notifications['pid'],
        'title'=>$notifications['title'],
        'message'=>$notifications['message'],
        'type'=>$notifications['type']
      );

      return $not_array;
   }

 }


 $bear = getBearerToken();

 error_log('********************************************');
 error_log('********************************************');
 error_log('********************************************');
 error_log($_SERVER['REQUEST_METHOD']);

 if (isset($_GET['action'])) {

     $token = $bear;

     $notifications = new HiveXAPINotification($token);

     $action = $_GET['action'];
     $type = $_GET['type'];
     $id = $_GET['id'];
     $date_from = $_POST['start_date'];
     $date_to = $_POST['end_date'];

     $post_data = $_POST['data'];

     if($_SERVER['REQUEST_METHOD'] === 'GET'){
        switch ($action) {
          case 'getallnotifications':
            $return_data = $notifications->getMyNotifications();
            break;
          case 'getsinglenotification':
            $return_data = $notifications->getMySingleNotifications($id);
            break;
          default:
            $return_data = array('error'=>'Error with Request Method');
            break;
        }
     }else if($_SERVER['REQUEST_METHOD'] === 'POST'){
        switch ($action) {
          default:
            $return_data = array('error'=>'Error with Request Method');
            break;
        }
     }else{
        $return_data = array('error'=>'Error with Request Method');
     }

      // switch ($action) {
      //   case 'getallnotifications':
      //     $return_data = $notifications->getMyNotifications();
      //     break;
      //   case 'getsinglenotification':
      //     $return_data = $notifications->getMySingleNotifications($id);
      //     break;
      //   default:
      //     break;
      // }


     echo json_encode($return_data);

 }else{

 }
