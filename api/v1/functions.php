<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 include($_SERVER['DOCUMENT_ROOT'].'/application.php');


 class hiveXAPI{

    private $accountToken = 'empty';
    private $accountID = 'empty';


    public function __construct($token) {
        $db = new database;
        $db->query("select * from api_logged_in where token = ?");
        $db->bind(1,$token);
        $member = $db->single();

        if($member['expiry_date'] > time()){
            $this->setToken($token);
            if(!empty($member)){
                $this->setAccountID($member['account_id']);
            }
        }else{
            throw new Exception( "Oh no, all is going wrong! Abort!" );
            exit();
            //1619952990
        }


    }

    public function setToken($token){
      $this->$accountToken = $token;
    }

    public function displayToken(){
      echo $this->$accountToken;
    }


    public function setAccountID($account){
        $this->$accountID = $account;
    }

    public function checkToken(){

    }

    public function getServiceSingle($id){
        $db = new database;
      $db->query("select * from ws_service where id = ?");
      $db->bind(1,$id);
      $data = $db->single();

      return $data;
    }

    public function getRotaBidSingle($id){
        $db = new database;
      $db->query("select * from ws_rota_bid where id = ?");
      $db->bind(1,$id);
      $data = $db->single();

      return $data;
    }

    public function getRotaBidRequestSingle($id){
        $db = new database;
        $db->query("select * from ws_rota_bid_request where id = ?");
        $db->bind(1,$id);
        $data = $db->single();

        return $data;
    }

    public function getRotaBidServiceRoleSingle($service_id, $role_id){
        $db = new database;
        $db->query("select * from ws_service_roles where pid = ? and job_role = ? and status = 1");
        $db->bind(1,$service_id);
        $db->bind(2,$role_id);
        $data = $db->single();

        return $data;
    }

    public function getRolesSingle($id){
        $db = new database;
        $db->query("select * from ws_roles where id = ?");
        $db->bind(1,$id);
        $data = $db->single();

        return $data;
    }

    public function getPCNSingle($id){
        $db = new database;
        $db->query("select * from ws_pcn where id = ?");
        $db->bind(1,$id);
        $data = $db->single();

        return $data;
    }

    public function getPracticeSingle($id){
        $db = new database;
        $db->query("select * from ws_practice where id = ?");
        $db->bind(1,$id);
        $data = $db->single();

        return $data;
    }

    public function sendNotification($to,$pid,$title,$message,$type = 'notification'){
        $db = new database;
        $db->Query("insert into ws_notifications (id, created_for, created_by, created_date, title, message, type, status, pid) values (?,?,?,?,?,?,?,?,?)");
        $db->bind(1,createid('not'));
        $db->bind(2,$to);
        $db->bind(3,$this->$accountID);
        $db->bind(4,time());
        $db->bind(5,$title);
        $db->bind(6,$message);
        $db->bind(7,$type);
        $db->bind(8,'1');
        $db->bind(9,$pid);
        $db->execute();
    }

 }
