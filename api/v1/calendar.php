<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/



 require 'functions.php';
 require 'auth_functions.php';

  header('Content-Type: application/json;charset=utf-8');
  header("HTTP/1.1 200 OK");
 $_POST = $HTTP_RAW_POST_DATA = file_get_contents('php://input');
 $_POST = json_decode($_POST,true);

 class HiveXAPICalendar extends hiveXAPI{

   public function getCalendarMonth($date_from = null,$date_to = null){

     $extra = $this->$accountID;
     $id_search = ' and hx_rota.member_id = "'.$extra.'" and hx_rota. start_ts > "'.strtotime(date('d-m-Y 23:59:59', strtotime('yesterday'))).'"';
     $date_search = '';

     if(empty($date_from)){
       $date_search .= ' and hx_rota.start_ts >= '.strtotime($date_from).'';
     }

      if(empty($date_to)){
        $date_search .= ' and hx_rota.start_end >= '.strtotime($date_to).'';
      }

      $db = new database;
     $db->query("select hx_rota.*, ws_accounts.first_name, ws_accounts.surname, hx_rota.id as rota_id
     from hx_rota left join ws_accounts on ws_accounts.id = hx_rota.member_id
     where hx_rota.status = 1 $id_search $date_search");
     $data = $db->resultset();

     return $data;
   }

   public function getCalendarEvent($id){
     //This would be a single one.
     $db = new database;
     $db->query("select * from hx_rota where id = ?");
     $db->bind(1,$id);
     $rota = $db->single();

     error_log($rota['start_date']);

     $db = new database;
     $db->query("select * from ws_roles where id = ?");
     $db->bind(1,$rota['role']);
     $role = $db->single();

     $return_array = array(
       'id' => $rota['id'],
       'start_date' => $rota['start_date'],
       'end_date' => $rota['end_date'],
       'start_time' => $rota['start_time'],
       'end_time' => $rota['end_time'],
       'start_ts' => $rota['start_ts'],
       'end_ts' => $rota['end_ts'],
       'role' => $role['title'],
       'location' => $rota['location'],
       'did_not_attend' => $rota['dna'],
       'did_not_attend_reason' => $rota['dna_reason']
      );

      return $return_array;

   }

   public function didNotAttned($data){
     $id = $data['id'];
     $reason = $data['reason'];

     $db = new database;
     $db->Query("update hx_rota set dna = ?, dna_reason = ?, dna_date = ? where id = ?");
     $db->bind(1,'1');
     $db->bind(2,$reason);
     $db->bind(3,time());
     $db->bind(4,$id);
     $db->execute();

     $db->query("select * from hx_rota where id = ?");
     $db->bind(1,$id);
     $my_rota = $db->single();

     $service_id = $my_rota['service_id'];
     $service_role_id = $my_rota['service_role_id'];
     $bid_id = $my_rota['bid_id'];
     $bid_request_id = $my_rota['bid_request_id'];
     $account_id = $my_rota['member_id'];

     $db = new database;
     $db->Query("update ws_rota_bid set completed = ? where id = ?");
     $db->bind(1,'2');
     $db->bind(2,$bid_id);
     $db->execute();

     $db = new database;
     $db->Query("update ws_rota_bid_amount set completed = 0 where pid = ? and service_role_id = ?");
     $db->bind(1,$bid_id);
     $db->bind(2,$service_role_id);
     $db->execute();

     $db = new database;
     $db->Query("update ws_rota_bid_request set accepted_user = 3 where id = ? and account_id = ?");
     $db->bind(1,$bid_request_id);
     $db->bind(2,$account_id);
     $db->execute();

     return array('calendar'=>'Shift Marked as Did Not attend');
   }

   public function didAttned($data){
     $db = new database;
     $db->Query("update hx_rota set dna = ?, dna_reason = ?, dna_date = ? where id = ?");
      $db->bind(1,'0');
      $db->bind(2,'');
      $db->bind(3,NULL);
      $db->bind(4,$data['id']);
     $db->execute();

     return array('calendar'=>'Shift Marked as Did attend');
   }

 }

$bear = getBearerToken();

 if (isset($_GET['action'])) {

     $token = $bear;



     $shifts = new HiveXAPICalendar($token);

     $action = $_GET['action'];
     $type = $_GET['type'];
     $id = $_GET['id'];
     $date_from = $_POST['start_date'];
     $date_to = $_POST['end_date'];

     $post_data = $_POST['data'];

     if($_SERVER['REQUEST_METHOD'] === 'GET'){
       switch ($action) {
         case 'getcalendarmonth':
           $return_data = $shifts->getCalendarMonth($date_from,$date_to);
           break;
         case 'getcalendarevent':
           $return_data = $shifts->getCalendarEvent($id);
           break;
         default:
           $return_data = array('error'=>'Error with Request Method');
           break;
       }
     }else if($_SERVER['REQUEST_METHOD'] === 'POST'){
       switch ($action) {
           case 'didnotattned';
           $return_data = $shifts->didNotAttned($post_data);
           break;
           case 'didattned';
           $return_data = $shifts->didAttned($post_data);
           break;
         default:
           $return_data = array('error'=>'Error with Request Method');
           break;
       }
     }else{
        $return_data = array('error'=>'Error with Request Method');
     }

      // switch ($action) {
      //   case 'getcalendarmonth':
      //     $return_data = $shifts->getCalendarMonth($date_from,$date_to);
      //     break;
      //   case 'getcalendarevent':
      //     $return_data = $shifts->getCalendarEvent($id);
      //     break;
      //     case 'didnotattned';
      //     $return_data = $shifts->didNotAttned($post_data);
      //     break;
      //     case 'didattned';
      //     $return_data = $shifts->didAttned($post_data);
      //     break;
      //   default:
      //     break;
      // }


     echo json_encode($return_data);

 }else{

 }
