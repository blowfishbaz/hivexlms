<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $page_title = 'Reset Password';
$now = time();
$time=$_GET['d']+3600;

if(!isset($_GET['code'])&&!isset($_GET['d'])){ header('Location: '.$fullurl.'index.php'); }
if($time<=$now){ header('Location: '.$fullurl.'index.php');}

$db->query("SELECT * FROM ws_accounts_reset where pid= ? and status = ? order by id desc limit 1");
$db->bind(1,decrypt($_GET['code']));
$db->bind(2,'1');
$reset = $db->single();
$count = $db->rowcount();


if ($count == 0) {header('Location: '.$fullurl.'index.php');}
if($reset['created_date']!=$_GET['d']){header('Location: '.$fullurl.'index.php');}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Password Reset</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="<? echo $fullurl ?>/staff-box/css/style.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="icon" href="<? echo $fullurl ?>/staff-box/images/Mini-Icon.png" type="image/png">
</head>
<body>

<div id="loginpage">
<div class="container">


<div id="mySidepanelrightnew" class="sidepanelrightnew">

<div class="logo">
<a href="index.html"><img src="<? echo $fullurl ?>/staff-box/images/Staff-Box-Logo.png" /></a>
</div>



<div class="loginleft">
  <form id="form-reset" class="form-reset" enctype="application/x-www-form-urlencoded" method="post">

    <div class="form-group label-floating is-empty">
    <label for="new_password" class="control-label">New Password</label>
    <p class="input_error_message">must be between 6 and 16 characters, contain 1 uppercase and lowercase letter and contain 1 number <br />may contain special characters like !@#$%^&*()_+</p>
    <input  id="new_password"type="password" class="form-control widthnew" id="new_password" name="new_password" value="" required="yes"><span class="material-input"></span></div>

     <div class="form-group label-floating is-empty">
              <label for="confirm_new_password" class="control-label">Confirm New Password</label>
              <p class="input_error_message">must be same as new password</p>
              <input  id="confirm_new_password" type="password" class="form-control widthnew" id="confirm_new_password" name="confirm_new_password" value="" required="yes">
              <input type="hidden" name="id" value="<?echo $reset['id'];?>"/>
              <input type="hidden" name="key" id="key" value="<? echo getCurrentKey(); ?>">
              <span class="material-input"></span>
        </div>

<button type="button" class="formbtn" id="Reset_Password">Reset Password</button><br>

</form>



</div>


<div class="loginright">
<img src="<? echo $fullurl ?>/staff-box/images/11.png" />
</div>


</div>





</div>
</div>







</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
$fullurl = '<?echo $fullurl;?>';
function email($input) {
  $input = $input.trim();
  var isAlphNum = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($input);
  var deciCount = $input.replace(/[^'.']/g,"").length;
  var atCount = $input.replace(/[^'@']/g,"").length;
  //if((atCount ==1) && (deciCount >=1) && (isAlphNum)){
if ((atCount ==1 && deciCount >=1 && isAlphNum) && $input.length!=0){return  0;}
  else{return 1;}
}

function password($input,$min,$max) {
  if ($input.length < $min) {return 1;}
  else if ($input.length > $max) {return 1;}
  else if ($input.search(/\d/) == -1) {  return 1;}
  else if ($input.search(/[a-z]/) == -1) {return 1;}
  else if ($input.search(/[A-Z]/) == -1) {return 1;}
  else if ($input.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {return 1;}
  else{return 0;}
}

$(document).ready(function() {
  $( "body" ).on( "click", "#Reset_Password", function() {

    var formid = '#form-reset';
    var $error = 0;

    if(password($('#new_password').val(),6,16)){
      $error=1;
      $('#new_password').parent().addClass('has-error');
    }
    if($('#new_password').val()!=$('#confirm_new_password').val()){
      $error=1;
      $('#confirm_new_password').parent().addClass('has-error');
    }
  if ($error == 1) {
    // Messenger().post({
    //     message: 'Please make sure all required elements of the form are filled out.',
    //     type: 'error',
    //     showCloseButton: false
    // });
    alert('Please make sure all required elements of the form are filled out');
  } else {

        $(formid).hide('slow');
        // $('.loader').html($Loader);
        $('.loader').show('slow');

        var FRMdata = $(formid).serialize(); // get form data
          $.ajax({
                type: "POST",
                url: $fullurl+"assets/app_ajax/website/reset_password.php",
                data: FRMdata,
                success: function(msg){

                  $message=$.trim(msg);

                  if($message=='ok'){
                      alert('Password has been reset');
                      setTimeout(function(){
                        window.location.replace($fullurl+'login.php');
                      },1000);
                }else{
                  setTimeout(function(){
                      $(formid).show('slow');
                      $('.loader').hide('slow');
                      alert('Error has occurred');
                   },600);
                }
               },error: function (xhr, status, errorThrown) {
                     setTimeout(function(){
                         $(formid).show('slow');
                         $('.loader').hide('slow');
                         alert('Error has occurred');
                      },600);
                    }
          });
      }
  });
});



</script>
