<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

  $new_member_id = createid('member');

  $db->query("select title,id from ws_roles where status = 1 order by title asc");
  $roles = $db->resultset();

  $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);


   $db = new database;
   $db->query("select * from accounts where id = ? ");
   $db->bind(1,$uid);
   $user = $db->single();

   if($user['account_type'] == 'user'){
     $db->query("select * from ws_accounts where pid = ? and id = ?");
     $db->bind(1,$uid);
     $db->bind(2,$uid);
     $user_ws = $db->single();
   }

   if(!empty($uid)){
     $new_member_id = $uid;
   }

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sign Up</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="icon" href="<? echo $fullurl ?>/staff-box/images/Mini-Icon.png" type="image/png">

 <link href=" <? echo $fullurl ?>assets/css/select.css" rel="stylesheet">

 <!-- Messenger -->
<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>

 	<link rel="stylesheet" type="text/css" href="<? echo $fullurl ?>/staff-box/css/style.css" />
</head>


  <style>
  body{
    background-image: none;
  }

  .container{
  max-width:1920px!important;
  width:auto!important;
  margin:0px auto;
  /* padding-left:15px;
  padding-right:15px; */
  }

  .btn-group.bootstrap-select {
      width: 45%!important;
  }

  button.btn.dropdown-toggle.selectpicker.btn-default {
    padding: 14px;
    border-radius: 15px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    color: #46bbc7;
}

.cert_upload_holder{
  margin-bottom:10px;
}

  </style>
<body>







<div id="loginpage">
<div class="container">


<div id="mySidepanelrightnew" class="sidepanelrightnew">

<div class="logo">
<a href="index.php"><img src="<? echo $fullurl ?>/staff-box/images/Staff-Box-Logo.png" /></a>
</div>



<div class="loginleft" style="width:100%;">
 <form id="register_rota_form">
   <input type="hidden" name="rota_id" value="<?echo $new_member_id;?>" />
   <input type="hidden" name="key_new" id="key_new" value="<? echo getCurrentKey(); ?>">
  <div class="rota_register_page_one" <?if(!empty($user_ws['status'])){echo 'style="display:none;"';}?>>
  <h1>Sign Up</h1>

  <div class="form-group label-floating is-empty">
    <label for="first_name"><b>First name*</b></label><br><p class="input_error_message">Please Enter your First Name</p>
    <input type="text" class="widthnew" placeholder="First Name" name="first_name" id="rota_first_name" required><br>
  </div>
  <div class="form-group label-floating is-empty">
    <label for="surname"><b>Surname*</b></label><br><p class="input_error_message">Please Enter your Surname</p>
    <input type="text" class="widthnew" placeholder="Surname" name="surname" id="rota_surname" required><br>
  </div>
  <div class="form-group label-floating is-empty">
    <label for="email"><b>Email*</b></label><br><p class="input_error_message">Please Enter your Email</p>
    <input type="email" class="widthnew" placeholder="Email" name="email" id="rota_email" required><br>
  </div>
  <div class="form-group label-floating is-empty">
    <label for="password"><b>Password*</b></label><br><p class="input_error_message">must be between 6 and 16 characters, contain 1 uppercase and lowercase letter and contain 1 number <br />may contain special characters like !@#$%^&amp;*()_+</p>
    <input type="password" class="widthnew" placeholder="Password" name="password" id="rota_password" required><br>
  </div>
  <div class="form-group label-floating is-empty">
    <label for="telephone"><b>Telephone*</b></label><br>
    <input type="text" class="widthnew" type="text" placeholder="Telephone" name="telephone" id="rota_telephone" required><br>
  </div>
  <div class="form-group label-floating is-empty">
    <label for="mobile_number"><b>Mobile Number*</b></label><br>
    <input type="text" class="widthnew" type="text" placeholder="Mobile Number" name="mobile_number" id="rota_mobile_number" required><br>
  </div>
  <div class="form-group label-floating is-empty">

    <label><b>Job Role*</b></label><br />
    <select name="job_role[]" data-live-search="true" class="selectpicker" id="job_role" required multiple>
      <?foreach ($roles as $r) {?>
        <option value="<?echo $r['id'];?>"><?echo $r['title'];?></option>
      <?}?>
  </select><br />
</div>
<div class="form-group label-floating is-empty">

  <label><b>Locations*</b></label><br />
  <select name="location[]" class="form-control selectpicker" data-live-search="true" id="location" required multiple>
    <option value="Wirral">Wirral</option>
    <option value="Sefton">Sefton</option>
    <option value="Liverpool">Liverpool</option>
</select><br /><br /><br />

<div class="form-group label-floating is-empty">
  <label for="bank_name"><b>Bank Name</b></label><br>
  <input type="text" class="widthnew" type="text" placeholder="Bank Name" name="bank_name" id="rota_bank_name"><br>
</div>

<div class="form-group label-floating is-empty">
  <label for="account_number"><b>Account Number</b></label><br>
  <input type="number" class="widthnew" type="text" placeholder="Account Number" name="account_number" id="rota_account_number"><br>
</div>

<div class="form-group label-floating is-empty">
  <label for="sort_code"><b>Sort Code</b></label><br>
  <input type="number" class="widthnew" type="text" placeholder="Sort Code (123456)" name="sort_code" id="rota_sort_cide"><br>
</div>

<div class="form-group label-floating is-empty">
  <input type="checkbox" id="agreement" name="agreement">
  <label for="agreement">I agree to the <a href="privacy-policy.php" target="_blank">Terms and Conditions</a> &amp; <a href="terms-of-use.php" target="_blank">Privacy Policy</a></label>
</div>
</div>



  <button type="button" class="formbtn" id="save_continue_rota">Save to Continue</button><br>

</div>
<div class="rota_register_page_two" <?if($user_ws['status'] != 2){echo 'style="display:none;"';}?>>
  <p>From the roles you have selected you must upload the relevent documents.<br />Click the upload button for each document below and upload your relevent documentaion</p>


  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder bls_certificate_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="bls_certificate" id="button_bls_certificate">BLS Certificate - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder enhanced_dbs_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="enhanced_dbs" id="button_enhanced_dbs">Enhanced DBS - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder university_qualifications_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="university_qualifications" id="button_university_qualifications">University Qualifications - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder safeguarding_adults_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="safeguarding_adults" id="button_safeguarding_adults">Safeguarding Adults - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder safeguarding_children_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="safeguarding_children" id="button_safeguarding_children">Safeguarding Children - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder cv_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="cv" id="button_cv">CV - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder hep_b_certificate_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="hep_b_certificate" id="button_hep_b_certificate">Hep B Certificate - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder photo_id_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="photo_id" id="button_photo_id">Photo ID - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder university_qualification_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="university_qualification" id="button_university_qualification">University Qualification - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder phlebotomy_training_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="phlebotomy_training" id="button_phlebotomy_training">Phlebotomy Training - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder hep_b_cert_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="hep_b_cert" id="button_hep_b_cert">Hep B Cert - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder reference_from_university_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="reference_from_university" id="button_reference_from_university">Reference from University - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder gp_cct_mrcgp_jcptgp_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="gp_cct_mrcgp_jcptgp" id="button_gp_cct_mrcgp_jcptgp">GP (CCT/MRCGP/JCPTGP) - Upload</button>
  </div>


  <!-- Paramedic -->
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder bsc_training_prog_coll_para_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="bsc_training_prog_coll_para" id="button_bsc_training_prog_coll_para">BSc in training programme approved by College of Paramedics - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder health_and_care_profession_council_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="health_and_care_profession_council" id="button_health_and_care_profession_council">Health and Care Professions Council (HCPC) registration - Upload</button>
  </div>

  <!-- Care Co-ordinator -->

  <!-- Nurse Supervisor -->
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder registered_general_nurse_q_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="registered_general_nurse_q" id="button_registered_general_nurse_q">Registered General Nurse qualification - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder nmc_registration_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="nmc_registration" id="button_nmc_registration">NMC registration - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder bsc_advanced_clinical_practice_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="bsc_advanced_clinical_practice" id="button_bsc_advanced_clinical_practice">BSC in advanced clinical practice (Level 7) <br />or RCN Credentialling qualification (this should include clinical skills and diagnostics)   - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder ind_prescriber_qual_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="ind_prescriber_qual" id="button_ind_prescriber_qual">Independent Prescriber qualification (V300) - Upload</button>
  </div>

  <!-- Clinical Pharmacist -->
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder general_phara_council_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="general_phara_council" id="button_general_phara_council">Mandatory registration with General Pharmaceutical Council (GPC) - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder masters_in_pharmacy_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="masters_in_pharmacy" id="button_masters_in_pharmacy">Masters degree in pharmacy (MPharm) (or equivalent) - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder specialist_through_post_grad_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="specialist_through_post_grad" id="button_specialist_through_post_grad">Specialist knowledge acquired through post <br />graduate diploma level or equivalent training/experience - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder independent_presriber_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="independent_presriber" id="button_independent_presriber">Independent prescriber (preferred) - Upload</button>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder cli_min_post_experience_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="cli_min_post_experience" id="button_cli_min_post_experience">Minimum of 2 years post[1]qualification experience - Upload</button>
  </div>

  <!-- PHARMACY Technician -->
  <div class="col-sm-6 col-md-4 col-lg-4 cert_upload_holder proof_of_experience_phara_tech_div">
    <button type="button" class="btn btn-sm btn-raised btn-info upload_rota_documents" data-id="proof_of_experience_phara_tech" id="button_proof_of_experience_phara_tech">Proof of experience in pharmacy technician - Upload</button>
  </div>

  <div class="clearfix"></div>

    <button type="button" class="formbtn" id="save_complete_rota">Save &amp; Complete</button><br>
</div>

</form>













<div class="loginnnnn">
<p><img src="<? echo $fullurl ?>/staff-box/images/22.png" />Need help</p>

</div>
</div>



</div>





</div>
</div>







</body>
</html>

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


?>

<script src="<? echo $fullurl; ?>assets/js/select.js"></script>





<script>
$fullurl = '<? echo $fullurl ?>';


function limit_rota_roles($fullurl){

  $('.cert_upload_holder').hide();

  $.ajax({
      url: $fullurl+'assets/uploaders/rota_limit_uploader.php?id=<?echo $new_member_id;?>',
      type: 'GET',
      dataType: "json",
      success: function(msg) {
        console.dir(msg);
        $.map(msg, function(item) {
          console.log('.'+item+'_div');
          $('.'+item+'_div').show();
        });
      }
    });

  console.log($fullurl);

}

jQuery(document).ready(function($) {
  <?if($user_ws['status'] == 2){?>
    limit_rota_roles($fullurl);
  <?}?>

});

function password($input,$min,$max) {
  if ($input.length < $min) {return 1;}
  else if ($input.length > $max) {return 1;}
  else if ($input.search(/\d/) == -1) {  return 1;}
  else if ($input.search(/[a-z]/) == -1) {return 1;}
  else if ($input.search(/[A-Z]/) == -1) {return 1;}
  else if ($input.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {return 1;}
  else{return 0;}
}




$( "body" ).on( "click", ".support", function() {
       $('.warning').hide();
       $('.sentMessage').hide();
       document.getElementById("supportForm").reset();
       $('#supportModal').modal('show');
   //$('.panel-default').slideToggle();
});

$( "body" ).on( "click", ".training", function() {
       $('#training').modal('show');
   //$('.panel-default').slideToggle();
});

$( "body" ).on( "click", "#closeSupportForm", function() {
       $('#supportModal').modal('hide');
   //$('.panel-default').slideToggle();
});

$( "body" ).on( "click", "#tClose", function() {
       $('#training').modal('hide');
   //$('.panel-default').slideToggle();
});

$( document ).ready(function() {
   $().alert('close')
});



$( "body" ).on( "click", "#index_back_button", function() {
 $('.index_back').fadeOut(500);
 $('.index_login_wrapper').fadeOut(500);
 $('.index_register_wrapper_rota').fadeOut(500);

 $('.index_cards_wrapper').fadeIn(500);
});

$( "body" ).on( "click", ".index_cards", function() {
       var type = $(this).data('id');

       $('.index_register_wrapper_rota').fadeOut(500);
       $('.index_cards_wrapper').fadeOut(500);
       $('.index_login_wrapper').fadeOut(500);

       $('.index_back').fadeIn(500);

       $('.index_login_wrapper_'+type+'').fadeIn(500);
});




$( "body" ).on( "click", "#rota_register", function() {
$('.index_login_wrapper').fadeOut(200);
$('.index_register_wrapper_rota').fadeIn(400);
});


$( "body" ).on( "click", "#save_continue_rota", function() {
var formid = '#register_rota_form';
var HasError = 0;

if(!$('#agreement').is(":checked")){
  HasError = 1;
}

$(formid).find('input').each(function(){
$(this).parent().removeClass('has-error');
Messenger().hideAll();

if(!$.trim(this.value).length) { // zero-length string AFTER a trim
        if(!$(this).prop('required')){
   } else {
     HasError = 1;
     $(this).parent().addClass('has-error');
   }
 }
});

if(password($('#rota_password').val(),6,16)){
HasError=1;
$('#rota_password').parent().addClass('has-error');
}


if(HasError == 0){

var FRMdata = $(formid).serialize(); // get form data
$.ajax({
     type: "POST",
     url: $fullurl+"admin/signup.php",
     data: FRMdata,
     success: function(msg){
       $message=$.trim(msg);
       if($message=='email already exists'){
         alert('email already exists');
       }else if($message=='id already exists'){
         alert('id already exists');
       }else{
         setTimeout(function(){
           //window.location.replace($fullurl+'jobs.php');
           Messenger().post({
               message: 'Account Created.',
               type: 'error',
               showCloseButton: false
           });


           $email=$('#rota_email').val();
           $password=$('#rota_password').val();
           $key=$('#key_new').val();


           $.ajax({
                 type: "POST",
                 url: $fullurl+"assets/app_php/auth.php?action=logon",
                 data: {login:$email,password:$password,key:$key},
                 success: function(msg){
                     $message=$.trim(msg);
                     if($message=='acount'){
                       alert('acount');
                     }else if($message=='locked out'){
                       alert('locked out');
                     }else if($message=='fail'){
                       alert('fail');
                     }else{
                       $('.rota_register_page_one').hide();
                       $('.rota_register_page_two').show();
                       limit_rota_roles($fullurl);




                     }
                },error: function (xhr, status, errorThrown) {
                       setTimeout(function(){alert('Error');},300);
                     }
           });



         },1000);
       }

    },error: function (xhr, status, errorThrown) {
           Messenger().post({
               message: 'An error has occurred please try again.',
               type: 'error',
               showCloseButton: false
           });
         }
});
}else{
  Messenger().post({
      message: 'Please make sure all fields are filled out.',
      type: 'error',
      showCloseButton: false
  });
}

});



$( "body" ).on( "click", "#save_complete_rota", function() {
var formid = '#register_rota_form';
// alert('here');


var FRMdata = $(formid).serialize(); // get form data
$.ajax({
     type: "POST",
     url: $fullurl+"admin/final_signup_rota.php",
     data: FRMdata,
     success: function(msg){
       $message=$.trim(msg);
       if($message=='ok'){
         setTimeout(function(){
           //window.location.replace($fullurl+'jobs.php');
           Messenger().post({
               message: 'Account Created.',
               type: 'error',
               showCloseButton: false
           });
           window.location.replace($fullurl+'index2?account_waiting');
           //We show a message at the topo fo the page for the user so they know what is happening.
           //window.location.reload();
         },1000);
       }

    },error: function (xhr, status, errorThrown) {
           Messenger().post({
               message: 'An error has occurred please try again.',
               type: 'error',
               showCloseButton: false
           });
         }
});
});


$( "body" ).on( "click", ".upload_rota_documents", function() {
$("#BaseModalLContent").html($Loader);
$('#BaseModalL').modal('show');

var new_id = '<?echo $new_member_id;?>';
var document_type = $(this).data('id');

$.ajax({
  type: "POST",
  url: $fullurl+"admin/upload_documents.php",
  data: {'new_id':new_id,"document_type":document_type},
  success: function(msg){
    $("#BaseModalLContent").delay(1000)
     .queue(function(n) {
         $(this).html(msg);
         n();
     }).fadeIn("slow").queue(function(n) {
                  $.material.init();
                 n();
     });
    }
});
});


$( "body" ).on( "click", "#complete_doc_upload", function() {
  $('.modal').modal('hide');
  var id = $(this).data('id');

  $('#button_'+id+'').attr('disabled','disabled');
});


</script>







<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
