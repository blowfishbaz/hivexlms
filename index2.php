<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
 header('Location: '.$fullurl.'index.php');
 exit();
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Staff Box</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="<? echo $fullurl ?>/staff-box/css/style.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="icon" href="<? echo $fullurl ?>/staff-box/images/Mini-Icon.png" type="image/png">
</head>

<style>
.staff-box-footer{
	position: absolute;
    left: 100%;
    transform: translate(-120%, 100%);
		max-width:200px;
}

.collab_holder{
	width: 50%;
    margin: 0 auto;
}

.collab_holder_inner{
	width: 46%;
	    float: left;
	    background-color: #f0f0f0;
	    margin: 1%;
	    border-radius: 20px;
	    padding: 1%;
			min-height: 350px;
}

.collab_holder_inner p{
	width:98%;
	line-height: 30px;
}

.footer_half{
	width:50%;
	float:left;
	height:inherit;
	color:white;
}

.footer_half img{
	max-width: 220px;
}

@media screen and (max-width: 850px){
	.collab_holder_inner{
		float: none;
		width:100%;
		height:auto;
		min-height:0px;
		margin-top:20px;
	}

	.footer_half{
		float:none;
		width: 100%;
	}

	.footer_half p{
		text-align: center!important;
	}

	.footer_half ul{
		padding:25px!important;
	}

	.collab_holder{
		width:80%;
	}

	.footer_half img{
		max-width: 190px;
	}
}
</style>
<body>

<div id="site">
<div class="site-inner">

<div id="header">
<div class="container">
<div class="fullbanner">
<div class="leftside">
<div id="mySidepanel" class="sidepanel">
<a href="javascript:void(0)" class="closebtn" onClick="closeNav()">&times;</a><img class="right" src="<? echo $fullurl ?>/assets/images/mini_logo.png" />

<div class="navmenus">
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/pink.png" /><span class="menu1">rota</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/blue.png" /><span class="menu2">HR</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/green.png" /><span class="menu3">learn</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/yellow.png" /><span class="menu4">governance</span></a>
</div>
<div class="btmbtns">
<a href="/login.php" class="btn1">Log in</a>
<a href="/sign-up.php" class="btn2">Register</a>
</div>
</div>
<button class="openbtn" onClick="openNav()"><img src="<? echo $fullurl ?>/staff-box/images/toggle.png" /></button>
</div>


<div class="rightside">
<ul>
<li><a href="tel:01511234567"><i class="fa fa-phone" aria-hidden="true"></i> 0151 1234 567</a></li>
<li><a href="mailto:info@onewirral.co.uk"><i class="fa fa-envelope" aria-hidden="true"></i>info@onewirral.co.uk</a></li>
</ul>
</div>

<!--
<div id="mySidepanelright" class="sidepanelright">
<a href="javascript:void(0)" class="closebtn" onClick="closeNavright()"><img src="images/close.png" /></a>

<div class="logo">
<a href="index2.php"><img src="<? echo $fullurl ?>/staff-box/images/Staff-Box-Logo.png" /></a>
</div>



<div class="loginleft">
 <form action="action_page.php" method="post">
  <div class="container">
  <h1>Log in</h1>
    <label for="uname"><b>Username</b></label><br>
    <input class="widthnew" type="text" placeholder="Username" name="uname" required><br>

    <label for="psw"><b>Password</b></label><br>
    <input class="widthnew" type="password" placeholder="**************" name="psw" required><br>

    <button type="submit" class="formbtn">Log in</button><br>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me<br>
    </label>
  </div>
  <div class="container">
   <button type="button" class="cancelbtn">Cancel</button><br>
    <span class="psw"> <a href="#">Forgot password?</a></span><br>
  </div>
</form>


<div class="loginnnnn">
<p><img src="<? echo $fullurl ?>/staff-box/images/22.png" />Need help</p>
</div>


</div>


<div class="loginright">
<img src="<? echo $fullurl ?>/staff-box/images/11.png" />
</div>




</div> -->


<div style="clear:both;"></div>

<? if (isset($_GET['error'])) { ?>
<div class="alert alert-warning alert-dismissible" role="alert">
 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 Username or Password Incorrect.
</div>
<? } ?>
<? if (isset($_GET['account'])) { ?>
<div class="alert alert-warning alert-dismissible" role="alert">
 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 Account has been locked.
</div>
<? } ?>
<? if (isset($_GET['account_waiting'])) { ?>
  <div style="margin: 0 auto; display: table; background-color: pink; padding: 20px 10px; color: white; border-radius: 20px; text-align: center;">
  Your account is pending verification. Once your account has been verified you will receive an email.
</div>
<? } ?>

<div class="logo">
<a href="index2.php"><img src="<? echo $fullurl ?>/staff-box/images/logo.png" /></a>
<h1>Four platforms, all in one <b>box!</b></h1>
<p>brought to you by One Wirral CIC</p>
</div>


<div style="clear:both;"></div>
</div>


<div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
</div>




<div class="four-blocks">
<div class="container">

<div class="block1">
<img src="<? echo $fullurl ?>/staff-box/images/pink.png" />
<h2>rota</h2>
<a class="btnnew" href="/login.php?type=rota">Log in</a>
<a href="#rota">Learn More<img src="<? echo $fullurl ?>/staff-box/images/arrow.png" /></a>
</div>

<div class="block2">
<img src="<? echo $fullurl ?>/staff-box/images/blue.png" />
<h2>HR</h2>
<a class="btnnew" href="/login.php?type=hr">Log in</a>
<a href="#hr">Learn More<img src="<? echo $fullurl ?>/staff-box/images/arrow.png" /></a>
</div>

<div class="block3">
<img src="<? echo $fullurl ?>/staff-box/images/green.png" />
<h2>learn</h2>
<a class="btnnew" href="/login.php?type=learn">Log in</a>
<a href="#Learn">Learn More<img src="<? echo $fullurl ?>/staff-box/images/arrow.png" /></a>
</div>

<div class="block4">
<img src="<? echo $fullurl ?>/staff-box/images/yellow.png" />
<h2>governance</h2>
<a class="btnnew" href="/login.php?type=governance">Log in</a>
<a href="#Governanace">Learn More<img src="<? echo $fullurl ?>/staff-box/images/arrow.png" /></a>
</div>


<div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
</div>


<div class="blackbtn">
<a href="#">Arrange a Demo</a>
</div>


<div id="rota">
<div class="container">
<div class="rotabg">

<div class="leftalign">
<h1>Rota</h1>
<p>A comprehensive staffing solution for a wide variety of Clinical Services such as; GP Practices, Hospitals, Outpatient or Rehabilitation Facilities, Vaccination Centres, Care Homes and Domiciliary Care settings. As a service; you can publish multiple roles available to source an agile and fully qualified workforce, as well as having full advantages of our automated invoicing system and complete integration to the other Staffbox Platforms.

As a user; you can apply for specific roles in multiple locations and have complete control and flexibility over your working hours. </p>
<a href="#">Register</a>
</div>

<div class="rightalign">
<img src="<? echo $fullurl ?>/staff-box/images/pink-rou.png" />
</div>

<div style="clear:both;"></div>
</div>
</div>
</div>



<div id="hr">
<div class="container">
<div class="rotabgright">

<div class="leftalign">
<h1>HR</h1>
<p>A complete solution for your Human Resources department to store and share company documentation, policies and procedures and much more! Staffbox HR allows Staff to book Holidays and Annual Leave, as well as keep their cross-platform Profiles up to date with a single document upload. Services can also send correspondence to Staff, receive notifications when Staff are due DBS Checks, expired Training and Qualifications, as well as manage sickness and absences. </p>
<a href="#">Register</a>
</div>

<div class="rightalign">
<img src="<? echo $fullurl ?>/staff-box/images/blue-r.png" />
</div>

<div style="clear:both;"></div>
</div>
</div>
</div>





<div id="Learn">
<div class="container">
<div class="rotabg">

<div class="leftalign">
<h1>Learn</h1>
<p>A simple, clean and user-friendly eLearning Platform that promotes a blended learning style. Staffbox Learn provides in depth content, condensed in to short 30-60 minute modules. It is an information access point, as well as an online hub for learning, attending or hosting webinars and micro-teach sessions. Our fully integrated training matrix will allow services to keep employee records up to date, increase productivity and decrease the time Staff spend in the classroom. </p>
<a href="#">Register</a>
</div>

<div class="rightalign">
<img src="<? echo $fullurl ?>/staff-box/images/green-r.png" />
</div>

<div style="clear:both;"></div>
</div>
</div>
</div>




<div id="Governanace">
<div class="container">
<div class="rotabgright">

<div class="leftalign">
<h1>Governanace</h1>
<p>A perfect solution for service management to ensure full compliance with CQC regulations. Follow the online guidance and policy management of Staffbox Governance to ensure you are reaching all targets set by CQC to enable an ‘Outstanding’ CQC inspection report. </p>
<a href="#">Register</a>
</div>

<div class="rightalign">
<img src="<? echo $fullurl ?>/staff-box/images/yellow-r.png" />
</div>

<div style="clear:both;"></div>
</div>
</div>
</div>





<div class="ourpartners">
<div class="container">
<h1>Our Partners</h1>
<p>Staffbox are proud partners of One Wirral CIC and NHS Merseyside, bringing you a simple yet complete and comprehensive and staffing solution to cater to all your recruitment needs … All in one delightfully colourful box! </p>
<img src="<? echo $fullurl ?>/staff-box/images/onelogo.png" /><img src="<? echo $fullurl ?>/staff-box/images/NHS.png" />
<div style="clear:both;"></div><BR /><BR />
<p>One Wirral collaborate with a wide range of partnets across the Wirral.<br />This is constantly growing. theses include;</p>

<div class="collab_holder">
	<div class="collab_holder_inner">
		<strong>Voluntary Community Sector</strong>
		<p>
			Health Junction<br />
			Wirral change<br />
			Acts Kindness charity<br />
			Grow CIC<br />
			The Maggie's Centre<br />
			The Conservation Colunteers<br />
			Change Grow Live<br />
			Health Watch<br />
			Mencap<br />
		</p>
	</div>
	<div class="collab_holder_inner">
		<strong>Heatlh Care</strong>
		<p>
			Wirral University Teaching Hospital<br />
			Wirral Leisure (Inlcuding we are undefeatable project)<br />
			Primary Care Wirral<br />
			Merseyside and Cheshire Cancel Alliance<br />
			NHSE<br />
		</p>
	</div>
</div>



</div><div style="clear:both;"></div></div>



<div style="clear:both;"></div>

<div class="footer" style="min-height:100px; background-color:black;">

	<div class="footer_half">
		<ul style="padding:50px;">
			<li style="margin-bottom:20px;"><img src="<? echo $fullurl ?>/staff-box/images/email_white.png" style="float:left;" /> <a href="mailto:info@onewirral.co.uk" style="color:white; padding-left:20px;"> info@onewirral.co.uk</a><div style="clear:both;"></div></li>
			<li style="margin-bottom:20px;"><img src="<? echo $fullurl ?>/staff-box/images/phone_white.png" style="float:left;" /> <p style="float:left; padding-left:20px;">0151 294 3322</p><div style="clear:both;"></div></li>
			<li><img src="<? echo $fullurl ?>/staff-box/images/location_white.png" style="float:left;" /> <p style="float:left; padding-left:20px;"> One Wirral CIC, Orchard Surgery, Brombrough Village Rd, <br />Brombrough, CH62 7EU</p><div style="clear:both;"></div></li>
		</ul>
	</div>
	<div class="footer_half">

		<p style="text-align:right;margin-top: 10px; margin-right: 20px;"><img src="<? echo $fullurl ?>/staff-box/images/Staff_Box_Logo_White.png" class="xstaff-box-footer" /> <br /> &copy; Staffbox Copyright <?echo date('Y');?> </p>
		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>
  <div style="padding-bottom:20px;">
		<div style="margin:0 auto; display:table;">
			<a href="#" style="color:white; padding-right:20px;">Privacy Policy</a>
			<a href="#" style="color:white;">Terms of Use</a>
		</div>
  </div>



<div style="clear:both;"></div>
<div style="width:100%;color:white;">

</div>
<div style="clear:both;"></div>
</div>







</div>
</div>





</div>
</div>
</body>
</html>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>





<script>
var width = $(window).width();

if(width > 500){
  function openNav() {
    document.getElementById("mySidepanel").style.width = "350px";
  }
}else{
  function openNav() {
    document.getElementById("mySidepanel").style.width = "100%";
  }
}



function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}
function openNavright() {
  document.getElementById("mySidepanelright").style.width = "1050px";
}

function closeNavright() {
  document.getElementById("mySidepanelright").style.width = "0";
}


</script>







	<script>
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 20
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	</script>
