 <?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    application.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



session_start();

 // VARIABLES

 $fullurl = 'https://'.$_SERVER['HTTP_HOST'].'/';
 $fullpath = $_SERVER['DOCUMENT_ROOT'].'/';

 $url = $_SERVER['SCRIPT_NAME'];
 $parts = explode('/', $url);


 //Constants

 	//Application Version
	define('version', '1.2');

	//How Long Will Persitence Last Default 30 days set it minutes.
	define("CookieLength", 43200); //


 // Load Composer Elements
 //require $fullpath.'vendor/autoload.php';

 //Load Encryption Functions
    include ($fullpath."assets/app_php/crypt.php");


 //Load DB Class

 include ($fullpath."assets/app_classes/bftdb.class.php");

 $db = new database;

 //Load Form Class

  include ($fullpath."assets/app_classes/bftform.class.php");

  $form = new form;

  // Load Functions

   include ($fullpath."assets/app_php/functions.php");

// error_log('========================================================================================================');
// error_log('========================================================================================================');
// error_log($parts['1'].'/'.$parts['2']);
// error_log('admin/index.php');
// error_log('========================================================================================================');



if($parts['1']=='assets'&&$parts['2']=='app_php'&&$parts['3']=='auth.php'||$parts['1']=='assets'&&$parts['2']=='app_php'&&$parts['3']=='auth_new.php' || $parts['3']=='shift_response.php'|| $parts['4'] == 'save_reponse.php'){


}else if($parts['1']=='admin'&&$parts['2']=='index.php'||$parts['1']=='admin'&&$parts['2']=='index2.php'||$parts['1']=='admin'&&$parts['2']=='signup.php'||$parts['1']=='sign-up.php'||$parts['1']=='index2.php'||$parts['1']=='index.php'||$parts['1']=='login.php'||$parts['1']=='privacy-policy.php'||$parts['1']=='terms-of-use.php'||$parts['1']=='reset.php'|| $parts['3']=='shift_response.php'|| $parts['4'] == 'save_reponse.php' || $parts['4'] == 'reset_password.php'){

}else{

   if(isset($_SESSION['SESS_ACCOUNT_ID'])){
       //checklogin(decrypt($_SESSION['SESS_ACCOUNT_ID']));
       if(checklogin(decrypt($_SESSION['SESS_ACCOUNT_ID']))=='not login'){
         header('Location:'.$fullurl.'login.php');
       }
   }
   else{
        //checkCookie();
        if(checkCookie()==0){
          header('Location:'.$fullurl.'login.php');
        }
   }
 }
 ?>
