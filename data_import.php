<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 echo '<h2>Data Import</h2>';


require_once 'php-excel/PHPExcel.php';



$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setReadDataOnly(true);

$objPHPExcel = $objReader->load("php-excel/files/wirraldataall.xlsx");
$objWorksheet = $objPHPExcel->getActiveSheet();

$highestRow = $objWorksheet->getHighestRow();
$highestColumn = $objWorksheet->getHighestColumn();

$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

echo $highestRow.'<br />';

echo '<table border="1">' . "\n";
for ($row = 1; $row <= $highestRow; ++$row) {
  echo '<tr>' . "\n";

  $user_id = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
  $firstname = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
  $surname = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
  $email = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
  $locationid = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
  $defaultcontextid = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
  $mobilephone = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
  $telephone = $objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
  $bestphone = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
  $address1 = $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
  $address2 = $objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
  $address3 = $objWorksheet->getCellByColumnAndRow(11, $row)->getValue();
  $city = $objWorksheet->getCellByColumnAndRow(12, $row)->getValue();
  $postcode = $objWorksheet->getCellByColumnAndRow(13, $row)->getValue();
  $activeStatus = $objWorksheet->getCellByColumnAndRow(14, $row)->getValue();
  $smartCard = $objWorksheet->getCellByColumnAndRow(15, $row)->getValue();
  $smartCardName = $objWorksheet->getCellByColumnAndRow(16, $row)->getValue();
  $dob = $objWorksheet->getCellByColumnAndRow(17, $row)->getValue();
  $nino = $objWorksheet->getCellByColumnAndRow(18, $row)->getValue();
  $nhsPensionRef = $objWorksheet->getCellByColumnAndRow(19, $row)->getValue();
  $gmc = $objWorksheet->getCellByColumnAndRow(20, $row)->getValue();
  $sex = $objWorksheet->getCellByColumnAndRow(21, $row)->getValue();
  $nhsPensionContributor = $objWorksheet->getCellByColumnAndRow(22, $row)->getValue();
  $nshPentionRate = $objWorksheet->getCellByColumnAndRow(23, $row)->getValue();
  $nhsPensionAddedYears = $objWorksheet->getCellByColumnAndRow(24, $row)->getValue();
  $nhsavc = $objWorksheet->getCellByColumnAndRow(25, $row)->getValue();
  $nhsap = $objWorksheet->getCellByColumnAndRow(26, $row)->getValue();
  $nhserrbo = $objWorksheet->getCellByColumnAndRow(27, $row)->getValue();
  $car1 = $objWorksheet->getCellByColumnAndRow(28, $row)->getValue();
  $car2 = $objWorksheet->getCellByColumnAndRow(29, $row)->getValue();
  $car3 = $objWorksheet->getCellByColumnAndRow(30, $row)->getValue();
  $nok1 = $objWorksheet->getCellByColumnAndRow(31, $row)->getValue();
  $nok1Phone = $objWorksheet->getCellByColumnAndRow(32, $row)->getValue();
  $nok2 = $objWorksheet->getCellByColumnAndRow(33, $row)->getValue();
  $nok2Phone = $objWorksheet->getCellByColumnAndRow(34, $row)->getValue();
  $newAccountID = createid('member');
  $newAccountExtraID = createid('ainfo');

  if($mobilephone != 'NULL'){
    $mobilephone = '0'.$mobilephone;
  }

  if($telephone != 'NULL'){
    $telephone = '0'.$telephone;
  }

    echo '<td>'.$user_id.'</td>'."\n";
    echo '<td>'.$firstname.'</td>'."\n";
    echo '<td>'.$surname.'</td>'."\n";
    echo '<td>'.$email.'</td>'."\n";
    echo '<td>'.$locationid.'</td>'."\n";
    echo '<td>'.$defaultcontextid.'</td>'."\n";
    echo '<td>'.$mobilephone.'</td>'."\n";
    echo '<td>'.$telephone.'</td>'."\n";
    echo '<td>'.$bestphone.'</td>'."\n";
    echo '<td>'.$address1.'</td>'."\n";
    echo '<td>'.$address2.'</td>'."\n";
    echo '<td>'.$address3.'</td>'."\n";
    echo '<td>'.$city.'</td>'."\n";
    echo '<td>'.$postcode.'</td>'."\n";
    echo '<td>'.$activeStatus.'</td>'."\n";
    echo '<td>'.$smartCard.'</td>'."\n";
    echo '<td>'.$smartCardName.'</td>'."\n";
    echo '<td>'.$dob.'</td>'."\n";
    echo '<td>'.$nino.'</td>'."\n";
    echo '<td>'.$nhsPensionRef.'</td>'."\n";
    echo '<td>'.$gmc.'</td>'."\n";
    echo '<td>'.$sex.'</td>'."\n";
    echo '<td>'.$nhsPensionContributor.'</td>'."\n";
    echo '<td>'.$nshPentionRate.'</td>'."\n";
    echo '<td>'.$nhsPensionAddedYears.'</td>'."\n";
    echo '<td>'.$nhsavc.'</td>'."\n";
    echo '<td>'.$nhsap.'</td>'."\n";
    echo '<td>'.$nhserrbo.'</td>'."\n";
    echo '<td>'.$car1.'</td>'."\n";
    echo '<td>'.$car2.'</td>'."\n";
    echo '<td>'.$car3.'</td>'."\n";
    echo '<td>'.$nok1.'</td>'."\n";
    echo '<td>'.$nok1Phone.'</td>'."\n";
    echo '<td>'.$nok2.'</td>'."\n";
    echo '<td>'.$nok2Phone.'</td>'."\n";

    $db = new database;
    $db->Query("select * from ws_accounts_extra_info_import where old_id = ?");
    $db->bind(1,$user_id);
    //$data = $db->single();

    if(!empty($data)){
      $db = new database;
      $db->Query("update ws_accounts_extra_info_import set mobile_number = ?, telephone = ? where id = ?");
      $db->bind(1,$mobilephone);
      $db->bind(2,$telephone);
      $db->bind(3,$data['id']);
      //$db->execute();
    }else{
      echo $user_id.' Not Found<br />';

      //Insert into accounts_import
      $db = new database;
      $db->Query("insert into accounts_import (id, username, password, name, email, created_date, account_type) values(?,?,?,?,?,?,?)");
      $db->bind(1,$newAccountID);
      $db->bind(2,encrypt($email));
      $db->bind(3,encrypt('SB2021user!'));
      $db->bind(4,$firstname.' '.$surname);
      $db->bind(5,$email);
      $db->bind(6,time());
      $db->bind(7,'user');
      //$db->execute();


      //Insert into ws_accounts
      $db = new database;
      $db->Query("insert into ws_accounts_import (id, created_date, first_name, surname, email, username, password, rota) values(?,?,?,?,?,?,?,?)");
      $db->bind(1,$newAccountID);
      $db->bind(2,time());
      $db->bind(3,$firstname);
      $db->bind(4,$surname);
      $db->bind(5,$email);
      $db->bind(6,encrypt($email));
      $db->bind(7,encrypt('SB2021user!'));
      $db->bind(8,1);
      //$db->execute();

      //Insert into ws_accounts_extra
      $db = new database;
      $db->Query("insert into ws_accounts_extra_info_import
      (id,
      account_id,
      created_date,
      created_by,
      mobile_number,
      telephone,
      old_id,
      address1,
      address2,
      address3,
      city,
      postcode,
      smart_card,
      smart_card_name,
      dob,
      nino,
      nhs_pension_ref,
      gmc,
      sex,
      nhs_pension_contributor,
      nhs_pension_rate,
      nhs_pension_added_years,
      nhsavc,
      nhsap,
      nsherrbo,
      car1,
      car2,
      car3,
      nok1,
      nok1_phone,
      nok2,
      nok2_phone) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $db->bind(1,$newAccountExtraID);
      $db->bind(2,$newAccountID);
      $db->bind(3,time());
      $db->bind(4,$newAccountID);
      $db->bind(5,$mobilephone);
      $db->bind(6,$telephone);
      $db->bind(7,$user_id);
      $db->bind(8,$address1);
      $db->bind(9,$address2);
      $db->bind(10,$address3);
      $db->bind(11,$city);
      $db->bind(12,$postcode);
      $db->bind(13,$smartCard);
      $db->bind(14,$smartCardName);
      $db->bind(15,$dob);
      $db->bind(16,$nino);
      $db->bind(17,$nhsPensionRef);
      $db->bind(18,$gmc);
      $db->bind(19,$sex);
      $db->bind(20,$nhsPensionContributor);
      $db->bind(21,$nshPentionRate);
      $db->bind(22,$nhsPensionAddedYears);
      $db->bind(23,$nhsavc);
      $db->bind(24,$nhsap);
      $db->bind(25,$nhserrbo);
      $db->bind(26,$car1);
      $db->bind(27,$car2);
      $db->bind(28,$car3);
      $db->bind(29,$nok1);
      $db->bind(30,$nok1Phone);
      $db->bind(31,$nok2);
      $db->bind(32,$nok2Phone);
      //$db->execute();

      $db = new database;
      $db->Query("insert into ws_accounts_locations_import (id, location_id, account_id) values (?,?,?)");
      $db->bind(1,createid('a_loc'));
      $db->bind(2,'Wirral');
      $db->bind(3,$newAccountID);
      //$db->execute();
    }



  echo '</tr>' . "\n";
}
echo '</table>' . "\n";
