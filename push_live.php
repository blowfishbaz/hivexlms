

  INSERT INTO `ws_accounts_locations` (`id`, `location_id`, `account_id`, `status`)
  VALUES
    ('id_a_loc_20210330_193446819600_91583', 'Wirral', 'id_member_20210322_094920682900_56629', 1),
    ('id_a_loc_20210330_193446851900_24158', 'Wirral', 'id_member_20210322_095001181200_22137', 1),
    ('id_a_loc_20210330_193446851900_24158', 'Wirral', 'id_member_20210322_094920622400_41155', 1),
    ('id_a_loc_20210330_193446851900_24158', 'Wirral', 'id_member_20210322_094920648800_41625', 1),
    ('id_a_loc_20210330_193446851900_24158', 'Wirral', 'id_member_20210322_094920678400_34027', 1);


        INSERT INTO `ws_accounts_roles` (`id`, `role_id`, `account_id`, `status`)
        VALUES
          ('id_a_role_20210430_215192928700_566611', '7', 'id_member_20210322_094920682900_56629', 1),
          ('id_a_role_20210430_21511022928700_566612', '7', 'id_member_20210322_095001181200_22137', 1),
          ('id_a_role_20210430_21511022928700_566613', '7', 'id_member_20210322_094920622400_41155', 1),
          ('id_a_role_20210430_21511022928700_566614', '7', 'id_member_20210322_094920648800_41625', 1),
          ('id_a_role_20210430_21511022928700_566615', '7', 'id_member_20210322_094920678400_34027', 1);



          INSERT INTO `accounts` (`id`, `username`, `password`, `name`, `email`, `type`, `status`, `attempts`, `lastattempt`, `created_date`, `profilepic`, `account_type`, `user_status`, `service_area`)
          VALUES
          	('id_member_20210322_095001181200_22137', '736d53175a9c2cc6b39c029a0069ad92ed812e4b7002bce9', '8617f5391c19ee9f2bac15f3e366ec93', 'Chris Howell', 's.morgan24@nhs.net', '', 1, 0, 0, 1616406601, '', 'user', 1, NULL),
          	('id_member_20210322_094920682900_56629', 'ce79f43215df2f23948976a807a03489fb1e5a145bb8879a', '8617f5391c19ee9f2bac15f3e366ec93', 'Hannah Davies', 'handavies@live.co.uk', '', 1, 0, 0, 1616406560, '', 'user', 1, NULL),
          	('id_member_20210322_094920622400_41155', '0ad3262f0cc0de3ed8fb68ccb656f241ec01c3dd5d7f0e13', '8617f5391c19ee9f2bac15f3e366ec93', 'Julia Thomas', 'julia.thomas3@nhs.net', '', 1, 0, 0, 1616406560, '', 'user', 1, NULL),
          	('id_member_20210322_094920678400_34027', 'd8afa4cd698bf3e6ec79d16c07319211f2b634c4abd9260c3b3de7a2931c6347', '8617f5391c19ee9f2bac15f3e366ec93', 'Karen McCabe', 'karenmccabern1@outlook.com', '', 1, 0, 0, 1616406560, '', 'user', 1, NULL),
          	('id_member_20210322_094920648800_41625', '5f9c04446fd60b2fd20741a171a61f799dddf365beee046f', '8617f5391c19ee9f2bac15f3e366ec93', 'Karen Trow', 'karen.trow@nhs.net', '', 1, 0, 0, 1616406560, '', 'user', 1, NULL);

            INSERT INTO `ws_accounts_extra_info` (`id`, `account_id`, `company_id`, `main_provision`, `created_date`, `created_by`, `modified_date`, `modified_by`, `mobile_number`, `telephone`, `job_title`, `status`, `account_type`, `credit`, `old_id`, `address1`, `address2`, `address3`, `city`, `postcode`, `smart_card`, `smart_card_name`, `dob`, `nino`, `nhs_pension_ref`, `gmc`, `sex`, `nhs_pension_contributor`, `nhs_pension_rate`, `nhs_pension_added_years`, `nhsavc`, `nhsap`, `nsherrbo`, `car1`, `car2`, `car3`, `nok1`, `nok1_phone`, `nok2`, `nok2_phone`)
            VALUES
            	('id_ainfo_20210322_094920622400_53717', 'id_member_20210322_094920622400_41155', NULL, NULL, 1616406560, 'id_member_20210322_094920622400_41155', NULL, NULL, 44, 0, NULL, 1, 'Learner', 0, '20700', '100 Frankby Road', 'West Kirby', NULL, 'MERSEYSIDE', 'CH48 9UX', '603696128041', 'Julia Thomas', '27072', 'NULL', 'NULL', 'RGN  NMC 97i5979e', 'F', 'N', '0', '0', '0', '0', '0', 'Bmw YA68GDO', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'),
            	('id_ainfo_20210322_094920648800_60194', 'id_member_20210322_094920648800_41625', NULL, NULL, 1616406560, 'id_member_20210322_094920648800_41625', NULL, NULL, 7792, 0, NULL, 1, 'Learner', 0, '20714', '22 Boundary Road', NULL, NULL, 'BIDSTON', 'CH43 7PF', 'NULL', 'NULL', '26799', 'NULL', 'NULL', 'NMC', 'F', 'N', '0', '0', '0', '0', '0', 'DV15HME', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'),
            	('id_ainfo_20210322_094920678400_72408', 'id_member_20210322_094920678400_34027', NULL, NULL, 1616406560, 'id_member_20210322_094920678400_34027', NULL, NULL, 0, 2147483647, NULL, 1, 'Learner', 0, '20721', '10 Mark Rake', 'Bromborough', NULL, 'WIRRAL', 'CH62 2DN', '247520987048', 'Karen McCabe', '27327', 'NULL', 'NULL', '94J1754E', 'F', 'N', '0', '0', '0', '0', '0', 'BMW 1 series VA10 JJZ', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'),
            	('id_ainfo_20210322_094920682900_81593', 'id_member_20210322_094920682900_56629', NULL, NULL, 1616406560, 'id_member_20210322_094920682900_56629', NULL, NULL, 2147483647, 2147483647, NULL, 1, 'Learner', 0, '20724', '54 Primrose Hill', 'Port Sunlight', NULL, 'WIRRAL', 'CH625EW', '213450052047', 'Hannah Davies', '34401', 'NULL', 'NULL', 'NMC Pin: 15I0093E', 'F', 'N', '0', '0', '0', '0', '0', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'),
            	('id_ainfo_20210322_095001181200_73536', 'id_member_20210322_095001181200_22137', NULL, NULL, 1616406601, 'id_member_20210322_095001181200_22137', NULL, NULL, 0, 0, NULL, 1, 'Learner', 0, '23153', NULL, NULL, NULL, 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'N', '0', '0', '0', '0', '0', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL');


              INSERT INTO `ws_accounts_upload` (`id`, `account_id`, `type`, `name`, `path`, `status`, `created_date`, `created_by`)
              VALUES
              	('id_img_20210322_100628903400_31382', 'id_member_20210322_094920678400_34027', 'Manual', '103302.pdf', 'uploads/users/20721/103302.pdf', 1, 1616407588, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100554020300_56273', 'id_member_20210322_094920678400_34027', 'Manual', '100797.pdf', 'uploads/users/20721/100797.pdf', 1, 1616407554, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553582300_61355', 'id_member_20210322_094920678400_34027', 'Manual', '100631.jpg', 'uploads/users/20721/100631.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553576000_32242', 'id_member_20210322_094920678400_34027', 'Manual', '100630.jpg', 'uploads/users/20721/100630.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553572100_94839', 'id_member_20210322_094920678400_34027', 'Manual', '100629.pdf', 'uploads/users/20721/100629.pdf', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553567100_15000', 'id_member_20210322_094920678400_34027', 'Manual', '100628.pdf', 'uploads/users/20721/100628.pdf', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553562800_11150', 'id_member_20210322_094920678400_34027', 'Manual', '100627.pdf', 'uploads/users/20721/100627.pdf', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553479000_87195', 'id_member_20210322_094920682900_56629', 'Manual', '100604.jpg', 'uploads/users/20724/100604.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553474500_14788', 'id_member_20210322_094920682900_56629', 'Manual', '100603.jpg', 'uploads/users/20724/100603.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553281800_96402', 'id_member_20210322_094920682900_56629', 'Manual', '100556.jpg', 'uploads/users/20724/100556.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553271500_66967', 'id_member_20210322_094920682900_56629', 'Manual', '100550.jpg', 'uploads/users/20724/100550.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553267100_40447', 'id_member_20210322_094920682900_56629', 'Manual', '100549.jpg', 'uploads/users/20724/100549.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553263200_84079', 'id_member_20210322_094920682900_56629', 'Manual', '100548.jpg', 'uploads/users/20724/100548.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553205700_67810', 'id_member_20210322_094920678400_34027', 'Manual', '100523.jpg', 'uploads/users/20721/100523.jpg', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100553201300_14511', 'id_member_20210322_094920678400_34027', 'Manual', '100522.pdf', 'uploads/users/20721/100522.pdf', 1, 1616407553, 'id_member_20161103_110736904700_82639'),
              	('id_img_20210322_100548203100_21157', 'id_member_20210322_094920622400_41155', 'Manual', '100410.jpg', 'uploads/users/20700/100410.jpg', 1, 1616407548, 'id_member_20161103_110736904700_82639');

                INSERT INTO `ws_accounts` (`id`, `created_date`, `first_name`, `surname`, `email`, `username`, `password`, `attempts`, `lastattempt`, `profilepic`, `status`, `type`, `pid`, `lxp`, `rota`, `hr`)
                VALUES
                	('id_member_20210322_094920622400_41155', 1616406560, 'Julia', 'Thomas', 'julia.thomas3@nhs.net', '0ad3262f0cc0de3ed8fb68ccb656f241ec01c3dd5d7f0e13', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_094920622400_41155', 1, 1, 1),
                	('id_member_20210322_094920648800_41625', 1616406560, 'Karen', 'Trow', 'karen.trow@nhs.net', '5f9c04446fd60b2fd20741a171a61f799dddf365beee046f', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_094920648800_41625', 1, 1, 1),
                	('id_member_20210322_094920678400_34027', 1616406560, 'Karen', 'McCabe', 'karenmccabern1@outlook.com', 'd8afa4cd698bf3e6ec79d16c07319211f2b634c4abd9260c3b3de7a2931c6347', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_094920678400_34027', 1, 1, 1),
                	('id_member_20210322_094920682900_56629', 1616406560, 'Hannah', 'Davies', 'handavies@live.co.uk', 'ce79f43215df2f23948976a807a03489fb1e5a145bb8879a', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_094920682900_56629', 1, 1, 1),
                	('id_member_20210322_095001181200_22137', 1616406601, 'Chris', 'Howell', 's.morgan24@nhs.net', '736d53175a9c2cc6b39c029a0069ad92ed812e4b7002bce9', '', NULL, NULL, '', NULL, 'web', 'id_member_20210322_095001181200_22137', 1, 1, 1);



          'id_member_20210322_094920682900_56629','id_member_20210322_095001181200_22137','id_member_20210322_094920622400_41155','id_member_20210322_094920648800_41625','id_member_20210322_094920678400_34027'
