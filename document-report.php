<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 $db = new database;
 $db->query("select accounts.name, accounts.id
from accounts
where accounts.account_type = 'user'
order by accounts.name asc ");
 $users = $db->resultset();

 foreach ($users as $u) {
   // code...
 }

 $db->query("select * from ws_roles where status = 1 order by title asc");
 $db->bind(1,$u['id']);
 $all_roles = $db->resultset();

 ?>

 <table border="1">
   <tr>
     <th>Name</th>
     <th>Document Count</th>
     <th>Roles</th>
   </tr>
   <? foreach ($users as $u) { ?>
     <?
     $db->query("select COALESCE(count(id),0) as doc_count from ws_accounts_upload where account_id = ? and status = ?");
     $db->bind(1,$u['id']);
     $db->bind(2,'1');
     $uploads_per = $db->single();

     $db->query("select * from ws_accounts_roles where account_id = ? and status = 1");
     $db->bind(1,$u['id']);
     $roles = $db->resultset();

     $my_roles = array();
     $my_location = array();

     foreach ($roles as $r) {
       array_push($my_roles, $r['role_id']);
     }
     ?>
      <tr>
        <td> <?echo $u['name'];?> </td>
        <td> <?echo $uploads_per['doc_count'];?> </td>
        <td>
          <?foreach ($all_roles as $ar) {?>
            <?if(in_array($ar['id'],$my_roles)){?>

            <?echo $ar['title'].', ';?>

            <?}?>
          <?}?>
        </td>
      </tr>
  <? } ?>
 </table>
