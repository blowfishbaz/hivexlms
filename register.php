<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/

ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 //Page Title
 $page_title = 'Register';

 $uid = decrypt($_SESSION['SESS_ACCOUNT_ID']);

 $db = new database;
  $db->query("select * from ws_accounts where id = ? ");
  $db->bind(1,$uid);
  $user = $db->single();
 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Sky Recruitment</title>


<style>



/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 parallax" style=' min-height: 50vh; background-image: url("<? echo $fullurl; ?>assets/images/Sky-about1.jpg");'>
		<div class="col-lg-2 col-md-2"></div>
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="page_spacer"></div>

				<h2 class="text-center media-heading color">New candidates</h2>
				<h3 class="text-center color">Register & Get Connected</h3>
				<h4 class="text-center color"><br />Creating an account with us will allow you to send us your CV and manage job alerts for email notifications when a vacancy that matches your requirements become available.<br /><br />Please upload your CV in .pdf, .doc, or .docx format.</h4>


			</div>
	</div>

	<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="page_spacer"></div>


				<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?if($user['status']=='1'){?>

						<h3 style=" margin-top: 0;"><span class="color1">Registration</span></h3>

						<p>You have completed registration and are now able to appy for jobs. Click below to start looking for jobs. If you need to change any on the information given then click below to edit your profile.</p>

						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="page_spacer2"></div>
								<div class="clearfix"></div>
								<a href="<? echo $fullurl ?>jobs.php" class="btn btn-lg btn-primary btn-block">Job Search</a>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="page_spacer2"></div>
								<div class="clearfix"></div>
								<a href="<? echo $fullurl ?>editprofile.php" class="btn btn-lg btn-primary btn-block">Edit Profile</a>
							</div>
						</div>
						<div class="clearfix"></div>
          <?}else if($user['status']=='2'){?>
						<h3 style=" margin-top: 0;">Complete Candidate <span class="color1">Registration</span></h3>

							<p>No matter what kind of role you are looking for, we will be able to help you. By becoming a candidate directly on our website you will be sent job alerts and notifications for vacancies that match your requirements.<br /><br />Simply fill in your details below and then start applying for listed jobs.</p>

							<form id="registration-form" class="registration-form" enctype="application/x-www-form-urlencoded" method="post">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step1">
			          <div class="form-group label-floating is-empty">
			                      <label for="address1" class="control-label">First Address Line*</label>
														<p class="input_error_message">first line address is required</p>
			                      <input id="address1" type="text" name="address1" class="form-control" value="" maxlength="200" required>
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="address2" class="control-label">Second Address Line</label>
														<!-- <p class="input_error_message"></p> -->
			                      <input id="address2" type="text" name="address2" class="form-control" value="" maxlength="200">
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="address3" class="control-label">Third Address Line</label>
														<!-- <p class="input_error_message"></p> -->
			                      <input id="address3" type="text" name="address3" class="form-control" value="" maxlength="200">
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="city" class="control-label">City*</label>
														<p class="input_error_message">a city is required</p>
			                      <input id="city" type="text" name="city" class="form-control" value="" maxlength="100" required>
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="county" class="control-label">County</label>
														<!-- <p class="input_error_message"></p> -->
			                      <input id="county" type="text" name="county" class="form-control" value="" maxlength="100">
			                    <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                      <label for="postcode" class="control-label">Postcode*</label>
														<p class="input_error_message">a postcode is required</p>
			                      <input id="postcode" type="text" name="postcode" class="form-control" value="" maxlength="20" required>
			                    <span class="material-input"></span>
			          </div>

			          <div class="form-group label-floating is-empty">
			                  <label for="telephone" class="control-label">Telephone*</label>
													<p class="input_error_message">a telephone number is required</p>
			                  <input id="telephone" type="number" name="telephone" class="form-control" value="" maxlength="30" required>
			                <span class="material-input"></span>
			          </div>

								<div class="form-group label-floating is-empty">
			                  <label for="mobile" class="control-label">Mobile</label>
													<p class="input_error_message">not a vailed mobile number</p>
			                  <input id="mobile" type="number" name="mobile" class="form-control" value="" maxlength="30">
			                <span class="material-input"></span>
			          </div>


									<div class="page_spacer2"></div>
										<div class="page_spacer2"></div>
										<button class="btn btn-lg btn-primary btn-block next_registration" data-id="1" type="button">Next</button>
										</div>
										</div>

										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step2" style="display:none;">



                    <div class="form-group">
											<label class="control-label">Position Looking To Apply*</label>
											<select name="positions" class="big_in form-control selectpicker" data-live-search="true" id="positions" required>
                            <option selected="" style="display:none">&nbsp;</option>


                            <option value="Health Care Assistant">Health Care Assistant</option>
                            <option value="Support Worker - Adults">Support Worker - Adults</option>
                            <option value="Support Worker - Children">Support Worker - Children</option>
                            <option value="Support Worker - Adult Mental Health">Support Worker - Adult Mental Health</option>
                            <option value="Support Worker - Children Mental Health">Support Worker - Children Mental Health</option>
                            <option value="Support Worker - Adult Learning Disabilities">Support Worker - Adult Learning Disabilities</option>
                            <option value="Support Worker - Chilren Learning Disabilities">Support Worker - Chilren Learning Disabilities</option>
                            <option value="Support Worker - Adults - Autism">Support Worker - Adults - Autism</option>
                            <option value="Support Worker - Chilren - Autism">Support Worker - Chilren - Autism</option>
                            <option value="Registered Nurse">Registered Nurse</option>
                            <option value="Registered Nurse - RMN">Registered Nurse - RMN</option>
                            <option value="Learning Disability Nurse (RNLD)">Learning Disability Nurse (RNLD)</option>
                            <option value="Peadiatric Nurse">Peadiatric Nurse</option>
                            <option value="Registered Manager - Adult">Registered Manager - Adult</option>
                            <option value="Registered Manager - Children">Registered Manager - Children</option>
                            <option value="Deputy Manager - Adult">Deputy Manager - Adult</option>
                            <option value="Deputy Manager - Children">Deputy Manager - Children</option>
                            <option value="Team Leader - Adult">Team Leader - Adult</option>
                            <option value="Team Leader - Children">Team Leader - Children</option>
                            <option value="Senior Support Worker - Adult">Senior Support Worker - Adult</option>
                            <option value="Senior Support Worker - Children">Senior Support Worker - Children</option>




                    </select></div>


												<div class="form-group">
													<label class="control-label">Looking For Job Type*</label>
													<div class="checkbox" style="margin-top: 0;">
									          <label><input type="checkbox" value="temporary" name="temporary" class="jobtype">Temporary</label>
									        </div>
													<div class="checkbox">
									          <label><input type="checkbox" value="permanent" name="permanent" class="jobtype">Permanent</label>
									        </div>
												</div>

												<div class="form-group">
													<label class="control-label">Willing To Travle*</label>
													<select name="travle" class="big_in form-control selectpicker" data-live-search="false" id="travle" required>
		                            <option selected="" style="display:none">&nbsp;</option>
																<option value="1">1 mile</option>
																<option value="3">3 miles</option>
																<option value="5">5 miles</option>
																<option value="10">10 miles</option>
																<option value="15">15 miles</option>
																<option value="20">20 miles</option>
																<option value="30">30 miles</option>
																<option value="50">50 miles</option>
																<option value="any">Any Distance</option>
		                    </select></div>

												<div class="form-group">
													<label class="control-label">Do you have a vailed driving license*</label>
													<select name="license" class="big_in form-control selectpicker" data-live-search="false" id="license" required>
		                            <option selected="" style="display:none">&nbsp;</option>
																<option value="yes">Yes</option>
																<option value="no">No</option>
		                    </select></div>

												<div class="form-group">
													<label class="control-label">Do you want to upload CV or talk to the team*</label>
													<select name="infotype" class="big_in form-control selectpicker" data-live-search="false" id="infotype" required>
		                            <option selected="" style="display:none">&nbsp;</option>
																<option value="cv">Upload CV</option>
																<option value="talk">Talk to the Team (A member of our team will call you on the number provided)</option>
		                    </select></div>



                        <div class="input-group cv_upload form-group" style="display:none;">
														<label class="control-label">Upload CV</label>
                            <input type="text" class="input_button" id="cv_upload" placeholder="CV" name="cv_upload" readonly>
                            <input type="hidden" name="cv_upload_id" id="cv_upload_id" />
                            <span class="input-group-btn">
                                    <button style="margin-top: 25px;" class="btn btn-default input_button btn-primary" id="cv_upload_but" type="button">Upload</button>
                            </span>
                        </div><!-- /input-group -->

											  <div class="row">
												 <div class="row visible-md visible-lg">
													 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															 	<label class="control-label">Qualifications</label>
														 	</div>
														 	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
															 	<label class="control-label">Grade</label>
														 	</div>
														 	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
															 	<label class="control-label">Date</label>
														 </div>
												 </div>
											 </div>

												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
																<label class="control-label visible-sm visible-xs">Qualifications</label>
																<input type="text" class="form-control" name="qualifications[]" value="" ><span class="material-input"></span>
															</div>
															<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
																<label class="control-label visible-sm visible-xs">Grade</label>
																<input type="text" class="form-control" name="grade[]" value="" ><span class="material-input"></span>
															</div>
															<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 form-group">
																<label class="control-label visible-sm visible-xs">Date</label>
																<input type="date" class="form-control" name="date[]" value="" ><span class="material-input"></span>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible-sm visible-xs">
																<div class="clearfix"></div>
																<hr class="btn-default" />
															</div>
													</div>
												</div>

												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<button type="button" class="btn btn-raised btn-success pull-right add_qualification">Add Row<div class="ripple-container"></div></button>
												</div>
											</div>

											<label class="control-label">Upload DBS check</label>
											<div class="input-group dbs_upload form-group">
													<input type="text" class="input_button" id="dbs_upload" placeholder="DBS check" name="dbs_upload" readonly>
													<input type="hidden" name="dbs_upload_id" id="dbs_upload_id" />
													<span class="input-group-btn">
																	<button class="btn btn-default input_button btn-primary" id="dbs_upload_but" type="button">Upload</button>
													</span>
											</div>



											<div class="form-group">
												<label class="control-label">Do you have any convictions, cautions, reprimands or final warnings which are not protected as defined by the Rehabilitation of Offenders Act 1974 (Exceptions) Order 1975 (as amended in 2013)?</label>
												<select name="conviction" class="big_in form-control selectpicker" data-live-search="false" id="conviction" required>
															<option selected="" style="display:none">&nbsp;</option>
															<option value="yes">Yes</option>
															<option value="no">No</option>
											</select></div>

											<div class="form-group">
											<div class="form-group" style="display:none">
												<label class="control-label">Please provide details of your criminal record in the space below</label>
												<textarea name="conviction_info" class="form-control" rows="5" id="conviction_info"></textarea></div>
												</div>





												<div class="page_spacer2"></div>
													<div class="page_spacer2"></div>
													<button class="btn btn-lg btn-primary btn-block next_registration" data-id="2" type="button">Next</button>

												</div>


												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step3" style="display:none;">

													<div class="row">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																 <label class="control-label">Employment History</label>
															 </div>

													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Employer Name</label>
																	<input type="text" class="form-control" name="emp_name[]" value="" ><span class="material-input"></span>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Job Title</label>
																	<input type="text" class="form-control" name="emt_title[]" value="" ><span class="material-input"></span>
																</div>
																<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Date</label>
																	<input type="date" class="form-control" name="emp_date_start[]" value="" ><span class="material-input"></span>
																</div>
																<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Date</label>
																	<input type="date" class="form-control" name="emp_date_end[]" value="" ><span class="material-input"></span>
																</div>

																<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group">
																	<label class="control-label">Employer Address</label>
																	<textarea name="emp_address[]" class="form-control" rows="5" id="emp_address"></textarea></div>




																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<div class="clearfix"></div>
																	<hr class="btn-default" />
																</div>
														</div>
													</div>

													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<button type="button" class="btn btn-raised btn-success pull-right add_employer">Add Row<div class="ripple-container"></div></button>
													</div>
												</div>


												<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															 <label class="control-label">References</label>
														 </div>



												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																<label class="control-label">References Name</label>
																<input type="text" class="form-control" name="ref_name[]" value="" ><span class="material-input"></span>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
																<label class="control-label">Contact Number</label>
																<input type="text" class="form-control" name="ref_contact[]" value="" ><span class="material-input"></span>
															</div>
															<div class="col-lg-4 col-md-2 col-sm-12 col-xs-12 form-group">
																<label class="control-label">Position Held</label>
																<input type="text" class="form-control" name="ref_position[]" value="" ><span class="material-input"></span>
															</div>


															<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 form-group">
																<label class="control-label">Contact Address</label>
																<textarea name="ref_address[]" class="form-control" rows="5" id="ref_address"></textarea></div>


															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="clearfix"></div>
																<hr class="btn-default" />
															</div>
													</div>
												</div>

												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<button type="button" class="btn btn-raised btn-success pull-right add_references">Add Row<div class="ripple-container"></div></button>
												</div>
											</div>

													<div class="form-group">
													<div class="checkbox" style="margin-top: 0;">
									          <label><input type="checkbox" value="confirm" name="confirm" class="confirm">I confirm that the information that I have provided in this form is correct and complete</label>
									        </div>
													</div>

													<div class="page_spacer2"></div>
													<button class="btn btn-lg btn-primary btn-block" id="save_registration" type="button">Save</button>

												</div>

												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 step4" style="display:none;"></div>

												</div>
									</form>
							<?}else{?>
					<h3 style=" margin-top: 0;">New Candidate <span class="color1">Registration</span></h3>
						<p>No matter what kind of role you are looking for, we will be able to help you. By becoming a candidate directly on our website you will be sent job alerts and notifications for vacancies that match your requirements.<br /><br />Simply fill in your details below and then start applying for listed jobs.</p>

						<form id="registration-form" class="registration-form" enctype="application/x-www-form-urlencoded" method="post">
		          <div class="form-group label-floating is-empty">
		                      <label for="first_name" class="control-label">First Name*</label>
													<p class="input_error_message">must only contain uppercase and lowercase letter and single quotes</p>
		                      <input id="first_name" type="text" name="first_name" class="form-control" value="" maxlength="50" required>
		                    <span class="material-input"></span>
		          </div>

		          <div class="form-group label-floating is-empty">
		                      <label for="surname" class="control-label">Surname*</label>
													<p class="input_error_message">must only contain uppercase and lowercase letter and single quotes</p>
		                      <input id="surname" type="text" name="surname" class="form-control" value="" maxlength="50" required>
		                    <span class="material-input"></span>
		          </div>

		          <div class="form-group label-floating is-empty">
		                  <label for="email" class="control-label">Email*</label>
												<p class="input_error_message">not a vailed email address</p>
		                  <input id="email" type="email" name="email" class="form-control" value="" maxlength="200" required>
		                <span class="material-input"></span>
		          </div>

							<div class="form-group label-floating is-empty">
              <label for="new_password" class="control-label">New Password</label>
							<p class="input_error_message">must be between 6 and 16 characters, contain 1 uppercase and lowercase letter and contain 1 number <br />may contain special characters like !@#$%^&*()_+</p>
              <input  id="new_password"type="password" class="form-control" id="new_password" name="new_password" value="" required="yes"><span class="material-input"></span></div>

		           <div class="form-group label-floating is-empty">
		                    <label for="confirm_new_password" class="control-label">Confirm New Password</label>
												<p class="input_error_message">must be same as new password</p>
		                    <input  id="confirm_new_password" type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" value="" required="yes">
												<input type="hidden" name="new_id" value="<?echo createId('user')?>"/>
												<input type="hidden" name="key" id="key" value="<? echo getCurrentKey(); ?>">
												<span class="material-input"></span>
		              </div>

									<div class="page_spacer2"></div>
									<div class="page_spacer2"></div>
									<button class="btn btn-lg btn-primary btn-block" id="Sign_Up" type="button">Sign Up</button>
								</form>
								<?}?>

			</div>

		</div>
		<div class="page_spacer"></div>
	</div>
	</div>
	<div class="clearfix"></div>
<div class="clearfix"></div>
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/footer.php'); ?>
</div>
</div>
</div><!--wrapper-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');


?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
<script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>


<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_web_modals.php'); ?>
</body>

</html>
