<?php
/**
*  ______   ____    ____
* |   _  \  \   \  /   /
* |  |_)  |  \   \/   /
* |   _  <    \      /
* |  |_)  |    \    /
* |______/      \__/
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');
$user = decrypt($_SESSION['SESS_ACCOUNT_ID']);
 //Page Title
 $page_title = 'Home';

 $db->query("SELECT * FROM accounts where id = ?");
 $db->bind(1, $user);
 $db->execute();
 $myinfo = $db->single();

 ?>

 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>


<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Bluevoyeurs</title>

<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="<? echo $fullurl; ?>assets/css/jquery.fileupload.css">
  <link rel="stylesheet" href="<? echo $fullurl; ?>assets/css/jquery.fileupload-ui.css">
<style>

html, body {
	width:100%;
	height:100%;
	padding:0;
	margin:0;
}

#wrapper{
	float:left;
	width:100%;
	height:100%;
	padding:0;
	margin:0;
}

#box{
		position: relative;
		top: 0%;
		left: 15%;
		width:70%;
		/* background-color:#c59959a8; */
		background-color: #ffffff006;
		-moz-border-radius: 30px;
		-webkit-border-radius: 30px;
		border-radius: 30px; /* future proofing */
		-khtml-border-radius: 30px; /* for old Konqueror browsers */
		float: left;
		margin-bottom: 10vh;
		padding: 35px 0;
}


#box p {
	width: 100%;
    box-sizing: border-box;
    text-align: center;
    padding: 0 25px;
    margin: 35px 0 0 0;
    color: #333;
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
		font-size: 20px;
}


#att{
	position:fixed;
	bottom:0;
	width:100%;
	/* font-family:Gadget, sans-serif; */
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}
#box p.b{
	border-top:solid #333333 1px;
	border-bottom:solid #333333 1px;
	width: 95% !important;
	margin-left: 2.5% !important;
	float: left;
	padding: 0 ;
}

#box h3 {
    width: 100%;
    box-sizing: border-box;
    font-size: 28px;
		    line-height: 28px;
    text-align: center;
    padding: 0 15px;
    margin: 35px 0 0 0;
    color: #333;
    clear: both;
}
p.c {
	width:100%;
	/* font-size:13px !important; */
	float:left;
	margin-top:25px !important;
	text-align:center;
	padding:0;
	margin:0;
	/* text-transform:uppercase; */
	color:#878786;
}

ul.l {
	width:100%;
	box-sizing: border-box;
	font-size:20px !important;
	float:left;
	margin-top:25px !important;
	text-align:left;
	margin:0;
	/* text-transform:uppercase; */
	color:#333;

}
ul {
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}


.button_holder {
    width: 480px;
    margin: 20px auto 15px auto;
    display: block;
}
.tab_button {
    width: 200px;
    float: left;
    background: #ffffff;
    /* color: #0b2f87; */
    /* border: 1px solid #0b2f87; */
		color: #1a71bd;
    border: 1px solid #1a71bd;
    font-size: 20px;
    border-radius: 15px;
    margin: 0 20px;
}
.tab_button:hover {
	background: #1a71bd;
    color: #ffffff;
    cursor: pointer;
}
.tab_button:focus {
    outline: 0;
}
.tab_button.current_but {
	background: #1a71bd;
	color: #ffffff;
}
/* .escort,
.agencies{
	display:none;
	float: left;
	width: 90%;
  margin: 0px 5%;
} */
.signup{
	/* display:none; */
	float: left;
	width: 90%;
  margin: 35px 5% 10px 5%;
}
.current {
    display: block;
}
#box .Profile_viewer{
width: 80%;
margin: 15px auto 15px auto;
display: block;
border-radius: 50%;
}

#box img{
width: 400px;
margin: 0px auto 15px auto;
display: block;
}
.backstretch{
	position: fixed !important;
}



input#appn,
input#appe {
	width: 32%;
border-radius: 15px;
margin: 0 0.3% 0 0;
border: solid 2px #ccc;
font-size: 18px;
padding: 16px;
box-sizing: border-box;
}

input.submit {
width: 32%;
    font-size: 20px;
    border-radius: 15px;
    margin: 0 0px;
		padding: 16px;
    background: #1a71bd;
    color: #ffffff;
		}

		form#email-form {
    margin-top: 25px;
}

/* .backstretch img {
    opacity: 0.6;
} */
.text_holder{
	background: #ffffff;
	float: left;
	width: 100%;
	margin-top: 35px;
	border-radius: 20px;
	display: none;
	padding-bottom: 35px;
}

.text_holder.current{
	display: block;
}


.form-signin_page{
	display: none;
}

.preferenceslist {
    float: left;
    width: auto;
    border: solid 2px #cccccc;
    border-radius: 10px;
    margin: 3px;
    font-size: 18px;
    padding: 0 0 0 10px;
}

.removepreferenceslist {
    background: transparent;
    border: none;
    padding: 0;
}
.glyphicons-remove.glyphicons:before {
    padding: 3px 5px;
}
.removepreferenceslist:hover {
    color:#f00;
}
.removepreferenceslist:focus {
    outline: none;
}

.form-registration {
    margin: 0 35px;
}


.needs_approving {
    position: absolute;
    background: #ff0000;
    color: #fff;
    padding: 5px 10px;
    border-radius: 5px;
    width: 60%;
    margin: 0 15%;
    text-align: center;
    bottom: 0;
}
@media (max-width: 800px) {
	#box {
    position: relative;
    top: 0%;
    left: 2%;
    width: 96%;
}
.escort, .agencies {
    width: 100%;
    margin: 0;
}

input.submit {
    width: 100%;
    font-size: 20px;
}
input#appn, input#appe {
    width: 100%;
		margin-bottom: 15px;
}
#box p {
    font-size: 20px;
}
#box h3 {
    font-size: 30px;
    line-height: 30px;
}
#box img {
    width: 100%;
}
form#email-form {
    margin-top: 5px;
}
.tab_button {
    width: 100%;
    font-size: 30px;
		    margin: 10px 0;
}
.button_holder {
    width: 100%;
    margin: 0;
    display: block;
}
}


/* h1 { font-family: Calibri; font-style: normal; font-variant: normal; font-weight: 100;}
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 700;}
p,li { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-style: normal; font-variant: normal; font-weight: 400;} */

</style>
</head>

<body>

	<div id="wrapper">
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_menu/topmenu.php'); ?>
<div id="box">

<img src="<? echo $fullurl; ?>assets/images/BV3.png">
<!-- <p class="b">Website Coming Soon</p>
<p class="c"><span style="border-right:#878786 solid 1px; padding-right:10px">M:07725304077</span>   &nbsp;&nbsp; E:mark@taurusconstructionnw.com</p> -->



<div class="row">
		<div class="col-lg-12">

			<div class="text_holder escort current">
				<h3>Profile uploader</h3>
				<p>Upload your Profile Photos Here<br />All photo will need to be aproved befor made live</p>

				<div class="col-lg-4">

					<?if($myinfo['profilepic']==$myinfo['profilepic_temp'] && $myinfo['profilepic']!=''){
							$db->query("SELECT * FROM ws_uploads where id = ?");
							$db->bind(1, $myinfo['profilepic']);
							$db->execute();
							$img = $db->single();
							$lowres = $fullurl.'img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';
						echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic" type="button">Upload Main Profile</button>';
						echo '<img src="'.$lowres.'" class="Profile_viewer">';
					}else if($myinfo['profilepic_temp']!=''){
						$db->query("SELECT * FROM ws_uploads where id = ?");
						$db->bind(1, $myinfo['profilepic_temp']);
						$db->execute();
						$img = $db->single();
						$lowres = $fullurl.'img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';
					echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic" type="button" disabled>Upload Main Profile</button>';
					echo '<img src="'.$lowres.'" class="Profile_viewer"><span class="needs_approving">Waiting Approval</span>';
					}else{
						echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic" type="button">Upload Main Profile</button>';
						echo '<img src="'.$fullurl.'/assets/images/profileplaceholder.png" class="Profile_viewer">';
					}?>

				</div>
				<div class="col-lg-4">
					<?if($myinfo['profilepic1']==$myinfo['profilepic1_temp'] && $myinfo['profilepic1']!=''){
							$db->query("SELECT * FROM ws_uploads where id = ?");
							$db->bind(1, $myinfo['profilepic1']);
							$db->execute();
							$img = $db->single();
							$lowres = $fullurl.'img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';
						echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic1" type="button">Upload Main Profile</button>';
						echo '<img src="'.$lowres.'" class="Profile_viewer">';
					}else if($myinfo['profilepic1_temp']!=''){
						$db->query("SELECT * FROM ws_uploads where id = ?");
						$db->bind(1, $myinfo['profilepic1_temp']);
						$db->execute();
						$img = $db->single();
					$lowres = $fullurl.'img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';
					echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic1" type="button" disabled>Upload Main Profile</button>';
					echo '<img src="'.$lowres.'" class="Profile_viewer"><span class="needs_approving">Waiting Approval</span>';
					}else{
						echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic1" type="button">Upload Main Profile</button>';
						echo '<img src="'.$fullurl.'/assets/images/profileplaceholder.png" class="Profile_viewer">';
					}?>
				</div>
				<div class="col-lg-4">
					<?if($myinfo['profilepic2']==$myinfo['profilepic2_temp'] && $myinfo['profilepic2']!=''){
							$db->query("SELECT * FROM ws_uploads where id = ?");
							$db->bind(1, $myinfo['profilepic2']);
							$db->execute();
							$img = $db->single();
						$lowres = $fullurl.'img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';

						echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic2" type="button">Upload Main Profile</button>';
						echo '<img src="'.$lowres.'" class="Profile_viewer">';
					}else if($myinfo['profilepic2_temp']!=''){
						$db->query("SELECT * FROM ws_uploads where id = ?");
						$db->bind(1, $myinfo['profilepic2_temp']);
						$db->execute();
						$img = $db->single();

						$lowres = $fullurl.'img.php?img='.$img['path'].$img['name'].'&qual=75&crop=1&size=250x250';

					echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic2" type="button" disabled>Upload Main Profile</button>';
					echo '<img src="'.$lowres.'" class="Profile_viewer"><span class="needs_approving">Waiting Approval</span>';
					}else{
						echo '<button class="btn btn-lg btn-primary btn-block" id="Uploads" data-id="profilepic2" type="button">Upload Main Profile</button>';
						echo '<img src="'.$fullurl.'/assets/images/profileplaceholder.png" class="Profile_viewer">';
					}?>
				</div>

			</div>

	</div>
</div>


</div><!--box-->

<!--
"Tajik mountains edit" by derivative work: INkubusseTajik_mountains.jpg: Ibrahimjon - Tajik_mountains.jpg. Licensed under Creative Commons Attribution 2.5 via Wikimedia Commons - http://commons.wikimedia.org/wiki/File:Tajik_mountains_edit.jpg#mediaviewer/File:Tajik_mountains_edit.jpg
-->
</div>
</div>
</div><!--wrapper-->

	<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->


	<!--[if (lt IE 9) & (!IEMobile)]>
	<script src="js/selectivizr-min.js"></script>
	<![endif]-->

<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');
?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
 <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<script src="<? echo $fullurl; ?>assets/js/backstretch.js"></script>

<script>
if ($(window).width() < 800) {
    $("#wrapper").backstretch("<? echo $fullurl; ?>assets/images/img8.jpg");
}
else{
    $("#wrapper").backstretch("<? echo $fullurl; ?>assets/images/img7.jpg");
}





$( "body" ).on( "click", "#Uploads", function() {
	$type=$(this).attr("data-id");
							$("#BaseModalLContent").html($Loading);
						  $('#BaseModalL').modal('show');
						$.ajax({
							 type: "POST",
							 url: "../../../assets/app_ajax/uploads/profile.php",
							 data: {id:'<?echo $user?>',type:$type},
							 success: function(msg){
								 //alert(msg);
								 $("#BaseModalLContent").delay(1000)
									.queue(function(n) {
											$(this).html(msg);
											n();
									}).fadeIn("slow").queue(function(n) {
															 $.material.init();
															n();
									});
								 }
					});
			});
</script>


<!-------------------------------- uploader --------------------------------->
  <!-------------------------------- uploader --------------------------------->
  <!-------------------------------- uploader --------------------------------->


  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <script src="<? echo $fullurl; ?>assets/js/load-image.all.min.js"></script>
  <script src="<? echo $fullurl; ?>assets/js/jquery.ui.widget.js"></script>
  <script src="<? echo $fullurl; ?>assets/js/tmpl.min.js"></script>

  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <script src="<? echo $fullurl; ?>assets/js/canvas-to-blob.min.js"></script>
  <!-- blueimp Gallery script -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.blueimp-gallery.min.js"></script>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.iframe-transport.js"></script>
  <!-- The basic File Upload plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload.js"></script>
  <!-- The File Upload processing plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-process.js"></script>
  <!-- The File Upload image preview & resize plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-image.js"></script>
  <!-- The File Upload audio preview plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-audio.js"></script>
  <!-- The File Upload video preview plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-video.js"></script>
  <!-- The File Upload validation plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-validate.js"></script>
  <!-- The File Upload user interface plugin -->
  <script src="<? echo $fullurl; ?>assets/js/jquery.fileupload-ui.js"></script>


  <script id="template-upload" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-upload fade">
          <td>
              <span class="preview"></span>
          </td>
          <td>
              <p class="name">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
          </td>
          <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
          </td>
          <td>
              {% if (!i && !o.options.autoUpload) { %}
                  <button class="btn btn-primary start" disabled>
                      <i class="glyphicon glyphicon-upload"></i>
                      <span>Start</span>
                  </button>
              {% } %}
              {% if (!i) { %}
                  <button class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>Cancel</span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
  </script>
  <!-- The template to display files available for download -->
  <script id="template-download" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-download fade">
          <td>
              <span class="preview">
                  {% if (file.thumbnailUrl) { %}
                      <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                  {% } %}
              </span>
          </td>
          <td>
              <p class="name">
                  {% if (file.url) { %}
                      <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                  {% } else { %}
                      <span>{%=file.name%}</span>
                  {% } %}
              </p>
              {% if (file.error) { %}
                  <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
          </td>
          <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
          </td>
          <td>
              {% if (file.deleteUrl) { %}
                  <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                      <i class="glyphicon glyphicon-trash"></i>
                      <span>Delete</span>
                  </button>
                  <input type="checkbox" name="delete" value="1" class="toggle">
              {% } else { %}
                  <button class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>Cancel</span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
  </script>
  <!-------------------------------- uploader --------------------------------->
  <!-------------------------------- uploader --------------------------------->
  <!-------------------------------- uploader --------------------------------->

<?  include($_SERVER['DOCUMENT_ROOT'].'/assets/app_modals/base_modals.php'); ?>
</body>

</html>
