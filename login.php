<?php
/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    template.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    2.5
 * @Date        23/05/2016
 */
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Log in</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="<? echo $fullurl ?>/staff-box/css/style.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="icon" href="<? echo $fullurl ?>/staff-box/images/Mini-Icon.png" type="image/png">
</head>
<body>

<div id="loginpage">
<div class="container">


<div id="mySidepanelrightnew" class="sidepanelrightnew">

<div class="logo">
<a href="index.html"><img src="<? echo $fullurl ?>/staff-box/images/Staff-Box-Logo.png" /></a>
</div>



<div class="loginleft">
 <form class="form-signin" action="<? echo $fullurl ?>assets/app_php/auth.php?action=logon" id="logon_form" enctype="application/x-www-form-urlencoded" method="post">
  <div class="container">
  <h1>Log in</h1>
    <label for="uname"><b>Username</b></label><br>
    <input type="email" class="widthnew username_signin" type="text" placeholder="Username" name="login" required><br>

    <label for="psw"><b>Password</b></label><br>
    <input type="password" class="widthnew" type="password" placeholder="**************" name="password" required><br>

    <button type="submit" class="formbtn">Log in</button><br>
    <label>
      <input type="checkbox" checked="checked" name="remember" value="1"> Remember me<br>
      <input type="hidden" name="key" value="<? echo getCurrentKey(); ?>">
      <input type="hidden" name="username">
      <input type="hidden" name="type" value="<?if(empty($_GET['type'])){echo 'rota';}else{echo $_GET['type'];}?>"/>
    </label>
  </div>
  <div class="container">
<!--    <button type="button" class="cancelbtn">Cancel</button><br>-->
    <span class="psw"> <a href="#" class="forgot_password">Forgot password?</a></span><br>
    <div class="forgot_password_text_holder">

    </div>
  </div>
</form>


<div class="loginnnnn">
<p><img src="<? echo $fullurl ?>/staff-box/images/22.png" />Need help</p>
</div>
</div>


<div class="loginright">
<img src="<? echo $fullurl ?>/staff-box/images/11.png" />
</div>


</div>





</div>
</div>







</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
$fullurl = '<?echo $fullurl;?>';
function email($input) {
  $input = $input.trim();
  var isAlphNum = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($input);
  var deciCount = $input.replace(/[^'.']/g,"").length;
  var atCount = $input.replace(/[^'@']/g,"").length;
  //if((atCount ==1) && (deciCount >=1) && (isAlphNum)){
if ((atCount ==1 && deciCount >=1 && isAlphNum) && $input.length!=0){return  0;}
  else{return 1;}
}

$(document).ready(function() {
  $( "body" ).on( "click", ".forgot_password", function() {

  var email_text = $('.username_signin').val();
  var $error = 0;

  if(email($('.username_signin').val())){
    $error=1;
  }
  if ($error == 1) {
    alert('Please make sure all required elements of the form are filled out.');
  } else {

      // $(formid).hide('slow');
      // $('.loader').html($Loader);
      // $('.loader').show('slow');

      var FRMdata = $('#logon_form').serialize(); // get form data
        $.ajax({
              type: "POST",
              url: $fullurl+"assets/app_ajax/website/forgot_password.php",
              data: FRMdata,
              success: function(msg){
                setTimeout(function(){
                  $('.forgot_password_text_holder').html('<h3 class="text-center" style="display:block;">An e-mail has been sent to <span class="color1">'+$('.username_signin').val()+'</span> if the account exists</h3>');
                },600);
             },error: function (xhr, status, errorThrown) {
                   setTimeout(function(){

                    },600);
                  }
        });
    }
  });
});



</script>
