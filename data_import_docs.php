<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 echo '<h2>Data Import</h2>';


require_once 'php-excel/PHPExcel.php';



$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setReadDataOnly(true);

$objPHPExcel = $objReader->load("php-excel/files/new_data.xlsx");
$objWorksheet = $objPHPExcel->getActiveSheet();

$highestRow = $objWorksheet->getHighestRow();
$highestColumn = $objWorksheet->getHighestColumn();

$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
echo $highestRow;

echo '<table border="1">' . "\n";
for ($row = 1; $row <= $highestRow; ++$row) {
  echo '<tr>' . "\n";

  $user_id = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
  $document_id = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
  $title = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
  $type = 'Manual';
  $filename = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
  $original_file_name = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
  $expiry_date  = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();

  $path = 'uploads/users/'.$user_id.'/'.$filename;

  $db = new database;
  $db->query("select * from ws_accounts_extra_info_import where old_id = ? ");
  $db->bind(1,$user_id);
  $account = $db->single();

  $newAccountID = $account['account_id'];

  $newUploadID = createid('img');

    echo '<td>'.$user_id.'</td>'."\n";
    echo '<td>'.$document_id.'</td>'."\n";
    echo '<td>'.$title.'</td>'."\n";
    echo '<td>'.$type.'</td>'."\n";
    echo '<td>'.$filename.'</td>'."\n";
    echo '<td>'.$original_file_name.'</td>'."\n";
    echo '<td>'.$expiry_date.'</td>'."\n";


    //Insert into accounts_import
    $db = new database;
    $db->Query("insert into ws_accounts_upload_import (id, account_id, type, name, path, status, created_date, created_by) values(?,?,?,?,?,?,?,?)");
    $db->bind(1,$newUploadID);
    $db->bind(2,$newAccountID);
    $db->bind(3,$type);
    $db->bind(4,$filename);
    $db->bind(5,$path);
    $db->bind(6,'1');
    $db->bind(7,time());
    $db->bind(8,decrypt($_SESSION['SESS_ACCOUNT_ID']));
    $db->execute();


  echo '</tr>' . "\n";
}
echo '</table>' . "\n";
