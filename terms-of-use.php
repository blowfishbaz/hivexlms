<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Staff Box</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="<? echo $fullurl ?>/staff-box/css/style.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="icon" href="<? echo $fullurl ?>/staff-box/images/Mini-Icon.png" type="image/png">
</head>

<style>
.staff-box-footer{
	position: absolute;
    left: 100%;
    transform: translate(-120%, 100%);
		max-width:200px;
}

.collab_holder{
	width: 50%;
    margin: 0 auto;
}

.collab_holder_inner{
	width: 46%;
	    float: left;
	    background-color: #f0f0f0;
	    margin: 1%;
	    border-radius: 20px;
	    padding: 1%;
			min-height: 350px;
}

.collab_holder_inner p{
	width:98%;
	line-height: 30px;
}

.footer_half{
	width:50%;
	float:left;
	height:inherit;
	color:white;
}

.footer_half img{
	max-width: 220px;
}

.pp-image{

/* margin: 0 auto; */
/* display: table; */
position: absolute;
left: 50%;
transform: translate(-50%, 0px);
}

.pp-image img{
		height: 45px;
}

@media screen and (max-width: 850px){
.pp-image{
		display: none;
	}
	.collab_holder_inner{
		float: none;
		width:100%;
		height:auto;
		min-height:0px;
		margin-top:20px;
	}

	.footer_half{
		float:none;
		width: 100%;
	}

	.footer_half p{
		text-align: center!important;
	}

	.footer_half ul{
		padding:25px!important;
	}

	.collab_holder{
		width:80%;
	}

	.footer_half img{
		max-width: 190px;
	}
}

.main-body-text{

}

.ourpartners p {
    width: 75%;
    margin: 0px auto;
    padding: 12px 0px;
		text-align:left;
}

.ourpartners li {
	width: 75%;
	margin: 0px auto;

		text-align:left;
}

.tocli{

}
</style>
<body>

<div id="site">
<div class="site-inner">

<div id="header">
<div class="container">
<div class="fullbanner">
<div class="leftside">
<div id="mySidepanel" class="sidepanel">
<a href="javascript:void(0)" class="closebtn" onClick="closeNav()">&times;</a><img class="right" src="<? echo $fullurl ?>/assets/images/mini_logo.png" />

<div class="navmenus">
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/pink.png" /><span class="menu1">rota</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/blue.png" /><span class="menu2">HR</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/green.png" /><span class="menu3">learn</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/yellow.png" /><span class="menu4">governance</span></a>
</div>
<div class="btmbtns">
<a href="/login.php" class="btn1">Log in</a>
<a href="/sign-up.php" class="btn2">Register</a>
</div>
</div>
<button class="openbtn" onClick="openNav()"><img src="<? echo $fullurl ?>/staff-box/images/toggle.png" /></button>
</div>


<a href="index.php" class="pp-image"><img src="<? echo $fullurl ?>/staff-box/images/logo.png" /></a>


<div class="rightside">
<ul>
<li><a href="tel:01511234567"><i class="fa fa-phone" aria-hidden="true"></i> 0151 1234 567</a></li>
<li><a href="mailto:info@onewirral.co.uk"><i class="fa fa-envelope" aria-hidden="true"></i>info@onewirral.co.uk</a></li>
</ul>
</div>

<!--
<div id="mySidepanelright" class="sidepanelright">
<a href="javascript:void(0)" class="closebtn" onClick="closeNavright()"><img src="images/close.png" /></a>

<div class="logo">
<a href="index2.php"><img src="<? echo $fullurl ?>/staff-box/images/Staff-Box-Logo.png" /></a>
</div>



<div class="loginleft">
 <form action="action_page.php" method="post">
  <div class="container">
  <h1>Log in</h1>
    <label for="uname"><b>Username</b></label><br>
    <input class="widthnew" type="text" placeholder="Username" name="uname" required><br>

    <label for="psw"><b>Password</b></label><br>
    <input class="widthnew" type="password" placeholder="**************" name="psw" required><br>

    <button type="submit" class="formbtn">Log in</button><br>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me<br>
    </label>
  </div>
  <div class="container">
   <button type="button" class="cancelbtn">Cancel</button><br>
    <span class="psw"> <a href="#">Forgot password?</a></span><br>
  </div>
</form>


<div class="loginnnnn">
<p><img src="<? echo $fullurl ?>/staff-box/images/22.png" />Need help</p>
</div>


</div>


<div class="loginright">
<img src="<? echo $fullurl ?>/staff-box/images/11.png" />
</div>




</div> -->


<div style="clear:both;"></div>





<div style="clear:both;"></div>
</div>

<div class="ourpartners">
<div class="container">
<h1>Terms of Use</h1>


<p>
<b>Parties</b></p><p>
	<li>(1)	One Wirral CIC (12352763) whose registered office is The Orchard Surgery, Bromborough Village Road, Birkenhead, Wirral, Merseyside, CH62 7EU (“One Wirral”, “we”, “our” or “us”)</li>
	<li>(2)	[INDIVIDUAL'S NAME] of [HOME OR BUSINESS ADDRESS] (“Applicant”, “you”)</li>


</p><p>
<b>Background</b>
<li>
	(A)	One Wirral is a Community Interest Company.
</li>
<li>
	(B)	The Applicant wishes to place a Profile (as defined below) on the Site and respond to Adverts for Sessions (both as defined below) from time to time.
</li>
<li>
	(C)	One Wirral and the Applicant have agreed to enter into this agreement in order to set out the parties’ respective rights and obligations in relation to the Applicant’s use of the Services (as defined below).
</li>





</p><p>
<b>Terms and Conditions</b>
</p><p>
These are the terms and conditions ("Terms") on which you may use the Services (as defined below). Please read these Terms carefully before you use the Services.
</p><p>
Your use of the Site is subject to One Wirral’s Terms and Conditions of Use, Acceptable Use Policy, Privacy Policy and End User Agreement which you should read before using the Site or the Services.
</p><p>
<b>Who we are and how to contact us</b>
</p><p>
The Services are provided by One Wirral.
</p><p>
To contact us, please email us using [EMAIL ADDRESS] or telephone us on [NUMBER] or use the address at the top of this document.
</p><p>
<b>Data protection</b>
</p><p>
We process personal data in accordance with our Privacy Policy which can be found by visiting our website at https://onewirral.co.uk
</p><p>
<b>Use of the Services</b>
</p><p>
The Services are only for the use of organisations delivering NHS Contracts and those individuals entitled to apply for roles (including voluntary roles) within such organisations. By signing this agreement, you confirm that you are eligible to apply for roles in NHS organisations.
</p><p>
These Terms constitute the entire agreement between us in relation to your use of the Services. You acknowledge that you have not relied on any statement, promise, representation, assurance or warranty made or given by or on behalf of ONE WIRRAL which is not set out in these Terms and that you shall have no claim for innocent or negligent misrepresentation or negligent misstatement based on any statement in these Terms.
</p><p>
If you do not accept these Terms, please do not use the Services. By using the Services, you confirm that you accept these Terms and that you agree to comply with them.
</p><p>
<b>The Services</b>
</p><p>
The Services allow GP Organisations and Primary Care Networks (“Advertisers”) to request that One Wirral post their requirements for temporary, shift cover staffing (“Adverts”) and process any responses to Adverts. Qualified nurses, general practitioners and other individuals entitled to apply for roles with Advertisers may use the Services in order to search and view these Adverts and apply for the specific shifts (“Sessions”) advertised within these Adverts. The Services allow the Applicant to create an individual profile ("Profile") and to upload evidence of their qualifications to this Profile. Such Profile and qualifications may be viewed by the Advertisers who use the Services. The Services may also provide for payment to be made by the Advertisers to the Applicant in relation to shifts completed.
</p><p>
The Applicant acknowledges that Advertisers are solely responsible for their Adverts on the Site. We are not an employer or any employment agency in respect of the Services. We do not introduce or supply work-seekers to hirers (or vice versa) or take steps to assess the suitability of an Applicant in relation to any Session or the validity of any certificates or other evidence of qualifications uploaded to the Site by the Applicant. It is the responsibility of the Applicant to take steps to identify the suitability of any Sessions, and the responsibility of Advertisers to assess the suitability of the Applicants in relation to a Session.
</p><p>
One Wirral will carry out Disclosure and Barring Service (DBS) checks on the Applicant before determining whether the Applicant will be permitted to use the Services. The Applicant will be required to pay an administration fee to cover the cost of such DBS checks.
</p><p>
We may revise these Terms at any time by posting an updated version to the Site.
</p><p>
If you do not comply with these Terms your access and use of the Services may be suspended or terminated at our discretion.
</p><p>
You confirm that you are at least 18 years of age.
</p><p>
The Services may be used by the Applicant only to seek and apply for Sessions. The performance of such Sessions may be subject to other contractual arrangements between the Advertiser and the Applicant. Although payment for Sessions may be facilitated via the Site, we are not a party to such contractual arrangements and shall have no liability in respect of the performance or non-performance by the parties of such contractual arrangements, in respect of the acts or omissions of the Advertiser or the Applicant, or in respect of any costs or expenses incurred by the Advertiser or the Applicant in relation to the formation or performance of such contractual arrangements.
</p><p>
The Applicant shall comply with all legal requirements including CQC requirements in relation to Sessions. The Applicant acknowledges that we have no control over such Sessions and shall not be liable for any failure by Applicants or Advertisers to comply with applicable CQC requirements or any other legal requirements.
</p><p>
In relation to the Services, the Applicant shall not:
</p>
<li>(a) 	use any data mining, robots or similar data gathering or extraction methods;</li>
<li>(b) 	aggregate, copy or duplicate in any manner any of the content or information available via the Services, including expired Adverts, other than as permitted by these Terms;</li>
<li>(c) 	post any Profile, or apply for any Session, on behalf of another party, without our prior written approval;</li>
<li>(d) 	access data not intended for you or log into a server or account which you are not authorised to access;</li>
<li>(e) 	delete or alter any material posted by any other user;</li>
<li>(f) 	transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam).</li>

<p>
You are responsible for maintaining the confidentiality of your account, username and password. You may not share your password or other account access information with any other party, whether temporarily or otherwise, and you shall be responsible for all uses of your registrations and passwords, whether or not such use was authorised by you. You agree to immediately notify us of any unauthorised use of your account, Profile, or passwords.
</p><p>
We have the right to disable any username or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these Terms.
</p><p>
You understand and acknowledge that if you cancel your account or your account is terminated, or after a period of inactivity, all your account information may be deleted from the system. Information may continue to be available for a period of time because of delays in effecting such deletion following the termination or cancellation of an account.
</p><p>
<b>Additional Terms</b>
</p><p>
The Applicant shall not:
</p>
<li>a)	defer or forward any contact from an Advertiser to any agent, agency, or other third party;</li>
<li>b)	post or submit any incomplete, false or inaccurate biographical information or information relating to your experience or qualifications;</li>
<li>c)	post any CV which is not a genuine CV or which attempts to advertise or promote products or services;</li>
<li>d)	post or otherwise provide any falsified, forged or otherwise misleading certificates or other evidence of qualifications;</li>
<li>e)	allow any information posted to remain posted where that information subsequently becomes incorrect or incomplete;</li>
<li>f)	maintain more than one Profile without our prior written consent;</li>
<li>g)	apply for a Session if you do not have the appropriate skills, insurance cover, qualifications or experience for such Session, or where you are unwilling or unable to adhere to any policies or other obligations referred to or set out within the relevant Advert which relates to such Session;</li>
<li>h)	attempt to deceive the Advertiser or ONE WIRRAL in any way.</li>

<p>
<b>Content Standards</b>
</p><p>
All content and other material posted to the Site (including Adverts and Profiles) or sent via the Services must:
</p>
<li>Be accurate (where it states facts).</li>
<li>Be genuinely held (where it states opinions).</li>
<li>Comply with the law applicable in England and Wales and in any country from which it is sent or posted.</li>
<p>
Such content and material must not:
</p>
<li>Be defamatory of any person.</li>
<li>Be obscene, offensive, hateful or inflammatory.</li>
<li>Bully, insult, intimidate or humiliate.</li>
<li>Promote sexually explicit material.</li>
<li>Promote discrimination based on race, sex, religion, nationality, disability, sexual orientation or age.</li>
<li>Infringe any copyright, database right, trade mark or other intellectual property right of any other person.</li>
<li>Be likely to deceive any person.</li>
<li>Breach any legal duty owed to a third party, such as a contractual duty or a duty of confidence.</li>
<li>Promote any illegal content or activity.</li>
<li>Be in contempt of court.</li>
<li>Be threatening, abuse or invade another's privacy, or cause annoyance, inconvenience or needless anxiety.</li>
<li>	Impersonate any person or misrepresent your identity or affiliation with any person.</li>
<li>Advocate, promote, incite any party to commit, or assist any unlawful or criminal act such as (by way of example only) copyright infringement or computer misuse.</li>
<li>Contain any advertising or promote any services or web links to other sites.</li>

<p>
We reserve the right to remove any Advert, Profile or other content from the Site which in our discretion does not comply with the above Terms, or if any content is posted that we believe could damage the reputation of One Wirral, the Site or the Services.
</p><p>
<b>Invoicing for Sessions</b>
</p><p>
Staffbox will automatically send an invoice for sessions completed to the advertiser on behalf of the applicant. One Wirral shall have no liability in respect of such payments. Any disputes regarding payment shall be resolved between the Advertiser and the Applicant.
</p><p>
<b>Adverts for One Wirral roles </b>
</p><p>
The Applicant  acknowledges that, in some cases, Adverts may be posted by One Wirral on their own behalf and not on behalf of another organisation. In such cases, contractual arrangements will be put in place between One Wirral and the Applicant relating to the relevant Session.
</p><p>
<b>We may make changes to the Services</b>
</p><p>
We may update and change our Services from time to time.
</p><p>
<b>We may suspend or withdraw the Services</b>
</p><p>
We do not guarantee that the Services or the Site, or any content on it, will always be available or be uninterrupted or error-free. We may suspend or withdraw or restrict the availability of all or any part of the Services for business and operational reasons. We will try to give you reasonable notice of any suspension or withdrawal.
</p><p>
You are also responsible for ensuring that all persons who access the Services through your internet connection are aware of these Terms and other applicable terms and conditions, and that they comply with them.
</p><p>
We do not guarantee that you will be accepted for all or any of the Sessions for which you apply, or that Sessions will be available via the Service at any given time or at all.
</p><p>
<b>We may transfer this agreement to someone else</b>
</p><p>
We may transfer our rights and obligations under these Terms to another organisation. We will always tell you in writing if this happens and we will ensure that the transfer will not affect your rights under these Terms.
</p><p>
<b>You need our consent to transfer your rights to someone else</b>
</p><p>
You may only transfer your rights or your obligations under these Terms to another person if we agree in writing.
</p><p>
<b>The Services are only for users in the UK</b>
</p><p>
The Services are directed to people residing in the United Kingdom. We do not represent that content available on or through the Site and/or Services is appropriate for use or available in other locations.
</p><p>
<b>How you may use material accessed via the Services </b>
</p><p>
We are the owner or the licensee of all intellectual property rights in the Services. These rights are protected by intellectual property laws and treaties around the world. All such rights are reserved. You may use the content which appears on the Site in accordance with the provisions of this agreement and Staffbox Rota’s Terms and Conditions of Use.
</p><p>

You must not use any part of the content which comprises the Services for commercial purposes without obtaining a licence to do so from us or our licensors.
</p><p>
<b>Do not rely on information on this Site</b>
</p><p>
You acknowledge that we have no control over the content or other material uploaded by users, and that such content and materials have not been verified or approved by us. We do not represent or guarantee the truthfulness, accuracy, or reliability of such content or any other communications posted by users and will have no liability in relation thereto. The views expressed by users do not represent our views or values.
Commentary and other material posted via the Services are not intended to amount to advice on which reliance should be placed. We make no representations, warranties or guarantees, whether express or implied, that the content and material that comprises the Services is accurate, complete or up to date.
</p><p>
<b>How to complain about content uploaded by other Users</b>
</p><p>
If you wish to complain about content uploaded by other users, please contact us using the details set out above. We may in our sole discretion investigate such complaints and determine whether to remove or request the removal of the content, however we have no liability or responsibility to users for performance or non-performance of such activities.
</p><p>
<b>We are not responsible for external websites</b>
</p><p>
Where the Site contains links to other sites and resources provided by third parties, these links are provided for your information only. Such links should not be interpreted as approval by us of those linked websites or information you may obtain from them. We have no control over the contents of those sites or resources.
</p><p>
<b>Our responsibility for loss or damage suffered by you</b>
</p><p>
You acknowledge that the Services are provided on an "as is" basis without any warranties of any kind. You acknowledge that the Services have not been developed to meet your individual requirements and that it is therefore your responsibility to ensure that the facilities and functions of the Services meet your requirements.
</p><p>

We do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.
</p><p>

If you are a business user, we exclude all implied conditions, warranties, representations or other terms that may apply to the Services or any content which comprises the Services.
</p><p>

We will not be liable to you for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:
</p>
<li>use of, or inability to use, the Services or the Site; or</li>
<li>use of or reliance on any content displayed within the Services or on the Site.</li>
<p>
In particular, we will not be liable for:
</p>
<li>loss of profits, sales, business, or revenue;</li>
<li>business interruption;</li>
<li>loss or corruption of data or information;</li>
<li>loss of anticipated savings;</li>
<li>loss of business opportunity, goodwill or reputation; or</li>
<li>any indirect or consequential loss or damage.</li>
<p>
Subject to the foregoing, our maximum aggregate liability under or in connection with these Terms whether in contract, tort (including negligence) or otherwise, shall in all circumstances be limited to £100 (one hundred pounds).
</p><p>
<b>Uploading content to the Site</b>
</p><p>
You warrant that any content and materials which you upload in relation to the Services and/or to the Site complies with the Content Standards set out above, and you will be liable to us and indemnify us for any breach of that warranty. This means you will be responsible for any loss or damage we suffer as a result of your breach of warranty.
</p><p>

Any content you upload to a Profile or Advert will be considered non-confidential and non-proprietary. You retain all of your ownership rights in your content, but you are required to grant us and other users of the Services a limited licence to use, store and copy that content and to distribute and make it available to third parties.
</p><p>

We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to the Site constitutes a violation of their intellectual property rights, their right to privacy, or any other rights.
</p><p>

We have the right to remove any Advert, Profile or communication if, in our opinion, it does not comply with the Content Standards set out above, however, we do not assume any obligation to do so and to the fullest extent permitted by law, disclaim any liability for failing to take any such action.
</p><p>

You are solely responsible for securing and backing up your content.
</p><p>

We do not store terrorist content.
</p><p>
<b>Rights you are giving us to use material you upload</b>
</p><p>
When you upload or post content to the Site or via the Services, you grant us the following rights to use that content:
</p><p>

a worldwide, non-exclusive, royalty-free, transferable licence to use, reproduce, distribute, prepare derivative works of, display, and perform that user-generated content in connection with the Services provided by the Site and across different media including to promote the Site or the Services forever; and
</p><p>

a worldwide, non-exclusive, royalty-free, transferable licence for other users, partners or advertisers to use the content in accordance with the functionality of the Site forever.
</p><p>
<b>We are not responsible for viruses and you must not introduce them</b>
</p><p>
We do not guarantee that the Site or the Services will be secure or free from bugs or viruses.
</p><p>

You are responsible for configuring your information technology, computer programmes and platform to access the Services and the Site. You should use your own virus protection software.
</p><p>

You must not misuse the Services or the Site by knowingly introducing viruses, trojans, worms, logic bombs or other material that is malicious or technologically harmful. You must not attempt to gain unauthorised access to the Services or the Site, the server on which the Site is stored or any server, computer or database connected to the Site. You must not attack the Site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use the Services will cease immediately.
</p><p>
<b>Rules about linking to the Site</b>
</p><p>
You will find rules regarding linking to the Site in Staffbox Rota’s Terms and Conditions of Use. In addition, you must not establish a link to the Services in such a way as to suggest any form of association, approval or endorsement on our part where none exists, and you must not establish a link to the Services in any website that is not owned by you.
</p><p>
<b>Indemnity</b>
</p><p>
You agree, both during the term of our agreement with you and thereafter, to defend, indemnify and hold harmless One Wirral against claims, actions, proceedings, losses, damages, expenses and costs (including without limitation court costs and reasonable legal fees) arising out of or in connection with an actual or alleged breach by you of any of the provisions of these Terms.
</p><p>
<b>General</b>
</p><p>
If any provision or part-provision of these Terms is or becomes invalid, illegal or unenforceable, it shall be deemed deleted, but that shall not affect the validity and enforceability of the rest of these Terms. No waiver of any provision of these Terms shall be deemed a further or continuing waiver of such provision or any other provision. In addition, One Wirral’s failure to enforce any provision of these Terms shall not be deemed as a waiver of such provision or otherwise affect One Wirral’s ability to enforce such provision at any point in the future. No variation of this agreement shall be effective unless it is in writing and signed by the parties (or their authorised representatives). No one other than a party to this agreement, shall have any right to enforce any of its terms.
</p><p>
<b>Commencement and Termination</b>
</p><p>
This agreement shall commence on the date when it has been signed by all the parties and shall continue in full force and effect until terminated as provided in this agreement.
</p><p>

Either party may terminate this agreement by giving 7 days’ written notice to the other party.
</p><p>

Termination of this agreement shall not affect any rights, remedies, obligations or liabilities of the parties that have accrued up to the date of termination or expiry, including the right to claim damages in respect of any breach of the agreement which existed at or before the date of termination or expiry.
</p><p>
<b>Confidentiality</b>
</p><p>
Each party undertakes that it shall not at any time disclose to any person any confidential information concerning the business, affairs, customers, clients or suppliers of the other party, except as permitted by this section.
</p><p>

Each party may disclose the other party's confidential information:
</p>
<li>(a)	to its employees, officers, representatives, contractors, subcontractors or advisers who need to know such information for the purposes of exercising the party's rights or carrying out its obligations under or in connection with this agreement. Each party shall ensure that its employees, officers, representatives, contractors, subcontractors or advisers to whom it discloses the other party's confidential information comply with this section; and</li>
<li>(b)	as may be required by law, a court of competent jurisdiction or any governmental or regulatory authority.</li>



</p><p><p>
No party shall use any other party's confidential information for any purpose other than to exercise its rights and perform its obligations under or in connection with this agreement.
</p><p>
<b>Which country's laws apply to any disputes?</b>
</p><p>
These Terms, their subject matter and their formation (and any non-contractual disputes or claims) are governed by English law. We both agree to the exclusive jurisdiction of the courts of England and Wales.

</p>

</div><div style="clear:both;"></div></div>


<div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
</div>

















<div style="clear:both;"></div>

<div class="footer" style="min-height:100px; background-color:black;">

	<div class="footer_half">
		<ul style="padding:50px;">
			<li style="margin-bottom:20px;"><img src="<? echo $fullurl ?>/staff-box/images/email_white.png" style="float:left;" /> <a href="mailto:info@onewirral.co.uk" style="color:white; padding-left:20px;"> info@onewirral.co.uk</a><div style="clear:both;"></div></li>
			<li style="margin-bottom:20px;"><img src="<? echo $fullurl ?>/staff-box/images/phone_white.png" style="float:left;" /> <p style="float:left; padding-left:20px;">0151 294 3322</p><div style="clear:both;"></div></li>
			<li><img src="<? echo $fullurl ?>/staff-box/images/location_white.png" style="float:left;" /> <p style="float:left; padding-left:20px;"> One Wirral CIC, Orchard Surgery, Brombrough Village Rd, <br />Brombrough, CH62 7EU</p><div style="clear:both;"></div></li>
		</ul>
	</div>
	<div class="footer_half">

		<p style="text-align:right;margin-top: 10px; margin-right: 20px;"><img src="<? echo $fullurl ?>/staff-box/images/Staff_Box_Logo_White.png" class="xstaff-box-footer" /> <br /> &copy; Staffbox Copyright <?echo date('Y');?> </p>
		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>
  <div style="padding-bottom:20px;">
		<div style="margin:0 auto; display:table;">
			<a href="privacy-policy.php" style="color:white; padding-right:20px;">Privacy Policy</a>
			<a href="terms-of-use.php" style="color:white;">Terms of Use</a>
		</div>
  </div>



<div style="clear:both;"></div>
<div style="width:100%;color:white;">

</div>
<div style="clear:both;"></div>
</div>







</div>
</div>





</div>
</div>
</body>
</html>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>





<script>
var width = $(window).width();

if(width > 500){
  function openNav() {
    document.getElementById("mySidepanel").style.width = "350px";
  }
}else{
  function openNav() {
    document.getElementById("mySidepanel").style.width = "100%";
  }
}



function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}
function openNavright() {
  document.getElementById("mySidepanelright").style.width = "1050px";
}

function closeNavright() {
  document.getElementById("mySidepanelright").style.width = "0";
}


</script>







	<script>
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 20
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	</script>
