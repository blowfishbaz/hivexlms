
<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');


 $id = $_GET['id'];

 $db->Query("select * from ws_booked_on where id = ?");
 $db->bind(1,$id);
 $data = $db->single();

 $db->Query("select * from ws_course_statements where course_id = ? and account_id = ? and result_completion = ? order by id desc limit 1");
 $db->bind(1,$data['course_id']);
 $db->bind(2,$data['account_id']);
 $db->bind(3,'1');
 $result = $db->single();

 if(!empty($result)){
   $db->Query("update ws_booked_on set course_status = 4, completed_date = ?, pass_score = ? where id = ?");
   $db->bind(1,time());
   $db->bind(2,$result['score_scaled']);
   $db->bind(3,$id);
   $db->execute();


 }else{
   $db->Query("update ws_booked_on set course_status = 4, completed_date = ?, pass_score = ? where id = ?");
   $db->bind(1,time());
   $db->bind(2,'0');
   $db->bind(3,$id);
   $db->execute();
 }



echo 'ok';
