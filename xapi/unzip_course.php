<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/

//ob_start();
 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

//This could be a cron job which runs and does this???? Yeah
//echo $_SERVER['DOCUMENT_ROOT'];

$db->Query("select * from ws_courses_content where status = 2");
$data = $db->resultset();

// echo '<pre>';
// var_dump($data);
// echo '</pre>';


if(!empty($data)){


  foreach ($data as $d) {
    $path = $d['zip_path'];
    $name = $d['zip_name'];
    $final = $_SERVER['DOCUMENT_ROOT'].$path.$name;
    $extract_url = $_SERVER['DOCUMENT_ROOT'].'/uploads/xapi/courses/'.$d['id'].'';
    $extract_url_final = '/uploads/xapi/courses/'.$d['id'].'/'.str_replace(".zip", "", $name).'/';

    //echo $final;

    $zip = new ZipArchive;
     $res = $zip->open($final);
     if ($res === TRUE) {
       $zip->extractTo($extract_url);
       $zip->close();

       $tincanxml_file = $_SERVER['DOCUMENT_ROOT'].$extract_url_final.'tincan.xml';
       $tincanxmlstring = trim(file_get_contents($tincanxml_file));
       $tincanxml = simplexml_load_string($tincanxmlstring);
       $launch_url = $tincanxml->activities->activity->launch;

       if(empty($launch_url)){
         $launch_url = 'story.html';
       }

       $activity_id = $tincanxml->activities->activity['id'];

       //Update to allow the online course to work
       $db->Query("update ws_courses_content set modified_date = ?, modified_by = ?, status = ?, activity_id = ?, launch_path = ?, launch_name = ? where id = ?");
       $db->bind(1,time());
       $db->bind(2,decrypt($_SESSION['SESS_ACCOUNT_ID']));
       $db->bind(3,'1');
       $db->bind(4,$activity_id);
       $db->bind(5,$extract_url_final);
       $db->bind(6,$launch_url);
       $db->bind(7,$d['id']);
       $db->execute();
     }else{

       $zip->close();
       //Update to say there was an error with this - maybe we should send an email to say that it did not work as well?
       $db->Query("update ws_courses_content set modified_date = ?, modified_by = ?, status = ?, activity_id = ? where id = ?");
       $db->bind(1,time());
       $db->bind(2,decrypt($_SESSION['SESS_ACCOUNT_ID']));
       $db->bind(3,'99');
       $db->bind(4,'');
       $db->bind(5,$d['id']);
       $db->execute();
     }

  }

}
