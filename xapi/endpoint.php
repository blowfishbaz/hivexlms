
<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    tab_tasks.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/web_application.php');

 $method = $_SERVER['REQUEST_METHOD'];

 $course_id = $_GET['course_id'];
 $user_id = $_GET['user_id'];


 $statment_id = '';
 $registration= '';
 $agent_name = '';
 $agent_id = '';
 $agent_mail = '';
 $verb_id = '';
 $verb = '';
 $object_id = '';
 $object_type = '';
 $object_def_type = '';
 $object_name = '';
 $object_desc = '';
 $created_date = time();
 $score_raw = '0';
 $score_scalled = '0';
 $score_min = '0';
 $score_max = '';
 $result_completion = '';
 $result_success = '';
 $result_duration = '';
 $statement = '';

 if ('PUT' === $method) {
      $request_body = file_get_contents('php://input');
      $data = json_decode($request_body);

      if(!empty($data->id)){
            $statment_id = $data->id;
      }
      if(!empty($data->context->registration)){
            $registration = $data->context->registration;
      }
      if(!empty($data->actor->name)){
            $agent_name = $data->actor->name;
      }
      if(!empty($data->actor->mbox)){
            $agent_id = $data->actor->mbox;
            $agent_mail = $data->actor->mbox;
      }
      if(!empty($data->verb->id)){
            $verb_id = $data->verb->id;
      }
      if(!empty($data->verb->display->{"en-US"})){
            $verb = $data->verb->display->{"en-US"};
      }
      if(!empty($data->object->id)){
            $object_id = $data->object->id;
      }
      if(!empty($data->object->objectType)){
            $object_type = $data->object->objectType;
      }
      if(!empty($data->object->definition->type)){
            $object_def_type = $data->object->definition->type;
      }
      if(!empty($data->object->definition->name->und)){
            $object_name = $data->object->definition->name->und;
      }
      if(!empty($data->object->definition->description->und)){
            $object_desc = $data->object->definition->description->und;
      }

      if($verb == 'answered'){
            $score_raw = $data->result->score->raw;

            if(!empty($data->result->success)){
                  $result_success = 'true';
            }else{
                  $result_success = 'false';
            }
      }
      if(!empty($data->result->score->scaled)){
            $score_scalled = $data->result->score->scaled;
      }

      $result_completion = $data->result->completion;



      $result_duration = $data->result->duration;







      if(!empty($statment_id)){
            $db = new database;
            $db->Query("insert into ws_course_statements (
                        statement_id,
                        registration,
                        agent_name,
                        agend_id,
                        agent_mail,
                        verb_id,
                        verb,
                        objectid,
                        object_objecttype,
                        object_definition_type,
                        object_definition_name,
                        object_definition_description,
                        created_date,
                        score_raw,
                        score_scaled,
                        score_min,
                        score_max,
                        result_completion,
                        result_success,
                        result_duration,
                        statement,
                        course_id,
                        account_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $db->bind(1,$statment_id);
            $db->bind(2,$registration);
            $db->bind(3,$agent_name);
            $db->bind(4,$agent_id);
            $db->bind(5,$agent_mail);
            $db->bind(6,$verb_id);
            $db->bind(7,$verb);
            $db->bind(8,$object_id);
            $db->bind(9,$object_type);
            $db->bind(10,$object_def_type);
            $db->bind(11,$object_name);
            $db->bind(12,$object_desc);
            $db->bind(13,$created_date);
            $db->bind(14,$score_raw);
            $db->bind(15,$score_scalled);
            $db->bind(16,$score_min);
            $db->bind(17,$score_max);
            $db->bind(18,$result_completion);
            $db->bind(19,$result_success);
            $db->bind(20,$result_duration);
            $db->bind(21,$statement);
            $db->bind(22,$course_id);
            $db->bind(23,$user_id);
            //$db->execute();
      }


}
