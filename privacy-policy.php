<?php

/**
 *
 *
 *
	*  __    __   __  ____    ____  _______ ___   ___
	* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
	* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
	* |   __   | |  |   \      /   |   __|    >   <
	* |  |  |  | |  |    \    /    |  |____  /  .  \
	* |__|  |__| |__|     \__/     |_______|/__/ \__\
 *
 *						BMS System
 *
 *
 *
 * @Filename    customers/index.php
 * @author     Graham Palfreyman
 * @copyright  1997-2015 Blowfish Technology Ltd
 * @version    1
 * @Date        23/05/2016
 */



 include($_SERVER['DOCUMENT_ROOT'].'/application.php');

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Staff Box</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="<? echo $fullurl ?>/staff-box/css/style.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="icon" href="<? echo $fullurl ?>/staff-box/images/Mini-Icon.png" type="image/png">
</head>

<style>
.staff-box-footer{
	position: absolute;
    left: 100%;
    transform: translate(-120%, 100%);
		max-width:200px;
}

.collab_holder{
	width: 50%;
    margin: 0 auto;
}

.collab_holder_inner{
	width: 46%;
	    float: left;
	    background-color: #f0f0f0;
	    margin: 1%;
	    border-radius: 20px;
	    padding: 1%;
			min-height: 350px;
}

.collab_holder_inner p{
	width:98%;
	line-height: 30px;
}

.footer_half{
	width:50%;
	float:left;
	height:inherit;
	color:white;
}

.footer_half img{
	max-width: 220px;
}

.pp-image{

/* margin: 0 auto; */
/* display: table; */
position: absolute;
left: 50%;
transform: translate(-50%, 0px);
}

.pp-image img{
		height: 45px;
}

@media screen and (max-width: 850px){
.pp-image{
		display: none;
	}
	.collab_holder_inner{
		float: none;
		width:100%;
		height:auto;
		min-height:0px;
		margin-top:20px;
	}

	.footer_half{
		float:none;
		width: 100%;
	}

	.footer_half p{
		text-align: center!important;
	}

	.footer_half ul{
		padding:25px!important;
	}

	.collab_holder{
		width:80%;
	}

	.footer_half img{
		max-width: 190px;
	}
}

.main-body-text{

}

.ourpartners p {
    width: 75%;
    margin: 0px auto;
    padding: 12px 0px;
		text-align:left;
}

.ourpartners li {
	width: 75%;
	margin: 0px auto;

		text-align:left;
}

.tocli{

}
</style>
<body>

<div id="site">
<div class="site-inner">

<div id="header">
<div class="container">
<div class="fullbanner">
<div class="leftside">
<div id="mySidepanel" class="sidepanel">
<a href="javascript:void(0)" class="closebtn" onClick="closeNav()">&times;</a><img class="right" src="<? echo $fullurl ?>/assets/images/mini_logo.png" />

<div class="navmenus">
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/pink.png" /><span class="menu1">rota</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/blue.png" /><span class="menu2">HR</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/green.png" /><span class="menu3">learn</span></a>
<a href="#"><img src="<? echo $fullurl ?>/staff-box/images/yellow.png" /><span class="menu4">governance</span></a>
</div>
<div class="btmbtns">
<a href="/login.php" class="btn1">Log in</a>
<a href="/sign-up.php" class="btn2">Register</a>
</div>
</div>
<button class="openbtn" onClick="openNav()"><img src="<? echo $fullurl ?>/staff-box/images/toggle.png" /></button>
</div>


<a href="index.php" class="pp-image"><img src="<? echo $fullurl ?>/staff-box/images/logo.png" /></a>


<div class="rightside">
<ul>
<li><a href="tel:01511234567"><i class="fa fa-phone" aria-hidden="true"></i> 0151 1234 567</a></li>
<li><a href="mailto:info@onewirral.co.uk"><i class="fa fa-envelope" aria-hidden="true"></i>info@onewirral.co.uk</a></li>
</ul>
</div>

<!--
<div id="mySidepanelright" class="sidepanelright">
<a href="javascript:void(0)" class="closebtn" onClick="closeNavright()"><img src="images/close.png" /></a>

<div class="logo">
<a href="index2.php"><img src="<? echo $fullurl ?>/staff-box/images/Staff-Box-Logo.png" /></a>
</div>



<div class="loginleft">
 <form action="action_page.php" method="post">
  <div class="container">
  <h1>Log in</h1>
    <label for="uname"><b>Username</b></label><br>
    <input class="widthnew" type="text" placeholder="Username" name="uname" required><br>

    <label for="psw"><b>Password</b></label><br>
    <input class="widthnew" type="password" placeholder="**************" name="psw" required><br>

    <button type="submit" class="formbtn">Log in</button><br>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me<br>
    </label>
  </div>
  <div class="container">
   <button type="button" class="cancelbtn">Cancel</button><br>
    <span class="psw"> <a href="#">Forgot password?</a></span><br>
  </div>
</form>


<div class="loginnnnn">
<p><img src="<? echo $fullurl ?>/staff-box/images/22.png" />Need help</p>
</div>


</div>


<div class="loginright">
<img src="<? echo $fullurl ?>/staff-box/images/11.png" />
</div>




</div> -->


<div style="clear:both;"></div>





<div style="clear:both;"></div>
</div>

<div class="ourpartners">
<div class="container">
<h1>Privacy Policy</h1>

<p>

	Thank you for choosing to be part of our community at One Wirral ("company", "we", "us", "our"). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about our policy, or our practices with regards to your personal information, please contact us at info@onewirral.co.uk
</p><p>
	When you visit our website http://onewirral.co.uk/, and use our services, you trust us with your personal information. We take your privacy very seriously. In this privacy notice, we describe our privacy policy. We seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy policy that you do not agree with, please discontinue use of our Sites and our services.
	</p><p>
	This privacy policy applies to all information collected through our website (such as http://onewirral.co.uk/), and/or any related services, sales, marketing or events (we refer to them collectively in this privacy policy as the "Sites").
	</p><p>
	Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.
	</p><p>
	<b>TABLE OF CONTENTS</b></p>

		<li style="list-style-type: decimal;"><a href="#scroll_1">WHAT INFORMATION DO WE COLLECT?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_2">HOW DO WE USE YOUR INFORMATION?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_3">WILL YOUR INFORMATION BE SHARED WITH ANYONE?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_4">WHO WILL YOUR INFORMATION BE SHARED WITH?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_5">DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_6">DO WE USE GOOGLE MAPS?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_7">HOW LONG DO WE KEEP YOUR INFORMATION?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_8">HOW DO WE KEEP YOUR INFORMATION SAFE?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_9">DO WE COLLECT INFORMATION FROM MINORS?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_0">WHAT ARE YOUR PRIVACY RIGHTS?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_1">CONTROLS FOR DO-NOT-TRACK FEATURES</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_12">DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_13">DO WE MAKE UPDATES TO THIS NOTICE?</a></li>
		<li style="list-style-type: decimal;"><a href="#scroll_14">HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</a></li>



<p>

<b id="scroll_1">	1. WHAT INFORMATION DO WE COLLECT? </b>
</p><p>
	Personal information you disclose to us
</p><p>
	In Short:  We collect personal information that you provide to us such as name, address, contact information, passwords and security data, and payment information. If you use our Staffbox system we will collect what information you choose to disclose, such as CV and qualifications.
</p><p>
	We collect personal information that you voluntarily provide to us when expressing an interest in obtaining information about us or our products and services, when participating in activities on the Sites (such as posting messages in our online forums or entering competitions, contests or giveaways) or otherwise contacting us.
</p><p>
	The personal information that we collect depends on the context of your interactions with us and the Sites, the choices you make and the products and features you use. The personal information we collect may include the following:
</p><p>
	Name and Contact Data.  We collect your first and last name, email address, postal address, phone number, and other similar contact data.
</p><p>
	Credentials.  We collect passwords, password hints, and similar security information used for authentication and account access. We will also collect CV, qualifications and training information when you use Staffbox service.
</p><p>
	Payment Data. We collect data necessary to process your payment if you make purchases, such as your payment instrument number (such as a credit card number), and the security code associated with your payment instrument. All payment data is stored by our payment processor and you should review its privacy policies and contact the payment processor directly to respond to your questions.
</p><p>
	All personal information that you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.
</p><p>
	Information automatically collected
</p><p>
	In Short:  Some information — such as IP address and/or browser and device characteristics — is collected automatically when you visit our Sites.
</p><p>
	We automatically collect certain information when you visit, use or navigate the Sites. This information does not reveal your specific identity (like your name or contact information) but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Sites and other technical information. This information is primarily needed to maintain the security and operation of our Sites, and for our internal analytics and reporting purposes.
</p><p>
	Like many businesses, we also collect information through cookies and similar technologies.
</p><p>
	Information collected from other sources
</p><p>
	In Short:  We may collect limited data from public databases, marketing partners, and other outside sources.
</p><p>
	We may obtain information about you from other sources, such as public databases, joint marketing partners, as well as from other third parties. Examples of the information we receive from other sources include: social media profile information; marketing leads and search results and links, including paid listings (such as sponsored links).
</p><p></p><p>
	<b id="scroll_2">2. HOW DO WE USE YOUR INFORMATION?</b>
</p><p>
	In Short:  We process your information for purposes based on legitimate business interests, the fulfilment of our contract with you, compliance with our legal obligations, and/or your consent.
</p><p>
	We use personal information collected via our Sites for a variety of business purposes described below. We process your personal information for these purposes in reliance on our legitimate business interests ("Business Purposes"), in order to enter into or perform a contract with you ("Contractual"), with your consent ("Consent"), and/or for compliance with our legal obligations ("Legal Reasons"). We indicate the specific processing grounds we rely on next to each purpose listed below.
</p><p>
	We use the information we collect or receive:</p>
	<li>To send you marketing and promotional communications. We and/or our third party marketing partners may use the personal information you send to us for our marketing purposes, if this is in accordance with your marketing preferences. You can opt-out of our marketing emails at any time (see the "WHAT ARE YOUR PRIVACY RIGHTS" below).</li>
	<li>To send administrative information to you. We may use your personal information to send you product, service and new feature information and/or information about changes to our terms, conditions, and policies.</li>
	<li>Fulfil and manage your orders. We may use your information to fulfil and manage your orders, payments, returns, and exchanges made through the Sites.</li>
	<li>Deliver targeted advertising to you. We may use your information to develop and display content and advertising (and work with third parties who do so) tailored to your interests and/or location and to measure its effectiveness. For more information.</li>
	<li>For other Business Purposes. We may use your information for other Business Purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Sites, products, services, marketing and your experience.</li>
<p></p><p>
	<b id="scroll_3">3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</b>
</p><p>
	In Short:  We only share information with your consent, to comply with laws, to provide you with services, to protect your rights, or to fulfil business obligations.
</p><p>
	We may process or share your data that we hold based on the following legal basis:</p>

	<li>Consent: We may process your data if you have given us specific consent to use your personal information for a specific purpose.</li>
	<li>Legitimate Interests: We may process your data when it is reasonably necessary to achieve our legitimate business interests.</li>
	<li>Performance of a Contract: Where we have entered into a contract with you, we may process your personal information to fulfil the terms of our contract.</li>
	<li>Legal Obligations: We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).</li>
	<li>Vital Interests: We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.
	More specifically, we may need to process your data or share your personal information in the following situations:</li>
	<li>Business Transfers. We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.</li>
	<li>Vendors, Consultants and Other Third-Party Service Providers. We may share your data with third party vendors, service providers, contractors or agents who perform services for us or on our behalf and require access to such information to do that work. Examples include: payment processing, data analysis, email delivery, hosting services, customer service and marketing efforts. We may allow selected third parties to use tracking technology on the Sites, which will enable them to collect data about how you interact with the Sites over time. This information may be used to, among other things, analyse and track data, determine the popularity of certain content and better understand online activity. Unless described in this Policy, we do not share, sell, rent or trade any of your information with third parties for their promotional purposes.</li>
<p></p><p>
	<b id="scroll_4">4. WHO WILL YOUR INFORMATION BE SHARED WITH?</b>
</p><p>
	In Short:  We only share information with the following third parties.
</p><p>
	We only share and disclose your information with the following third parties. We have categorised each party so that you may be easily understand the purpose of our data collection and processing practices. If we have processed your data based on your consent and you wish to revoke your consent, please contact us.</p>
	<li>Content Optimisation
	YouTube video embed, Google Fonts and MailChimp widget plugin</li>
	<li>Functionality and Infrastructure Optimisation
	Termly.io</li>
	<li>Invoice and Billing
	Stripe</li>
	<li>Retargeting Platforms
	Facebook Custom Audience</li>
	<li>Social Media Sharing and Advertising
	Facebook advertising</li>
	<li>User Account Registration and Authentication
	MailChimp OAuth2</li>
	<li>Web and Mobile Analytics
	Google Analytics
	Blowfish Technology</li>
<p></p><p>
	<b id="scroll_5">5. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</b>
</p><p>
	In Short:  We may use cookies and other tracking technologies to collect and store your information.
</p><p>
	We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information.
</p><p></p><p>
	<b id="scroll_6">6. DO WE USE GOOGLE MAPS?</b>
</p><p>
	In Short:  Yes, we use Google Maps for the purpose of providing better service.
</p><p>
	This website, mobile application, or Facebook application uses Google Maps APIs. You may find the Google Maps APIs Terms of Service here. To better understand Google’s Privacy Policy, please refer to this link.
</p><p>
	By using our Maps API Implementation, you agree to be bound by Google’s Terms of Service.
</p><p></p><p>
	<b id="scroll_7">7. HOW LONG DO WE KEEP YOUR INFORMATION?</b>
</p><p>
	In Short:  We keep your information for as long as necessary to fulfil the purposes outlined in this privacy policy unless otherwise required by law.
</p><p>
	We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy, unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). No purpose in this policy will require us keeping your personal information for longer than 1 year.
</p><p>
	When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.
</p><p></p><p>
	<b id="scroll_8">8. HOW DO WE KEEP YOUR INFORMATION SAFE?</b>
</p><p>
	In Short:  We aim to protect your personal information through a system of organisational and technical security measures.
</p><p>
	We have implemented appropriate technical and organisational security measures designed to protect the security of any personal information we process. However, please also remember that we cannot guarantee that the internet itself is 100% secure. Although we will do our best to protect your personal information, transmission of personal information to and from our Sites is at your own risk. You should only access the services within a secure environment.
</p><p></p><p>
	<b id="scroll_9">9. DO WE COLLECT INFORMATION FROM MINORS?</b>
</p><p>
	In Short:  We do not knowingly collect data from or market to children under 18 years of age.
</p><p>
	We do not knowingly solicit data from or market to children under 18 years of age. By using the Sites, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the Sites. If we learn that personal information from users less than 18 years of age has been collected, we will deactivate the account and take reasonable measures to promptly delete such data from our records. If you become aware of any data we may have collected from children under age 18, please contact us at natalie.young-calvert@nhs.net.
</p><p></p><p>
<b  id="scroll_10">	10. WHAT ARE YOUR PRIVACY RIGHTS?</b>
</p><p>
	In Short:  In some regions, such as the European Economic Area, you have rights that allow you greater access to and control over your personal information. You may review, change, or terminate your account at any time.
</p><p>
	In some regions (like the European Economic Area), you have certain rights under applicable data protection laws. These may include the right (i) to request access and obtain a copy of your personal information, (ii) to request rectification or erasure; (iii) to restrict the processing of your personal information; and (iv) if applicable, to data portability. In certain circumstances, you may also have the right to object to the processing of your personal information. To make such a request, please use the contact details provided below. We will consider and act upon any request in accordance with applicable data protection laws.
</p><p>
	If we are relying on your consent to process your personal information, you have the right to withdraw your consent at any time. Please note however that this will not affect the lawfulness of the processing before its withdrawal.
</p><p>
	If you are a resident in the European Economic Area and you believe we are unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority. You can find their contact details here: http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.
</p><p>
	Cookies and similar technologies: Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Sites. To opt-out of interest-based advertising by advertisers on our Sites visit http://www.aboutads.info/choices/. =
</p><p></p><p>
	<b id="scroll_11">11. CONTROLS FOR DO-NOT-TRACK FEATURES</b>
</p><p>
	Most web browsers and some mobile operating systems and mobile applications include a Do-Not-Track ("DNT") feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. No uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this Privacy Policy.
</p><p></p><p>
	<b id="scroll_12">12. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</b>
</p><p>
	In Short:  Yes, if you are a resident of California, you are granted specific rights regarding access to your personal information.
</p><p>
	California Civil Code Section 1798.83, also known as the "Shine The Light" law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.
</p><p>
	If you are under 18 years of age, reside in California, and have a registered account with the Sites, you have the right to request removal of unwanted data that you publicly post on the Sites. To request removal of such data, please contact us using the contact information provided below, and include the email address associated with your account and a statement that you reside in California. We will make sure the data is not publicly displayed on the Sites, but please be aware that the data may not be completely or comprehensively removed from all our systems.
</p><p></p><p>
	<b id="scroll_13">13. DO WE MAKE UPDATES TO THIS POLICY?</b>
</p><p>
	In Short:  Yes, we will update this policy as necessary to stay compliant with relevant laws.
</p><p>
	We may update this privacy policy from time to time. The updated version will be indicated by an updated "Revised" date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy policy, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification. We encourage you to review this privacy policy frequently to be informed of how we are protecting your information.
</p><p></p><p>
	<b id="scroll_14">14. HOW CAN YOU CONTACT US ABOUT THIS POLICY?</b>
</p><p>
	If you have questions or comments about this policy, you may contact our Data Protection Officer (DPO), Natalie Young-Calvert, by email at natalie.young-calvert@nhs.net, by phone at 0151 294 3322, or by post to:
</p><p>
	One Wirral
	Natalie Young-Calvert
	The Orchard Surgery, Bromborough Village Rd, Bromborough, Birkenhead, Wirral CH62 7EU, UK
	United Kingdom
</p><p>
	HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT FROM YOU?
</p><p>
	Based on the applicable laws of your country, you may have the right to request access to the personal information we collect from you, change that information, or delete it in some circumstances. To request to review, update, or delete your personal information, please submit a request form by clicking here. We will respond to your request within 30 days.




</p>


</div><div style="clear:both;"></div></div>


<div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
</div>

















<div style="clear:both;"></div>

<div class="footer" style="min-height:100px; background-color:black;">

	<div class="footer_half">
		<ul style="padding:50px;">
			<li style="margin-bottom:20px;"><img src="<? echo $fullurl ?>/staff-box/images/email_white.png" style="float:left;" /> <a href="mailto:info@onewirral.co.uk" style="color:white; padding-left:20px;"> info@onewirral.co.uk</a><div style="clear:both;"></div></li>
			<li style="margin-bottom:20px;"><img src="<? echo $fullurl ?>/staff-box/images/phone_white.png" style="float:left;" /> <p style="float:left; padding-left:20px;">0151 294 3322</p><div style="clear:both;"></div></li>
			<li><img src="<? echo $fullurl ?>/staff-box/images/location_white.png" style="float:left;" /> <p style="float:left; padding-left:20px;"> One Wirral CIC, Orchard Surgery, Brombrough Village Rd, <br />Brombrough, CH62 7EU</p><div style="clear:both;"></div></li>
		</ul>
	</div>
	<div class="footer_half">

		<p style="text-align:right;margin-top: 10px; margin-right: 20px;"><img src="<? echo $fullurl ?>/staff-box/images/Staff_Box_Logo_White.png" class="xstaff-box-footer" /> <br /> &copy; Staffbox Copyright <?echo date('Y');?> </p>
		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>
  <div style="padding-bottom:20px;">
		<div style="margin:0 auto; display:table;">
			<a href="privacy-policy.php" style="color:white; padding-right:20px;">Privacy Policy</a>
			<a href="terms-of-use.php" style="color:white;">Terms of Use</a>
		</div>
  </div>



<div style="clear:both;"></div>
<div style="width:100%;color:white;">

</div>
<div style="clear:both;"></div>
</div>







</div>
</div>





</div>
</div>
</body>
</html>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>





<script>
var width = $(window).width();

if(width > 500){
  function openNav() {
    document.getElementById("mySidepanel").style.width = "350px";
  }
}else{
  function openNav() {
    document.getElementById("mySidepanel").style.width = "100%";
  }
}



function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
}
function openNavright() {
  document.getElementById("mySidepanelright").style.width = "1050px";
}

function closeNavright() {
  document.getElementById("mySidepanelright").style.width = "0";
}


</script>







	<script>
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 20
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	</script>
