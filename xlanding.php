<?php
/**
*  __    __   __  ____    ____  _______ ___   ___
* |  |  |  | |  | \   \  /   / |   ____|\  \ /  /
* |  |__|  | |  |  \   \/   /  |  |__    \  V  /
* |   __   | |  |   \      /   |   __|    >   <
* |  |  |  | |  |    \    /    |  |____  /  .  \
* |__|  |__| |__|     \__/     |_______|/__/ \__\
*
*						BV System
*
* @author     Stephen Jones
* @copyright  1997-2019 Blowfish Technology Ltd
* @version    1
* @Date       01/07/2019
*/


 ?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<? include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_css.php'); ?>
<link href="<? echo $fullurl ?>assets/css/website.css" rel="stylesheet">



<link rel="shortcut icon" type="image/x-icon" href="<? echo $fullurl ?>assets/images/favicon-32x32.png" />
<title>Sky Recruitment</title>
</head>

<body>

	<div id="wrapper">
		<div class="video_background" id="video_background1" style='height:100vh;'>

			<div class="col-lg-3 col-md-2 col-sm-1 col-xs-1"></div>
			<div class="col-lg-6 col-md-8 col-sm-10 col-xs-10">
			       <img src="<? echo $fullurl; ?>assets/images/skylogo.png" class="img-responsive" id="video_logo" style="margin-top: 20vh;">
			</div>
			<div class="clearfix"></div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

						 <h1 style="text-align:center; color:#818286;">This site is under construction - new site coming soon! </h1>
			</div>

			<div class="clearfix">

			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

						 <h2 style="text-align:center; color:#818286;">You can still register</h2>
						 <a href="/landing" class="btn btn-info" style="color:white !important; margin:0 auto; display:table; background-color: #4ec3ff;">Register Here</a>
			</div>

		</div>
</div><!--wrapper-->

</style>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 background3">
         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>


        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
            <h3 class="color1">Sky Recruitment</h3>
            <p class="color">Having worked in a range of sectors for many years, our team knows what you’re looking for, and most importantly we know a good candidate when we see one.<br />
              <br />Operating Hours: 8am - 7pm, Monday - Friday
              <br />Out of Hours: 7pm - 11pm, Monday - Friday. 7am - 11pm, Saturday - Sunday<br />
            Out of Hours Tel: 07519 121385</p>
        </div>
        <!-- <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12"> -->

        <div class="col-lg-3 col-md-3  col-sm-6 col-xs-12">
            <address class="color pull-left pull-none-xs pull-right-sm">
            <h3 class="color1">Contact</h3>
            88-91 Walton Vale<br />
            Liverpool<br />
            L9 4RQ<br /><br />

            <a href="tel:01519092616" style="color:white;">Tel: 0151 909 2616</a><br />
            <a href="mailto:admin@skyrandd.co.uk" style="color:white;">E: admin@skyrandd.co.uk</a>
            </address>
        </div>
         <!-- <div class="col-lg-0 col-md-0 col-sm-4 col-xs-3"></div> -->
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-4 col-xs-offset-3"> <div class="page_spacer2"></div><div class="page_spacer2"></div><img class="img-responsive" src="<? echo $fullurl ?>assets/images/Comb-logo.png"><div class="page_spacer2"></div><div class="page_spacer2"></div></div>



      </div>

	<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->


	<!--[if (lt IE 9) & (!IEMobile)]>
	<script src="js/selectivizr-min.js"></script>
	<![endif]-->


</body>
<?
//JS Include
include($_SERVER['DOCUMENT_ROOT'].'/assets/app_php/base_js.php');
?>
<script src="<? echo $fullurl ?>assets/js/font.js"></script>
 <script src="<? echo $fullurl; ?>assets/js/select.js"></script>
<!-- <script src="<? echo $fullurl; ?>assets/js/backstretch.js"></script> -->
<script src="<? echo $fullurl ?>assets/js/jquery.vide.js"></script>
<script src="<? echo $fullurl; ?>assets/js/website.js"></script>
<script>
var windowSize = $(window).width();
if (windowSize > 767) {
	$('#video_background1').vide({
      mp4: '<? echo $fullurl ?>assets/images/Moving_Clouds.mp4',
			webm: '<? echo $fullurl ?>assets/images/Moving_Clouds.webm',
			ogv: '<? echo $fullurl ?>assets/images/Moving_Clouds.ogv',
			poster: '<? echo $fullurl ?>assets/images/Moving_Clouds.png'
		});
	}
			if (windowSize <= 767) {
				$('#video_background1').vide({
					mp4: '<? echo $fullurl ?>assets/images/Moving_Clouds.mp4',
					webm: '<? echo $fullurl ?>assets/images/Moving_Clouds.webm',
					ogv: '<? echo $fullurl ?>assets/images/Moving_Clouds.ogv',
					poster: '<? echo $fullurl ?>assets/images/Moving_Clouds.png'
 		 		});
			 }

		setTimeout(function () {
			$("#video_logo").fadeIn(1000, function() {});
		}, 3000);
</script>
</html>
